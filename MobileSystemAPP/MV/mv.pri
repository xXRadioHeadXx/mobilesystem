QT += core gui
CONFIG += c++11

INCLUDEPATH += $$PWD/

HEADERS += \
    $$PWD/treemodelunitnode.h \
    $$PWD/tablemodelunitnode.h \
    $$PWD/proxysorttreecontainerun.h \
    $$PWD/tablemodelarchivemsg.h \
    $$PWD/tablemodelarchiveimg.h

SOURCES += \
    $$PWD/treemodelunitnode.cpp \
    $$PWD/tablemodelunitnode.cpp \
    $$PWD/proxysorttreecontainerun.cpp \
    $$PWD/tablemodelarchivemsg.cpp \
    $$PWD/tablemodelarchiveimg.cpp
