﻿#ifndef TABLEMODELUNITNODE_H
#define TABLEMODELUNITNODE_H

#include <QAbstractTableModel>
#include <databasemanager.h>
#include <kamuflagemanager.h>
#include <kiparismanager.h>
#include <bskmanager.h>


class TableModelUnitNode : public QAbstractTableModel
{
    Q_OBJECT
public:
    explicit TableModelUnitNode(KamuflageManager *km,
                                KiparisManager *ks,
                                BSKManager *bsk,
                                DataBaseManager *db,
                                TypeUnitNodeSystem typeSystem = UnknownSystemType,//KMSystemType,
                                QObject *parent = nullptr);

    virtual ~TableModelUnitNode();

    int sortColumn = 0;
    Qt::SortOrder typeSortOrder = Qt::AscendingOrder;
    TypeUnitNodeSystem filterTypeSystem;

    DataBaseManager *dbManager;
    KamuflageManager *kmManager;
    KiparisManager *ksManager;
    BSKManager *bskManager;
    UnitNode *rootItem;
    QList<UnitNode*> listItem,
                     sortProxyListItem;
    UnitNode* selectUN;
//    bool flagDisplayHidden;

    // количество строк. Устанавливаем так, чтобы скроллер отображался корректно
    /*virtual*/ int rowCount(const QModelIndex &index = QModelIndex()) const override;
    // устанавливаем количество столбцов.
    /*virtual*/ int columnCount(const QModelIndex &index = QModelIndex()) const override;
    // функция для передачи данных пользователю
    /*virtual*/ QVariant data(const QModelIndex &index, int role) const override;
    // Функция для приёма данных от пользователя
    /*virtual*/  bool  setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole) override;
    // отображение   названий   столбцов
    /*virtual*/ QVariant headerData( int section, Qt::Orientation orientation, int role) const override;
    // возможность редактирования элемента
    /*virtual*/ Qt::ItemFlags flags(const QModelIndex &index) const override;
    //сортировка

    void filterHiddenUN();

signals:
    void selectedUN(QList<UnitNode*> listUN);
    void selectedUN(UnitNode* un);
    void needHideRow(int row);
    void needSelectRow(int row);

public slots:
    void updateStructure(UnitNode* un = nullptr);
    void updateUN(UnitNode* un = nullptr);
    void updateListItem();
    UnitNode* clickedUN(const QModelIndex &index);
    void emitNeedHideRow(int row);
    void sort(int column = 1, Qt::SortOrder order = Qt::AscendingOrder);
    void sortingProxyListItem();

};

#endif // TABLEMODELUNITNODE_H
