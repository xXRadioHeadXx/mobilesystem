#include <tablemodelarchiveimg.h>

TableModelArchiveIMG::TableModelArchiveIMG(DataBaseManager *db,
                                           TypeUnitNodeSystem system,
                                           QObject *parent) :
    QAbstractTableModel(parent),
    dbManager(db),
    typeSystem(system)
{
    //
    currentFilter.filterMode =
    currentFilter.filterDate =
    currentFilter.filterTime =
    currentFilter.filterSN =
    currentFilter.filterType =
    currentFilter.filterID = false;

    currentFilter.filterMSGAlarm =
    currentFilter.filterMSGConnect =
    currentFilter.filterMSGError =
    currentFilter.filterMSGDischarge =
    currentFilter.filterMSGFF =
    currentFilter.filterMSGCommand =
    currentFilter.filterMSGSystem = true;
    //
    needScroll = true;
//    m_listRecordsIMG = dbManager->getAllIMGRecords();
    m_listRecordsIMG = dbManager->getFilterIMGRecords(currentFilter, typeSystem);

    connect(dbManager,
            SIGNAL(insertNewIMG(quint32)),
            this,
            SLOT(updateListRecords(quint32)));
}

TableModelArchiveIMG::~TableModelArchiveIMG()
{
    try
    {
        dbManager = nullptr;
        newRecordIMG = 0;
        needScroll = false;
        m_listRecordsIMG.clear();
    }
    catch(...)
    {
#ifdef QT_DEBUG
        qDebug() << "Assert(~TableModelArchiveIMG)";
#endif
        return;
    }
}

// количество строк. Устанавливаем так, чтобы скроллер отображался корректно
int TableModelArchiveIMG::rowCount(const QModelIndex &index) const
{
    if(index.isValid())
        return m_listRecordsIMG.size();
    return m_listRecordsIMG.size();
}

// устанавливаем количество столбцов.
int TableModelArchiveIMG::columnCount(const QModelIndex &index) const
{
    if(index.isValid())
        return 9;
    return 9;
}

// функция для передачи данных пользователю
QVariant TableModelArchiveIMG::data(const QModelIndex &index, int role) const
{
    int row(index.row());
    QVariant result;
    // достаём запись из Select'a
    QSqlRecord imgRecord(dbManager->getIMGRecord(m_listRecordsIMG.at(row)));

    if(imgRecord.isEmpty())
    {
#ifdef QT_DEBUG
        qDebug() << "!2! data empety result !2! row[" << row << "] index[" << m_listRecordsIMG.at(row) << "]";
#endif
        return result;
    }

    // выводим в консоль текущие значения параметров и считаем, сколько
    // раз вызывается метод TableModelArchiveMSG::data и для каких ролей

    // закрасим строчку
    if((role == Qt::BackgroundRole) &&
       (0 == imgRecord.value("checkedUp").toUInt()))
        return QColor(255, 125, 125);

    // Если необходимо отобразить картинку - ловим роль Qt::DecorationRole
    // Если хотим отобразить CheckBox, используем роль Qt::CheckStateRole
    // если текущий вызов не относится к роли отображения, завершаем
    if (!index.isValid() ||
        (role != Qt::DisplayRole &&
        role != Qt::DecorationRole))
    {
        return result;
    }

    if (index.isValid() && role == Qt::DisplayRole)
    {
        switch(index.column())
        {
            case 0:
            {
                return imgRecord.value("id").toUInt();
            };

            case 1:
            {
//                return imgRecord.value("nameUN").toString();
                QString str;
                str.append(ProxyMetod().getNameDevice(imgRecord.value("idUN").toByteArray(),
                                                      (TypeUnitNodeSystem)imgRecord.value("typeSystemMSG").toUInt()));
                str.append("%1[");
                str = str.arg(imgRecord.value("numberVK").toInt());
                str.append(imgRecord.value("idUN").toByteArray().toHex().toUpper());
                str.append("] %1/%2");
                str = str.arg(imgRecord.value("area").toUInt())
                         .arg(imgRecord.value("number").toUInt());
                return str;
            };

            case 2:
            {
                return imgRecord.value("date").toDate().toString("dd.MM.yy");
            };

            case 3:
            {
                return imgRecord.value("time").toTime().toString("hh:mm:ss");
            };
        }
    }

    if (index.isValid() && role == Qt::DecorationRole)
    {
        switch(index.column())
        {
            case 4:
            {
                QPixmap pxm1;
                if(pxm1.loadFromData(imgRecord.value("dataIMG1").toByteArray()))
                {
                    if(pxm1.isNull())
                        return result;

                    return pxm1.scaled(80, 60, Qt::KeepAspectRatio, Qt::FastTransformation);
//                    return pxm1.scaled(80, 60, Qt::KeepAspectRatio, Qt::SmoothTransformation);
                }
                return result;
            };

            case 5:
            {
                QPixmap pxm2;
                if(pxm2.loadFromData(imgRecord.value("dataIMG2").toByteArray()))
                {
                    if(pxm2.isNull())
                        return result;

                    return pxm2.scaled(80, 60, Qt::KeepAspectRatio, Qt::FastTransformation);
//                    return pxm2.scaled(80, 60, Qt::KeepAspectRatio, Qt::SmoothTransformation);
                }
                return result;
            };

            case 6:
            {
                QPixmap pxm3;
                if(pxm3.loadFromData(imgRecord.value("dataIMG3").toByteArray()))
                {
                    if(pxm3.isNull())
                        return result;

                    return pxm3.scaled(80, 60, Qt::KeepAspectRatio, Qt::FastTransformation);
//                    return pxm3.scaled(80, 60, Qt::KeepAspectRatio, Qt::SmoothTransformation);
                }
                return result;
            };

            case 7:
            {
                QPixmap pxm4;
                if(pxm4.loadFromData(imgRecord.value("dataIMG4").toByteArray()))
                {
                    if(pxm4.isNull())
                        return result;

                    return pxm4.scaled(80, 60, Qt::KeepAspectRatio, Qt::FastTransformation);
//                    return pxm4.scaled(80, 60, Qt::KeepAspectRatio, Qt::SmoothTransformation);
                }
                return result;
            };

            case 8:
            {
                QPixmap pxm5;
                if(pxm5.loadFromData(imgRecord.value("dataIMG5").toByteArray()))
                {
                    if(pxm5.isNull())
                        return result;

                    return pxm5.scaled(80, 60, Qt::KeepAspectRatio, Qt::FastTransformation);
//                    return pxm5.scaled(80, 60, Qt::KeepAspectRatio, Qt::SmoothTransformation);
                }
                return result;
            };
        }
    }

    return result;
}

// Функция для приёма данных от пользователя
//bool TableModelArchiveIMG::setData(QModelIndex index, QVariant value, int role)
//{
//    if(index.isValid() && value.isValid() && (-1 != role))
//        return false;
//    return false;
//}

// отображение   названий   столбцов
QVariant TableModelArchiveIMG::headerData(int section, Qt::Orientation orientation, int role) const
{
    // Для любой роли, кроме запроса на отображение, прекращаем обработку
    if (role != Qt::DisplayRole)
        return QVariant();

    // формируем заголовки по номуру столбца
    if (orientation == Qt::Horizontal)
    {
        switch(section)
        {
            case 0:
                return "№/#";
            case 1:
                return tr("From");
            case 2:
                return tr("Date");
            case 3:
                return tr("Time");
            case 4:
                return tr("AfAl1");
//                return tr("FF1");
            case 5:
                return tr("AfAl2");
//                return tr("FF2");
            case 6:
                return tr("AfAl3");
//                return tr("FF3");
            case 7:
                return tr("Al");
                return tr("FF4");
            case 8:
                return tr("PstAl");
                return tr("FF5");
        }
    }
    return QVariant();
};

// возможность редактирования элемента
Qt::ItemFlags TableModelArchiveIMG::flags(const QModelIndex &index) const
{
    Qt::ItemFlags result;
    if (!index.isValid())
        return Qt::ItemIsEnabled;

    // разрешаем редактирование всего, кроме первого столбца
    result = QAbstractTableModel::flags(index);
    result |= Qt::ItemIsEditable;
    return result;
}

//добавление строки
bool TableModelArchiveIMG::insertRows(int row, int count, const QModelIndex &parent)
{
    if(1 == count)
    {
        this->beginInsertRows(parent, row, row);

        m_listRecordsIMG.insert(row, newRecordIMG);
        emit this->newRecord(row, newRecordIMG);

        this->endInsertRows();
        return true;
    }
    return false;
}


//обновление выборки
void TableModelArchiveIMG::updateListRecords(quint32 idIMG)
{
    newRecordIMG = idIMG;
    QSqlRecord newRecord(dbManager->getIMGRecord(newRecordIMG));
    if(typeSystem != newRecord.value("typeSystemMSG").toInt())
    {
        return;
    }

    if(0 != newRecordIMG &&
       !m_listRecordsIMG.contains(newRecordIMG))
    {
        if(currentFilter.filterMode)
        {

            if(currentFilter.filterDate)
            {
                if(currentFilter.filterMinDate > newRecord.value("date").toDate() ||
                   newRecord.value("date").toDate() > currentFilter.filterMaxDate)
                {
                    return;
                }

            }

            if(currentFilter.filterTime)
            {
                if(currentFilter.filterMinTime > newRecord.value("time").toTime() ||
                   newRecord.value("time").toTime() > currentFilter.filterMaxTime)
                {
                    return;
                }

            }

            if(currentFilter.filterSN)
            {
                if(-1 != currentFilter.filterSector &&
                   newRecord.value("area").toInt() != currentFilter.filterSector)
                {
                    return;
                }

                if(-1 != currentFilter.filterNumber &&
                   newRecord.value("number").toInt() != currentFilter.filterNumber)
                {
                    return;
                }
            }

            if(currentFilter.filterType)
            {
                if((TypeUnitNodeDevice)newRecord.value("typeUN").toUInt() != currentFilter.filterTypeValue)
                {
                    return;
                }
            }

            if(currentFilter.filterID)
            {
                if(newRecord.value("idUN").toByteArray() != currentFilter.filterIDValue)
                {
                    return;
                }
            }

            this->insertRows(this->rowCount());
        }
        else
        {
            this->insertRows(this->rowCount());
        }
        emitNeedScrollToBottom();
    }
    else
    {
        emit this->dataChanged(this->createIndex(m_listRecordsIMG.indexOf(newRecordIMG), 0),
                               this->createIndex(m_listRecordsIMG.indexOf(newRecordIMG), 8));
        emit this->newRecord(m_listRecordsIMG.indexOf(newRecordIMG), newRecordIMG);
    }

//    //
//    QSqlRecord imgRecord(dbManager->getIMGRecord(idIMG));
//    if(imgRecord.isEmpty())
//    {
//#ifdef QT_DEBUG
//        qDebug() << "updateListRecords empety result idIMG[" << idIMG << "]";
//#endif
//        return;
//    }
//    //

//    newRecordIMG = idIMG;
//    if(0 != newRecordIMG)
//    {
//        if(!m_listRecordsIMG.contains(newRecordIMG))
//        {
//            this->insertRows(this->rowCount());
//            emitNeedScrollToBottom();
//        }
//        else
//        {
//            emit this->dataChanged(this->createIndex(m_listRecordsIMG.indexOf(newRecordIMG), 0),
//                                   this->createIndex(m_listRecordsIMG.indexOf(newRecordIMG), 8));
//            emit this->newRecord(m_listRecordsIMG.indexOf(newRecordIMG), newRecordIMG);
//        }
//    }
}

void TableModelArchiveIMG::setNeedScroll(bool value)
{
    needScroll = value;
}

void TableModelArchiveIMG::emitNeedViewIMG(QModelIndex index)
{
    dbManager->setIMGCheckedUp(m_listRecordsIMG.at(index.row()),
                               true);
    emit this->dataChanged(this->createIndex(index.row(), 0),
                           this->createIndex(index.row(), 8));
    emit needViewIMG(m_listRecordsIMG.at(index.row()), ((index.column() < 4) ? 0 : (index.column() - 3)));

}

void TableModelArchiveIMG::emitViewlastQueueIMG()
{
    QSqlRecord record(this->dbManager->getLastQueueIMGRecord());
    if(record.isEmpty())
        return;

    dbManager->setIMGCheckedUp(record.value("id").toUInt());

    emit this->dataChanged(this->createIndex(m_listRecordsIMG.indexOf(record.value("id").toUInt()), 0),
                           this->createIndex(m_listRecordsIMG.indexOf(record.value("id").toUInt()), 8));
    emit needViewIMG(record.value("id").toUInt(), 0);

}

void TableModelArchiveIMG::emitNeedScrollToBottom()
{
    if(needScroll)
    {
        emit this->needScrollToBottom();
    }
}

void TableModelArchiveIMG::updateListRecordsIMG()
{
    this->beginResetModel();
    m_listRecordsIMG = dbManager->getFilterIMGRecords(currentFilter, typeSystem);
    this->endResetModel();
}

void TableModelArchiveIMG::updateFilter(FilterStruct newFilter)
{
    currentFilter = newFilter;
    updateListRecordsIMG();
}
