﻿#ifndef TREEMODELUNITNODE_H
#define TREEMODELUNITNODE_H

#include <QAbstractItemModel>
#include <databasemanager.h>
#include <kamuflagemanager.h>
#include <kiparismanager.h>
#include <bskmanager.h>
#include <proxysorttreecontainerun.h>
#include <global_value_and_structure.h>


class TreeModelUnitNode : public QAbstractItemModel
{
    Q_OBJECT
public:
    explicit TreeModelUnitNode(KamuflageManager *km,
                               KiparisManager *ks,
                               BSKManager *bsk,
                               DataBaseManager *db,
                               TypeUnitNodeSystem typeSystem = UnknownSystemType,
                               QObject *parent = nullptr);

    Qt::SortOrder sortOrder = Qt::AscendingOrder;
    KamuflageManager *kmManager;
    KiparisManager *ksManager;
    BSKManager *bskManager;
    DataBaseManager *dbManager;
    UnitNode *rootItemUN;
    ProxySortTreeContainerUN *rootItemSortUN;
    QList<UnitNode*> listItemUN;
    QList<ProxySortTreeContainerUN*> sortProxyListItemTC,
                                     sortProxyListParentItemTC;
    TypeUnitNodeSystem filterTypeSystem;
    UnitNode* selectUN;

    QVariant data(const QModelIndex &index, int role) const override;
    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole) override;
    Qt::ItemFlags flags(const QModelIndex &index) const override;
    QVariant headerData(int section,
                        Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const override;
    QModelIndex index(int row,
                      int column,
                      const QModelIndex &parent = QModelIndex()) const override;
    QModelIndex parent(const QModelIndex &index) const;
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;

//    QModelIndex findeIndexUNL(UnitNode *un,
//                              UnitNode *parentUN = nullptr);
    QModelIndex findeIndexUNL(ProxySortTreeContainerUN *tc,
                              ProxySortTreeContainerUN *parentTC = nullptr);

//    QModelIndex findeIndexUNR(UnitNode *un,
//                              UnitNode *parentUN = nullptr);
    QModelIndex findeIndexUNR(ProxySortTreeContainerUN *tc,
                              ProxySortTreeContainerUN *parentTC = nullptr);

//    QModelIndex findeIndexUN(UnitNode *un,
//                             int rl,
//                             UnitNode *parent = nullptr);
    QModelIndex findeIndexUN(ProxySortTreeContainerUN *tc,
                             int rl,
                             ProxySortTreeContainerUN *parentTC = nullptr);

    void createProxySortTree();
    void sortingProxyListItemUN();
    void sortingListItemUN();
    void filterHiddenUN();

signals:
    void selectedUN(QList<UnitNode*> listUN);
    void selectedUN(UnitNode* un);
    void needHideRow(int row);
    void needSelectRow(int row);

public slots:
    void appendNewUNInStructure(UnitNode* un);
    void updateUNStructure(UnitNode* un);
    UnitNode *clickedUN(const QModelIndex &index);
//    void moveUNStructure(UnitNode *objPtr,
//                         UnitNode *sourceParent,
//                         int sourceChild,
//                         UnitNode *destinationParent,
//                         int destinationChild);
    //сортировка
//    void sort(int column, Qt::SortOrder order = Qt::AscendingOrder);


};

#endif // TREEMODELUNITNODE_H
