#include <proxysorttreecontainerun.h>

ProxySortTreeContainerUN::ProxySortTreeContainerUN(QObject *parent) /*noexcept*/ :
QObject(parent)
{
    m_un = nullptr;
}

ProxySortTreeContainerUN::~ProxySortTreeContainerUN() /*noexcept*/
{
    try
    {

    }
    catch(...)
    {
#ifdef QT_DEBUG
        qDebug() << "Assert(~ProxySortTreeContainerUN)";
#endif
        return;
    }
}

void ProxySortTreeContainerUN::insertUN(int area, UnitNode* un) /*noexcept*/
{
    numArea = area;
    m_un = un;
}

void ProxySortTreeContainerUN::appendChild(ProxySortTreeContainerUN *tc) /*noexcept*/
{
    if(!tc)
    {
        return;
    }
    if(!this->listChilde.contains(tc))
    {
        this->listChilde.append(tc);
        tc->treeParent = this;
    }
}

ProxySortTreeContainerUN* ProxySortTreeContainerUN::child(int num) /*noexcept*/
{
    return listChilde.value(num, 0);
}

int ProxySortTreeContainerUN::childCount() const /*noexcept*/
{
    return this->listChilde.size();
}

int ProxySortTreeContainerUN::row() const /*noexcept*/
{
    if (treeParent)
        return treeParent->listChilde.indexOf(const_cast<ProxySortTreeContainerUN*>(this));
    return 0;
}

int ProxySortTreeContainerUN::columnCount() const /*noexcept*/
{
    return 4;//itemData.count();
}

QVariant ProxySortTreeContainerUN::data(int column) const /*noexcept*/
{
    if(m_un)
    {
        return m_un->data(column);
    }
    else
    {
        if(0xFF == numArea)
        {
            switch(column)
            {
                case 0:
                {
                    return QVariant(QObject::tr("Device"));

                }
                case 1:
                {
                    return QVariant(QObject::tr("S/Num"));
                }
                case 2:
                {
                    return QVariant(QObject::tr("U, pow"));
                }
                case 3:
                {
                    return QVariant(QObject::tr("ID"));
                }
            }
        }
        else
        {
            switch(column)
            {
                case 0://Тип устройства
                {
                    QString str(QObject::tr("S"));
                    str.append(" %1");
                    str = str.arg(numArea);

                    return QVariant(str);
                }
                default:
                {
                    return QVariant();
                }
            }
        }
    }
    return QVariant();
}

QPixmap ProxySortTreeContainerUN::getViewPxm(void) /*noexcept*/
{
    if(m_un)
    {
        return m_un->getViewPXM();
    }
    else
    {
        QPixmap pxm;
        if(pxm.load(":/icon/folder_20x20.png"))
        {
            return pxm;
        }
    }
    return QPixmap();
}

QPixmap ProxySortTreeContainerUN::getViewRssiPxm(void) /*noexcept*/
{
    if(m_un)
    {
        return m_un->getViewRssiPXM();
    }
    return QPixmap();
}

