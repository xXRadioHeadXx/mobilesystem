#ifndef TABLEMODELARCHIVEIMG_H
#define TABLEMODELARCHIVEIMG_H

#include <QAbstractTableModel>
#include <databasemanager.h>

class TableModelArchiveIMG : public QAbstractTableModel
{
    Q_OBJECT
public:
    explicit TableModelArchiveIMG(DataBaseManager *db,
                                  TypeUnitNodeSystem system,
                                  QObject *parent = nullptr);
    virtual ~TableModelArchiveIMG();

    //
    DataBaseManager *dbManager;
    QList<quint32> m_listRecordsIMG;
    quint32 newRecordIMG;
    bool needScroll;
    TypeUnitNodeSystem typeSystem;
    //
    FilterStruct currentFilter;
    //
    void updateListRecordsIMG();
    void updateFilter(FilterStruct newFilter);
    // количество строк. Устанавливаем так, чтобы скроллер отображался корректно
    /*virtual*/ int rowCount(const QModelIndex &index = QModelIndex()) const override;
    // устанавливаем количество столбцов.
    /*virtual*/ int columnCount(const QModelIndex &index = QModelIndex()) const override;
    // функция для передачи данных пользователю
    /*virtual*/ QVariant data(const QModelIndex &index, int role) const override;
    // Функция для приёма данных от пользователя
//    virtual  bool  setData(QModelIndex index, QVariant value, int role);
    // отображение   названий   столбцов
    /*virtual*/ QVariant headerData( int section, Qt::Orientation orientation, int role) const override;
    // возможность редактирования элемента
    /*virtual*/ Qt::ItemFlags flags(const QModelIndex &index) const override;
    // добавление новой строки
    /*virtual*/ bool insertRows(int row, int count = 1, const QModelIndex &parent = QModelIndex()) override;


signals:
    void needScrollToBottom();
    void needViewIMG(quint32 dbIndex, int number);
    void newRecord(quint32 index, quint32 dbIndex);


public slots:
    //обновление выборки
    void updateListRecords(quint32 idIMG);
    void setNeedScroll(bool value);
    void emitNeedViewIMG(QModelIndex index);
    void emitViewlastQueueIMG();
    void emitNeedScrollToBottom();

};

#endif // TABLEMODELARCHIVEIMG_H
