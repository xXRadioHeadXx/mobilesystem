#ifndef PROXYSORTTREECONTAINERUN_H
#define PROXYSORTTREECONTAINERUN_H

#include <QObject>
#include <unitnode.h>

class ProxySortTreeContainerUN : public QObject
{
    Q_OBJECT
public:

    UnitNode *m_un;

    explicit ProxySortTreeContainerUN(QObject *parent = nullptr) /*noexcept*/;
    virtual ~ProxySortTreeContainerUN();

    int numArea;
    void insertUN(int area, UnitNode* un) /*noexcept*/;

    QPixmap getViewPxm(void) /*noexcept*/;
    QPixmap getViewRssiPxm(void) /*noexcept*/;


    /// относящиеся в моделяи представления -->
    ProxySortTreeContainerUN *treeParent; //родительское устройство
    QList<ProxySortTreeContainerUN*> listChilde; //список детей

    ProxySortTreeContainerUN* child(int num) /*noexcept*/;
    ProxySortTreeContainerUN* getTreeParent() /*noexcept*/;

    void appendChild(ProxySortTreeContainerUN *tc) /*noexcept*/;
    int childCount() const /*noexcept*/;
    int columnCount() const /*noexcept*/;

    int row() const /*noexcept*/;
    /// относящиеся в моделяи представления <--

signals:

public slots:
    QVariant data(int column) const /*noexcept*/;
};

#endif // PROXYSORTTREECONTAINERUN_H
