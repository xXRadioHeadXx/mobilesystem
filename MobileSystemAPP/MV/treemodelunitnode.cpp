﻿#include <treemodelunitnode.h>

TreeModelUnitNode::TreeModelUnitNode(KamuflageManager *km,
                                     KiparisManager *ks,
                                     BSKManager *bsk,
                                     DataBaseManager *db,
                                     TypeUnitNodeSystem typeSystem,
                                     QObject *parent) :
QAbstractItemModel(parent),
kmManager(km),
ksManager(ks),
bskManager(bsk),
dbManager(db),
filterTypeSystem(typeSystem),
sortOrder(Qt::AscendingOrder)

{
    try
    {
        selectUN = nullptr;
        rootItemSortUN = new ProxySortTreeContainerUN;
        rootItemSortUN->numArea = 0xFF;
    }
    catch (std::runtime_error &rte)
    {
        qWarning("catch runtime_error %s [TreeModelUnitNode::TreeModelUnitNode]", rte.what());
    }
    catch (std::bad_alloc& ba)
    {
        qWarning("catch bad_alloc %s [TreeModelUnitNode::TreeModelUnitNode]", ba.what());
    }
    catch (std::exception &e)
    {
        qWarning("catch exception %s [TreeModelUnitNode::TreeModelUnitNode]", e.what());
    }
    catch (...)
    {
        qWarning("catch exception <unknown> [TreeModelUnitNode::TreeModelUnitNode]");
    }
}

int TreeModelUnitNode::columnCount(const QModelIndex &parent) const
 {
     if(parent.isValid())
//         return static_cast<UnitNode*>(parent.internalPointer())->columnCount();
         return static_cast<ProxySortTreeContainerUN*>(parent.internalPointer())->columnCount();
     else
//         return rootItemUN->columnCount();
         return rootItemSortUN->columnCount();
 }

 QVariant TreeModelUnitNode::data(const QModelIndex &index, int role) const
 {
     if (!index.isValid())
         return QVariant();

//     UnitNode *item = static_cast<UnitNode*>(index.internalPointer());
     ProxySortTreeContainerUN *item = static_cast<ProxySortTreeContainerUN*>(index.internalPointer());

//     if(role == Qt::BackgroundRole && !item->flag_connection)
     if(item->m_un)
         if(role == Qt::BackgroundRole && !item->m_un->flag_connection)
         {
//            return QVariant(QColor(0xCC,0xCC,0xCC));
            return QVariant(QColor(Qt::lightGray));
         }

     if(role == Qt::DecorationRole/* &&
        rootItem != item*/)
     {
         QPixmap pxm;
         switch(index.column())
         {
             case 0:
             {
                 pxm = item->getViewPxm();
                 break;
             }
             case 2:
             {
                 pxm = item->getViewRssiPxm();
                 break;
             }
             default:
             {
                 break;
             }
         }
         if(pxm.isNull())
             return QVariant();

         return QVariant(pxm);
//         return QVariant(pxm.scaled(20, 20, Qt::KeepAspectRatio, Qt::FastTransformation));
//         return QVariant(pxm.scaled(20, 20, Qt::KeepAspectRatio, Qt::SmoothTransformation));
     }

     if(role == Qt::DisplayRole)
         return item->data(index.column());

     return QVariant();
 }

bool TreeModelUnitNode::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if(!index.isValid() || value.toString().isEmpty())
        return false;

    if(Qt::EditRole == role)
    {
//        UnitNode *item = static_cast<UnitNode*>(index.internalPointer());
//        item->updateAbstractName(value.toByteArray());
        ProxySortTreeContainerUN *item = static_cast<ProxySortTreeContainerUN*>(index.internalPointer());
        item->m_un->updateAbstractName(value.toByteArray());
        return true;
    }
    return false;
}

 Qt::ItemFlags TreeModelUnitNode::flags(const QModelIndex &index) const
 {
     if (!index.isValid())
         return 0;

     if(0 == index.column())
     {
         return Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsEditable;
     }
     else
     {
         return Qt::ItemIsEnabled | Qt::ItemIsSelectable;
     }
 }

 QVariant TreeModelUnitNode::headerData(int section, Qt::Orientation orientation,
                                int role) const
 {
     if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
     {
         switch(section)
         {
             case 0:
             {
                 return QVariant(QObject::tr("Device"));

             }
             case 1:
             {
                 return QVariant(QObject::tr("S/Num"));
             }
             case 2:
             {
                 return QVariant(QObject::tr("U, pow"));
             }
             case 3:
             {
                 return QVariant(QObject::tr("ID"));
             }
             default:
                 return QVariant();
         }
     }

     return QVariant();
 }

 QModelIndex TreeModelUnitNode::index(int row, int column, const QModelIndex &parent) const
 {
     if (!hasIndex(row, column, parent))
         return QModelIndex();

//     UnitNode *parentItem;
     ProxySortTreeContainerUN *parentItem;

     if (!parent.isValid())
//         parentItem = rootItemUN;
         parentItem = rootItemSortUN;
     else
//         parentItem = static_cast<UnitNode*>(parent.internalPointer());
         parentItem = static_cast<ProxySortTreeContainerUN*>(parent.internalPointer());

//     UnitNode *childItem = parentItem->child(row);
     ProxySortTreeContainerUN *childItem = parentItem->child(row);
     if (childItem)
         return createIndex(row, column, childItem);
     else
         return QModelIndex();
 }

 QModelIndex TreeModelUnitNode::parent(const QModelIndex &index) const
 {
     if (!index.isValid())
         return QModelIndex();

//     UnitNode *childItem = static_cast<UnitNode*>(index.internalPointer());
//     UnitNode *parentItem = childItem->unTreeParent;
     ProxySortTreeContainerUN *childItem = static_cast<ProxySortTreeContainerUN*>(index.internalPointer());
     ProxySortTreeContainerUN *parentItem = childItem->treeParent;

//     if (parentItem == rootItemUN)
     if (parentItem == rootItemSortUN)
         return QModelIndex();

     return createIndex(parentItem->row(), 0, parentItem);
 }

 int TreeModelUnitNode::rowCount(const QModelIndex &parent) const
 {
//     UnitNode *parentItem;
     ProxySortTreeContainerUN *parentItem;
     if (parent.column() > 0)
         return 0;

     if (!parent.isValid())
//         parentItem = rootItemUN;
         parentItem = rootItemSortUN;
     else
//         parentItem = static_cast<UnitNode*>(parent.internalPointer());
         parentItem = static_cast<ProxySortTreeContainerUN*>(parent.internalPointer());

     return parentItem->childCount();
 }

 void TreeModelUnitNode::appendNewUNInStructure(UnitNode* un)
 {
     if(filterTypeSystem != un->_typeSystem() &&
        UnknownSystemType != filterTypeSystem)
         return;

     this->beginResetModel();
     this->createProxySortTree();//
     this->endResetModel();
     return;

//     this->beginInsertRows(this->findeIndexUNL(un->unTreeParent),
//                           un->unTreeParent->listChilde.indexOf(un),
//                           un->unTreeParent->listChilde.indexOf(un));
//     this->endInsertRows();
 }

 void TreeModelUnitNode::updateUNStructure(UnitNode* un)
 {
     if(filterTypeSystem != un->_typeSystem() &&
        UnknownSystemType != filterTypeSystem)
         return;

     this->beginResetModel();
     this->createProxySortTree();
     this->endResetModel();
     return;

//     emit this->dataChanged(this->findeIndexUNL(un),
//                            this->findeIndexUNR(un));//3
 }

 UnitNode * TreeModelUnitNode::clickedUN(const QModelIndex &index)
 {
     if (!index.isValid())
         return nullptr;

//     UnitNode *item = static_cast<UnitNode*>(index.internalPointer());
//     item->graphicsPxmItem->setSelected(true);
//     emit selectedUN(item);
//     return item;
     ProxySortTreeContainerUN *item = static_cast<ProxySortTreeContainerUN*>(index.internalPointer());
     if(item->m_un)
     {
         item->m_un->graphicsPxmItem->setSelected(true);
         emit selectedUN(item->m_un);
         return item->m_un;
     }
     return nullptr;
 }

//QModelIndex TreeModelUnitNode::findeIndexUNL(UnitNode *un,
//                                             UnitNode *parentUN)
//{
//    return this->findeIndexUN(un, 0, parentUN);
//}

QModelIndex TreeModelUnitNode::findeIndexUNL(ProxySortTreeContainerUN *tc,
                                             ProxySortTreeContainerUN *parentTC)
{
    return this->findeIndexUN(tc, 0, parentTC);
}

//QModelIndex TreeModelUnitNode::findeIndexUNR(UnitNode *un,
//                                             UnitNode *parentUN)
//{
//    return this->findeIndexUN(un, 4, parentUN);///2
//}

QModelIndex TreeModelUnitNode::findeIndexUNR(ProxySortTreeContainerUN *tc,
                                             ProxySortTreeContainerUN *parentTC)
{
    return this->findeIndexUN(tc, 4, parentTC);///2
}


//QModelIndex TreeModelUnitNode::findeIndexUN(UnitNode *un,
//                                            int lr,
//                                            UnitNode *parentUN)
//{
//     QModelIndex index;

//     if (0 == parentUN)
//     {
//         parentUN = rootItemUN;
//         if(parentUN == un)
//         {
//             qDebug() << "findeIndexUN false 1";
//             return index;
//         }
//     }

//     for(int i(0), n(un->childCount()); i < n; i++)
//     {
//         if (parentUN->child(i) == un)
//         {
//             return createIndex(i, lr, un);
//         }
//         else
//         {
//             index = this->findeIndexUN(un,
//                                        lr,
//                                        parentUN);///1
//             if(index.isValid())
//             {
//                 qDebug() << "findeIndexUN " << index;
//                 return index;
//             }
//         }

//     }
//     qDebug() << "findeIndexUN false 2";
//     return index;
//}

QModelIndex TreeModelUnitNode::findeIndexUN(ProxySortTreeContainerUN *tc,
                                            int lr,
                                            ProxySortTreeContainerUN *parentTC)
{
     QModelIndex index;

     if (0 == parentTC)
     {
         parentTC = rootItemSortUN;
         if(parentTC == tc)
         {
#ifdef QT_DEBUG
             qDebug() << "findeIndexUN false 1";
#endif
             return index;
         }
     }

     for(int i(0), n(tc->childCount()); i < n; i++)
     {
         if (parentTC->child(i) == tc)
         {
             return createIndex(i, lr, tc);
         }
         else
         {
             index = this->findeIndexUN(tc,
                                        lr,
                                        parentTC);///1
             if(index.isValid())
             {
#ifdef QT_DEBUG
                 qDebug() << "findeIndexUN " << index;
#endif
                 return index;
             }
         }

     }
#ifdef QT_DEBUG
     qDebug() << "findeIndexUN false 2";
#endif
     return index;
}


//void TreeModelUnitNode::moveUNStructure(UnitNode *objPtr,
//                                        UnitNode *sourceParent,
//                                        int sourceChild,
//                                        UnitNode *destinationParent,
//                                        int destinationChild)
//{
//    this->beginResetModel();
//    this->createProxySortTree();
//    this->endResetModel();
//    return;
//}

//сортировка
//void TreeModelUnitNode::sort(int column, Qt::SortOrder order)
//{

//}

void TreeModelUnitNode::createProxySortTree()
{
//    this->beginResetModel();
    switch(this->filterTypeSystem)
    {
        case KMSystemType:
        {
            listItemUN = kmManager->getListUnitNode();
            break;
        }
        case KSSystemType:
        {
            listItemUN = ksManager->getListUnitNode();
            break;
        }
        case BSKSystemType:
        {
            listItemUN = bskManager->getListUnitNode();
            break;
        }
        case UnknownSystemType:
        {
            listItemUN = kmManager->getListUnitNode();
            listItemUN.append(ksManager->getListUnitNode());
            listItemUN.append(bskManager->getListUnitNode());
            break;
        }
        default:
        {
            break;
        }
    }
    filterHiddenUN();
    sortingListItemUN();

    //добавление новых в sortProxyListItemTC -->
    for(int i(0), n(listItemUN.size()); i < n; i++)
    {
        bool appendKey(true);
        for(int j(0); j < sortProxyListItemTC.size(); j++)
        {
            if(listItemUN.at(i) == sortProxyListItemTC.at(j)->m_un)
            {
                appendKey = false;
                break;
            }
        }
        try
        {
            if(appendKey)
            {
                ProxySortTreeContainerUN *newTC(new ProxySortTreeContainerUN);
                newTC->insertUN(listItemUN.at(i)->_numArea(), listItemUN.at(i));
                newTC->numArea = listItemUN.at(i)->_numArea();
                sortProxyListItemTC.append(newTC);
            }
        }
        catch (std::runtime_error &rte)
        {
            qWarning("catch runtime_error %s [TreeModelUnitNode::createProxySortTree]", rte.what());
        }
        catch (std::bad_alloc& ba)
        {
            qWarning("catch bad_alloc %s [TreeModelUnitNode::createProxySortTree]", ba.what());
        }
        catch (std::exception &e)
        {
            qWarning("catch exception %s [TreeModelUnitNode::createProxySortTree]", e.what());
        }
        catch (...)
        {
            qWarning("catch exception <unknown> [TreeModelUnitNode::createProxySortTree]");
        }
    }
    for(int i(0); i < sortProxyListItemTC.size(); i++)
    {
        sortProxyListItemTC.at(i)->numArea = sortProxyListItemTC.at(i)->m_un->_numArea();
    }
    //добавление новых в sortProxyListItemTC <--
    //отцепляем sortProxyListItemTC от sortProxyListParentItemTC -->
    for(int i(0), n(sortProxyListItemTC.size()); i < n; i++)
    {
        sortProxyListItemTC.at(i)->treeParent = nullptr;
    }
    //отцепляем sortProxyListItemTC от sortProxyListParentItemTC <--

    //очистим sortProxyListParentItemTC -->
    this->rootItemSortUN->listChilde.clear();
    sortProxyListParentItemTC.clear();
    //очистим sortProxyListParentItemTC <--

    //добавление новых в sortProxyListParentItemTC -->
    for(int i(0), n(sortProxyListItemTC.size()); i < n; i++)
    {
        bool appendKey(true);
        for(int j(0); j < sortProxyListParentItemTC.size(); j++)
        {
            if(sortProxyListParentItemTC.at(j)->numArea == sortProxyListItemTC.at(i)->numArea)
            {
                appendKey = false;
                break;
            }
        }
        try
        {
            if(appendKey)
            {
                ProxySortTreeContainerUN *newTC(new ProxySortTreeContainerUN);
                newTC->insertUN(sortProxyListItemTC.at(i)->numArea, 0);
                newTC->treeParent = this->rootItemSortUN;
                this->rootItemSortUN->appendChild(newTC);

                sortProxyListParentItemTC.append(newTC);
            }
        }
        catch (std::runtime_error &rte)
        {
            qWarning("catch runtime_error %s [TreeModelUnitNode::createProxySortTree]_2", rte.what());
        }
        catch (std::bad_alloc& ba)
        {
            qWarning("catch bad_alloc %s [TreeModelUnitNode::createProxySortTree]_2", ba.what());
        }
        catch (std::exception &e)
        {
            qWarning("catch exception %s [TreeModelUnitNode::createProxySortTree]_2", e.what());
        }
        catch (...)
        {
            qWarning("catch exception <unknown> [TreeModelUnitNode::createProxySortTree]_2");
        }
    }
    //добавление новых в sortProxyListParentItemTC <--

    //удаление из sortProxyListParentItemTC -->
    for(int i(0); i < sortProxyListParentItemTC.size(); i++)
    {
        bool removeKey(true);
        for(int j(0); j < sortProxyListItemTC.size(); j++)
        {
            if(sortProxyListParentItemTC.at(i)->numArea == sortProxyListItemTC.at(j)->numArea)
            {
                removeKey = false;
                break;
            }
        }
        if(removeKey)
        {
            sortProxyListParentItemTC.at(i)->treeParent = nullptr;
            sortProxyListParentItemTC.at(i)->listChilde.clear();
            sortProxyListParentItemTC.removeAt(i);
        }
    }
    //удаление из sortProxyListParentItemTC <--
    //добавление sortProxyListItemTC к sortProxyListParentItemTC -->
    for(int i(0); i < sortProxyListParentItemTC.size(); i++)
    {
        for(int j(0); j < sortProxyListItemTC.size(); j++)
        {
            if(sortProxyListParentItemTC.at(i)->numArea == sortProxyListItemTC.at(j)->numArea)
            {
                sortProxyListItemTC.at(j)->treeParent = sortProxyListParentItemTC.at(i);
                sortProxyListParentItemTC.at(i)->appendChild(sortProxyListItemTC.at(j));
            }
        }
    }
    //добавление sortProxyListItemTC к sortProxyListParentItemTC <--
    sortingProxyListItemUN();
//    this->endResetModel();
}

void TreeModelUnitNode::sortingProxyListItemUN()
{
    this->rootItemSortUN->listChilde;
    //сортировка по номерам уч
    for(int i(0), n(this->rootItemSortUN->listChilde.size()); i < n - 1; i++)
    {
        for(int j(0); j < n - i - 1; j++)
        {
            if(Qt::AscendingOrder == sortOrder)
            {
                if(this->rootItemSortUN->listChilde[j]->numArea > this->rootItemSortUN->listChilde[j + 1]->numArea)
                {
                    ProxySortTreeContainerUN *temp = this->rootItemSortUN->listChilde[j]; //change for elements
                    this->rootItemSortUN->listChilde[j] = this->rootItemSortUN->listChilde[j + 1];
                    this->rootItemSortUN->listChilde[j + 1] = temp;
                }
            }
            if(Qt::DescendingOrder == sortOrder)
            {
                if(this->rootItemSortUN->listChilde[j]->numArea < this->rootItemSortUN->listChilde[j + 1]->numArea)
                {
                    ProxySortTreeContainerUN *temp = this->rootItemSortUN->listChilde[j]; //change for elements
                    this->rootItemSortUN->listChilde[j] = this->rootItemSortUN->listChilde[j + 1];
                    this->rootItemSortUN->listChilde[j + 1] = temp;
                }
            }
        }
    }
    //сортировка по номерам устр
    for(int x(0), xx(sortProxyListParentItemTC.size()); x < xx; x++)
    {
        for(int i(0), n(sortProxyListParentItemTC[x]->listChilde.size()); i < n - 1; i++)
        {
            for(int j(0); j < n - i - 1; j++)
            {
                if(Qt::AscendingOrder == sortOrder)
                {
                    if(sortProxyListParentItemTC[x]->listChilde[j]->m_un->_numNode() > sortProxyListParentItemTC[x]->listChilde[j + 1]->m_un->_numNode())
                    {
                        ProxySortTreeContainerUN *temp = sortProxyListParentItemTC[x]->listChilde[j]; //change for elements
                        sortProxyListParentItemTC[x]->listChilde[j] = sortProxyListParentItemTC[x]->listChilde[j + 1];
                        sortProxyListParentItemTC[x]->listChilde[j + 1] = temp;
                    }
                }
                if(Qt::DescendingOrder == sortOrder)
                {
                    if(sortProxyListParentItemTC[x]->listChilde[j]->m_un->_numNode() < sortProxyListParentItemTC[x]->listChilde[j + 1]->m_un->_numNode())
                    {
                        ProxySortTreeContainerUN *temp = sortProxyListParentItemTC[x]->listChilde[j]; //change for elements
                        sortProxyListParentItemTC[x]->listChilde[j] = sortProxyListParentItemTC[x]->listChilde[j + 1];
                        sortProxyListParentItemTC[x]->listChilde[j + 1] = temp;
                    }
                }
            }
        }
    }
}

void TreeModelUnitNode::sortingListItemUN()
{
    //сортировка по номерам уч
    for(int i(0), n(listItemUN.size()); i < n - 1; i++)
    {
        for(int j(0); j < n - i - 1; j++)
        {
            if(Qt::AscendingOrder == sortOrder)
            {
                if(listItemUN[j]->_numArea() > listItemUN[j + 1]->_numArea())
                {
                    UnitNode *temp = listItemUN[j]; //change for elements
                    listItemUN[j] = listItemUN[j + 1];
                    listItemUN[j + 1] = temp;
                }
            }
            if(Qt::DescendingOrder == sortOrder)
            {
                if(listItemUN[j]->_numArea() < listItemUN[j + 1]->_numArea())
                {
                    UnitNode *temp = listItemUN[j]; //change for elements
                    listItemUN[j] = listItemUN[j + 1];
                    listItemUN[j + 1] = temp;
                }
            }
        }
    }
    //сортировка по номерам устр
    for(int i(0), n(listItemUN.size()); i < n - 1; i++)
    {
        for(int j(0); j < n - i - 1; j++)
        {
            if(listItemUN[j]->_numArea() == listItemUN[j + 1]->_numArea())
            {
                if(Qt::AscendingOrder == sortOrder)
                {
                    if(listItemUN[j]->_numNode() > listItemUN[j + 1]->_numNode())
                    {
                        UnitNode *temp = listItemUN[j]; //change for elements
                        listItemUN[j] = listItemUN[j + 1];
                        listItemUN[j + 1] = temp;
                    }
                }
                if(Qt::DescendingOrder == sortOrder)
                {
                    if(listItemUN[j]->_numNode() < listItemUN[j + 1]->_numNode())
                    {
                        UnitNode *temp = listItemUN[j]; //change for elements
                        listItemUN[j] = listItemUN[j + 1];
                        listItemUN[j + 1] = temp;
                    }
                }
            }

        }
    }
}

void TreeModelUnitNode::filterHiddenUN()
{
    for(int i(0); i < listItemUN.size();/* i++*/)
    {
        if(listItemUN.at(i)->_hidden())
        {
            listItemUN.removeAt(i);
        }
        else
        {
            i++;
        }
    }
}
