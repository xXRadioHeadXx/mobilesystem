#ifndef TABLEMODELARCHIVEMSG_H
#define TABLEMODELARCHIVEMSG_H

#include <QAbstractItemModel>
#include <databasemanager.h>
#include <proxymetod.h>

class TableModelArchiveMSG : public QAbstractTableModel
{
    Q_OBJECT
public:
    explicit TableModelArchiveMSG(DataBaseManager *db,
                                  TypeUnitNodeSystem system,
                                  QObject *parent = nullptr);
    virtual ~TableModelArchiveMSG();

    //
    DataBaseManager *dbManager;
    QList<quint32> m_listRecordsMSG;
    quint32 newRecordMSG;
    bool needScroll;
    TypeUnitNodeSystem typeSystem;
    //
    FilterStruct currentFilter;
    //
    void updateListRecordsMSG();
    void updateFilter(FilterStruct newFilter);
    // количество строк. Устанавливаем так, чтобы скроллер отображался корректно
    /*virtual*/ int rowCount(const QModelIndex &index = QModelIndex()) const override;
    // устанавливаем количество столбцов.
    /*virtual*/ int columnCount(const QModelIndex &index = QModelIndex()) const override;
    // функция для передачи данных пользователю
    /*virtual*/ QVariant data(const QModelIndex &index, int role) const override;
    // Функция для приёма данных от пользователя
//    virtual  bool  setData(QModelIndex index, QVariant value, int role);
    // отображение   названий   столбцов
    /*virtual*/ QVariant headerData( int section, Qt::Orientation orientation, int role) const override;
    // возможность редактирования элемента
    /*virtual*/ Qt::ItemFlags flags(QModelIndex index);
    // добавление новой строки
    /*virtual*/ bool insertRows(int row, int count = 1, const QModelIndex &parent = QModelIndex()) override;

signals:
    void needScrollToBottom();

public slots:
    //обновление выборки
    void updateListRecords(quint32 idMSG);
    void setNeedScroll(bool value);
    void emitNeedScrollToBottom();
    void needResetModel();

};

#endif // TABLEMODELARCHIVEMSG_H
