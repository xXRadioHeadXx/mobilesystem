﻿#include <tablemodelunitnode.h>

TableModelUnitNode::TableModelUnitNode(KamuflageManager *km,
                                       KiparisManager *ks,
                                       BSKManager *bsk,
                                       DataBaseManager *db,
                                       TypeUnitNodeSystem typeSystem,
                                       QObject *parent) :
    QAbstractTableModel(parent),
    kmManager(km),
    ksManager(ks),
    bskManager(bsk),
    dbManager(db),
    filterTypeSystem(typeSystem),
    typeSortOrder(Qt::AscendingOrder),
    sortColumn(0)
{
//    rootItem = nullptr;
    this->selectUN = nullptr;
    switch(filterTypeSystem)
    {
        case KMSystemType:
        {
            listItem = kmManager->getListUnitNode();
            break;
        }
        case KSSystemType:
        {
            listItem = ksManager->getListUnitNode();
            break;
        }
        case BSKSystemType:
        {
            listItem = bskManager->getListUnitNode();
            break;
        }
        case UnknownSystemType:
        {
            listItem = kmManager->getListUnitNode();
            listItem.append(ksManager->getListUnitNode());
            listItem.append(bskManager->getListUnitNode());
            break;
        }
        default:
            break;
    }
    filterHiddenUN();
    sortProxyListItem = listItem;
    sortingProxyListItem();
}




TableModelUnitNode::~TableModelUnitNode()
{
    try
    {
        kmManager = nullptr;
        ksManager = nullptr;
        bskManager = nullptr;
        dbManager = nullptr;
    //    rootItem = nullptr;
        sortProxyListItem.clear();
        listItem.clear();
    }
    catch(...)
    {
#ifdef QT_DEBUG
        qDebug() << "Assert(~TableModelUnitNode)";
#endif
        return;
    }
}

// количество строк. Устанавливаем так, чтобы скроллер отображался корректно
int TableModelUnitNode::rowCount(const QModelIndex &index) const
{
    if(index.isValid())
    {
        return sortProxyListItem.size();
    }
    return sortProxyListItem.size();
}

// устанавливаем количество столбцов.
int TableModelUnitNode::columnCount(const QModelIndex &index) const
{
    if(index.isValid())
    {
        return 4;
    }
    return 4;
}

// функция для передачи данных пользователю
QVariant TableModelUnitNode::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

//    if(listItem.size() < index.row() + 1)
    if(sortProxyListItem.size() < index.row() + 1)
        return QVariant();

//    UnitNode *item = listItem.at(index.row());
    UnitNode *item = sortProxyListItem.at(index.row());

    if(role == Qt::BackgroundRole && !item->flag_connection)
        return QColor(197,197,197);//Qt::lightGray;

    if(role == Qt::DecorationRole/* &&
       rootItem != item*/)
    {
        QPixmap pxm;
        switch(index.column())
        {
            case 0:
            {
                pxm = item->getViewPXM();
                break;
            }
            case 2:
            {
                pxm = item->getViewRssiPXM();
                break;
            }
            default:
            {
                break;
            }
        }
        if(pxm.isNull())
            return QVariant();

        return QVariant(pxm);
//        return QVariant(pxm.scaled(20, 20, Qt::KeepAspectRatio, Qt::FastTransformation));
//        return QVariant(pxm.scaled(20, 20, Qt::KeepAspectRatio, Qt::SmoothTransformation));
    }

    if(role == Qt::DisplayRole)
        return item->data(index.column());

    return QVariant();
}

// Функция для приёма данных от пользователя
bool  TableModelUnitNode::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if(!index.isValid() || value.toString().isEmpty())
        return false;

//    if(listItem.size() < index.row() + 1)
//    if(sortProxyListItem.size() < index.row() + 1)
//        return false;

    if(Qt::EditRole == role)
    {
//        UnitNode *item = listItem.at(index.row());
        UnitNode *item(nullptr);
        item = sortProxyListItem.value(index.row(), nullptr);
        if(nullptr != item)
        {
            item->updateAbstractName(value.toByteArray());
            return true;
        }
    }
    return false;
}

// отображение   названий   столбцов
QVariant TableModelUnitNode::headerData( int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal &&
        role == Qt::DisplayRole)
    {
        switch(section)
        {
            case 0:
            {
                return QVariant(QObject::tr("Device"));
            }
            case 1:
            {
                return QVariant(QObject::tr("S/Num"));
            }
            case 2:
            {
                return QVariant(QObject::tr("U, pow"));
            }
            case 3:
            {
                return QVariant(QObject::tr("ID"));
            }
            default:
            {
                return QVariant();
            }
        }
    }

    return QVariant();
}

// возможность редактирования элемента
Qt::ItemFlags TableModelUnitNode::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return 0;

    if(0 == index.column())
    {
//        return Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsEditable | Qt::ItemIsUserCheckable | Qt::ItemIsTristate;
        return Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsEditable;
    }
    else
    {
        return Qt::ItemIsEnabled | Qt::ItemIsSelectable;
    }
}

//сортировка
void TableModelUnitNode::sort(int column, Qt::SortOrder order)
{
    this->beginResetModel();
    sortColumn = column;
    typeSortOrder = order;
    filterHiddenUN();
    sortingProxyListItem();
    this->endResetModel();
    return;
}

void TableModelUnitNode::sortingProxyListItem()
{

    for(int i(0), n(sortProxyListItem.size()); i < n - 1; i++)
    {
        for (int j(0); j < n - i - 1; j++)
        {
            if(Qt::AscendingOrder == typeSortOrder)
            {
                if (sortProxyListItem[j]->_typeSystem() > sortProxyListItem[j + 1]->_typeSystem())
                {
                    UnitNode *temp = sortProxyListItem[j]; //change for elements
                    sortProxyListItem[j] = sortProxyListItem[j + 1];
                    sortProxyListItem[j + 1] = temp;
                }
            }
            if(Qt::DescendingOrder == typeSortOrder)
            {
                if (sortProxyListItem[j]->_typeSystem() < sortProxyListItem[j + 1]->_typeSystem())
                {
                    UnitNode *temp = sortProxyListItem[j]; //change for elements
                    sortProxyListItem[j] = sortProxyListItem[j + 1];
                    sortProxyListItem[j + 1] = temp;
                }
            }
        }
    }

    switch(sortColumn)
    {
        case 0:
        {
            for(int i(0), n(sortProxyListItem.size()); i < n - 1; i++)
            {
                for (int j(0); j < n - i - 1; j++)
                {
                    if (sortProxyListItem[j]->_typeSystem() == sortProxyListItem[j + 1]->_typeSystem())
                    {
                        if(Qt::AscendingOrder == typeSortOrder)
                        {
                            if (sortProxyListItem[j]->_typeUN() > sortProxyListItem[j + 1]->_typeUN())
                            {
                                UnitNode *temp = sortProxyListItem[j]; //change for elements
                                sortProxyListItem[j] = sortProxyListItem[j + 1];
                                sortProxyListItem[j + 1] = temp;
                            }
                        }
                        if(Qt::DescendingOrder == typeSortOrder)
                        {
                            if (sortProxyListItem[j]->_typeUN() < sortProxyListItem[j + 1]->_typeUN())
                            {
                                UnitNode *temp = sortProxyListItem[j]; //change for elements
                                sortProxyListItem[j] = sortProxyListItem[j + 1];
                                sortProxyListItem[j + 1] = temp;
                            }
                        }
                    }
                }
            }
            break;
        }
        case 1:
        {
            //сортировка по номерам уч
            for(int i(0), n(sortProxyListItem.size()); i < n - 1; i++)
            {
                for(int j(0); j < n - i - 1; j++)
                {
                    if (sortProxyListItem[j]->_typeSystem() == sortProxyListItem[j + 1]->_typeSystem())
                    {
                        if(Qt::AscendingOrder == typeSortOrder)
                        {
                            if(sortProxyListItem[j]->_numArea() > sortProxyListItem[j + 1]->_numArea())
                            {
                                UnitNode *temp = sortProxyListItem[j]; //change for elements
                                sortProxyListItem[j] = sortProxyListItem[j + 1];
                                sortProxyListItem[j + 1] = temp;
                            }
                        }
                        if(Qt::DescendingOrder == typeSortOrder)
                        {
                            if(sortProxyListItem[j]->_numArea() < sortProxyListItem[j + 1]->_numArea())
                            {
                                UnitNode *temp = sortProxyListItem[j]; //change for elements
                                sortProxyListItem[j] = sortProxyListItem[j + 1];
                                sortProxyListItem[j + 1] = temp;
                            }
                        }
                    }
                }
            }
            //сортировка по номерам устр
            for(int i(0), n(sortProxyListItem.size()); i < n - 1; i++)
            {
                for(int j(0); j < n - i - 1; j++)
                {
                    if (sortProxyListItem[j]->_typeSystem() == sortProxyListItem[j + 1]->_typeSystem())
                    {
                        if(sortProxyListItem[j]->_numArea() == sortProxyListItem[j + 1]->_numArea())
                        {
                            if(Qt::AscendingOrder == typeSortOrder)
                            {
                                if(sortProxyListItem[j]->_numNode() > sortProxyListItem[j + 1]->_numNode())
                                {
                                    UnitNode *temp = sortProxyListItem[j]; //change for elements
                                    sortProxyListItem[j] = sortProxyListItem[j + 1];
                                    sortProxyListItem[j + 1] = temp;
                                }
                            }
                            if(Qt::DescendingOrder == typeSortOrder)
                            {
                                if(sortProxyListItem[j]->_numNode() < sortProxyListItem[j + 1]->_numNode())
                                {
                                    UnitNode *temp = sortProxyListItem[j]; //change for elements
                                    sortProxyListItem[j] = sortProxyListItem[j + 1];
                                    sortProxyListItem[j + 1] = temp;
                                }
                            }
                        }
                    }

                }
            }
            break;
        }
        case 2:
        {
            for(int i(0), n(sortProxyListItem.size()); i < n - 1; i++)
            {
                for (int j(0); j < n - i - 1; j++)
                {
                    if (sortProxyListItem[j]->_typeSystem() == sortProxyListItem[j + 1]->_typeSystem())
                    {
                        if(Qt::AscendingOrder == typeSortOrder)
                        {
                            if (sortProxyListItem[j]->_voltage() > sortProxyListItem[j + 1]->_voltage())
                            {
                                UnitNode *temp = sortProxyListItem[j]; //change for elements
                                sortProxyListItem[j] = sortProxyListItem[j + 1];
                                sortProxyListItem[j + 1] = temp;
                            }
                        }
                        if(Qt::DescendingOrder == typeSortOrder)
                        {
                            if (sortProxyListItem[j]->_voltage() < sortProxyListItem[j + 1]->_voltage())
                            {
                                UnitNode *temp = sortProxyListItem[j]; //change for elements
                                sortProxyListItem[j] = sortProxyListItem[j + 1];
                                sortProxyListItem[j + 1] = temp;
                            }
                        }
                    }
                }
            }
            break;
        }
        case 3:
        {
            for(int i(0), n(sortProxyListItem.size()); i < n - 1; i++)
            {
                for (int j(0); j < n - i - 1; j++)
                {
                    if (sortProxyListItem[j]->_typeSystem() == sortProxyListItem[j + 1]->_typeSystem())
                    {
                        quint64 val1(sortProxyListItem[j]->_id_toUInt()),
                                val2(sortProxyListItem[j + 1]->_id_toUInt());
                        if(Qt::AscendingOrder == typeSortOrder)
                        {
                            if (val1 > val2)
                            {
                                UnitNode *temp = sortProxyListItem[j]; //change for elements
                                sortProxyListItem[j] = sortProxyListItem[j + 1];
                                sortProxyListItem[j + 1] = temp;
                            }
                        }
                        if(Qt::DescendingOrder == typeSortOrder)
                        {
                            if (val1 < val2)
                            {
                                UnitNode *temp = sortProxyListItem[j]; //change for elements
                                sortProxyListItem[j] = sortProxyListItem[j + 1];
                                sortProxyListItem[j + 1] = temp;
                            }
                        }
                    }

                }
            }
            break;
        }
    }
}

void TableModelUnitNode::updateStructure(UnitNode* un)
{
    if(filterTypeSystem != un->_typeSystem())
        return;
    this->beginResetModel();
    switch(this->filterTypeSystem)
    {
        case KMSystemType:
        {
            listItem = kmManager->getListUnitNode();
            break;
        }
        case KSSystemType:
        {
            listItem = ksManager->getListUnitNode();
            break;
        }
        case BSKSystemType:
        {
            listItem = bskManager->getListUnitNode();
            break;
        }
        case UnknownSystemType:
        {
            listItem = kmManager->getListUnitNode();
            listItem.append(ksManager->getListUnitNode());
            listItem.append(bskManager->getListUnitNode());
            break;
        }
        default:
            break;
    }
    filterHiddenUN();
    sortProxyListItem = listItem;
    sortingProxyListItem();
    this->endResetModel();
    return;
}

void TableModelUnitNode::updateUN(UnitNode* un)
{
    if(filterTypeSystem != un->_typeSystem() &&
       UnknownSystemType != filterTypeSystem)
        return;

    this->beginResetModel();
    switch(this->filterTypeSystem)
    {
        case KMSystemType:
        {
            listItem = kmManager->getListUnitNode();
            break;
        }
        case KSSystemType:
        {
            listItem = ksManager->getListUnitNode();
            break;
        }
        case BSKSystemType:
        {
            listItem = bskManager->getListUnitNode();
            break;
        }
        case UnknownSystemType:
        {
            listItem = kmManager->getListUnitNode();
            listItem.append(ksManager->getListUnitNode());
            listItem.append(bskManager->getListUnitNode());
            break;
        }
        default:
        {
            break;
        }
    }

    filterHiddenUN();
    sortProxyListItem = listItem;
    sortingProxyListItem();
    this->endResetModel();

    if(nullptr != selectUN)
    {
        int rowSelect(sortProxyListItem.indexOf(selectUN));
        if(-1 != rowSelect)
        {
            emit this->needSelectRow(rowSelect);
        }
    }

    return;
}

void TableModelUnitNode::updateListItem()
{
    this->beginResetModel();

    switch(this->filterTypeSystem)
    {
        case KMSystemType:
        {
            listItem = kmManager->getListUnitNode();
            break;
        }
        case KSSystemType:
        {
            listItem = ksManager->getListUnitNode();
            break;
        }
        case BSKSystemType:
        {
            listItem = bskManager->getListUnitNode();
            break;
        }
        case UnknownSystemType:
        {
            listItem = kmManager->getListUnitNode();
            listItem.append(ksManager->getListUnitNode());
            listItem.append(bskManager->getListUnitNode());
            break;
        }
        default:
            break;
    }
    filterHiddenUN();
    sortProxyListItem = listItem;
    sortingProxyListItem();
    this->endResetModel();

    if(nullptr != selectUN)
    {
        int rowSelect(sortProxyListItem.indexOf(selectUN));
        if(-1 != rowSelect)
        {
            emit this->needSelectRow(rowSelect);
        }
    }
}

UnitNode* TableModelUnitNode::clickedUN(const QModelIndex &index)
{
    if (!index.isValid())
        return nullptr;

    UnitNode *item = sortProxyListItem.at(index.row());
    if(item)
    {
        item->graphicsPxmItem->setSelected(true);
        selectUN = item;
        emit selectedUN(item);
        return item;
    }
    return nullptr;
}

void TableModelUnitNode::emitNeedHideRow(int row)
{
    emit this->needHideRow(row);
}

void TableModelUnitNode::filterHiddenUN()
{
    for(int i(0); i < listItem.size();)
    {
//#ifdef QT_DEBUG
//        qDebug() << "DataBaseManager::filterHiddenUN :" << listItem.at(i)->_name() << " " << listItem.at(i)->_hidden();
//#endif
        if(listItem.at(i)->_hidden())
        {
            listItem.removeAt(i);
        }
        else
        {
            i++;
        }
    }
}
