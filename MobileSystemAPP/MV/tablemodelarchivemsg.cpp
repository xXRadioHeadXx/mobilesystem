#include <tablemodelarchivemsg.h>

TableModelArchiveMSG::TableModelArchiveMSG(DataBaseManager *db,
                                           TypeUnitNodeSystem system,
                                           QObject *parent) :
    QAbstractTableModel(parent),
    dbManager(db),
    typeSystem(system)
{
    //
    currentFilter.filterMode =
    currentFilter.filterDate =
    currentFilter.filterTime =
    currentFilter.filterSN =
    currentFilter.filterType =
    currentFilter.filterID =
    currentFilter.filterMSGGroup = false;

    currentFilter.filterMSGAlarm =
    currentFilter.filterMSGConnect =
    currentFilter.filterMSGError =
    currentFilter.filterMSGDischarge =
    currentFilter.filterMSGFF =
    currentFilter.filterMSGCommand =
    currentFilter.filterMSGSystem = true;
    //
    needScroll = true;
    m_listRecordsMSG = dbManager->getFilter_MSGRecords(currentFilter, typeSystem);

    connect(dbManager,
            SIGNAL(insertNewMSG(quint32)),
            this,
            SLOT(updateListRecords(quint32)));

}

void TableModelArchiveMSG::needResetModel()
{
    this->beginResetModel();
    this->endResetModel();
}

TableModelArchiveMSG::~TableModelArchiveMSG()
{
    try
    {
        dbManager = nullptr;
        newRecordMSG = 0;
        needScroll = false;
        m_listRecordsMSG.clear();
    }
    catch(...)
    {
#ifdef QT_DEBUG
        qDebug() << "Assert(~TableModelArchiveMSG)";
#endif
        return;
    }
}

// количество строк. Устанавливаем так, чтобы скроллер отображался корректно
int TableModelArchiveMSG::rowCount(const QModelIndex &index) const
{
    if(index.isValid())
    {
        return m_listRecordsMSG.size();
    }
    return m_listRecordsMSG.size();
}

// устанавливаем количество столбцов.
int TableModelArchiveMSG::columnCount(const QModelIndex &index) const
{
    if(index.isValid())
    {
        return 8;
    }
    return 8;
}

// функция для передачи данных пользователю
QVariant TableModelArchiveMSG::data(const QModelIndex &index, int role) const
{
    int row(index.row());
    QVariant result;
    // достаём запись из Select'a
    QSqlRecord msgRecord(dbManager->get_MSGRecord(m_listRecordsMSG.at(row)));

    if(msgRecord.isEmpty())
    {
        return result;
    }

    // выводим в консоль текущие значения параметров и считаем, сколько
    // раз вызывается метод TableModelArchiveMSG::data и для каких ролей

    // закрасим строчку по io признаку
    if (role == Qt::BackgroundRole)
    {
        QColor resultColor(Qt::white);
        QByteArray resultBA(msgRecord.value("color").toByteArray());
        if(4 == resultBA.size())
        {
            resultColor.setRgb((quint8)resultBA[0], (quint8)resultBA[1], (quint8)resultBA[2]);
//            resultColor.setRed((int)resultBA[0]);
//            resultColor.setGreen((int)resultBA[1]);
//            resultColor.setBlue((int)resultBA[2]);
//            resultColor.setAlpha((int)resultBA[3]);
        }
        else
        {
            if(0 == msgRecord.value("ioType").toUInt())
            {
                return QColor(0xCC,0xCC,0xCC);//QVariant(Qt::lightGray);//QColor(255,255,204);
            }
        }
        result = resultColor;
        return result;
    }
    // Если необходимо отобразить картинку - ловим роль Qt::DecorationRole
    // Если хотим отобразить CheckBox, используем роль Qt::CheckStateRole
    // если текущий вызов не относится к роли отображения, завершаем
    if (!index.isValid() || role != Qt::DisplayRole)
        return result;

    if (index.isValid() && role == Qt::DisplayRole)
    {
        // устанавливаем соответствие между номером столбца и полем записи
        QSqlRecord unRecord(dbManager->getUNRecord(msgRecord.value("foreignIDUN").toUInt()));

        switch(index.column())
        {
            case 0:
            {
//                    quint32 iii(msgRecord.value("id").toUInt());
                return msgRecord.value("id").toUInt();
            };

            case 1:
            {
                return msgRecord.value("idUN").toByteArray().toHex().toUpper();
            };

            case 2:
            {
                return QVariant(ProxyMetod().getNameDevice(msgRecord.value("idUN").toByteArray(),
                                                           (TypeUnitNodeSystem)msgRecord.value("typeSystemMSG").toUInt()));
            };

            case 3:
            {
                if(msgRecord.value("idUN").toByteArray().isEmpty() &&
                   0x00 == msgRecord.value("numberAreaUN").toUInt() &&
                   0x00 == msgRecord.value("numberNodeUN").toUInt())
                {
                    return QVariant();
                }
                QString strAreaNum("%1/%2");
                strAreaNum = strAreaNum.arg(msgRecord.value("numberAreaUN").toUInt())
                                       .arg(msgRecord.value("numberNodeUN").toUInt());
                return QVariant(strAreaNum);
            };

            case 4:
            {
                if("ru" == dbManager->typeLanguage())
                {
                    return msgRecord.value("commentRUS").toString();
                }
                if("en" == dbManager->typeLanguage())
                {
                    return msgRecord.value("commentENG").toString();
                }
            };

            case 5:
            {
                return msgRecord.value("date").toDate().toString("dd.MM.yy");
            };

            case 6:
            {
                return msgRecord.value("time").toTime().toString("hh:mm:ss");
            };

            case 7:
            {
                return msgRecord.value("timeCorection").toTime().toString("hh:mm:ss");
            };
        };
    }

    return result;
}

// Функция для приёма данных от пользователя
//bool TableModelArchiveMSG::setData(QModelIndex index, QVariant value, int role)
//{
//    if(index.isValid() && value.isValid() && (-1 != role))
//        return false;
//    return false;
//}

// отображение   названий   столбцов
QVariant TableModelArchiveMSG::headerData(int section, Qt::Orientation orientation, int role) const
{
    // Для любой роли, кроме запроса на отображение, прекращаем обработку
    if (role != Qt::DisplayRole)
        return QVariant();

    // формируем заголовки по номуру столбца
    if (orientation == Qt::Horizontal)
    {
        switch(section)
        {
            case 0:
                return "№/#";
            case 1:
                return "ID";
            case 2:
                return tr("Device");
            case 3:
                return tr("S/Num");
            case 4:
                return tr("Message");
            case 5:
                return tr("Date");
            case 6:
                return tr("Time");
            case 7:
                return tr("Time Corection");
        }
    }

    return QVariant();
};

// возможность редактирования элемента
Qt::ItemFlags TableModelArchiveMSG::flags(QModelIndex index)
{
    Qt::ItemFlags result;
    if (!index.isValid())
        return Qt::ItemIsEnabled;

    // разрешаем редактирование всего, кроме первого столбца
    result = QAbstractTableModel::flags(index);
    result |= Qt::ItemIsEditable;
    return result;
}

//добавление строки
bool TableModelArchiveMSG::insertRows(int row, int count, const QModelIndex &parent)
{
    if(1 == count)
    {
        this->beginInsertRows(parent, row, row);

        m_listRecordsMSG.insert(row, newRecordMSG);

        this->endInsertRows();
        return true;
    }
    return false;
}


//обновление выборки
void TableModelArchiveMSG::updateListRecords(quint32 idMSG)
{
    QSqlRecord newRecord(dbManager->get_MSGRecord(idMSG));
    if(typeSystem != newRecord.value("typeSystemMSG").toInt())
    {
        return;
    }


//#ifdef QT_DEBUG
//#else
//    if(0 != newRecord.value("hidden").toInt())//сделать только на релиз
//    {
//        return;
//    }

//    if(KMSystemType == typeSystem)
//    {
//        if(UnknownDeviceType == (TypeUnitNodeDevice)newRecord.value("typeUN").toUInt())
//        {
//            if((quint8)0xFF == (quint8)newRecord.value("typeMSG").toUInt() &&
//               (quint8)0xFF == (quint8)newRecord.value("codeMSG").toUInt())
//            {
//                if(newRecord.value("commentRUS").toString().contains("SET"))
//                {
//                    return;
//                }
//            }
//        }
//        else
//        {
//            if((quint8)0xFF == (quint8)newRecord.value("codeMSG").toUInt() &&
//               (
//                  (quint8)0xFF == (quint8)newRecord.value("typeMSG").toUInt() ||
//                  (quint8)0x00 == (quint8)newRecord.value("typeMSG").toUInt()
//               )
//              )
//            {
//                return;
//            }
//        }
////        if(UnknownDeviceType != (TypeUnitNodeDevice)newRecord.value("typeUN").toUInt() &&
////           (
////              (quint8)0xFF == (quint8)newRecord.value("typeMSG").toUInt() ||
////              (quint8)0x00 == (quint8)newRecord.value("typeMSG").toUInt()
////           ) &&
////           (quint8)0xFF == (quint8)newRecord.value("codeMSG").toUInt())
////        {
////            return;
////        }
////        if(UnknownDeviceType == (TypeUnitNodeDevice)newRecord.value("typeUN").toUInt() &&
////           (quint8)0xFF == (quint8)newRecord.value("typeMSG").toUInt() &&
////           (quint8)0xFF == (quint8)newRecord.value("codeMSG").toUInt() &&
////           newRecord.value("commentRUS").toString().contains("SET"))
////        {
////            return;
////        }

//    }
//#endif
    newRecordMSG = idMSG;



    if(0 != newRecordMSG &&
       !m_listRecordsMSG.contains(newRecordMSG))
    {
        if(currentFilter.filterMode)
        {

            if(currentFilter.filterDate)
            {
                if(currentFilter.filterMinDate > newRecord.value("date").toDate() ||
                   newRecord.value("date").toDate() > currentFilter.filterMaxDate)
                {
                    return;
                }

            }

            if(currentFilter.filterTime)
            {
                if(currentFilter.filterMinTime > newRecord.value("time").toTime() ||
                   newRecord.value("time").toTime() > currentFilter.filterMaxTime)
                {
                    return;
                }

            }

            if(currentFilter.filterSN)
            {
                if(-1 != currentFilter.filterSector &&
                   newRecord.value("area").toInt() != currentFilter.filterSector)
                {
                    return;
                }

                if(-1 != currentFilter.filterNumber &&
                   newRecord.value("number").toInt() != currentFilter.filterNumber)
                {
                    return;
                }
            }

            if(currentFilter.filterType)
            {
                if((TypeUnitNodeDevice)newRecord.value("typeUN").toUInt() != currentFilter.filterTypeValue)
                {
                    return;
                }
            }

            if(currentFilter.filterID)
            {
                if(newRecord.value("idUN").toByteArray() != currentFilter.filterIDValue)
                {
                    return;
                }
            }

            if(currentFilter.filterMSGGroup)
            {
                if(false == currentFilter.filterMSGAlarm &&
                   (quint8)Alarm_TypeGroupMSG == (quint8)newRecord.value("groupMSG").toUInt())
                {
                    return;
                }
                if(false == currentFilter.filterMSGCommand &&
                   (quint8)Command_TypeGroupMSG == (quint8)newRecord.value("groupMSG").toUInt())
                {
                    return;
                }
                if(false == currentFilter.filterMSGConnect &&
                   (quint8)Connect_TypeGroupMSG == (quint8)newRecord.value("groupMSG").toUInt())
                {
                    return;
                }
                if(false == currentFilter.filterMSGDischarge &&
                   (quint8)Discharge_TypeGroupMSG == (quint8)newRecord.value("groupMSG").toUInt())
                {
                    return;
                }
                if(false == currentFilter.filterMSGError &&
                   (quint8)Error_TypeGroupMSG == (quint8)newRecord.value("groupMSG").toUInt())
                {
                    return;
                }
                if(false == currentFilter.filterMSGFF &&
                   (quint8)FF_TypeGroupMSG == (quint8)newRecord.value("groupMSG").toUInt())
                {
                    return;
                }
                if(false == currentFilter.filterMSGSystem &&
                   (quint8)System_TypeGroupMSG == (quint8)newRecord.value("groupMSG").toUInt())
                {
                    return;
                }
            }

            this->insertRows(this->rowCount());
        }
        else
        {
            this->insertRows(this->rowCount());
        }
        emitNeedScrollToBottom();
    }
}

void TableModelArchiveMSG::setNeedScroll(bool value)
{
    needScroll = value;
}

void TableModelArchiveMSG::emitNeedScrollToBottom()
{
    if(needScroll)
    {
        emit this->needScrollToBottom();
    }
}

void TableModelArchiveMSG::updateListRecordsMSG()
{
    this->beginResetModel();
    m_listRecordsMSG = dbManager->getFilter_MSGRecords(currentFilter, typeSystem);
    this->endResetModel();
}

void TableModelArchiveMSG::updateFilter(FilterStruct newFilter)
{
    currentFilter = newFilter;
    updateListRecordsMSG();
}
