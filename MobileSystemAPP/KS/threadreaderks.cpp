#include <threadreaderks.h>

ThreadReaderKS::ThreadReaderKS(QSerialPort *port,
                               QByteArray *collector,
                               QByteArray data,
                               COSVariantList *listPattern,
                               COSVariantList *listRequest,
                               QList<QByteArray> *listData_0x2D,
                               bool *pass_0x2D,
                               QMutex *mutexPattern,
                               QMutex *mutexRequest,
                               QObject *parent) /*noexcept*/ :
    QThread(parent),
    m_port(port),
    m_ByteCollector(collector),
    newData(data),
    m_listPattern(listPattern),
    m_listRequest(listRequest),
    listDataRequest_0x2D(listData_0x2D),
    passClearListDataRequest_0x2D(pass_0x2D),
    m_mutexPattern(mutexPattern),
    m_mutexRequest(mutexRequest)
{
//    CrcTable.append(0x00);
//    CrcTable.append(0x31);
//    CrcTable.append(0x62);
//    CrcTable.append(0x53);
//    CrcTable.append(0xC4);
//    CrcTable.append(0xF5);
//    CrcTable.append(0xA6);
//    CrcTable.append(0x97);

//    CrcTable.append(0xB9);
//    CrcTable.append(0x88);
//    CrcTable.append(0xDB);
//    CrcTable.append(0xEA);
//    CrcTable.append(0x7D);
//    CrcTable.append(0x4C);
//    CrcTable.append(0x1F);
//    CrcTable.append(0x2E);

//    CrcTable.append(0x43);
//    CrcTable.append(0x72);
//    CrcTable.append(0x21);
//    CrcTable.append(0x10);
//    CrcTable.append(0x87);
//    CrcTable.append(0xB6);
//    CrcTable.append(0xE5);
//    CrcTable.append(0xD4);

//    CrcTable.append(0xFA);
//    CrcTable.append(0xCB);
//    CrcTable.append(0x98);
//    CrcTable.append(0xA9);
//    CrcTable.append(0x3E);
//    CrcTable.append(0x0F);
//    CrcTable.append(0x5C);
//    CrcTable.append(0x6D);

//    CrcTable.append(0x86);
//    CrcTable.append(0xB7);
//    CrcTable.append(0xE4);
//    CrcTable.append(0xD5);
//    CrcTable.append(0x42);
//    CrcTable.append(0x73);
//    CrcTable.append(0x20);
//    CrcTable.append(0x11);

//    CrcTable.append(0x3F);
//    CrcTable.append(0x0E);
//    CrcTable.append(0x5D);
//    CrcTable.append(0x6C);
//    CrcTable.append(0xFB);
//    CrcTable.append(0xCA);
//    CrcTable.append(0x99);
//    CrcTable.append(0xA8);

//    CrcTable.append(0xC5);
//    CrcTable.append(0xF4);
//    CrcTable.append(0xA7);
//    CrcTable.append(0x96);
//    CrcTable.append(0x01);
//    CrcTable.append(0x30);
//    CrcTable.append(0x63);
//    CrcTable.append(0x52);

//    CrcTable.append(0x7C);
//    CrcTable.append(0x4D);
//    CrcTable.append(0x1E);
//    CrcTable.append(0x2F);
//    CrcTable.append(0xB8);
//    CrcTable.append(0x89);
//    CrcTable.append(0xDA);
//    CrcTable.append(0xEB);

//    CrcTable.append(0x3D);
//    CrcTable.append(0x0C);
//    CrcTable.append(0x5F);
//    CrcTable.append(0x6E);
//    CrcTable.append(0xF9);
//    CrcTable.append(0xC8);
//    CrcTable.append(0x9B);
//    CrcTable.append(0xAA);

//    CrcTable.append(0x84);
//    CrcTable.append(0xB5);
//    CrcTable.append(0xE6);
//    CrcTable.append(0xD7);
//    CrcTable.append(0x40);
//    CrcTable.append(0x71);
//    CrcTable.append(0x22);
//    CrcTable.append(0x13);

//    CrcTable.append(0x7E);
//    CrcTable.append(0x4F);
//    CrcTable.append(0x1C);
//    CrcTable.append(0x2D);
//    CrcTable.append(0xBA);
//    CrcTable.append(0x8B);
//    CrcTable.append(0xD8);
//    CrcTable.append(0xE9);

//    CrcTable.append(0xC7);
//    CrcTable.append(0xF6);
//    CrcTable.append(0xA5);
//    CrcTable.append(0x94);
//    CrcTable.append(0x03);
//    CrcTable.append(0x32);
//    CrcTable.append(0x61);
//    CrcTable.append(0x50);

//    CrcTable.append(0xBB);
//    CrcTable.append(0x8A);
//    CrcTable.append(0xD9);
//    CrcTable.append(0xE8);
//    CrcTable.append(0x7F);
//    CrcTable.append(0x4E);
//    CrcTable.append(0x1D);
//    CrcTable.append(0x2C);

//    CrcTable.append(0x02);
//    CrcTable.append(0x33);
//    CrcTable.append(0x60);
//    CrcTable.append(0x51);
//    CrcTable.append(0xC6);
//    CrcTable.append(0xF7);
//    CrcTable.append(0xA4);
//    CrcTable.append(0x95);

//    CrcTable.append(0xF8);
//    CrcTable.append(0xC9);
//    CrcTable.append(0x9A);
//    CrcTable.append(0xAB);
//    CrcTable.append(0x3C);
//    CrcTable.append(0x0D);
//    CrcTable.append(0x5E);
//    CrcTable.append(0x6F);

//    CrcTable.append(0x41);
//    CrcTable.append(0x70);
//    CrcTable.append(0x23);
//    CrcTable.append(0x12);
//    CrcTable.append(0x85);
//    CrcTable.append(0xB4);
//    CrcTable.append(0xE7);
//    CrcTable.append(0xD6);

//    CrcTable.append(0x7A);
//    CrcTable.append(0x4B);
//    CrcTable.append(0x18);
//    CrcTable.append(0x29);
//    CrcTable.append(0xBE);
//    CrcTable.append(0x8F);
//    CrcTable.append(0xDC);
//    CrcTable.append(0xED);

//    CrcTable.append(0xC3);
//    CrcTable.append(0xF2);
//    CrcTable.append(0xA1);
//    CrcTable.append(0x90);
//    CrcTable.append(0x07);
//    CrcTable.append(0x36);
//    CrcTable.append(0x65);
//    CrcTable.append(0x54);

//    CrcTable.append(0x39);
//    CrcTable.append(0x08);
//    CrcTable.append(0x5B);
//    CrcTable.append(0x6A);
//    CrcTable.append(0xFD);
//    CrcTable.append(0xCC);
//    CrcTable.append(0x9F);
//    CrcTable.append(0xAE);

//    CrcTable.append(0x80);
//    CrcTable.append(0xB1);
//    CrcTable.append(0xE2);
//    CrcTable.append(0xD3);
//    CrcTable.append(0x44);
//    CrcTable.append(0x75);
//    CrcTable.append(0x26);
//    CrcTable.append(0x17);

//    CrcTable.append(0xFC);
//    CrcTable.append(0xCD);
//    CrcTable.append(0x9E);
//    CrcTable.append(0xAF);
//    CrcTable.append(0x38);
//    CrcTable.append(0x09);
//    CrcTable.append(0x5A);
//    CrcTable.append(0x6B);

//    CrcTable.append(0x45);
//    CrcTable.append(0x74);
//    CrcTable.append(0x27);
//    CrcTable.append(0x16);
//    CrcTable.append(0x81);
//    CrcTable.append(0xB0);
//    CrcTable.append(0xE3);
//    CrcTable.append(0xD2);

//    CrcTable.append(0xBF);
//    CrcTable.append(0x8E);
//    CrcTable.append(0xDD);
//    CrcTable.append(0xEC);
//    CrcTable.append(0x7B);
//    CrcTable.append(0x4A);
//    CrcTable.append(0x19);
//    CrcTable.append(0x28);

//    CrcTable.append(0x06);
//    CrcTable.append(0x37);
//    CrcTable.append(0x64);
//    CrcTable.append(0x55);
//    CrcTable.append(0xC2);
//    CrcTable.append(0xF3);
//    CrcTable.append(0xA0);
//    CrcTable.append(0x91);

//    CrcTable.append(0x47);
//    CrcTable.append(0x76);
//    CrcTable.append(0x25);
//    CrcTable.append(0x14);
//    CrcTable.append(0x83);
//    CrcTable.append(0xB2);
//    CrcTable.append(0xE1);
//    CrcTable.append(0xD0);

//    CrcTable.append(0xFE);
//    CrcTable.append(0xCF);
//    CrcTable.append(0x9C);
//    CrcTable.append(0xAD);
//    CrcTable.append(0x3A);
//    CrcTable.append(0x0B);
//    CrcTable.append(0x58);
//    CrcTable.append(0x69);

//    CrcTable.append(0x04);
//    CrcTable.append(0x35);
//    CrcTable.append(0x66);
//    CrcTable.append(0x57);
//    CrcTable.append(0xC0);
//    CrcTable.append(0xF1);
//    CrcTable.append(0xA2);
//    CrcTable.append(0x93);

//    CrcTable.append(0xBD);
//    CrcTable.append(0x8C);
//    CrcTable.append(0xDF);
//    CrcTable.append(0xEE);
//    CrcTable.append(0x79);
//    CrcTable.append(0x48);
//    CrcTable.append(0x1B);
//    CrcTable.append(0x2A);

//    CrcTable.append(0xC1);
//    CrcTable.append(0xF0);
//    CrcTable.append(0xA3);
//    CrcTable.append(0x92);
//    CrcTable.append(0x05);
//    CrcTable.append(0x34);
//    CrcTable.append(0x67);
//    CrcTable.append(0x56);

//    CrcTable.append(0x78);
//    CrcTable.append(0x49);
//    CrcTable.append(0x1A);
//    CrcTable.append(0x2B);
//    CrcTable.append(0xBC);
//    CrcTable.append(0x8D);
//    CrcTable.append(0xDE);
//    CrcTable.append(0xEF);

//    CrcTable.append(0x82);
//    CrcTable.append(0xB3);
//    CrcTable.append(0xE0);
//    CrcTable.append(0xD1);
//    CrcTable.append(0x46);
//    CrcTable.append(0x77);
//    CrcTable.append(0x24);
//    CrcTable.append(0x15);

//    CrcTable.append(0x3B);
//    CrcTable.append(0x0A);
//    CrcTable.append(0x59);
//    CrcTable.append(0x68);
//    CrcTable.append(0xFF);
//    CrcTable.append(0xCE);
//    CrcTable.append(0x9D);
//    CrcTable.append(0xAC);
}

ThreadReaderKS::~ThreadReaderKS() /*noexcept*/
{
    try
    {
        ksManager = nullptr;
        m_ByteCollector = nullptr;

        newData.clear();
        m_listPattern = nullptr;
        m_listRequest = nullptr;
        m_mutexPattern = nullptr;
        m_mutexRequest = nullptr;
        listDataRequest_0x2D = nullptr;
        passClearListDataRequest_0x2D = nullptr;
    }
    catch(...)
    {
#ifdef QT_DEBUG
        qDebug() << "Assert(~ThreadReaderKS)";
#endif
        return;
    }

}

void ThreadReaderKS::run() /*noexcept*/
{
    try
    {
        this->m_ByteCollector->append(newData);
        this->analizMainBuffer();

    //    qWarning("ThreadReaderKS::run <--");

        this->quit();
    }
    catch (std::runtime_error &rte)
    {
        qWarning("catch runtime_error %s [ThreadReaderKS::run]", rte.what());
    }
    catch (std::bad_alloc& ba)
    {
        qWarning("catch bad_alloc %s [ThreadReaderKS::run]", ba.what());
    }
    catch (std::exception &e)
    {
        qWarning("catch exception %s [ThreadReaderKS::run]", e.what());
    }
    catch (...)
    {
        qWarning("catch exception <unknown> [ThreadReaderKS::run]");
    }
}

void ThreadReaderKS::analizMainBuffer() /*noexcept*/
{
    while(true)
    {
//      удаляем всё перед A5 -->
        int indexA5(0);
        QByteArray trashSegment;
        indexA5 = this->m_ByteCollector->indexOf(0xA5, 0);
        if(0 < indexA5)
        {
//            qDebug() << "!!! Remove Trash !!!";
            trashSegment = this->m_ByteCollector->left(indexA5);
            this->m_ByteCollector->remove(0, indexA5);
//            qDebug() << "TrashsSEGMENT (" << trashSegment.toHex() << ")";
        }
        if(-1 == indexA5)
        {
            this->m_ByteCollector->clear();
        }
//      удаляем всё перед A5 <--

//      проверяем по размеру -->
        quint32 size(this->m_ByteCollector->size());
        if(7 > size)//нужны ещё байты. а пока выходим.
        {
//            qDebug() << "return(7 > m_ByteCollector.size(" << this->m_ByteCollector->size() << ")) --x  m_ByteCollector(" << this->m_ByteCollector->left(5).toHex() << "... ["  << this->m_ByteCollector->size() << "] ..." << this->m_ByteCollector->right(5).toHex() << ")";
//            qDebug() << "analizMainBuffer() <--";
            return;
        }

        quint32 dataSize((quint8)this->m_ByteCollector->at(4) + 0x100 * ((quint8)this->m_ByteCollector->at(3) & 0x0F)),//размер поля данных
                count1B(this->m_ByteCollector->count((quint8)0x1B));
        dataSize = dataSize & 0x0000FFFF;

        if((7 + dataSize) > (size - count1B))
        {//нужны ещё байты. а пока выходим.
            if(1 < this->m_ByteCollector->count((quint8)0xA5))
            {
                this->m_ByteCollector->remove(0,
                                              this->m_ByteCollector->indexOf((quint8)0xA5,
                                                                             1));
//                                qDebug() << "Rmove segment from this->listBuffer->[" << ci << "].data";
                continue;
            }
            return;
        }
//      проверяем по размеру <--

        QByteArray pattern;
        if(1 < this->m_ByteCollector->count((quint8)0xA5))
        {
            qint32 indexSecondA5(this->m_ByteCollector->indexOf((quint8)0xA5, 1));
            pattern = this->m_ByteCollector->left(indexSecondA5);
            this->m_ByteCollector->remove(0, indexSecondA5);
//                                qDebug() << "Rmove segment from this->listBuffer->[" << ci << "].data";
        }
        else
        {
            pattern = this->m_ByteCollector->left(this->m_ByteCollector->size());
            this->m_ByteCollector->clear();
        }

//      ребайтстаффинг -->
        for(qint32 i(1); i < pattern.size(); i++)
        {
            if(0x1B ==(quint8)pattern.at(i))
            {
                pattern.remove(i, 1);
                if(i < pattern.size())
                {
                    pattern[i] = pattern.at(i) - 0x20;
                }
            }
        }
//      ребайтстаффинг <--

        pattern = pattern.left(7 + dataSize);

//      проверка crc -->
//        quint8 crc8(0x00);
        quint8 crc8(0xFF);
        for(qint32 i(1), n(pattern.size() - 1); i < n; i++)
        {
            crc8 -= (quint8)pattern.at(i);
//            crc8 = CrcTable.at((crc8 ^ (quint8)pattern.at(i))); // Табличный метод
        }
//      проверка crc <--

        if(crc8 == (quint8)pattern.right(1).at(0))
        {//отправить на обработку!!!-->
            possRequestReciept(pattern);

            bool mx_stat(false);
            while(false == mx_stat)
            {
                mx_stat = m_mutexPattern->tryLock(100);
            }

            this->m_listPattern->appendUnique(QVariant(pattern));

            if(true == mx_stat)
            {
                m_mutexPattern->unlock();
            }
        }//отправить на обработку!!!<--
        else
        {
#ifdef QT_DEBUG
            qDebug() << "CRC FALSE !!! (" << pattern.left(15).toHex() << "... ["  << pattern.size() << "] ..." << pattern.right(5).toHex() << ")  right[" << crc8 << "]";
#endif
        }

        continue;
    }
}

QByteArray ThreadReaderKS::setCRC(QByteArray msg) /*noexcept*/
{
    msg[msg.size() - 1] = 0xFF;
    for(int i(1), n(msg.size() - 1); i < n; i++)
    {
        msg[n] = (quint8)msg.at(n) - (quint8)msg.at(i);
    }
    return msg;
}

QByteArray ThreadReaderKS::setByteStaffing(QByteArray msg) /*noexcept*/
{
    for(int i(1); i < msg.size(); i++)
    {
        if((quint8)0xA5 == (quint8)msg.at(i) ||
           (quint8)0x1B == (quint8)msg.at(i))
        {
            msg[i] = (quint8)msg.at(i) + (quint8)0x20;
            msg.insert(i, (quint8)0x1B);
            i++;
        }
    }
    return msg;
}

QByteArray ThreadReaderKS::setPreambula(QByteArray msg) /*noexcept*/
{
    msg.prepend((quint8)0xFF);
    msg.prepend((quint8)0xFF);
    msg.prepend((quint8)0xFF);
    msg.prepend((quint8)0xFF);
    msg.prepend((quint8)0xFF);
    msg.prepend((quint8)0xFF);
    msg.prepend((quint8)0xFF);
    msg.prepend((quint8)0xFF);
    msg.prepend((quint8)0xFF);
    msg.prepend((quint8)0xFF);
    msg.prepend((quint8)0xFF);
    msg.prepend((quint8)0xFF);
    msg.prepend((quint8)0xFF);
    msg.prepend((quint8)0xFF);
    msg.prepend((quint8)0xFF);

    msg.append((quint8)0x22);
    msg.append((char)0x00);
    return msg;
}


void ThreadReaderKS::possRequestReciept(QByteArray pattern) /*noexcept*/
{
    switch((quint8)pattern.at(5))
    {
        case (quint8)0xA1:
        {
            request_0xA1();
            break;
        }
        case (quint8)0x9F:
        {
            request_0x0D(pattern.at(6),
                         pattern.mid(7, 2),
                         pattern.at(9),
                         pattern.at(10),
                         pattern.at(11),
                         pattern.at(12),
                         pattern.at(13),
                         pattern.at(14),
                         pattern.at(15),
                         pattern.at(16),
                         pattern.at(17));
            break;
        }
        case (quint8)0x14:
        {
            request_0x1D(pattern.mid(6, 2),
                         pattern.at(8),
                         pattern.at(9),
                         pattern.at(10),
                         pattern.at(11));
            break;
        }
        case (quint8)0x24:
        {
            if(*passClearListDataRequest_0x2D)
            {
                listDataRequest_0x2D->clear();
                *passClearListDataRequest_0x2D = false;
            }
            QByteArray pice(pattern.mid(6, 6));

            if(false == listDataRequest_0x2D->contains(pice) &&
               6 == pice.size())
            {
                listDataRequest_0x2D->append(pice);
            }
            break;
        }
        case (quint8)0x2D:
        {
            request_0x2D();
            break;
        }
        default:
        {
            break;
        }
    }
    return;
}

void ThreadReaderKS::request_0xA1() /*noexcept*/
{
    QByteArray msgA1;
    msgA1.append((quint8)0xA5);
    msgA1.append((quint8)0x0A);
    msgA1.append((quint8)0x08);
    msgA1.append((char)0x00);
    msgA1.append((quint8)0x06);
    msgA1.append((quint8)0xA1);

    msgA1.append((quint8)QDate::currentDate().day());
    msgA1.append((quint8)QDate::currentDate().month());
    msgA1.append((quint8)(QDate::currentDate().year() - 2000));
    msgA1.append((quint8)QTime::currentTime().hour());
    msgA1.append((quint8)QTime::currentTime().minute());
    msgA1.append((quint8)QTime::currentTime().second());

    msgA1.append((quint8)0xFF);

    msgA1 = setCRC(msgA1);
//#ifdef QT_DEBUG
//    qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " KsMSG  OUTPUT >> [command    A1] [" << msgA1.toHex() << "]";
//#endif
    msgA1 = setByteStaffing(msgA1);

    msgA1 = setPreambula(msgA1);

    bool mx_stat(false);
    while(false == mx_stat)
    {
        mx_stat = m_mutexRequest->tryLock(100);
    }

    m_listRequest->append(msgA1);

    if(true == mx_stat)
    {
        m_mutexRequest->unlock();
    }
}


void ThreadReaderKS::request_0x0D(quint8 code,
                                  QByteArray idUN,
                                  quint8 numZone,
                                  quint8 numArea,
                                  quint8 numNode,
                                  quint8 d1,
                                  quint8 d2,
                                  quint8 d3,
                                  quint8 d4,
                                  quint8 d5,
                                  quint8 d6) /*noexcept*/
{
    QByteArray msg0D;
    msg0D.append((quint8)0xA5);
    msg0D.append((quint8)0x0A);
    msg0D.append((quint8)0x08);
    msg0D.append((char)0x00);
    msg0D.append((quint8)0x0C);
    msg0D.append((quint8)0x0D);
    //
    msg0D.append(code);
    msg0D.append(idUN);
    msg0D.append(numZone);
    msg0D.append(numArea);
    msg0D.append(numNode);
    msg0D.append(d1);
    msg0D.append(d2);
    msg0D.append(d3);
    msg0D.append(d4);
    msg0D.append(d5);
    msg0D.append(d6);
    //
    msg0D.append((quint8)0xFF);

    msg0D = setCRC(msg0D);
#ifdef QT_DEBUG
    qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " KsMSG  OUTPUT >> [command    0D] [" << msg0D.toHex() << "]";
#endif

    msg0D = setByteStaffing(msg0D);

    msg0D = setPreambula(msg0D);

    bool mx_stat(false);
    while(false == mx_stat)
    {
        mx_stat = m_mutexRequest->tryLock(100);
    }

    m_listRequest->append(msg0D);

    if(true == mx_stat)
    {
        m_mutexRequest->unlock();
    }
}

void ThreadReaderKS::request_0x1D(QByteArray idUN,
                                  quint8 numShot,
                                  quint8 numVK,
                                  quint8 numBlockH,
                                  quint8 numBlockL) /*noexcept*/
{
    QByteArray msg1D;
    msg1D.append((quint8)0xA5);
    msg1D.append((quint8)0x0A);
    msg1D.append((quint8)0x08);
    msg1D.append((char)0x00);
    msg1D.append((quint8)0x06);
    msg1D.append((quint8)0x1D);
    msg1D.append(idUN);
    msg1D.append(numShot);
    msg1D.append(numVK);
    msg1D.append(numBlockH);
    msg1D.append(numBlockL);
    msg1D.append((quint8)0xFF);

    msg1D = setCRC(msg1D);
#ifdef QT_DEBUG
    qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " KsMSG  OUTPUT >> [command    1D] [" << msg1D.toHex() << "]";
#endif
    msg1D = setByteStaffing(msg1D);

    msg1D = setPreambula(msg1D);

    bool mx_stat(false);
    while(false == mx_stat)
    {
        mx_stat = m_mutexRequest->tryLock(100);
    }

    m_listRequest->append(msg1D);

    if(true == mx_stat)
    {
        m_mutexRequest->unlock();
    }
}

void ThreadReaderKS::request_0x2D() /*noexcept*/
{
    *passClearListDataRequest_0x2D = true;
    QByteArray msg2D;
    msg2D.append((quint8)0xA5);
    msg2D.append((quint8)0x0A);
    msg2D.append((quint8)0x08);
    msg2D.append((quint8)(((quint16)listDataRequest_0x2D->size() * 6) / 0x0100));
    msg2D.append((quint8)(((quint16)listDataRequest_0x2D->size() * 6) & 0x00FF));
    msg2D.append((quint8)0x2D);

    for(int i(0), n(listDataRequest_0x2D->size()); i < n; i++)
    {
        QByteArray pice(listDataRequest_0x2D->at(i));
//#ifdef QT_DEBUG
//        if(6 != pice.size())
//        {
//            qDebug() << "wtf";
//        }
//#endif
        msg2D.append(pice);
    }
    msg2D.append((quint8)0xFF);

    msg2D = setCRC(msg2D);
#ifdef QT_DEBUG
    qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " KsMSG  OUTPUT >> [command    2D] [" << msg2D.toHex() << "]";
#endif
    msg2D = setByteStaffing(msg2D);

    msg2D = setPreambula(msg2D);

    bool mx_stat(false);
    while(false == mx_stat)
    {
        mx_stat = m_mutexRequest->tryLock(100);
    }

    m_listRequest->append(msg2D);

    if(true == mx_stat)
    {
        m_mutexRequest->unlock();
    }
}
