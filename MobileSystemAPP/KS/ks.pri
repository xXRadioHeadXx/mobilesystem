QT += core serialport
CONFIG += c++11

INCLUDEPATH += $$PWD/

HEADERS += \
    $$PWD/serialportmanagerks.h \
    $$PWD/kiparismanager.h \
    $$PWD/ksmsgmanager.h \
    $$PWD/threadanalysisks.h \
    $$PWD/threadreaderks.h 

SOURCES += \
    $$PWD/serialportmanagerks.cpp \
    $$PWD/kiparismanager.cpp \
    $$PWD/ksmsgmanager.cpp \
    $$PWD/threadanalysisks.cpp \
    $$PWD/threadreaderks.cpp 
