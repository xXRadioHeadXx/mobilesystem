#ifndef KSMSGMANAGER_H
#define KSMSGMANAGER_H

#include <QObject>
#include <QTime>
//#include <QStack>
#include <QStringList>

#ifdef QT_DEBUG
#include <QDebug>
#include <QFile>
#endif

#include <QPixmap>
#include <QMutex>
#include <global_value_and_structure.h>
#include <databasemanager.h>
#include <soundalarm.h>

class KSMsgManager : public QObject
{
    Q_OBJECT
public:

    explicit KSMsgManager(DataBaseManager *db,
                          QObject *parent = nullptr) /*noexcept*/;
    virtual ~KSMsgManager();

    int iterationCheckA1;
    DataBaseManager *dbManager;
    UnitNode *m_treeRootUN,
             *m_lastAppendBase;
    SoundAlarm *beeper;
    bool beeperFlag;

//    QMutex *m_resursLock;
    bool    m_resurceStatus;
    quint8 m_numMSG,
           m_numLastMSG,
           m_codeLastMSG;
//    QList<quint8> CrcTable;
    QList<UnitNode* > listKSUnitNode;
    QByteArray lastMSGToSend;
    UnitNode* lastUNToSend;
//    QList<QByteArray> listDataRequest_0x2D;
//    bool passClearListDataRequest_0x2D;

    bool lockResource();
    void unlockResource();

    void setInitialListUN() /*noexcept*/;
    void extractionChildeFromStructure(UnitNode *un) /*noexcept*/;
    void extractionFromStructure(UnitNode *un) /*noexcept*/;
    void involvingInStructure(UnitNode *unMaster,
                              UnitNode *unSlave) /*noexcept*/;
    UnitNode* possGetUnitNode(QByteArray id) /*noexcept*/;
    UnitNode* possAddUnitNode(QByteArray id) /*noexcept*/;
    QByteArray setCRC8(QByteArray pattern) /*noexcept*/;
    QByteArray setByteStuffing(QByteArray pattern) /*noexcept*/;
    QByteArray setPreambula(QByteArray pattern) /*noexcept*/;
    void newMSG(QByteArray pattern) /*noexcept*/;

//    void request_0x0D(UnitNode *un) /*noexcept*/;
//    void request_0x0D() /*noexcept*/;
//    void request_0x1D(UnitNode *un,
//                      QByteArray pattern) /*noexcept*/;
//    void request_0x1D(QByteArray pattern) /*noexcept*/;
//    void request_0x1D(QByteArray idUN,
//                      quint8 numShot,
//                      quint8 numVK,
//                      quint8 numBlockH,
//                      quint8 numBlockL) /*noexcept*/;
//    void request_0x1D(UnitNode *un,
//                      quint8 numShot,
//                      quint8 numVK,
//                      quint8 numBlockH,
//                      quint8 numBlockL) /*noexcept*/;
    void request_0x41(UnitNode *un,
                   quint8 command,
                   quint8 d1 = 0x00,
                   quint8 d2 = 0x00,
                   quint8 d3 = 0x00,
                   quint8 d4 = 0x00) /*noexcept*/;

    void analyzer_0x0A(/*QByteArray pattern*/) /*noexcept*/;
    void analyzer_0xA1(UnitNode *un,
                    QByteArray pattern) /*noexcept*/;
    void analyzer_0xCE(QByteArray pattern) /*noexcept*/;
//    void analyzer_0x9F_0xC1_emit(UnitNode *un,
//                                 quint8 numVK,
//                                 quint8 numTrain,
//                                 quint8 numShot,
//                                 int countBlock,
//                                 quint32 sizeBlock = 310) /*noexcept*/;
//    void analyzer_0x9F_0xC1(UnitNode *un,
//                       QByteArray pattern) /*noexcept*/;
    //
    void analyzer_0x9F_0xC1_emit(UnitNode *un,
                                 quint8 numVK,
                                 quint8 numTrain,
                                 quint8 numShot,
                                 quint32 blockCount,
                                 quint32 blockSize = 310) /*noexcept*/;
    void analyzer_0x9F_0xC1(UnitNode *un,
                            QByteArray pattern) /*noexcept*/;
    //
    void analyzer_0x9F_0xC2(UnitNode *un,
                       QByteArray pattern) /*noexcept*/;
    void analyzer_0x9F_0xC3(UnitNode *un,
                       QByteArray pattern) /*noexcept*/;
    void analyzer_0x9F_0xC4(UnitNode *un,
                       QByteArray pattern) /*noexcept*/;
    void analyzer_0x9F_0xC5(UnitNode *un,
                       QByteArray pattern) /*noexcept*/;
    void analyzer_0x9F_0xC5_emit(UnitNode *un,
                                 quint8 numVK,
                                 quint8 numTrain,
                                 quint8 numShot) /*noexcept*/;
    void analyzer_0x9F_0xCF(UnitNode *un,
                       QByteArray pattern) /*noexcept*/;
    void analyzer_0x9F_0xCA(UnitNode *un,
                       QByteArray pattern) /*noexcept*/;
    void analyzer_0x9F_0xCB(UnitNode *un,
                       QByteArray pattern) /*noexcept*/;
    void analyzer_0x9F_0x90(UnitNode *un,
                       QByteArray pattern) /*noexcept*/;
    void analyzer_0x9F_0x91(UnitNode *un,
                       QByteArray pattern) /*noexcept*/;
    void analyzer_0x9F_0xBA(UnitNode *un,
                       QByteArray pattern) /*noexcept*/;
    void analyzer_0x9F_0xBB(UnitNode *un,
                       QByteArray pattern) /*noexcept*/;
    void analyzer_0x9F_0xBC(UnitNode *un,
                       QByteArray pattern) /*noexcept*/;
    void analyzer_0x9F_0xBD(UnitNode *un,
                       QByteArray pattern) /*noexcept*/;
    void analyzer_0x9F_0x01(UnitNode *un,
                       QByteArray pattern) /*noexcept*/;
    void analyzer_0x9F_0x05(UnitNode *un,
                       QByteArray pattern) /*noexcept*/;
    void analyzer_0x9F_0x40(UnitNode *un,
                       QByteArray pattern) /*noexcept*/;
    void analyzer_0x9F_0xD0(UnitNode *un,
                       QByteArray pattern) /*noexcept*/;
    void analyzer_0x9F_0x20(UnitNode *un,
                       QByteArray pattern) /*noexcept*/;
    void analyzer_0x9F_0x18(UnitNode *un,
                       QByteArray pattern) /*noexcept*/;
    void analyzer_0x9F_0x28(UnitNode *un,
                       QByteArray pattern) /*noexcept*/;
    void analyzer_0x9F_0xC0(UnitNode *un,
                            QByteArray pattern) /*noexcept*/;
    void analyzer_0x9F_0xCC(UnitNode *un,
                       QByteArray pattern) /*noexcept*/;
    void analyzer_0x9F_0x44(UnitNode *un,
                       QByteArray pattern) /*noexcept*/;
    void analyzer_0x9F_0x45(UnitNode *un,
                       QByteArray pattern) /*noexcept*/;
    void analyzer_0x9F_0x49(UnitNode *un,
                       QByteArray pattern) /*noexcept*/;
    void analyzer_0x9F_0x51(UnitNode *un,
                       QByteArray pattern) /*noexcept*/;
    void analyzer_0x9F_0x58(UnitNode *un,
                       QByteArray pattern) /*noexcept*/;
    void analyzer_0x9F_0x1F(UnitNode *un,
                       QByteArray pattern) /*noexcept*/;
    void analyzer_0x9F_0x1D(UnitNode *un,
                       QByteArray pattern) /*noexcept*/;
    void analyzer_0x9F_0x19(UnitNode *un,
                       QByteArray pattern) /*noexcept*/;
    void analyzer_0x9F_0xEF(UnitNode *un,
                       QByteArray pattern) /*noexcept*/;
    void analyzer_0x14(UnitNode *un,
                    QByteArray pattern) /*noexcept*/;
    void analyzer_0x24(UnitNode *un,
                    QByteArray pattern) /*noexcept*/;
    void new_analyzer_0x14(UnitNode *un,
                    QByteArray pattern) /*noexcept*/;
    void new_analyzer_0x24(UnitNode *un,
                    QByteArray pattern) /*noexcept*/;

    void emitAccept0x0A(UnitNode *un = nullptr) /*noexcept*/;
    void emitAddNewUN(UnitNode *un = nullptr) /*noexcept*/;
    void emitUpdateUN(UnitNode *un = nullptr) /*noexcept*/;
    quint32 emitInsertNewMSG(UnitNode *unSender,
                             quint8 codeMSG,
                             QByteArray msg,
                             bool ioType, TypeGroupMSG typeGroup,
                             bool flagHidden = false) /*noexcept*/;
    void emitNewMSGSend(QByteArray msg) /*noexcept*/;
    void emitTicPattern();

public slots:

signals:
    void ticPattern();
    void addNewUN(UnitNode *newUN);
    void updateUN(UnitNode *un);
    void accept0x0A(UnitNode *un = nullptr);
    void newMSGSend(QByteArray msg);
    void updateProgressShotLoad(UnitNode* un,
                                quint8 numShot,
                                quint8 numTrain,
                                quint8 value);
    void setAlarmCondition(UnitNode* un);


};

#endif // KSMSGMANAGER_H
