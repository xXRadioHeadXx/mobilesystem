#ifndef SERIALPORTMANAGERKS_H
#define SERIALPORTMANAGERKS_H

#include <QObject>
#include <QThread>
#include <QSerialPort>
#include <QSharedPointer>

#include <ksmsgmanager.h>
#include <threadanalysisks.h>
#include <threadreaderks.h>


class SerialPortManagerKS : public QObject
{
    Q_OBJECT
public:

    QString m_portName;
    qint32 m_rate;
    QSerialPort::DataBits m_dataBit;
    QSerialPort::StopBits m_stopBit;
    QSerialPort::FlowControl m_flow;
    QSerialPort::Parity m_parity;
    quint8 lastNumPattern,
           lastCodePattern;
    bool repeatExcept;

    KSMsgManager *ksMsgManager;
    QSerialPort *m_port;// порт
//    QSharedPointer<QSerialPort> m_port;// порт
    bool m_stopFlag;//главный флаг остановки

    ThreadAnalysisKS *prevousThreadAnalysis;
    ThreadReaderKS *prevousThreadReader;
//    QThread *prevousThreadAnalysis;
//    QThread *prevousThreadReader;

    QByteArray *m_ByteCollector;
    COSVariantList *m_listPattern;
    COSVariantList *m_listRequest;

    QList<QByteArray> *listDataRequest_0x2D;
    bool *passClearListDataRequest_0x2D;

    QMutex *m_mutexPattern;
    QMutex *m_mutexRequest;
    QMutex *m_mutexMSGAnalysis;

    explicit SerialPortManagerKS(KSMsgManager *ksMsg,
                                 QMutex *mutexPattern,
                                 QMutex *mutexRequest,
                                 QMutex *mutexMSGAnalysis,
                                 QObject *parent = nullptr) /*noexcept*/;

    virtual ~SerialPortManagerKS() /*noexcept*/;


    //    открытие порта
    bool openPort(QString portName = "COM1",
                  qint32 rate = 576000,//QSerialPort::BaudRate576000,
                  QSerialPort::DataBits dataBit = QSerialPort::Data8,
                  QSerialPort::StopBits stopBit = QSerialPort::OneStop,
                  QSerialPort::FlowControl flow = QSerialPort::NoFlowControl,
                  QSerialPort::Parity parity = QSerialPort::NoParity) /*noexcept*/;
    bool reopenPort() /*noexcept*/;
    void emitAcceptMSG();

signals:
    void portCondition(qint32 condition);
    void acceptMSG();

public slots:
    void process() /*noexcept*/;
    void sendRequestMSG() /*noexcept*/;
    void sendKSMSG(QByteArray msg) /*noexcept*/;
    bool refreshBuffer() /*noexcept*/;
//    void analisePattern(QByteArray pattern) /*noexcept*/;
    void acceptPattern() /*noexcept*/;
    void closePort() /*noexcept*/;
    void emitPortCondition() /*noexcept*/;
    void errorPort(QSerialPort::SerialPortError error) /*noexcept*/;

    bool isOpen() /*noexcept*/;
    QString portName() /*noexcept*/;
    qint32 baudRate() /*noexcept*/;
    qint32 dataBits() /*noexcept*/;
    qint32 parity() /*noexcept*/;
    qint32 stopBits() /*noexcept*/;
};

#endif // SERIALPORTMANAGERKS_H
