#include <kiparismanager.h>

KiparisManager::KiparisManager(DataBaseManager *db,
                               QObject *parent) /*noexcept*/ :
    QObject(parent),
    dbManager(db)
{
    try
    {
        this->m_ksMsgManager = new KSMsgManager(dbManager);
        this->m_portManager = new SerialPortManagerKS(m_ksMsgManager,
                                                      &m_mutexPattern,
                                                      &m_mutexRequest,
                                                      &m_mutexMSGAnalysis);
        this->m_portConnectionTimer = new QTimer;

        this->timetTicPattern = new QTimer;
        connect(this->timetTicPattern,
                SIGNAL(timeout()),
                this,
                SLOT(timeoutTicPattern()));

    //    this->m_threadReader = new QThread(this);
    //    this->m_portManager->moveToThread(this->m_threadReader);

        connect(this->m_portManager,
                SIGNAL(portCondition(qint32)),
                this,
                SIGNAL(portCondition(qint32)));

        connect(m_ksMsgManager,
                SIGNAL(newMSGSend(QByteArray)),
                m_portManager,
                SLOT(sendKSMSG(QByteArray)));

        connect(this->m_ksMsgManager,
                SIGNAL(addNewUN(UnitNode*)),
                this,
                SIGNAL(addNewUN(UnitNode*)));

        connect(this->m_ksMsgManager,
                SIGNAL(updateUN(UnitNode*)),
                this,
                SIGNAL(updateUN(UnitNode*)));

        connect(this->m_ksMsgManager,
                SIGNAL(ticPattern()),
                this,
                SLOT(ticPattern()));

    //    connect(this->m_ksMsgManager,
    //            SIGNAL(progress14(UnitNode*,quint8,quint8,quint8)),
    //            this,
    //            SIGNAL(progress14(UnitNode*,quint8,quint8,quint8)));

    //    connect(this->m_ksMsgManager,
    //            SIGNAL(progress24(UnitNode*,quint8,quint8,quint8)),
    //            this,
    //            SIGNAL(progress24(UnitNode*,quint8,quint8,quint8)));
        connect(this->m_ksMsgManager,
                SIGNAL(updateProgressShotLoad(UnitNode*,quint8,quint8,quint8)),
                this,
                SIGNAL(updateProgressShotLoad(UnitNode*,quint8,quint8,quint8)));

    //    connect(this->m_ksMsgManager,
    //            SIGNAL(setAlarmCondition(UnitNode*)),
    //            this,
    //            SLOT(setAlarmCondition(UnitNode*)));
    }
    catch (std::runtime_error &rte)
    {
        qWarning("catch runtime_error %s [KiparisManager::KiparisManager]", rte.what());
    }
    catch (std::bad_alloc& ba)
    {
        qWarning("catch bad_alloc %s [KiparisManager::KiparisManager]", ba.what());
    }
    catch (std::exception &e)
    {
        qWarning("catch exception %s [KiparisManager::KiparisManager]", e.what());
    }
    catch (...)
    {
        qWarning("catch exception <unknown> [KiparisManager::KiparisManager]");
    }
}

KiparisManager::~KiparisManager() /*noexcept*/
{
    try
    {
//    while(!listTimerOffAlarmCondition.isEmpty())
//    {
//        listTimerOffAlarmCondition.first()->stop();
//        delete listTimerOffAlarmCondition.first();
//        listTimerOffAlarmCondition.removeFirst();
//    }

        delete m_portManager;
        delete m_ksMsgManager;
        dbManager = nullptr;
    }
    catch(...)
    {
#ifdef QT_DEBUG
        qDebug() << "Assert(~KiparisManager)";
#endif
        return;
    }
}

void KiparisManager::portConnectionTimeout()
{
    this->m_portConnectionTimer->stop();
    if(false != this->connectionStatus)
    {
        this->connectionStatus = false;
    }
    emit this->updateConnectionStatus(false);
}

void KiparisManager::portConnectionTimerStart()
{
    this->m_portConnectionTimer->stop();
    if(true != this->connectionStatus)
    {
        this->connectionStatus = true;
        emit this->updateConnectionStatus(true);
    }
}

void KiparisManager::closePort() /*noexcept*/
{
    this->m_portManager->closePort();
    emit this->updatePortSetting(this->getPortSetting());

    this->emitInsertNewMSG(0x00,
                           0xFF,
                           QString("Port closed succeeded").toUtf8(),
                           true,
                           Connect_TypeGroupMSG);
}

bool KiparisManager::reopenPort()
{
    if(this->m_portManager->reopenPort())
    {
        emit this->updatePortSetting(this->getPortSetting());
        return true;
    }
    emit this->updatePortSetting(QList<QString>());
    return false;
}

bool KiparisManager::openPort(QString portName,
                              qint32 rate,
                              QSerialPort::DataBits dataBits,
                              QSerialPort::StopBits stopBits,
                              QSerialPort::FlowControl flow,
                              QSerialPort::Parity parity) /*noexcept*/
{
//    this->m_portManager->inst(this->m_ksManager);
    if(this->m_portManager->openPort(portName,
                                     rate,
                                     dataBits,
                                     stopBits,
                                     flow,
                                     parity))
    {
        this->emitInsertNewMSG(0x00,
                               0xFF,
                               QString("Port " + portName + " opening succeeded").toUtf8(),
                               true,
                               Connect_TypeGroupMSG);
//        this->m_portManager->btManager = this->m_btManager;
//        this->m_portManager->start();
//        this->m_ksMsgManager->requestSET();
        emit this->updatePortSetting(this->getPortSetting());
        this->ticPattern();
        return true;
    }

    this->emitInsertNewMSG(0x00,
                           0xFF,
                           QString("Port " + portName + " opening failed").toUtf8(),
                           true,
                           Connect_TypeGroupMSG);

    emit this->updatePortSetting(QList<QString>());
    return false;
}

QList<QString> KiparisManager::getPortSetting() /*noexcept*/
{
    QList<QString> listSetting;
    if(this->m_portManager->m_port)
    {
        if(this->m_portManager->isOpen())
        {
            listSetting.append(this->m_portManager->portName());
            QString strBaud("%1");
            strBaud = strBaud.arg(this->m_portManager->baudRate());
            listSetting.append(strBaud);
            QString strData("%1");
            strData = strData.arg(this->m_portManager->dataBits());
            listSetting.append(strData);
            listSetting.append((0 != this->m_portManager->parity()) ? "Y" : "N");
            QString strStop("%1");
            strStop = strStop.arg(this->m_portManager->stopBits());
            listSetting.append(strStop);
            return listSetting;
        }
    }
    return listSetting;
}

QList<UnitNode*> KiparisManager::getListUnitNode() /*noexcept*/
{
    return this->m_ksMsgManager->listKSUnitNode;
}

quint32 KiparisManager::emitInsertNewMSG(UnitNode *unSender,
                                       quint8 codeMSG,
                                       QByteArray msg,
                                       bool ioType,
                                       TypeGroupMSG typeGroup,
                                       bool flagHidden) /*noexcept*/
{
    quint32 indexDBMsg(0);
    indexDBMsg = this->dbManager->addNewMSG(unSender,
                                            codeMSG,
                                            msg,
                                            ioType,
                                            KSSystemType,
                                            typeGroup,
                                            flagHidden);
    return indexDBMsg;
}

//quint32 KiparisManager::emitInsertNewMSG(quint32 indexDBMsg) /*noexcept*/
//{
//    emit this->insertNewMSG(indexDBMsg);
//    return indexDBMsg;
//}

void KiparisManager::cmdRequest_0x41(UnitNode *unSender,
                                     quint8 cmdCod,
                                     quint8 d1,
                                     quint8 d2,
                                     quint8 d3,
                                     quint8 d4) /*noexcept*/
{
    this->m_ksMsgManager->request_0x41(unSender,
                                       cmdCod,
                                       d1,
                                       d2,
                                       d3,
                                       d4);
}

void KiparisManager::ticPattern()
{
    if(this->timetTicPattern->isActive())
    {
        this->timetTicPattern->stop();
    }
    else
    {
        this->emitInsertNewMSG(0x00,
                               0xFF,
                               QString("Recovered connect").toUtf8(),
                               true,
                               Connect_TypeGroupMSG);
    }
    this->timetTicPattern->start(10000);
}

void KiparisManager::timeoutTicPattern()
{
    this->timetTicPattern->stop();

    QList<UnitNode*> list(this->getListUnitNode());
    for(int i(0), n(list.size()); i < n; i++)
    {
        list.at(i)->updateFlagConnection(false);
    }


    this->emitInsertNewMSG(0x00,
                           0xFF,
                           QString("Lost connect").toUtf8(),
                           true,
                           Connect_TypeGroupMSG);
}

//void KiparisManager::setAlarmCondition(UnitNode *un) /*noexcept*/
//{
//    QTimer *timerOffAlarmCondition(new QTimer(this));

//    connect(timerOffAlarmCondition,
//            SIGNAL(timeout()),
//            un,
//            SLOT(offAlarmCondition()));

//    connect(timerOffAlarmCondition,
//            SIGNAL(timeout()),
//            timerOffAlarmCondition,
//            SLOT(stop()));

//    connect(timerOffAlarmCondition,
//            SIGNAL(timeout()),
//            this,
//            SLOT(offAlarmCondition()));

//    this->listTimerOffAlarmCondition.append(timerOffAlarmCondition);

//    un->setAlarmCondition();

//    timerOffAlarmCondition->start(3000);

//}

//void KiparisManager::offAlarmCondition() /*noexcept*/
//{
//    for(int i(0); i < this->listTimerOffAlarmCondition.size();)
//    {
//        if(this->listTimerOffAlarmCondition.at(i)->isActive())
//        {
//            i++;
//        }
//        else
//        {
//            delete this->listTimerOffAlarmCondition.at(i);
//            this->listTimerOffAlarmCondition.removeAt(i);
//        }
//    }
//}
