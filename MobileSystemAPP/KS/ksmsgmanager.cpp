#include <ksmsgmanager.h>

KSMsgManager::KSMsgManager(DataBaseManager *db,
                           QObject *parent) /*noexcept*/ :
    QObject(parent),
    dbManager(db)
{
    try
    {
        this->m_numLastMSG = 0xFF;
        this->m_codeLastMSG = 0xFF;
        this->m_numMSG = 0x00;
        this->m_lastAppendBase = nullptr;
//        this->m_resursLock = new QMutex;
        this->m_treeRootUN = new UnitNode;
        this->m_treeRootUN->updateType(KSSystemType, UnknownDeviceType);
        this->beeper = new SoundAlarm(this);
        this->lastUNToSend = nullptr;
        this->lastMSGToSend.clear();
        this->m_resurceStatus = false;
    }
    catch (std::runtime_error &rte)
    {
        qWarning("catch runtime_error %s [KSMsgManager::KSMsgManager]", rte.what());
    }
    catch (std::bad_alloc& ba)
    {
        qWarning("catch bad_alloc %s [KSMsgManager::KSMsgManager]", ba.what());
    }
    catch (std::exception &e)
    {
        qWarning("catch exception %s [KSMsgManager::KSMsgManager]", e.what());
    }
    catch (...)
    {
        qWarning("catch exception <unknown> [KSMsgManager::KSMsgManager]");
    }
//    passClearListDataRequest_0x2D = false;
}

KSMsgManager::~KSMsgManager() /*noexcept*/
{
    try
    {
        for(int i(0), n(this->listKSUnitNode.size()); i < n; i++)
        {
            this->listKSUnitNode.at(i)->updateFlagConnection(false);
            this->listKSUnitNode.at(i)->emitUploadUN();
        }
//        delete m_resursLock;
        delete m_treeRootUN;
        delete beeper;
    }
    catch(...)
    {
#ifdef QT_DEBUG
        qDebug() << "Assert(~KSMsgManager)";
#endif
        return;
    }
}

bool KSMsgManager::lockResource()
{
    if(false == m_resurceStatus)
    {
//        while(false == m_resursLock->tryLock()){};
        m_resurceStatus = true;
        return true;
    }
    else
    {
        qWarning("Assert(softly lock) [KSMsgManager::lockResurce]");
    }
    return false;
}

void KSMsgManager::unlockResource()
{
    if(true == m_resurceStatus)
    {
//        m_resursLock->unlock();
        m_resurceStatus = false;
    }
    else
    {
        qWarning("Assert(softly unlock) [KSMsgManager::unlockResurce]");
    }
}


void KSMsgManager::setInitialListUN() /*noexcept*/
{
//    QList<quint32> listUNIndex(dbManager->getAllUNRecords());
    auto listUNIndex(dbManager->getAllUNRecords());
    if(listUNIndex.isEmpty())
    {
        return;
    }
    while(!listUNIndex.isEmpty())
    {

        QSqlRecord unRecord(dbManager->getUNRecord(listUNIndex.first()));
        listUNIndex.removeFirst();
        if(unRecord.isEmpty())
            return;

        if(KSSystemType != (TypeUnitNodeSystem)unRecord.value("typeSystemUN").toUInt())
        {
            continue;
        }

        try
        {
            UnitNode *newUN(new UnitNode(this));
            newUN->id = unRecord.value("idUN").toByteArray();
            newUN->mac = unRecord.value("macUN").toByteArray();
            newUN->name = unRecord.value("nameUN").toByteArray();
            newUN->abstractName = unRecord.value("abstractNameUN").toByteArray();
            newUN->typeUN = (TypeUnitNodeDevice)unRecord.value("typeUN").toUInt();
            newUN->typeSystem = (TypeUnitNodeSystem)unRecord.value("typeSystemUN").toUInt();
            newUN->btLink = unRecord.value("link").toByteArray();
            newUN->flag_connection = unRecord.value("linkValid").toBool();
            newUN->graphicsPxmItem->setX(unRecord.value("xPos").toInt());
            newUN->graphicsPxmItem->setY(unRecord.value("yPos").toInt());
            newUN->voltage = unRecord.value("voltage").toUInt();
            newUN->numArea = unRecord.value("area").toUInt();
            newUN->numNode = unRecord.value("number").toUInt();
            newUN->flag_discharge = unRecord.value("discharge").toBool();
            newUN->dbIndex = unRecord.value("id").toUInt();
            newUN->dbIndexParent = unRecord.value("idParentUN").toUInt();
    //        newUN->graphicsTxtItem->setX(unRecord.value("xPos").toInt());
    //        newUN->graphicsTxtItem->setY(unRecord.value("yPos").toInt() + 20);
    //        newUN->graphicsTxtItem->setHtml(newUN->getGraphicsTextLabel());

            newUN->geoAltitude = unRecord.value("geoAltitude").toDouble();

            newUN->geoLatitudeFlag = unRecord.value("geoLatitudeFlag").toUInt();
            newUN->geoLatitude = unRecord.value("geoLatitude").toDouble();
            newUN->geoLatitudeDegrees = unRecord.value("geoLatitudeDegrees").toDouble();
            newUN->geoLatitudeMinutes = unRecord.value("geoLatitudeMinutes").toDouble();
            newUN->geoLatitudeSeconds = unRecord.value("geoLatitudeSeconds").toDouble();

            newUN->geoLongitudeFlag = unRecord.value("geoLongitudeFlag").toUInt();
            newUN->geoLongitude = unRecord.value("geoLongitude").toDouble();
            newUN->geoLongitudeDegrees = unRecord.value("geoLongitudeDegrees").toDouble();
            newUN->geoLongitudeMinutes = unRecord.value("geoLongitudeMinutes").toDouble();
            newUN->geoLongitudeSeconds = unRecord.value("geoLongitudeSeconds").toDouble();

            newUN->flag_hidden = ((0 != unRecord.value("hidden").toInt()) ? true : false);

            for(int i(0); i < 2; i++)
            {
                QString strMode("modeVK%1");
                strMode = strMode.arg(i + 1);
                newUN->listSettingVK[i].modeVK = unRecord.value(strMode).toUInt();

                QByteArray nullIDBSK;
                nullIDBSK.append((char)0x00);
                nullIDBSK.append((char)0x00);
                for(int j(0); j < 20; j++)
                {
                    QByteArray idBSK;
                    QString strBSK("ConnectVK%1_%2");
                    strBSK = strBSK.arg(i + 1)
                                   .arg(j + 1);

                    idBSK.append(unRecord.value(strBSK).toByteArray());

                    newUN->updateListIDBSK(i + 1,
                                           j + 1,
                                           idBSK);

                    if(idBSK.isEmpty() || idBSK == nullIDBSK)
                    {
                        newUN->listSettingVK[i].listIDBSK[j] = nullIDBSK;
                    }
                    else
                    {
                        newUN->listSettingVK[i].listIDBSK[j] = idBSK;
                    }
                }

                for(int j(0); j < 5; j++)
                {
                    QString strX("xPosLFlangP_%1"), strY("yPosLFlangP_%1");
                    strX = strX.arg(j + 1);
                    strY = strY.arg(j + 1);
                    newUN->listLFlangP[j]->setPos(unRecord.value(strX).toInt(),
                                                  unRecord.value(strY).toInt());
                }

                for(int j(0); j < 5; j++)
                {
                    QString strX("xPosRFlangP_%1"), strY("yPosRFlangP_%1");
                    strX = strX.arg(j + 1);
                    strY = strY.arg(j + 1);
                    newUN->listRFlangP[j]->setPos(unRecord.value(strX).toInt(),
                                                  unRecord.value(strY).toInt());
                }
            }

            if(UnknownDeviceType != newUN->_typeUN())
            {
    //            newUN->graphicsPxmItem->setFlag(QGraphicsItem::ItemIsMovable);
                newUN->graphicsPxmItem->setFlag(QGraphicsItem::ItemIsSelectable);
                newUN->graphicsPxmItem->setFlag(QGraphicsItem::ItemIsFocusable);
                newUN->graphicsPxmItem->setZValue(1);
            }

            connect(newUN,
                    SIGNAL(updateUN(UnitNode*)),
                    this,
                    SIGNAL(updateUN(UnitNode*)));

            connect(newUN,
                    SIGNAL(uploadUN(UnitNode*)),
                    dbManager,
                    SLOT(uploadUNToDB(UnitNode*)));


    //        connect(newUN,
    //                SIGNAL(updateUN(UnitNode*)),
    //                dbManager,
    //                SLOT(updateUN(UnitNode*)));

            this->listKSUnitNode.append(newUN);
        }
        catch (std::runtime_error &rte)
        {
            qWarning("catch runtime_error %s [KSMsgManager::setInitialListUN]", rte.what());
        }
        catch (std::bad_alloc& ba)
        {
            qWarning("catch bad_alloc %s [KSMsgManager::setInitialListUN]", ba.what());
        }
        catch (std::exception &e)
        {
            qWarning("catch exception %s [KSMsgManager::setInitialListUN]", e.what());
        }
        catch (...)
        {
            qWarning("catch exception <unknown> [KSMsgManager::setInitialListUN]");
        }
    }

    for(int i(0), n(this->listKSUnitNode.size()); i < n; i++)
    {
        if(0 == this->listKSUnitNode.at(i)->dbIndexParent)
        {
            this->involvingInStructure(this->m_treeRootUN,
                                       this->listKSUnitNode.at(i));
            emit this->addNewUN(this->listKSUnitNode.at(i));
        }
        else
        {
            for(int j(0); j < n; j++)
            {
                if(this->listKSUnitNode.at(j)->dbIndex == this->listKSUnitNode.at(i)->dbIndexParent)
                {
                    this->involvingInStructure(this->listKSUnitNode.at(j),
                                               this->listKSUnitNode.at(i));
                    emit this->addNewUN(this->listKSUnitNode.at(i));
                    break;
                }
            }
        }
    }
}

UnitNode* KSMsgManager::possGetUnitNode(QByteArray id) /*noexcept*/
{
    for(int i(0), n(this->listKSUnitNode.size()); i < n; i++)
    {
        if(id == this->listKSUnitNode.at(i)->id)
        {
            if(CPP_KSDeviceType == this->listKSUnitNode.at(i)->_typeUN())
            {
                this->m_lastAppendBase = this->listKSUnitNode.at(i);
            }
            return this->listKSUnitNode.at(i);
        }
    }
    return nullptr;
}

UnitNode* KSMsgManager::possAddUnitNode(QByteArray id) /*noexcept*/
{
    if(id.isEmpty() || 2 != id.size())
        return nullptr;
    if((quint8)0x00 == (quint8)id.at(0) && (quint8)0x00 == (quint8)id.at(1))
        return nullptr;
    if((quint8)0xFE == (quint8)id.at(0) && (quint8)0xFE == (quint8)id.at(1))
        return nullptr;
    if((quint8)0x0F == (quint8)id.at(0) && (quint8)0x0F == (quint8)id.at(1))
        return nullptr;

    quint8 id1(((quint8)id.at(0) / 0x10) & 0x0F),
           id2((quint8)id.at(0) & 0x0F),
           id3(((quint8)id.at(1) / 0x10) & 0x0F),
           id4((quint8)id.at(1) & 0x0F);
    if((quint8)CPP_KSDeviceType != id1 &&
       (quint8)RT01_KSDeviceType != id1 &&
       (quint8)RT02_KSDeviceType != id1 &&
       (quint8)VK09_KSDeviceType != id1 &&
       (quint8)VK0A_KSDeviceType != id1 &&
       (quint8)VK0B_KSDeviceType != id1)
        return nullptr;
    if((quint8)0x09 < id2 ||
       (quint8)0x09 < id3 ||
       (quint8)0x09 < id4)
        return nullptr;
    if(!(((quint8)id.at(0) & 0x0F) + (quint8)id.at(1)))
        return nullptr;
    QByteArray name;
    bool addKey(true);

    for(int i(0), n(this->listKSUnitNode.size()); i < n; i++)
    {
        if(id == this->listKSUnitNode.at(i)->id)
        {
            addKey = false;
            if(CPP_KSDeviceType == this->listKSUnitNode.at(i)->_typeUN())
            {
                this->m_lastAppendBase = this->listKSUnitNode.at(i);
            }
            return this->listKSUnitNode.at(i);
        }
    }
    try
    {
        if(addKey)
        {
            UnitNode *newUnitNode(new UnitNode(this));
            newUnitNode->clear();

            TypeUnitNodeDevice type((TypeUnitNodeDevice)(((quint8)id.at(0) & (quint8)0xF0) / (quint8)0x10));
            newUnitNode->updateType(KSSystemType, type);

            switch(type)
            {
                case CPP_KSDeviceType:
                {
                    name.append("AWS_");
                    break;
                }

    //            case RT_KSDeviceType:
    //            {
    //                name.append("RT_");
    //                break;
    //            }

                case RT01_KSDeviceType:
                {
                    name.append("RT_");
                    break;
                }

                case RT02_KSDeviceType:
                {
                    name.append("RT_");
                    break;
                }

                case VK09_KSDeviceType:
                {
                    name.append("VK_");
                    break;
                }

                case VK0A_KSDeviceType:
                {
                    name.append("VK_");
                    break;
                }

                case VK0B_KSDeviceType:
                {
                    name.append("VK_");
                    break;
                }

                default:
                {
                    name.append("UN_");
                    break;
                }
            }
            name.append(id.toHex());
            newUnitNode->updateName(name);
            newUnitNode->updateID(id);

            this->listKSUnitNode.append(newUnitNode);

            //
            newUnitNode->updateDBIndex(this->dbManager->addNewUN(newUnitNode));
            //

            this->involvingInStructure(this->m_treeRootUN,
                                       newUnitNode);

            connect(newUnitNode,
                    SIGNAL(updateUN(UnitNode*)),
                    this,
                    SIGNAL(updateUN(UnitNode*)));

            connect(newUnitNode,
                    SIGNAL(uploadUN(UnitNode*)),
                    dbManager,
                    SLOT(uploadUNToDB(UnitNode*)));

            this->emitAddNewUN(newUnitNode);

            if(CPP_KSDeviceType == newUnitNode->_typeUN())
            {
                this->m_lastAppendBase = newUnitNode;
            }
            return newUnitNode;
        }
    }
    catch (std::runtime_error &rte)
    {
        qWarning("catch runtime_error %s [KSMsgManager::possAddUnitNode]", rte.what());
    }
    catch (std::bad_alloc& ba)
    {
        qWarning("catch bad_alloc %s [KSMsgManager::possAddUnitNode]", ba.what());
    }
    catch (std::exception &e)
    {
        qWarning("catch exception %s [KSMsgManager::possAddUnitNode]", e.what());
    }
    catch (...)
    {
        qWarning("catch exception <unknown> [KSMsgManager::possAddUnitNode]");
    }
    return nullptr;
}

void KSMsgManager::extractionChildeFromStructure(UnitNode *un) /*noexcept*/
{
    while(!un->listChilde.isEmpty())
    {
//        UnitNode *objPtr(un->listChilde.first()),
//                 *sourceParent(un);
//        int sourceChild(0);
        //
        this->extractionFromStructure(un->listChilde.first());
        //
//        UnitNode *destinationParent(objPtr->unTreeParent);
//        int destinationChild(objPtr->unTreeParent->listChilde.indexOf(objPtr));

//        emit this->moveUN(objPtr,
//                          sourceParent,
//                          sourceChild,
//                          destinationParent,
//                          destinationChild);
    }

    un->listChilde.clear();
}

void KSMsgManager::extractionFromStructure(UnitNode *un) /*noexcept*/
{
    while(!un->listChilde.isEmpty())
    {
        this->extractionFromStructure(un->listChilde.first());
//        un->listChilde.removeFirst();
    }

    if(un->unTreeParent)
    {
        if(un->unTreeParent->listChilde.contains(un))
        {
            un->unTreeParent->listChilde.removeOne(un);
        }
    }
    this->m_treeRootUN->appendChild(un);
    un->listChilde.clear();
    un->updateFlagConnection(false);
}

QByteArray KSMsgManager::setByteStuffing(QByteArray pattern) /*noexcept*/
{
    QByteArray marker0x1B,
               marker0xA5,
               marker0x1Bx3B,
               marker0x1BxC5;
    marker0x1B.append((quint8)0x1B);
    marker0xA5.append((quint8)0xA5);
    marker0x1Bx3B.append((quint8)0x1B);
    marker0x1Bx3B.append((quint8)0x3B);
    marker0x1BxC5.append((quint8)0x1B);
    marker0x1BxC5.append((quint8)0xC5);
    for(int i(1); i < pattern.size(); i++)
    {
        if((quint8)0xA5 == (quint8)pattern.at(i) ||
           (quint8)0x1B == (quint8)pattern.at(i))
        {
            pattern[i] = (quint8)pattern.at(i) + (quint8)0x20;
            pattern.insert(i, marker0x1B);
            i++;
        }
    }
    return pattern;
}

QByteArray KSMsgManager::setCRC8(QByteArray pattern) /*noexcept*/
{
    int n(pattern.size() - 1);
    pattern[n] = (quint8)0xFF;
    for(int i(1); i < n; i++)
    {
        pattern[n] = (quint8)pattern.at(n) - (quint8)pattern.at(i);
    }
    return pattern;
}

QByteArray KSMsgManager::setPreambula(QByteArray pattern) /*noexcept*/
{
    pattern.prepend((quint8)0xFF);
    pattern.prepend((quint8)0xFF);
    pattern.prepend((quint8)0xFF);
    pattern.prepend((quint8)0xFF);
    pattern.prepend((quint8)0xFF);
    pattern.prepend((quint8)0xFF);
    pattern.prepend((quint8)0xFF);
    pattern.prepend((quint8)0xFF);
    pattern.prepend((quint8)0xFF);
    pattern.prepend((quint8)0xFF);
    pattern.prepend((quint8)0xFF);
    pattern.prepend((quint8)0xFF);
    pattern.prepend((quint8)0xFF);
    pattern.prepend((quint8)0xFF);
    pattern.prepend((quint8)0xFF);
//    pattern.prepend((quint8)0xFF);
//    pattern.prepend((quint8)0xFF);
//    pattern.prepend((quint8)0xFF);
//    pattern.prepend((quint8)0xFF);
//    pattern.prepend((quint8)0xFF);
//    pattern.prepend((quint8)0xFF);
//    pattern.prepend((quint8)0xFF);
//    pattern.prepend((quint8)0xFF);
//    pattern.prepend((quint8)0xFF);
//    pattern.prepend((quint8)0xFF);
//    pattern.prepend((quint8)0xFF);
//    pattern.prepend((quint8)0xFF);
//    pattern.prepend((quint8)0xFF);
//    pattern.prepend((quint8)0xFF);
//    pattern.prepend((quint8)0xFF);
//    pattern.prepend((quint8)0xFF);
//    pattern.prepend((quint8)0xFF);
//    pattern.prepend((quint8)0xFF);
//    pattern.prepend((quint8)0xFF);
//    pattern.prepend((quint8)0xFF);
//    pattern.prepend((quint8)0xFF);
    pattern.append((quint8)0x22);
    pattern.append((char)0x00);
    return pattern;
}

void KSMsgManager::involvingInStructure(UnitNode *unMaster,
                                        UnitNode *unSlave) /*noexcept*/
{
    if(unMaster && unSlave)
    {
        if(!this->m_treeRootUN->listChilde.removeOne(unSlave))
        {
            for(int i(0); i < this->listKSUnitNode.size(); i++)
            {
                if(this->listKSUnitNode.at(i)->listChilde.removeOne(unSlave))
                    break;
            }
        }
        unMaster->appendChild(unSlave);
    }
}


void KSMsgManager::newMSG(QByteArray pattern) /*noexcept*/
{
    this->emitTicPattern();

    bool needUnlock(false);
    needUnlock = this->lockResource();

    bool exceptAnalyzeKey(true);
    quint8 numMSG((quint8)pattern.at(3) / (quint8)0x10),
           codeMSG((quint8)pattern.at(5));
    if(m_numLastMSG == numMSG &&
       m_codeLastMSG == codeMSG)
    {
        exceptAnalyzeKey = false;
    }
    m_numLastMSG = numMSG;
    m_codeLastMSG = codeMSG;

    QByteArray idUN;
    UnitNode *unSender(0);
    switch ((quint8)pattern.at(5))
    {
        case (quint8)0x09:
        {
#ifdef QT_DEBUG
            qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " KsMSG  INPUT  << [command    09] [" << pattern.left(20).toHex() << "..." << pattern.size() << "..." << pattern.right(10).toHex() << "]";
#endif
            break;
        };

        case (quint8)0x0A:
        {
#ifdef QT_DEBUG
            qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " KsMSG  INPUT  << [command    0A] [" << pattern.left(20).toHex() << "..." << pattern.size() << "..." << pattern.right(10).toHex() << "]";
#endif
//            idUN = pattern.mid(6, 2);
//            if(!m_lastAppendBase)
//            {
//                m_lastAppendBase = unSender = this->possAddUnitNode(idUN);
//            }
//            else
//            {
//                unSender = m_lastAppendBase;
//            }

            unSender = m_lastAppendBase;

            if(unSender)
            {
                unSender->updateFlagHidden(false);
            }
            this->analyzer_0x0A();
            break;
        }

        case (quint8)0x0B:
        {
#ifdef QT_DEBUG
            qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " KsMSG  INPUT  << [command    0B] [" << pattern.left(20).toHex() << "..." << pattern.size() << "..." << pattern.right(10).toHex() << "]";
#endif
            break;
        };

        case (quint8)0x2D:
        {
#ifdef QT_DEBUG
            qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " KsMSG  INPUT  << [command    2D] [" << pattern.left(20).toHex() << "..." << pattern.size() << "..." << pattern.right(10).toHex() << "]";
#endif
//            request_0x2D();
            break;
        };


        case (quint8)0x51:
        {
#ifdef QT_DEBUG
            qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " KsMSG  INPUT  << [command    51] [" << pattern.left(20).toHex() << "..." << pattern.size() << "..." << pattern.right(10).toHex() << "]";
#endif
            break;
        };

        case (quint8)0x52:
        {
#ifdef QT_DEBUG
            qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " KsMSG  INPUT  << [command    52] [" << pattern.left(20).toHex() << "..." << pattern.size() << "..." << pattern.right(10).toHex() << "]";
#endif
            break;
        };

        case (quint8)0x55:
        {
#ifdef QT_DEBUG
            qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " KsMSG  INPUT  << [command    55] [" << pattern.left(20).toHex() << "..." << pattern.size() << "..." << pattern.right(10).toHex() << "]";
#endif
            break;
        };

        case (quint8)0x56:
        {
#ifdef QT_DEBUG
            qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " KsMSG  INPUT  << [command    56] [" << pattern.left(20).toHex() << "..." << pattern.size() << "..." << pattern.right(10).toHex() << "]";
#endif
            break;
        };

        case (quint8)0x60:
        {
#ifdef QT_DEBUG
            qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " KsMSG  INPUT  << [command    60] [" << pattern.left(20).toHex() << "..." << pattern.size() << "..." << pattern.right(10).toHex() << "]";
#endif
            break;
        };

        case (quint8)0x61:
        {
#ifdef QT_DEBUG
            qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " KsMSG  INPUT  << [command    61] [" << pattern.left(20).toHex() << "..." << pattern.size() << "..." << pattern.right(10).toHex() << "]";
#endif
            break;
        };

        case (quint8)0x14:
        {
#ifdef QT_DEBUG
            qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " KsMSG  INPUT  << [command    14] [" << pattern.left(20).toHex() << "..." << pattern.size() << "..." << pattern.right(10).toHex() << "]";
#endif

            if(!exceptAnalyzeKey)
            {
#ifdef QT_DEBUG
                qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " KsMSG  INPUT  << [command    14] [" << "exceptAnalyzeKey:" << m_numLastMSG << "]";
#endif
                break;
            }

            idUN = pattern.mid(6, 2);
            unSender = this->possAddUnitNode(idUN);
            if(unSender)
            {
                unSender->updateFlagHidden(false);
            }
            this->analyzer_0x14(unSender,
                                pattern);

            break;
        };

        case (quint8)0x24:
        {
#ifdef QT_DEBUG
            if(100 > pattern.size())
            {
                qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " KsMSG  INPUT  << [command    24] [" << pattern.toHex() << "]";
            }
            else
            {
                qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " KsMSG  INPUT  << [command    24] [" << pattern.left(20).toHex() << "..." << pattern.size() << "..." << pattern.right(10).toHex() << "]";
            }
#endif
            idUN = pattern.mid(6, 2);
            unSender = this->possAddUnitNode(idUN);

            if(unSender)
            {
                unSender->updateFlagHidden(false);
            }
            this->analyzer_0x24(unSender,
                             pattern);
            break;
        };

        case (quint8)0x35:
        {
#ifdef QT_DEBUG
            qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " KsMSG  INPUT  << [command    35] [" << pattern.left(20).toHex() << "..." << pattern.size() << "..." << pattern.right(10).toHex() << "]";
#endif
            break;
        };

        case (quint8)0x9F:
        {
    //            this->request_0x0D();

            if(!exceptAnalyzeKey)
            {
#ifdef QT_DEBUG
                qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " KsMSG  INPUT  << [command 9F_XX] [" << "exceptAnalyzeKey:" << m_numLastMSG << "] [" << pattern.toHex() << "]";
#endif
                break;
            }

            if(21 != pattern.size())
            {
                break;
            }

            //

            idUN = pattern.mid(7, 2);
            unSender = this->possAddUnitNode(idUN);

            if(nullptr == unSender)
            {
                break;
//                if(needUnlock){this->unlockResource();}
//                return;
            }

            unSender->updateFlagHidden(false);

            unSender->updateFlagOff(false);
            unSender->updateFlagConnection(true);
            unSender->updateNumberArea((quint8)pattern.at(10));
            unSender->updateNumberNode((quint8)pattern.at(11));

            if((quint8)0xC1 != (quint8)pattern.at(6))
            {
                unSender->updateVoltage((quint8)pattern.at(17));
            }

            switch((quint8)pattern.at(6))
            {

                case (quint8)0x01:
                {
#ifdef QT_DEBUG
                    qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " KsMSG  INPUT  << [command 9F_01] [" << pattern.toHex() << "]";
#endif
                    this->analyzer_0x9F_0x01(unSender,
                                             pattern);
                    break;
                };

                case (quint8)0x05:
                {
#ifdef QT_DEBUG
                    qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " KsMSG  INPUT  << [command 9F_01] [" << pattern.toHex() << "]";
#endif
                    this->analyzer_0x9F_0x05(unSender,
                                             pattern);
                    break;
                };

                case (quint8)0x18:
                {
#ifdef QT_DEBUG
                    qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " KsMSG  INPUT  << [command 9F_18] [" << pattern.toHex() << "]";
#endif
                    this->analyzer_0x9F_0x18(unSender,
                                             pattern);
                    break;
                };

                case (quint8)0x28:
                {
#ifdef QT_DEBUG
                    qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " KsMSG  INPUT  << [command 9F_28] [" << pattern.toHex() << "]";
#endif
                    this->analyzer_0x9F_0x28(unSender,
                                             pattern);
                    break;
                };


                case (quint8)0x19:
                {
#ifdef QT_DEBUG
                    qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " KsMSG  INPUT  << [command 9F_19] [" << pattern.toHex() << "]";
#endif
                    this->analyzer_0x9F_0x19(unSender,
                                        pattern);
                    break;
                };

                case (quint8)0x1D:
                {
#ifdef QT_DEBUG
                    qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " KsMSG  INPUT  << [command 9F_1D] [" << pattern.toHex() << "]";
#endif
                    this->analyzer_0x9F_0x1D(unSender,
                                        pattern);
                    break;
                };

                case (quint8)0x1F:
                {
#ifdef QT_DEBUG
                    qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " KsMSG  INPUT  << [command 9F_1F] [" << pattern.toHex() << "]";
#endif
                    this->analyzer_0x9F_0x1F(unSender,
                                        pattern);
                    break;
                };

                case (quint8)0x20:
                {
#ifdef QT_DEBUG
                    qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " KsMSG  INPUT  << [command 9F_20] [" << pattern.toHex() << "]";
#endif
                    this->analyzer_0x9F_0x20(unSender,
                                        pattern);
                    break;
                };

                case (quint8)0x21:
                {
                    break;
                };

                case (quint8)0x22:
                {
                    break;
                };

                case (quint8)0x40:
                {
#ifdef QT_DEBUG
                    qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " KsMSG  INPUT  << [command 9F_40] [" << pattern.toHex() << "]";
#endif
                    this->analyzer_0x9F_0x40(unSender,
                                        pattern);
                    break;
                };

                case (quint8)0x44:
                {
#ifdef QT_DEBUG
                    qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " KsMSG  INPUT  << [command 9F_44] [" << pattern.toHex() << "]";
#endif
                    this->analyzer_0x9F_0x44(unSender,
                                        pattern);
                    break;
                };

                case (quint8)0x45:
                {
#ifdef QT_DEBUG
                    qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " KsMSG  INPUT  << [command 9F_45] [" << pattern.toHex() << "]";
#endif
                    this->analyzer_0x9F_0x45(unSender,
                                        pattern);
                    break;
                };

                case (quint8)0x49:
                {
#ifdef QT_DEBUG
                    qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " KsMSG  INPUT  << [command 9F_49] [" << pattern.toHex() << "]";
#endif
                    this->analyzer_0x9F_0x49(unSender,
                                             pattern);
                    break;
                };

                case (quint8)0x51:
                {
#ifdef QT_DEBUG
                    qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " KsMSG  INPUT  << [command 9F_51] [" << pattern.toHex() << "]";
#endif
                    this->analyzer_0x9F_0x51(unSender,
                                             pattern);

                    break;
                };

                case (quint8)0x58:
                {
#ifdef QT_DEBUG
                    qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " KsMSG  INPUT  << [command 9F_58] [" << pattern.toHex() << "]";
#endif
                    this->analyzer_0x9F_0x58(unSender,
                                             pattern);
                    break;
                };

                case (quint8)0x90:
                {
#ifdef QT_DEBUG
                    qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " KsMSG  INPUT  << [command 9F_90] [" << pattern.toHex() << "]";
#endif
                    this->analyzer_0x9F_0x90(unSender,
                                        pattern);
                    break;
                };

                case (quint8)0x91:
                {
#ifdef QT_DEBUG
                    qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " KsMSG  INPUT  << [command 9F_91] [" << pattern.toHex() << "]";
#endif
                    this->analyzer_0x9F_0x91(unSender,
                                        pattern);
                    break;
                };

                case (quint8)0xBA:
                {
#ifdef QT_DEBUG
                    qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " KsMSG  INPUT  << [command 9F_BA] [" << pattern.toHex() << "]";
#endif
                    this->analyzer_0x9F_0xBA(unSender,
                                             pattern);
                    break;
                };

                case (quint8)0xBB:
                {
#ifdef QT_DEBUG
                    qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " KsMSG  INPUT  << [command 9F_BB] [" << pattern.toHex() << "]";
#endif
                    this->analyzer_0x9F_0xBB(unSender,
                                        pattern);
                    break;
                };

                case (quint8)0xBC:
                {
#ifdef QT_DEBUG
                    qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " KsMSG  INPUT  << [command 9F_BC] [" << pattern.toHex() << "]";
#endif
                    this->analyzer_0x9F_0xBC(unSender,
                                        pattern);
                    break;
                };

                case (quint8)0xBD:
                {
#ifdef QT_DEBUG
                    qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " KsMSG  INPUT  << [command 9F_DB] [" << pattern.toHex() << "]";
#endif
                    this->analyzer_0x9F_0xBD(unSender,
                                        pattern);
                    break;
                };

                case (quint8)0xC0:
                {
#ifdef QT_DEBUG
                    qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " KsMSG  INPUT  << [command 9F_C0] [" << pattern.toHex() << "]";
#endif
                    this->analyzer_0x9F_0xC0(unSender,
                                             pattern);
                    break;
                };



                case (quint8)0xC1:
                {
#ifdef QT_DEBUG
                    qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " KsMSG  INPUT  << [command 9F_C1] [" << pattern.toHex() << "]";
#endif
                    this->analyzer_0x9F_0xC1(unSender,
                                        pattern);
                    break;
                };

                case (quint8)0xC2:
                {
#ifdef QT_DEBUG
                    qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " KsMSG  INPUT  << [command 9F_C2] [" << pattern.toHex() << "]";
#endif
                    this->analyzer_0x9F_0xC2(unSender,
                                        pattern);
                    break;
                };

                case (quint8)0xC3:
                {
#ifdef QT_DEBUG
                    qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " KsMSG  INPUT  << [command 9F_C3] [" << pattern.toHex() << "]";
#endif
                    this->analyzer_0x9F_0xC3(unSender,
                                        pattern);
                    break;
                };

                case (quint8)0xC4:
                {
#ifdef QT_DEBUG
                    qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " KsMSG  INPUT  << [command 9F_C4] [" << pattern.toHex() << "]";
#endif
                    this->analyzer_0x9F_0xC4(unSender,
                                        pattern);
                    break;
                };

                case (quint8)0xC5:
                {
#ifdef QT_DEBUG
                    qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " KsMSG  INPUT  << [command 9F_C5] [" << pattern.toHex() << "]";
#endif
                    this->analyzer_0x9F_0xC5(unSender,
                                        pattern);
                    break;
                };

                case (quint8)0xCA:
                {
#ifdef QT_DEBUG
                    qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " KsMSG  INPUT  << [command 9F_CA] [" << pattern.toHex() << "]";
#endif
                    this->analyzer_0x9F_0xCA(unSender,
                                        pattern);
                    break;
                };

                case (quint8)0xCB:
                {
#ifdef QT_DEBUG
                    qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " KsMSG  INPUT  << [command 9F_CB] [" << pattern.toHex() << "]";
#endif
                    this->analyzer_0x9F_0xCB(unSender,
                                        pattern);
                    break;
                };

                case (quint8)0xCC:
                {
#ifdef QT_DEBUG
                    qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " KsMSG  INPUT  << [command 9F_CC] [" << pattern.toHex() << "]";
#endif
                    this->analyzer_0x9F_0xCC(unSender,
                                        pattern);
                    break;
                };

                case (quint8)0xCF:
                {
#ifdef QT_DEBUG
                    qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " KsMSG  INPUT  << [command 9F_CF] [" << pattern.toHex() << "]";
#endif
                    this->analyzer_0x9F_0xCF(unSender,
                                        pattern);
                    break;
                };

                case (quint8)0xD0:
                {
#ifdef QT_DEBUG
                    qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " KsMSG  INPUT  << [command 9F_D0] [" << pattern.toHex() << "]";
#endif
                    this->analyzer_0x9F_0xD0(unSender,
                                        pattern);
                    break;
                };

                case (quint8)0xEF:
                {
#ifdef QT_DEBUG
                    qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " KsMSG  INPUT  << [command 9F_EF] [" << pattern.toHex() << "]";
#endif
                    this->analyzer_0x9F_0xEF(unSender,
                                        pattern);
                    break;
                };


                default:
                {
#ifdef QT_DEBUG
                    if(100 > pattern.size())
                    {
                        qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " KsMSG  INPUT  << [command 9F_XX] [" << pattern.toHex() << "]";
                    }
                    else
                    {
                        qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " KsMSG  INPUT  << [command 9F_XX] [" << pattern.left(20).toHex() << "..." << pattern.size() << "..." << pattern.right(10).toHex() << "]";
                    }
#endif
                    this->emitInsertNewMSG(unSender,
                                           pattern.at(6),
                                           pattern,
                                           true,
                                           Error_TypeGroupMSG,
                                           true);
                    break;
                }
            }
            break;
        };

        case (quint8)0xA1:
        {
//            qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " KsMSG  INPUT  << [command    A1] [" << pattern.toHex() << "]";

            idUN = pattern.mid(6, 2);
            if(!m_lastAppendBase)
            {
                m_lastAppendBase = unSender = this->possAddUnitNode(idUN);
            }
            else
            {
                unSender = m_lastAppendBase;
            }

            if(unSender)
            {
                unSender->updateFlagHidden(false);
            }

            this->analyzer_0xA1(unSender,
                             pattern);
            break;
        };

        case (quint8)0xCE:
        {
#ifdef QT_DEBUG
            qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " KsMSG  INPUT  << [command    CE] [" << pattern.toHex() << "]";
#endif

            if(!exceptAnalyzeKey)
            {
#ifdef QT_DEBUG
                qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << "                  [command    CE] [" << " exceptAnalyzeKey: " << m_numLastMSG << "]";
#endif
                break;
            }

            this->analyzer_0xCE(pattern);
            break;
        };

        default:
        {
#ifdef QT_DEBUG
            if(100 > pattern.size())
            {
                qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " KsMSG  INPUT  << [command    XX] [" << pattern.toHex() << "]";
            }
            else
            {
                qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " KsMSG  INPUT  << [command    XX] [" << pattern.left(20).toHex() << "..." << pattern.size() << "..." << pattern.right(10).toHex() << "]";
            }
#endif
            this->emitInsertNewMSG(0x00,
                                   pattern.at(5),
                                   pattern,
                                   true,
                                   Error_TypeGroupMSG,
                                   true);

            break;
        }
    }

    if(needUnlock){this->unlockResource();}
//    else{qWarning("lock fail [KSMsgManager::newMSG]");}
}

//Квитанция на ПК о приеме команды
void KSMsgManager::analyzer_0x0A(/*QByteArray pattern*/) /*noexcept*/
{
    UnitNode *un(this->m_lastAppendBase);
    if(nullptr == this->lastUNToSend || this->lastMSGToSend.isEmpty())
        return;

    if(nullptr != un)
    {
        this->emitAccept0x0A(un);
    }

    if(nullptr != this->lastUNToSend &&
       !this->lastMSGToSend.isEmpty())
    {
        emitInsertNewMSG(this->lastUNToSend,
                         this->lastMSGToSend.at(5),
                         this->lastMSGToSend,
                         false,
                         Command_TypeGroupMSG);

        this->lastUNToSend = nullptr;
        this->lastMSGToSend.clear();
    }
}

//Контроль связи с База-ТВ по RS-485-интерфейсу
void KSMsgManager::analyzer_0xA1(UnitNode *un,
                                 QByteArray pattern) /*noexcept*/
{
    if(!un)
        return;
    this->m_lastAppendBase = un;
    un->updateVoltage(pattern.at(8));
    un->updateFlagConnection(true);
//    this->emitInsertNewMSG(un,
//                           pattern.at(5),
//                           pattern,
//                           true,
//                           Other_TypeGroupMSG);
}

//Загрузка на ПЭВМ таблицы сети с ПУЦ-МВСР
void KSMsgManager::analyzer_0xCE(QByteArray pattern) /*noexcept*/
{
    int count(((quint8)pattern.at(6) * 0x0100) + (quint8)pattern.at(7));

    auto listAllUN(this->listKSUnitNode);
    listAllUN.removeOne(this->m_lastAppendBase);

    if(9 ==  pattern.size())
    {
        for(auto item: listAllUN)
        {
            item->updateFlagConnection(false);
        }
        return;
    }

    if(12 >= pattern.size())
    {
        for(auto item: listAllUN)
        {
            item->updateFlagConnection(false);
        }
    }
    else
    {
        for(int i(0); i < count; i++)
        {
            UnitNode *unSlave(this->possAddUnitNode(pattern.mid(8 + 4 * i, 2))),
                     *unMaster(this->possAddUnitNode(pattern.mid(10 + 4 * i, 2)));

            if(unSlave)
            {
                unSlave->updateFlagConnection(true);
                unSlave->updateFlagHidden(false);
                listAllUN.removeOne(unSlave);
            }
            if(unMaster)
            {
                unMaster->updateFlagConnection(true);
                unMaster->updateFlagHidden(false);
                listAllUN.removeOne(unMaster);
            }

            this->involvingInStructure(unMaster,
                                       unSlave);
        }

        for(auto item: listAllUN)
        {
            item->updateFlagConnection(false);
        }
    }
}

void KSMsgManager::analyzer_0x9F_0xC1_emit(UnitNode *un,
                                           quint8 numVK,
                                           quint8 numTrain,
                                           quint8 numShot,
                                           quint32 blockCount,
                                           quint32 blockSize) /*noexcept*/
{
    // Даже себе нельзя верить

//    // Проверка типа устройства
//    if((quint8)TypeUnitNodeDevice::VK09_KSDeviceType != (quint8)un->typeUN &&
//       (quint8)TypeUnitNodeDevice::VK0A_KSDeviceType != (quint8)un->typeUN &&
//       (quint8)TypeUnitNodeDevice::VK0B_KSDeviceType != (quint8)un->typeUN)
//    {
//#ifdef QT_DEBUG
//        qDebug() << "                                 [command 9F_C1] [*wrong type UN(" << un->typeUN << ")]";
//#endif
//        return;
//    }

    // Проверка номера камеры
    if(1 != numVK &&
       2 != numVK)
    {
#ifdef QT_DEBUG
        qDebug() << "                                 [command 9F_C1] [*wrong numVK(" << numVK << ")]";
#endif
        return;
    }

    // Проверка номера кадра
    if(1 != numShot &&
       2 != numShot &&
       3 != numShot &&
       4 != numShot &&
       5 != numShot)
    {
#ifdef QT_DEBUG
        qDebug() << "                                 [command 9F_C1] [*wrong numShot(" << numShot << ")]";
#endif
        return;
    }

    quint32 possibleReleaseSize(blockCount * blockSize);

    QByteArray pattern;
    pattern.append((quint8)0xA5);
    pattern.append((quint8)0x08);
    pattern.append((quint8)0x0A);
    pattern.append((char)0x00);
    pattern.append((quint8)0x0E);
    pattern.append((quint8)0x9F);
    pattern.append((quint8)0xC1);
    pattern.append(un->_id());
    pattern.append((char)0x00);
    pattern.append(un->_numArea());
    pattern.append(un->_numNode());
    pattern.append(numTrain * 0x10 + numShot);//12
    pattern.append((quint8)(possibleReleaseSize / 0x0100));//13
    pattern.append((quint8)(possibleReleaseSize / 0x00FF));//14
    pattern.append(blockSize / 0x0100);//15
    pattern.append(blockSize / 0x00FF);//16
    pattern.append(numVK);//17
    pattern.append((char)0x00);
    pattern.append((char)0x00);
    pattern.append((quint8)0xFF);
    pattern = this->setCRC8(pattern);

#ifdef QT_DEBUG
    qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " KsMSG  EMIT   << [command 9F_C1] [" << pattern.toHex() << "]";
#endif

//!___________________________________________________________________!

    // уже было такое сообщение: ничего не делаем и уходим
    for(int i(0), n(un->listIMGCollectorCell.size()); i < n; i++)
    {
        if(un->listIMGCollectorCell.at(i).numVK == numVK &&
           un->listIMGCollectorCell.at(i).numTrain == numTrain &&
           un->listIMGCollectorCell.at(i).numShot == numShot)
        {
            if(un->listIMGCollectorCell.at(i).releaseSize == possibleReleaseSize)
            {
#ifdef QT_DEBUG
                qDebug() << "                                 [command 9F_C1] [*repeat]";
#endif
                return;
            }
        }
    }
//!___________________________________________________________________!

    // запишем в БД, индекс сохраним и будем думать, выделять память или нет.
    quint32 id_This_MSG0xC1(this->emitInsertNewMSG(un,
                                                   (quint8)pattern.at(6),
                                                   pattern,
                                                   true,
                                                   FF_TypeGroupMSG/*,
                                                   true*/));
    quint32 id_Main_MSG0xC1(0);
    if(0 == id_This_MSG0xC1)
    {
#ifdef QT_DEBUG
                qDebug() << "                               [command 9F_C1] [*id_This_MSG0xC1 is null]";
#endif
        return;
    }

//!___________________________________________________________________!
    // -имеется или нет такой же картеж \
    //                                     =>
    // -повторения не найденно          /

    // из обнарежения совпадения numVK, numTrain, numShot и не совпадения releaseSize
    // следует: на подходе новый кортеж того же порядка numTrain
    // сносимнах всё от старого кортежа, dbIndexMSG0xC1 запомним как id_Main_MSG0xC1
    bool passRemoveTrain(false);
    for(int i(0), n(un->listIMGCollectorCell.size()); i < n; i++)
    {
        if(un->listIMGCollectorCell.at(i).numVK == numVK &&
           un->listIMGCollectorCell.at(i).numTrain == numTrain)
        {
            id_Main_MSG0xC1 = un->listIMGCollectorCell.at(i).id_Main_MSG0xC1;
            if(un->listIMGCollectorCell.at(i).numShot == numShot)
            {
                //будем удалять весь кортеж
                passRemoveTrain = true;
                id_Main_MSG0xC1 = id_This_MSG0xC1;
#ifdef QT_DEBUG
                qDebug() << "                               [command 9F_C1] [*passRemoveTrain]";
#endif
                break;
            }
        }
    }

    if(passRemoveTrain)
    {
        for(int i(0); i < un->listIMGCollectorCell.size();)
        {
            if(un->listIMGCollectorCell.at(i).numTrain == numTrain)
            {
                un->listIMGCollectorCell.removeAt(i);
            }
            else
            {
                i++;
            }
        }
    }

    id_Main_MSG0xC1 = ((0 == id_Main_MSG0xC1) ? id_This_MSG0xC1 : id_Main_MSG0xC1);
//!___________________________________________________________________!

//!___________________________________________________________________!
    // добавить место под ещё один кадр:
    IMGCollectorCell newIMGCell;
    newIMGCell.numVK = numVK;
    newIMGCell.numShot = numShot;
    newIMGCell.numTrain = numTrain;
    newIMGCell.blockSize = blockSize;
    newIMGCell.blockCount = blockCount;
    newIMGCell.releaseSize = possibleReleaseSize;
    newIMGCell.id_This_MSG0xC1 = id_This_MSG0xC1;
    newIMGCell.id_Main_MSG0xC1 = id_Main_MSG0xC1;
    // добавление
    un->listIMGCollectorCell.append(newIMGCell);
#ifdef QT_DEBUG
    qDebug() << "                               [command 9F_C1] [*append new IMGCollectorCell]";

    for(int i(0), n(un->listIMGCollectorCell.size()); i < n; i++)
    {
        if(un->listIMGCollectorCell.at(i).numVK == numVK &&
           un->listIMGCollectorCell.at(i).numTrain == numTrain)
        {
            qDebug() << "                               [command 9F_C1] [*ls: "
//                     << "id("                 << un->_id().toHex().toUpper()
                     << "numVK("            << un->listIMGCollectorCell.at(i).numVK
                     << ") numTrain("         << un->listIMGCollectorCell.at(i).numTrain
                     << ") numShot("          << un->listIMGCollectorCell.at(i).numShot
                     << ") releaseSize("      << un->listIMGCollectorCell.at(i).releaseSize
                     << ") id_This_MSG0xC1("  << un->listIMGCollectorCell.at(i).id_This_MSG0xC1
                     << ") id_Main_MSG0xC1("  << un->listIMGCollectorCell.at(i).id_Main_MSG0xC1
                     << ")"
                     << "]";
        }
    }
#endif
    //!___________________________________________________________________!

    if(id_This_MSG0xC1 == id_Main_MSG0xC1)
    {
        quint8 rmSaveNumTrain0((numTrain - 0x02) & 0x0F),
               rmSaveNumTrain1((numTrain - 0x01) & 0x0F),
               rmSaveNumTrain2((numTrain) & 0x0F),
               rmSaveNumTrain3((numTrain + 0x01) & 0x0F),
               rmSaveNumTrain4((numTrain + 0x02) & 0x0F);
        for(int i(0); i < un->listIMGCollectorCell.size();)
        {
            if(un->listIMGCollectorCell.at(i).numVK == numVK &&
               un->listIMGCollectorCell.at(i).numTrain != rmSaveNumTrain0 &&
               un->listIMGCollectorCell.at(i).numTrain != rmSaveNumTrain1 &&
               un->listIMGCollectorCell.at(i).numTrain != rmSaveNumTrain2 &&
               un->listIMGCollectorCell.at(i).numTrain != rmSaveNumTrain3 &&
               un->listIMGCollectorCell.at(i).numTrain != rmSaveNumTrain4)
            {
                un->listIMGCollectorCell.removeAt(i);
            }
            else
            {
                i++;
            }
        }
    }

    un->updateFlagFailure(false); // !!!

    return;
}


void KSMsgManager::analyzer_0x9F_0xC1(UnitNode *un,
                                          QByteArray pattern) /*noexcept*/
{
// Почему такие глупые проверки? Больше, ни одной суке нельзя доверять!

//// Проверка типа устройства
//    if((quint8)TypeUnitNodeDevice::VK09_KSDeviceType != (quint8)un->typeUN &&
//       (quint8)TypeUnitNodeDevice::VK0A_KSDeviceType != (quint8)un->typeUN &&
//       (quint8)TypeUnitNodeDevice::VK0B_KSDeviceType != (quint8)un->typeUN)
//    {
//#ifdef QT_DEBUG
//        qDebug() << "                                 [command 9F_C1] [wrong type UN(" << un->typeUN << ")]";
//#endif
//        return;
//    }

// Проверка номера камеры
    quint8 numVK((quint8)pattern.at(17));
    if(1 != numVK &&
       2 != numVK)
        return;

// Проверка номера кадра
    quint8 numShot((quint8)pattern.at(12) & (quint8)0x0F);
    if(1 != numShot &&
       2 != numShot &&
       3 != numShot &&
       4 != numShot &&
       5 != numShot)
        return;

    quint8 numTrain(((quint8)pattern.at(12) / (quint8)0x10) & (quint8)0x0F);

    quint32 blockSize(((quint32)pattern.at(15) * (quint32)0x00000100) + (quint32)pattern.at(16));
    blockSize = blockSize & (quint32)0x0000FFFF;

    quint32 releaseSize((((quint32)pattern.at(13) * 0x0100) & (quint32)0x0000FF00) + ((quint32)pattern.at(14) & (quint32)0x000000FF));
    releaseSize = releaseSize & (quint32)0x0000FFFF;

    quint32 blockCount((releaseSize / blockSize) + ((0 != (releaseSize % blockSize)) ? 1 : 0));

    quint32 possibleReleaseSize(blockCount * blockSize);

//!___________________________________________________________________!
    // уже было такое сообщение: ничего не делаем и уходим
    for(int i(0), n(un->listIMGCollectorCell.size()); i < n; i++)
    {
        if(un->listIMGCollectorCell.at(i).numVK == numVK &&
           un->listIMGCollectorCell.at(i).numTrain == numTrain &&
           un->listIMGCollectorCell.at(i).numShot == numShot)
        {
            if(un->listIMGCollectorCell.at(i).releaseSize == releaseSize)
            {
#ifdef QT_DEBUG
                qDebug() << "                               [command 9F_C1] [repeat]";
#endif
                return;
            }
            if(un->listIMGCollectorCell.at(i).releaseSize == possibleReleaseSize)
            {
                // обработка С1 "случилась" позже обработки блока кадров
                un->listIMGCollectorCell[i].releaseSize = releaseSize;
#ifdef QT_DEBUG
                qDebug() << "                               [command 9F_C1] [repeat and size editing]";
#endif
                return;
            }
        }
    }
//!___________________________________________________________________!

    // запишем в БД, индекс сохраним и будем думать, выделять память или нет.
    quint32 id_This_MSG0xC1(this->emitInsertNewMSG(un,
                                                   (quint8)pattern.at(6),
                                                   pattern,
                                                   true,
                                                   FF_TypeGroupMSG/*,
                                                   true*/));
    quint32 id_Main_MSG0xC1(0);
    if(0 == id_This_MSG0xC1)
    {
#ifdef QT_DEBUG
                qDebug() << "                                   [command 9F_C1] [id_This_MSG0xC1 is null]";
#endif
        return;
    }

//!___________________________________________________________________!
    // -имеется или нет такой же картеж \
    //                                     =>
    // -повторения не найденно          /

    // из обнарежения совпадения numVK, numTrain, numShot и не совпадения releaseSize
    // следует: на подходе новый кортеж того же порядка numTrain
    // сносимнах всё от старого кортежа, dbIndexMSG0xC1 запомним как id_Main_MSG0xC1
    bool passRemoveTrain(false);
    for(int i(0), n(un->listIMGCollectorCell.size()); i < n; i++)
    {
        if(un->listIMGCollectorCell.at(i).numVK == numVK &&
           un->listIMGCollectorCell.at(i).numTrain == numTrain)
        {
            id_Main_MSG0xC1 = un->listIMGCollectorCell.at(i).id_Main_MSG0xC1;
            if(un->listIMGCollectorCell.at(i).numShot == numShot)
            {
                //будем удалять весь кортеж
                passRemoveTrain = true;
                id_Main_MSG0xC1 = id_This_MSG0xC1;
#ifdef QT_DEBUG
                qDebug() << "                               [command 9F_C1] [passRemoveTrain]";
#endif
                break;
            }
        }
    }

    if(passRemoveTrain)
    {
        for(int i(0); i < un->listIMGCollectorCell.size();)
        {
            if(un->listIMGCollectorCell.at(i).numTrain == numTrain)
            {
                un->listIMGCollectorCell.removeAt(i);
            }
            else
            {
                i++;
            }
        }
    }

    id_Main_MSG0xC1 = ((0 == id_Main_MSG0xC1) ? id_This_MSG0xC1 : id_Main_MSG0xC1);
//!___________________________________________________________________!

//!___________________________________________________________________!
    // добавить место под ещё один кадр:
    IMGCollectorCell newIMGCell;
    newIMGCell.numVK = numVK;
    newIMGCell.numShot = numShot;
    newIMGCell.numTrain = numTrain;
    newIMGCell.blockSize = blockSize;
    newIMGCell.blockCount = blockCount;
    newIMGCell.releaseSize = releaseSize;
    newIMGCell.id_This_MSG0xC1 = id_This_MSG0xC1;
    newIMGCell.id_Main_MSG0xC1 = id_Main_MSG0xC1;
    // добавление
    un->listIMGCollectorCell.append(newIMGCell);
#ifdef QT_DEBUG
    qDebug() << "                                 [command 9F_C1] [append new IMGCollectorCell]";

    for(int i(0), n(un->listIMGCollectorCell.size()); i < n; i++)
    {
        if(un->listIMGCollectorCell.at(i).numVK == numVK &&
           un->listIMGCollectorCell.at(i).numTrain == numTrain)
        {
            qDebug() << "                                 [command 9F_C1] [ls: "
//                     << "id("                 << un->_id().toHex().toUpper()
                     << "numVK("            << un->listIMGCollectorCell.at(i).numVK
                     << ") numTrain("         << un->listIMGCollectorCell.at(i).numTrain
                     << ") numShot("          << un->listIMGCollectorCell.at(i).numShot
                     << ") releaseSize("      << un->listIMGCollectorCell.at(i).releaseSize
                     << ") id_This_MSG0xC1("  << un->listIMGCollectorCell.at(i).id_This_MSG0xC1
                     << ") id_Main_MSG0xC1("  << un->listIMGCollectorCell.at(i).id_Main_MSG0xC1
                     << ")"
                     << "]";
        }
    }
#endif
    //!___________________________________________________________________!

    if(id_This_MSG0xC1 == id_Main_MSG0xC1)
    {
        quint8 rmSaveNumTrain0((numTrain - 0x02) & 0x0F),
               rmSaveNumTrain1((numTrain - 0x01) & 0x0F),
               rmSaveNumTrain2((numTrain) & 0x0F),
               rmSaveNumTrain3((numTrain + 0x01) & 0x0F),
               rmSaveNumTrain4((numTrain + 0x02) & 0x0F);
        for(int i(0); i < un->listIMGCollectorCell.size();)
        {
            if(un->listIMGCollectorCell.at(i).numVK == numVK &&
               un->listIMGCollectorCell.at(i).numTrain != rmSaveNumTrain0 &&
               un->listIMGCollectorCell.at(i).numTrain != rmSaveNumTrain1 &&
               un->listIMGCollectorCell.at(i).numTrain != rmSaveNumTrain2 &&
               un->listIMGCollectorCell.at(i).numTrain != rmSaveNumTrain3 &&
               un->listIMGCollectorCell.at(i).numTrain != rmSaveNumTrain4)
            {
                un->listIMGCollectorCell.removeAt(i);
            }
            else
            {
                i++;
            }
        }
    }

    un->updateFlagFailure(false); // !!!

    return;
}


//СК не готов к передаче
void KSMsgManager::analyzer_0x9F_0xC2(UnitNode *un,
                                 QByteArray pattern) /*noexcept*/
{
    this->emitInsertNewMSG(un,
                           pattern.at(6),
                           pattern,
                           true,
                           FF_TypeGroupMSG);
}

//ВК приняла команду на формировании пакета СК
void KSMsgManager::analyzer_0x9F_0xC3(UnitNode *un,
                                 QByteArray pattern) /*noexcept*/
{
    this->emitInsertNewMSG(un,
                           pattern.at(6),
                           pattern,
                           true,
                           FF_TypeGroupMSG);

}

//Соответствие СК Тревоге
void KSMsgManager::analyzer_0x9F_0xC4(UnitNode *un,
                                 QByteArray pattern) /*noexcept*/
{
    quint32 idMSG = this->emitInsertNewMSG(un,
                                           pattern.at(6),
                                           pattern,
                                           true,
                                           FF_TypeGroupMSG,
                                           true);
    this->dbManager->updateForeignIDAlarm(un, idMSG);
}

//CК получен
void KSMsgManager::analyzer_0x9F_0xC5_emit(UnitNode *un,
                                           quint8 numVK,
                                           quint8 numTrain,
                                           quint8 numShot) /*noexcept*/
{
    QByteArray pattern;
    pattern.append((quint8)0xA5);
    pattern.append((quint8)0x08);
    pattern.append((quint8)0x0A);
    pattern.append((char)0x00);
    pattern.append((quint8)0x0E);
    pattern.append((quint8)0x9F);
    pattern.append((quint8)0xC5);
    pattern.append(un->_id());
    pattern.append((char)0x00);
    pattern.append(un->_numArea());
    pattern.append(un->_numNode());
    pattern.append(numVK);
    pattern.append(numTrain * 0x10 + numShot);
    pattern.append((char)0x00);
    pattern.append((char)0x00);
    pattern.append((char)0x00);
    pattern.append((char)0x00);
    pattern.append((char)0x00);
    pattern.append((char)0x00);
    pattern.append((quint8)0xFF);
    pattern = this->setCRC8(pattern);

#ifdef QT_DEBUG
    qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " KsMSG  EMIT   << [command 9F_C5] [" << pattern.toHex() << "]";
#endif


    this->emitInsertNewMSG(un,
                           pattern.at(6),
                           pattern,
                           true,
                           FF_TypeGroupMSG);

    emit this->updateProgressShotLoad(un,
                                      pattern.at(13) & 0x0F,
                                      (pattern.at(13) / 0x10) & 0x0F,
                                      0);
    return;
}

//CК получен
void KSMsgManager::analyzer_0x9F_0xC5(UnitNode *un,
                                 QByteArray pattern) /*noexcept*/
{
    this->emitInsertNewMSG(un,
                           pattern.at(6),
                           pattern,
                           true,
                           FF_TypeGroupMSG,
                           true);

    emit this->updateProgressShotLoad(un,
                                      pattern.at(13) & 0x0F,
                                      (pattern.at(13) / 0x10) & 0x0F,
                                      0);

    return;
}

//Неисправность модуля ВК
void KSMsgManager::analyzer_0x9F_0xCF(UnitNode *un,
                                 QByteArray pattern) /*noexcept*/
{
    un->updateFlagFailure(true);

    this->emitInsertNewMSG(un,
                           pattern.at(6),
                           pattern,
                           true,
                           Error_TypeGroupMSG/*,
                           true*/);

    return;
    
    quint8 numVK(pattern.at(12));
    if(1 != numVK &&
       2 != numVK)
        return;

    if(numVK > un->listSettingVK.size())
        return;

    numVK--;
    un->listSettingVK[numVK].flag_connection = false;
}

//Отсутствие связи по RS-485
void KSMsgManager::analyzer_0x9F_0xCA(UnitNode *un,
                                 QByteArray pattern) /*noexcept*/
{
    un->updateFlagFailure(true);

    this->emitInsertNewMSG(un,
                           pattern.at(6),
                           pattern,
                           true,
                           Error_TypeGroupMSG);
    return;

//    un->updateVoltage(pattern.at(17));
//    un->updateFlagConnection(true);
    
    quint8 typeD(pattern.at(12));
    switch((quint8)typeD)
    {
        case (quint8)0x09:
        {
            break;
        }
        case (quint8)0x0B:
        {
            quint8 numVK(pattern.at(13));
			if(0 == numVK ||
			   2 < numVK)
                return;
            numVK--;
            un->listSettingVK[numVK].flag_connection = false;
            break;
        }
        case (quint8)0x0E:
        {
            break;
        }
        default:
        {
            break;
        }
    }
}

//Восстановление  связи по RS-485
void KSMsgManager::analyzer_0x9F_0xCB(UnitNode *un,
                                 QByteArray pattern) /*noexcept*/
{
    un->updateFlagFailure(false);

    this->emitInsertNewMSG(un,
                           pattern.at(6),
                           pattern,
                           true,
                           Error_TypeGroupMSG);

    quint8 typeD(pattern.at(12));
    switch((quint8)typeD)
    {
        case (quint8)0x09:
        {
            un->updateNumberKit(pattern.at(13));
            un->listSettingVK[0].numComplex = pattern.at(13);
            un->listSettingVK[1].numComplex = pattern.at(13);
            break;
        }
        case (quint8)0x0B:
        {
            quint8 numVK(pattern.at(13));
			if(0 == numVK ||
               2 < numVK)
                return;
            numVK--;
            un->listSettingVK[numVK].flag_connection = true;
            un->listSettingVK[numVK].modeVK = pattern.at(13) & 0x0F;
            break;
        }
        case (quint8)0x0E:
        {
            break;
        }
        default:
        {
            break;
        }
    }
}

//Включено
void KSMsgManager::analyzer_0x9F_0x90(UnitNode *un,
                                 QByteArray pattern) /*noexcept*/
{
    un->updateFlagOff(false);
    un->updateFlagDischarge(false);
    un->updateFlagAlarm(false);
    un->updateFlagFailure(false);

    //
    for(int i(0), n(un->listTrainIMG.size()); i < n; i++)
    {
        un->listTrainIMG[i].numTrain = (quint8)0xFF;//(quint8)0x00;
        un->listTrainIMG[i].mainIDMSG0xC1 = (quint8)0x00;//(quint8)0x00;
        un->listTrainIMG[i].currentNumShot = (quint8)0xFF;
        un->listTrainIMG[i].listColleclorIMG.clear();

    }

    this->emitInsertNewMSG(un,
                           (quint8)pattern.at(6),
                           pattern,
                           true,
                           Discharge_TypeGroupMSG);
}

//Отключено
void KSMsgManager::analyzer_0x9F_0x91(UnitNode *un,
                                 QByteArray pattern) /*noexcept*/
{
    un->updateFlagOff(true);
    un->updateFlagDischarge(false);
    un->updateFlagConnection(false);
    this->extractionFromStructure(un);

    this->emitInsertNewMSG(un,
                           (quint8)pattern.at(6),
                           pattern,
                           true,
                           Discharge_TypeGroupMSG);
}

//Норма ИПА
void KSMsgManager::analyzer_0x9F_0xBA(UnitNode *un,
                                 QByteArray pattern) /*noexcept*/
{
    un->updateFlagDischarge(false);

    this->emitInsertNewMSG(un,
                           (quint8)pattern.at(6),
                           pattern,
                           true,
                           Discharge_TypeGroupMSG);
}

//Разряд ИПА
void KSMsgManager::analyzer_0x9F_0xBB(UnitNode *un,
                                 QByteArray pattern) /*noexcept*/
{
    un->updateFlagDischarge(true);

    this->emitInsertNewMSG(un,
                           (quint8)pattern.at(6),
                           pattern,
                           true,
                           Discharge_TypeGroupMSG);
}

void KSMsgManager::analyzer_0x9F_0xBC(UnitNode *un,
                                 QByteArray pattern) /*noexcept*/
{
    un->updateFlagDischarge(true);

    this->emitInsertNewMSG(un,
                           (quint8)pattern.at(6),
                           pattern,
                           true,
                           Discharge_TypeGroupMSG);
}

//Отключение блока по разряду АКК
void KSMsgManager::analyzer_0x9F_0xBD(UnitNode *un,
                                 QByteArray pattern) /*noexcept*/
{
    un->updateFlagDischarge(true);
    un->updateFlagOff(true);
    un->updateFlagConnection(false);
    this->extractionFromStructure(un);

    this->emitInsertNewMSG(un,
                           (quint8)pattern.at(6),
                           pattern,
                           true,
                           Discharge_TypeGroupMSG);
}

//ПредТревога
void KSMsgManager::analyzer_0x9F_0x05(UnitNode *un,
                                 QByteArray pattern) /*noexcept*/
{    
    if(!this->beeper->isRunning() &&
       this->beeperFlag)
    {
        this->beeper->start();
    }

    this->emitInsertNewMSG(un,
                           (quint8)pattern.at(6),
                           pattern,
                           true,
                           Alarm_TypeGroupMSG);
}

//Тревога
void KSMsgManager::analyzer_0x9F_0x01(UnitNode *un,
                                 QByteArray pattern) /*noexcept*/
{
//    un->updateFlagAlarm(true);
    un->setAlarmCondition();

    if(!this->beeper->isRunning() &&
       this->beeperFlag)
    {
        this->beeper->start();
    }

    this->emitInsertNewMSG(un,
                           (quint8)pattern.at(6),
                           pattern,
                           true,
                           Alarm_TypeGroupMSG);
}

//Неисправность
void KSMsgManager::analyzer_0x9F_0x40(UnitNode *un,
                                 QByteArray pattern) /*noexcept*/
{
    this->emitInsertNewMSG(un,
                           (quint8)pattern.at(6),
                           pattern,
                           true,
                           Error_TypeGroupMSG);

    un->updateFlagFailure(true);
}

//норма
void KSMsgManager::analyzer_0x9F_0xD0(UnitNode *un,
                                 QByteArray pattern) /*noexcept*/
{
    un->updateFlagFailure(false);


    this->emitInsertNewMSG(un,
                           pattern.at(6),
                           pattern,
                           true,
                           Error_TypeGroupMSG);
}

//Связь норма
void KSMsgManager::analyzer_0x9F_0x20(UnitNode *un,
                                 QByteArray pattern) /*noexcept*/
{
    QByteArray idSlave(pattern.mid(12, 2));
    UnitNode *unSlave(this->possAddUnitNode(idSlave));

	if(nullptr == un ||
       nullptr == unSlave)
        return;

    unSlave->updateNumberArea((quint8)pattern.at(14));
    unSlave->updateNumberNode((quint8)pattern.at(15));
    unSlave->updateFlagConnection(true);

    this->involvingInStructure(un,
                               unSlave);

    this->emitInsertNewMSG(unSlave,
                           (quint8)pattern.at(6),
                           pattern,
                           true,
                           Connect_TypeGroupMSG);

}

void KSMsgManager::analyzer_0x9F_0x28(UnitNode *un,
                                 QByteArray pattern) /*noexcept*/
{
    un->updateAnxiety((quint8)pattern.at(12));

    un->updateNumberKit((quint8)pattern.at(13));

    this->emitInsertNewMSG(un,
                           (quint8)pattern.at(6),
                           pattern,
                           true,
                           Other_TypeGroupMSG,
                           true);
}

//Состояние
void KSMsgManager::analyzer_0x9F_0x18(UnitNode *un,
                                 QByteArray pattern) /*noexcept*/
{

    un->updateFlagSelfD(((quint8)0x00 != ((quint8)0x80 & (quint8)pattern.at(12))) ? (quint8)0 : (quint8)1);
    un->updateFlagOtherD1(((quint8)0x00 != ((quint8)0x40 & (quint8)pattern.at(12))) ? (quint8)0 : (quint8)1);
    un->updateFlagOtherD2(((quint8)0x00 != ((quint8)0x20 & (quint8)pattern.at(12))) ? (quint8)0 : (quint8)1);

    un->updateRssi((quint8)pattern.at(13));

    for(int i(0), countVK(un->listSettingVK.size()); i < countVK; i++)
    {
        un->listSettingVK[i].modeVK = (quint8)pattern.at(14 + i);
        un->listSettingVK[i].flag_alarm =
        un->listSettingVK[i].flag_attention =
        un->listSettingVK[i].flag_failure =
        un->listSettingVK[i].flag_off = false;
        un->listSettingVK[i].flag_connection = true;

        switch((quint8)pattern.at(14 + i))
        {
            case (quint8)0x00:
            {
                un->listSettingVK[i].flag_off = true;
                un->listSettingVK[i].flag_connection = false;
                break;
            }
            case (quint8)0x01:
            {
                un->listSettingVK[i].flag_off = false;
                un->listSettingVK[i].flag_connection = true;
                break;
            }
            case (quint8)0x04:
            {
                un->listSettingVK[i].flag_off = false;
                un->listSettingVK[i].flag_connection = true;
                un->listSettingVK[i].flag_failure = true;
                break;
            }
            case (quint8)0x55:
            {
                break;
            }
            default:
            {
                un->listSettingVK[i].flag_off = false;
                un->listSettingVK[i].flag_connection = true;
                un->listSettingVK[i].flag_failure = false;

                if((quint8)0x01 == ((quint8)pattern.at(14 + i) & 0x0F) ||
                   (quint8)0x02 == ((quint8)pattern.at(14 + i) & 0x0F) ||
                   (quint8)0x03 == ((quint8)pattern.at(14 + i) & 0x0F))
                {
                    un->listSettingVK[i].modeVK = (quint8)pattern.at(14 + i);
                }

                quint8 modeDN(((quint8)pattern.at(14 + i) / (quint8)0x10) & (quint8)0x0F);
                un->updateModeDayNight(i + 1, modeDN);


                break;
            }
        }
    }

    this->emitInsertNewMSG(un,
                           (quint8)pattern.at(6),
                           pattern,
                           true,
                           Connect_TypeGroupMSG);
}

//Обрыв связи
void KSMsgManager::analyzer_0x9F_0xC0(UnitNode *un,
                                      QByteArray pattern) /*noexcept*/
{
    QByteArray idSlave(pattern.mid(12, 2));
    UnitNode *unSlave(this->possAddUnitNode(idSlave));

    if(nullptr == unSlave ||
       nullptr == un)
        return;

    this->extractionFromStructure(unSlave);

    unSlave->updateNumberArea(pattern.at(14));
    unSlave->updateNumberNode(pattern.at(15));
    unSlave->updateFlagConnection(false);

    this->emitInsertNewMSG(unSlave,
                           (quint8)pattern.at(6),
                           pattern,
                           true,
                           Connect_TypeGroupMSG);
}

//Сообщение о выполненной команде
void KSMsgManager::analyzer_0x9F_0xCC(UnitNode *un,
                                 QByteArray pattern) /*noexcept*/
{
    this->emitInsertNewMSG(un,
                           pattern.at(6),
                           pattern,
                           true,
                           Command_TypeGroupMSG);

    switch((quint8)pattern.at(12))
    {
        case (quint8)0x01:
        {
            QByteArray selfD,
                       otherD1,
                       otherD2;
            selfD.append((char)0x00);
            selfD.append((quint8)0x0B);
            otherD1.append((char)0x00);
            otherD1.append((quint8)0x01);
            otherD2.append((char)0x00);
            otherD2.append((quint8)0x02);
            if(selfD == pattern.mid(13, 2))
            {
                un->updateFlagSelfD(0);
            }
            if(otherD1 == pattern.mid(13, 2))
            {
                un->updateFlagOtherD1(0);
            }
            if(otherD2 == pattern.mid(13, 2))
            {
                un->updateFlagOtherD2(0);
            }
            break;
        };
        case (quint8)0x02:
        {
            QByteArray selfD,
                       otherD1,
                       otherD2;
            selfD.append((char)0x00);
            selfD.append((quint8)0x0B);
            otherD1.append((char)0x00);
            otherD1.append((quint8)0x01);
            otherD2.append((char)0x00);
            otherD2.append((quint8)0x02);
            if(selfD == pattern.mid(13, 2))
            {
                un->updateFlagSelfD(1);
            }
            if(otherD1 == pattern.mid(13, 2))
            {
                un->updateFlagOtherD1(1);
            }
            if(otherD2 == pattern.mid(13, 2))
            {
                un->updateFlagOtherD2(1);
            }
            break;
        };
        case (quint8)0x03:
        {
            un->updateNumberKit(pattern.at(13));
            break;
        };
        case (quint8)0x42:
        {
            if(1 > un->listSettingVK.size())
            {
                break;
            }
            un->listSettingVK[0].modeVK = (quint8)pattern.at(13);
            un->listSettingVK[0].timeout1 = (quint8)pattern.at(14);
            un->listSettingVK[0].timeout2 = (quint8)pattern.at(15);
            un->listSettingVK[0].timeout3 = (quint8)pattern.at(16);
            break;
        };
        case (quint8)0x43:
        {
            if(1 > un->listSettingVK.size())
            {
                break;
            }
            un->listSettingVK[1].modeVK = (quint8)pattern.at(13);
            un->listSettingVK[1].timeout1 = (quint8)pattern.at(14);
            un->listSettingVK[1].timeout2 = (quint8)pattern.at(15);
            un->listSettingVK[1].timeout3 = (quint8)pattern.at(16);
            break;
        };
        case (quint8)0x46:
        {
            un->clearListIDBSK(pattern.at(13));

//            this->request_0x41(un,
//                               0x45,
//                               pattern.at(13));
            break;
        };
        case (quint8)0x47:
        {
            un->updateListIDBSK(pattern.at(13),
                                pattern.at(14) + 1,
                                pattern.mid(15, 2));

//            this->request_0x41(un,
//                               0x45,
//                               pattern.at(13));

            break;
        };
        case (quint8)0x48:
        {
            un->removeListIDBSK(pattern.at(13),
                                pattern.at(14) + 1,
                                pattern.mid(15, 2));

//            this->request_0x41(un,
//                               0x45,
//                               pattern.at(13));
            break;
        };
        case (quint8)0x78:
        {
            un->updateNumberArea(pattern.at(13));
            un->updateNumberNode(pattern.at(14));
            break;
        };
        default:
        {
            break;
        };
    }

    un->updateVoltage(pattern.at(17));
    un->updateFlagConnection(true);
}

//Команда на РМ-ТВ не дошла
void KSMsgManager::analyzer_0x9F_0x44(UnitNode *un,
                                 QByteArray pattern) /*noexcept*/
{
    this->emitInsertNewMSG(un,
                           (quint8)pattern.at(6),
                           pattern,
                           true,
                           Error_TypeGroupMSG,
                           true);
}

//Привязка к ВК
void KSMsgManager::analyzer_0x9F_0x45(UnitNode *un,
                                 QByteArray pattern) /*noexcept*/
{
    quint8 numVK((quint8)pattern.at(12)),
           indexBSK((quint8)pattern.at(13));
    if(0x01 != numVK &&
       0x02 != numVK)
        return;

    if((quint8)0x14 < indexBSK ||
       (quint8)0x01 > indexBSK)
        return;

    QByteArray idBSK;
    idBSK.append(pattern.mid(14, 2));

    un->updateListIDBSK(numVK,
                        indexBSK,
                        idBSK);

    if((quint8)0x14 == indexBSK)
    {
        this->emitInsertNewMSG(un,
                               (quint8)pattern.at(6),
                               pattern,
                               true,
                               Command_TypeGroupMSG);
    }
}


void KSMsgManager::analyzer_0x9F_0x49(UnitNode *un,
                                 QByteArray pattern) /*noexcept*/
{    

    quint8 modeDN((quint8)pattern.at(15));
    switch(modeDN)
    {
        case (quint8)0x00:
        {
            un->updateModeDayNight((quint8)pattern.at(12), 0x03);
            break;
        }
        case (quint8)0xEE:
        {
            un->updateModeDayNight((quint8)pattern.at(12), 0x0E);
            break;
        }
        default:
        {
            break;
        }
    }

    this->emitInsertNewMSG(un,
                           (quint8)pattern.at(6),
                           pattern,
                           true,
                           Command_TypeGroupMSG,
                           true);
}

void KSMsgManager::analyzer_0x9F_0x51(UnitNode *un,
                                      QByteArray pattern) /*noexcept*/
{
    this->emitInsertNewMSG(un,
                           (quint8)pattern.at(6),
                           pattern,
                           true,
                           Other_TypeGroupMSG,
                           true);
}


void KSMsgManager::analyzer_0x9F_0x58(UnitNode *un,
                                      QByteArray pattern) /*noexcept*/
{
    this->emitInsertNewMSG(un,
                           (quint8)pattern.at(6),
                           pattern,
                           true,
                           Error_TypeGroupMSG,
                           true);
}

//Проведена инсталляция ВК
void KSMsgManager::analyzer_0x9F_0x1F(UnitNode *un,
                                 QByteArray pattern) /*noexcept*/
{
    quint8 numVK((quint8)pattern.at(12)),
           modeVK((quint8)pattern.at(13)),
           t1((quint8)pattern.at(14)),
           t2((quint8)pattern.at(15)),
           t3((quint8)pattern.at(16));
		   
    if(1 != numVK &&
       2 != numVK)
        return;

    if(numVK > un->listSettingVK.size())
        return;

    un->updateFlagFailure(false);

    numVK--;
    un->listSettingVK[numVK].modeVK = modeVK;
    un->listSettingVK[numVK].timeout1 = t1;
    un->listSettingVK[numVK].timeout2 = t2;
    un->listSettingVK[numVK].timeout3 = t3;

    this->emitInsertNewMSG(un,
                           (quint8)pattern.at(6),
                           pattern,
                           true,
                           Command_TypeGroupMSG);
}



void KSMsgManager::analyzer_0x9F_0x1D(UnitNode *un,
                                 QByteArray pattern) /*noexcept*/
{    


    QByteArray idMaster(pattern.mid(12, 2));
    UnitNode *unMaster(this->possAddUnitNode(idMaster));
    unMaster->updateFlagConnection(true);
    un->updateRssi((quint8)pattern.at(14));

    this->involvingInStructure(unMaster,
                               un);

    this->emitInsertNewMSG(un,
                           (quint8)pattern.at(6),
                           pattern,
                           true,
                           Connect_TypeGroupMSG,
                           true);
}

//новый ведущий
void KSMsgManager::analyzer_0x9F_0x19(UnitNode *un,
                                 QByteArray pattern) /*noexcept*/
{
    QByteArray idMaster(pattern.mid(12, 2));
    UnitNode *unMaster(this->possAddUnitNode(idMaster));
    if(nullptr == un ||
       nullptr == unMaster)
        return;

    un->updateRssi((quint8)pattern.at(14));

    unMaster->updateFlagConnection(true);

    this->involvingInStructure(unMaster,
                               un);

    this->emitInsertNewMSG(un,
                           (quint8)pattern.at(6),
                           pattern,
                           true,
                           Connect_TypeGroupMSG,
                           true);

}

//Неверные параметры команды
void KSMsgManager::analyzer_0x9F_0xEF(UnitNode *un,
                                 QByteArray pattern) /*noexcept*/
{
    this->emitInsertNewMSG(un,
                           pattern.at(6),
                           pattern,
                           true,
                           Command_TypeGroupMSG);
}

void KSMsgManager::analyzer_0x14(UnitNode *un,
                                 QByteArray pattern) /*noexcept*/
{
    if(!un)
    {
#ifdef QT_DEBUG
        qDebug() << "                                 [command    14] [un(" << nullptr << "]";
#endif
        return;
    }

//    // Проверка типа устройства
//    if((quint8)TypeUnitNodeDevice::VK09_KSDeviceType != (quint8)un->typeUN &&
//       (quint8)TypeUnitNodeDevice::VK0A_KSDeviceType != (quint8)un->typeUN &&
//       (quint8)TypeUnitNodeDevice::VK0B_KSDeviceType != (quint8)un->typeUN)
//    {
//#ifdef QT_DEBUG
//        qDebug() << "                                 [command    14] [wrong type UN(" << un->typeUN << ")]";
//#endif
//        return;
//    }

    // Проверка номера камеры
    quint8 numVK((quint8)pattern.at(9));
    if(1 != numVK &&
       2 != numVK)
    {
#ifdef QT_DEBUG
        qDebug() << "                                 [command    14] [numVK(" << numVK << ") out of rang{1;2}]";
#endif
        return;
    }

    // Проверка номера кадра
    quint8 numShot((quint8)pattern.at(8) & (quint8)0x0F);
    if(1 != numShot &&
       2 != numShot &&
       3 != numShot &&
       4 != numShot &&
       5 != numShot)
    {
#ifdef QT_DEBUG
        qDebug() << "                                 [command    14] [numShot(" << numShot << ") out of rang{1;5}]";
#endif
        return;
    }

    quint8 numTrain(((quint8)pattern.at(8) / (quint8)0x10) & (quint8)0x0F);

    quint32 numBlock((quint32)((((quint32)pattern.at(10) * (quint32)0x00000100) & (quint32)0x0000FF00) + ((quint32)pattern.at(11) & (quint32)0x000000FF)) & (quint32)0x0000FFFF),
            blockCount((quint32)((((quint32)pattern.at(12) * (quint32)0x00000100) & (quint32)0x0000FF00) + ((quint32)pattern.at(13) & (quint32)0x000000FF)) & (quint32)0x0000FFFF);

    if((quint32)0 == numBlock ||
       numBlock > blockCount)//d3
    {
#ifdef QT_DEBUG
        qDebug() << "                                 [command    14] [numBlock(" << numBlock << ") out of rang{1;" << blockCount << "}]";
#endif
        return;
    }

    QByteArray piece(pattern);//кусок кадра
    piece.remove(0, 16);
    piece.chop(1);

//    quint32 possReleasSize(blockCount *  310);
//    quint32 blockSize((quint32)piece.size());

#ifdef QT_DEBUG
    qDebug() << "                                 [command    14] [VK(" << numVK
                                                             << ") train(" << numTrain
                                                             << ") shot(" << numShot
                                                             << ") block(" << numBlock << "/" << blockCount << ")]";
#endif

    // проверка наличия IMGCollectorCell
    bool necessaryAddIMGCell(true);
    for(int i(0), n(un->listIMGCollectorCell.size()); i < n; i++)
    {
        if(un->listIMGCollectorCell.at(i).numVK == numVK &&
           un->listIMGCollectorCell.at(i).numTrain == numTrain &&
           un->listIMGCollectorCell.at(i).numShot == numShot)
        {
            necessaryAddIMGCell = false;
            break;
        }
    }
    if(necessaryAddIMGCell)
    {
        this->analyzer_0x9F_0xC1_emit(un,
                                      numVK,
                                      numTrain,
                                      numShot,
                                      blockCount);
    }

    // вставим кусок на его место
    for(int i(0), n(un->listIMGCollectorCell.size()); i < n; i++)
    {
        if(un->listIMGCollectorCell.at(i).numVK == numVK &&
           un->listIMGCollectorCell.at(i).numTrain == numTrain &&
           un->listIMGCollectorCell.at(i).numShot == numShot)
        {
            PieceIMG newPiece;
            newPiece.begin = (quint32)310 * (numBlock - 1);
            newPiece.end = newPiece.begin + piece.size();
            newPiece.piece = piece;

            int missCount((int)numBlock - un->listIMGCollectorCell.at(i).listPiece.size());

            if(0 < missCount)
            {
                // список кусков надо увеличить
                PieceIMG empetyPiece;
                for(int j(0); j < (missCount - 1); j++)
                {
                    // добавим пустые кусочки
                    un->listIMGCollectorCell[i].listPiece.append(empetyPiece);
                }

                un->listIMGCollectorCell[i].listPiece.append(newPiece);
                // общий размер считать не будем возьмём конец последнего добавленного
                // можно проебаться если есть пустые кусочки
                un->listIMGCollectorCell[i].currentSize = newPiece.end;
            }
            else
            {
                // перетрём уже имеющийся кусочек
                un->listIMGCollectorCell[i].listPiece[numBlock - 1] = newPiece;
            }

            if(numBlock == blockCount)//принят последний блок пора собирать кадр
            {
                un->listIMGCollectorCell[i].listPiece = un->listIMGCollectorCell[i].listPiece.mid(0, (int)blockCount);
                QByteArray releaseIMG;
                for(int j(0), m(un->listIMGCollectorCell.at(i).listPiece.size()); j < m; j++)
                {
                    if(un->listIMGCollectorCell.at(i).listPiece.at(j).piece.isEmpty())
                    {
#ifdef QT_DEBUG
                        qDebug() << "                                 [command    14] [missing block #" << j << "]";
#endif
                    }
                    else
                    {
                        releaseIMG.append(un->listIMGCollectorCell.at(i).listPiece.at(j).piece);
                    }
                }
                releaseIMG.resize(un->listIMGCollectorCell.at(i).releaseSize);
                un->listIMGCollectorCell[i].releaseIMG = releaseIMG;
#ifdef QT_DEBUG
                qDebug() << "                                 [command    14] [collected size " << releaseIMG.size() << " / " << (int)un->listIMGCollectorCell.at(i).releaseSize << "]";
#endif
                un->listIMGCollectorCell[i].doneStatus = true;

                if(this->dbManager->addNewIMGPack(un))
                {
                    QString fname(QDir::current().currentPath());
                    fname.append("/img/");
                    if(QDir().exists(fname))
                    {
                        fname.append(QDateTime::currentDateTime().toString("dd_MM_yy [hh_mm_ss_zzz]") + " KS UN " + un->id.toHex() + " nVK%1 nT%2 nS%3.jpg");
                        fname = fname.arg(numVK + 1)
                                     .arg(numTrain)
                                     .arg(numShot);
                        QFile f_ou(fname);
                        f_ou.open(QIODevice::WriteOnly);
                        f_ou.write(releaseIMG);
                        f_ou.close();
#ifdef QT_DEBUG
                        qDebug() << "SAVE: " << fname;
#endif
                    }
//numvk +1 было
                    this->analyzer_0x9F_0xC5_emit(un,
                                                  numVK,
                                                  numTrain,
                                                  numShot);
                }

                emit this->updateProgressShotLoad(un,
                                                  numShot,
                                                  numTrain,
                                                  0);
            }
            else
            {
                emit this->updateProgressShotLoad(un,
                                                  numShot,
                                                  numTrain,
                                                  (numBlock * 100) / blockCount);
            }

#ifdef QT_DEBUG
            qDebug() << "                                 [command    14] [commit]";
#endif
            break;
        }
    }

    return;
}

void KSMsgManager::analyzer_0x24(UnitNode *un,
                              QByteArray pattern) /*noexcept*/
{
    if(!un)
    {
#ifdef QT_DEBUG
        qDebug() << "                                 [command    24] [un(" << nullptr << "]";
#endif
        return;
    }

//    // Проверка типа устройства
//    if((quint8)TypeUnitNodeDevice::VK09_KSDeviceType != (quint8)un->typeUN &&
//       (quint8)TypeUnitNodeDevice::VK0A_KSDeviceType != (quint8)un->typeUN &&
//       (quint8)TypeUnitNodeDevice::VK0B_KSDeviceType != (quint8)un->typeUN)
//    {
//#ifdef QT_DEBUG
//        qDebug() << "                                 [command    24] [wrong type UN(" << un->typeUN << ")]";
//#endif
//        return;
//    }

    // Проверка номера камеры
    quint8 numVK((quint8)pattern.at(9));
    if(1 != numVK &&
       2 != numVK)
    {
#ifdef QT_DEBUG
        qDebug() << "                                 [command    24] [numVK(" << numVK << ") out of rang{1;2}]";
#endif
        return;
    }

    // Проверка номера кадра
    quint8 numShot((quint8)pattern.at(8) & (quint8)0x0F);
    if(1 != numShot &&
       2 != numShot &&
       3 != numShot &&
       4 != numShot &&
       5 != numShot)
    {
#ifdef QT_DEBUG
        qDebug() << "                                 [command    24] [numShot(" << numShot << ") out of rang{1;5}]";
#endif
        return;
    }

    quint8 numTrain(((quint8)pattern.at(8) / (quint8)0x10) & (quint8)0x0F);

    quint32 numBlock((quint32)((((quint32)pattern.at(10) * (quint32)0x00000100) & (quint32)0x0000FF00) + ((quint32)pattern.at(11) & (quint32)0x000000FF)) & (quint32)0x0000FFFF),
            blockCount((quint32)((((quint32)pattern.at(12) * (quint32)0x00000100) & (quint32)0x0000FF00) + ((quint32)pattern.at(13) & (quint32)0x000000FF)) & (quint32)0x0000FFFF);

    if((quint32)0 == numBlock ||
       numBlock > blockCount)
    {
#ifdef QT_DEBUG
        qDebug() << "                                 [command    24] [numBlock(" << numBlock << ") out of rang{1;" << blockCount << "}]";
#endif
        return;
    }

    QByteArray piece(pattern);//кусок кадра
    piece.remove(0, 16);
    piece.chop(1);

//    int possReleasSize(blockCount *  310);
//    quint32 blockSize((quint32)piece.size());

#ifdef QT_DEBUG
    qDebug() << "                                 [command    24] [VK(" << numVK
                                                             << ") train(" << numTrain
                                                             << ") shot(" << numShot
                                                             << ") block(" << numBlock << "/" << blockCount << ")]";
#endif

    // проверка наличия IMGCollectorCell
    bool necessaryAddIMGCell(true);
    for(int i(0), n(un->listIMGCollectorCell.size()); i < n; i++)
    {
        if(un->listIMGCollectorCell.at(i).numVK == numVK &&
           un->listIMGCollectorCell.at(i).numTrain == numTrain &&
           un->listIMGCollectorCell.at(i).numShot == numShot)
        {
            necessaryAddIMGCell = false;
            break;
        }
    }
    if(necessaryAddIMGCell)
    {
        this->analyzer_0x9F_0xC1_emit(un,
                                      numVK,
                                      numTrain,
                                      numShot,
                                      blockCount);
    }

    // вставим кусок на его место
    for(int i(0), n(un->listIMGCollectorCell.size()); i < n; i++)
    {
        if(un->listIMGCollectorCell.at(i).numVK == numVK &&
           un->listIMGCollectorCell.at(i).numTrain == numTrain &&
           un->listIMGCollectorCell.at(i).numShot == numShot)
        {
            PieceIMG newPiece;
            newPiece.begin = 310 * (numBlock - 1);
            newPiece.end = newPiece.begin + piece.size();
            newPiece.piece = piece;

            int missCount((int)numBlock - un->listIMGCollectorCell.at(i).listPiece.size());

            if(0 < missCount)
            {
                // список кусков надо увеличить
                PieceIMG empetyPiece;
                for(int j(0); j < (missCount - 1); j++)
                {
                    // добавим пустые кусочки
                    un->listIMGCollectorCell[i].listPiece.append(empetyPiece);
                }

                un->listIMGCollectorCell[i].listPiece.append(newPiece);
                // общий размер считать не будем возьмём конец последнего добавленного
                // можно проебаться если есть пустые кусочки
                un->listIMGCollectorCell[i].currentSize = newPiece.end;
            }
            else
            {
                // перетрём уже имеющийся кусочек
                un->listIMGCollectorCell[i].listPiece[numBlock - 1] = newPiece;
            }

            if(numBlock == blockCount)//принят последний блок пора собирать кадр
            {
                un->listIMGCollectorCell[i].listPiece = un->listIMGCollectorCell[i].listPiece.mid(0, (int)blockCount);
                QByteArray releaseIMG;
                for(int j(0), m(un->listIMGCollectorCell.at(i).listPiece.size()); j < m; j++)
                {
                    if(un->listIMGCollectorCell.at(i).listPiece.at(j).piece.isEmpty())
                    {
#ifdef QT_DEBUG
                        qDebug() << "                                 [command    24] [missing block #" << j << "]";
#endif
                    }
                    else
                    {
                        releaseIMG.append(un->listIMGCollectorCell.at(i).listPiece.at(j).piece);
                    }
                }
                releaseIMG.resize(un->listIMGCollectorCell.at(i).releaseSize);
                un->listIMGCollectorCell[i].releaseIMG = releaseIMG;
#ifdef QT_DEBUG
                qDebug() << "                                 [command    24] [collected size " << releaseIMG.size() << " / " << (int)un->listIMGCollectorCell.at(i).releaseSize << "]";
#endif
                un->listIMGCollectorCell[i].doneStatus = true;

                if(this->dbManager->addNewIMGPack(un))
                {
                    QString fname(QDir::current().currentPath());
                    fname.append("/img/");
                    if(QDir().exists(fname))
                    {
                        fname.append(QDateTime::currentDateTime().toString("dd_MM_yy [hh_mm_ss_zzz]") + " KS UN " + un->id.toHex() + " nVK%1 nT%2 nS%3.jpg");
                        fname = fname.arg(numVK + 1)
                                     .arg(numTrain)
                                     .arg(numShot);
                        QFile f_ou(fname);
                        f_ou.open(QIODevice::WriteOnly);
                        f_ou.write(releaseIMG);
                        f_ou.close();
#ifdef QT_DEBUG
                        qDebug() << "SAVE: " << fname;
#endif
                    }
//numvk+1 было
                    this->analyzer_0x9F_0xC5_emit(un,
                                                  numVK,
                                                  numTrain,
                                                  numShot);
                }

                emit this->updateProgressShotLoad(un,
                                                  numShot,
                                                  numTrain,
                                                  0);
            }
            else
            {
                emit this->updateProgressShotLoad(un,
                                                  numShot,
                                                  numTrain,
                                                  (numBlock * 100) / blockCount);
            }

#ifdef QT_DEBUG
            qDebug() << "                                 [command    24] [commit]";
#endif
            break;
        }
    }

    return;
}

void KSMsgManager::request_0x41(UnitNode *un,
                                quint8 cmdCod,
                                quint8 d1,
                                quint8 d2,
                                quint8 d3,
                                quint8 d4) /*noexcept*/
{


    QByteArray msg41;
    msg41.append((quint8)0xA5);
    msg41.append((quint8)0x0A);
    msg41.append((quint8)0x08);
//    msg41.append((char)0x00);
    msg41.append((quint8)(0x00 + (m_numMSG * 0x10)));
    msg41.append((quint8)0x07);
    msg41.append((quint8)0x41);
    if((quint8)0x0F == cmdCod &&
       (quint8)CPP_KSDeviceType == (quint8)un->_typeUN())
    {
        msg41.append((char)0x00);
        msg41.append((char)0x00);
    }
    else
    {
        msg41.append(un->id);
    }
    msg41.append(cmdCod);
    msg41.append(d1);
    msg41.append(d2);
    msg41.append(d3);
    msg41.append(d4);
    msg41.append((quint8)0xFF);
    msg41 = this->setCRC8(msg41);

    un->lastMSGToSend = msg41;
    this->lastUNToSend = un;
    this->lastMSGToSend = msg41;

//    emitInsertNewMSG(un,
//                     0x41,
//                     msg41,
//                     false,
//                     Command_TypeGroupMSG);
#ifdef QT_DEBUG
    qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " KsMSG  OUTPUT >> [command    41] [" << msg41.toHex() << "]";
#endif
    msg41 = this->setByteStuffing(msg41);
    msg41 = this->setPreambula(msg41);

    emitNewMSGSend(msg41);
    m_numMSG = (m_numMSG + 1) % 0x10;
}


void KSMsgManager::emitAccept0x0A(UnitNode *un) /*noexcept*/
{
    emit this->accept0x0A(un);
}

void KSMsgManager::emitAddNewUN(UnitNode *un) /*noexcept*/
{
    emit this->addNewUN(un);
}

void KSMsgManager::emitUpdateUN(UnitNode *un) /*noexcept*/
{
    emit this->updateUN(un);
}

quint32 KSMsgManager::emitInsertNewMSG(UnitNode *unSender,
                                       quint8 codeMSG,
                                       QByteArray msg,
                                       bool ioType,
                                       TypeGroupMSG typeGroup,
                                       bool flagHidden) /*noexcept*/
{
    quint32 indexDBMsg(0);
    indexDBMsg = this->dbManager->addNewMSG(unSender,
                                            codeMSG,
                                            msg,
                                            ioType,
                                            KSSystemType,
                                            typeGroup,
                                            flagHidden);
    return indexDBMsg;
}

void KSMsgManager::emitNewMSGSend(QByteArray msg) /*noexcept*/
{
    emit this->newMSGSend(msg);
}

void KSMsgManager::emitTicPattern()
{
    emit this->ticPattern();
}


