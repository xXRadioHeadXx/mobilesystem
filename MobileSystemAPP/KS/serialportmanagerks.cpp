#include <serialportmanagerks.h>

SerialPortManagerKS::SerialPortManagerKS(KSMsgManager *ksMsg,
                                         QMutex *mutexPattern,
                                         QMutex *mutexRequest,
                                         QMutex *mutexMSGAnalysis,
                                         QObject *parent) /*noexcept*/ :
    QObject(parent),
    ksMsgManager(ksMsg)
{
    try
    {
        m_mutexPattern = mutexPattern;
        m_mutexRequest = mutexRequest;
        m_mutexMSGAnalysis = mutexMSGAnalysis;

        lastNumPattern = 0xFF;
        lastCodePattern = 0xFF;
        this->m_port = nullptr;
        prevousThreadAnalysis = nullptr;
        prevousThreadReader = nullptr;
        this->m_ByteCollector = new QByteArray;
        listDataRequest_0x2D = new QList<QByteArray>;
        passClearListDataRequest_0x2D = new bool;
        *passClearListDataRequest_0x2D = false;

        QByteArray ba;
        ba.append(0xFF);
        QVariant vba(ba);

        this->m_listPattern = new COSVariantList;
        this->m_listPattern->append(vba);
        connect(this->m_listPattern,
                SIGNAL(addedElenent()),
                this,
                SLOT(acceptPattern()));

        this->m_listRequest = new COSVariantList;
        this->m_listRequest->append(vba);
        /// Внимание(!) этот коннект даёт скорость всему
//        connect(this->m_listRequest,
//                SIGNAL(addedElenent()),
//                this,
//                SLOT(sendRequestMSG()));
        /// но великая сила это ещё и великая ответственность и вылеты

        m_port = new QSerialPort;
        connect(m_port,
                SIGNAL(error(QSerialPort::SerialPortError)),
                this,
                SLOT(errorPort(QSerialPort::SerialPortError)));
        connect(m_port,
                SIGNAL(readyRead()),
                this,
                SLOT(process()));

        this->closePort();
    }
    catch (std::runtime_error &rte)
    {
        qWarning("catch runtime_error %s [SerialPortManagerKS::SerialPortManagerKS]", rte.what());
    }
    catch (std::bad_alloc& ba)
    {
        qWarning("catch bad_alloc %s [SerialPortManagerKS::SerialPortManagerKS]", ba.what());
    }
    catch (std::exception &e)
    {
        qWarning("catch exception %s [SerialPortManagerKS::SerialPortManagerKS]", e.what());
    }
    catch (...)
    {
        qWarning("catch exception <unknown> [SerialPortManagerKS::SerialPortManagerKS]");
    }
}

SerialPortManagerKS::~SerialPortManagerKS() /*noexcept*/
{
    try
    {
        this->closePort();

        disconnect(this->m_listPattern,
                SIGNAL(addedElenent()),
                this,
                SLOT(acceptPattern()));
        disconnect(this->m_listRequest,
                SIGNAL(addedElenent()),
                this,
                SLOT(sendRequestMSG()));
        this->m_listPattern->clear();
        this->m_listRequest->clear();


        if(prevousThreadReader)
        {
            prevousThreadReader->terminate();
            prevousThreadReader->wait();
            delete prevousThreadReader;
            prevousThreadReader = nullptr;
        }

        if(prevousThreadAnalysis)
        {
            prevousThreadAnalysis->terminate();
            prevousThreadAnalysis->wait();
            delete prevousThreadAnalysis;
            prevousThreadAnalysis = nullptr;
        }

        delete m_listPattern;
        delete m_listRequest;
        m_listPattern = nullptr;
        m_listRequest = nullptr;

        delete m_port;
        delete m_ByteCollector;
    }
    catch(...)
    {
#ifdef QT_DEBUG
        qDebug() << "Assert(~SerialPortManagerKS)";
#endif
        return;
    }
}

void SerialPortManagerKS::errorPort(QSerialPort::SerialPortError error) /*noexcept*/
{
#ifdef QT_DEBUG
    qDebug() << "SerialPortManagerKS::errorPort(" << error << m_port->errorString() << ")";
#endif
    return;
    switch(error)
    {
        case QSerialPort::NoError:
        {
            break;
        };
        case QSerialPort::DeviceNotFoundError:
        {
            break;
        };
        case QSerialPort::PermissionError:
        {
            break;
        };
        case QSerialPort::OpenError:
        {
            break;
        };
        case QSerialPort::NotOpenError:
        {
            break;
        };
        case QSerialPort::ParityError:
        {
            break;
        };
        case QSerialPort::FramingError:
        {
            break;
        };
        case QSerialPort::BreakConditionError:
        {
            break;
        };
        case QSerialPort::WriteError:
        {
            this->reopenPort();
            break;
        };
        case QSerialPort::ReadError:
        {
            this->reopenPort();
            break;
        };
        case QSerialPort::ResourceError:
        {
            this->reopenPort();
            break;
        };
        case QSerialPort::UnsupportedOperationError:
        {
            break;
        };
        case QSerialPort::TimeoutError:
        {
            this->reopenPort();
            break;
        };
        case QSerialPort::UnknownError:
        {
            this->reopenPort();
            break;
        };
        default:
        {
            break;
        };
    }
}


bool SerialPortManagerKS::reopenPort() /*noexcept*/
{
    if(m_portName != this->m_port->portName())
        return false;

    this->closePort();

    this->m_port->setPortName(m_portName);

    this->m_port->open(QSerialPort::ReadWrite);


    if(this->m_port->isOpen())
    {
        this->m_port->setBaudRate(m_rate);
        this->m_port->setDataBits(m_dataBit);
        this->m_port->setParity(m_parity);
        this->m_port->setStopBits(m_stopBit);
        this->m_port->setFlowControl(m_flow);
#ifdef QT_DEBUG
        qDebug() << "";
        qDebug() << "";
        qDebug() << "SerialPort " << m_portName << " isOpen(TRUE)";

        qDebug() << "= Current parameters =";
//        qDebug() << "Device name            : " << m_port->portName();
        qDebug() << "Device name            : " << m_port->portName();
        qDebug() << "Baud rate              : " << m_port->baudRate();
        qDebug() << "Data bits              : " << m_port->dataBits();
        qDebug() << "Parity                 : " << m_port->parity();
        qDebug() << "Stop bits              : " << m_port->stopBits();
        qDebug() << "Flow                   : " << m_port->flowControl();
//        this->m_port->setReadBufferSize(262144);
//        qDebug() << "BufferSize             : " << m_port->readBufferSize();
        qDebug() << "";
        qDebug() << "";
#endif



        this->m_ByteCollector->clear();
        this->emitPortCondition();

        return this->m_stopFlag = true;
    }
    else
    {
#ifdef QT_DEBUG
        qDebug() << "";
        qDebug() << "";
        qDebug() << "";
        qDebug() << "SerialPort " << m_portName << " isOpen(FALSE)";
        qDebug() << "";
#endif
        this->closePort();
        return this->m_stopFlag = false;
    }
}

bool SerialPortManagerKS::openPort(QString portName,
              qint32 rate,
              QSerialPort::DataBits dataBit,
              QSerialPort::StopBits stopBit,
              QSerialPort::FlowControl flow,
              QSerialPort::Parity parity) /*noexcept*/
{
    this->closePort();

    this->m_port->setPortName(m_portName = portName);

    this->m_port->open(QSerialPort::ReadWrite);


    if(this->m_port->isOpen())
    {
        this->m_port->setBaudRate(m_rate = rate);
        this->m_port->setDataBits(m_dataBit = dataBit);
        this->m_port->setParity(m_parity = parity);
        this->m_port->setStopBits(m_stopBit = stopBit);
        this->m_port->setFlowControl(m_flow = flow);
#ifdef QT_DEBUG
        qDebug() << "";
        qDebug() << "";
        qDebug() << "SerialPort " << portName << " isOpen(TRUE)";

        qDebug() << "= Current parameters =";
//        qDebug() << "Device name            : " << m_port->portName();
        qDebug() << "Device name            : " << m_port->portName();
        qDebug() << "Baud rate              : " << m_port->baudRate();
        qDebug() << "Data bits              : " << m_port->dataBits();
        qDebug() << "Parity                 : " << m_port->parity();
        qDebug() << "Stop bits              : " << m_port->stopBits();
        qDebug() << "Flow                   : " << m_port->flowControl();
//        this->m_port->setReadBufferSize(262144);
//        qDebug() << "BufferSize             : " << m_port->readBufferSize();
        qDebug() << "";
        qDebug() << "";
#endif



        this->m_ByteCollector->clear();
        this->emitPortCondition();

        return this->m_stopFlag = true;
    }
    else
    {
#ifdef QT_DEBUG
        qDebug() << "";
        qDebug() << "";
        qDebug() << "";
        qDebug() << "SerialPort " << portName << " isOpen(FALSE)";
        qDebug() << "";
#endif
        this->closePort();
        return this->m_stopFlag = false;
    }
}

void SerialPortManagerKS::process() /*noexcept*/
{
    try
    {
        this->sendRequestMSG();

        if(!this->m_stopFlag)
        {
            return;
        }

    //    this->m_port->waitForReadyRead(500);
        QByteArray newData(this->m_port->readAll());

        ThreadReaderKS *newThreadReader(new ThreadReaderKS(this->m_port,
                                                           this->m_ByteCollector,
                                                           newData,
                                                           this->m_listPattern,
                                                           this->m_listRequest,
                                                           listDataRequest_0x2D,
                                                           passClearListDataRequest_0x2D,
                                                           m_mutexPattern,
                                                           m_mutexRequest/*,
                                                           this*/));
//        QThread *newThreadReader(new ThreadReaderKS(ksMsgManager,
//                                                    this->m_ByteCollector,
//                                                    newData,
//                                                    this->m_listPattern,
//                                                    this->m_listRequest,
//                                                    listDataRequest_0x2D,
//                                                    passClearListDataRequest_0x2D/*,
//                                                    this*/));
        connect(newThreadReader,
                SIGNAL(finished()),
                this,
                SLOT(sendRequestMSG()));

        if(prevousThreadReader)
        {
            connect(newThreadReader,
                    SIGNAL(started()),
                    prevousThreadReader,
                    SLOT(deleteLater()));

            connect(prevousThreadReader,
                    SIGNAL(finished()),
                    newThreadReader,
                    SLOT(start()));

            if(prevousThreadReader->isFinished())
            {
                newThreadReader->start();
            }
            prevousThreadReader = newThreadReader;
        }
        else
        {
            newThreadReader->start();
            prevousThreadReader = newThreadReader;
        }

        this->sendRequestMSG();
    }
    catch (std::runtime_error &rte)
    {
        qWarning("catch runtime_error %s [SerialPortManagerKS::process]", rte.what());
    }
    catch (std::bad_alloc& ba)
    {
        qWarning("catch bad_alloc %s [SerialPortManagerKS::process]", ba.what());
    }
    catch (std::exception &e)
    {
        qWarning("catch exception %s [SerialPortManagerKS::process]", e.what());
    }
    catch (...)
    {
        qWarning("catch exception <unknown> [SerialPortManagerKS::process]");
    }

    return;
}

//void SerialPortManagerKS::sendKSMSG(QByteArray msg)
//{
//    this->sendKSMSG(msg, "");
//}

void SerialPortManagerKS::sendRequestMSG() /*noexcept*/
{
    try
    {
        if(nullptr == this->m_port)
            return;

        if(true == this->m_port->isOpen())
        {
            bool mx_stat(false);
            while(false == mx_stat)
            {
                mx_stat = m_mutexRequest->tryLock(100);
            }

            while(1 < m_listRequest->size())
            {
                QByteArray msg(m_listRequest->value(1).toByteArray());
                m_listRequest->removeAt(1);

                if(!msg.isEmpty())
                {
                    this->m_port->write(msg);
    //                this->m_port->waitForBytesWritten(50);
                }
            }

            if(true == mx_stat)
            {
                m_mutexRequest->unlock();
            }
        }
    }
    catch (std::runtime_error &rte)
    {
        qWarning("catch runtime_error %s [SerialPortManagerKS::sendRequestMSG]", rte.what());
    }
    catch (std::bad_alloc& ba)
    {
        qWarning("catch bad_alloc %s [SerialPortManagerKS::sendRequestMSG]", ba.what());
    }
    catch (std::exception &e)
    {
        qWarning("catch exception %s [SerialPortManagerKS::sendRequestMSG]", e.what());
    }
    catch (...)
    {
        qWarning("catch exception <unknown> [SerialPortManagerKS::sendRequestMSG]");
    }
}


void SerialPortManagerKS::sendKSMSG(QByteArray msg) /*noexcept*/
{
    try
    {
        bool mx_stat(false);
        while(false == mx_stat)
        {
            mx_stat = m_mutexRequest->tryLock(100);
        }

        this->m_listRequest->append(msg);

        if(true == mx_stat)
        {
            m_mutexRequest->unlock();
        }
    }
    catch (std::runtime_error &rte)
    {
        qWarning("catch runtime_error %s [SerialPortManagerKS::sendRequestMSG]", rte.what());
    }
    catch (std::bad_alloc& ba)
    {
        qWarning("catch bad_alloc %s [SerialPortManagerKS::sendRequestMSG]", ba.what());
    }
    catch (std::exception &e)
    {
        qWarning("catch exception %s [SerialPortManagerKS::sendRequestMSG]", e.what());
    }
    catch (...)
    {
        qWarning("catch exception <unknown> [SerialPortManagerKS::sendRequestMSG]");
    }
}

//void SerialPortManagerKS::portTick() /*noexcept*/
//{
//    QByteArray ba;
//    ba.append(0xFF);
//    for(int i(0); i < 30; i++)
//    {
//        this->m_port->write(ba);
//    }
//}

bool SerialPortManagerKS::refreshBuffer() /*noexcept*/
{
    //
    emit this->portCondition(0);
    //
    bool result(false);
    this->m_stopFlag = false;
    result = this->openPort(m_portName,
                            m_rate,
                            m_dataBit,
                            m_stopBit,
                            m_flow,
                            m_parity);

    this->m_ByteCollector->clear();
    this->m_stopFlag = true;
    return result;

}

//void SerialPortManagerKS::analisePattern(QByteArray pattern) /*noexcept*/
//{

////    m_listPattern->m_list.append(QVariant(pattern));
//    m_listPattern->append(QVariant(pattern));
//    acceptPattern();
//}

void SerialPortManagerKS::acceptPattern() /*noexcept*/
{
    try
    {
        this->sendRequestMSG();

        bool mx_stat(false);
        while(false == mx_stat)
        {
            mx_stat = m_mutexPattern->tryLock(100);
        }

        while(1 < m_listPattern->size())
        {
            QByteArray pattern(m_listPattern->value(1).toByteArray());
            m_listPattern->removeAt(1);



            if((quint32)6 <= (quint32)pattern.size())
            {
                quint32 dataSize((quint8)pattern.at(4) + 0x100 * ((quint8)pattern.at(3) & 0x0F));//размер поля данных
                if(((quint32)7 + dataSize) > (quint32)pattern.size())
                {
                    continue;
                }
            }
            else
            {
                continue;
            }

            if((quint8)0xA1 != (quint8)pattern.at(5) &&
               (quint8)0x0A != (quint8)pattern.at(5) &&
               (quint8)0xCE != (quint8)pattern.at(5))
            {
                repeatExcept = false;
                quint8 numMSG((quint8)pattern.at(3) / 0x10),
                       codeMSG((quint8)pattern.at(5));
                if(lastNumPattern == numMSG &&
                   lastCodePattern == codeMSG)
                {
                    repeatExcept = true;
#ifdef QT_DEBUG
                    if((quint8)0x9F != (quint8)pattern.at(5))
                    {
                        qDebug() << "SerialPortManagerKS::acceptPattern RepeatExcept (numMSG[" << (quint8)pattern.at(3) / (quint8)0x10 << "] codeMSG[" << pattern.mid(5, 1).toHex().toUpper() << "])";
                    }
                    else
                    {
                        qDebug() << "SerialPortManagerKS::acceptPattern RepeatExcept (numMSG[" << (quint8)pattern.at(3) / (quint8)0x10 << "] codeMSG[" << pattern.mid(5, 1).toHex().toUpper() << "_" << pattern.mid(6, 1).toHex().toUpper() << "])";
                    }
#endif
                    continue;
                }
                lastNumPattern = numMSG;
                lastCodePattern = codeMSG;
            }

            ThreadAnalysisKS *newThreadAnalysis(new ThreadAnalysisKS(ksMsgManager,
                                                                     pattern,
                                                                     m_mutexMSGAnalysis/*,
                                                                     this*/));
//            QThread *newThreadAnalysis(new ThreadAnalysisKS(ksMsgManager,
//                                                            pattern/*,
//                                                            this*/));

            if(prevousThreadAnalysis)
            {
                connect(newThreadAnalysis,
                        SIGNAL(started()),
                        prevousThreadAnalysis,
                        SLOT(deleteLater()));

                connect(prevousThreadAnalysis,
                        SIGNAL(finished()),
                        newThreadAnalysis,
                        SLOT(start()));

                if(prevousThreadAnalysis->isFinished())
                {
                    newThreadAnalysis->start();
                }

                prevousThreadAnalysis = newThreadAnalysis;
            }
            else
            {
                prevousThreadAnalysis = newThreadAnalysis;
                newThreadAnalysis->start();
            }
        }

        if(true == mx_stat)
        {
            m_mutexPattern->unlock();
        }

        this->sendRequestMSG();
    }
    catch (std::runtime_error &rte)
    {
        qWarning("catch runtime_error %s [SerialPortManagerKS::acceptPattern]", rte.what());
    }
    catch (std::bad_alloc& ba)
    {
        qWarning("catch bad_alloc %s [SerialPortManagerKS::acceptPattern]", ba.what());
    }
    catch (std::exception &e)
    {
        qWarning("catch exception %s [SerialPortManagerKS::acceptPattern]", e.what());
    }
    catch (...)
    {
        qWarning("catch exception <unknown> [SerialPortManagerKS::acceptPattern]");
    }
    return;
    //
}

//void SerialPortManagerKS::clearListThreadAnalysis() /*noexcept*/
//{
//    for(int i(0), n(this->listThreadAnalysis.size()); i < n; i++)
//    {
//        if(!this->listThreadAnalysis.at(i)->isFinished() &&
//           !this->listThreadAnalysis.at(i)->isRunning())
//        {
//            this->listThreadAnalysis.at(i)->start();
//            break;
//        }
//        else
//        {
//            if(this->listThreadAnalysis.at(i)->isRunning())
//            {
//                break;
//            }
//        }
//    }

//    for(int i(0); i < this->listThreadAnalysis.size();)
//    {
//        if(this->listThreadAnalysis.at(i)->isFinished() &&
//           this->listThreadAnalysis.at(i)->isRunning())
//        {
//            delete this->listThreadAnalysis.at(i);
//            this->listThreadAnalysis.removeAt(i);
//        }
//        else
//        {
//            i++;
//        }
//    }

//}

//void SerialPortManagerKS::clearListThreadReader() /*noexcept*/
//{
//    for(int i(0), n(this->listThreadReader.size()); i < n; i++)
//    {
//        if(!this->listThreadReader.at(i)->isFinished() &&
//           !this->listThreadReader.at(i)->isRunning())
//        {
//            this->listThreadReader.at(i)->start();
//            break;
//        }
//        else
//        {
//            if(this->listThreadReader.at(i)->isRunning())
//            {
//                break;
//            }
//        }
//    }

//    for(int i(0); i < this->listThreadReader.size();)
//    {
//        if(this->listThreadReader.at(i)->isFinished() &&
//           this->listThreadReader.at(i)->isRunning())
//        {
//            delete this->listThreadReader.at(i);
//            this->listThreadReader.removeAt(i);
//        }
//        else
//        {
//            i++;
//        }
//    }
//}

void SerialPortManagerKS::closePort() /*noexcept*/
{
    try
    {
    //    timer_portTick->stop();
        this->m_stopFlag = false;
        if(this->m_port)
        {
            if(this->m_port->isOpen())
            {
                this->m_port->close();
            }
            disconnect(m_port,
                    SIGNAL(error(QSerialPort::SerialPortError)),
                    this,
                    SLOT(errorPort(QSerialPort::SerialPortError)));
            disconnect(m_port,
                    SIGNAL(readyRead()),
                    this,
                    SLOT(process()));

            delete this->m_port;
            this->m_port = nullptr;

        }
        this->m_port = new QSerialPort;
        connect(m_port,
                SIGNAL(error(QSerialPort::SerialPortError)),
                this,
                SLOT(errorPort(QSerialPort::SerialPortError)));
        connect(m_port,
                SIGNAL(readyRead()),
                this,
                SLOT(process()));
        this->emitPortCondition();
    }
    catch (std::runtime_error &rte)
    {
        qWarning("catch runtime_error %s [SerialPortManagerKS::closePort]", rte.what());
    }
    catch (std::bad_alloc& ba)
    {
        qWarning("catch bad_alloc %s [SerialPortManagerKS::closePort]", ba.what());
    }
    catch (std::exception &e)
    {
        qWarning("catch exception %s [SerialPortManagerKS::closePort]", e.what());
    }
    catch (...)
    {
        qWarning("catch exception <unknown> [SerialPortManagerKS::closePort]");
    }
}

void SerialPortManagerKS::emitPortCondition() /*noexcept*/
{
    qint32 condition(this->m_port->isOpen() ? 1 : -1);
    emit this->portCondition(condition);
}

bool SerialPortManagerKS::isOpen() /*noexcept*/
{
    if(m_port)
    {
        return m_port->isOpen();
    }
    return false;
}

QString SerialPortManagerKS::portName() /*noexcept*/
{
    return m_port->portName();
}

qint32 SerialPortManagerKS::baudRate() /*noexcept*/
{
    return m_port->baudRate();
}

qint32 SerialPortManagerKS::dataBits() /*noexcept*/
{
    return m_port->dataBits();
}

qint32 SerialPortManagerKS::parity() /*noexcept*/
{
    return m_port->parity();
}

qint32 SerialPortManagerKS::stopBits() /*noexcept*/
{
    return m_port->stopBits();
}
