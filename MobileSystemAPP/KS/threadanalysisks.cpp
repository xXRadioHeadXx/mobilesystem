#include <threadanalysisks.h>

ThreadAnalysisKS::ThreadAnalysisKS(KSMsgManager *ksMsgM,
                                   QByteArray patternMsg,
                                   QMutex *mutexMSGAnalysis,
                                   QObject *parent) /*noexcept*/ :
    QThread(parent),
    ksMsgManager(ksMsgM),
    pattern(patternMsg),
    m_mutexMSGAnalysis(mutexMSGAnalysis)
{
}

ThreadAnalysisKS::~ThreadAnalysisKS() /*noexcept*/
{
    try
    {
        ksMsgManager = nullptr;
        pattern.clear();
    }
    catch(...)
    {
#ifdef QT_DEBUG
        qDebug() << "Assert(~ThreadAnalysisKS)";
#endif
        return;
    }
}

void ThreadAnalysisKS::run() /*noexcept*/
{
    try
    {
//        bool mx_stat(false);
//        while(false == mx_stat)
//        {
//            mx_stat = m_mutexMSGAnalysis->tryLock(100);
//        }

        this->ksMsgManager->newMSG(pattern);

//        if(true == mx_stat)
//        {
//            m_mutexMSGAnalysis->unlock();
//        }
        this->quit();
    }
    catch (std::runtime_error &rte)
    {
        qWarning("catch runtime_error %s [ThreadAnalysisKS::run]", rte.what());
    }
    catch (std::bad_alloc& ba)
    {
        qWarning("catch bad_alloc %s [ThreadAnalysisKS::run]", ba.what());
    }
    catch (std::exception &e)
    {
        qWarning("catch exception %s [ThreadAnalysisKS::run]", e.what());
    }
    catch (...)
    {
        qWarning("catch exception <unknown> [ThreadAnalysisKS::run]");
    }}
