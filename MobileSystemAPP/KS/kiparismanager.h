#ifndef KIPARISMANAGER_H
#define KIPARISMANAGER_H

#include <QObject>
//#include <QThread>

#include <serialportmanagerks.h>
#include <ksmsgmanager.h>

class KiparisManager : public QObject
{
    Q_OBJECT
public:
    QTimer *m_portConnectionTimer;
    bool connectionStatus;
    SerialPortManagerKS *m_portManager;
    DataBaseManager *dbManager;
    KSMsgManager *m_ksMsgManager;
    QList<QTimer *> listTimerOffAlarmCondition;
    QTimer *timetTicPattern;

    QMutex m_mutexPattern;
    QMutex m_mutexRequest;
    QMutex m_mutexMSGAnalysis;

    explicit KiparisManager(DataBaseManager *db,
                            QObject *parent = nullptr) /*noexcept*/;
    virtual ~KiparisManager() /*noexcept*/;

    QList<QString> getPortSetting() /*noexcept*/;
    QList<UnitNode*> getListUnitNode() /*noexcept*/;
    quint32 emitInsertNewMSG(UnitNode *unSender,
                             quint8 codeMSG,
                             QByteArray msg,
                             bool ioType,
                             TypeGroupMSG typeGroup,
                             bool flagHidden = false) /*noexcept*/;
//    quint32 emitInsertNewMSG(quint32 indexDBMsg) /*noexcept*/;
    void cmdRequest_0x41(UnitNode *unSender,
                         quint8 cmdCod,
                         quint8 d1 = 0x00,
                         quint8 d2 = 0x00,
                         quint8 d3 = 0x00,
                         quint8 d4 = 0x00) /*noexcept*/;
    bool reopenPort();

signals:
    void updatePortSetting(QList<QString> listArg);
    void insertNewIMG(quint32 indexImg);
    void insertNewMSG(quint32 indexMsg);
    //добавление в список устройств
    void addNewUN(UnitNode* un);
    //обновление элемента списка устройств
    void updateUN(UnitNode * un);
    //
    void moveUN(UnitNode *objPtr,
                UnitNode *sourceParent,
                int sourceChild,
                UnitNode *destinationParent,
                int destinationChild);
    void portCondition(qint32 condition);
//    void progress14(UnitNode* un,
//                    quint8 numShot,
//                    quint8 numTrain,
//                    quint8 value);
//    void progress24(UnitNode* un,
//                    quint8 numShot,
//                    quint8 numTrain,
//                    quint8 value);
    void updateProgressShotLoad(UnitNode* un,
                                quint8 numShot,
                                quint8 numTrain,
                                quint8 value);
    void updateConnectionStatus(bool status);

public slots:
    void ticPattern();
    void timeoutTicPattern();
    bool openPort(QString portName = "COM1",
                  qint32 rate = 230400,//QSerialPort::BaudRate576000,
                  QSerialPort::DataBits dataBit = QSerialPort::Data8,
                  QSerialPort::StopBits stopBit = QSerialPort::TwoStop,
                  QSerialPort::FlowControl flow = QSerialPort::NoFlowControl,
                  QSerialPort::Parity parity = QSerialPort::NoParity) /*noexcept*/;
    void closePort() /*noexcept*/;

    void portConnectionTimeout();
    void portConnectionTimerStart();

//    void setAlarmCondition(UnitNode *un) /*noexcept*/;
//    void offAlarmCondition() /*noexcept*/;

};

#endif // KIPARISMANAGER_H
