#ifndef THREADREADERKS_H
#define THREADREADERKS_H

#include <QThread>
#include <QSerialPort>
#include <ksmsgmanager.h>
//#include <threadanalysisks.h>
//#include <COSVariantList.h>
#include <cosvariantlist.h>



class ThreadReaderKS : public QThread
{
    Q_OBJECT
public:    
    QSerialPort *m_port;// порт
    KSMsgManager *ksManager;
    QByteArray *m_ByteCollector;
    QByteArray newData;
//    QList<quint8> CrcTable;
    COSVariantList *m_listPattern;
    COSVariantList *m_listRequest;
    QList<QByteArray> *listDataRequest_0x2D;
    bool *passClearListDataRequest_0x2D;

    QMutex *m_mutexPattern;
    QMutex *m_mutexRequest;

    explicit ThreadReaderKS(QSerialPort *port,
                            QByteArray *collector,
                            QByteArray data,
                            COSVariantList *listPattern,
                            COSVariantList *listRequest,
                            QList<QByteArray> *listData_0x2D,
                            bool *pass_0x2D,
                            QMutex *mutexPattern,
                            QMutex *mutexRequest,
                            QObject *parent = nullptr) /*noexcept*/;
    virtual ~ThreadReaderKS() /*noexcept*/;

    void analizMainBuffer() /*noexcept*/;
    void possRequestReciept(QByteArray pattern) /*noexcept*/;
    void request_0xA1() /*noexcept*/;
    void request_0x0D(quint8 code,
                      QByteArray idUN,
                      quint8 numZone,
                      quint8 numArea,
                      quint8 numNode,
                      quint8 d1,
                      quint8 d2,
                      quint8 d3,
                      quint8 d4,
                      quint8 d5,
                      quint8 d6) /*noexcept*/;
    void request_0x1D(QByteArray idUN,
                      quint8 numShot,
                      quint8 numVK,
                      quint8 numBlockH,
                      quint8 numBlockL) /*noexcept*/;
    void request_0x2D() /*noexcept*/;
    QByteArray setCRC(QByteArray msg) /*noexcept*/;
    QByteArray setByteStaffing(QByteArray msg) /*noexcept*/;
    QByteArray setPreambula(QByteArray msg) /*noexcept*/;

signals:

public slots:

protected slots:
    void run() /*noexcept*/;
};

#endif // THREADREADERKS_H
