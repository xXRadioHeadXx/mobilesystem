#ifndef THREADANALYSISKS_H
#define THREADANALYSISKS_H

#include <QThread>
#include <ksmsgmanager.h>

class ThreadAnalysisKS : public QThread
{
    Q_OBJECT
public:
    QMutex *m_mutexMSGAnalysis;
    KSMsgManager *ksMsgManager;
    QByteArray m_ksID,
               pattern;

    explicit ThreadAnalysisKS(KSMsgManager *ksMsgM,
                              QByteArray patternMsg,
                              QMutex *mutexMSGAnalysis,
                              QObject *parent = nullptr) /*noexcept*/;
    virtual ~ThreadAnalysisKS() /*noexcept*/;

signals:

protected slots:
    void run() /*noexcept*/;
};

#endif // THREADANALYSISKS_H
