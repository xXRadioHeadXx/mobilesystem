#ifndef THREADREADERPKP_H
#define THREADREADERPKP_H

#include <QThread>
#include <QSerialPort>
#include <pkpmsgmanager.h>
//#include <COSVariantList.h>
#include <cosvariantlist.h>


class ThreadReaderPKP : public QThread
{
    Q_OBJECT
public:

    QSerialPort *m_port;// порт
    PKPMsgManager *pkpMsgManager;
    QByteArray *m_ByteCollector;
    QByteArray newData;
    COSVariantList *m_listPattern;
    COSVariantList *m_listRequest;

    explicit ThreadReaderPKP(QSerialPort *port,
                             QByteArray *collector,
                             QByteArray data,
                             COSVariantList *listPattern,
                             COSVariantList *listRequest,
                             QObject *parent = nullptr) /*noexcept*/;
    virtual ~ThreadReaderPKP() /*noexcept*/;

    void process() /*noexcept*/;
    void analizMainBuffer() /*noexcept*/;

signals:

protected slots:
     void run() /*noexcept*/;

};

#endif // THREADREADERPKP_H
