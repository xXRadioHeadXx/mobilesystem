#ifndef PKPMANAGER
#define PKPMANAGER

#include <QObject>
#include <QDebug>

#include <serialportmanagerpkp.h>
#include <pkpmsgmanager.h>

class PKPManager : public QObject
{
    Q_OBJECT
public:
    DataBaseManager         *m_dbManager;
    SerialPortManagerPKP    *m_portManager;
    PKPMsgManager           *m_pkpMsgManager;



    explicit PKPManager(DataBaseManager *db, QObject *parent = nullptr) /*noexcept*/;
    virtual ~PKPManager() /*noexcept*/;

    QList<QString> getPortSetting() /*noexcept*/;

    QList<QByteArray> received_from_PKP;

signals:
    void updatePortSetting(QList<QString> setting);
    void portCondition(qint32 condition);
    void newMSGPKP(QByteArray pattern);

public slots:

    bool openPort(QString portName = "COM1",
                  qint32 rate = QSerialPort::Baud38400,
                  QSerialPort::DataBits dataBit = QSerialPort::Data8,
                  QSerialPort::StopBits stopBit = QSerialPort::OneStop,
                  QSerialPort::FlowControl flow = QSerialPort::NoFlowControl,
                  QSerialPort::Parity parity = QSerialPort::NoParity) /*noexcept*/;

    void closePort() /*noexcept*/;
    void send_query() /*noexcept*/;
    void request_0x60() /*noexcept*/;//PKP




};

#endif // PKPMANAGER

