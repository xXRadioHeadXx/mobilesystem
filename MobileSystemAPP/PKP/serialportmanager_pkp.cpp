
#include <serialportmanager_pkp.h>

SerialPortManagerPKP::SerialPortManagerPKP(///BSKMsgManager *bsk,
                                           QObject *parent) noexcept :
    QObject(parent)///,
    ///bskMsgManager(bsk)
{
    this->m_port = nullptr;
    lastNumPattern = 0xFF;
    lastCodePattern = 0xFF;
    this->m_ByteCollector = new QByteArray;

    prevousThreadReader = nullptr;
    prevousThreadAnalysis = nullptr;

    this->m_listPattern = new ListValueContainer;
    connect(this->m_listPattern,
            SIGNAL(newValue()),
            this,
            SLOT(acceptPattern()));
    this->m_listRequest = new ListValueContainer;
    connect(this->m_listRequest,
            SIGNAL(newValue()),
            this,
            SLOT(sendRequestMSG()));
    m_port = new QSerialPort;
    connect(m_port,
            SIGNAL(error(QSerialPort::SerialPortError)),
            this,
            SLOT(errorPort(QSerialPort::SerialPortError)));
    connect(m_port,
            SIGNAL(readyRead()),
            this,
            SLOT(process()));

    this->closePort();
}

SerialPortManagerBSK::~SerialPortManagerBSK() noexcept
{
    this->closePort();

    disconnect(this->m_listPattern,
            SIGNAL(newValue()),
            this,
            SLOT(acceptPattern()));
    disconnect(this->m_listRequest,
            SIGNAL(newValue()),
            this,
            SLOT(sendRequestMSG()));
    this->m_listPattern->clear();
    this->m_listRequest->clear();


    if(prevousThreadReader)
    {
        prevousThreadReader->terminate();
        prevousThreadReader->wait();
        delete prevousThreadReader;
        prevousThreadReader = nullptr;
    }

    if(prevousThreadAnalysis)
    {
        prevousThreadAnalysis->terminate();
        prevousThreadAnalysis->wait();
        delete prevousThreadAnalysis;
        prevousThreadAnalysis = nullptr;
    }

    delete m_listPattern;
    delete m_listRequest;
    m_listPattern = nullptr;
    m_listRequest = nullptr;

    delete m_port;
    delete m_ByteCollector;
}

void SerialPortManagerBSK::errorPort(QSerialPort::SerialPortError error) noexcept
{
#ifdef QT_DEBUG
    qDebug() << "SerialPortManagerKS::errorPort(" << error << m_port->errorString() << ")";
#endif
    return;
    switch(error)
    {
        case QSerialPort::NoError:
        {
            break;
        };
        case QSerialPort::DeviceNotFoundError:
        {
            break;
        };
        case QSerialPort::PermissionError:
        {
            break;
        };
        case QSerialPort::OpenError:
        {
            break;
        };
        case QSerialPort::NotOpenError:
        {
            break;
        };
        case QSerialPort::ParityError:
        {
            break;
        };
        case QSerialPort::FramingError:
        {
            break;
        };
        case QSerialPort::BreakConditionError:
        {
            break;
        };
        case QSerialPort::WriteError:
        {
            this->reopenPort();
            break;
        };
        case QSerialPort::ReadError:
        {
            this->reopenPort();
            break;
        };
        case QSerialPort::ResourceError:
        {
            this->reopenPort();
            break;
        };
        case QSerialPort::UnsupportedOperationError:
        {
            break;
        };
        case QSerialPort::TimeoutError:
        {
            this->reopenPort();
            break;
        };
        case QSerialPort::UnknownError:
        {
            this->reopenPort();
            break;
        };
        default:
        {
            break;
        };
    }
}

//    закрытие порта
void SerialPortManagerBSK::closePort() noexcept
{
    this->m_stopFlag = false;
    if(this->m_port)
    {
        if(this->m_port->isOpen())
        {
            this->m_port->close();
        }
        disconnect(m_port,
                SIGNAL(error(QSerialPort::SerialPortError)),
                this,
                SLOT(errorPort(QSerialPort::SerialPortError)));
        disconnect(m_port,
                SIGNAL(readyRead()),
                this,
                SLOT(process()));

        delete this->m_port;
        this->m_port = nullptr;

    }
    this->m_port = new QSerialPort;
    connect(m_port,
            SIGNAL(error(QSerialPort::SerialPortError)),
            this,
            SLOT(errorPort(QSerialPort::SerialPortError)));
    connect(m_port,
            SIGNAL(readyRead()),
            this,
            SLOT(process()));
    this->emitPortCondition();


}

bool SerialPortManagerBSK::reopenPort() noexcept
{
    if(m_portName != this->m_port->portName())
        return false;

    this->closePort();

    this->m_port->setPortName(m_portName);

    this->m_port->open(QSerialPort::ReadWrite);


    if(this->m_port->isOpen())
    {
        this->m_port->setBaudRate(m_rate);
        this->m_port->setDataBits(m_dataBit);
        this->m_port->setParity(m_parity);
        this->m_port->setStopBits(m_stopBit);
        this->m_port->setFlowControl(m_flow);
#ifdef QT_DEBUG
        qDebug() << "";
        qDebug() << "";
        qDebug() << "SerialPort " << m_portName << " isOpen(TRUE)";

        qDebug() << "= Current parameters =";
//        qDebug() << "Device name            : " << m_port->portName();
        qDebug() << "Device name            : " << m_port->portName();
        qDebug() << "Baud rate              : " << m_port->baudRate();
        qDebug() << "Data bits              : " << m_port->dataBits();
        qDebug() << "Parity                 : " << m_port->parity();
        qDebug() << "Stop bits              : " << m_port->stopBits();
        qDebug() << "Flow                   : " << m_port->flowControl();
//        this->m_port->setReadBufferSize(262144);
//        qDebug() << "BufferSize             : " << m_port->readBufferSize();
        qDebug() << "";
        qDebug() << "";
#endif



        this->m_ByteCollector->clear();
        this->emitPortCondition();

        return this->m_stopFlag = true;
    }
    else
    {
#ifdef QT_DEBUG
        qDebug() << "";
        qDebug() << "";
        qDebug() << "";
        qDebug() << "SerialPort " << m_portName << " isOpen(FALSE)";
        qDebug() << "";
#endif
        this->closePort();
        return this->m_stopFlag = false;
    }
}

//    открытие порта
bool SerialPortManagerBSK::openPort(QString portName,
                                    qint32 rate,
                                    QSerialPort::DataBits dataBit,
                                    QSerialPort::StopBits stopBit,
                                    QSerialPort::FlowControl flow,
                                    QSerialPort::Parity parity) noexcept
{
    this->closePort();

    this->m_port->setPortName(m_portName = portName);

    this->m_port->open(QSerialPort::ReadWrite);


    if(this->m_port->isOpen())
    {
        this->m_port->setBaudRate(m_rate = rate);
        this->m_port->setDataBits(m_dataBit = dataBit);
        this->m_port->setParity(m_parity = parity);
        this->m_port->setStopBits(m_stopBit = stopBit);
        this->m_port->setFlowControl(m_flow = flow);
        this->m_port->setReadBufferSize(262144);
#ifdef QT_DEBUG
        qDebug() << "";
        qDebug() << "";
        qDebug() << "= Current parameters =";
        qDebug() << "SerialPort             : " << portName;
        qDebug() << "Device name            : " << m_port->portName();
        qDebug() << "Baud rate              : " << m_port->baudRate();
        qDebug() << "Data bits              : " << m_port->dataBits();
        qDebug() << "Parity                 : " << m_port->parity();
        qDebug() << "Stop bits              : " << m_port->stopBits();
        qDebug() << "Flow                   : " << m_port->flowControl();
        qDebug() << "BufferSize             : " << m_port->readBufferSize();
        qDebug() << "";
        qDebug() << "";
#endif


        this->m_ByteCollector->clear();
        this->emitPortCondition();

        return this->m_stopFlag = true;
    }
    else
    {
#ifdef QT_DEBUG
        qDebug() << "";
        qDebug() << "";
        qDebug() << "";
        qDebug() << "SerialPort " << portName << " isOpen(FALSE)";
        qDebug() << "";
#endif
        this->closePort();
        return this->m_stopFlag = false;
    }
}

void SerialPortManagerBSK::sendBSKMSG() noexcept
{
    this->sendBSKMSG(this->bskMsgManager->msgToSend);
}

void SerialPortManagerBSK::sendRequestMSG() noexcept
{
    if(this->m_port->isOpen())
    {
//        while(!m_listRequest->m_list.isEmpty())
        while(!m_listRequest->isEmpty())
        {
//            QByteArray msg(m_listRequest->m_list.first().toByteArray());
//            m_listRequest->m_list.removeFirst();
            QByteArray msg(m_listRequest->first().toByteArray());
            m_listRequest->removeFirst();
            if(msg.isEmpty())
                continue;

            this->m_port->write(msg);
        }
    }
}

void SerialPortManagerBSK::sendBSKMSG(QByteArray msg) noexcept
{
    m_listRequest->append(msg);
//    if(this->m_port->isOpen())
//    {
//        this->m_port->write(msg);
//    }
//    return;
}

void SerialPortManagerBSK::process() noexcept
{
    if(!this->m_stopFlag)
    {
        return;
    }

    QByteArray newData(this->m_port->readAll());

    ThreadReaderBSK *newThreadReader(new ThreadReaderBSK(m_port,
                                                         this->m_ByteCollector,
                                                         newData,
                                                         m_listPattern,
                                                         m_listRequest/*,
                                                         this*/));


    if(prevousThreadReader)
    {
        connect(newThreadReader,
                SIGNAL(started()),
                prevousThreadReader,
                SLOT(deleteLater()));
        connect(prevousThreadReader,
                SIGNAL(finished()),
                newThreadReader,
                SLOT(start()));
        if(prevousThreadReader->isFinished())
        {
            newThreadReader->start();
        }
        prevousThreadReader = newThreadReader;
    }
    else
    {
        newThreadReader->start();
        prevousThreadReader = newThreadReader;
    }

    return;
}

bool SerialPortManagerBSK::refreshBuffer() noexcept
{
    //
    emit this->portCondition(0);
    //
    bool result(false);
    this->m_stopFlag = false;
    result = this->openPort(m_portName,
                            m_rate,
                            m_dataBit,
                            m_stopBit,
                            m_flow,
                            m_parity);
    this->m_ByteCollector->clear();
    this->m_stopFlag = true;
    return result;
}

void SerialPortManagerBSK::acceptPattern() noexcept
{
    //
    while(!m_listPattern->isEmpty())
    {
        QByteArray pattern(m_listPattern->first().toByteArray());
        m_listPattern->removeFirst();

        if(pattern.isEmpty())
            continue;

        repeatExcept = false;
        quint8 numMSG((quint8)pattern.at(3) / 0x10),
               codeMSG((quint8)pattern.at(5));
        if(lastNumPattern == numMSG &&
           lastCodePattern == codeMSG)
        {
            repeatExcept = true;
        }
        lastNumPattern = numMSG;
        lastCodePattern = codeMSG;

        ThreadAnalysisBSK *newThreadAnalysis(new ThreadAnalysisBSK(bskMsgManager,
                                                                   pattern/*,
                                                                   this*/));

        if(prevousThreadAnalysis)
        {
            connect(newThreadAnalysis,
                    SIGNAL(started()),
                    prevousThreadAnalysis,
                    SLOT(deleteLater()));
            connect(prevousThreadAnalysis,
                    SIGNAL(finished()),
                    newThreadAnalysis,
                    SLOT(start()));
            if(prevousThreadAnalysis->isFinished())
            {
                newThreadAnalysis->start();
            }
            prevousThreadAnalysis = newThreadAnalysis;
        }
        else
        {
            newThreadAnalysis->start();
            prevousThreadAnalysis = newThreadAnalysis;
        }
    }
    return;
    //
}

void SerialPortManagerBSK::emitPortCondition() noexcept
{
    qint32 condition(this->m_port->isOpen() ? 1 : -1);
    emit this->portCondition(condition);
}

bool SerialPortManagerBSK::isOpen() noexcept
{
    if(m_port)
    {
        return m_port->isOpen();
    }
    return false;
}

QString SerialPortManagerBSK::portName() noexcept
{
    return m_port->portName();
}

qint32 SerialPortManagerBSK::baudRate() noexcept
{
    return m_port->baudRate();
}

qint32 SerialPortManagerBSK::dataBits() noexcept
{
    return m_port->dataBits();
}

qint32 SerialPortManagerBSK::parity() noexcept
{
    return m_port->parity();
}

qint32 SerialPortManagerBSK::stopBits() noexcept
{
    return m_port->stopBits();
}
