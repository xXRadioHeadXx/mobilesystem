#ifndef THREADANALYSISPKP_H
#define THREADANALYSISPKP_H

#include <QThread>
#include <pkpmsgmanager.h>

class ThreadAnalysisPKP : public QThread
{
    Q_OBJECT
public:

    PKPMsgManager *pkpMsgManager;
    QByteArray pattern;

    explicit ThreadAnalysisPKP(PKPMsgManager *pkp,
                               QByteArray patternMsg,
                               QObject *parent = nullptr) /*noexcept*/;
    virtual ~ThreadAnalysisPKP() /*noexcept*/;
    void process() /*noexcept*/;

signals:

protected slots:
    void run() /*noexcept*/;

public slots:
};

#endif // THREADANALYSISPKP_H
