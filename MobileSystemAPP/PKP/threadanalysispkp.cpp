#include <threadanalysispkp.h>



ThreadAnalysisPKP::ThreadAnalysisPKP(PKPMsgManager *pkp,
                                     QByteArray patternMsg,
                                     QObject *parent) /*noexcept*/ :
    QThread(parent),
    pkpMsgManager(pkp),
    pattern(patternMsg)
{
}

ThreadAnalysisPKP::~ThreadAnalysisPKP() /*noexcept*/
{
    try
    {
        pkpMsgManager = nullptr;
        pattern.clear();
    }
    catch(...)
    {
#ifdef QT_DEBUG
        qDebug() << "Assert(~ThreadAnalysisPKP)";
#endif
        return;
    }
}

void ThreadAnalysisPKP::run() /*noexcept*/
{
    this->pkpMsgManager->newMSG(pattern);
    this->quit();
    return;
}
