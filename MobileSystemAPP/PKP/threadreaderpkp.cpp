
#include <threadreaderpkp.h>

ThreadReaderPKP::ThreadReaderPKP(QSerialPort *port,
                                 QByteArray *collector,
                                 QByteArray data,
                                 COSVariantList *listPattern,
                                 COSVariantList *listRequest,
                                 QObject *parent) /*noexcept*/ :
    QThread(parent),
    m_port(port),
    m_ByteCollector(collector),
    newData(data),
    m_listPattern(listPattern),
    m_listRequest(listRequest)
{
}

ThreadReaderPKP::~ThreadReaderPKP() /*noexcept*/
{
    try
    {
        m_port = nullptr;
        m_ByteCollector = nullptr;
    }
    catch(...)
    {
#ifdef QT_DEBUG
        qDebug() << "Assert(~ThreadReaderPKP)";
#endif
        return;
    }
}

void ThreadReaderPKP::process() /*noexcept*/
{
    if(this->m_port->isOpen())
    {
        this->m_ByteCollector->append(newData);
        this->analizMainBuffer();
    }
}

void ThreadReaderPKP::run() /*noexcept*/
{
    this->process();
    this->quit();
}


//обработка главного буфера
void ThreadReaderPKP::analizMainBuffer() /*noexcept*/
{
    while(true)
    {
//      удаляем всё перед A5 -->
        int indexA5(0);
        indexA5 = this->m_ByteCollector->indexOf(0xA5, 0);
        if(0 < indexA5)
        {
//            qDebug() << "!!! Remove Trash !!!";
            this->m_ByteCollector->remove(0, indexA5);
        }
        if(-1 == indexA5)
        {
            this->m_ByteCollector->clear();
        }
//      удаляем всё перед A5 <--

//      проверяем по размеру -->
        quint32 size(this->m_ByteCollector->size());
        if(6 > size)//нужны ещё байты. а пока выходим.
        {
//            qDebug() << "return(7 > m_ByteCollector.size(" << this->m_ByteCollector->size() << ")) --x  m_ByteCollector(" << this->m_ByteCollector->left(5).toHex() << "... ["  << this->m_ByteCollector->size() << "] ..." << this->m_ByteCollector->right(5).toHex() << ")";
//            qDebug() << "analizMainBuffer() <--";
            return;
        }

        quint8 dataSize((quint8)this->m_ByteCollector->at(3));//размер поля данных;

        if((6 + (int)dataSize) > (int)size)
        {//нужны ещё байты. а пока выходим.
            return;
        }
//      проверяем по размеру <--

        QByteArray pattern;
        if(1 < this->m_ByteCollector->count((quint8)0xA5))
        {
            qint32 indexSecondA5(this->m_ByteCollector->indexOf((quint8)0xA5, 1));
            pattern = this->m_ByteCollector->left(indexSecondA5);
            this->m_ByteCollector->remove(0, indexSecondA5);
            if(6 + dataSize > pattern.size())
            {
                continue;
            }
//                                qDebug() << "Rmove segment from this->listBuffer->[" << ci << "].data";
        }
        else
        {
            pattern = this->m_ByteCollector->left(this->m_ByteCollector->size());
            this->m_ByteCollector->clear();
        }

//      ребайтстаффинг -->
//      ребайтстаффинг <--

        pattern = pattern.left(6 + dataSize);

//      проверка crc -->
        quint8 crc8(0x00);
        for(qint32 i(1), n(pattern.size() - 1); i < n; i++)
        {
            crc8 = crc8 ^ (quint8)pattern.at(i);
        }
//      проверка crc <--

        if(crc8 == (quint8)pattern.right(1).at(0))
        {//отправить на обработку!!!-->
            this->m_listPattern->appendUnique(QVariant(pattern));
//            this->m_listPattern->possAppend(pattern);
//            emit this->pattern(pattern);
        }//отправить на обработку!!!<--
        else
        {
#ifdef QT_DEBUG
            qDebug() << "CRC FALSE !!! (" << pattern.toHex() << ")  right[" << crc8 << "]";
#endif
        }

        continue;
    }
}
