#include <pkpmanager.h>

#ifdef QT_DEBUG
#include <QDebug>
#endif


PKPManager::PKPManager(DataBaseManager *db,
                       QObject *parent) /*noexcept*/ :
    QObject(parent),
    m_dbManager(db)
{
    try
    {
        this->m_pkpMsgManager = new PKPMsgManager(m_dbManager,
                                                  this);
        this->m_portManager = new SerialPortManagerPKP(m_pkpMsgManager,
                                                       this);

        connect(this->m_portManager,
                SIGNAL(portCondition(qint32)),
                this,
                SIGNAL(portCondition(qint32)));

        connect(this->m_pkpMsgManager,
                SIGNAL(sendPKPcommand(QByteArray)),
                this->m_portManager,
                SLOT(sendPKPMSG(QByteArray)));

        connect(this->m_pkpMsgManager,
                SIGNAL(newMSGPKP(QByteArray)),
                this,
                SIGNAL(newMSGPKP(QByteArray)));
    }
    catch (std::runtime_error &rte)
    {
        qWarning("catch runtime_error %s [PKPManager::PKPManager]", rte.what());
    }
    catch (std::bad_alloc& ba)
    {
        qWarning("catch bad_alloc %s [PKPManager::PKPManager]", ba.what());
    }
    catch (std::exception &e)
    {
        qWarning("catch exception %s [PKPManager::PKPManager]", e.what());
    }
    catch (...)
    {
        qWarning("catch exception <unknown> [PKPManager::PKPManager]");
    }

}

void PKPManager::request_0x60() /*noexcept*/
{
    this->m_pkpMsgManager->compare_pattern.clear();
    this->m_pkpMsgManager->request_0x60_PKP();
}


//запрос на передачу одного блока
void PKPManager::send_query() /*noexcept*/
{
    this->m_pkpMsgManager->compare_pattern.clear();
    this->m_pkpMsgManager->request_0x60_PKP();
}


PKPManager::~PKPManager() /*noexcept*/
{
    try
    {
        delete m_portManager;
        delete m_pkpMsgManager;
        m_dbManager = nullptr;
    }
    catch(...)
    {
#ifdef QT_DEBUG
        qDebug() << "Assert(~PKPManager)";
#endif
        return;
    }
}



//PKP___открытие порта
bool PKPManager::openPort(QString portName,
                          qint32 rate,
                          QSerialPort::DataBits dataBits,
                          QSerialPort::StopBits stopBits,
                          QSerialPort::FlowControl flow,
                          QSerialPort::Parity parity) /*noexcept*/
{

    if(this->m_portManager->openPort(portName,
                                     rate,
                                     dataBits,
                                     stopBits,
                                     flow,
                                     parity))
    {
        emit this->updatePortSetting(this->getPortSetting());
#ifdef QT_DEBUG
        qDebug() << "PORT OPENED IS " << portName;
#endif
        //this->emitInsertNewMSG(0x00,
        //                       0xFF,
        //                       QString("Port " + portName + " opening succeeded").toUtf8(),
        //                       true);
        return true;
    }

    //this->emitInsertNewMSG(0x00,
    //                       0xFF,
    //                       QString("Port " + portName + " opening failed").toUtf8(),
    //                       true);

    emit this->updatePortSetting(QList<QString>());
    return false;
}

//закрытие порта
void PKPManager::closePort() /*noexcept*/
{/*
    while(this->timerPortCheck.isActive())
        this->timerPortCheck.stop();

    while(this->timerWaitAnswer.isActive())
        this->timerWaitAnswer.stop();
*/
    this->m_portManager->closePort();
#ifdef QT_DEBUG
    qDebug() << "Closing port";
#endif

    emit this->updatePortSetting(this->getPortSetting());

}


QList<QString> PKPManager::getPortSetting() /*noexcept*/
{
    QList<QString> listSetting;
    if(this->m_portManager->m_port)
    {
        if(this->m_portManager->isOpen())
        {
            listSetting.append(this->m_portManager->portName());
            QString strBaud("%1");
            strBaud = strBaud.arg(this->m_portManager->baudRate());
            listSetting.append(strBaud);
            QString strData("%1");
            strData = strData.arg(this->m_portManager->dataBits());
            listSetting.append(strData);
            listSetting.append((0 != this->m_portManager->parity()) ? "Y" : "N");
            QString strStop("%1");
            strStop = strStop.arg(this->m_portManager->stopBits());
            listSetting.append(strStop);
            return listSetting;
        }
    }
    return listSetting;
}
