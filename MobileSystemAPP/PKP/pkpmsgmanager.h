#ifndef PKPMSGMANAGER_H
#define PKPMSGMANAGER_H

#include <QObject>
#include <databasemanager.h>
///#include <soundalarm.h>

class PKPMsgManager : public QObject
{
    Q_OBJECT
public:

    DataBaseManager *dbManager;
    QMutex          *m_resursLock;
    QByteArray msgToSend,
               idUNToSend;
    quint8 currentKitNum;
    QList<QByteArray> listAcceptPattern;

    explicit PKPMsgManager(DataBaseManager *db,
                           QObject *parent = nullptr) /*noexcept*/;

    //контрольная сумма
    QByteArray setCRC8XOR(QByteArray pattern) /*noexcept*/;


    void send_query();              //запрос ПКП
    QByteArray compare_pattern;     //для определения повторов


    ///
    bool CheckSum(QByteArray pattern) /*noexcept*/;              //BSK___ проверка контрольной суммы
    ///


    void newMSG(QByteArray pattern) /*noexcept*/;
    void updateStatusPKPExchange(bool status) /*noexcept*/;


    //формирование команд -->
    void request_0x60_PKP() /*noexcept*/;//PKP


    quint32 emitInsertNewMSG(UnitNode *unSender,
                             quint8 codeMSG,
                             QByteArray msg,
                             bool ioType, TypeGroupMSG typeGroup,
                             bool flagHidden = false) /*noexcept*/;


public slots:


signals:

    void newMSGPKP(QByteArray msg);
    void ssUpdateStatusPKPExchange(int timeout);
    void sendPKPcommand(QByteArray command);

};

#endif // PKPMSGMANAGER_H
