#include <pkpmsgmanager.h>



PKPMsgManager::PKPMsgManager(DataBaseManager *db,
                             QObject *parent) /*noexcept*/ :
    QObject(parent),
    dbManager(db)
{
    try
    {
        this->m_resursLock = new QMutex;
        currentKitNum = 0x01;

        listAcceptPattern.clear();
    }
    catch (std::runtime_error &rte)
    {
        qWarning("catch runtime_error %s [PKPMsgManager::PKPMsgManager]", rte.what());
    }
    catch (std::bad_alloc& ba)
    {
        qWarning("catch bad_alloc %s [PKPMsgManager::PKPMsgManager]", ba.what());
    }
    catch (std::exception &e)
    {
        qWarning("catch exception %s [PKPMsgManager::PKPMsgManager]", e.what());
    }
    catch (...)
    {
        qWarning("catch exception <unknown> [PKPMsgManager::PKPMsgManager]");
    }
}

//контрольная сумма
QByteArray PKPMsgManager::setCRC8XOR(QByteArray pattern) /*noexcept*/
{
    int n(pattern.size() - 1);
    pattern[n] = (quint8)0x00;
    for(int i(1); i < n; i++)
    {
        pattern[n] = pattern.at(n) ^ pattern.at(i);
    }
    return pattern;
}

void PKPMsgManager::updateStatusPKPExchange(bool status) /*noexcept*/
{

    emit this->ssUpdateStatusPKPExchange(5000);
}

//отправка запроса на ПКП
/*
void PKPMsgManager::send_query()
{




}
*/

//BSK___проверка контрольной суммы
bool PKPMsgManager::CheckSum(QByteArray pattern) /*noexcept*/
{
    int     n(pattern.size() - 1);
    quint8  sample = 0;

    for(int i(1); i < n; i++)
    {
        sample = sample ^ pattern.at(i);
    }

    if((quint8)pattern.at(n) == sample)
    {
        return true;   //OK
    }
    return false;   //not OK
}

//анализ принятого сообщения___
void PKPMsgManager::newMSG(QByteArray pattern) /*noexcept*/
{

    //наличие соединения
    this->updateStatusPKPExchange(true);

    //сравнение по содержимому
    if(pattern == compare_pattern)
    {
        //совпадение - выходим
        qDebug() << "ПКП: совпадение пакета";
        //compare_pattern.clear();
        return;
    }
    else
    {
        if(compare_pattern.isEmpty())
        {
            compare_pattern = pattern;
        }
    }

    //ПКП____________________________________

    qDebug() << "полученный пакет " << pattern.toHex();
    if((quint8)0x80 == ((quint8)pattern.at(1) & 0xF0) && //проверка, от ПКП ли пакет (код 0x80)
       (quint8)0xFF == (quint8)pattern.at(2))
    {
        qDebug() << "сообщение от ПКП";

        if(!listAcceptPattern.contains(pattern))
        {
            this->request_0x60_PKP();
        }
        else
        {
            listAcceptPattern.clear();
        }

        switch((quint8)pattern.at(4))
        {
            case (quint8)0x60://PKP
            {
                listAcceptPattern.append(pattern);
                qDebug() << "код 0x60";

                //отправить сигнал
                emit this->newMSGPKP(pattern);

                //return;
            }
            default:
            {

            }
        }
    }


    //следующий запрос
    this->request_0x60_PKP();



#ifdef QT_DEBUG
    qDebug() << "PKP: Something is wrong with it: " << pattern.toHex();
#endif
}

void PKPMsgManager::request_0x60_PKP() /*noexcept*///PKP
{
    //compare_pattern.clear();
    //отправка запроса
    QByteArray conf;

    conf.append((quint8)0xFF);
    conf.append((quint8)0xFF);
    conf.append((quint8)0xA5);
    conf.append((quint8)0xFF);
    conf.append((quint8)0x80);
    conf.append((char)  0x00);
    conf.append((quint8)0x60);
    conf.append((quint8)(conf.at(3) ^ conf.at(4) ^ conf.at(5) ^ conf.at(6)));

    //отправить ПКП
    emit this->sendPKPcommand(conf);

#ifdef QT_DEBUG
    qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " PkpMSG OUTPUT << [command    0x60] [" << conf.toHex() << "]";
#endif

}

quint32 PKPMsgManager::emitInsertNewMSG(UnitNode *unSender,
                                       quint8 codeMSG,
                                       QByteArray msg,
                                       bool ioType,
                                        TypeGroupMSG typeGroup,
                                       bool flagHidden) /*noexcept*/
{
    quint32 indexDBMsg(0);
    indexDBMsg = this->dbManager->addNewMSG(unSender,
                                            codeMSG,
                                            msg,
                                            ioType,
                                            BSKSystemType,
                                            typeGroup,
                                            flagHidden);
    return indexDBMsg;
}






