QT += core serialport
CONFIG += c++11

INCLUDEPATH += $$PWD/

HEADERS += \
    $$PWD/pkpmanager.h \
    $$PWD/pkpmsgmanager.h \
    $$PWD/serialportmanagerpkp.h \
    $$PWD/threadanalysispkp.h \
    $$PWD/threadreaderpkp.h

SOURCES += \
    $$PWD/pkpmanager.cpp \
    $$PWD/pkpmsgmanager.cpp \
    $$PWD/serialportmanagerpkp.cpp \
    $$PWD/threadanalysispkp.cpp \
    $$PWD/threadreaderpkp.cpp
