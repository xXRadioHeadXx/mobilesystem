﻿#ifndef GLOBAL_VALUE_AND_STRUCTURE_H
#define GLOBAL_VALUE_AND_STRUCTURE_H

#include <QDateTime>
#include <QColor>

#define _szb_ 10
#define _wt_ 10000
#define _dt_ 150
#define _pt_ 300000
#define _dtB4_ 300



enum TypeUnitNodeSystem
{
    UnknownSystemType = (quint8)0xFF,
    KMSystemType = (quint8)0x00,
    KSSystemType = (quint8)0x01,
    BSKSystemType = (quint8)0x02
};

enum TypeUnitNodeDevice
{
    UnknownDeviceType =  (quint8)0xFF,
    // KM Device -->
    Unknown_KMDeviceType=(quint8)0x00,
    VK_KMDeviceType =    (quint8)0x01,
    RT_KMDeviceType =    (quint8)0x02,
    PKP_KMDeviceType =   (quint8)0x03,
    // KM Device <--
    // KS Device -->
    CPP_KSDeviceType =   (quint8)0x00,
    RT01_KSDeviceType =  (quint8)0x01,
    RT02_KSDeviceType =  (quint8)0x02,
    VK09_KSDeviceType =  (quint8)0x09,
    VK0A_KSDeviceType =  (quint8)0x0A,
    VK0B_KSDeviceType =  (quint8)0x0B,
    // KS Device <--
    // BSK Device -->
    CPP_BSKDeviceType =  (quint8)0x00,
    RT_BSKDeviceType =   (quint8)0x01,
    S_BSKDeviceType =    (quint8)0x02,
    RLO_BSKDeviceType =  (quint8)0x03,
    RLD_BSKDeviceType =  (quint8)0x04,
    RWD_BSKDeviceType =  (quint8)0x05,
    MSO_BSKDeviceType =  (quint8)0x06,
    BVU_BSKDeviceType =  (quint8)0x07,
    PKP_BSKDeviceType =  (quint8)0x08,
    VK_BSKDeviceType =   (quint8)0x09,
    O_BSKDeviceType =    (quint8)0x0A,
    IK_BSKDeviceType =   (quint8)0x0B,
    RW_BSKDeviceType =   (quint8)0x0C,
    RVP_BSKDeviceType =  (quint8)0x0D,
    PKSC_BSKDeviceType = (quint8)0x0E,
    VSO_BSKDeviceType =  (quint8)0x0F
    // BSK Device <--
};

enum TypeGroupMSG
{
    Other_TypeGroupMSG =     (quint8)0x00,
    System_TypeGroupMSG =    (quint8)0x01,
    Command_TypeGroupMSG =   (quint8)0x02,
    Alarm_TypeGroupMSG =     (quint8)0x03,
    Connect_TypeGroupMSG =   (quint8)0x04,
    Error_TypeGroupMSG =     (quint8)0x05,
    Discharge_TypeGroupMSG = (quint8)0x06,
    FF_TypeGroupMSG =        (quint8)0x07
};

struct FilterStruct
{
    bool filterMode,
         filterDate,
         filterTime,
         filterSN,
         filterType,
         filterID,
         filterMSGGroup,
         filterMSGAlarm,
         filterMSGConnect,
         filterMSGError,
         filterMSGDischarge,
         filterMSGFF,
         filterMSGCommand,
         filterMSGSystem;

    QDate filterMinDate, filterMaxDate;
    QTime filterMinTime, filterMaxTime;

    int filterSector, filterNumber;
    TypeUnitNodeDevice filterTypeValue;

    QByteArray filterIDValue;

};

#endif // GLOBAL_VALUE_AND_STRUCTURE_H
