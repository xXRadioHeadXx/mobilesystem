#include <proxymetod.h>

ProxyMetod::ProxyMetod(QObject *parent) /*noexcept*/ : QObject(parent)
{

}

QByteArray ProxyMetod::sumQListQByteArray(QList<QByteArray> list) /*noexcept*/
{
    QByteArray sum;
    for(int i(0), n(list.size()); i < n; i++)
    {
        sum += list.at(i);
        sum += ' ';
    }
    return sum;
}

QByteArray ProxyMetod::msgColorToByteArray(bool ioType,
                                           QByteArray baMSG,
                                           TypeUnitNodeSystem typeSystem) /*noexcept*/
{
    QColor resultColor(this->msgColor(ioType,
                                      baMSG,
                                      typeSystem));
    QByteArray resultBA;
    resultBA.append((quint8)resultColor.red());
    resultBA.append((quint8)resultColor.green());
    resultBA.append((quint8)resultColor.blue());
    resultBA.append((quint8)resultColor.alpha());

    return resultBA;
}


QColor ProxyMetod::msgColor(bool ioType,
                            QByteArray baMSG,
                            TypeUnitNodeSystem typeSystem) /*noexcept*/
{
    QColor result;
    switch(typeSystem)
    {
        case KMSystemType:
        {
            return this->msgColor_KM(ioType, baMSG);
            break;
        };

        case KSSystemType:
        {
            return this->msgColor_KS(ioType, baMSG);
            break;
        };

        case BSKSystemType:
        {
            return this->msgColor_BSK(ioType, baMSG);
            break;
        };

        case UnknownSystemType:
        {
            return this->msgColor_SYS(ioType, baMSG);
            break;
        };

        default:
        {
            result.setRgb(255, 255, 255);//Qt::white);
            return result;
            break;
        }

    }
    result.setRgb(255, 255, 255);//Qt::white);
    return result;
}

QString ProxyMetod::msgCommentRUS(QByteArray baMSG,
                                  TypeUnitNodeSystem typeSystem) /*noexcept*/
{
    switch(typeSystem)
    {
        case KMSystemType:
        {
            return this->msgCommentRUS_KM(baMSG);
            break;
        };

        case KSSystemType:
        {
            return this->msgCommentRUS_KS(baMSG);
            break;
        };

        case BSKSystemType:
        {
            return this->msgCommentRUS_BSK(baMSG);
            break;
        };

        case UnknownSystemType:
        {
            return this->msgCommentRUS_SYS(baMSG);
            break;
        };

        default:
        {
            return baMSG.toHex();
            break;
        }

    }
    return QString();
}

QColor ProxyMetod::msgColor_SYS(bool ioType,
                                QByteArray baMSG) /*noexcept*/
{
    QColor result;
    if(ioType)
    {
        result.setRgb(255, 255, 255);//Qt::white
    }
    else
    {
        result.setRgb(192, 192, 192);//Qt::lightGray;
    }

    return result;
}

QString ProxyMetod::msgCommentRUS_SYS(QByteArray baMSG) /*noexcept*/
{
    QString str;

    if((quint8)0xA5 !=(quint8)baMSG.at(0))
    {
        str = baMSG;
        if(baMSG.contains("Start session"))
        {
            str = "Начало сессии";
        }
        if(baMSG.contains("Complete session"))
        {
            str = "Конец сессии";
        }
    }
    return str;
}

QColor ProxyMetod::msgColor_BSK(bool ioType,
                                QByteArray baMSG) /*noexcept*/
{
    QColor result;
    result.setRgb(192, 192, 192);//Qt::lightGray
    if(ioType)
    {
        result.setRgb(255, 255, 255);//Qt::white;
    }
    else
    {
        result.setRgb(192, 192, 192);//Qt::lightGray;
    }

    if((quint8)0xA5 !=(quint8)baMSG.at(0))
    {
        if(baMSG.contains("opening succeeded"))
        {
            result.setRgb(125, 255, 125);//Qt::green;
        }
        if(baMSG.contains("opening failed"))
        {
            result.setRgb(255, 255, 125);//Qt::yellow;
        }
        if(baMSG.contains("Deleted"))
        {
            result.setRgb(255, 125, 125);//Qt::red;
        }
        if(baMSG.contains("Port closed succeeded"))
        {
            result.setRgb(255, 125, 125);//Qt::red;
        }
        if(baMSG.contains("Lost connect"))
        {
            result.setRgb(255, 125, 125);//Qt::red;
        }
        if(baMSG.contains("Recovered connect"))
        {
            result.setRgb(125, 255, 125);//Qt::green;
        }
    }
    else
    {
        switch((quint8)baMSG.at(4))
        {
            case (quint8)0x9F:
            {
                switch((quint8)baMSG.at(5))
                {
                    case (quint8)0x01:
                    {
//                        str.append("Тревога");
                        result.setRgb(255, 125, 125);//Qt::red;
                        break;
                    };

                    case (quint8)0x02:
                    {
//                        str.append("Тревожное сообщение: ");
                        result.setRgb(255, 125, 125);//Qt::red;
                        break;
                    };

                    case (quint8)0x20:
                    {
//                        str.append("Есть связь");
                        result.setRgb(125, 255, 125);//Qt::green;
                        break;
                    };

                    case (quint8)0x40:
                    {
//                        str.append("Неисправность");
                        result.setRgb(255, 255, 125);//Qt::yellow;
                        break;
                    };

                    case (quint8)0x44:
                    {
//                        str.append("Нет узла сети");
                        result.setRgb(255, 255, 125);//Qt::yellow;
                        break;
                    };

                    case (quint8)0x90:
                    {
//                        str.append("Включение питания блока");
                        result.setRgb(125, 255, 125);//Qt::green;
                        break;
                    };

                    case (quint8)0x91:
                    {
//                        str.append("Отключение питания блока");
                        result.setRgb(125, 125, 125);//Qt::gray;
                        break;
                    };

                    case (quint8)0xBB:
                    {
//                        str.append("Контроль питания");
                        result.setRgb(128,203,249);//синий
                        break;
                    };

                    case (quint8)0xC0:
                    {
//                        str.append("Нет связи");
//                        result.setRgb(255, 255, 125);//Qt::yellow;
                        result.setRgb(188,185,210);//фиолетовый
                        break;
                    };

                    case (quint8)0xCC:
                    {
//                        str.append("Сообщение о выполненной команде");
                        break;
                    };

                    case (quint8)0xD0:
                    {
//                        str.append("Норма");
                        break;
                    };

                    default:
                    {
                        break;
                    }
                }
            };

            default:
            {
                break;
            }
        }
    }

    return result;
}


QString ProxyMetod::msgCommentRUS_BSK(QByteArray baMSG) /*noexcept*/
{
    QString str;

    if((quint8)0xA5 !=(quint8)baMSG.at(0))
    {
        str = baMSG;
        if(baMSG.contains("Start session"))
        {
            str = "Начало сессии";
        }
        if(baMSG.contains("Complete session"))
        {
            str = "Конец сессии";
        }
        if(baMSG.contains("opening succeeded"))
        {
            str = "Открытие порта " + baMSG.split(' ').at(1) + " успешно";
        }
        if(baMSG.contains("opening failed"))
        {
            str = "Открытие порта " + baMSG.split(' ').at(1) + " не успешно";
        }
        if(baMSG.contains("Port opening succeeded"))
        {
            str = "Открытие порта успешно";
        }
        if(baMSG.contains("Port opening failed"))
        {
            str = "Открытие порта не успешно";
        }
        if(baMSG.contains("Reset alarm"))
        {
            str = "Сброс тревоги";
        }
        if(baMSG.contains("Reset failure"))
        {
            str = "Сброс неисправности";
        }
        if(baMSG.contains("Reset discharge"))
        {
            str = "Сброс разряда";
        }
        if(baMSG.contains("Deleted"))
        {
            str = "Удалено";
        }
        if(baMSG.contains("Port closed succeeded"))
        {
            str = "Порт закрыт";
        }
        if(baMSG.contains("Lost connect"))
        {
            str = "Превышенно ожидание нормального обмена сообщениями";
        }
        if(baMSG.contains("Recovered connect"))
        {
            str = "Нормальный обмен сообщениями";
        }
    }
    else
    {
        switch((quint8)baMSG.at(4))
        {
            case (quint8)0x9F:
            {
                switch((quint8)baMSG.at(5))
                {
                    case (quint8)0x01:
                    {
                        str.append("Тревога");
                        return str;
                        break;
                    };

                    case (quint8)0x02:
                    {
                        str.append("Тревожное сообщение: ");

                        switch((quint8)baMSG.at(12))
                        {
                            case (quint8)1:
                                {str.append("Внимание.");	break;}

                            case (quint8)2:
                                {str.append("Тревога."); 	break;}

                            case (quint8)3:
                                {str.append("Внимание. Класс нарушителя не определен.");	break;}

                            case (quint8)4:
                                {str.append("Тревога. Одиночный.");	break;}

                            case (quint8)5:
                                {str.append("Тревога. Группа.");	break;}

                            case (quint8)6:
                                {str.append("Тревога. Транспортное средство.");	break;}

                            case (quint8)7:
                                {str.append("Внимание. Удаленное транспортное средство."); break;}

                            case (quint8)8:
                                {str.append("Внимание. Поезд."); break;}

                            case (quint8)9:
                                {str.append("Внимание. Животное.");	break;}

                            case (quint8)10:
                                {str.append("Внимание. Тяжелое транспортное средство."); break;}

                            case (quint8)11:
                                {str.append("Внимание."); break;}

                            case (quint8)12:
                                {str.append("Направление на объект.");	break;}

                            case (quint8)13:
                                {str.append("Направление с объекта."); 	break;}

                            case (quint8)14:
                                {str.append("Тревога. Класс нарушителя не определен."); break;}

                            case (quint8)15:
                                {str.append("Внимание. Одиночный."); break;}

                            case (quint8)19:
                                {str.append("Внимание. Группа."); break;}

                            case (quint8)20:
                                {str.append("Внимание. Транспортное средство."); break;}

                            case (quint8)21:
                                {str.append("Тревога. Удаленное транспортное средство."); 	break;}

                            case (quint8)22:
                                {str.append("Тревога. Поезд."); break;}

                            case (quint8)23:
                                {str.append("Тревога. Животное."); break;}

                            case (quint8)24:
                                {str.append("Тревога. Тяжелое транспортное средство."); break;}

                            default:
                                {str.append("Не определено."); break;}
                        }

                        return str;
                        break;
                    };

                    case (quint8)0x40:
                    {
                        str.append("Неисправность");
                        return str;
                        break;
                    };

                    case (quint8)0x44:
                    {
                        str.append("Нет узла сети");
                        return str;
                        break;
                    };

                    case (quint8)0x90:
                    {
                        str.append("Включение питания блока");
                        return str;
                        break;
                    };

                    case (quint8)0x91:
                    {
                        str.append("Отключение питания блока");
                        return str;
                        break;
                    };

                    case (quint8)0xBD:
                    {
                        str.append("Отключение по разряду ИПА");
                        return str;
                        break;
                    };

                    case (quint8)0xBB:
                    {
//                        str.append("Контроль питания");
                        str.append("Разряд");
                        return str;
                        break;
                    };

                    case (quint8)0xBA:
                    {
    //                        str.append("Контроль питания");
                        str.append("Норма ИПА");
                        return str;
                        break;
                    };

                    case (quint8)0x20:
                    {
                        str.append("Есть связь");
                        //str = QString("Есть связь с %1[%2]").arg(getNameSO_RUS(baMSG.mid(7, 2)))
                        //                                    .arg(baMSG.mid(7, 2).toHex().toUpper());
                        //str = QString("Есть связь с %1").arg(baMSG.mid(7, 2).toHex().toUpper());

                        return str;
                        break;
                    };

                    case (quint8)0xC0:
                    {
                        str.append("Нет связи");
                        //str = QString("Нет связи с %1%2").arg((quint8)baMSG.at(7)).arg((quint8)baMSG.at(8));
                        return str;
                        break;
                    };

                    case (quint8)0xCC:
                    {
                        switch((quint8)baMSG.at(10))
                        {
                            case (quint8)0x24://подтверждение включения радиомаяка
                            {
                                str.append("Проведена команда: Режим радиомаяка включен");
                                break;
                            }

                            case (quint8)0x25://подтверждение отключения радиомаяка
                            {
                                str.append("Проведена команда: Режима радиомаяка отключен");
                                break;
                            }

                            case (quint8)0x75://подтверждение изменения регистров сейсмики
                            {
                                str.append("Проведена команда: Подтверждение изменения признака");
                                break;
                            }

                            case (quint8)0x01://отключить СО
                            {
                                str.append("Проведена команда: СО Отключено");
                                break;
                            }

                            case (quint8)0x02://включить СО
                            {
                                str.append("Проведена команда: СО Включено");
                                break;
                            }

                            case (quint8)0x30://установить длину ЗО РЛО
                            {
                                switch((quint8)baMSG.at(11))
                                {
                                    case (quint8)0x20:
                                    {
                                        str.append("Проведена команда: Установлена зона обнаружения 20м");
                                        break;
                                    }
                                    case (quint8)0x40:
                                    {
                                        str.append("Проведена команда: Установлена зона обнаружения 40м");
                                        break;
                                    }
                                    case (quint8)0x60:
                                    {
                                        str.append("Установлена зона обнаружения 60м");
                                        break;
                                    }
                                    default:
                                    {
                                        str.append("Проведена команда: " + QString(baMSG.toHex().toUpper()));
                                        break;
                                    }
                                }
                                break;
                            }

                            case (quint8)0x7B://Ответ на команду "Передать конфигурацию"
                            {


                                str.append("Конфигурация получена");
                                break;


                                //в зависимости от типа устройства интерпретировать параметры конфигурации
                                switch((quint8)baMSG.at(7) / 0x10)
                                {
                                    case (quint8)0x01: //ретранслятор
                                    {

                                    }
                                    break;

                                    case (quint8)0x02: //сейсмика
                                    {

                                    }
                                    break;

                                    case (quint8)0x03: //РЛО
                                    {

                                    }
                                    break;

                                    case (quint8)0x04: //РЛД
                                    {

                                    }
                                    break;

                                    case (quint8)0x05: //РВД
                                    {

                                    }
                                    break;

                                    case (quint8)0x06: //М
                                    {

                                    }
                                    break;

                                    case (quint8)0x0A: //обрывное СО
                                    {

                                    }
                                    break;

                                    case (quint8)0x0B: break;//ИК

                                    case (quint8)0x0D: //РВП
                                    {

                                    }
                                    break;

                                    default:
                                    {
                                        break;
                                    }
                                }

                                //дальше нужно считать другие данные из пакета 0x7b


                                break;
                            }

                            case (quint8)0x40://установить чувствительность БСК-РЛД
                            {

                                if(((quint8)baMSG.at(7) / 0x10) == (quint8)RVP_BSKDeviceType)//проверка, что это РВП
                                {
                                    //РВП
                                    switch((quint8)baMSG.at(11))
                                    {
                                        case (quint8)0x00: str.append("Установлена чувствительность 1"); break;
                                        case (quint8)0x01: str.append("Установлена чувствительность 2"); break;
                                        case (quint8)0x02: str.append("Установлена чувствительность 3"); break;
                                        case (quint8)0x03: str.append("Установлена чувствительность 4"); break;
                                        case (quint8)0x04: str.append("Установлена чувствительность 5"); break;
                                        case (quint8)0x05: str.append("Установлена чувствительность 6"); break;
                                        case (quint8)0x06: str.append("Установлена чувствительность 7"); break;
                                        case (quint8)0x07: str.append("Установлена чувствительность 8"); break;
                                    }
                                }

                                if(((quint8)baMSG.at(7) / 0x10) == (quint8)RLD_BSKDeviceType)//проверка, что это РЛД
                                {
                                    //РЛД
                                    switch((quint8)baMSG.at(11))
                                    {
                                        case (quint8)0x00: str.append("Установлена минимальная чувствительность");  break;
                                        case (quint8)0x01: str.append("Установлена средняя чувствительность");      break;
                                        case (quint8)0x02: str.append("Установлена чувствительность выше средней"); break;
                                        case (quint8)0x03: str.append("Установлена максимальная чувствительность"); break;
                                    }
                                }


                                break;
                            }

                            case (quint8)0x55://подтверждение команды ДК
                            {
                                str.append("Проведена команда: ДК");
                                break;
                            }

                            case (quint8)0x78://подтрерждение смены номеров участка и СО
                            {
                                str.append("Проведена команда: Изменён номеров участка и СО");
                                break;
                            }
                        }

                        //str.append("Сообщение о выполненной команде");
                        return str;
                        break;
                    };

                    case (quint8)0xD0:
                    {
                        str.append("Норма");
                        return str;
                        break;
                    };

                    default:
                    {
                        str.append(baMSG.toHex().toUpper());
                        return str;
                        break;
                    }
                }
            };

            case (quint8)0xCE:
            {

                if((quint8)0x40 == (quint8)baMSG.at(1) &&
                   (quint8)0xFF == (quint8)baMSG.at(2))
                {
                    str = "Слив ID блоков сети";
                }
                else
                {
                    str = "Передать ID блоков сети";
                }

                return str;
                break;
            };

            case (quint8)0x0B:
            {
                str = "Выйти на связь";
                return str;
                break;
            };

            case (quint8)0x0E:
            {
                str = "Перейти в режим оконечного устройства";
                return str;
                break;
            };

            case (quint8)0x09:
            {
                str = "Провести самотестирование";
                return str;
                break;
            };

            case (quint8)0x41:
            {
                switch ((quint8)baMSG.at(7))
                {
                    case (quint8)0x55:
                    {
                        //str = "ДК СО";
                        str = "Команда: ДК";
                        return str;
                        break;
                    };

                    //запрос конфигурации для выделенного устройства
                    case (quint8)0x7b:
                    {
                        str = "Команда: Запрос конфигурации";
                        return str;
                        break;
                    };

                    case (quint8)0x01:
                    {
                        str = "Команда: Отключить СО";
                        return str;
                        break;
                    };

                    case (quint8)0x02:
                    {
                        str = "Команда: Включить СО";
                        return str;
                        break;
                    };

                    case (quint8)0x78:
                    {
//                        str = QString("Команда: Изменить номер участка %1 и номер СО %2").arg((quint8)baMSG.at(8)).arg((quint8)baMSG.at(9));
                        str = "Команда: Изменить номер участка %1 и номер СО %2";
                        str = str.arg((quint8)baMSG.at(8))
                                 .arg((quint8)baMSG.at(9));
                        return str;
                        break;
                    }

                    //команды настройки БСК-С
                    case (quint8)0x75:
                    {
                        switch ((quint8)baMSG.at(8))
                        {
                            case (quint8)0x00: //зима/лето
                                if((quint8)baMSG.at(9) == 0x01){str = "Команда: Включить признак ЗИМА";}
                                if((quint8)baMSG.at(9) == 0x00){str = "Команда: Включить признак ЛЕТО";}
                                break;

                            case (quint8)0x02: //тревога
                                if((quint8)baMSG.at(9) == 0x01){str = "Команда: Включить признак ТРЕВОГА";}
                                if((quint8)baMSG.at(9) == 0x00){str = "Команда: Отключить признак ТРЕВОГА";}
                                break;

                            case (quint8)0x03: //одиночный
                                if((quint8)baMSG.at(9) == 0x01){str = "Команда: Включить признак ОДИНОЧНЫЙ";}
                                if((quint8)baMSG.at(9) == 0x00){str = "Команда: Отключить признак ОДИНОЧНЫЙ";}
                                break;

                            case (quint8)0x04: //группа
                                if((quint8)baMSG.at(9) == 0x01){str = "Команда: Включить признак ГРУППА";}
                                if((quint8)baMSG.at(9) == 0x00){str = "Команда: Отключить признак ГРУППА";}
                                break;

                            case (quint8)0x05: //ТС
                                if((quint8)baMSG.at(9) == 0x01){str = "Команда: Включить признак ТС";}
                                if((quint8)baMSG.at(9) == 0x00){str = "Команда: Отключить признак ТС";}
                                break;

                            case (quint8)0x06: //удаленное ТС
                                if((quint8)baMSG.at(9) == 0x01){str = "Команда: Включить признак УДАЛЕННОЕ ТС";}
                                if((quint8)baMSG.at(9) == 0x00){str = "Команда: Отключить признак УДАЛЕННОЕ ТС";}
                                break;

                            case (quint8)0x07: //класс не определен
                                if((quint8)baMSG.at(9) == 0x01){str = "Команда: Включить признак КЛАСС НЕ ОПРЕДЕЛЕН";}
                                if((quint8)baMSG.at(9) == 0x00){str = "Команда: Отключить признак КЛАСС НЕ ОПРЕДЕЛЕН";}
                                break;

                            case (quint8)0x08: //поезд
                                if((quint8)baMSG.at(9) == 0x01){str = "Команда: Включить признак ПОЕЗД";}
                                if((quint8)baMSG.at(9) == 0x00){str = "Команда: Отключить признак ПОЕЗД";}
                                break;

                            case (quint8)0x09: //направление
                                if((quint8)baMSG.at(9) == 0x01){str = "Команда: Включить признак НАПРАВЛЕНИЕ";}
                                if((quint8)baMSG.at(9) == 0x00){str = "Команда: Отключить признак НАПРАВЛЕНИЕ";}
                                break;

                        }
                    }
                    break;


                    //установить зону обнаружения РЛО
                    case (quint8)0x30:
                    {
                        str = "Команда: Установить зону обнаружения";

                        switch((quint8)baMSG.at(8))
                        {
                            case (quint8)0x20: str.append(" 20м"); break;
                            case (quint8)0x40: str.append(" 40м"); break;
                            case (quint8)0x60: str.append(" 60м"); break;
                        }

                        return str;
                        break;
                    };


                    //установить чувствительность БСК-РВП или РЛД
                    case (quint8)0x40:
                    {
                        if(((quint8)baMSG.at(5) / 0x10) == (quint8)RVP_BSKDeviceType)//БСК-РВП
                        {
                            str = QString("Команда: Установить чувствительность  %1").arg((quint8)baMSG.at(8) + 1);
                        }

                        if(((quint8)baMSG.at(5) / 0x10) == (quint8)RLD_BSKDeviceType)//БСК-РЛД
                        {
                            //str = QString("Команда: Установить чувствительность  %1").arg((quint8)baMSG.at(8) + 1);
                            switch((quint8)baMSG.at(8))
                            {
                                case (quint8)0: str = "Команда: Установить минимальную чувствительность"; break;
                                case (quint8)1: str = "Команда: Установить среднюю чувствительность"; break;
                                case (quint8)2: str = "Команда: Установить чувствительность выше средней"; break;
                                case (quint8)3: str = "Команда: Установить максимальную чувствительность"; break;
                            }
                        }
                        return str;
                        break;
                    };



                    default:
                    {
                        return baMSG.toHex();
                        break;
                    };
                }
                break;
            };
        }
    }
    return str;
}

QColor ProxyMetod::msgColor_KS(bool ioType,
                               QByteArray baMSG) /*noexcept*/
{
    QColor result;
    result.setRgb(192, 192, 192);//Qt::lightGray;
    if(ioType)
    {
        result.setRgb(255, 255, 255);//Qt::white;
    }
    else
    {
        result.setRgb(192, 192, 192);//Qt::lightGray;
    }

    if((quint8)0xA5 !=(quint8)baMSG.at(0))
    {
        if(baMSG.contains("opening succeeded"))
        {
            result.setRgb(125, 255, 125);//Qt::green;
        }
        if(baMSG.contains("opening failed"))
        {
            result.setRgb(255, 255, 125);//Qt::yellow;
        }
        if(baMSG.contains("Deleted"))
        {
            result.setRgb(255, 125, 125);//Qt::red;
        }
        if(baMSG.contains("Port closed succeeded"))
        {
            result.setRgb(255, 125, 125);//Qt::red;
        }
        if(baMSG.contains("Lost connect"))
        {
            result.setRgb(255, 125, 125);//Qt::red;
        }
        if(baMSG.contains("Recovered connect"))
        {
            result.setRgb(125, 255, 125);//Qt::green;
        }
    }
    else
    {
        switch((quint8)baMSG.at(5))
        {
            case (quint8)0xA1:
            {
//                str.append("Контроль связи с База-ТВ по RS-485 интерфейсу");
                break;
            };

            case (quint8)0xCE:
            {
//                str.append("Таблица сети (%1 кол-во связей)");
                break;
            };

            case (quint8)0x0A:
            {
//                str.append("Квитанция на АРМ о приеме команды");
                break;
            };

            case (quint8)0x0D:
            {
//                str.append("Квитанция от АРМ на прием сообщения от База-ТВ");
                break;
            };

            case (quint8)0x1D:
            {
//                str.append("Квитанция от АРМ при приеме блока СК");
                break;
            };

            case (quint8)0x9F:
            {//Передача сообщения из эфира RF (короткое)
                switch((quint8)baMSG.at(6))
                {
                    case (quint8)0xC1:
                    {
//                        str.append("Сделан СК: ВК%1 CК%2 Гр%3 [%4]");
                        break;
                    };

                    case (quint8)0xC2:
                    {
//                        str.append("На ВК%1 СК не готов к передаче");
                        result.setRgb(255, 255, 125);//Qt::yellow;
                        break;
                    };

                    case (quint8)0xC3:
                    {
//                        str.append("ВК%1 приняла команду на формирование пакета СК");
                        break;
                    };

                    case (quint8)0xC4:
                    {
//                        str.append("Соответствие СК: ВК%1 CК%2 Гр%3. Тревоге: ");
                        break;
                    };

                    case (quint8)0xC5:
                    {
//                        str.append("ВК%1 CК%2 Гр%3 получен");
                        break;
                    };

                    case (quint8)0xCF:
                    {
//                        str.append("Неисправность модуля ВК%1");
                        result.setRgb(255, 255, 125);//Qt::yellow;
                        break;
                    };

                    case (quint8)0xCA:
                    {
//                        str.append("Отсутствие связи по RS-485 с ");
                        result.setRgb(255, 255, 125);//Qt::yellow;
                        break;
                    };

                    case (quint8)0xCB:
                    {
//                        str.append("Восстановление  связи по RS-485 c ");
                        result.setRgb(125, 255, 125);//Qt::green;
                        break;
                    };

                    case (quint8)0x90:
                    {
//                        str.append("Включено");
                        result.setRgb(125, 255, 125);//Qt::green;
                        break;
                    };

                    case (quint8)0x91:
                    {
//                        str.append("Отключено");
                        result.setRgb(125, 125, 125);//Qt::gray;
                        break;
                    };

                    case (quint8)0xBB:
                    {
//                        str.append("Разряд ИПА %1.%2 U,В");
                        result.setRgb(125, 125, 255);//Qt::blue;
                        break;
                    };

                    case (quint8)0xBC:
                    {
//                        str.append("Разряд ИПА %1.%2 U,В");
                        result.setRgb(125, 125, 255);//Qt::blue;
                        break;
                    };

                    case (quint8)0xBA:
                    {
//                        str.append("Норма ИПА %1.%2 U,В");
                        break;
                    };

                    case (quint8)0xBD:
                    {
//                        str.append("Отключение блока по разряду АКК  %1.%2 U,В");
                        result.setRgb(125, 125, 255);//Qt::blue;
                        break;
                    };

                    case (quint8)0x01:
                    {
//                        str.append("Тревога ");
                        result.setRgb(255, 125, 125);//Qt::red;

                        break;
                    };

                    case (quint8)0x05:
                    {
//                        str.append("ПредТревога ");
                        result.setRgb(255, 125, 125);//Qt::red;
                        break;
                    };

                    case (quint8)0x40:
                    {
//                        str.append("Неисправность ");
                        result.setRgb(255, 255, 125);//Qt::yellow;
                        break;
                    };

                    case (quint8)0x44:
                    {
//                        str.append("Команда на РМ-ТВ не дошла");
                        result.setRgb(255, 255, 125);//Qt::yellow;
                        break;
                    };

                    case (quint8)0x45:
                    {
//                        str.append("Привязка к ВК%1: %2");
                        break;
                    };

                    case (quint8)0x1F:
                    {
//                        str.append("Проведена инсталляция ВК%1[Режим:%2, Т1:%3, Т2:%4, Т3:%5]");
                        break;
                    };

                    case (quint8)0xD0:
                    {
//                        str.append("Норма ");
                        break;
                    };

                    case (quint8)0x20:
                    {
//                        str.append("Связь норма c ");
                        result.setRgb(125, 255, 125);//Qt::green;
                        break;
                    };

                    case (quint8)0x18:
                    {
//                        str.append("Состояние: ");
                        break;
                    };

                    case (quint8)0x19:
                    {
//                        str.append("Новый ведущий ");
                        break;
                    };

                    case (quint8)0x1D:
                    {
    //                        str.append("Есть связь с РМ-ТВ ");
                        break;
                    };

                    case (quint8)0xC0:
                    {
//                        str.append("Обрыв связи c ");
                        result.setRgb(255, 255, 125);//Qt::yellow;
                        break;
                    };

                    case (quint8)0xCC:
                    {
//                        str.append("Сообщение о выполненной команде");
                        break;
                    };

                    case (quint8)0xEF:
                    {
//                        str.append("Неверные параметры команды");
                        result.setRgb(255, 125, 125);//Qt::red;
                        break;
                    };

                    case (quint8)0x58:
                    {
//                        str.append("Ошибка теста памяти блока РМ-ТВ");
                        result.setRgb(255, 125, 125);//Qt::red;
                        break;
                    };

                    default:
                    {
//                        str.append("Не описанное сообщение:");
                        break;
                    }
                }

                break;
            };

            case (quint8)0x41:
            {//Передача команды в эфир
                switch((quint8)baMSG.at(8))
                {
                    case (quint8)0x42:
                    {
//                        str.append("Команда: Инсталляция ВК1[Режим:%1, Т1:%2, Т2:%3, Т3:%4]");
                        break;
                    }

                    case (quint8)0x43:
                    {
//                        str.append("Команда: Инсталляция ВК2[Режим:%1, Т1:%2, Т2:%3, Т3:%4]");
                        break;
                    }

                    case (quint8)0x0F:
                    {
//                        str.append("Команда: Проверка связи");
                        break;
                    }

                    case (quint8)0xFD:
                    {
//                        str.append("Команда: Запрос СК ВК%1");
                        break;
                    }

                    case (quint8)0x45:
                    {
//                        str.append("Команда: Запрос привязки");
                        break;
                    }

                    case (quint8)0x46:
                    {
//                        str.append("Команда: Очистить привязку  ВК%1");
                        break;
                    }

                    case (quint8)0x47:
                    {
//                        str.append("Команда: Добавить привязку  ВК%1 %2");
                        break;
                    }

                    case (quint8)0x48:
                    {
//                        str.append("Команда: Удалить привязку  ВК%1 %2");
                        break;
                    }

                    default:
                    {
//                        str.append("Не описанное сообщение:");
                        break;
                    }
                }
                break;
            };



            default:
            {
//                str.append("Не описанное сообщение:");
                break;
            }
        }
    }
    return result;
}

QString ProxyMetod::msgCommentRUS_KS(QByteArray baMSG) /*noexcept*/
{
    QString str;

    if((quint8)0xA5 !=(quint8)baMSG.at(0))
    {
        str = baMSG;
        if(baMSG.contains("Start session"))
        {
            str = "Начало сессии";
        }
        if(baMSG.contains("Complete session"))
        {
            str = "Конец сессии";
        }
        if(baMSG.contains("opening succeeded"))
        {
            str = "Открытие порта " + baMSG.split(' ').at(1) + " успешно";
        }
        if(baMSG.contains("opening failed"))
        {
            str = "Открытие порта " + baMSG.split(' ').at(1) + " не успешно";
        }
        if(baMSG.contains("Builded network"))
        {
            str = "Построена сеть";
        }
        if(baMSG.contains("Port opening succeeded"))
        {
            str = "Открытие порта успешно";
        }
        if(baMSG.contains("Port opening failed"))
        {
            str = "Открытие порта не успешно";
        }
        if(baMSG.contains("Reset alarm"))
        {
            str = "Сброс тревоги";
        }
        if(baMSG.contains("Reset failure"))
        {
            str = "Сброс неисправности";
        }
        if(baMSG.contains("Reset discharge"))
        {
            str = "Сброс разряда";
        }
        if(baMSG.contains("Deleted"))
        {
            str = "Удалено";
        }
        if(baMSG.contains("Port closed succeeded"))
        {
            str = "Порт закрыт";
        }
        if(baMSG.contains("Lost connect"))
        {
            str = "Превышенно ожидание нормального обмена сообщениями";
        }
        if(baMSG.contains("Recovered connect"))
        {
            str = "Нормальный обмен сообщениями";
        }
        return str;
    }
    else
    {
        switch((quint8)baMSG.at(5))
        {
            case (quint8)0xA1:
            {
                str = "Контроль связи с База-ТВ по RS-485 интерфейсу";
                return str;
                break;
            };

            case (quint8)0xCE:
            {
                quint16 count(((quint16)baMSG.at(6) * (quint16)0x0100) + (quint16)baMSG.at(7));
                str = "Таблица сети (%1 кол-во связей)";
                str = str.arg(count);
                return str;
                break;
            };

            case (quint8)0x0A:
            {
                str = "Квитанция на АРМ о приеме команды";
                return str;
                break;
            };

            case (quint8)0x0D:
            {
                str = "Квитанция от АРМ на прием сообщения от База-ТВ";
                return str;
                break;
            };

            case (quint8)0x1D:
            {
                str = "Квитанция от АРМ при приеме блока СК";
                return str;
                break;
            };

            case (quint8)0x9F:
            {//Передача сообщения из эфира RF (короткое)
                switch((quint8)baMSG.at(6))
                {
                    case (quint8)0x28:
                    {
                        str.append("Состояние блоков: ");
                        if((quint8)0x00 == (quint8)baMSG.at(12))
                        {
                            str.append("Режим[стандартный], ");
                        }
                        if((quint8)0x01 == (quint8)baMSG.at(12))
                        {
                            str.append("Режим[повышенного внимания], ");
                        }
                        if((quint8)0x00 != (quint8)baMSG.at(13))
                        {
                            str.append("Комплект РМ-433 №%1");
                            str = str.arg((quint8)baMSG.at(13));
                        }
                        else
                        {
                            str.append("РМ-433[нет связи]");
                        }

                        return str;
                        break;
                    };

                    case (quint8)0xC1:
                    {
                        str.append("Сделан СК: ВК%1 CК%2 Гр%3 [%4]");
                        str = str.arg((quint8)baMSG.at(17))
                                 .arg((quint8)(baMSG.at(12) & 0x0F))
                                 .arg(((quint8)((quint8)baMSG.at(12) & 0xF0) / 0x10))
                                 .arg((quint16)((quint8)baMSG.at(13) * (quint16)0x0100) + (quint8)baMSG.at(14));

//                        str = "Сделан СК: ВК%1 CК%2 Гр%3";
//                        str = str.arg((quint8)baMSG.at(17))
//                                 .arg((quint8)(baMSG.at(12) & (quint8)0x0F))
//                                 .arg(((quint8)((quint8)baMSG.at(12) & (quint8)0xF0) / (quint8)0x10));

                        return str;
                        break;
                    };

                    case (quint8)0xC2:
                    {
                        str = "На ВК%1 СК не готов к передаче";
                        str = str.arg((quint8)baMSG.at(12));
                        return str;
                        break;
                    };

                    case (quint8)0xC3:
                    {
                        str = "ВК%1 приняла команду на формирование пакета СК";
                        str = str.arg((quint8)baMSG.at(12));
                        return str;
                        break;
                    };

                    case (quint8)0xC4:
                    {
                        str = "Соответствие СК: ВК%1 CК%2 Гр%3. Тревоге: ";
                        str = str.arg((quint8)baMSG.at(12))
                                 .arg((quint8)(baMSG.at(13) & (quint8)0x0F))
                                 .arg(((quint8)baMSG.at(13) / (quint8)0x10) & (quint8)0x0F);

                        quint16 SO(((quint16)baMSG.at(14) * (quint16)0x0100) + (quint16)baMSG.at(15));
                        switch(SO)
                        {
                            case (quint16)0x0000:
                            {
                                str.append("Команда с АРМ");
                                break;
                            }

                            case (quint16)0x0001:
                            {
                                str.append("Внешнее СО1");
                                break;
                            }

                            case (quint16)0x0002:
                            {
                                str.append("Внешнее СО2");
                                break;
                            }

                            case (quint16)0x000B:
                            {
                                str.append("Встроенное СО");
                                break;
                            }

                            default:
                            {
                                str.append(getNameSO_RUS(baMSG.mid(14, 2)));
                                str.append("[%1]");
                                str = str.arg(QString(baMSG.mid(14, 2).toHex().toUpper()));
                                break;
                            }
                        }

                        return str;
                        break;
                    };

                    case (quint8)0xC5:
                    {
                        str.append("ВК%1 CК%2 Гр%3 получен");
                        str = str.arg((quint8)baMSG.at(12))
                                 .arg((quint8)(baMSG.at(13) & (quint8)0x0F))
                                 .arg(((quint8)baMSG.at(13) / (quint8)0x10) & (quint8)0x0F);
                        return str;
                        break;
                    };

                    case (quint8)0xCF:
                    {
                        str = "Неисправность модуля ВК%1";
                        str = str.arg((quint8)baMSG.at(12));
                        return str;
                        break;
                    };

                    case (quint8)0xCA:
                    {
                        str = "Отсутствие связи по RS-485 с ";
                        switch((quint8)baMSG.at(12))
                        {
                            case (quint8)0x09:
                            {
                                str.append("РМ-433");
                                break;
                            }

                            case (quint8)0x0C:
                            {
                                str.append("КК");
                                break;
                            }

                            case (quint8)0x0E:
                            {
                                str.append("БАРС");
                                break;
                            }

                            case (quint8)0x0B:
                            {
                                str.append("ВК%1");
                                str = str.arg((quint8)baMSG.at(13));
                                break;
                            }
                            default:
                            {
                                str.append("Неописанным устройством");
                                break;
                            }
                        }

                        return str;
                        break;
                    };

                    case (quint8)0xCB:
                    {
                        str = "Восстановление  связи по RS-485 c ";
                        switch((quint8)baMSG.at(12))
                        {
                            case (quint8)0x09:
                            {
                                str.append("РМ-433");
                                str.append(" комплект №%1");
                                str = str.arg((quint8)baMSG.at(13));
                                break;
                            }

                            case (quint8)0x0C:
                            {
                                str.append("КК");
                                break;
                            }

                            case (quint8)0x0E:
                            {
                                str.append("БАРС");
                                break;
                            }

                            case (quint8)0x0B:
                            {
                                str.append("ВК%1");
                                str = str.arg((quint8)baMSG.at(13));
                                break;
                            }
                            default:
                            {
                                str.append("Неописанным устройством");
                                break;
                            }

                        }

                        return str;
                        break;
                    };

                    case (quint8)0x90:
                    {
                        str = "Включено";

                        str.append(" Тип ИПА[%1]");
                        QString typeIPA(getTypeIPA_RUS((quint8)baMSG.at(12)));
                        str = str.arg(typeIPA);

                        //if((quint8)baMSG.at(16))
                        int t((64 < (quint8)baMSG.at(16)) ? ((quint8)baMSG.at(16) - (quint8)64) : (-1 * ((quint8)64 - (quint8)baMSG.at(16))));
                        if((-60 < t) && (50 > t))
                        {
                            str.append(" [Температура %1°C]");
                            str = str.arg(t);
                        }

                        return str;
                        break;
                    };

                    case (quint8)0x91:
                    {
                        str = "Отключено";
                        //if((quint8)baMSG.at(16))
                        int t((64 < (quint8)baMSG.at(16)) ? ((quint8)baMSG.at(16) - (quint8)64) : (-1 * ((quint8)64 - (quint8)baMSG.at(16))));
                        if((-60 < t) && (50 > t))
                        {
                            str.append(" [Температура %1°C]");
                            str = str.arg(t);
                        }

                        return str;
                        break;
                    };

                    case (quint8)0x58:
                    {
                        str = "Тест памяти [%1]";
                        switch((quint8)baMSG.at(12))
                        {
                            case (quint8)0x00:
                            {
                                str = str.arg("Норма");
                                break;
                            };
                            case (quint8)0x01:
                            {
                                str = str.arg("Ошибка A0,D0-D7");
                                break;
                            };
                            default:
                            {
                                str = str.arg("Ошибка A%1");
                                str = str.arg((quint8)baMSG.at(12) - 1);
                                break;
                            };
                        }
                        return str;
                        break;
                    };

                    case (quint8)0xBB:
                    {
                        str = "Разряд ИПА %1.%2 U,В";
                        str = str.arg((quint8)baMSG.at(17) / 0x10)
                                 .arg((quint8)baMSG.at(17) & 0x0F);

                        str.append(" Тип ИПА[%1]");
                        QString typeIPA(this->getTypeIPA_RUS((quint8)baMSG.at(12)));
                        str = str.arg(typeIPA);

                        //if((quint8)baMSG.at(16))
                        int t((64 < (quint8)baMSG.at(16)) ? ((quint8)baMSG.at(16) - (quint8)64) : (-1 * ((quint8)64 - (quint8)baMSG.at(16))));
                        if((-60 < t) && (50 > t))
                        {
                            str.append(" [Температура %1°C]");
                            str = str.arg(t);
                        }

                        return str;
                        break;
                    };

                    case (quint8)0xBC:
                    {
                        str = "Полный разряд ИПА %1.%2 U,В";
                        str = str.arg((quint8)baMSG.at(17) / 0x10)
                                 .arg((quint8)baMSG.at(17) & 0x0F);

                        str.append(" Тип ИПА[%1]");
                        QString typeIPA(this->getTypeIPA_RUS((quint8)baMSG.at(12)));
                        str = str.arg(typeIPA);

                        //if((quint8)baMSG.at(16))
                        int t((64 < (quint8)baMSG.at(16)) ? ((quint8)baMSG.at(16) - (quint8)64) : (-1 * ((quint8)64 - (quint8)baMSG.at(16))));
                        if((-60 < t) && (50 > t))
                        {
                            str.append(" [Температура %1°C]");
                            str = str.arg(t);
                        }

                        return str;
                        break;
                    };

                    case (quint8)0xBA:
                    {
                        str = "Норма ИПА %1.%2 U,В";
                        str = str.arg((quint8)baMSG.at(17) / 0x10)
                                 .arg((quint8)baMSG.at(17) & 0x0F);

                        str.append(" Тип ИПА[%1]");
                        QString typeIPA(this->getTypeIPA_RUS((quint8)baMSG.at(12)));
                        str = str.arg(typeIPA);

                        //if((quint8)baMSG.at(16))
                        int t((64 < (quint8)baMSG.at(16)) ? ((quint8)baMSG.at(16) - (quint8)64) : (-1 * ((quint8)64 - (quint8)baMSG.at(16))));
                        if((-60 < t) && (50 > t))
                        {
                            str.append(" [Температура %1°C]");
                            str = str.arg(t);
                        }

                        return str;
                        break;
                    };

                    case (quint8)0xBD:
                    {
                        str = "Отключение блока по разряду АКК  %1.%2 U,В";
                        str = str.arg((quint8)baMSG.at(17) / 0x10)
                                 .arg((quint8)baMSG.at(17) & 0x0F);

                        str.append(" Тип ИПА[%1]");
                        QString typeIPA(this->getTypeIPA_RUS((quint8)baMSG.at(12)));
                        str = str.arg(typeIPA);

                        //if((quint8)baMSG.at(16))
                        int t((64 < (quint8)baMSG.at(16)) ? ((quint8)baMSG.at(16) - (quint8)64) : (-1 * ((quint8)64 - (quint8)baMSG.at(16))));
                        if((-60 < t) && (50 > t))
                        {
                            str.append(" [Температура %1°C]");
                            str = str.arg(t);
                        }

                        return str;
                        break;
                    };

                    case (quint8)0x01:
                    {
                        str = "Тревога: ";

                        quint16 SO(((quint16)baMSG.at(12) * (quint16)0x0100) + (quint16)baMSG.at(13));

                        switch(SO)
                        {
                            case (quint16)0x000B:
                            {
                                str.append("Встроенное СО");
                                break;
                            }
                            case (quint16)0x0001:
                            {
                                str.append("Внешнее СО1");
                                break;
                            }
                            case (quint16)0x0002:
                            {
                                str.append("Внешнее СО2");
                                break;
                            }
                            default:
                            {
                                str.append(getNameSO_RUS(baMSG.mid(12, 2)));
                                str.append("[%1]");
                                str = str.arg(QString(baMSG.mid(12, 2).toHex().toUpper()));
                                str.append(" Уч%1 СО%2");
                                str = str.arg((quint8)baMSG.at(14))
                                         .arg((quint8)baMSG.at(15));
                            }
                        }

                        //if((quint8)baMSG.at(16))
                        int t((64 < (quint8)baMSG.at(16)) ? ((quint8)baMSG.at(16) - (quint8)64) : (-1 * ((quint8)64 - (quint8)baMSG.at(16))));
                        if((-60 < t) && (50 > t))
                        {
                            str.append(" [Температура %1°C]");
                            str = str.arg(t);
                        }

                        return str;
                        break;
                    };

                    case (quint8)0x05:
                    {
                        str = "ПредТревога Встроенное СО";
                        return str;
                        break;
                    };

                    case (quint8)0x40:
                    {
                        str = "Неисправность ";
                        quint16 SO(((quint16)baMSG.at(14) * (quint16)0x0100) + (quint16)baMSG.at(15));
                        switch(SO)
                        {
                            case (quint16)0x0001:
                            {
                                str.append("Внешнее СО1");
                                break;
                            }

                            case (quint16)0x0002:
                            {
                                str.append("Внешнее СО2");
                                break;
                            }

                            case (quint16)0x000B:
                            {
                                str.append("Встроенное СО");
                                break;
                            }

                            default:
                            {
                                str.append(getNameSO_RUS(baMSG.mid(14, 2)));
                                str.append("[%1]");
                                str = str.arg(QString(baMSG.mid(14, 2).toHex().toUpper()));
                                break;
                            }
                        }

                        return str;
                        break;
                    };

                    case (quint8)0x44:
                    {
                        str = "Команда на РМ-ТВ не дошла";

                        return str;
                        break;
                    };

                    case (quint8)0x45:
                    {
                        str = "Обновлён список привязанных СО к ВК%1";
                        str = str.arg((quint8)baMSG.at(12));

                        return str;
                        break;
                    };

                    case (quint8)0x49:
                    {
//                    str.append("Освещённость ВК%1: Уровень[%2], Порог[%3], Режим съёмки[%4]");
////                        str = str.arg((quint8)baMSG.at(12))
////                                 .arg((0x00 == (quint8)baMSG.at(13)) ? "Неизвестно" : (quint8)baMSG.at(13))
////                                 .arg((0x00 == (quint8)baMSG.at(14)) ? "Неизвестно" : (quint8)baMSG.at(14))
////                                 .arg((0x00 == (quint8)baMSG.at(15)) ? "День" : "Ночь");
//                    str = str.arg((quint8)baMSG.at(12))
//                             .arg((quint8)baMSG.at(13))
//                             .arg((quint8)baMSG.at(14))
//                             .arg((0x00 == (quint8)baMSG.at(15)) ? "День" : "Ночь");
                    str = "Освещённость ВК%1: Режим съёмки[%2]";
                    str = str.arg((quint8)baMSG.at(12))
                             .arg((0x00 == (quint8)baMSG.at(15)) ? "День" : "Ночь");

                        int t((64 < (quint8)baMSG.at(16)) ? ((quint8)baMSG.at(16) - (quint8)64) : (-1 * ((quint8)64 - (quint8)baMSG.at(16))));
                        if((-60 < t) && (50 > t))
                        {
                            str.append(" [Температура %1°C]");
                            str = str.arg(t);
                        }

                        return str;
                        break;
                    };

                    case (quint8)0x1F:
                    {
                        str = "Проведена инсталляция ВК%1[Режим:%2, Т1:%3, Т2:%4, Т3:%5]";
                        str = str.arg((quint8)baMSG.at(12))
                                 .arg((quint8)baMSG.at(13))
                                 .arg((quint8)baMSG.at(14))
                                 .arg((quint8)baMSG.at(15))
                                 .arg((quint8)baMSG.at(16));

                        return str;
                        break;
                    };

                    case (quint8)0xD0:
                    {
                        str = "Норма: ";

                        quint16 SO(((quint16)baMSG.at(12) * (quint16)0x0100) + (quint16)baMSG.at(13));

                        switch(SO)
                        {
                            case (quint16)0x000B:
                            {
                                str.append("Встроенное СО");
                                break;
                            }
                            case (quint16)0x0001:
                            {
                                str.append("Внешнее СО1");
                                break;
                            }
                            case (quint16)0x0002:
                            {
                                str.append("Внешнее СО2");
                                break;
                            }
                            default:
                            {
                                str.append(getNameSO_RUS(baMSG.mid(12, 2)));
                                str.append("[%1]");
                                str = str.arg(QString(baMSG.mid(12, 2).toHex().toUpper()));
                            }
                        }

                        return str;
                        break;
                    };

                    case (quint8)0x20:
                    {
                        TypeUnitNodeDevice typeVed((TypeUnitNodeDevice)(((quint8)baMSG.at(7) / 0x10) & 0x0F));
                        str = "Связь норма c ";

                        QString connectUN;
                        switch((quint8)typeVed)
                        {
                            case (quint8)CPP_KSDeviceType:
                            {
                                connectUN = "АРМ[%1]";
                                break;
                            }
                            case (quint8)RT01_KSDeviceType:
                            {
                                connectUN = "РТ[%1]";
                                break;
                            }case (quint8)RT02_KSDeviceType:
                            {
                                connectUN = "РТ[%1]";
                                break;
                            }
                            case (quint8)VK09_KSDeviceType:
                            {
                                connectUN = "ВК[%1]";
                                break;
                            }
                            case (quint8)VK0A_KSDeviceType:
                            {
                                connectUN = "ВК[%1]";
                                break;
                            }
                            case (quint8)VK0B_KSDeviceType:
                            {
                                connectUN = "ВК[%1]";
                                break;
                            }
                            default:
                            {
                                connectUN = "UN[%1]";
                                break;
                            }
                        }
                        connectUN = connectUN.arg(QString(baMSG.mid(7, 2).toHex().toUpper())).toUpper();
                        str.append(connectUN);

                        str.append(" Уч%1 №%2");
                        str = str.arg((quint8)baMSG.at(10))
                                 .arg((quint8)baMSG.at(11));

                        return str;
                        break;
                    };

                    case (quint8)0x18:
                    {
                        str = "Состояние: ";

                        if((quint8)0x80 & (quint8)baMSG.at(12))
                        {
                            str.append("ВстроенноеСО[Откл], ");
                        }
                        if((quint8)0x40 & (quint8)baMSG.at(12))
                        {
                            str.append("ВнешнееСО1[Откл], ");
                        }
                        if((quint8)0x20 & (quint8)baMSG.at(12))
                        {
                            str.append("ВнешнееСО2[Откл], ");
                        }


                        str.append("РМ-433[%1], ");
                        switch((quint8)baMSG.at(12) & (quint8)0x0F)
                        {
                            case (quint8)0x00:
                            {
                                str = str.arg("нет");
                                break;
                            }
                            case (quint8)0x01:
                            {
                                str = str.arg("есть");
                                break;
                            }
                            case (quint8)0x04:
                            {
                                str = str.arg("неисправность");
                                break;
                            }
//                            case (quint8)0x55:
//                            {
//                                str = str.arg("не определённое");
//                                break;
//                            }

                            default:
                            {
                                str = str.arg("не описанное");
                                break;
                            }
                        }

                        str.append("RSSI[%1], ");
                        str = str.arg((quint8)baMSG.at(13));

                        str.append("ВК1[%1], ");
                        switch((quint8)baMSG.at(14))
                        {
                            case (quint8)0x00:
                            {
                                str = str.arg("нет");
                                break;
                            }
                            case (quint8)0x01:
                            {
                                str = str.arg("есть");
                                break;
                            }
                            case (quint8)0x04:
                            {
                                str = str.arg("неисправность");
                                break;
                            }
                            case (quint8)0x11:
                            {
                                str = str.arg("1й режим");
                                break;
                            }
                            case (quint8)0x12:
                            {
                                str = str.arg("2й режим");
                                break;
                            }
                            case (quint8)0x13:
                            {
                                str = str.arg("3й режим");
                                break;
                            }
                            case (quint8)0x31:
                            {
                                str = str.arg("1й режим День");
                                break;
                            }
                            case (quint8)0x32:
                            {
                                str = str.arg("2й режим День");
                                break;
                            }
                            case (quint8)0x33:
                            {
                                str = str.arg("3й режим День");
                                break;
                            }
                            case (quint8)0xE1:
                            {
                                str = str.arg("1й режим Ночь");
                                break;
                            }
                            case (quint8)0xE2:
                            {
                                str = str.arg("2й режим Ночь");
                                break;
                            }
                            case (quint8)0xE3:
                            {
                                str = str.arg("3й режим Ночь");
                                break;
                            }
                            case (quint8)0x55:
                            {
                                str = str.arg("не определённое");
                                break;
                            }

                            default:
                            {
                                str = str.arg("не описанное");
                                break;
                            }
                        }

                        str.append("ВК2[%1], ");
                        switch((quint8)baMSG.at(15))
                        {
                            case (quint8)0x00:
                            {
                                str = str.arg("нет");
                                break;
                            }
                            case (quint8)0x01:
                            {
                                str = str.arg("есть");
                                break;
                            }
                            case (quint8)0x04:
                            {
                                str = str.arg("неисправность");
                                break;
                            }
                            case (quint8)0x11:
                            {
                                str = str.arg("1й режим");
                                break;
                            }
                            case (quint8)0x12:
                            {
                                str = str.arg("2й режим");
                                break;
                            }
                            case (quint8)0x13:
                            {
                                str = str.arg("3й режим");
                                break;
                            }
                            case (quint8)0x31:
                            {
                                str = str.arg("1й режим День");
                                break;
                            }
                            case (quint8)0x32:
                            {
                                str = str.arg("2й режим День");
                                break;
                            }
                            case (quint8)0x33:
                            {
                                str = str.arg("3й режим День");
                                break;
                            }
                            case (quint8)0xE1:
                            {
                                str = str.arg("1й режим Ночь");
                                break;
                            }
                            case (quint8)0xE2:
                            {
                                str = str.arg("2й режим Ночь");
                                break;
                            }
                            case (quint8)0xE3:
                            {
                                str = str.arg("3й режим Ночь");
                                break;
                            }
                            case (quint8)0x55:
                            {
                                str = str.arg("не определённое");
                                break;
                            }

                            default:
                            {
                                str = str.arg("не описанное");
                                break;
                            }
                        }

//                        str.append("БАРС %1");

                        switch((quint8)baMSG.at(16))
                        {
                            case (quint8)0x00:
                            {
                                str.append("БАРС[%1]");
                                str = str.arg("нет");
                                break;
                            }
                            case (quint8)0x01:
                            {
                                str.append("БАРС[%1]");
                                str = str.arg("есть");
                                break;
                            }
                            case (quint8)0x04:
                            {
                                str.append("БАРС[%1]");
                                str = str.arg("неисправность");
                                break;
                            }
//                            case (quint8)0x55:
//                            {
//                                str.append("БАРС[%1]");
//                                str = str.arg("не определённое");
//                                break;
//                            }

                            default:
                            {
                                str.append("ИПА[%1]");
                                QString typeIPA(this->getTypeIPA_RUS((quint8)baMSG.at(16)));
                                str = str.arg(typeIPA);
                                break;
                            }
                        }

                        return str;
                        break;
                    };

                    case (quint8)0x19:
                    {
                        str = "Новый ведущий ";
                        TypeUnitNodeDevice typeMaster((TypeUnitNodeDevice)(((quint8)baMSG.at(12) / 0x10) & 0x0F));
//                        quint8 typeVed(((quint8)baMSG.at(12) / 0x10);
                        QString strMasterUN;
                        switch((quint8)typeMaster)
                        {
                            case (quint8)CPP_KSDeviceType:
                            {
                                strMasterUN = "АРМ[%1]";
                                break;
                            }
                            case (quint8)RT01_KSDeviceType:
                            {
                                strMasterUN = "РТ[%1]";
                                break;
                            }case (quint8)RT02_KSDeviceType:
                            {
                                strMasterUN = "РТ[%1]";
                                break;
                            }
                            case (quint8)VK09_KSDeviceType:
                            {
                                strMasterUN = "ВК[%1]";
                                break;
                            }
                            case (quint8)VK0A_KSDeviceType:
                            {
                                strMasterUN = "ВК[%1]";
                                break;
                            }
                            case (quint8)VK0B_KSDeviceType:
                            {
                                strMasterUN = "ВК[%1]";
                                break;
                            }
                        }
                        strMasterUN = strMasterUN.arg(QString(baMSG.mid(12, 2).toHex().toUpper())).toUpper();
                        str.append(strMasterUN);
                        str.append(" RSSI[%1]");
                        str = str.arg((quint8)baMSG.at(14));
                        return str;
                        break;
                    };

                    case (quint8)0x1D:
                    {
                        str = "Есть связь с ";
                        TypeUnitNodeDevice typeMaster((TypeUnitNodeDevice)(((quint8)baMSG.at(12) / (quint8)0x10) & (quint8)0x0F));
//                        quint8 typeVed(((quint8)baMSG.at(12) / 0x10);
                        QString strMasterUN;
                        switch((quint8)typeMaster)
                        {
                            case (quint8)CPP_KSDeviceType:
                            {
                                strMasterUN = "АРМ[%1]";
                                break;
                            }
                            case (quint8)RT01_KSDeviceType:
                            {
                                strMasterUN = "РТ[%1]";
                                break;
                            }
                            case (quint8)RT02_KSDeviceType:
                            {
                                strMasterUN = "РТ[%1]";
                                break;
                            }
                            case (quint8)VK09_KSDeviceType:
                            {
                                strMasterUN = "ВК[%1]";
                                break;
                            }
                            case (quint8)VK0A_KSDeviceType:
                            {
                                strMasterUN = "ВК[%1]";
                                break;
                            }
                            case (quint8)VK0B_KSDeviceType:
                            {
                                strMasterUN = "ВК[%1]";
                                break;
                            }
                        }
                        strMasterUN = strMasterUN.arg(QString(baMSG.mid(12, 2).toHex().toUpper())).toUpper();
                        str.append(strMasterUN);
                        str.append(" RSSI[%1]");
                        str = str.arg((quint8)baMSG.at(14));
                        return str;
                        break;
                    };

                    case (quint8)0xC0:
                    {
                        str = "Обрыв связи c ";

                        TypeUnitNodeDevice typeMaster((TypeUnitNodeDevice)(((quint8)baMSG.at(7) / (quint8)0x10) & (quint8)0x0F));
                        QString lostUN;
                        switch((quint8)typeMaster)
                        {
                            case (quint8)CPP_KSDeviceType:
                            {
                                lostUN = "АРМ[%1]";
                                break;
                            }
                            case (quint8)RT01_KSDeviceType:
                            {
                                lostUN = "РТ[%1]";
                                break;
                            }
                            case (quint8)RT02_KSDeviceType:
                            {
                                lostUN = "РТ[%1]";
                                break;
                            }
                            case (quint8)VK09_KSDeviceType:
                            {
                                lostUN = "ВК[%1]";
                                break;
                            }
                            case (quint8)VK0A_KSDeviceType:
                            {
                                lostUN = "ВК[%1]";
                                break;
                            }
                            case (quint8)VK0B_KSDeviceType:
                            {
                                lostUN = "ВК[%1]";
                                break;
                            }
                            default:
                            {
                                lostUN = "UN[%1]";
                                break;
                            }
                        }
                        lostUN = lostUN.arg(QString(baMSG.mid(7, 2).toHex().toUpper())).toUpper();
                        str.append(lostUN);

                        if((quint8)baMSG.at(10) ||
                           (quint8)baMSG.at(11))
                        {
                            str.append(" Уч%1 №%2");
                            str = str.arg((quint8)baMSG.at(10))
                                     .arg((quint8)baMSG.at(11));
                        }

                        return str;
                        break;
                    };

                    case (quint8)0xCC:
                    {
//                        str.append("Сообщение о выполненной команде");
//                        return str;
                        switch ((quint8)baMSG.at(12))
                        {
                            case (quint8)0x01:
                            {
//                                str = "Принята команда: Отключение %1";
                                str = "Проведена команда: Отключено ";

                                quint16 SO(((quint16)baMSG.at(13) * (quint16)0x0100) + (quint16)baMSG.at(14));

                                switch(SO)
                                {
                                    case (quint16)0x000B:
                                    {
                                        str.append("Встроенное СО");
                                        break;
                                    }
                                    case (quint16)0x0001:
                                    {
                                        str.append("Внешнее СО1");
                                        break;
                                    }
                                    case (quint16)0x0002:
                                    {
                                        str.append("Внешнее СО2");
                                        break;
                                    }
                                    default:
                                    {
                                        str.append(getNameSO_RUS(baMSG.mid(13, 2)));
                                        str.append("[%1]");
                                        str = str.arg(QString(baMSG.mid(13, 2).toHex().toUpper()));
                                    }
                                }

                                return str;
                                break;
                            }

                            case (quint8)0x02:
                            {
//                                str = "Принята команда: Включение %1";
                                str = "Проведена команда: Включено ";

                                quint16 SO(((quint16)baMSG.at(13) * (quint16)0x0100) + (quint16)baMSG.at(14));

                                switch(SO)
                                {
                                    case (quint16)0x000B:
                                    {
                                        str.append("Встроенное СО");
                                        break;
                                    }
                                    case (quint16)0x0001:
                                    {
                                        str.append("Внешнее СО1");
                                        break;
                                    }
                                    case (quint16)0x0002:
                                    {
                                        str.append("Внешнее СО2");
                                        break;
                                    }
                                    default:
                                    {
                                        str.append(getNameSO_RUS(baMSG.mid(13, 2)));
                                        str.append("[%1]");
                                        str = str.arg(QString(baMSG.mid(13, 2).toHex().toUpper()));
                                    }
                                }

                                return str;
                                break;
                            }

                            case (quint8)0x03:
                            {
//                                str = "Принята команда: Установить комплект РМ-433 №%1";
                                str = "Проведена команда: Установлен комплект РМ-433 №%1";
                                str = str.arg((quint8)baMSG.at(13));
                                return str;
                                break;
                            }

                            case (quint8)0x42:
                            {
                                str.append("Проведена команда: Инсталляция ВК1[Режим:%1, Т1:%2, Т2:%3, Т3:%4]");
                                str = str.arg((quint8)baMSG.at(13))
                                         .arg((quint8)baMSG.at(14))
                                         .arg((quint8)baMSG.at(15))
                                         .arg((quint8)baMSG.at(16));
                                return str;
                                break;
                            }

                            case (quint8)0x43:
                            {
                                str.append("Проведена команда: Инсталляция ВК2[Режим:%1, Т1:%2, Т2:%3, Т3:%4]");
                                str = str.arg((quint8)baMSG.at(13))
                                         .arg((quint8)baMSG.at(14))
                                         .arg((quint8)baMSG.at(15))
                                         .arg((quint8)baMSG.at(16));
                                return str;
                                break;
                            }

                            case (quint8)0x0F:
                            {
                                str.append("Принята команда: Проверка связи");
                                return str;
                                break;
                            }

                            case (quint8)0xFD:
                            {
                                str.append("Принята команда: Запрос СК ВК%1");
                                str = str.arg((quint8)baMSG.at(13));
                                return str;
                                break;
                            }

                            case (quint8)0xFA:
                            {
                                str = "Принята команда: Запрос Тест СК";
                                return str;
                                break;
                            }

                            case (quint8)0x45:
                            {
                                str.append("Проведена команда: Запрос привязки");
                                return str;
                                break;
                            }

                            case (quint8)0x46:
                            {
                                str.append("Проведена команда: Очистить привязку СО к ВК%1");
                                str = str.arg((quint8)baMSG.at(13));
                                return str;
                                break;
                            }

                            case (quint8)0x47:
                            {
                                str.append("Проведена команда: Добавить привязку к ВК%1 %2[%3]");
                                str = str.arg((quint8)baMSG.at(13))
                                         .arg(getNameSO_RUS(baMSG.mid(15, 2)))
                                         .arg(QString(baMSG.mid(15, 2).toHex().toUpper()));
                                return str;
                                break;
                            }

                            case (quint8)0x48:
                            {
                                str.append("Проведена команда: Удалить привязку от ВК%1 %2[%3]");
                                str = str.arg((quint8)baMSG.at(13))
                                         .arg(getNameSO_RUS(baMSG.mid(15, 2)))
                                         .arg(QString(baMSG.mid(15, 2).toHex().toUpper()));
                                return str;
                                break;
                            }

                            case (quint8)0x49:
                            {
                                if((quint8)0x55 == (quint8)baMSG.at(15))
                                {
                                    str = "Проведена команда: Запиать в ВК%1 порог освещённости %2";
                                    str = str.arg((quint8)baMSG.at(12))
                                             .arg((quint8)baMSG.at(14));
                                }
                                if((quint8)0xEE == (quint8)baMSG.at(15))
                                {
                                    str = "Проведена команда: Чтение с ВК%1 уровня и порога освещённости";
                                    str = str.arg((quint8)baMSG.at(12));
                                }

                                return str;
                                break;
                            }

                            case (quint8)0x78:
                            {
                                str = "Проведена команда: Установить Уч%1 Ном%2";
                                str = str.arg((quint8)baMSG.at(13))
                                         .arg((quint8)baMSG.at(14));
                                return str;
                                break;
                            }

                            default:
                            {
                                str = "Принята не описанная команда:";
                                str.append(baMSG.toHex().toUpper());
                                return str;
                                break;
                            }
                        }
                        break;
                    };

                    case (quint8)0xEF:
                    {
                        str = "Неверные параметры команды ";
                        switch((quint8)baMSG.at(12))
                        {
                            case (quint8)0x01:
                            {
                                str.append("[Отключение CO]");
                                break;
                            }
                            case (quint8)0x02:
                            {
                                str.append("[Включение CO]");
                                break;
                            }
                            case (quint8)0x03:
                            {
                                str.append("[Установить комплект]");
                                break;
                            }
                            case (quint8)0x42:
                            {
                                str.append("[Инсталляция ВК1]");
                                break;
                            }
                            case (quint8)0x43:
                            {
                                str.append("[Инсталляция ВК2]");
                                break;
                            }
                            case (quint8)0x0F:
                            {
                                str.append("[Проверка связи]");
                                break;
                            }
                            case (quint8)0xFD:
                            {
                                str.append("[Запрос СК]");
                                break;
                            }
                            case (quint8)0xFA:
                            {
                                str.append("[Запрос Тест СК]");
                                break;
                            }
                            case (quint8)0x45:
                            {
                                str.append("[Запрос привязки]");
                                break;
                            }
                            case (quint8)0x47:
                            {
                                str.append("[Добавить привязку]");
                                break;
                            }
                            case (quint8)0x48:
                            {
                                str.append("[Удалить привязку]");
                                break;
                            }
                            case (quint8)0x49:
                            {
                                str.append("[Освещённость]");
                                break;
                            }
                            case (quint8)0x78:
                            {
                                str.append("[Установить Уч и Ном]");
                                break;
                            }
                            default:
                            {
                                str.append("[Command:%1]");
                                str = str.arg(QString(baMSG.mid(12, 1).toHex().toUpper()));
                                break;
                            }
                        }
                        str.append(" [D1:%1,D2:%2,D3:%3,D4:%4]");
                        str = str.arg(QString(baMSG.mid(13, 1).toHex().toUpper()))
                                 .arg(QString(baMSG.mid(14, 1).toHex().toUpper()))
                                 .arg(QString(baMSG.mid(15, 1).toHex().toUpper()))
                                 .arg(QString(baMSG.mid(16, 1).toHex().toUpper()));

                        return str;
                        break;
                    };

                    default:
                    {
                        str.append("Не описанное сообщение:");
                        str.append(baMSG.toHex().toUpper());
                        return str;
                        break;
                    }
                }
                return str;
                break;
            };

            case (quint8)0x41:
            {//Передача команды в эфир
                switch((quint8)baMSG.at(8))
                {
                    case (quint8)0x01:
                    {
                        str = "Команда: Отключение ";

                        quint16 SO(((quint16)baMSG.at(9) * (quint16)0x0100) + (quint16)baMSG.at(10));

                        switch(SO)
                        {
                            case (quint16)0x000B:
                            {
                                str.append("Встроенное СО");
                                break;
                            }
                            case (quint16)0x0001:
                            {
                                str.append("Внешнее СО1");
                                break;
                            }
                            case (quint16)0x0002:
                            {
                                str.append("Внешнее СО2");
                                break;
                            }
                            default:
                            {
                                str.append(getNameSO_RUS(baMSG.mid(9, 2)));
                                str.append("[%1]");
                                str = str.arg(QString(baMSG.mid(9, 2).toHex().toUpper()));
                            }
                        }

                        return str;
                        break;
                    }

                    case (quint8)0x02:
                    {
                        str = "Команда: Включение ";

                        quint16 SO(((quint16)baMSG.at(9) * (quint16)0x0100) + (quint16)baMSG.at(10));

                        switch(SO)
                        {
                            case (quint16)0x000B:
                            {
                                str.append("Встроенное СО");
                                break;
                            }
                            case (quint16)0x0001:
                            {
                                str.append("Внешнее СО1");
                                break;
                            }
                            case (quint16)0x0002:
                            {
                                str.append("Внешнее СО2");
                                break;
                            }
                            default:
                            {
                                str.append(getNameSO_RUS(baMSG.mid(9, 2)));
                                str.append("[%1]");
                                str = str.arg(QString(baMSG.mid(9, 2).toHex().toUpper()));
                            }
                        }

                        return str;
                        break;
                    }

                    case (quint8)0x03:
                    {
                        str = "Команда: Установить комплект РМ-433 №%1";
                        str = str.arg((quint8)baMSG.at(9));
                        return str;
                        break;
                    }

                    case (quint8)0x42:
                    {
                        str.append("Команда: Инсталляция ВК1[Режим:%1, Т1:%2, Т2:%3, Т3:%4]");
                        str = str.arg((quint8)baMSG.at(9))
                                 .arg((quint8)baMSG.at(10))
                                 .arg((quint8)baMSG.at(11))
                                 .arg((quint8)baMSG.at(12));
                        return str;
                        break;
                    }

                    case (quint8)0x43:
                    {
                        str.append("Команда: Инсталляция ВК2[Режим:%1, Т1:%2, Т2:%3, Т3:%4]");
                        str = str.arg((quint8)baMSG.at(9))
                                 .arg((quint8)baMSG.at(10))
                                 .arg((quint8)baMSG.at(11))
                                 .arg((quint8)baMSG.at(12));
                        return str;
                        break;
                    }

                    case (quint8)0x0F:
                    {
                        str.append("Команда: Проверка связи");
                        return str;
                        break;
                    }

                    case (quint8)0xFD:
                    {
                        str.append("Команда: Запрос СК ВК%1");
                        str = str.arg((quint8)baMSG.at(9));
                        return str;
                        break;
                    }

                    case (quint8)0xFA:
                    {
                        str = "Команда: Запрос Тест СК";
                        return str;
                        break;
                    }

                    case (quint8)0x45:
                    {
                        str.append("Команда: Запрос привязки");
                        return str;
                        break;
                    }

                    case (quint8)0x46:
                    {
                        str.append("Команда: Очистить привязку  ВК%1");
                        str = str.arg((quint8)baMSG.at(9));
                        return str;
                        break;
                    }

                    case (quint8)0x47:
                    {
                        str.append("Команда: Добавить привязку к ВК%1 %2[%3]");
                        str = str.arg((quint8)baMSG.at(9))
                                 .arg(getNameSO_RUS(baMSG.mid(11, 2)))
                                 .arg(QString(baMSG.mid(11, 2).toHex().toUpper()));
                        return str;
                        break;
                    }

                    case (quint8)0x48:
                    {
                        str.append("Команда: Удалить привязку от ВК%1 %2[%3]");
                        str = str.arg((quint8)baMSG.at(9))
                                 .arg(getNameSO_RUS(baMSG.mid(11, 2)))
                                 .arg(QString(baMSG.mid(11, 2).toHex().toUpper()));
                        return str;
                        break;
                    }

                    case (quint8)0x49:
                    {
                        str = "Команда: не описанная";
                        if((quint8)0x55 == (quint8)baMSG.at(12))
                        {
                            str = "Команда: Запиать в ВК%1 порог освещённости %2";
                            str = str.arg((quint8)baMSG.at(9))
                                     .arg((quint8)baMSG.at(11));
                        }
                        if((quint8)0xEE == (quint8)baMSG.at(12))
                        {
                            str = "Команда: Чтение с ВК%1 уровня и порога освещённости";
                            str = str.arg((quint8)baMSG.at(9));
                        }

                        return str;
                        break;
                    }

                    case (quint8)0x78:
                    {
                        str = "Команда: Установить Уч%1 Ном%2";
                        str = str.arg((quint8)baMSG.at(9))
                                 .arg((quint8)baMSG.at(10));
//                        str = "Команда: Изменить порядковый номер и номер участка";
                        return str;
                        break;
                    }

                    default:
                    {
                        str.append("Не описанное сообщение:");
                        str.append(baMSG.toHex().toUpper());
                        return str;
                        break;
                    }
                }
                return str;
                break;
            };



            default:
            {
                str.append("Не описанное сообщение:");
                str.append(baMSG.toHex().toUpper());
                return str;
                break;
            }
        }
    }
    return str;
}

QColor ProxyMetod::msgColor_KM(bool ioType,
                               QByteArray baMSG) /*noexcept*/
{
    QColor result;
    result.setRgb(192, 192, 192);//Qt::lightGray;
    if(ioType)
    {
        result.setRgb(255, 255, 255);//Qt::white;
    }
    else
    {
        result.setRgb(192, 192, 192);//Qt::lightGray;
    }

    if((quint8)0xA5 !=(quint8)baMSG.at(0))
    {
        if(baMSG.contains("opening succeeded"))
        {
            result.setRgb(255, 255, 255);//Qt::white;
        }
        if(baMSG.contains("opening failed"))
        {
            result.setRgb(255, 255, 125);//Qt::yellow;
        }
        if(baMSG.contains("Deleted"))
        {
            result.setRgb(255, 125, 125);//Qt::red;
        }
        if(baMSG.contains("Port closed succeeded"))
        {
            result.setRgb(255, 125, 125);//Qt::red;
        }
        if(baMSG.contains("Lost connect"))
        {
            result.setRgb(255, 125, 125);//Qt::red;
        }
        if(baMSG.contains("Recovered connect"))
        {
            result.setRgb(125, 255, 125);//Qt::green;
        }
    }
    else
    {
        switch((quint8)baMSG.at(8))
        {
            case (quint8)0x0A:
            {
//                str.append("Квитанция");
                break;
            };

            case (quint8)0xA1:
            {
//                str.append("Опрос состояния устройства");
                break;
            };

            case (quint8)0xAA:
            {
//                str.append("Состояние устройства ");
                break;
            };

            case (quint8)0xB1:
            {
//                return "Сделать тревожный стоп-кадр";
                break;
            };

            case (quint8)0xC1:
            {
//                str.append("Готовность к передаче стоп-кадров: ");
                break;
            };

            case (quint8)0xB3:
            {
//                str = "Запрос блока стоп-кадров: Кадр%1[%2]";
                break;
            };

            case (quint8)0xB5:
            {
//                str = "Приём группы кадров закончен";
                break;
            };

            case (quint8)0xC5:
            {
//                str.append("Квитанция");
                break;
            };

            case (quint8)0xC3:
            {
//                str.append("Передача стоп-кадра: img%1 block%2");
                break;
            };

            case (quint8)0x1F:
            {
//                str = "Инсталляция";
                break;
            };

            case (quint8)0x2F:
            {
//                str = "Запрос инсталляции с камеры";
                break;
            };

            case (quint8)0x3F:
            {
//                str.append("Ответ на запрос инсталляции");
                break;
            };

            case (quint8)0xE1:
            {
//                str.append("Тревожное сообщение: ");
                result.setRgb(255, 125, 125);//Qt::red;
                break;
            };

            default:
            {
                break; //;" !!! не известное сообщение !!! ";
            }
        }
    }

    return result;
}

QString ProxyMetod::msgCommentRUS_KM(QByteArray baMSG) /*noexcept*/
{
    QString str;

    if((quint8)0xA5 !=(quint8)baMSG.at(0))
    {
        str = baMSG;
        if(baMSG.contains("Start session"))
        {
            str = "Начало сессии";
        }
        if(baMSG.contains("Complete session"))
        {
            str = "Конец сессии";
        }
        if(baMSG.contains("opening succeeded"))
        {
            str = "Открытие порта " + baMSG.split(' ').at(1) + " успешно";
        }
        if(baMSG.contains("opening failed"))
        {
            str = "Открытие порта " + baMSG.split(' ').at(1) + " не успешно";
        }
        if(baMSG.contains("Builded network"))
        {
            str = "Построена сеть";
        }
        if(baMSG.contains("Port opening succeeded"))
        {
            str = "Открытие порта успешно";
        }
        if(baMSG.contains("Port opening failed"))
        {
            str = "Открытие порта не успешно";
        }
        if(baMSG.contains("Reset alarm"))
        {
            str = "Сброс тревоги";
        }
        if(baMSG.contains("Reset failure"))
        {
            str = "Сброс неисправности";
        }
        if(baMSG.contains("Reset discharge"))
        {
            str = "Сброс разряда";
        }
        if(baMSG.contains("Deleted"))
        {
            str = "Удалено";
        }
        if(baMSG.contains("Port closed succeeded"))
        {
            str = "Порт закрыт";
        }
        if(baMSG.contains("Lost connect"))
        {
            str = "Превышенно ожидание нормального обмена сообщениями";
        }
        if(baMSG.contains("Recovered connect"))
        {
            str = "Нормальный обмен сообщениями";
        }
    }
    else
    {
        switch((quint8)baMSG.at(8))
        {
            case (quint8)0x0A:
            {
                str.append("Квитанция на АРМ о приеме команды");
                return str;
                break;
            };

            case (quint8)0xA1:
            {
                str.append("Опрос состояния устройства");
                return str;
                break;
            };

            case (quint8)0xAA:
            {
                str.append("Состояние: ");

//                switch((quint8)(baMSG.at(12) & 0x0F))
//                {
//                    case (quint8)0x02:
//                    {
//                        str.append(": норма ");
//                        break;
//                    };

//                    case (quint8)0x03:
//                    {
//                        str.append(": тревога ");
//                        break;
//                    };

//                    case (quint8)0x04:
//                    {
//                        str.append(": неисправность ");
//                        break;
//                    };

//                    case (quint8)0x0A:
//                    {
//                        str.append(": разряд Встроенного АКБ ");
//                        break;
//                    };

//                    case (quint8)0x0B:
//                    {
//                        str.append(": разряд Внешнего АКБ ");
//                        break;
//                    };

//                    case (quint8)0x0C:
//                    {
//                        str.append(": отключение пользователем ");
//                        break;
//                    };

//                    case (quint8)0x0D:
//                    {
//                        str.append(": самостоятельное отключение по разряду ");
//                        break;
//                    };
//                }

                QString condition1("[неизв.], "),
                        condition2("[неизв.]");
                switch((quint8)(baMSG.at(12) & 0xF0))
                {
                    case (quint8)0x10:
                    {
                        condition1 = "[Встроенное АКБ ";
                        break;
                    };

                    case (quint8)0x20:
                    {
                        condition1 = "[АИП-4 ";
                        break;
                    };

                    case (quint8)0x30:
                    {
                        condition1 = "[БАРС ";
                        break;
                    };

                    case (quint8)0x40:
                    {
                        condition1 = "[Кор Комм  ";
                        break;
                    };

                    case (quint8)0x50:
                    {
                        condition1 = "[Зар АКБ  ";
                        break;
                    };
                };

                switch((quint8)(baMSG.at(12) & 0x0F))
                {
                    case (quint8)0x02:
                    {
                        condition1.append("норма], ");
                        break;
                    };

                    case (quint8)0x03:
                    {
                        condition1.append("тревога], ");
                        break;
                    };

                    case (quint8)0x04:
                    {
                        condition1.append("неисправность], ");
                        break;
                    };

                    case (quint8)0x0A:
                    {
                        condition1.append("разряд  встроенного АКБ], ");
                        break;
                    };

                    case (quint8)0x0B:
                    {
                        condition1.append("разряд внешнего АКБ], ");
                        break;
                    };

                    case (quint8)0x0C:
                    {
                        condition1.append("Отключение пользователем], ");
                        break;
                    };

                    case (quint8)0x0D:
                    {
                        condition1.append("Самостоятельное отключение по разряду], ");
                        break;
                    };
                };

                if((quint8)0x01 == (quint8)baMSG.at(2))
                {
                    switch((quint8)baMSG.at(12))
                    {
                        case (quint8)0x01:
                        {
                            condition2 = "[ВК есть не переданные кадры]";
                            break;
                        };

                        case (quint8)0x02:
                        {
                            condition2 = "[ВК норма]";
                            break;
                        };

                        case (quint8)0x04:
                        {
                            condition2 = "[неисправность]";
                            break;
                        };
                    };
                }

                if((quint8)0x02 == (quint8)baMSG.at(2))
                {
                    switch((quint8)baMSG.at(12))
                    {
                        case (quint8)0x01:
                        {
                            condition2 = "[РТ есть не переданные кадры]";
                            break;
                        };

                        case (quint8)0x00:
                        {
                            condition2 = "[всё хуйня]";
                            break;
                        };
                    };
                }

                str.append(condition1);
                str.append(condition2);
                return str;
                break;
            };

            case (quint8)0xB1:
            {
                return "Команда: Запрос СК ВК1";
                break;
            };

            case (quint8)0xC1:
            {
                str.append("Сделаны СК: ");
                for(int i(0); i < 5; i++)
                {
                    quint32 sizeIMG(0);
                    sizeIMG = ((((quint32)baMSG.at((3 * i) + 12) * 0x00010000) & 0x00FF0000) +
                              (((quint32)baMSG.at((3 * i) + 13) * 0x00000100) & 0x0000FF00) +
                              ((quint32)baMSG.at((3 * i) + 14) & 0x000000FF));

                    if(sizeIMG)
                    {
                        str.append("СК%1[%2] ");
                        str = str.arg(1 + i)
                                 .arg(sizeIMG);
                    }
                }
                return str;
                break;
            };

            case (quint8)0xB3:
            {
                str = "Команда: Запрос СК%1[%2]";
                str = str.arg((quint8)baMSG.at(12))
                         .arg((((quint8)baMSG.at(17) * 0x00010000) & 0x00FF0000) +
                              (((quint8)baMSG.at(18) * 0x00000100) & 0x0000FF00) +
                              ((quint8)baMSG.at(19) & 0x000000FF));
                return str;
                break;
            };

            case (quint8)0xB5:
            {
                str = "ВК1 группа СК получена";
//                str = "Приём группы кадров закончен";
                return str;
                break;
            };

            case (quint8)0xC5:
            {
                str.append("Квитанция");
                return str;
                break;
            };

            case (quint8)0xC3:
            {
                str.append("Передача стоп-кадра: img%1 block%2");
                str = str.arg((quint8)baMSG.at(12))
                         .arg((quint8)baMSG.at(13));
                return str;
                break;
            };

            case (quint8)0x1F:
            {
                str = "Инсталляция";
                return str;
                break;
            };

            case (quint8)0x2F:
            {
                str = "Запрос инсталляции с камеры";
                return str;
                break;
            };

            case (quint8)0x3F:
            {
                str.append("Ответ на запрос инсталляции");
                return str;
                break;
            };

            case (quint8)0xE1:
            {
                str.append("Тревожное сообщение: ");
                switch((quint8)baMSG.at(12))
                {
                    case (quint8)0x01:
                    {
                        str.append(" СО[встроенное]");
                        break;
                    };

                    case (quint8)0x02:
                    {
                        str.append(" СО[%1 %2]");
//                        str.append(" СО[БСК %1]");
                        str = str.arg(getNameSO_RUS(baMSG.mid(13, 2)))
                                 .arg(QString(baMSG.mid(13, 2).toHex().toUpper()));
//                        str = str.arg((((quint8)baMSG.at(13) * 0x0100) & 0xFF00)  +
//                                      ((quint8)baMSG.at(14) & 0xFF00));
                        break;
                    };

                    case (quint8)0x03:
                    {
                        str.append(" СО[внешнее 1]");
                        break;
                    };

                    case (quint8)0x04:
                    {
                        str.append(" СО[внешнее 2]");
                        break;
                    };
                }

                switch((quint8)baMSG.at(15))
                {
                    case (quint8)0x02:
                    {
                        str.append(" Встроенное СО[норма]");
                        break;
                    };

                    case (quint8)0x03:
                    {
                        str.append(" Встроенное СО[тревога]");
                        break;
                    };

                    case (quint8)0x05:
                    {
                        str.append(" Встроенное СО[отключено]");
                        break;
                    };
                }

                switch((quint8)baMSG.at(16))
                {
                    case (quint8)0x02:
                    {
                        str.append(" СО[норма]");
                        break;
                    };

                    case (quint8)0x03:
                    {
                        str.append(" СО[тревога]");
                        break;
                    };

                    case (quint8)0x04:
                    {
                        str.append(" СО[неиспр]");
                        break;
                    };

                    case (quint8)0x05:
                    {
                        str.append(" СО[отключено]");
                        break;
                    };
                }
                return str;
                break;
            };

            default:
            {
                return " !!! не известное сообщение !!! ";
            }
        }
    }
    return str;
}

QString ProxyMetod::msgCommentENG(QByteArray baMSG,
                                       TypeUnitNodeSystem typeSystem) /*noexcept*/
{
    switch(typeSystem)
    {
        case KMSystemType:
        {
            return this->msgCommentENG_KM(baMSG);
        };

        case KSSystemType:
        {
            return this->msgCommentENG_KS(baMSG);
        };

        case BSKSystemType:
        {
            return this->msgCommentENG_BSK(baMSG);
        };

        case UnknownSystemType:
        {
            return this->msgCommentENG_SYS(baMSG);
        };

        default:
        {
            return baMSG.toHex();
        }
    }
    return QString();
}

QString ProxyMetod::msgCommentENG_SYS(QByteArray baMSG) /*noexcept*/
{
    QString str;

    if((quint8)0xA5 !=(quint8)baMSG.at(0))
    {
        str = baMSG;
    }
    else
    {
        str = baMSG.toHex();
    }
    return str;
}

QString ProxyMetod::msgCommentENG_BSK(QByteArray baMSG) /*noexcept*/
{
    QString str;

    if((quint8)0xA5 !=(quint8)baMSG.at(0))
    {
        str = baMSG;
    }
    else
    {
        switch((quint8)baMSG.at(4))
        {
            case (quint8)0x9F:
            {
                switch((quint8)baMSG.at(5))
                {
                    case (quint8)0x01:
                    {
                        str.append("Alarm");
                        return str;
                        break;
                    };

                    case (quint8)0x40:
                    {
                        str.append("Malfunction");
                        return str;
                        break;
                    };

                    case (quint8)0xBB:
                    {
                        str.append("Food control");
                        return str;
                        break;
                    };

                    case (quint8)0xC0:
                    {
                        str.append("No connect");
                        return str;
                        break;
                    };

                    case (quint8)0x20:
                    {
                        str.append("Connect");
                        return str;
                        break;
                    };

                    case (quint8)0x44:
                    {
                        str.append("No network node");
                        return str;
                        break;
                    };

                    default:
                    {
                        str.append(baMSG.toHex().toUpper());
                        return str;
                        break;
                    }
                }
            };

            case (quint8)0xCE:
            {

                if((quint8)0x40 == (quint8)baMSG.at(1) &&
                   (quint8)0xFF == (quint8)baMSG.at(2))
                {
                    str = "ID plums of blocks of a network";
                }
                else
                {
                    str = "To transfer network ID blocks";
                }

                return str;
                break;
            };

            case (quint8)0x0B:
            {
                str = "To contact";
                return str;
                break;
            };

            case (quint8)0x0E:
            {
                str = "To pass into the terminal mode";
                return str;
                break;
            };

            case (quint8)0x09:
            {
                str = "To carry out self-testing";
                return str;
                break;
            };

            case (quint8)0x41:
            {
                switch ((quint8)baMSG.at(7))
                {
                    case (quint8)0x78:
                    {
                        str = "Change number and sector";
                        return str;
                        break;
                    }

                    case (quint8)0x55:
                    {
                        str = "DK D";
                        return str;
                        break;
                    }

                    case (quint8)0x01:
                    {
                        str = "Off D";
                        return str;
                        break;
                    }

                    case (quint8)0x02:
                    {
                        str = "On D";
                        return str;
                        break;
                    }

                    default:
                    {
                        return baMSG.toHex();
                        break;
                    }
                }
                break;
            };
        }
    }
    return str;
}

QString ProxyMetod::msgCommentENG_KS(QByteArray baMSG) /*noexcept*/
{
    return baMSG;
}

QString ProxyMetod::msgCommentENG_KM(QByteArray baMSG) /*noexcept*/
{
    QString str;

    if((quint8)0xA5 !=(quint8)baMSG.at(0))
    {
        str = baMSG;
    }
    else
    {
//        qDebug() << str << baMSG.toHex();

        switch((quint8)baMSG.at(8))
        {
            case (quint8)0x0A:
            {
                str = "Confirmation";
                return str;
                break;
            };

            case (quint8)0xA1:
            {
                str = "Device state scanning";
                return str;
                break;
            };

            case (quint8)0xAA:
            {
                str = "Device state: ";
                QString condition1("[Undefined], "),
                        condition2("[Undefined]");
                switch((quint8)(baMSG.at(12) & 0xF0))
                {
                    case (quint8)0x10:
                    {
                        condition1 = "[Built-in ACC ";
                        break;
                    };

                    case (quint8)0x20:
                    {
                        condition1 = "[IPS-4 ";
                        break;
                    };

                    case (quint8)0x30:
                    {
                        condition1 = "[BARS ";
                        break;
                    };

                    case (quint8)0x40:
                    {
                        condition1 = "[Junction box  ";
                        break;
                    };

                    case (quint8)0x50:
                    {
                        condition1 = "[ADD charge  ";
                        break;
                    };
                };

                switch((quint8)(baMSG.at(12) & 0x0F))
                {
                    case (quint8)0x02:
                    {
                        condition1.append("Norm], ");
                        break;
                    };

                    case (quint8)0x03:
                    {
                        condition1.append("Alarm], ");
                        break;
                    };

                    case (quint8)0x04:
                    {
                        condition1.append("Failure], ");
                        break;
                    };

                    case (quint8)0x0A:
                    {
                        condition1.append("Built-in ACC discharge], ");
                        break;
                    };

                    case (quint8)0x0B:
                    {
                        condition1.append("External ACC discharge], ");
                        break;
                    };

                    case (quint8)0x0C:
                    {
                        condition1.append("User switch-off], ");
                        break;
                    };

                    case (quint8)0x0D:
                    {
                        condition1.append("Discharge switch-off], ");
                        break;
                    };
                };

                if((quint8)0x01 == (quint8)baMSG.at(2))
                {
                    switch((quint8)baMSG.at(12))
                    {
                        case (quint8)0x01:
                        {
                            condition2 = "[Contains non-transmitted frames]";
                            break;
                        };

                        case (quint8)0x02:
                        {
                            condition2 = "[Norm]";
                            break;
                        };

                        case (quint8)0x04:
                        {
                            condition2 = "[Failure]";
                            break;
                        };
                    };
                }

                if((quint8)0x02 == (quint8)baMSG.at(2))
                {
                    switch((quint8)baMSG.at(12))
                    {
                        case (quint8)0x01:
                        {
                            condition2 = "[Contains non-transmitted frames]";
                            break;
                        };

                        case (quint8)0x00:
                        {
                            condition2 = "[Undefined]";
                            break;
                        };
                    };
                }

                str.append(condition1);
                str.append(condition2);
                return str;
                break;
            };

            case (quint8)0xB1:
            {
                str =  "Take alarm freeze-frame";
                return str;
                break;
            };

            case (quint8)0xC1:
            {
                str = "Ready to transmit freeze-frames: ";
                for(int i(0); i < 5; i++)
                {
                    quint32 sizeIMG(0);
                    sizeIMG = ((((quint32)baMSG.at((3 * i) + 12) * 0x00010000) & 0x00FF0000) +
                              (((quint32)baMSG.at((3 * i) + 13) * 0x00000100) & 0x0000FF00) +
                              ((quint32)baMSG.at((3 * i) + 14) & 0x000000FF));

                    if(sizeIMG)
                    {
                        str.append("FF%1[%2] ");
                        str = str.arg(1 + i)
                                 .arg(sizeIMG);
                    }
                }
                return str;
                break;
            };

            case (quint8)0xB3:
            {
                str = "Freeze-frame block inquiry: FF%1[%2]";
                str = str.arg((quint8)baMSG.at(12))
                         .arg((((quint8)baMSG.at(17) * 0x00010000) & 0x00FF0000) +
                              (((quint8)baMSG.at(18) * 0x00000100) & 0x0000FF00) +
                              ((quint8)baMSG.at(19) & 0x000000FF));
                return str;
                break;
            };

            case (quint8)0xB5:
            {
                str = "Receiving group of frames completed";
                return str;
                break;
            };

            case (quint8)0xC5:
            {
                str.append("Confirmation");
                return str;
                break;
            };

            case (quint8)0xC3:
            {
                str.append("Передача стоп-кадра: img%1 block%2");
                str = str.arg((quint8)baMSG.at(12))
                         .arg((quint8)baMSG.at(13));
                return str;
                break;
            };

            case (quint8)0x1F:
            {
                str = "Installation";
                return str;
                break;
            };

            case (quint8)0x2F:
            {
                str = "Installation inquiry";
                return str;
                break;
            };

            case (quint8)0x3F:
            {
                str = "Reply to installation inquiry";
                return str;
                break;
            };

            case (quint8)0xE1:
            {
                str = "Alarm message: ";
                switch((quint8)baMSG.at(12))
                {
                    case (quint8)0x01:
                    {
                        str.append(" D[Built-in]");
                        break;
                    };

                    case (quint8)0x02:
                    {
                        str.append(" D[BSK %1]");
                        str = str.arg((((quint8)baMSG.at(13) * 0x0100) & 0xFF00)  +
                                      ((quint8)baMSG.at(14) & 0xFF00));
                        break;
                    };

                    case (quint8)0x03:
                    {
                        str.append(" D[External 1]");
                        break;
                    };

                    case (quint8)0x04:
                    {
                        str.append(" D[External 2]");
                        break;
                    };
                }

                switch((quint8)baMSG.at(15))
                {
                    case (quint8)0x02:
                    {
                        str.append(" Built-in D[Norm]");
                        break;
                    };

                    case (quint8)0x03:
                    {
                        str.append(" Built-in D[Alarm]");
                        break;
                    };

                    case (quint8)0x05:
                    {
                        str.append(" Built-in D[Disconnected]");
                        break;
                    };
                }

                switch((quint8)baMSG.at(16))
                {
                    case (quint8)0x02:
                    {
                        str.append(" D[Norm]");
                        break;
                    };

                    case (quint8)0x03:
                    {
                        str.append(" D[Alarm]");
                        break;
                    };

                    case (quint8)0x04:
                    {
                        str.append(" D[Failure]");
                        break;
                    };

                    case (quint8)0x05:
                    {
                        str.append(" D[Disconnected]");
                        break;
                    };
                }
                return str;
                break;
            };

            default:
            {
                return " !!! не известное сообщение !!! ";
                break;
            }
        }
    }
    return str;
}

QString ProxyMetod::getNameDevice(QByteArray id, TypeUnitNodeSystem typeSystem) /*noexcept*/
{
    if(id.isEmpty())
    {
        return tr("System");
    }
    if(id.size() < 2)
    {
        return "";
    }
    TypeUnitNodeDevice typeUN;
    if(BSKSystemType == typeSystem)
    {
        typeUN = (TypeUnitNodeDevice)((quint8)id.at(0) / (quint8)0x10);
        if(CPP_BSKDeviceType ==typeUN)
        {
            return tr("BSK-AWS");
        }
        if(RT_BSKDeviceType ==typeUN)
        {
            return tr("BSK-RT");
        }
        if(S_BSKDeviceType ==typeUN)
        {
            return tr("BSK-S");
        }
        if(RLO_BSKDeviceType ==typeUN)
        {
            return tr("BSK-RLO");
        }
        if(RLD_BSKDeviceType ==typeUN)
        {
            return tr("BSK-RLD");
        }
        if(RWD_BSKDeviceType ==typeUN)
        {
            return tr("BSK-RWD");
        }
        if(MSO_BSKDeviceType ==typeUN)
        {
            return tr("BSK-MSO");
        }
        if(BVU_BSKDeviceType ==typeUN)
        {
            return tr("BSK-BVU");
        }
        if(PKP_BSKDeviceType ==typeUN)
        {
            return tr("BSK-PKP");
        }
        if(VK_BSKDeviceType ==typeUN)
        {
            return tr("BSK-VK");
        }
        if(O_BSKDeviceType ==typeUN)
        {
            return tr("BSK-O");
        }
        if(IK_BSKDeviceType ==typeUN)
        {
            return tr("BSK-IK");
        }
        if(RW_BSKDeviceType ==typeUN)
        {
            return tr("BSK-RW");
        }
        if(RVP_BSKDeviceType ==typeUN)
        {
            return tr("BSK-RVP");
        }
        if(PKSC_BSKDeviceType ==typeUN)
        {
            return tr("BSK-PKSC");
        }
        if(VSO_BSKDeviceType ==typeUN)
        {
            return tr("BSK-VSO");
        }
    }
    if(KSSystemType == typeSystem)
    {
        typeUN = (TypeUnitNodeDevice)((quint8)id.at(0) / (quint8)0x10);
        if(CPP_KSDeviceType == typeUN)
        {
            return tr("AWS");
        }
        if(RT01_KSDeviceType == typeUN ||
           RT02_KSDeviceType == typeUN)
        {
            return tr("RT");
        }
        if(VK09_KSDeviceType == typeUN ||
           VK0A_KSDeviceType == typeUN ||
           VK0B_KSDeviceType == typeUN)
        {
            return tr("VK");
        }
    }
    if(KMSystemType == typeSystem)
    {
        typeUN = (TypeUnitNodeDevice)(quint8)id.at(0);
        if(RT_KMDeviceType == typeUN)
        {
            return tr("RT");
        }
        if(VK_KMDeviceType == typeUN)
        {
            return tr("VK");
        }
    }
    return tr("System");
}

QString ProxyMetod::getNameDevice_RUS(QByteArray id, TypeUnitNodeSystem typeSystem) /*noexcept*/
{
    if(id.isEmpty())
    {
        return "Система";
    }
    if(id.size() < 2)
    {
        return "";
    }
    TypeUnitNodeDevice typeUN((TypeUnitNodeDevice)((quint8)id.at(0) / (quint8)0x10));
    if(BSKSystemType == typeSystem)
    {
        if(CPP_BSKDeviceType ==typeUN)
        {
            return "БСК-АРМ";
        }
        if(RT_BSKDeviceType ==typeUN)
        {
            return "БСК-РТ";
        }
        if(S_BSKDeviceType ==typeUN)
        {
            return "БСК-С";
        }
        if(RLO_BSKDeviceType ==typeUN)
        {
            return "БСК-РЛО";
        }
        if(RLD_BSKDeviceType ==typeUN)
        {
            return "БСК-РЛД";
        }
        if(RWD_BSKDeviceType ==typeUN)
        {
            return "БСК-РВД";
        }
        if(MSO_BSKDeviceType ==typeUN)
        {
            return "БСК-МСО";
        }
        if(BVU_BSKDeviceType ==typeUN)
        {
            return "БСК-БВУ";
        }
        if(PKP_BSKDeviceType ==typeUN)
        {
            return "БСК-ПКП";
        }
        if(VK_BSKDeviceType ==typeUN)
        {
            return "БСК-ВК";
        }
        if(O_BSKDeviceType ==typeUN)
        {
            return "БСК-О";
        }
        if(IK_BSKDeviceType ==typeUN)
        {
            return "БСК-ИК";
        }
        if(RW_BSKDeviceType ==typeUN)
        {
            return "БСК-РВ";
        }
        if(RVP_BSKDeviceType ==typeUN)
        {
            return "БСК-РВП";
        }
        if(PKSC_BSKDeviceType ==typeUN)
        {
            return "БСК-ПКСЧ";
        }
        if(VSO_BSKDeviceType ==typeUN)
        {
            return "БСК-ВСО";
        }
    }
    if(KSSystemType == typeSystem)
    {
        if(CPP_KSDeviceType == typeUN)
        {
            return "АРМ";
        }
        if(RT01_KSDeviceType == typeUN ||
           RT02_KSDeviceType == typeUN)
        {
            return "РТ";
        }
        if(VK09_KSDeviceType == typeUN ||
           VK0A_KSDeviceType == typeUN ||
           VK0B_KSDeviceType == typeUN)
        {
            return "ВК";
        }
    }
    if(KMSystemType == typeSystem)
    {
        if(RT_KMDeviceType == typeUN)
        {
            return "РТ";
        }
        if(VK_KMDeviceType == typeUN)
        {
            return "ВК";
        }
    }
    return "Система";
}

QString ProxyMetod::getNameDevice_ENG(QByteArray id, TypeUnitNodeSystem typeSystem) /*noexcept*/
{
    if(id.isEmpty())
    {
        return "System";
    }
    if(id.size() < 2)
    {
        return "";
    }
    TypeUnitNodeDevice typeUN((TypeUnitNodeDevice)((quint8)id.at(0) / (quint8)0x10));
    if(BSKSystemType == typeSystem)
    {
        if(CPP_BSKDeviceType ==typeUN)
        {
            return "BSK-AWS";
        }
        if(RT_BSKDeviceType ==typeUN)
        {
            return "BSK-RT";
        }
        if(S_BSKDeviceType ==typeUN)
        {
            return "BSK-S";
        }
        if(RLO_BSKDeviceType ==typeUN)
        {
            return "BSK-RLO";
        }
        if(RLD_BSKDeviceType ==typeUN)
        {
            return "BSK-RLD";
        }
        if(RWD_BSKDeviceType ==typeUN)
        {
            return "BSK-RWD";
        }
        if(MSO_BSKDeviceType ==typeUN)
        {
            return "BSK-MSO";
        }
        if(BVU_BSKDeviceType ==typeUN)
        {
            return "BSK-BVU";
        }
        if(PKP_BSKDeviceType ==typeUN)
        {
            return "BSK-PKP";
        }
        if(VK_BSKDeviceType ==typeUN)
        {
            return "BSK-VK";
        }
        if(O_BSKDeviceType ==typeUN)
        {
            return "BSK-O";
        }
        if(IK_BSKDeviceType ==typeUN)
        {
            return "BSK-IK";
        }
        if(RW_BSKDeviceType ==typeUN)
        {
            return "BSK-RW";
        }
        if(RVP_BSKDeviceType ==typeUN)
        {
            return "BSK-RVP";
        }
        if(PKSC_BSKDeviceType ==typeUN)
        {
            return "BSK-PKSC";
        }
        if(VSO_BSKDeviceType ==typeUN)
        {
            return "BSK-VSO";
        }
    }
    if(KSSystemType == typeSystem)
    {
        if(CPP_KSDeviceType == typeUN)
        {
            return "AWS";
        }
        if(RT01_KSDeviceType == typeUN ||
           RT02_KSDeviceType == typeUN)
        {
            return "RT";
        }
        if(VK09_KSDeviceType == typeUN ||
           VK0A_KSDeviceType == typeUN ||
           VK0B_KSDeviceType == typeUN)
        {
            return "VK";
        }
    }
    if(KMSystemType == typeSystem)
    {
        if(RT_KMDeviceType == typeUN)
        {
            return "RT";
        }
        if(VK_KMDeviceType == typeUN)
        {
            return "VK";
        }
    }
    return "System";
}


QString ProxyMetod::getNameSO(QByteArray idSO) /*noexcept*/
{
    if(idSO.size() < 2)
    {
        return "";
    }

    TypeUnitNodeDevice typeBSK((TypeUnitNodeDevice)(((quint8)idSO.at(0) / (quint8)0x10)  & (quint8)0x0F));

    switch(typeBSK)
    {
        case CPP_BSKDeviceType:
        {
            if((quint8)0x00 == (quint8)idSO.at(1))
            {
                return tr("ARM Command");
            }
            if((quint8)0x01 == (quint8)idSO.at(1))
            {
//                return tr("BARS");
                return tr("Other D1");
            }
            if((quint8)0x02 == (quint8)idSO.at(1))
            {
//                return tr("BARS");
                return tr("Other D2");
            }
            if((quint8)0x0B == (quint8)idSO.at(1))
            {
                return tr("Self D");
            }
            break;
        }
        case RT_BSKDeviceType:
        {
           return tr("BSK-RT"); break;
        }
        case S_BSKDeviceType:
        {
           return tr("BSK-S"); break;
        }
        case RLO_BSKDeviceType:
        {
           return tr("BSK-RLO"); break;
        }
        case RLD_BSKDeviceType:
        {
           return tr("BSK-RLD"); break;
        }
        case RWD_BSKDeviceType:
        {
           return tr("BSK-RWD"); break;
        }
        case MSO_BSKDeviceType:
        {
           return tr("BSK-MSO"); break;
        }
        case BVU_BSKDeviceType:
        {
           return tr("BSK-BVU"); break;
        }
        case PKP_BSKDeviceType:
        {
           return tr("BSK-PKP"); break;
        }
        case VK_BSKDeviceType:
        {
           return tr("BSK-VK"); break;
        }
        case O_BSKDeviceType:
        {
           return tr("BSK-O"); break;
        }
        case IK_BSKDeviceType:
        {
           return tr("BSK-IK"); break;
        }
        case RW_BSKDeviceType:
        {
           return tr("BSK-RW"); break;
        }
        case RVP_BSKDeviceType:
        {
           return tr("BSK-RVP"); break;
        }
        case PKSC_BSKDeviceType:
        {
           return tr("BSK-PKSC"); break;
        }
        case VSO_BSKDeviceType:
        {
           return tr("BSK-VSO"); break;
        }
        default:
        {
            return tr("UN"); break;
        }
    }
    return "";
}

QString ProxyMetod::getNameSO_RUS(QByteArray idSO) /*noexcept*/
{
    if(idSO.size() < 2)
    {
        return "";
    }

    QString str;
    TypeUnitNodeDevice typeBSK((TypeUnitNodeDevice)(((quint8)idSO.at(0) / (quint8)0x10)  & (quint8)0x0F));

    switch(typeBSK)
    {
        case CPP_BSKDeviceType:
        {
            if((quint8)0x01 == (quint8)idSO.at(1))
            {
                return "Внешнее СО1";
            }
            if((quint8)0x02 == (quint8)idSO.at(1))
            {
                return "Внешнее СО2";
            }
            if((quint8)0x0B == (quint8)idSO.at(1))
            {
                return "Встроенное СО";
            }
            break;
        }
        //
        case S_BSKDeviceType:
        {
            return "БСК-С";
            break;
        }
        case RLO_BSKDeviceType:
        {
            return "БСК-РЛО";
            break;
        }
        case RLD_BSKDeviceType:
        {
            return "БСК-РЛД";
            break;
        }
        case RWD_BSKDeviceType:
        {
            return "БСК-РВД";
            break;
        }
        case MSO_BSKDeviceType:
        {
            return "БСК-МСО";
            break;
        }
        case BVU_BSKDeviceType:
        {
            return "БСК-БВУ";
            break;
        }
        case O_BSKDeviceType:
        {
            return "БСК-О";
            break;
        }
        case IK_BSKDeviceType:
        {
            return "БСК-ИК";
            break;
        }
        case RW_BSKDeviceType:
        {
            return "БСК-РВ";
            break;
        }
        case RVP_BSKDeviceType:
        {
            return "БСК-РВП";
            break;
        }
        case PKSC_BSKDeviceType:
        {
            return "БСК-ПКСЧ";
            break;
        }
        case VSO_BSKDeviceType:
        {
            return "БСК-ВСО";
            break;
        }
        default:
        {
            return "UN";
            break;
        }
    }
    return "";
}

QString ProxyMetod::getNameSO_ENG(QByteArray idSO) /*noexcept*/
{
    if(idSO.size() < 2)
    {
        return "";
    }

    TypeUnitNodeDevice typeBSK((TypeUnitNodeDevice)(((quint8)idSO.at(0) / (quint8)0x10)  & (quint8)0x0F));

    switch(typeBSK)
    {
        case CPP_BSKDeviceType:
        {
            if((quint8)0x01 == (quint8)idSO.at(1))
            {
                return "BARS";
            }
            if((quint8)0x02 == (quint8)idSO.at(1))
            {
                return "BARS";
            }
            if((quint8)0x0B == (quint8)idSO.at(1))
            {
                return "Self SO";
            }
            break;
        }
        //
        case S_BSKDeviceType:
        {
            return "BSK-S";
            break;
        }
        case RLO_BSKDeviceType:
        {
            return "BSK-RLO";
            break;
        }
        case RLD_BSKDeviceType:
        {
            return "BSK-RLD";
            break;
        }
        case RWD_BSKDeviceType:
        {
            return "BSK-RWD";
            break;
        }
        case MSO_BSKDeviceType:
        {
            return "BSK-MSO";
            break;
        }
        case BVU_BSKDeviceType:
        {
            return "BSK-BVU";
            break;
        }
        case VK_BSKDeviceType:
        {
            return "BSK-VK";
            break;
        }
        case O_BSKDeviceType:
        {
            return "BSK-O";
            break;
        }
        case IK_BSKDeviceType:
        {
            return "BSK-IK";
            break;
        }
        case RW_BSKDeviceType:
        {
            return "BSK-RW";
            break;
        }
        case RVP_BSKDeviceType:
        {
            return "BSK-RVP";
            break;
        }
        case PKSC_BSKDeviceType:
        {
            return "BSK-PKSC";
            break;
        }
        case VSO_BSKDeviceType:
        {
            return "BSK-VSO";
            break;
        }
        default:
        {
            return "UN";
            break;
        }
    }
    return "";
}

QString ProxyMetod::getTypeIPA(quint8 ipa) /*noexcept*/
{
    QString typeIPA(tr("Unknown"));
    if('b' == ipa ||
       'B' == ipa)
    {
        typeIPA = tr("BAT");
    }
    if((quint8)'a' == ipa ||
       (quint8)'A' == ipa)
    {
        typeIPA = tr("AIP");
    }
    if((quint8)'m' == ipa ||
       (quint8)'M' == ipa)
    {
        typeIPA = tr("MA");
    }
    if((quint8)'u' == ipa ||
       (quint8)'U' == ipa)
    {
        typeIPA = tr("USB");
    }
    if((quint8)'e' == ipa ||
       (quint8)'E' == ipa)
    {
        typeIPA = tr("BARS");
    }
    if((quint8)'k' == ipa ||
       (quint8)'K' == ipa)
    {
        typeIPA = tr("KK");
    }

    return typeIPA;
}

QString ProxyMetod::getTypeIPA_RUS(quint8 ipa) /*noexcept*/
{
    QString typeIPA("Без Типа");
    if('b' == ipa ||
       'B' == ipa)
    {
        typeIPA = "БАТ";
    }
    if((quint8)'a' == ipa ||
       (quint8)'A' == ipa)
    {
        typeIPA = "АИП";
    }
    if((quint8)'m' == ipa ||
       (quint8)'M' == ipa)
    {
        typeIPA = "МА";
    }
    if((quint8)'u' == ipa ||
       (quint8)'U' == ipa)
    {
        typeIPA = "USB";
    }
    if((quint8)'e' == ipa ||
       (quint8)'E' == ipa)
    {
        typeIPA = "БАРС";
    }
    if((quint8)'k' == ipa ||
       (quint8)'K' == ipa)
    {
        typeIPA = "КК";
    }

    return typeIPA;
}

QString ProxyMetod::getTypeIPA_ENG(quint8 ipa) /*noexcept*/
{
    QString typeIPA("Unknown");
    if('b' == ipa ||
       'B' == ipa)
    {
        typeIPA = "BAT";
    }
    if((quint8)'a' == ipa ||
       (quint8)'A' == ipa)
    {
        typeIPA = "AIP";
    }
    if((quint8)'m' == ipa ||
       (quint8)'M' == ipa)
    {
        typeIPA = "MA";
    }
    if((quint8)'u' == ipa ||
       (quint8)'U' == ipa)
    {
        typeIPA = "USB";
    }
    if((quint8)'e' == ipa ||
       (quint8)'E' == ipa)
    {
        typeIPA = "BARS";
    }
    if((quint8)'k' == ipa ||
       (quint8)'K' == ipa)
    {
        typeIPA = "KK";
    }

    return typeIPA;
}
