QT += core serialport
CONFIG += c++11

INCLUDEPATH += $$PWD/

HEADERS += \
    $$PWD/cosvariantlist.h \ 
    $$PWD/proxymetod.h \
    $$PWD/soundalarm.h

SOURCES += \
    $$PWD/cosvariantlist.cpp \ 
    $$PWD/proxymetod.cpp \
    $$PWD/soundalarm.cpp
