#include "cosvariantlist.h"

COSVariantList::COSVariantList(QObject *parent) :
    QObject(parent),
    flag_emitArgSignal(false),
    flag_emitAddSignal(true),
    flag_emitRemoveSignal(false),
    flag_resurce(false)
{
    try
    {
//        this->m_resurceMutex = new QMutex;
    }
    catch (std::runtime_error &rte)
    {
        qWarning("catch runtime_error %s [COSVariantList::COSVariantList]", rte.what());
    }
    catch (std::bad_alloc& ba)
    {
        qWarning("catch bad_alloc %s [COSVariantList::COSVariantList]", ba.what());
    }
    catch (std::exception &e)
    {
        qWarning("catch exception %s [COSVariantList::COSVariantList]", e.what());
    }
    catch (...)
    {
        qWarning("catch exception <unknown> [COSVariantList::COSVariantList]");
    }

//    this->m_list = new QList<QVariant>;
}

//COSVariantList::COSVariantList(const QList<QVariant> &other, QObject *parent) :
//    QObject(parent),
//    flag_emitArgSignal(false),
//    flag_emitAddSignal(true),
//    flag_emitRemoveSignal(false)
//{
//    this->m_list = new QList<QVariant>(other);
//}

//COSVariantList::COSVariantList(const COSVariantList &other, QObject *parent) :
//    QObject(parent),
//    flag_emitArgSignal(false),
//    flag_emitAddSignal(true),
//    flag_emitRemoveSignal(false)
//{
//    this->m_list = new QList<QVariant>(other.mid(0));
//}


//COSVariantList::COSVariantList(QList<QVariant> &&other, QObject *parent) :
//    QObject(parent),
//    flag_emitArgSignal(false),
//    flag_emitAddSignal(true),
//    flag_emitRemoveSignal(false)
//{
//    this->lockResurce();
//    this->m_list = new QList<QVariant>(other);
//    if(needUnlock) {this->unlockResurce();}
//}

//COSVariantList::COSVariantList(COSVariantList &&other, QObject *parent) :
//    QObject(parent),
//    flag_emitArgSignal(false),
//    flag_emitAddSignal(true),
//    flag_emitRemoveSignal(false)
//{
//    this->lockResurce();
//    this->m_list = new QList<QVariant>(other.mid(0));
//    if(needUnlock) {this->unlockResurce();}
//}

//COSVariantList::COSVariantList(std::initializer_list<QVariant> args, QObject *parent) :
//    QObject(parent),
//    flag_emitArgSignal(false),
//    flag_emitAddSignal(true),
//    flag_emitRemoveSignal(false)
//{
//    this->lockResurce();
//    this->m_list = new QList<QVariant>(args);
//    if(needUnlock) {this->unlockResurce();}
//}


COSVariantList::~COSVariantList()
{
    try
    {
        bool needUnlock(false);
        needUnlock = this->lockResource();
        this->m_list.clear();
//        delete this->m_list;
//        this->m_list = nullptr;
        if(needUnlock) {this->unlockResource();}

//        delete this->m_resurceMutex;
//        this->m_resurceMutex = nullptr;
    }
    catch(...)
    {
//#ifdef QT_DEBUG
//        qDebug() << "Assert(~COSVariantList)";
//#endif
        return;
    }
}

bool COSVariantList::appendUnique(const QVariant &value)
{
    bool needUnlock(false);
    needUnlock = this->lockResource();
    if(!this->m_list.contains(value))
    {
    //    int add_i(this->size());
    //    QVariant add_value(value);

        this->m_list.append(value);

    //    this->emitAddSignal(add_i, add_value);
        this->emitAddSignal();
        if(needUnlock) {this->unlockResource();}
        return true;
    }
    if(needUnlock) {this->unlockResource();}
    return false;
}

void COSVariantList::append(const QVariant &value)
{
    bool needUnlock(false);
    needUnlock = this->lockResource();
//    int add_i(this->size());
    QVariant add_value(value);

//    this->m_list.append(value);

    this->m_list.append(add_value);

//    this->emitAddSignal(add_i, add_value);
    this->emitAddSignal();
    if(needUnlock) {this->unlockResource();}
}

void COSVariantList::append(const QList<QVariant> &value)
{
    bool needUnlock(false);
    needUnlock = this->lockResource();
//    int add_i(this->size());
//    QVariant add_value(value);

    this->m_list.append(value);

//    this->emitAddSignal(add_i, add_value);
    this->emitAddSignal();
    if(needUnlock) {this->unlockResource();}
}

void COSVariantList::append(const COSVariantList &value)
{
    bool needUnlock(false);
    needUnlock = this->lockResource();
//    int add_i(this->size());
//    QVariant add_value(value.mid(0));

//    for(int i(0), n(value.size()); i < n; i++)
//    {
//        this->m_list.append(value.at(i));
//    }
    this->m_list.append(value.mid(0));

//    this->emitAddSignal(add_i, add_value);
    this->emitAddSignal();
    if(needUnlock) {this->unlockResource();}
}

const QVariant & COSVariantList::at(int i) const
{
    /*while(!this->m_resurceMutex->tryLock(5000));
*/

    if(this->m_list.isEmpty() ||
       i > (this->m_list.size() - 1) ||
       0 > i)
    {
        /*this->m_resurceMutex->unlock();*/
        return QVariant();
    }
    QVariant result(this->m_list.at(i));
    /*this->m_resurceMutex->unlock();*/
    return result;
}

//QVariant & COSVariantList::back()
//{
//    QVariant result(this->m_list.back());
//    return result;
//}

//const QVariant & COSVariantList::back() const
//{
//    if(this->m_list.isEmpty()) {
//        return QVariant();
//    }
//    QVariant result(this->m_list.back());
//    return result;
//}

//QList<QVariant>::iterator COSVariantList::begin()
//{
//    QList<QVariant>::iterator result(this->m_list.begin());
//    return result;
//}

//QList<QVariant>::const_iterator COSVariantList::begin() const {
//    return this->m_list.begin();
//}

//QList<QVariant>::const_iterator COSVariantList::cbegin() const {
//    return this->m_list.cbegin();
//}

//QList<QVariant>::const_iterator COSVariantList::cend() const {
//    return this->m_list.cend();
//}

void COSVariantList::clear()
{
    bool needUnlock(false);
    needUnlock = this->lockResource();
    this->m_list.clear();
    if(needUnlock) {this->unlockResource();}
}

//QList<QVariant>::const_iterator COSVariantList::constBegin() const {
//    return this->m_list.constBegin();
//}

//QList<QVariant>::const_iterator COSVariantList::constEnd() const {
//    return this->m_list.constEnd();
//}

//const QVariant & COSVariantList::constFirst() const {
//    if(this->m_list.isEmpty()) {
//        return QVariant();
//    }
//    return this->m_list.constFirst();
//}

//const QVariant & COSVariantList::constLast() const {
//    if(this->m_list.isEmpty()) {
//        return QVariant();
//    }
//    return this->m_list.constLast();
//}

bool COSVariantList::contains(const QVariant &value) const
{
    /*while(!this->m_resurceMutex->tryLock(5000));
*/
    bool result(this->m_list.contains(value));
    /*this->m_resurceMutex->unlock();*/
    return result;
}

int COSVariantList::count(const QVariant &value) const
{
    /*while(!this->m_resurceMutex->tryLock(5000));
*/
    bool result(this->m_list.count(value));
    /*this->m_resurceMutex->unlock();*/
    return result;
}

int COSVariantList::count() const
{
    /*while(!this->m_resurceMutex->tryLock(5000));
*/
    bool result(this->m_list.count());
    /*this->m_resurceMutex->unlock();*/
    return result;
}

//QList<QVariant>::const_reverse_iterator COSVariantList::crbegin() const {
//    return this->m_list.crbegin();
//}

//QList<QVariant>::const_reverse_iterator COSVariantList::crend() const {
//    return this->m_list.crend();
//}

//bool COSVariantList::empty() const {
//    return this->m_list.empty();
//}

//QList<QVariant>::iterator COSVariantList::end() {
//    return this->m_list.end();
//}

//QList<QVariant>::const_iterator COSVariantList::end() const {
//    return this->m_list.end();
//}

//bool COSVariantList::endsWith(const QVariant &value) const {
//    return this->m_list.endsWith(value);
//}

//QList<QVariant>::iterator COSVariantList::erase(QList<QVariant>::iterator pos) {
//    QList<QVariant>::iterator temp_ierator(this->m_list.erase(pos));

//    this->emitRemoveSignal();

//    return temp_ierator;
//}

//QList<QVariant>::iterator COSVariantList::erase(QList<QVariant>::iterator begin, QList<QVariant>::iterator end) {
//    QList<QVariant>::iterator temp_ierator(this->m_list.erase(begin, end));

//    this->emitRemoveSignal();

//    return temp_ierator;
//}

QVariant COSVariantList::first()
{
    bool needUnlock(false);
    needUnlock = this->lockResource();
    if(this->m_list.isEmpty()) {
        /*this->m_resurceMutex->unlock();*/
        return QVariant();
    }
    QVariant result(this->m_list.first());
    if(needUnlock) {this->unlockResource();}
    return result;
}

const QVariant & COSVariantList::first() const
{
    /*while(!this->m_resurceMutex->tryLock(5000));
*/

    if(this->m_list.isEmpty()) {
        /*this->m_resurceMutex->unlock();*/
        return QVariant();
    }
    QVariant result(this->m_list.first());
    /*this->m_resurceMutex->unlock();*/
    return result;
}

//QVariant & COSVariantList::front()
//{
//    this->lockResurce();
//    QVariant result(this->m_list.front());
//    if(needUnlock) {this->unlockResurce();}
//    return result;
//}

//const QVariant & COSVariantList::front() const {
//    if(this->m_list.isEmpty()) {
//        return QVariant();
//    }
//    return this->m_list.front();
//}

//int COSVariantList::indexOf(const QVariant &value, int from) const {
//    return this->m_list.indexOf(value, from);
//}

//void COSVariantList::insert(int i, const QVariant &value) {
//    this->m_list.insert(i, value);

////    this->emitAddSignal(i, value);
//    this->emitAddSignal();
//}

//QList<QVariant>::iterator COSVariantList::insert(QList<QVariant>::iterator before, const QVariant &value) {
//    QList<QVariant>::iterator temp_iterator(this->m_list.insert(before, value));

//    this->emitAddSignal();

//    return temp_iterator;
//}

bool COSVariantList::isEmpty() /*const*/
{
    bool needUnlock(false);
    needUnlock = this->lockResource();
    bool result(this->m_list.isEmpty());
    if(needUnlock) {this->unlockResource();}
    return result;
}

//QVariant & COSVariantList::last() {
//    return this->m_list.last();
//}

//const QVariant & COSVariantList::last() const {
//    if(this->m_list.isEmpty()) {
//        return QVariant();
//    }
//    return this->m_list.last();
//}

//int COSVariantList::lastIndexOf(const QVariant &value, int from) const {
//    return this->m_list.lastIndexOf(value, from);
//}

//int COSVariantList::length() const {
//    return this->m_list.length();
//}

QList<QVariant> COSVariantList::mid(int pos, int length) const
{
    /*while(!this->m_resurceMutex->tryLock(5000));
*/
    QList<QVariant> result(this->m_list.mid(pos, length));
    /*this->m_resurceMutex->unlock();*/
    return result;
}

//void COSVariantList::move(int from, int to) {
//    this->m_list.move(from, to);
//}

//void COSVariantList::pop_back() {
//    if(this->m_list.isEmpty()) {
//        return;
//    }
////    int remove_i(this->size() - 1);
////    QVariant remove_value(this->last());

//    this->m_list.pop_back();

////    this->emitRemoveSignal(remove_i, remove_value);
//    this->emitRemoveSignal();
//}

//void COSVariantList::pop_front() {
//    if(this->m_list.isEmpty()) {
//        return;
//    }
////    QVariant remove_value(this->last());

//    this->m_list.pop_front();

////    this->emitRemoveSignal(0, remove_value);
//    this->emitRemoveSignal();
//}

void COSVariantList::prepend(const QVariant &value)
{
    bool needUnlock(false);
    needUnlock = this->lockResource();
//    QVariant add_value(value);

    this->m_list.prepend(value);

//    this->emitAddSignal(0, add_value);
    this->emitAddSignal();
    if(needUnlock) {this->unlockResource();}
}

void COSVariantList::prepend(const QList<QVariant> &value)
{
    bool needUnlock(false);
    needUnlock = this->lockResource();

    QList<QVariant> temp_value(value);
    QVariant add_value(value);

    while(!temp_value.isEmpty()) {
        this->m_list.prepend(temp_value.last());
        temp_value.removeLast();
    }

//    this->emitAddSignal(0, add_value);
    this->emitAddSignal();
    if(needUnlock) {this->unlockResource();}
}

void COSVariantList::prepend(const COSVariantList &other)
{
    bool needUnlock(false);
    needUnlock = this->lockResource();

    QList<QVariant> value(other.mid(0));
    QVariant add_value(value);

    while(!value.isEmpty()) {
        this->m_list.prepend(value.last());
        value.removeLast();
    }

//    this->emitAddSignal(0, add_value);
    this->emitAddSignal();
    if(needUnlock) {this->unlockResource();}
}

//void COSVariantList::push_back(const QVariant &value) {
//    this->m_list.push_back(value);

////    this->emitAddSignal(this->size() - 1, value);
//    this->emitAddSignal();
//}

//void COSVariantList::push_front(const QVariant &value) {
//    this->m_list.push_front(value);

////    this->emitAddSignal(0, value);
//    this->emitAddSignal();
//}

//QList<QVariant>::reverse_iterator COSVariantList::rbegin() {
//    return this->m_list.rbegin();
//}

//QList<QVariant>::const_reverse_iterator COSVariantList::rbegin() const {
//    return this->m_list.rbegin();
//}

int COSVariantList::removeAll(const QVariant &value)
{
    bool needUnlock(false);
    needUnlock = this->lockResource();
    if(this->m_list.isEmpty())
    {
        if(needUnlock) {this->unlockResource();}
        return 0;
    }
    int count_remove_element(this->m_list.removeAll(value));

    this->emitRemoveSignal();

    if(needUnlock) {this->unlockResource();}
    return count_remove_element;
}

void COSVariantList::removeAt(int i)
{
    bool needUnlock(false);
    needUnlock = this->lockResource();
    if(this->m_list.isEmpty() ||
       i > (this->m_list.size() - 1) ||
       0 > i)
    {
        if(needUnlock) {this->unlockResource();}
        return;
    }

    this->m_list.removeAt(i);

    this->emitRemoveSignal();
    if(needUnlock) {this->unlockResource();}
}

void COSVariantList::removeFirst()
{
    bool needUnlock(false);
    needUnlock = this->lockResource();
    if(this->m_list.isEmpty())
    {
        if(needUnlock) {this->unlockResource();}
        return;
    }

    this->m_list.removeFirst();

    this->emitRemoveSignal();
    if(needUnlock) {this->unlockResource();}
}

void COSVariantList::removeLast()
{
    bool needUnlock(false);
    needUnlock = this->lockResource();
    if(this->m_list.isEmpty())
    {
        if(needUnlock) {this->unlockResource();}
        return;
    }

    this->m_list.removeFirst();

    this->emitRemoveSignal();
    if(needUnlock) {this->unlockResource();}
}

bool COSVariantList::removeOne(const QVariant &value)
{
    bool needUnlock(false);
    needUnlock = this->lockResource();
    bool remove_result(this->m_list.removeOne(value));

    this->emitRemoveSignal();

    if(needUnlock) {this->unlockResource();}
    return remove_result;
}

//QList<QVariant>::reverse_iterator COSVariantList::rend() {
//    return this->m_list.rend();
//}

//QList<QVariant>::const_reverse_iterator COSVariantList::rend() const {
//    return this->m_list.rend();
//}

//void COSVariantList::replace(int i, const QVariant &value) {
//    this->m_list.replace(i, value);
//}

//void COSVariantList::reserve(int alloc) {
//    this->m_list.reserve(alloc);
//}

int COSVariantList::size() const
{
    /*while(!this->m_resurceMutex->tryLock(5000));
*/
    int result(this->m_list.size());
    /*this->m_resurceMutex->unlock();*/
    return result;
}

//bool COSVariantList::startsWith(const QVariant &value) const {
//    return this->m_list.startsWith(value);
//}

//void COSVariantList::swap(QList<QVariant> &other) {
//    return this->m_list.swap(other);
//}

//void COSVariantList::swap(int i, int j) {
//    if(this->m_list.isEmpty() ||
//       i > (this->m_list.size() - 1) ||
//       0 > i ||
//       j > (this->m_list.size() - 1) ||
//       0 > j) {
//        return;
//    }

//    this->m_list.swap(i, j);
//}

QVariant COSVariantList::takeAt(int i)
{
    bool needUnlock(false);
    needUnlock = this->lockResource();
    if(this->m_list.isEmpty() ||
       i > (this->m_list.size() - 1) ||
       0 > i)
    {
        if(needUnlock) {this->unlockResource();}
        return QVariant();
    }

    QVariant take_value(this->m_list.takeAt(i));

    this->emitRemoveSignal();

    if(needUnlock) {this->unlockResource();}
    return take_value;
}

QVariant COSVariantList::takeFirst()
{
    bool needUnlock(false);
    needUnlock = this->lockResource();
    if(this->m_list.isEmpty())
    {
        if(needUnlock) {this->unlockResource();}
        return QVariant();
    }

    QVariant take_value;
    if(this->m_list.isEmpty())
    {
        take_value = QVariant();
    }
    else
    {
        take_value = this->m_list[0];
        int index_rm(this->m_list.indexOf(take_value));
        if(-1 != index_rm)
            this->m_list.removeAt(index_rm);
    }

//    QVariant take_value(this->m_list.isEmpty() ? QVariant() : this->m_list.first());
//    if(!this->m_list.isEmpty())
//        this->m_list.removeFirst();

    this->emitRemoveSignal();

    if(needUnlock) {this->unlockResource();}
    return take_value;
}

QVariant COSVariantList::takeLast()
{
    bool needUnlock(false);
    needUnlock = this->lockResource();
    if(this->m_list.isEmpty())
    {
        if(needUnlock) {this->unlockResource();}
        return QVariant();
    }

    QVariant take_value(this->m_list.takeLast());

    this->emitRemoveSignal();

    if(needUnlock) {this->unlockResource();}
    return take_value;
}

//QSet<QVariant> COSVariantList::toSet() const
//{
//    return this->m_list.toSet();
//}

//std::list<QVariant> COSVariantList::toStdList() const
//{
//    return this->m_list.toStdList();
//}

//QVector<QVariant> COSVariantList::toVector() const
//{
//    return this->m_list.toVector();
//}

QVariant COSVariantList::value(int i) const
{
    /*while(!this->m_resurceMutex->tryLock(5000));
*/
    QVariant result(this->m_list.value(i));
    /*this->m_resurceMutex->unlock();*/
    return result;
}

QVariant COSVariantList::value(int i, const QVariant &defaultValue) const
{
    /*while(!this->m_resurceMutex->tryLock(5000));
*/
    QVariant result(this->m_list.isEmpty() ? QVariant() : this->m_list.value(i, defaultValue));
    /*this->m_resurceMutex->unlock();*/
    return result;
}


bool COSVariantList::operator!=(const QList<QVariant> &other) const {
    return this->m_list.mid(0) != other;
}

bool COSVariantList::operator!=(const COSVariantList &other) const {
    return this->m_list.mid(0) != other.mid(0);
}

QList<QVariant> COSVariantList::operator+(const QList<QVariant> &other) const {
    COSVariantList temp;
    temp.append(other);
    return temp.mid(0);
}

QList<QVariant> COSVariantList::operator+(const COSVariantList &other) const {
    COSVariantList temp;
    temp.append(other);
    return temp.mid(0);
}

QList<QVariant> COSVariantList::operator+(const QVariant &value) const {
    COSVariantList temp;
    temp.append(value);
    return temp.mid(0);
}

QList<QVariant> COSVariantList::operator+=(const QList<QVariant> &other) {
    this->append(other);
    return this->mid(0);
}

QList<QVariant> COSVariantList::operator+=(const COSVariantList &other) {
    this->append(other);
    return this->mid(0);
}

QList<QVariant> COSVariantList::operator+=(const QVariant &value) {
    this->append(value);
    return this->mid(0);
}

//QList<QVariant>  COSVariantList::operator<<(const QList<QVariant> &other)
//{
//    this->append(other);
//    return this->mid(0);
//}

//QList<QVariant>  COSVariantList::operator<<(const COSVariantList &other)
//{
//    this->append(other);
//    return this->mid(0);
//}

//QList<QVariant>  COSVariantList::operator<<(const QVariant &value)
//{
//    this->append(value);
//    return this->mid(0);
//}

QList<QVariant> COSVariantList::operator=(const QList<QVariant> &other) {
    this->clear();
    this->append(other);
    return this->mid(0);
}

QList<QVariant> COSVariantList::operator=(const COSVariantList &other) {
    this->clear();
    this->append(other);
    return this->mid(0);
}

QList<QVariant> COSVariantList::operator=(const QVariant &value) {
    this->clear();
    this->append(value);
    return this->mid(0);
}

//QList & COSVariantList::operator=(QList<QVariant> &&other)
//{
//    this->clear();
//    this->append(other);
//    return this->mid(0);
//}

bool COSVariantList::operator==(const QList<QVariant> &other) const {
    return this->m_list.mid(0) == other;
}

bool COSVariantList::operator==(const COSVariantList &other) const {
    return this->m_list.mid(0) == other.mid(0);
}

//    QVariant & COSVariantList::operator[](int i)
//{
//}

//    const QVariant & COSVariantList::operator[](int i) const
//{
//}

bool COSVariantList::blockArgSignals() {
    return flag_emitArgSignal;
}

bool COSVariantList::blockAddSignals() {
    return flag_emitAddSignal;
}

bool COSVariantList::blockRemoveSignals() {
    return flag_emitRemoveSignal;
}

void COSVariantList::setBlockArgSignals(bool value) {
    flag_emitArgSignal = value;
}
void COSVariantList::setBlockAddSignals(bool value) {
    flag_emitAddSignal = value;
}

void COSVariantList::setBlockRemoveSignals(bool value) {
    flag_emitRemoveSignal = value;
}

//void COSVariantList::emitAddSignal(int i, QVariant value) {
//    if(!flag_emitAddSignal) {
//        return;
//    }

////    if(flag_emitArgSignal) {
////        emit this->addedElenent(i, value);
////    }
////    else {
//        emit this->addedElenent();
////    }
//}
//void COSVariantList::emitRemoveSignal(int i, QVariant value) {
//    if(!flag_emitRemoveSignal) {
//        return;
//    }

////    if(flag_emitArgSignal) {
////        emit this->removedElenent(i, value);
////    }
////    else {
//        emit this->removedElenent();
////    }
//}

void COSVariantList::emitAddSignal() {
    if(!flag_emitAddSignal) {
        return;
    }
    emit this->addedElenent();
}
void COSVariantList::emitRemoveSignal() {
    if(!flag_emitRemoveSignal) {
        return;
    }
    emit this->removedElenent();
}

bool COSVariantList::lockResource()
{
    if(false == flag_resurce)
    {
//        while(false == this->m_resurceMutex->tryLock(50));
        flag_resurce = true;
        return true;
    }
    else
    {
        qWarning("Assert(softly lock) [COSVariantList::lockResurce]");
//        qWarning("Assert(softly lock) [COSVariantList::lockResurce(%d)]", this->thread()->currentThreadId());
    }
    return false;
}

void COSVariantList::unlockResource()
{
    if(true == flag_resurce)
    {
//        this->m_resurceMutex->unlock();
        flag_resurce = false;
    }
    else
    {
        qWarning("Assert(softly unlock) [COSVariantList::unlockResurce]");
//        qWarning("Assert(softly unlock) [COSVariantList::unlockResurce(%d)]", this->thread()->currentThreadId());
    }
}
