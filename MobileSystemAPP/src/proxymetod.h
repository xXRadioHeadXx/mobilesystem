#ifndef PROXYMETOD_H
#define PROXYMETOD_H

#include <QObject>
#include <global_value_and_structure.h>

class ProxyMetod : public QObject
{
    Q_OBJECT
public:
    explicit ProxyMetod(QObject *parent = nullptr) /*noexcept*/;

    QByteArray sumQListQByteArray(QList<QByteArray> list) /*noexcept*/;

    QByteArray msgColorToByteArray(bool ioType,
                                   QByteArray baMSG,
                                   TypeUnitNodeSystem typeSystem) /*noexcept*/;
    QColor msgColor(bool ioType,
                    QByteArray baMSG,
                    TypeUnitNodeSystem typeSystem) /*noexcept*/;
    QString msgCommentRUS(QByteArray baMSG,
                          TypeUnitNodeSystem typeSystem) /*noexcept*/;
    QColor msgColor_KM(bool ioType,
                       QByteArray baMSG) /*noexcept*/;
    QString msgCommentRUS_KM(QByteArray baMSG) /*noexcept*/;
    QColor msgColor_KS(bool ioType,
                       QByteArray baMSG) /*noexcept*/;
    QString msgCommentRUS_KS(QByteArray baMSG) /*noexcept*/;
    QColor msgColor_BSK(bool ioType,
                        QByteArray baMSG) /*noexcept*/;
    QString msgCommentRUS_BSK(QByteArray baMSG) /*noexcept*/;
    QColor msgColor_SYS(bool ioType,
                        QByteArray baMSG) /*noexcept*/;
    QString msgCommentRUS_SYS(QByteArray baMSG) /*noexcept*/;
    QString msgCommentENG(QByteArray baMSG,
                          TypeUnitNodeSystem typeSystem) /*noexcept*/;
    QString msgCommentENG_KM(QByteArray baMSG) /*noexcept*/;
    QString msgCommentENG_KS(QByteArray baMSG) /*noexcept*/;
    QString msgCommentENG_BSK(QByteArray baMSG) /*noexcept*/;
    QString msgCommentENG_SYS(QByteArray baMSG) /*noexcept*/;

    QString getNameDevice(QByteArray id, TypeUnitNodeSystem typeSystem) /*noexcept*/;
    QString getNameDevice_RUS(QByteArray id, TypeUnitNodeSystem typeSystem) /*noexcept*/;
    QString getNameDevice_ENG(QByteArray id, TypeUnitNodeSystem typeSystem) /*noexcept*/;

    QString getNameSO(QByteArray idSO) /*noexcept*/;
    QString getNameSO_RUS(QByteArray idSO) /*noexcept*/;
    QString getNameSO_ENG(QByteArray idSO) /*noexcept*/;

    QString getTypeIPA(quint8 ipa) /*noexcept*/;
    QString getTypeIPA_RUS(quint8 ipa) /*noexcept*/;
    QString getTypeIPA_ENG(quint8 ipa) /*noexcept*/;


signals:

public slots:
};

#endif // PROXYMETOD_H
