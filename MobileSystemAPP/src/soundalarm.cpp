﻿#include <soundalarm.h>

SoundAlarm::SoundAlarm(QObject *parent) /*noexcept*/ :
    QThread(parent)
{
}

void SoundAlarm::run() /*noexcept*/
{
#ifdef Q_OS_LINUX
    QApplication::beep();
    QApplication::beep();
    QApplication::beep();
    QApplication::beep();
    QApplication::beep();
    QApplication::beep();
#endif

#ifdef Q_OS_WIN
    Beep(523,100);
    Beep(659,100);
    Beep(784,100);
    Beep(523,100);
    Beep(659,100);
    Beep(784,100);
#endif
    this->quit();
}
