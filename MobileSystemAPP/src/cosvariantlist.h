#ifndef COSVARIANTLIST_H
#define COSVARIANTLIST_H

#include <QObject>
#include <QVariant>
#include <QList>
#include <QMutex>
#include <QThread>
//#include <QVector>
//#include <QSet>


class COSVariantList : public QObject
{
    Q_OBJECT
public:
    explicit COSVariantList(QObject *parent = nullptr);

//    COSVariantList(const QList<QVariant> &other, QObject *parent = nullptr);
//    COSVariantList(const COSVariantList &other, QObject *parent = nullptr);

//    COSVariantList(QList<QVariant> &&other, QObject *parent = nullptr);
//    COSVariantList(COSVariantList &&other, QObject *parent = nullptr);

//    COSVariantList(std::initializer_list<QVariant> args, QObject *parent = nullptr);

    virtual ~COSVariantList();

    virtual void append(const QVariant &value);
    virtual void append(const QList<QVariant> &value);
    virtual void append(const COSVariantList &value);
    virtual bool appendUnique(const QVariant &value);
    const QVariant & at(int i) const;
//    QVariant & back();
//    const QVariant & back() const;
//    QList<QVariant>::iterator begin();
//    QList<QVariant>::const_iterator begin() const;
//    QList<QVariant>::const_iterator cbegin() const;
//    QList<QVariant>::const_iterator cend() const;
    void clear();
//    QList<QVariant>::const_iterator constBegin() const;
//    QList<QVariant>::const_iterator constEnd() const;
//    const QVariant & constFirst() const;
//    const QVariant & constLast() const;
    bool contains(const QVariant &value) const;
    int count(const QVariant &value) const;
    int count() const;
//    QList<QVariant>::const_reverse_iterator crbegin() const;
//    QList<QVariant>::const_reverse_iterator crend() const;
//    bool empty() const;
//    QList<QVariant>::iterator end();
//    QList<QVariant>::const_iterator end() const;
//    bool endsWith(const QVariant &value) const;
//    QList<QVariant>::iterator erase(QList<QVariant>::iterator pos);
//    QList<QVariant>::iterator erase(QList<QVariant>::iterator begin, QList<QVariant>::iterator end);
    QVariant first();
    const QVariant & first() const;
//    QVariant & front();
//    const QVariant & front() const;
//    int indexOf(const QVariant &value, int from = 0) const;
//    virtual void insert(int i, const QVariant &value);
//    virtual QList<QVariant>::iterator insert(QList<QVariant>::iterator before, const QVariant &value);
    bool isEmpty() /*const*/;
//    QVariant & last();
//    const QVariant & last() const;
//    int lastIndexOf(const QVariant &value, int from = -1) const;
//    int length() const;
    QList<QVariant> mid(int pos, int length = -1) const;
//    void move(int from, int to);
//    void pop_back();
//    void pop_front();
    virtual void prepend(const QVariant &value);
    virtual void prepend(const QList<QVariant> &value);
    virtual void prepend(const COSVariantList &other);
//    virtual void push_back(const QVariant &value);
//    virtual void push_front(const QVariant &value);
//    QList<QVariant>::reverse_iterator rbegin();
//    QList<QVariant>::const_reverse_iterator rbegin() const;
    int removeAll(const QVariant &value);
    void removeAt(int i);
    void removeFirst();
    void removeLast();
    bool removeOne(const QVariant &value);
//    QList<QVariant>::reverse_iterator rend();
//    QList<QVariant>::const_reverse_iterator rend() const;
//    virtual void replace(int i, const QVariant &value);
//    void reserve(int alloc);
    int size() const;
//    bool startsWith(const QVariant &value) const;
//    void swap(QList<QVariant> &other);
//    void swap(int i, int j);
    QVariant takeAt(int i);
    QVariant takeFirst();
    QVariant takeLast();
////    QSet<QVariant> toSet() const;
////    std::list<QVariant> toStdList() const;
////    QVector<QVariant> toVector() const;
    QVariant value(int i) const;
    QVariant value(int i, const QVariant &defaultValue) const;

    bool operator!=(const QList<QVariant> &other) const;
    bool operator!=(const COSVariantList &other) const;

    QList<QVariant> operator+(const QList<QVariant> &other) const;
    QList<QVariant> operator+(const COSVariantList &other) const;
    QList<QVariant> operator+(const QVariant &other) const;

    QList<QVariant> operator+=(const QList<QVariant> &other);
    QList<QVariant> operator+=(const COSVariantList &other);
    QList<QVariant> operator+=(const QVariant &value);

//    QList<QVariant>  operator<<(const QList<QVariant> &other);
//    QList<QVariant>  operator<<(const COSVariantList &other);
//    QList<QVariant>  operator<<(const QVariant &value);

    QList<QVariant> operator=(const QList<QVariant> &other);
    QList<QVariant> operator=(const COSVariantList &other);
    QList<QVariant> operator=(const QVariant &value);
//    QList & operator=(QList<QVariant> &&other);

    bool operator==(const QList<QVariant> &other) const;
    bool operator==(const COSVariantList &other) const;

//    QVariant & operator[](int i);
//    QVariant & operator[](int i);
//    const QVariant & operator[](int i) const;
//    const QVariant & operator[](int i) const;

    bool blockArgSignals();
    bool blockAddSignals();
    bool blockRemoveSignals();
    void setBlockArgSignals(bool value);
    void setBlockAddSignals(bool value);
    void setBlockRemoveSignals(bool value);

    QList<QVariant> m_list;

protected:
//    void emitAddSignal(int i = -1, QVariant value = QVariant());
    void emitAddSignal();
//    void emitRemoveSignal(int i = -1, QVariant value = QVariant());
    void emitRemoveSignal();

private:

//    QList<QVariant> * m_list;

    bool flag_emitArgSignal,
         flag_emitAddSignal,
         flag_emitRemoveSignal,
         flag_resurce;

//    QMutex *m_resurceMutex;
    bool lockResource();
    void unlockResource();

signals:
//    void addedElenent(int i, const QVariant &value);
    void addedElenent();
//    void removedElenent(int i, const QVariant &value);
    void removedElenent();

public slots:
};

#endif // COSVARIANTLIST_H
