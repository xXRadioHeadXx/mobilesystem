﻿#ifndef SOUNDALARM_H
#define SOUNDALARM_H

#include <QThread>
#include <QApplication>
#include <QTextCodec>
#include <QtSql>
//#include <unistd.h>
#include <QGraphicsPixmapItem>
#include <QGraphicsTextItem>
#include <QBitmap>
#include <QTextStream>
#ifdef Q_OS_WIN
#include <windows.h>
#endif

class SoundAlarm : public QThread
{
    Q_OBJECT
public:
    explicit SoundAlarm(QObject *parent = nullptr) /*noexcept*/;
    
private:
    void run() /*noexcept*/;
signals:
    
public slots:
    
};

#endif // SOUNDALARM_H
