﻿#include <databasemanager.h>

DataBaseManager::DataBaseManager(QObject *parent) /*noexcept*/ :
    QObject(parent)
{
    m_resurceStatus = false;

    if(QFile::exists("archive.db3"))
    {//открываем сущестыующую
        if(m_db.isValid() || m_db.isOpen())
        {
            m_db.close();
        }
        m_db = QSqlDatabase::addDatabase("QSQLITE");
        m_db.setConnectOptions();
        m_db.setDatabaseName("archive.db3");
        m_db.open();
        if (!m_db.isOpen())
        {
#ifdef QT_DEBUG
            qDebug() << "Cannot open database: archive.db3 Error: " << m_db.lastError();
#endif
        }
        else
        {
#ifdef QT_DEBUG
            qDebug()<<"Opened database: archive.db3";
//            this->createDataBase();
#endif
//            QSqlQuery query("PRAGMA journal_mode = TRUNCATE; "
//                            "PRAGMA synchronous = OFF;",
//                            m_db);
//            query.exec();

        }
    }
    else
    {//создаём новую
        QFile fBase("archive.db3");
        fBase.open(QIODevice::ReadWrite);
        fBase.close();
        m_db = QSqlDatabase::addDatabase("QSQLITE");
        m_db.setConnectOptions();
        m_db.setDatabaseName("archive.db3");
        m_db.open();
        if (!m_db.isOpen())
        {
#ifdef QT_DEBUG
            qDebug() << "Cannot open database: archive.db3 Error: " << m_db.lastError();
#endif
        }
        else
        {
#ifdef QT_DEBUG
            qDebug()<<"Opened database: archive.db3";
#endif
            this->createDataBase();
//            QSqlQuery query("PRAGMA journal_mode = TRUNCATE; "
//                            "PRAGMA synchronous = OFF;",
//                            m_db);
//            query.exec();
        }
    }
    this->addNewMSG(0x00,
                    0xFF,
                    "Start session",
                    true,
                    UnknownSystemType,
                    System_TypeGroupMSG);
}

DataBaseManager::~DataBaseManager() /*noexcept*/
{
    try
    {
        this->addNewMSG(0x00,
                        0xFF,
                        "Complete session",
                        true,
                        UnknownSystemType,
                        System_TypeGroupMSG);

//        QSqlQuery query("PRAGMA journal_mode = DELETE; "
//                        "PRAGMA synchronous = FULL;",
//                        m_db);
//        query.exec();

        if(m_db.isOpen())
            m_db.close();
    }
    catch(...)
    {
#ifdef QT_DEBUG
        qDebug() << "Assert(~DataBaseManager)";
#endif
        return;
    }
}

bool DataBaseManager::lockResource()
{
    if(true == m_resurceStatus)
    {
        while(false == m_resursLock->tryLock(100)){};
        m_resurceStatus = true;
        return true;
    }
    else
    {
        qWarning("Assert(softly lock) [DataBaseManager::lockResurce]");
    }
    return false;
}

void DataBaseManager::unlockResource()
{
    if(true == m_resurceStatus)
    {
        m_resursLock->unlock();
        m_resurceStatus = false;
    }
    else
    {
        qWarning("Assert(softly unlock) [DataBaseManager::unlockResurce]");
    }
}

void DataBaseManager::setTypeLanguage(QString type) /*noexcept*/
{
    m_typeLanguage = type;
}

QString DataBaseManager::typeLanguage() /*noexcept*/
{
    return m_typeLanguage;
}


void DataBaseManager::createDataBase() /*noexcept*/
{
    QSqlQuery query(m_db);
    //создание списка устройств
//    query.exec("DROP TABLE IF EXISTS ArchiveUnitNode;");
    query.exec("CREATE TABLE IF NOT EXISTS [ArchiveUnitNode] ( "
                   "[id] INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "
                   "[idUN] BLOB, "
                   "[macUN] BLOB, "
                   "[nameUN] BLOB, "
                   "[abstractNameUN] CHAR, "
                   "[typeUN] INTEGER, "
                   "[typeSystemUN] INTEGER, "
                   "[link] BLOB, " //
                   "[linkValid] INTEGER, " //
                   "[xPos] INTEGER DEFAULT (0), "
                   "[yPos] INTEGER DEFAULT (0), "
                   "[voltage] INTEGER DEFAULT (0), "
                   "[area] INTEGER, "
                   "[number] INTEGER, "
                   "[discharge] INTEGER DEFAULT (0), "
                   "[idParentUN] INTEGER DEFAULT (0), "
                   "[modeUN] INTEGER, "
                   "[hidden] INTEGER DEFAULT (0), "
                   "[Zone] INTEGER DEFAULT (0), "

                   "[modeVK1] INTEGER DEFAULT (2), "
                   "[ConnectVK1_1] BLOB, "
                   "[ConnectVK1_2] BLOB,"
                   "[ConnectVK1_3] BLOB, "
                   "[ConnectVK1_4] BLOB, "
                   "[ConnectVK1_5] BLOB, "
                   "[ConnectVK1_6] BLOB, "
                   "[ConnectVK1_7] BLOB, "
                   "[ConnectVK1_8] BLOB, "
                   "[ConnectVK1_9] BLOB, "
                   "[ConnectVK1_10] BLOB, "
                   "[ConnectVK1_11] BLOB, "
                   "[ConnectVK1_12] BLOB, "
                   "[ConnectVK1_13] BLOB, "
                   "[ConnectVK1_14] BLOB, "
                   "[ConnectVK1_15] BLOB, "
                   "[ConnectVK1_16] BLOB, "
                   "[ConnectVK1_17] BLOB, "
                   "[ConnectVK1_18] BLOB, "
                   "[ConnectVK1_19] BLOB, "
                   "[ConnectVK1_20] BLOB, "

                   "[modeVK2] INTEGER DEFAULT (2), "
                   "[ConnectVK2_1] BLOB, "
                   "[ConnectVK2_2] BLOB, "
                   "[ConnectVK2_3] BLOB, "
                   "[ConnectVK2_4] BLOB, "
                   "[ConnectVK2_5] BLOB, "
                   "[ConnectVK2_6] BLOB, "
                   "[ConnectVK2_7] BLOB, "
                   "[ConnectVK2_8] BLOB, "
                   "[ConnectVK2_9] BLOB, "
                   "[ConnectVK2_10] BLOB, "
                   "[ConnectVK2_11] BLOB, "
                   "[ConnectVK2_12] BLOB, "
                   "[ConnectVK2_13] BLOB, "
                   "[ConnectVK2_14] BLOB, "
                   "[ConnectVK2_15] BLOB, "
                   "[ConnectVK2_16] BLOB, "
                   "[ConnectVK2_17] BLOB, "
                   "[ConnectVK2_18] BLOB, "
                   "[ConnectVK2_19] BLOB, "
                   "[ConnectVK2_20] BLOB, "

                   "[xPosLFlangP_1] INTEGER DEFAULT (0), "
                   "[yPosLFlangP_1] INTEGER DEFAULT (0), "
                   "[xPosLFlangP_2] INTEGER DEFAULT (0), "
                   "[yPosLFlangP_2] INTEGER DEFAULT (0), "
                   "[xPosLFlangP_3] INTEGER DEFAULT (0), "
                   "[yPosLFlangP_3] INTEGER DEFAULT (0), "
                   "[xPosLFlangP_4] INTEGER DEFAULT (0), "
                   "[yPosLFlangP_4] INTEGER DEFAULT (0), "
                   "[xPosLFlangP_5] INTEGER DEFAULT (0), "
                   "[yPosLFlangP_5] INTEGER DEFAULT (0), "

                   "[xPosRFlangP_1] INTEGER DEFAULT (0), "
                   "[yPosRFlangP_1] INTEGER DEFAULT (0), "
                   "[xPosRFlangP_2] INTEGER DEFAULT (0), "
                   "[yPosRFlangP_2] INTEGER DEFAULT (0), "
                   "[xPosRFlangP_3] INTEGER DEFAULT (0), "
                   "[yPosRFlangP_3] INTEGER DEFAULT (0), "
                   "[xPosRFlangP_4] INTEGER DEFAULT (0), "
                   "[yPosRFlangP_4] INTEGER DEFAULT (0), "
                   "[xPosRFlangP_5] INTEGER DEFAULT (0), "
                   "[yPosRFlangP_5] INTEGER DEFAULT (0), "

                   "[geoAltitude] DOUBLE DEFAULT (0), "

                   "[geoLatitudeFlag] INTEGER DEFAULT (0), "
                   "[geoLatitude] DOUBLE DEFAULT (0), "
                   "[geoLatitudeDegrees] DOUBLE DEFAULT (0), "
                   "[geoLatitudeMinutes] DOUBLE DEFAULT (0), "
                   "[geoLatitudeSeconds] DOUBLE DEFAULT (0), "

                   "[geoLongitudeFlag] INTEGER DEFAULT (0), "
                   "[geoLongitude] DOUBLE DEFAULT (0), "
                   "[geoLongitudeDegrees] DOUBLE DEFAULT (0), "
                   "[geoLongitudeMinutes] DOUBLE DEFAULT (0), "
                   "[geoLongitudeSeconds] DOUBLE DEFAULT (0) );");



    //создание таблицы архива сообщений

//    query.exec( "DROP TABLE IF EXISTS ArchiveMSG;");
    query.exec("CREATE TABLE IF NOT EXISTS [ArchiveMSG] ( "
                   "[id] INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "
                   "[ioType] INTEGER, "
                   "[typeUN] INTEGER, "
                   "[typeSystemMSG] INTEGER, "
                   "[idUN] BLOB, "
                   "[numberNodeUN] INTEGER DEFAULT (0), "
                   "[numberAreaUN] INTEGER DEFAULT (0), "
                   "[foreignIDUN] INTEGER, "
                   "[msg] BLOB, "
                   "[typeMSG] INTEGER, "
                   "[codeMSG] INTEGER, "
                   "[groupMSG] INTEGER DEFAULT (0), "

                   "[date] DATE,"
                   "[time] TIME, "
                   "[timeCorection] TIME, "
                   "[commentRUS] CHAR, "
                   "[commentENG] CHAR, "
                   "[color] BLOB, "
                   "[hidden] INTEGER);");

    //создание архива кадров
//    query.exec( "DROP TABLE IF EXISTS ArchiveIMG;");
    query.exec( "CREATE TABLE IF NOT EXISTS [ArchiveIMG] ( "
                   "[id] INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "
                   "[foreignIDMainMSG] INTEGER, "
                   "[foreignIDThisMSGIMG1] INTEGER, "
                   "[sizeIMG1] INTEGER, "
                   "[dataIMG1] BLOB, "
                   "[foreignIDThisMSGIMG2] INTEGER, "
                   "[sizeIMG2] INTEGER, "
                   "[dataIMG2] BLOB, "
                   "[foreignIDThisMSGIMG3] INTEGER, "
                   "[sizeIMG3] INTEGER, "
                   "[dataIMG3] BLOB, "
                   "[foreignIDThisMSGIMG4] INTEGER, "
                   "[sizeIMG4] INTEGER, "
                   "[dataIMG4] BLOB, "
                   "[foreignIDThisMSGIMG5] INTEGER, "
                   "[sizeIMG5] INTEGER, "
                   "[dataIMG5] BLOB, "
                   "[checkedUp] INTEGER, "
                   "[foreignIDAlarm] INTEGER, "
                   "[numberVK] INTEGER, "
                   "[hidden] INTEGER);");

}

quint32 DataBaseManager::setIMGCheckedUp(quint32 index,
                                         bool value) /*noexcept*/
{
    bool needUnlock(false);
    needUnlock = this->lockResource();

    QSqlQuery queryIMG(m_db);
    queryIMG.prepare("UPDATE ArchiveIMG "
                     "SET checkedUp = :checkedUpV "
                     "WHERE id = :index;");
    queryIMG.bindValue(":checkedUpV", (value ? 1 : 0));
    queryIMG.bindValue(":index", index);
    if(queryIMG.exec())
    {
        if(needUnlock){this->unlockResource();}
        return index;
    }
    else
    {
#ifdef QT_DEBUG
        qDebug() << "DataBaseManager::setIMGCheckedUp Error :" << queryIMG.lastError().text();
#endif
        if(needUnlock){this->unlockResource();}
        return 0;
    }
}

quint32 DataBaseManager::addNewIMGPack(UnitNode* un) /*noexcept*/
{
    if(KMSystemType == un->_typeSystem())
    {
        return this->addNewIMGPackFromBT(un);
    }
    if(KSSystemType == un->_typeSystem())
    {
        return this->addNewIMGPackFromKS(un);
    }
    return 0;
}

quint32 DataBaseManager::addNewIMGPackFromBT(UnitNode* un,
                                             /*quint8 type,*/
                                             QByteArray msg/*,
                                             bool ioType*/) /*noexcept*/
{
    bool needUnlock(false);
    needUnlock = this->lockResource();

    quint32 result(0);
    if(un &&
       (quint8)0xC4 == (quint8)msg.at(8))
    {
        if(un->pack.idC1MSG)
        {
            QSqlQuery query(m_db);
            query.prepare("SELECT * FROM ArchiveIMG WHERE foreignIDMainMSG = :idC1MSG;");
            query.bindValue(":idC1MSG", un->pack.idC1MSG);

            if(query.exec())
            {
                while (query.next())
                {
                    result = this->updateIMGPackFromBT(query.record().value("id").toUInt(),
                                                       un,
                                                       /*type,*/
                                                       msg/*,
                                                       ioType*/);

                    if(needUnlock){this->unlockResource();}

                    return result;
                }

                result = this->addNewIMGPackFromBT(un);

                if(needUnlock){this->unlockResource();}

                return result;
            }
            else
            {
#ifdef QT_DEBUG
                qDebug() << "DataBaseManager::addNewIMGPackFromBT Error :" << query.lastError().text();
#endif

                if(needUnlock){this->unlockResource();}

                return 0;

            }

        }
    }

    if(needUnlock){this->unlockResource();}

    return 0;
}

quint32 DataBaseManager::updateIMGPackFromBT(quint32 index,
                                             UnitNode* un,
                                             /*quint8 type,*/
                                             QByteArray msg/*,
                                             bool ioType*/) /*noexcept*/
{
    QSqlQuery query(m_db);
    QString sizeIMGF("sizeIMG%1"),
            dataIMGF("dataIMG%1");
    sizeIMGF = sizeIMGF.arg((quint8)msg.at(12));
    dataIMGF = dataIMGF.arg((quint8)msg.at(12));

    query.prepare("UPDATE ArchiveIMG SET " +
                  sizeIMGF + " = :sizeIMGV, " +
                  dataIMGF + " = :dataIMGV, "
                  "checkedUp = :checkedUpV "
                  "WHERE id = :id;");

    for(int i(0); i < un->pack.listIMGItem.size(); i++)
    {
        if(un->pack.listIMGItem.at(i).num == (quint8)msg.at(12))
        {
            un->pack.listIMGItem.removeAt(i);
        }
        else
        {
            i++;
        }
    }


    QByteArray imgData(msg);
    imgData.remove(0, 17);
    imgData.chop(1);

    query.bindValue(":sizeIMGV", (quint32)imgData.size());
    query.bindValue(":dataIMGV", imgData);
    query.bindValue(":checkedUp", (false ? 1 : 0));
    query.bindValue(":id", index);

    if(query.exec())
    {
        emit this->insertNewIMG(index);
        return index;
    }
    else
    {
#ifdef QT_DEBUG
        qDebug() << "DataBaseManager::updateIMGPackFromBT Error :" << query.lastError().text();
#endif
        return 0;
    }
}

quint32 DataBaseManager::addNewIMGPackFromBT(UnitNode* un) /*noexcept*/
{
    bool needUnlock(false);
    needUnlock = this->lockResource();

    quint32 result(0);

    QSqlQuery query(m_db);
    query.prepare("INSERT INTO ArchiveIMG "
                  "(foreignIDMainMSG, "
                   "foreignIDThisMSGIMG1, "
                   "sizeIMG1, "
                   "dataIMG1, "
                   "foreignIDThisMSGIMG2, "
                   "sizeIMG2, "
                   "dataIMG2, "
                   "foreignIDThisMSGIMG3, "
                   "sizeIMG3, "
                   "dataIMG3, "
                   "foreignIDThisMSGIMG4, "
                   "sizeIMG4, "
                   "dataIMG4, "
                   "foreignIDThisMSGIMG5, "
                   "sizeIMG5, "
                   "dataIMG5, "
                   "checkedUp, "
                   "foreignIDAlarm, "
                   "numberVK, "
                   "hidden) "
                   "VALUES (:foreignIDMainMSG, "
                          ":foreignIDThisMSGIMG1, "
                          ":sizeIMG1, "
                          ":dataIMG1, "
                          ":foreignIDThisMSGIMG2, "
                          ":sizeIMG2, "
                          ":dataIMG2, "
                          ":foreignIDThisMSGIMG3, "
                          ":sizeIMG3, "
                          ":dataIMG3, "
                          ":foreignIDThisMSGIMG4, "
                          ":sizeIMG4, "
                          ":dataIMG4, "
                          ":foreignIDThisMSGIMG5, "
                          ":sizeIMG5, "
                          ":dataIMG5, "
                          ":checkedUp, "
                          ":foreignIDAlarm, "
                          ":numberVK, "
                          ":hidden);");
    query.bindValue(":foreignIDMainMSG", un->pack.idC1MSG);
    query.bindValue(":foreignIDThisMSGIMG1", 0);
    query.bindValue(":sizeIMG1", 0);
    query.bindValue(":dataIMG1", QByteArray());
    query.bindValue(":foreignIDThisMSGIMG2", 0);
    query.bindValue(":sizeIMG2", 0);
    query.bindValue(":dataIMG2", QByteArray());
    query.bindValue(":foreignIDThisMSGIMG3", 0);
    query.bindValue(":sizeIMG3", 0);
    query.bindValue(":dataIMG3", QByteArray());
    query.bindValue(":foreignIDThisMSGIMG4", 0);
    query.bindValue(":sizeIMG4", 0);
    query.bindValue(":dataIMG4", QByteArray());
    query.bindValue(":foreignIDThisMSGIMG5", 0);
    query.bindValue(":sizeIMG5", 0);
    query.bindValue(":dataIMG5", QByteArray());
    query.bindValue(":checkedUp", (false ? 1 : 0));
    query.bindValue(":foreignIDAlarm", 0);
    query.bindValue(":numberVK", 1);
    query.bindValue(":hidden", 0);
    while(un->pack.listIMGItem.size())
    {
        QString foreignIDThisMSGIMGF(":foreignIDThisMSGIMG%1"),
                sizeIMGF(":sizeIMG%1"),
                dataIMGF(":dataIMG%1");
        foreignIDThisMSGIMGF = foreignIDThisMSGIMGF.arg(un->pack.listIMGItem.first().num);
        sizeIMGF = sizeIMGF.arg(un->pack.listIMGItem.first().num);
        dataIMGF = dataIMGF.arg(un->pack.listIMGItem.first().num);

        query.bindValue(foreignIDThisMSGIMGF, un->pack.idC1MSG);
        query.bindValue(sizeIMGF, un->pack.listIMGItem.first().size);
        query.bindValue(dataIMGF, un->pack.listIMGItem.first().img);
        un->pack.listIMGItem.removeFirst();
    }

    if(query.exec())
    {
//        qDebug() << "DataBaseManager::addNewIMGPackFromBT lastInsertId(" << query.lastInsertId().toUInt() << ")";
        emit this->insertNewIMG(query.lastInsertId().toUInt());

        result = query.lastInsertId().toUInt();

        if(needUnlock){this->unlockResource();}

        return result;
    }
    else
    {
#ifdef QT_DEBUG
        qDebug() << "DataBaseManager::addNewIMGPackFromBT Error :" << query.lastError().text();
#endif
        result = 0;

        if(needUnlock){this->unlockResource();}

        return result;
    }
    result = 0;

    if(needUnlock){this->unlockResource();}

    return result;
}


quint32 DataBaseManager::addNewIMGPackFromKS(UnitNode* un) /*noexcept*/
{
    bool needUnlock(false);
    needUnlock = this->lockResource();

    quint32 result(0);

    for(int i(0), n(un->listIMGCollectorCell.size()); i < n; i++)
    {
        if(true == un->listIMGCollectorCell.at(i).doneStatus)
        {//найден готовый кадр будем добавлять

            //проверим есть ли такая группа кадров в базе -->
            QSqlQuery querySearchExist(m_db);
            querySearchExist.prepare("SELECT * FROM ArchiveIMG "
                                     "WHERE foreignIDMainMSG = :id_Main_MSG0xC1;");
            querySearchExist.bindValue(":id_Main_MSG0xC1", un->listIMGCollectorCell.at(i).id_Main_MSG0xC1);

            if(querySearchExist.exec())
            {
                bool nextIMGCollectorCell(false);
                while(querySearchExist.next())
                {// такая группа есть это делается через обновление полей
                    nextIMGCollectorCell = true;

                    quint32 id_This_MSG0xC1_1(querySearchExist.record().value("foreignIDThisMSGIMG1").toUInt()),
                            id_This_MSG0xC1_2(querySearchExist.record().value("foreignIDThisMSGIMG2").toUInt()),
                            id_This_MSG0xC1_3(querySearchExist.record().value("foreignIDThisMSGIMG3").toUInt()),
                            id_This_MSG0xC1_4(querySearchExist.record().value("foreignIDThisMSGIMG4").toUInt()),
                            id_This_MSG0xC1_5(querySearchExist.record().value("foreignIDThisMSGIMG5").toUInt());
                     if(id_This_MSG0xC1_1 == un->listIMGCollectorCell.at(i).id_This_MSG0xC1 ||
                        id_This_MSG0xC1_2 == un->listIMGCollectorCell.at(i).id_This_MSG0xC1 ||
                        id_This_MSG0xC1_3 == un->listIMGCollectorCell.at(i).id_This_MSG0xC1 ||
                        id_This_MSG0xC1_4 == un->listIMGCollectorCell.at(i).id_This_MSG0xC1 ||
                        id_This_MSG0xC1_5 == un->listIMGCollectorCell.at(i).id_This_MSG0xC1)
                     {
                         result = querySearchExist.record().value("id").toUInt();
                         break;
//                         continue;
//                         return un->listIMGCollectorCell.at(i).id_Main_MSG0xC1;
                     }

                     result = this->updateIMGPackFromKS(un,
                                                        querySearchExist.record().value("id").toUInt());
                     break;
                }

                if(nextIMGCollectorCell)
                {
                    continue;
                }

//                result = querySearchExist.record().value("id").toUInt();
//                continue;
            }
            else
            {
    #ifdef QT_DEBUG
                qDebug() << "DataBaseManager::addNewIMGPackFromKS Error :" << querySearchExist.lastError().text();
    #endif
                result = ((0 != result) ? result : 0);
//                result = 0;
                continue;
//                return 0;
            }
            //проверим есть ли такая группа кадров в базе <--

            // приготовим инъекцию в базу -->
            QSqlQuery queryInsert_KS_IMG(m_db);
            queryInsert_KS_IMG.prepare("INSERT INTO ArchiveIMG "
                                       "(foreignIDMainMSG, "
                                        "foreignIDThisMSGIMG1, "
                                        "sizeIMG1, "
                                        "dataIMG1, "
                                        "foreignIDThisMSGIMG2, "
                                        "sizeIMG2, "
                                        "dataIMG2, "
                                        "foreignIDThisMSGIMG3, "
                                        "sizeIMG3, "
                                        "dataIMG3, "
                                        "foreignIDThisMSGIMG4, "
                                        "sizeIMG4, "
                                        "dataIMG4, "
                                        "foreignIDThisMSGIMG5, "
                                        "sizeIMG5, "
                                        "dataIMG5, "
                                        "checkedUp, "
                                        "foreignIDAlarm, "
                                        "numberVK, "
                                        "hidden) "
                                        "VALUES (:foreignIDMainMSG, "
                                               ":foreignIDThisMSGIMG1, "
                                               ":sizeIMG1, "
                                               ":dataIMG1, "
                                               ":foreignIDThisMSGIMG2, "
                                               ":sizeIMG2, "
                                               ":dataIMG2, "
                                               ":foreignIDThisMSGIMG3, "
                                               ":sizeIMG3, "
                                               ":dataIMG3, "
                                               ":foreignIDThisMSGIMG4, "
                                               ":sizeIMG4, "
                                               ":dataIMG4, "
                                               ":foreignIDThisMSGIMG5, "
                                               ":sizeIMG5, "
                                               ":dataIMG5, "
                                               ":checkedUp, "
                                               ":foreignIDAlarm, "
                                               ":numberVK, "
                                               ":hidden);");
            queryInsert_KS_IMG.bindValue(":foreignIDMainMSG", un->listIMGCollectorCell.at(i).id_Main_MSG0xC1);
            queryInsert_KS_IMG.bindValue(":foreignIDThisMSGIMG1", 0);
            queryInsert_KS_IMG.bindValue(":sizeIMG1", 0);
            queryInsert_KS_IMG.bindValue(":dataIMG1", QByteArray());
            queryInsert_KS_IMG.bindValue(":foreignIDThisMSGIMG2", 0);
            queryInsert_KS_IMG.bindValue(":sizeIMG2", 0);
            queryInsert_KS_IMG.bindValue(":dataIMG2", QByteArray());
            queryInsert_KS_IMG.bindValue(":foreignIDThisMSGIMG3", 0);
            queryInsert_KS_IMG.bindValue(":sizeIMG3", 0);
            queryInsert_KS_IMG.bindValue(":dataIMG3", QByteArray());
            queryInsert_KS_IMG.bindValue(":foreignIDThisMSGIMG4", 0);
            queryInsert_KS_IMG.bindValue(":sizeIMG4", 0);
            queryInsert_KS_IMG.bindValue(":dataIMG4", QByteArray());
            queryInsert_KS_IMG.bindValue(":foreignIDThisMSGIMG5", 0);
            queryInsert_KS_IMG.bindValue(":sizeIMG5", 0);
            queryInsert_KS_IMG.bindValue(":dataIMG5", QByteArray());
            queryInsert_KS_IMG.bindValue(":checkedUp", (false ? 1 : 0));
            queryInsert_KS_IMG.bindValue(":foreignIDAlarm", 0);
            queryInsert_KS_IMG.bindValue(":numberVK", un->listIMGCollectorCell.at(i).numVK);
            queryInsert_KS_IMG.bindValue(":hidden", 0);
            // приготовим инъекцию в базу <--

            // заполним поля инъекции тем что есть -->
            for(int j(0), m(un->listIMGCollectorCell.size()); j < m; j++)
            {
                if(un->listIMGCollectorCell.at(j).id_Main_MSG0xC1 == un->listIMGCollectorCell.at(i).id_Main_MSG0xC1 &&
                   true == un->listIMGCollectorCell.at(j).doneStatus)
                {
                    QString foreignIDThisMSGIMGF(":foreignIDThisMSGIMG%1"),
                            sizeIMGF(":sizeIMG%1"),
                            dataIMGF(":dataIMG%1");
                    foreignIDThisMSGIMGF = foreignIDThisMSGIMGF.arg(un->listIMGCollectorCell.at(j).numShot);
                    sizeIMGF = sizeIMGF.arg(un->listIMGCollectorCell.at(j).numShot);
                    dataIMGF = dataIMGF.arg(un->listIMGCollectorCell.at(j).numShot);

                    queryInsert_KS_IMG.bindValue(foreignIDThisMSGIMGF, un->listIMGCollectorCell.at(j).id_This_MSG0xC1);
                    queryInsert_KS_IMG.bindValue(sizeIMGF, un->listIMGCollectorCell.at(j).releaseSize);
                    queryInsert_KS_IMG.bindValue(dataIMGF, un->listIMGCollectorCell.at(j).releaseIMG);
                }
            }
            // заполним поля инъекции тем что есть <--

            if(queryInsert_KS_IMG.exec())
            {
                emit this->insertNewIMG(queryInsert_KS_IMG.lastInsertId().toUInt());
                result = queryInsert_KS_IMG.lastInsertId().toUInt();
                continue;
//                return queryInsert_KS_IMG.lastInsertId().toUInt();
            }
            else
            {
    #ifdef QT_DEBUG
                qDebug() << "DataBaseManager::addNewIMGPackFromKS Error :" << queryInsert_KS_IMG.lastError().text();
    #endif
                result = ((0 != result) ? result : 0);
//                result = 0;
                continue;
//                return 0;
            }
        }
    }

    if(needUnlock){this->unlockResource();}

    return result;
}

quint32 DataBaseManager::updateIMGPackFromKS(UnitNode* un,
                                             quint32 index) /*noexcept*/
{
    quint32 result(0);
    QSqlQuery querySearchExist(m_db);
    querySearchExist.prepare("SELECT * FROM ArchiveIMG WHERE id = :id;");
    querySearchExist.bindValue(":id", index);

    if(querySearchExist.exec())
    {
        while(querySearchExist.next())
        {
            //запись для обновления существует

            quint32 id_Main_MSG0xC1(querySearchExist.record().value("foreignIDMainMSG").toUInt()),
                    id_This_MSG0xC1_1(querySearchExist.record().value("foreignIDThisMSGIMG1").toUInt()),
                    id_This_MSG0xC1_2(querySearchExist.record().value("foreignIDThisMSGIMG2").toUInt()),
                    id_This_MSG0xC1_3(querySearchExist.record().value("foreignIDThisMSGIMG3").toUInt()),
                    id_This_MSG0xC1_4(querySearchExist.record().value("foreignIDThisMSGIMG4").toUInt()),
                    id_This_MSG0xC1_5(querySearchExist.record().value("foreignIDThisMSGIMG5").toUInt());

            for(int i(0), n(un->listIMGCollectorCell.size()); i < n; i++)
            {
                if(id_Main_MSG0xC1 == un->listIMGCollectorCell.at(i).id_Main_MSG0xC1 &&
                   id_This_MSG0xC1_1 != un->listIMGCollectorCell.at(i).id_This_MSG0xC1 &&
                   id_This_MSG0xC1_2 != un->listIMGCollectorCell.at(i).id_This_MSG0xC1 &&
                   id_This_MSG0xC1_3 != un->listIMGCollectorCell.at(i).id_This_MSG0xC1 &&
                   id_This_MSG0xC1_4 != un->listIMGCollectorCell.at(i).id_This_MSG0xC1 &&
                   id_This_MSG0xC1_5 != un->listIMGCollectorCell.at(i).id_This_MSG0xC1 &&
                   true == un->listIMGCollectorCell.at(i).doneStatus)
                {
                    // приготовим инъекцию в базу -->
                    QSqlQuery queryUpdate_KS_IMG(m_db);
                    QString foreignIDThisMSGIMGF("foreignIDThisMSGIMG%1"),
                            strSizeIMGF("sizeIMG%1"),
                            strDataIMGF("dataIMG%1"),
                            strQuery("UPDATE ArchiveIMG SET "
                                     "%1 = :foreignIDThisMSGIMG, "
                                     "%2 = :sizeIMG, "
                                     "%3 = :dataIMG, "
                                     "checkedUp = :checkedUp "
                                     "WHERE id = :id;");

                    strSizeIMGF = strSizeIMGF.arg(un->listIMGCollectorCell.at(i).numShot);
                    strDataIMGF = strDataIMGF.arg(un->listIMGCollectorCell.at(i).numShot);
                    foreignIDThisMSGIMGF = foreignIDThisMSGIMGF.arg(un->listIMGCollectorCell.at(i).numShot);
                    strQuery = strQuery.arg(foreignIDThisMSGIMGF)
                                       .arg(strSizeIMGF)
                                       .arg(strDataIMGF);

                    queryUpdate_KS_IMG.prepare(strQuery);
                    queryUpdate_KS_IMG.bindValue(":foreignIDThisMSGIMG", un->listIMGCollectorCell.at(i).id_This_MSG0xC1);
                    queryUpdate_KS_IMG.bindValue(":sizeIMG", un->listIMGCollectorCell.at(i).releaseIMG.size());
                    queryUpdate_KS_IMG.bindValue(":dataIMG", un->listIMGCollectorCell.at(i).releaseIMG);
                    queryUpdate_KS_IMG.bindValue(":checkedUp", (false ? 1 : 0));
                    queryUpdate_KS_IMG.bindValue(":id", index);
                    // приготовим инъекцию в базу <--

                    if(queryUpdate_KS_IMG.exec())
                    {
                        result = index;
                        emit this->insertNewIMG(result);
//                        continue;
                    }
                    else
                    {
#ifdef QT_DEBUG
                        qDebug() << "DataBaseManager::updateIMGPackFromKS Error :" << queryUpdate_KS_IMG.lastError().text();
#endif
                        result = ((0 != result) ? result : 0);
//                        result = 0;
//                        continue;
                    }
                }
            }
            break;
        }
    }
    else
    {
#ifdef QT_DEBUG
        qDebug() << "DataBaseManager::updateIMGPackFromKS Error :" << querySearchExist.lastError().text();
#endif
        result = ((0 != result) ? result : 0);
//        result = 0;
    }
    return result;
}

quint32 DataBaseManager::addNewMSG(UnitNode* un,
                                   quint8 type,
                                   QByteArray msg,
                                   bool ioType,
                                   TypeUnitNodeSystem typeSystem,
                                   TypeGroupMSG typeGroup,
                                   bool flagHidden) /*noexcept*/
{
    switch((quint8)typeSystem)
    {
        case (quint8)KMSystemType:
        {
            if(!ioType)
            {
                lastInputMSG_KM.clear();
                break;
            }
            if(lastInputMSG_KM == msg)
            {
//                lastInputMSG_KM = msg;
                return 0;
            }
            lastInputMSG_KM = msg;
            break;
        }
        case (quint8)KSSystemType:
        {
            if(!ioType)
            {
                lastInputMSG_KS.clear();
                break;
            }
            if(lastInputMSG_KS == msg)
            {
//                lastInputMSG_KS = msg;
                return 0;
            }
            lastInputMSG_KS = msg;
            break;
        }
        case (quint8)BSKSystemType:
        {
            if(!ioType)
            {
                lastInputMSG_BSK.clear();
                break;
            }
            if(lastInputMSG_BSK == msg)
            {
//                lastInputMSG_BSK = msg;
                return 0;
            }
            lastInputMSG_BSK = msg;
            break;
        }
    }
    QSqlQuery query(m_db);
    TypeUnitNodeDevice typeUN(UnknownDeviceType);
    TypeUnitNodeSystem typeSystemUN(typeSystem);
    quint8 typeMSG(type),
           codeMSG(0xFF),
           foreignIDUN(0);
    QString tCorection("00:00:00.000");
    if((quint8)0xA5 == (quint8)msg.at(0))
    {
        if(KMSystemType == typeSystem)
        {
            codeMSG = (quint8)msg.at(8);
        }
        if(KSSystemType == typeSystem)
        {
            codeMSG = (quint8)msg.at(5);
            if((quint8)0x9F == codeMSG)
            {
                int s(((quint8)msg.at(18) * 2 * 60) + ((quint8)msg.at(19) / 2));
                int h(s / 3600);
                s -= h * 3600;
                int m(s / 60);
                s -= m * 60;
                tCorection = QTime(h, m, s).toString("hh:mm:ss.zzz");
            }
        }
    }
    else
    {
        codeMSG = 0xFF;
    }

    if(KMSystemType == typeSystem &&
       (quint8)0xC3 == codeMSG)
    {
        return 0;
    }

    QByteArray idUN;
    quint8 numNode(0), numArea(0);
    if(un)
    {
        numNode = un->_numNode();
        numArea = un->_numArea();

        typeUN = un->_typeUN();
        foreignIDUN = this->getUNID(un->id,
                                    un->_typeUN(),
                                    un->_typeSystem());
        idUN = un->id;


        if(KMSystemType == typeSystem &&
           ((quint8)0xC4) == codeMSG &&
           un->pack.readyStatus)
        {
//            qDebug() << "!!! Catch IMG !!!";
            this->addNewIMGPackFromBT(un);
            return 0;
        }
    }

    QString d(QDateTime::currentDateTime().toString("yyyy-MM-dd")),
            t(QDateTime::currentDateTime().toString("hh:mm:ss.zzz"));

    query.prepare("INSERT INTO ArchiveMSG "
                  "(ioType, "
                  "typeUN, "
                  "typeSystemMSG, "
                  "idUN, "
                  "numberNodeUN, "
                  "numberAreaUN, "
                  "foreignIDUN, "
                  "msg, "
                  "typeMSG, "
                  "codeMSG, "
                  "groupMSG, "
                  "date, "
                  "time, "
                  "timeCorection, "
                  "commentRUS, "
                  "commentENG, "
                  "color, "
                  "hidden) "
                  "VALUES (:ioType, "
                          ":typeUN, "
                          ":typeSystemMSG, "
                          ":idUN, "
                          ":numberNodeUN, "
                          ":numberAreaUN, "
                          ":foreignIDUN, "
                          ":msg, "
                          ":typeMSG, "
                          ":codeMSG, "
                          ":groupMSG, "
                          ":date, "
                          ":time, "
                          ":timeCorection, "
                          ":commentRUS, "
                          ":commentENG, "
                          ":color, "
                          ":hidden);");
    query.bindValue(":ioType", (ioType ? 1 : 0));
    query.bindValue(":typeUN", (quint8)typeUN);
    query.bindValue(":typeSystemMSG", (quint8)typeSystemUN);
    query.bindValue(":idUN", idUN);
    query.bindValue(":numberNodeUN", numNode);
    query.bindValue(":numberAreaUN", numArea);
    query.bindValue(":foreignIDUN", foreignIDUN);
    query.bindValue(":msg", msg);
    query.bindValue(":typeMSG", typeMSG);
    query.bindValue(":codeMSG", codeMSG);
    query.bindValue(":groupMSG", typeGroup);
    query.bindValue(":date", d);
    query.bindValue(":time", t);
    query.bindValue(":timeCorection", tCorection);
    query.bindValue(":commentRUS", ProxyMetod().msgCommentRUS(msg, typeSystem));
    query.bindValue(":commentENG", ProxyMetod().msgCommentENG(msg, typeSystem));
    query.bindValue(":color", ProxyMetod().msgColorToByteArray(ioType, msg, typeSystem));
    query.bindValue(":hidden", (flagHidden ? 1 : 0));

    quint32 result(0);

    bool needUnlock(false);
    needUnlock = this->lockResource();

    if(query.exec())
    {
        if(!flagHidden)
        {
            emit this->insertNewMSG((quint32)query.lastInsertId().toUInt());
        }

        result = (quint32)query.lastInsertId().toUInt();

        if(needUnlock){this->unlockResource();}

        return result;
//        qint32 idNewMSG(query.lastInsertId().toUInt());
//        return (quint32)idNewMSG;
    }

#ifdef QT_DEBUG
    qDebug() << "DataBaseManager::addNewMSG Error :" << query.lastError().text();
#endif
    if(needUnlock){this->unlockResource();}

    return 0;
}

quint32 DataBaseManager::getUNID(QByteArray idUnitNode,
                                 TypeUnitNodeSystem typeSystem) /*noexcept*/
{
    QSqlQuery query(m_db);

    query.prepare("SELECT * FROM [ArchiveUnitNode] "
                  "WHERE [ArchiveUnitNode].[idUN] = :idUN "
                  "AND [ArchiveUnitNode].[typeSystemUN] = :typeSystemUN;");
    query.bindValue(":idUN", idUnitNode);
    query.bindValue(":typeSystemUN", (quint8)typeSystem);

    if(query.exec())
    {
        while(query.next())
        {
            return query.value("id").toUInt();
        }
#ifdef QT_DEBUG
        qDebug() << "DataBaseManager::getUNID Empety result";
        qDebug() << ":idUnitNoded " << idUnitNode.toHex();
        qDebug() << ":typeSystemUN" << typeSystem;
#endif
    }
#ifdef QT_DEBUG
    qDebug() << "DataBaseManager::getUNID Error :" << query.lastError().text();
#endif
    return 0;

}

quint32 DataBaseManager::getUNID(QByteArray idUnitNode,
                                 TypeUnitNodeDevice typeUnitNode,
                                 TypeUnitNodeSystem typeSystem,
                                 QByteArray macUnitNode) /*noexcept*/
{//закладка
    QSqlQuery /*query(m_db),*/query2(m_db);

    query2.prepare("SELECT * FROM [ArchiveUnitNode] "
                  "WHERE [ArchiveUnitNode].[idUN] = :idUN "
                  "AND [ArchiveUnitNode].[typeSystemUN] = :typeSystemUN "
                  "AND [ArchiveUnitNode].[typeUN] = :typeUN ;");
    query2.bindValue(":idUN", idUnitNode);
    query2.bindValue(":typeSystemUN", (quint8)typeSystem);
    query2.bindValue(":typeUN", (quint8)typeUnitNode);

    if(query2.exec())
    {
        while(query2.next())
        {
            return query2.value("id").toUInt();
        }
#ifdef QT_DEBUG
        qDebug() << "DataBaseManager::getUNID Empety result";
        qDebug() << ":idUnitNoded " << idUnitNode.toHex();
        qDebug() << ":macUnitNode " << macUnitNode;
        qDebug() << ":typeUnitNode" << typeUnitNode;
        qDebug() << ":typeSystemUN" << typeSystem;
#endif
    }
#ifdef QT_DEBUG
    qDebug() << "DataBaseManager::getUNID Error :" << query2.lastError().text();
#endif
    return 0;
}

QSqlQuery DataBaseManager::getAllUN() /*noexcept*/
{
    QSqlQuery query(m_db);
    query.prepare("SELECT * FROM [ArchiveUnitNode];");

    if(query.exec())
    {
        return query;
    }
    else
    {
#ifdef QT_DEBUG
        qDebug() << "DataBaseManager::getAllUN Error :" << query.lastError().text();
#endif
        return QSqlQuery();
    }
}

QList<quint32> DataBaseManager::getAllUNRecords() /*noexcept*/
{
    QList<quint32> list;
    QSqlQuery query(this->getAllUN());
    while (query.next())
    {
        list.append(query.record().value("id").toUInt());
    }
    return list;
}


QSqlQuery DataBaseManager::getUN(quint32 id) /*noexcept*/
{
    QSqlQuery query(m_db);
    query.prepare("SELECT * FROM [ArchiveUnitNode] "
                  "WHERE [ArchiveUnitNode].[id] = :id;");
    query.bindValue(":id", id);

    if(query.exec())
    {
        return query;
    }
    else
    {
#ifdef QT_DEBUG
        qDebug() << "DataBaseManager::getUN Error :" << query.lastError().text();
#endif
        return QSqlQuery();
    }
}

QSqlRecord DataBaseManager::getUNRecord(quint32 id) /*noexcept*/
{
    QSqlRecord result;
    QSqlQuery query(this->getUN(id));
    while(query.next())
    {
        result = query.record();
    }
    return result;
}

QSqlQuery DataBaseManager::getFilter_MSG(FilterStruct filter,
                                         TypeUnitNodeSystem typeSystem) /*noexcept*/
{
    QSqlQuery query(m_db);
    QString str("SELECT * FROM [ArchiveMSG] ");

    str.append("WHERE ([ArchiveMSG].[typeSystemMSG] = :typeSystem "
               "OR     [ArchiveMSG].[typeSystemMSG] = :system) ");

    if(filter.filterDate)
    {
        str.append("AND (    :minDate <= [ArchiveMSG].[date] "
                    "     AND [ArchiveMSG].[date] <= :maxDate) ");
    }

    if(filter.filterTime)
    {
        str.append("AND (    :minTime <= [ArchiveMSG].[time] "
                    "     AND [ArchiveMSG].[time] <= :maxTime) ");
    }

    if(filter.filterSN)
    {
        if(-1 != filter.filterSector)
        {
            str.append("AND ([ArchiveMSG].[area] = :area) ");
        }

        if(-1 != filter.filterNumber)
        {
            str.append("AND ([ArchiveMSG].[number] = :number) ");
        }
    }

    if(filter.filterType && -1 != filter.filterTypeValue)
    {
        if(KSSystemType == typeSystem &&
           CPP_KSDeviceType != filter.filterTypeValue)
        {
            if(RT01_KSDeviceType != filter.filterTypeValue &&
               RT02_KSDeviceType != filter.filterTypeValue)
            {
                str.append("AND (    [ArchiveMSG].[typeUN] = :typeUN_09 "
                            "     OR [ArchiveMSG].[typeUN] = :typeUN_0A "
                            "     OR [ArchiveMSG].[typeUN] = :typeUN_0B) ");
            }
            else
            {
                str.append("AND (    [ArchiveMSG].[typeUN] = :typeUN_01 "
                            "     OR [ArchiveMSG].[typeUN] = :typeUN_02) ");
            }
        }
        else
        {
            str.append("AND ([ArchiveMSG].[typeUN] = :typeUN) ");
        }
    }

    quint32 foreignIDUN(0);
    if(filter.filterID && !filter.filterIDValue.isEmpty())
    {
        foreignIDUN = this->getUNID(filter.filterIDValue, typeSystem);
        str.append("AND ([ArchiveMSG].[foreignIDUN] = :foreignIDUN) ");
    }

    if(filter.filterMSGGroup)
    {
        str.append("AND ( ");
        str.append("([ArchiveMSG].[groupMSG] <> :groupOther) ");

        if(false == filter.filterMSGAlarm)
        {
            str.append("AND ([ArchiveMSG].[groupMSG] <> :groupAlarm) ");
        }
        if(false == filter.filterMSGConnect)
        {
            str.append("AND ([ArchiveMSG].[groupMSG] <> :groupConnect) ");
        }
        if(false == filter.filterMSGError)
        {
            str.append("AND ([ArchiveMSG].[groupMSG] <> :groupError) ");
        }
        if(false == filter.filterMSGDischarge)
        {
            str.append("AND ([ArchiveMSG].[groupMSG] <> :groupDischarge) ");
        }
        if(false == filter.filterMSGFF)
        {
            str.append("AND ([ArchiveMSG].[groupMSG] <> :groupFF) ");
        }
        if(false == filter.filterMSGCommand)
        {
            str.append("AND ([ArchiveMSG].[groupMSG] <> :groupCommand) ");
        }
        if(false == filter.filterMSGSystem)
        {
            str.append("AND ([ArchiveMSG].[groupMSG] <> :groupSystem) ");
        }
        str.append(" ) ");
    }


    str.append(";");
    query.prepare(str);

    query.bindValue(":typeSystem", (quint8)typeSystem);
    query.bindValue(":system", (quint8)UnknownSystemType);

    if(filter.filterDate)
    {
        query.bindValue(":minDate", filter.filterMinDate);
        query.bindValue(":maxDate", filter.filterMaxDate);
    }

    if(filter.filterTime)
    {
        query.bindValue(":minTime", filter.filterMinTime);
        query.bindValue(":maxTime", filter.filterMaxTime);
    }

    if(filter.filterSN)
    {
        if(-1 != filter.filterSector)
        {
            query.bindValue(":area", filter.filterSector);
        }

        if(-1 != filter.filterNumber)
        {
            query.bindValue(":number", filter.filterNumber);
        }
    }

    if(filter.filterType && -1 != filter.filterTypeValue)
    {
        if(KSSystemType == typeSystem &&
           CPP_KSDeviceType != filter.filterTypeValue)
        {
            if(RT01_KSDeviceType != filter.filterTypeValue &&
               RT02_KSDeviceType != filter.filterTypeValue)
            {
                query.bindValue(":typeUN_09", (quint8)VK09_KSDeviceType);
                query.bindValue(":typeUN_0A", (quint8)VK0A_KSDeviceType);
                query.bindValue(":typeUN_0B", (quint8)VK0B_KSDeviceType);
            }
            else
            {
                query.bindValue(":typeUN_01", (quint8)RT01_KSDeviceType);
                query.bindValue(":typeUN_02", (quint8)RT02_KSDeviceType);
            }
        }
        else
        {
            query.bindValue(":typeUN", (quint8)filter.filterTypeValue);
        }
    }

    if(filter.filterID && !filter.filterIDValue.isEmpty())
    {
        query.bindValue(":foreignIDUN", foreignIDUN);
    }

    if(filter.filterMSGGroup)
    {
        query.bindValue(":groupOther", (quint8)Other_TypeGroupMSG);

        if(false == filter.filterMSGAlarm)
        {
            query.bindValue(":groupAlarm", (quint8)Alarm_TypeGroupMSG);
        }
        if(false == filter.filterMSGConnect)
        {
            query.bindValue(":groupConnect", (quint8)Connect_TypeGroupMSG);
        }
        if(false == filter.filterMSGError)
        {
            query.bindValue(":groupError", (quint8)Error_TypeGroupMSG);
        }
        if(false == filter.filterMSGDischarge)
        {
            query.bindValue(":groupDischarge", (quint8)Discharge_TypeGroupMSG);
        }
        if(false == filter.filterMSGFF)
        {
            query.bindValue(":groupFF", (quint8)FF_TypeGroupMSG);
        }
        if(false == filter.filterMSGCommand)
        {
            query.bindValue(":groupCommand", (quint8)Command_TypeGroupMSG);
        }
        if(false == filter.filterMSGSystem)
        {
            query.bindValue(":groupSystem", (quint8)System_TypeGroupMSG);
        }
    }

    if(query.exec())
    {
        return query;
    }

#ifdef QT_DEBUG
    qDebug() << "DataBaseManager::getFilter_MSG Error :" << query.lastError().text();
#endif
    return QSqlQuery();
}

QList<quint32> DataBaseManager::getFilter_MSGRecords(FilterStruct filter,
                                                     TypeUnitNodeSystem typeSystem) /*noexcept*/
{
    QList<quint32> list;
    QSqlQuery query(this->getFilter_MSG(filter, typeSystem));
    while (query.next())
    {

//#ifdef QT_DEBUG
//#else
//        if(0 != query.record().value("hidden").toInt())//сделать только на релиз
//        {
//            continue;
//        }
//        if(KMSystemType == typeSystem)
//        {
//            if(UnknownDeviceType == (TypeUnitNodeDevice)query.record().value("typeUN").toUInt())
//            {
//                if((quint8)0xFF == (quint8)query.record().value("typeMSG").toUInt() &&
//                   (quint8)0xFF == (quint8)query.record().value("codeMSG").toUInt())
//                {
//                    if(query.record().value("commentRUS").toString().contains("SET"))
//                    {
//                        continue;
//                    }
//                }
//            }
//            else
//            {
//                if((quint8)0xFF == (quint8)query.record().value("codeMSG").toUInt() &&
//                   (
//                      (quint8)0xFF == (quint8)query.record().value("typeMSG").toUInt() ||
//                      (quint8)0x00 == (quint8)query.record().value("typeMSG").toUInt()
//                   )
//                  )
//                {
//                    continue;
//                }
//            }
////            if(UnknownDeviceType != (TypeUnitNodeDevice)query.record().value("typeUN").toUInt() &&
////               (
////                  (quint8)0xFF == (quint8)query.record().value("typeMSG").toUInt() ||
////                  (quint8)0x00 == (quint8)query.record().value("typeMSG").toUInt()
////               ) &&
////               (quint8)0xFF == (quint8)query.record().value("codeMSG").toUInt())
////            {
////                continue;
////            }
////            if(UnknownDeviceType == (TypeUnitNodeDevice)query.record().value("typeUN").toUInt() &&
////               (quint8)0xFF == (quint8)query.record().value("typeMSG").toUInt() &&
////               (quint8)0xFF == (quint8)query.record().value("codeMSG").toUInt() &&
////               query.record().value("commentRUS").toString().contains("SET"))
////            {
////                continue;
////            }
//        }

        if(KMSystemType == typeSystem)
        {
            if(UnknownDeviceType != (TypeUnitNodeDevice)query.record().value("typeUN").toUInt() &&
               (
                  (quint8)0xFF == (quint8)query.record().value("typeMSG").toUInt() ||
                  (quint8)0x00 == (quint8)query.record().value("typeMSG").toUInt()
               ) &&
               (quint8)0xFF == (quint8)query.record().value("codeMSG").toUInt())
            {
                continue;
            }
            if(UnknownDeviceType == (TypeUnitNodeDevice)query.record().value("typeUN").toUInt() &&
               (quint8)0xFF == (quint8)query.record().value("typeMSG").toUInt() &&
               (quint8)0xFF == (quint8)query.record().value("codeMSG").toUInt() &&
               query.record().value("commentRUS").toString().contains("SET"))
            {
                continue;
            }

        }
//#endif
        list.append(query.record().value("id").toUInt());
    }
    return list;
}

QSqlRecord DataBaseManager::get_MSGRecord(quint32 idMSG) /*noexcept*/
{
    QString str("SELECT * FROM [ArchiveMSG] WHERE [ArchiveMSG].[id] = %1;");
    str = str.arg(idMSG);
    QSqlQuery query(str,
                    m_db);

    QSqlRecord result;

    if(query.exec())
    {
        while(query.next())
        {
            result = query.record();
            break;
        }
    }
    else
    {
#ifdef QT_DEBUG
        qDebug() << "DataBaseManager::get_MSGRecord(" << idMSG << ") Error :" << query.lastError().text();
#endif
    }
    return result;
}

quint32 DataBaseManager::addNewUN(UnitNode* un) /*noexcept*/
{
    if(!un)
        return 0;

    if(un->id.isEmpty() ||
       un->name.isEmpty() ||
       UnknownDeviceType == un->_typeUN())
        return 0;

//    while(this->lockerWriteUpdate.tryLock(200));
    quint32 idExistUN(this->getUNID(un->id,
                                    un->_typeUN(),
                                    un->_typeSystem()));

//    bool needUnlock(false);
//    needUnlock = this->lockResource();

    quint32 result(0);

    if(0 == idExistUN)
    {
        QSqlQuery query(m_db);
        query.prepare("INSERT INTO ArchiveUnitNode "
                     "(idUN, "
                      "macUN, "
                      "nameUN, "
                      "abstractNameUN, "
                      "typeUN, "
                      "typeSystemUN, "
                      "link, "
                      "linkValid, " //
                      "xPos, "
                      "yPos, "
                      "voltage, "
                      "area, "
                      "number, "
                      "discharge, "
                      "idParentUN, "
                      "hidden, "
                      "Zone, "
                      "modeVK1, "
                      "modeVK2) "
                      "VALUES (:idUN, "
                              ":macUN, "
                              ":nameUN, "
                              ":abstractNameUN, "
                              ":typeUN, "
                              ":typeSystemUN, "
                              ":link, "
                              ":linkV, "
                              ":xp, "
                              ":yp, "
                              ":voltageV, "
                              ":areaV, "
                              ":numberV, "
                              ":dischargeV, "
                              ":idParent, "
                              ":hidden, "
                              ":Zone, "
                              ":modeVK1, "
                              ":modeVK2);");
        query.bindValue(":idUN", un->_id());
        query.bindValue(":macUN", un->_mac());
        query.bindValue(":nameUN", un->_name());
        query.bindValue(":abstractNameUN", "");
        query.bindValue(":typeUN", (quint8)un->_typeUN());
        query.bindValue(":typeSystemUN", (quint8)un->_typeSystem());
        query.bindValue(":link", un->btLink);
        query.bindValue(":linkV", (un->flag_connection ? 1 : 0));
        query.bindValue(":xp", un->graphicsPxmItem->x());
        query.bindValue(":yp", un->graphicsPxmItem->y());
        query.bindValue(":voltageV", un->voltage);
        query.bindValue(":areaV", un->numArea);
        query.bindValue(":numberV", un->numNode);
        query.bindValue(":dischargeV", (un->flag_discharge ? 1 : 0));
        query.bindValue(":idParent", un->dbIndexParent);
        query.bindValue(":hidden", (un->flag_hidden ? 1 : 0));
        query.bindValue(":Zone", 0);
        query.bindValue(":modeVK1", 2);
        query.bindValue(":modeVK2", 2);
//        //!!! Вылет из КМ possAdd !!!//
//        if(!un->_id().isEmpty())
//        {
//            un->graphicsTxtItem->setHtml(un->getGraphicsTextLabel());
//        }

        if(query.exec())
        {
            result = query.lastInsertId().toUInt();

//            if(needUnlock){this->unlockResource();}

            return result;
        }
        else
        {
#ifdef QT_DEBUG
            qDebug() << "DataBaseManager::addNewUN Error :" << query.lastError().text();
            qDebug() << query.lastQuery();
            qDebug() << query.boundValues();
#endif
//            if(needUnlock){this->unlockResource();}

            return 0;
        }
    }
    else
    {
        QSqlQuery query(m_db);
        query.prepare("UPDATE ArchiveUnitNode SET "
                      "idUN = :idUN, "
                      "macUN = :macUN , "
                      "nameUN = :nameUN, "
                      "abstractNameUN = :abstractNameUN, "
                      "typeUN = :typeUN, "
                      "typeSystemUN = :typeSystemUN, "
                      "link = :link, "
                      "linkValid = :linkV, "
                      "xPos = :xp , "
                      "yPos = :yp, "
                      "voltage = :voltageV, "
                      "area = :areaV, "
                      "number = :numberV, "
                      "discharge = :dischargeV, "
                      "idParentUN = :idParent, "
                      "hidden = :hidden "
                      "WHERE id = :id;");
        query.bindValue(":idUN", un->_id());
        query.bindValue(":macUN", un->_mac());
        query.bindValue(":nameUN", un->_name());
        query.bindValue(":abstractNameUN", un->_abstractName());
        query.bindValue(":typeUN", (quint8)un->_typeUN());
        query.bindValue(":typeSystemUN", (quint8)un->_typeSystem());
        query.bindValue(":link", un->btLink);
        query.bindValue(":linkV", (un->flag_connection ? 1 : 0));
        query.bindValue(":xp", un->graphicsPxmItem->x());
        query.bindValue(":yp", un->graphicsPxmItem->y());
        query.bindValue(":voltageV", un->voltage);
        query.bindValue(":areaV", un->_numArea());
        query.bindValue(":numberV", un->_numNode());
        query.bindValue(":dischargeV", (un->flag_discharge ? 1 : 0));
        query.bindValue(":idParent", un->dbIndexParent);
        query.bindValue(":hidden", (un->flag_hidden ? 1 : 0));
        query.bindValue(":id", idExistUN);

//        un->graphicsTxtItem->setX(un->graphicsPxmItem->x());
//        un->graphicsTxtItem->setY(un->graphicsPxmItem->y() + 20);
//        if(!un->_id().isEmpty())
//        {
//            un->graphicsTxtItem->setHtml(un->getGraphicsTextLabel());
//        }


        if(query.exec())
        {
//            if(needUnlock){this->unlockResource();}

            return idExistUN;
        }
        else
        {
#ifdef QT_DEBUG
            qDebug() << "DataBaseManager::addNewUN Error :" << query.lastError().text();
            qDebug() << query.lastQuery();
            qDebug() << query.boundValues();
#endif
//            if(needUnlock){this->unlockResource();}

            return 0;
        }
    }

//    if(needUnlock){this->unlockResource();}

    return 0;
}

quint32 DataBaseManager::updateUN(UnitNode* un) /*noexcept*/
{
    if(!un)
        return 0;

    if(KMSystemType == un->_typeSystem())
    {
        if(un->id.isEmpty() ||
           un->mac.isEmpty() ||
           un->name.isEmpty() ||
           UnknownDeviceType == un->_typeUN())
            return 0;
    }
    else
    {
        if(un->id.isEmpty() ||
           un->name.isEmpty() ||
           UnknownDeviceType == un->_typeUN())
            return 0;
    }

    quint32 idExistUN(this->getUNID(un->id,
                                    un->_typeUN(),
                                    un->_typeSystem()));
    if(0 == idExistUN)
    {
        return this->addNewUN(un);
    }
    else
    {
        QSqlQuery query(m_db);
        query.prepare("UPDATE ArchiveUnitNode SET "
                      "idUN = :idUN, "
                      "macUN = :macUN , "
                      "nameUN = :nameUN, "
                      "abstractNameUN = :abstractNameUN, "
                      "typeUN = :typeUN, "
                      "typeSystemUN = :typeSystemUN, "
                      "link = :link, "
                      "linkValid = :linkV, "
                      "xPos = :xp , "
                      "yPos = :yp, "
                      "voltage = :voltageV, "
                      "area = :areaV, "
                      "number = :numberV, "
                      "discharge = :dischargeV, "
                      "idParentUN = :idParent, "
                      "hidden = :hidden "
                      "WHERE id = :id;");
        query.bindValue(":idUN", un->_id());
        query.bindValue(":macUN", un->_mac());
        query.bindValue(":nameUN", un->_name());
        query.bindValue(":abstractNameUN", un->_abstractName());
        query.bindValue(":typeUN", (quint8)un->_typeUN());
        query.bindValue(":typeSystemUN", (quint8)un->_typeSystem());
        query.bindValue(":link", un->btLink);
        query.bindValue(":linkV", (un->flag_connection ? 1 : 0));
        query.bindValue(":xp", un->graphicsPxmItem->x());
        query.bindValue(":yp", un->graphicsPxmItem->y());
        query.bindValue(":voltageV", un->voltage);
        query.bindValue(":areaV", un->_numArea());
        query.bindValue(":numberV", un->_numNode());
        query.bindValue(":dischargeV", (un->flag_discharge ? 1 : 0));
        query.bindValue(":idParent", un->dbIndexParent);
        query.bindValue(":hidden", (un->flag_hidden ? 1 : 0));
        query.bindValue(":id", idExistUN);
//        un->graphicsTxtItem->setX(un->graphicsPxmItem->x());
//        un->graphicsTxtItem->setY(un->graphicsPxmItem->y() + 20);
//        //!!! Вылет из КМ possAdd !!!//
//        if(!un->_id().isEmpty())
//        {
//            un->graphicsTxtItem->setHtml(un->getGraphicsTextLabel());
//        }

        if(query.exec())
        {
            //
            QSqlQuery queryUpdateSettings(m_db);
            queryUpdateSettings.prepare("UPDATE ArchiveUnitNode SET "

                                        "modeVK1 = :modeVK1, "
                                        "ConnectVK1_1 = :ConnectVK1_1, "
                                        "ConnectVK1_2 = :ConnectVK1_2, "
                                        "ConnectVK1_3 = :ConnectVK1_3, "
                                        "ConnectVK1_4 = :ConnectVK1_4, "
                                        "ConnectVK1_5 = :ConnectVK1_5, "
                                        "ConnectVK1_6 = :ConnectVK1_6, "
                                        "ConnectVK1_7 = :ConnectVK1_7, "
                                        "ConnectVK1_8 = :ConnectVK1_8, "
                                        "ConnectVK1_9 = :ConnectVK1_9, "
                                        "ConnectVK1_10 = :ConnectVK1_10, "
                                        "ConnectVK1_11 = :ConnectVK1_11, "
                                        "ConnectVK1_12 = :ConnectVK1_12, "
                                        "ConnectVK1_13 = :ConnectVK1_13, "
                                        "ConnectVK1_14 = :ConnectVK1_14, "
                                        "ConnectVK1_15 = :ConnectVK1_15, "
                                        "ConnectVK1_16 = :ConnectVK1_16, "
                                        "ConnectVK1_17 = :ConnectVK1_17, "
                                        "ConnectVK1_18 = :ConnectVK1_18, "
                                        "ConnectVK1_19 = :ConnectVK1_19, "
                                        "ConnectVK1_20 = :ConnectVK1_20, "

                                        "modeVK2 = :modeVK2, "
                                        "ConnectVK2_1 = :ConnectVK2_1, "
                                        "ConnectVK2_2 = :ConnectVK2_2, "
                                        "ConnectVK2_3 = :ConnectVK2_3, "
                                        "ConnectVK2_4 = :ConnectVK2_4, "
                                        "ConnectVK2_5 = :ConnectVK2_5, "
                                        "ConnectVK2_6 = :ConnectVK2_6, "
                                        "ConnectVK2_7 = :ConnectVK2_7, "
                                        "ConnectVK2_8 = :ConnectVK2_8, "
                                        "ConnectVK2_9 = :ConnectVK2_9, "
                                        "ConnectVK2_10 = :ConnectVK2_10, "
                                        "ConnectVK2_11 = :ConnectVK2_11, "
                                        "ConnectVK2_12 = :ConnectVK2_12, "
                                        "ConnectVK2_13 = :ConnectVK2_13, "
                                        "ConnectVK2_14 = :ConnectVK2_14, "
                                        "ConnectVK2_15 = :ConnectVK2_15, "
                                        "ConnectVK2_16 = :ConnectVK2_16, "
                                        "ConnectVK2_17 = :ConnectVK2_17, "
                                        "ConnectVK2_18 = :ConnectVK2_18, "
                                        "ConnectVK2_19 = :ConnectVK2_19, "
                                        "ConnectVK2_20 = :ConnectVK2_20, "

                                        "xPosLFlangP_1 = :xPosLFlangP_1, "
                                        "yPosLFlangP_1 = :yPosLFlangP_1, "
                                        "xPosLFlangP_2 = :xPosLFlangP_2, "
                                        "yPosLFlangP_2 = :yPosLFlangP_2, "
                                        "xPosLFlangP_3 = :xPosLFlangP_3, "
                                        "yPosLFlangP_3 = :yPosLFlangP_3, "
                                        "xPosLFlangP_4 = :xPosLFlangP_4, "
                                        "yPosLFlangP_4 = :yPosLFlangP_4, "
                                        "xPosLFlangP_5 = :xPosLFlangP_5, "
                                        "yPosLFlangP_5 = :yPosLFlangP_5, "

                                        "xPosRFlangP_1 = :xPosRFlangP_1, "
                                        "yPosRFlangP_1 = :yPosRFlangP_1, "
                                        "xPosRFlangP_2 = :xPosRFlangP_2, "
                                        "yPosRFlangP_2 = :yPosRFlangP_2, "
                                        "xPosRFlangP_3 = :xPosRFlangP_3, "
                                        "yPosRFlangP_3 = :yPosRFlangP_3, "
                                        "xPosRFlangP_4 = :xPosRFlangP_4, "
                                        "yPosRFlangP_4 = :yPosRFlangP_4, "
                                        "xPosRFlangP_5 = :xPosRFlangP_5, "
                                        "yPosRFlangP_5 = :yPosRFlangP_5 "

                                        "WHERE id = :id;");

            QByteArray nullIDBSK;
            nullIDBSK.append((char)0x00);
            nullIDBSK.append((char)0x00);
            queryUpdateSettings.bindValue(":modeVK1", un->listSettingVK.at(0).modeVK);
            queryUpdateSettings.bindValue(":ConnectVK1_1", un->listSettingVK.at(0).listIDBSK.at(0)/*.toUInt()*/);
            queryUpdateSettings.bindValue(":ConnectVK1_2", un->listSettingVK.at(0).listIDBSK.at(1)/*.toUInt()*/);
            queryUpdateSettings.bindValue(":ConnectVK1_3", un->listSettingVK.at(0).listIDBSK.at(2)/*.toUInt()*/);
            queryUpdateSettings.bindValue(":ConnectVK1_4", un->listSettingVK.at(0).listIDBSK.at(3)/*.toUInt()*/);
            queryUpdateSettings.bindValue(":ConnectVK1_5", un->listSettingVK.at(0).listIDBSK.at(4)/*.toUInt()*/);
            queryUpdateSettings.bindValue(":ConnectVK1_6", un->listSettingVK.at(0).listIDBSK.at(5)/*.toUInt()*/);
            queryUpdateSettings.bindValue(":ConnectVK1_7", un->listSettingVK.at(0).listIDBSK.at(6)/*.toUInt()*/);
            queryUpdateSettings.bindValue(":ConnectVK1_8", un->listSettingVK.at(0).listIDBSK.at(7)/*.toUInt()*/);
            queryUpdateSettings.bindValue(":ConnectVK1_9", un->listSettingVK.at(0).listIDBSK.at(8)/*.toUInt()*/);
            queryUpdateSettings.bindValue(":ConnectVK1_10", un->listSettingVK.at(0).listIDBSK.at(9)/*.toUInt()*/);
            queryUpdateSettings.bindValue(":ConnectVK1_11", un->listSettingVK.at(0).listIDBSK.at(10)/*.toUInt()*/);
            queryUpdateSettings.bindValue(":ConnectVK1_12", un->listSettingVK.at(0).listIDBSK.at(11)/*.toUInt()*/);
            queryUpdateSettings.bindValue(":ConnectVK1_13", un->listSettingVK.at(0).listIDBSK.at(12)/*.toUInt()*/);
            queryUpdateSettings.bindValue(":ConnectVK1_14", un->listSettingVK.at(0).listIDBSK.at(13)/*.toUInt()*/);
            queryUpdateSettings.bindValue(":ConnectVK1_15", un->listSettingVK.at(0).listIDBSK.at(14)/*.toUInt()*/);
            queryUpdateSettings.bindValue(":ConnectVK1_16", un->listSettingVK.at(0).listIDBSK.at(15)/*.toUInt()*/);
            queryUpdateSettings.bindValue(":ConnectVK1_17", un->listSettingVK.at(0).listIDBSK.at(16)/*.toUInt()*/);
            queryUpdateSettings.bindValue(":ConnectVK1_18", un->listSettingVK.at(0).listIDBSK.at(17)/*.toUInt()*/);
            queryUpdateSettings.bindValue(":ConnectVK1_19", un->listSettingVK.at(0).listIDBSK.at(18)/*.toUInt()*/);
            queryUpdateSettings.bindValue(":ConnectVK1_20", un->listSettingVK.at(0).listIDBSK.at(19)/*.toUInt()*/);

            queryUpdateSettings.bindValue(":modeVK2", un->listSettingVK.at(1).modeVK);
            queryUpdateSettings.bindValue(":ConnectVK2_1", un->listSettingVK.at(1).listIDBSK.at(0)/*.toUInt()*/);
            queryUpdateSettings.bindValue(":ConnectVK2_2", un->listSettingVK.at(1).listIDBSK.at(1)/*.toUInt()*/);
            queryUpdateSettings.bindValue(":ConnectVK2_3", un->listSettingVK.at(1).listIDBSK.at(2)/*.toUInt()*/);
            queryUpdateSettings.bindValue(":ConnectVK2_4", un->listSettingVK.at(1).listIDBSK.at(3)/*.toUInt()*/);
            queryUpdateSettings.bindValue(":ConnectVK2_5", un->listSettingVK.at(1).listIDBSK.at(4)/*.toUInt()*/);
            queryUpdateSettings.bindValue(":ConnectVK2_6", un->listSettingVK.at(1).listIDBSK.at(5)/*.toUInt()*/);
            queryUpdateSettings.bindValue(":ConnectVK2_7", un->listSettingVK.at(1).listIDBSK.at(6)/*.toUInt()*/);
            queryUpdateSettings.bindValue(":ConnectVK2_8", un->listSettingVK.at(1).listIDBSK.at(7)/*.toUInt()*/);
            queryUpdateSettings.bindValue(":ConnectVK2_9", un->listSettingVK.at(1).listIDBSK.at(8)/*.toUInt()*/);
            queryUpdateSettings.bindValue(":ConnectVK2_10", un->listSettingVK.at(1).listIDBSK.at(9)/*.toUInt()*/);
            queryUpdateSettings.bindValue(":ConnectVK2_11", un->listSettingVK.at(1).listIDBSK.at(10)/*.toUInt()*/);
            queryUpdateSettings.bindValue(":ConnectVK2_12", un->listSettingVK.at(1).listIDBSK.at(11)/*.toUInt()*/);
            queryUpdateSettings.bindValue(":ConnectVK2_13", un->listSettingVK.at(1).listIDBSK.at(12)/*.toUInt()*/);
            queryUpdateSettings.bindValue(":ConnectVK2_14", un->listSettingVK.at(1).listIDBSK.at(13)/*.toUInt()*/);
            queryUpdateSettings.bindValue(":ConnectVK2_15", un->listSettingVK.at(1).listIDBSK.at(14)/*.toUInt()*/);
            queryUpdateSettings.bindValue(":ConnectVK2_16", un->listSettingVK.at(1).listIDBSK.at(15)/*.toUInt()*/);
            queryUpdateSettings.bindValue(":ConnectVK2_17", un->listSettingVK.at(1).listIDBSK.at(16)/*.toUInt()*/);
            queryUpdateSettings.bindValue(":ConnectVK2_18", un->listSettingVK.at(1).listIDBSK.at(17)/*.toUInt()*/);
            queryUpdateSettings.bindValue(":ConnectVK2_19", un->listSettingVK.at(1).listIDBSK.at(18)/*.toUInt()*/);
            queryUpdateSettings.bindValue(":ConnectVK2_20", un->listSettingVK.at(1).listIDBSK.at(19)/*.toUInt()*/);

            queryUpdateSettings.bindValue(":xPosLFlangP_1", un->listLFlangP.at(0)->x());
            queryUpdateSettings.bindValue(":yPosLFlangP_1", un->listLFlangP.at(0)->y());
            queryUpdateSettings.bindValue(":xPosLFlangP_2", un->listLFlangP.at(1)->x());
            queryUpdateSettings.bindValue(":yPosLFlangP_2", un->listLFlangP.at(1)->y());
            queryUpdateSettings.bindValue(":xPosLFlangP_3", un->listLFlangP.at(2)->x());
            queryUpdateSettings.bindValue(":yPosLFlangP_3", un->listLFlangP.at(2)->y());
            queryUpdateSettings.bindValue(":xPosLFlangP_4", un->listLFlangP.at(3)->x());
            queryUpdateSettings.bindValue(":yPosLFlangP_4", un->listLFlangP.at(3)->y());
            queryUpdateSettings.bindValue(":xPosLFlangP_5", un->listLFlangP.at(4)->x());
            queryUpdateSettings.bindValue(":yPosLFlangP_5", un->listLFlangP.at(4)->y());

            queryUpdateSettings.bindValue(":xPosRFlangP_1", un->listRFlangP.at(0)->x());
            queryUpdateSettings.bindValue(":yPosRFlangP_1", un->listRFlangP.at(0)->y());
            queryUpdateSettings.bindValue(":xPosRFlangP_2", un->listRFlangP.at(1)->x());
            queryUpdateSettings.bindValue(":yPosRFlangP_2", un->listRFlangP.at(1)->y());
            queryUpdateSettings.bindValue(":xPosRFlangP_3", un->listRFlangP.at(2)->x());
            queryUpdateSettings.bindValue(":yPosRFlangP_3", un->listRFlangP.at(2)->y());
            queryUpdateSettings.bindValue(":xPosRFlangP_4", un->listRFlangP.at(3)->x());
            queryUpdateSettings.bindValue(":yPosRFlangP_4", un->listRFlangP.at(3)->y());
            queryUpdateSettings.bindValue(":xPosRFlangP_5", un->listRFlangP.at(4)->x());
            queryUpdateSettings.bindValue(":yPosRFlangP_5", un->listRFlangP.at(4)->y());

            queryUpdateSettings.bindValue(":id", idExistUN);

            if(queryUpdateSettings.exec())
            {
                return idExistUN;
            }
            else
            {
#ifdef QT_DEBUG
                qDebug() << "DataBaseManager::updateUN Error :" << queryUpdateSettings.lastError().text();
#endif
            }
            //
            return idExistUN;
        }
        else
        {
#ifdef QT_DEBUG
            qDebug() << "DataBaseManager::updateUN Error :" << query.lastError().text();
#endif
            return 0;
        }
    }
}

QSqlQuery DataBaseManager::getFilterIMG(FilterStruct filter, TypeUnitNodeSystem typeSystem) /*noexcept*/
{
    QSqlQuery query(m_db);
    QString str;
    str =        ("SELECT [ArchiveIMG].[id], "
                         "[ArchiveIMG].[foreignIDMainMSG], "
                         "[ArchiveIMG].[foreignIDThisMSGIMG1], "
                         "[ArchiveIMG].[sizeIMG1], "
                         "[ArchiveIMG].[dataIMG1], "
                         "[ArchiveIMG].[foreignIDThisMSGIMG2], "
                         "[ArchiveIMG].[sizeIMG2], "
                         "[ArchiveIMG].[dataIMG2], "
                         "[ArchiveIMG].[foreignIDThisMSGIMG3], "
                         "[ArchiveIMG].[sizeIMG3], "
                         "[ArchiveIMG].[dataIMG3], "
                         "[ArchiveIMG].[foreignIDThisMSGIMG4], "
                         "[ArchiveIMG].[sizeIMG4], "
                         "[ArchiveIMG].[dataIMG4], "
                         "[ArchiveIMG].[foreignIDThisMSGIMG5], "
                         "[ArchiveIMG].[sizeIMG5], "
                         "[ArchiveIMG].[dataIMG5], "
                         "[ArchiveIMG].[checkedUp], "
                         "[ArchiveIMG].[foreignIDAlarm], "
                         "[ArchiveIMG].[numberVK], "
                         "[ArchiveMSG].[date], "
                         "[ArchiveMSG].[time], "
                         "[ArchiveMSG].[timeCorection], "
                         "[ArchiveMSG].[typeSystemMSG], "
                         "[ArchiveUnitNode].[id], "
                         "[ArchiveUnitNode].[idUN], "
                         "[ArchiveUnitNode].[macUN], "
                         "[ArchiveUnitNode].[nameUN], "
                         "[ArchiveUnitNode].[abstractNameUN], "
                         "[ArchiveUnitNode].[typeUN], "
                         "[ArchiveUnitNode].[typeSystemUN], "
                         "[ArchiveUnitNode].[link], "
                         "[ArchiveUnitNode].[linkValid], "
                         "[ArchiveUnitNode].[xPos], "
                         "[ArchiveUnitNode].[yPos], "
                         "[ArchiveUnitNode].[voltage], "
                         "[ArchiveUnitNode].[area], "
                         "[ArchiveUnitNode].[number], "
                         "[ArchiveUnitNode].[discharge], "
                         "[ArchiveUnitNode].[idParentUN] "
                  "FROM   [ArchiveIMG] INNER JOIN [ArchiveMSG] "
                  "ON [ArchiveIMG].[foreignIDMainMSG] = [ArchiveMSG].[id] "
                  "INNER JOIN [ArchiveUnitNode] "
                  "ON [ArchiveMSG].[foreignIDUN] = [ArchiveUnitNode].[id] ");

    str.append("WHERE ([ArchiveMSG].[typeSystemMSG] = :typeSystem) ");

    if(filter.filterDate)
    {
        str.append("AND (    :minDate <= [ArchiveMSG].[date] "
                    "     AND [ArchiveMSG].[date] <= :maxDate) ");
    }

    if(filter.filterTime)
    {
        str.append("AND (    :minTime <= [ArchiveMSG].[time] "
                    "     AND [ArchiveMSG].[time] <= :maxTime) ");
    }

    if(filter.filterSN)
    {
        if(-1 != filter.filterSector)
        {
            str.append("AND ([ArchiveUnitNode].[area] = :area) ");
        }

        if(-1 != filter.filterNumber)
        {
            str.append("AND ([ArchiveUnitNode].[number] = :number) ");
        }
    }

    //по типу не нужна

    quint32 foreignIDUN(0);
    if(filter.filterID && filter.filterIDValue.isEmpty())
    {
        foreignIDUN = this->getUNID(filter.filterIDValue, typeSystem);
        str.append("AND ([ArchiveUnitNode].[id] = :foreignIDUN) ");
    }

    str.append(";");
    query.prepare(str);

    query.bindValue(":typeSystem", (quint8)typeSystem);

    if(filter.filterDate)
    {
        query.bindValue(":minDate", filter.filterMinDate);
        query.bindValue(":maxDate", filter.filterMaxDate);
    }

    if(filter.filterTime)
    {
        query.bindValue(":minTime", filter.filterMinTime);
        query.bindValue(":maxTime", filter.filterMaxTime);
    }

    if(filter.filterSN)
    {
        if(-1 != filter.filterSector)
        {
            query.bindValue(":area", filter.filterSector);
        }

        if(-1 != filter.filterNumber)
        {
            query.bindValue(":number", filter.filterNumber);
        }
    }

    if(filter.filterID && filter.filterIDValue.isEmpty())
    {
        query.bindValue(":foreignIDUN", foreignIDUN);
    }

    if(query.exec())
    {
        return query;
    }
    else
    {
#ifdef QT_DEBUG
        qDebug() << "DataBaseManager::getFilterIMG Error :" << query.lastError().text();
#endif
        return QSqlQuery();
    }
}

QList<quint32> DataBaseManager::getFilterIMGRecords(FilterStruct filter, TypeUnitNodeSystem typeSystem) /*noexcept*/
{
    QList<quint32> list;
    QSqlQuery query(this->getFilterIMG(filter, typeSystem));
    while (query.next())
    {
        list.append(query.record().value("id").toUInt());
//        qDebug() << query.record();
    }
    return list;
}

QSqlRecord DataBaseManager::getIMGRecord(quint32 idIMG) /*noexcept*/
{
    QSqlRecord result;
    QString str = "SELECT [ArchiveIMG].[id], "
                         "[ArchiveIMG].[foreignIDMainMSG], "
                         "[ArchiveIMG].[foreignIDThisMSGIMG1], "
                         "[ArchiveIMG].[sizeIMG1], " //2
                         "[ArchiveIMG].[dataIMG1], " //3
                         "[ArchiveIMG].[foreignIDThisMSGIMG2], "
                         "[ArchiveIMG].[sizeIMG2], " //4
                         "[ArchiveIMG].[dataIMG2], " //5
                         "[ArchiveIMG].[foreignIDThisMSGIMG3], "
                         "[ArchiveIMG].[sizeIMG3], " //6
                         "[ArchiveIMG].[dataIMG3], " //7
                         "[ArchiveIMG].[foreignIDThisMSGIMG4], "
                         "[ArchiveIMG].[sizeIMG4], " //8
                         "[ArchiveIMG].[dataIMG4], " //9
                         "[ArchiveIMG].[foreignIDThisMSGIMG5], "
                         "[ArchiveIMG].[sizeIMG5], " //10
                         "[ArchiveIMG].[dataIMG5], " //11
                         "[ArchiveIMG].[checkedUp], "
                         "[ArchiveIMG].[foreignIDAlarm], "
                         "[ArchiveIMG].[numberVK], "
                         "[ArchiveMSG].[date], " //13
                         "[ArchiveMSG].[time], " //14
                         "[ArchiveMSG].[timeCorection], " //15
                         "[ArchiveMSG].[typeSystemMSG], "
//                       "[ArchiveUnitNode].[id], "
                         "[ArchiveUnitNode].[idUN], "
                         "[ArchiveUnitNode].[macUN], "
                         "[ArchiveUnitNode].[nameUN], " //19
                         "[ArchiveUnitNode].[abstractNameUN], "
                         "[ArchiveUnitNode].[typeUN], "
                         "[ArchiveUnitNode].[typeSystemUN], "
                         "[ArchiveUnitNode].[link], "
                         "[ArchiveUnitNode].[linkValid], "
                         "[ArchiveUnitNode].[xPos], "
                         "[ArchiveUnitNode].[yPos], "
                         "[ArchiveUnitNode].[voltage], "
                         "[ArchiveUnitNode].[area], " //27
                         "[ArchiveUnitNode].[number], " //28
                         "[ArchiveUnitNode].[discharge], "
                         "[ArchiveUnitNode].[idParentUN] "
                  "FROM   [ArchiveIMG] INNER JOIN [ArchiveMSG] "
                  "ON [ArchiveIMG].[foreignIDMainMSG] = [ArchiveMSG].[id] "
                  "INNER JOIN [ArchiveUnitNode] "
                  "ON [ArchiveMSG].[foreignIDUN] = [ArchiveUnitNode].[id] "
                  "WHERE [ArchiveIMG].[id] = %1;";
    str = str.arg(idIMG);
    QSqlQuery query(m_db);
    if(query.exec(str))
    {
        while(query.next())
        {
            result = query.record();
        }
    }
    else
    {
#ifdef QT_DEBUG
        qDebug() << "DataBaseManager::getIMGRecord(" << idIMG << ") Error :" << query.lastError().text();
#endif
    }
    return result;
}

QSqlRecord DataBaseManager::getLastQueueIMGRecord() /*noexcept*/
{
    QSqlRecord result;
    QSqlQuery query(m_db);
    query.prepare("SELECT [ArchiveIMG].[id], "
                         "[ArchiveIMG].[foreignIDMainMSG], "
                         "[ArchiveIMG].[foreignIDThisMSGIMG1], "
                         "[ArchiveIMG].[sizeIMG1], "
                         "[ArchiveIMG].[dataIMG1], "
                         "[ArchiveIMG].[foreignIDThisMSGIMG2], "
                         "[ArchiveIMG].[sizeIMG2], "
                         "[ArchiveIMG].[dataIMG2], "
                         "[ArchiveIMG].[foreignIDThisMSGIMG3], "
                         "[ArchiveIMG].[sizeIMG3], "
                         "[ArchiveIMG].[dataIMG3], "
                         "[ArchiveIMG].[foreignIDThisMSGIMG4], "
                         "[ArchiveIMG].[sizeIMG4], "
                         "[ArchiveIMG].[dataIMG4], "
                         "[ArchiveIMG].[foreignIDThisMSGIMG5], "
                         "[ArchiveIMG].[sizeIMG5], "
                         "[ArchiveIMG].[dataIMG5], "
                         "[ArchiveIMG].[checkedUp], "
                         "[ArchiveIMG].[foreignIDAlarm], "
                         "[ArchiveIMG].[numberVK], "
                         "[ArchiveMSG].[date], "
                         "[ArchiveMSG].[time], "
                         "[ArchiveMSG].[timeCorection], "
                         "[ArchiveMSG].[typeSystemMSG], "
                         "[ArchiveUnitNode].[id], "
                         "[ArchiveUnitNode].[idUN], "
                         "[ArchiveUnitNode].[macUN], "
                         "[ArchiveUnitNode].[nameUN], "
                         "[ArchiveUnitNode].[abstractNameUN], "
                         "[ArchiveUnitNode].[typeUN], "
                         "[ArchiveUnitNode].[typeSystemUN], "
                         "[ArchiveUnitNode].[link], "
                         "[ArchiveUnitNode].[linkValid], "
                         "[ArchiveUnitNode].[xPos], "
                         "[ArchiveUnitNode].[yPos], "
                         "[ArchiveUnitNode].[voltage], "
                         "[ArchiveUnitNode].[area], "
                         "[ArchiveUnitNode].[number], "
                         "[ArchiveUnitNode].[discharge], "
                         "[ArchiveUnitNode].[idParentUN] "
                  "FROM   [ArchiveIMG] INNER JOIN [ArchiveMSG] "
                  "ON [ArchiveIMG].[foreignIDMainMSG] = [ArchiveMSG].[id] "
                  "INNER JOIN [ArchiveUnitNode] "
                  "ON [ArchiveMSG].[foreignIDUN] = [ArchiveUnitNode].[id] "
                  "WHERE [ArchiveIMG].[checkedUp] = 0 "
                  "ORDER BY [ArchiveIMG].[id] DESC "
                  "LIMIT 1;");
    if(query.exec())
    {
        while(query.next())
        {
            result = query.record();
        }
    }
    else
    {
#ifdef QT_DEBUG
        qDebug() << "DataBaseManager::getLastQueueIMGRecord() Error :" << query.lastError().text();
#endif
    }
    return result;
}


// выгрузка параметров в бд
void DataBaseManager::uploadUNToDB(UnitNode* un) /*noexcept*/
{
    if(un->dbIndex)
    {
        QSqlQuery query(m_db);
        query.prepare("UPDATE ArchiveUnitNode SET "
                      "idUN = :idUN, "
                      "macUN = :macUN , "
                      "nameUN = :nameUN, "
                      "abstractNameUN = :abstractNameUN, "
                      "typeUN = :typeUN, "
                      "typeSystemUN = :typeSystemUN, "
                      "link = :link, "
                      "linkValid = :linkV, "
                      "xPos = :xp , "
                      "yPos = :yp, "
                      "voltage = :voltageV, "
                      "area = :areaV, "
                      "number = :numberV, "
                      "discharge = :dischargeV, "
                      "idParentUN = :idParent, "
                      "modeUN = :mode, "
                      "modeVK1 = :modeVK1, "
                      "modeVK2 = :modeVK2, "
                      "hidden = :hidden "
                      "WHERE id = :id;");
        query.bindValue(":idUN", un->id);
        query.bindValue(":macUN", un->mac);
        query.bindValue(":nameUN", un->name);
        query.bindValue(":abstractNameUN", un->abstractName);
        query.bindValue(":typeUN", (quint8)un->_typeUN());
        query.bindValue(":typeSystemUN", (quint8)un->_typeSystem());
        query.bindValue(":link", un->btLink);
        query.bindValue(":linkV", un->flag_connection);
        query.bindValue(":xp", un->graphicsPxmItem->x());
        query.bindValue(":yp", un->graphicsPxmItem->y());
        query.bindValue(":voltageV", un->voltage);
        query.bindValue(":areaV", un->numArea);
        query.bindValue(":numberV", un->numNode);
        query.bindValue(":dischargeV", un->flag_discharge);
        query.bindValue(":idParent", un->dbIndexParent);
        query.bindValue(":mode", un->modeVK);
        query.bindValue(":modeVK1", un->listSettingVK.at(0).modeVK);
        query.bindValue(":modeVK2", un->listSettingVK.at(1).modeVK);
        query.bindValue(":hidden", (un->flag_hidden ? 1 : 0));
        query.bindValue(":id", un->dbIndex);

//        un->graphicsTxtItem->setX(un->graphicsPxmItem->x());
//        un->graphicsTxtItem->setY(un->graphicsPxmItem->y() + 20);
//        if(!un->_id().isEmpty())
//        {
//            un->graphicsTxtItem->setHtml(un->getGraphicsTextLabel());
//        }


        if(query.exec())
        {
            //
            QSqlQuery queryUpdateSettings(m_db);
            queryUpdateSettings.prepare("UPDATE ArchiveUnitNode SET "

                                        "modeVK1 = :modeVK1, "
                                        "ConnectVK1_1 = :ConnectVK1_1, "
                                        "ConnectVK1_2 = :ConnectVK1_2, "
                                        "ConnectVK1_3 = :ConnectVK1_3, "
                                        "ConnectVK1_4 = :ConnectVK1_4, "
                                        "ConnectVK1_5 = :ConnectVK1_5, "
                                        "ConnectVK1_6 = :ConnectVK1_6, "
                                        "ConnectVK1_7 = :ConnectVK1_7, "
                                        "ConnectVK1_8 = :ConnectVK1_8, "
                                        "ConnectVK1_9 = :ConnectVK1_9, "
                                        "ConnectVK1_10 = :ConnectVK1_10, "
                                        "ConnectVK1_11 = :ConnectVK1_11, "
                                        "ConnectVK1_12 = :ConnectVK1_12, "
                                        "ConnectVK1_13 = :ConnectVK1_13, "
                                        "ConnectVK1_14 = :ConnectVK1_14, "
                                        "ConnectVK1_15 = :ConnectVK1_15, "
                                        "ConnectVK1_16 = :ConnectVK1_16, "
                                        "ConnectVK1_17 = :ConnectVK1_17, "
                                        "ConnectVK1_18 = :ConnectVK1_18, "
                                        "ConnectVK1_19 = :ConnectVK1_19, "
                                        "ConnectVK1_20 = :ConnectVK1_20, "

                                        "modeVK2 = :modeVK2, "
                                        "ConnectVK2_1 = :ConnectVK2_1, "
                                        "ConnectVK2_2 = :ConnectVK2_2, "
                                        "ConnectVK2_3 = :ConnectVK2_3, "
                                        "ConnectVK2_4 = :ConnectVK2_4, "
                                        "ConnectVK2_5 = :ConnectVK2_5, "
                                        "ConnectVK2_6 = :ConnectVK2_6, "
                                        "ConnectVK2_7 = :ConnectVK2_7, "
                                        "ConnectVK2_8 = :ConnectVK2_8, "
                                        "ConnectVK2_9 = :ConnectVK2_9, "
                                        "ConnectVK2_10 = :ConnectVK2_10, "
                                        "ConnectVK2_11 = :ConnectVK2_11, "
                                        "ConnectVK2_12 = :ConnectVK2_12, "
                                        "ConnectVK2_13 = :ConnectVK2_13, "
                                        "ConnectVK2_14 = :ConnectVK2_14, "
                                        "ConnectVK2_15 = :ConnectVK2_15, "
                                        "ConnectVK2_16 = :ConnectVK2_16, "
                                        "ConnectVK2_17 = :ConnectVK2_17, "
                                        "ConnectVK2_18 = :ConnectVK2_18, "
                                        "ConnectVK2_19 = :ConnectVK2_19, "
                                        "ConnectVK2_20 = :ConnectVK2_20, "

                                        "xPosLFlangP_1 = :xPosLFlangP_1, "
                                        "yPosLFlangP_1 = :yPosLFlangP_1, "
                                        "xPosLFlangP_2 = :xPosLFlangP_2, "
                                        "yPosLFlangP_2 = :yPosLFlangP_2, "
                                        "xPosLFlangP_3 = :xPosLFlangP_3, "
                                        "yPosLFlangP_3 = :yPosLFlangP_3, "
                                        "xPosLFlangP_4 = :xPosLFlangP_4, "
                                        "yPosLFlangP_4 = :yPosLFlangP_4, "
                                        "xPosLFlangP_5 = :xPosLFlangP_5, "
                                        "yPosLFlangP_5 = :yPosLFlangP_5, "

                                        "xPosRFlangP_1 = :xPosRFlangP_1, "
                                        "yPosRFlangP_1 = :yPosRFlangP_1, "
                                        "xPosRFlangP_2 = :xPosRFlangP_2, "
                                        "yPosRFlangP_2 = :yPosRFlangP_2, "
                                        "xPosRFlangP_3 = :xPosRFlangP_3, "
                                        "yPosRFlangP_3 = :yPosRFlangP_3, "
                                        "xPosRFlangP_4 = :xPosRFlangP_4, "
                                        "yPosRFlangP_4 = :yPosRFlangP_4, "
                                        "xPosRFlangP_5 = :xPosRFlangP_5, "
                                        "yPosRFlangP_5 = :yPosRFlangP_5, "

                                        "geoAltitude = :geoAltitude, "

                                        "geoLatitudeFlag = :geoLatitudeFlag, "
                                        "geoLatitude = :geoLatitude, "
                                        "geoLatitudeDegrees = :geoLatitudeDegrees, "
                                        "geoLatitudeMinutes = :geoLatitudeMinutes, "
                                        "geoLatitudeSeconds = :geoLatitudeSeconds, "

                                        "geoLongitudeFlag = :geoLongitudeFlag, "
                                        "geoLongitude = :geoLongitude, "
                                        "geoLongitudeDegrees = :geoLongitudeDegrees, "
                                        "geoLongitudeMinutes = :geoLongitudeMinutes, "
                                        "geoLongitudeSeconds = :geoLongitudeSeconds "

                                        "WHERE id = :id;");

            queryUpdateSettings.bindValue(":modeVK1", un->listSettingVK.at(0).modeVK);
            queryUpdateSettings.bindValue(":ConnectVK1_1", un->listSettingVK.at(0).listIDBSK.at(0)/*.toUInt()*/);
            queryUpdateSettings.bindValue(":ConnectVK1_2", un->listSettingVK.at(0).listIDBSK.at(1)/*.toUInt()*/);
            queryUpdateSettings.bindValue(":ConnectVK1_3", un->listSettingVK.at(0).listIDBSK.at(2)/*.toUInt()*/);
            queryUpdateSettings.bindValue(":ConnectVK1_4", un->listSettingVK.at(0).listIDBSK.at(3)/*.toUInt()*/);
            queryUpdateSettings.bindValue(":ConnectVK1_5", un->listSettingVK.at(0).listIDBSK.at(4)/*.toUInt()*/);
            queryUpdateSettings.bindValue(":ConnectVK1_6", un->listSettingVK.at(0).listIDBSK.at(5)/*.toUInt()*/);
            queryUpdateSettings.bindValue(":ConnectVK1_7", un->listSettingVK.at(0).listIDBSK.at(6)/*.toUInt()*/);
            queryUpdateSettings.bindValue(":ConnectVK1_8", un->listSettingVK.at(0).listIDBSK.at(7)/*.toUInt()*/);
            queryUpdateSettings.bindValue(":ConnectVK1_9", un->listSettingVK.at(0).listIDBSK.at(8)/*.toUInt()*/);
            queryUpdateSettings.bindValue(":ConnectVK1_10", un->listSettingVK.at(0).listIDBSK.at(9)/*.toUInt()*/);
            queryUpdateSettings.bindValue(":ConnectVK1_11", un->listSettingVK.at(0).listIDBSK.at(10)/*.toUInt()*/);
            queryUpdateSettings.bindValue(":ConnectVK1_12", un->listSettingVK.at(0).listIDBSK.at(11)/*.toUInt()*/);
            queryUpdateSettings.bindValue(":ConnectVK1_13", un->listSettingVK.at(0).listIDBSK.at(12)/*.toUInt()*/);
            queryUpdateSettings.bindValue(":ConnectVK1_14", un->listSettingVK.at(0).listIDBSK.at(13)/*.toUInt()*/);
            queryUpdateSettings.bindValue(":ConnectVK1_15", un->listSettingVK.at(0).listIDBSK.at(14)/*.toUInt()*/);
            queryUpdateSettings.bindValue(":ConnectVK1_16", un->listSettingVK.at(0).listIDBSK.at(15)/*.toUInt()*/);
            queryUpdateSettings.bindValue(":ConnectVK1_17", un->listSettingVK.at(0).listIDBSK.at(16)/*.toUInt()*/);
            queryUpdateSettings.bindValue(":ConnectVK1_18", un->listSettingVK.at(0).listIDBSK.at(17)/*.toUInt()*/);
            queryUpdateSettings.bindValue(":ConnectVK1_19", un->listSettingVK.at(0).listIDBSK.at(18)/*.toUInt()*/);
            queryUpdateSettings.bindValue(":ConnectVK1_20", un->listSettingVK.at(0).listIDBSK.at(19)/*.toUInt()*/);

            queryUpdateSettings.bindValue(":modeVK2", un->listSettingVK.at(1).modeVK);
            queryUpdateSettings.bindValue(":ConnectVK2_1", un->listSettingVK.at(1).listIDBSK.at(0)/*.toUInt()*/);
            queryUpdateSettings.bindValue(":ConnectVK2_2", un->listSettingVK.at(1).listIDBSK.at(1)/*.toUInt()*/);
            queryUpdateSettings.bindValue(":ConnectVK2_3", un->listSettingVK.at(1).listIDBSK.at(2)/*.toUInt()*/);
            queryUpdateSettings.bindValue(":ConnectVK2_4", un->listSettingVK.at(1).listIDBSK.at(3)/*.toUInt()*/);
            queryUpdateSettings.bindValue(":ConnectVK2_5", un->listSettingVK.at(1).listIDBSK.at(4)/*.toUInt()*/);
            queryUpdateSettings.bindValue(":ConnectVK2_6", un->listSettingVK.at(1).listIDBSK.at(5)/*.toUInt()*/);
            queryUpdateSettings.bindValue(":ConnectVK2_7", un->listSettingVK.at(1).listIDBSK.at(6)/*.toUInt()*/);
            queryUpdateSettings.bindValue(":ConnectVK2_8", un->listSettingVK.at(1).listIDBSK.at(7)/*.toUInt()*/);
            queryUpdateSettings.bindValue(":ConnectVK2_9", un->listSettingVK.at(1).listIDBSK.at(8)/*.toUInt()*/);
            queryUpdateSettings.bindValue(":ConnectVK2_10", un->listSettingVK.at(1).listIDBSK.at(9)/*.toUInt()*/);
            queryUpdateSettings.bindValue(":ConnectVK2_11", un->listSettingVK.at(1).listIDBSK.at(10)/*.toUInt()*/);
            queryUpdateSettings.bindValue(":ConnectVK2_12", un->listSettingVK.at(1).listIDBSK.at(11)/*.toUInt()*/);
            queryUpdateSettings.bindValue(":ConnectVK2_13", un->listSettingVK.at(1).listIDBSK.at(12)/*.toUInt()*/);
            queryUpdateSettings.bindValue(":ConnectVK2_14", un->listSettingVK.at(1).listIDBSK.at(13)/*.toUInt()*/);
            queryUpdateSettings.bindValue(":ConnectVK2_15", un->listSettingVK.at(1).listIDBSK.at(14)/*.toUInt()*/);
            queryUpdateSettings.bindValue(":ConnectVK2_16", un->listSettingVK.at(1).listIDBSK.at(15)/*.toUInt()*/);
            queryUpdateSettings.bindValue(":ConnectVK2_17", un->listSettingVK.at(1).listIDBSK.at(16)/*.toUInt()*/);
            queryUpdateSettings.bindValue(":ConnectVK2_18", un->listSettingVK.at(1).listIDBSK.at(17)/*.toUInt()*/);
            queryUpdateSettings.bindValue(":ConnectVK2_19", un->listSettingVK.at(1).listIDBSK.at(18)/*.toUInt()*/);
            queryUpdateSettings.bindValue(":ConnectVK2_20", un->listSettingVK.at(1).listIDBSK.at(19)/*.toUInt()*/);

            queryUpdateSettings.bindValue(":xPosLFlangP_1", un->listLFlangP.at(0)->x());
            queryUpdateSettings.bindValue(":yPosLFlangP_1", un->listLFlangP.at(0)->y());
            queryUpdateSettings.bindValue(":xPosLFlangP_2", un->listLFlangP.at(1)->x());
            queryUpdateSettings.bindValue(":yPosLFlangP_2", un->listLFlangP.at(1)->y());
            queryUpdateSettings.bindValue(":xPosLFlangP_3", un->listLFlangP.at(2)->x());
            queryUpdateSettings.bindValue(":yPosLFlangP_3", un->listLFlangP.at(2)->y());
            queryUpdateSettings.bindValue(":xPosLFlangP_4", un->listLFlangP.at(3)->x());
            queryUpdateSettings.bindValue(":yPosLFlangP_4", un->listLFlangP.at(3)->y());
            queryUpdateSettings.bindValue(":xPosLFlangP_5", un->listLFlangP.at(4)->x());
            queryUpdateSettings.bindValue(":yPosLFlangP_5", un->listLFlangP.at(4)->y());

            queryUpdateSettings.bindValue(":xPosRFlangP_1", un->listRFlangP.at(0)->x());
            queryUpdateSettings.bindValue(":yPosRFlangP_1", un->listRFlangP.at(0)->y());
            queryUpdateSettings.bindValue(":xPosRFlangP_2", un->listRFlangP.at(1)->x());
            queryUpdateSettings.bindValue(":yPosRFlangP_2", un->listRFlangP.at(1)->y());
            queryUpdateSettings.bindValue(":xPosRFlangP_3", un->listRFlangP.at(2)->x());
            queryUpdateSettings.bindValue(":yPosRFlangP_3", un->listRFlangP.at(2)->y());
            queryUpdateSettings.bindValue(":xPosRFlangP_4", un->listRFlangP.at(3)->x());
            queryUpdateSettings.bindValue(":yPosRFlangP_4", un->listRFlangP.at(3)->y());
            queryUpdateSettings.bindValue(":xPosRFlangP_5", un->listRFlangP.at(4)->x());
            queryUpdateSettings.bindValue(":yPosRFlangP_5", un->listRFlangP.at(4)->y());

            queryUpdateSettings.bindValue(":geoAltitude, ", un->geoAltitude);

            queryUpdateSettings.bindValue(":geoLatitudeFlag", un->geoLatitudeFlag);
            queryUpdateSettings.bindValue(":geoLatitude", un->geoLatitude);
            queryUpdateSettings.bindValue(":geoLatitudeDegrees", un->geoLatitudeDegrees);
            queryUpdateSettings.bindValue(":geoLatitudeMinutes", un->geoLatitudeMinutes);
            queryUpdateSettings.bindValue(":geoLatitudeSeconds", un->geoLatitudeSeconds);

            queryUpdateSettings.bindValue(":geoLongitudeFlag", un->geoLongitudeFlag);
            queryUpdateSettings.bindValue(":geoLongitude", un->geoLongitude);
            queryUpdateSettings.bindValue(":geoLongitudeDegrees", un->geoLongitudeDegrees);
            queryUpdateSettings.bindValue(":geoLongitudeMinutes", un->geoLongitudeMinutes);
            queryUpdateSettings.bindValue(":geoLongitudeSeconds", un->geoLongitudeSeconds);

            queryUpdateSettings.bindValue(":id", un->dbIndex);

            if(queryUpdateSettings.exec())
            {
                return;
            }
            else
            {
#ifdef QT_DEBUG
                qDebug() << "DataBaseManager::uploadUNToDB2 Error :" << queryUpdateSettings.lastError().text();
#endif
            }
            //

            return;
        }
        else
        {
#ifdef QT_DEBUG
            qDebug() << "DataBaseManager::uploadUNToDB1 Error :" << query.lastError().text();
#endif
            return;
        }
    }
}

// загрузка параметров из бд
void DataBaseManager::loadUNFromDB(UnitNode* un) /*noexcept*/
{
    QSqlRecord unRecord(this->getUNRecord(un->dbIndex));
    if(unRecord.isEmpty())
        return;

    un->id = unRecord.value("idUN").toByteArray();
    un->mac = unRecord.value("macUN").toByteArray();
    un->name = unRecord.value("nameUN").toByteArray();
    un->abstractName = unRecord.value("abstractNameUN").toByteArray();
    un->typeUN = (TypeUnitNodeDevice)unRecord.value("typeUN").toUInt();
    un->typeSystem = (TypeUnitNodeSystem)unRecord.value("typeSystemUN").toUInt();
    un->btLink = unRecord.value("link").toByteArray();
    un->flag_connection = unRecord.value("linkValid").toBool();
    un->graphicsPxmItem->setX(unRecord.value("xPos").toInt());
    un->graphicsPxmItem->setY(unRecord.value("yPos").toInt());
    un->voltage = unRecord.value("voltage").toUInt();
    un->numArea = unRecord.value("area").toUInt();
    un->numNode = unRecord.value("number").toUInt();
    un->flag_discharge = unRecord.value("discharge").toBool();
    un->dbIndex = unRecord.value("id").toUInt();
    un->dbIndexParent = unRecord.value("idParentUN").toUInt();
    un->modeVK = unRecord.value("modeUN").toUInt();

//    un->graphicsTxtItem->setX(un->graphicsPxmItem->x());
//    un->graphicsTxtItem->setY(un->graphicsPxmItem->y() + 20);
//    if(!un->_id().isEmpty())
//    {
//        un->graphicsTxtItem->setHtml(un->getGraphicsTextLabel());
//    }

    un->flag_hidden = ((0 != unRecord.value("hidden").toInt()) ? true : false);


    if(UnknownDeviceType != un->_typeUN())
    {
//        un->graphicsPxmItem->setFlag(QGraphicsItem::ItemIsMovable);
        un->graphicsPxmItem->setFlag(QGraphicsItem::ItemIsSelectable);
        un->graphicsPxmItem->setFlag(QGraphicsItem::ItemIsFocusable);
        un->graphicsPxmItem->setZValue(1);
    }
}

//связываем тревогу и кадр
quint64 DataBaseManager::updateForeignIDAlarm(UnitNode *un,
                                              quint64 idMSG0xC4) /*noexcept*/
{
    QSqlRecord recordMSG_0xC4(this->get_MSGRecord(idMSG0xC4));
    QByteArray msg0xC4(recordMSG_0xC4.value("msg").toByteArray());

    QSqlQuery msg0xC1Query(m_db);
    msg0xC1Query.prepare("SELECT [ArchiveMSG].* "
                         "FROM [ArchiveMSG] "
                         "INNER JOIN [ArchiveIMG] "
                         "ON [ArchiveIMG].[foreignIDMainMSG] = [ArchiveMSG].[id] "
                         "WHERE [ArchiveMSG].[typeMSG] = 193 "
                         "AND [ArchiveMSG].[codeMSG] = 159 "
                         "AND [ArchiveMSG].[foreignIDUN] = :foreignIDUN "
                         "ORDER BY [ArchiveMSG].[id] DESC "
                         "LIMIT 10;");

    msg0xC1Query.bindValue(":foreignIDUN", un->_dbIndex());

    if(msg0xC1Query.exec())
    {
        while (msg0xC1Query.next())
        {
            QSqlRecord msg0xC1Record(msg0xC1Query.record());
            QByteArray msg0xC1(msg0xC1Record.value("msg").toByteArray());

            if((quint8)msg0xC1.at(17) == (quint8)msg0xC4.at(12) &&//numVK
               ((quint8)msg0xC1.at(12) & (quint8)0xF0) == ((quint8)msg0xC4.at(13) & (quint8)0xF0))//numTrain
            {
                QSqlQuery updateQuery(m_db);
                updateQuery.prepare("UPDATE ArchiveIMG SET "
                                    "foreignIDAlarm = :idMSG0xC4 "
                                    "WHERE foreignIDMainMSG = :idMSG0xC1;");
                updateQuery.bindValue(":idMSG0xC4", idMSG0xC4);
                updateQuery.bindValue(":idMSG0xC1", msg0xC1Record.value("id").toUInt());

                if(updateQuery.exec())
                {
                    return idMSG0xC4;
                }
                else
                {
#ifdef QT_DEBUG
                    qDebug() << "DataBaseManager::updateForeignIDAlarm Error :" << updateQuery.lastError().text();
#endif
                    return 0;
                }
            }
            return 0;

            if((quint8)msg0xC1.at(17) == (quint8)msg0xC4.at(12) &&
               (quint8)msg0xC1.at(12) == (quint8)msg0xC4.at(13))
            {
                QSqlQuery msg0x01Query(m_db);
                msg0x01Query.prepare("SELECT [ArchiveMSG].* "
                                     "FROM [ArchiveMSG] "
                                     "WHERE [ArchiveMSG].[typeMSG] = 1 "
                                     "AND [ArchiveMSG].[codeMSG] = 159 "
                                     "ORDER BY [ArchiveMSG].[id] DESC "
                                     "LIMIT 5;");
                if(msg0x01Query.exec())
                {
                    while (msg0x01Query.next())
                    {
                        QSqlRecord msg0x01Record(msg0x01Query.record());
                        QByteArray msg0x01(msg0x01Record.value("msg").toByteArray());

                        if((quint8)msg0x01.at(12) == (quint8)msg0xC4.at(14) &&
                           (quint8)msg0x01.at(13) == (quint8)msg0xC4.at(15))
                        {
                            QSqlQuery updateQuery(m_db);
                            updateQuery.prepare("UPDATE ArchiveIMG SET "
                                                "foreignIDAlarm = :idMSG0x01 "
                                                "WHERE foreignIDMainMSG = :idMSG0xC1;");
                            updateQuery.bindValue(":idMSG0x01", msg0x01Record.value("id").toUInt());
                            updateQuery.bindValue(":idMSG0xC1", msg0xC1Record.value("id").toUInt());

                            if(updateQuery.exec())
                            {
                                return msg0x01Record.value("id").toUInt();
                            }
                            else
                            {
#ifdef QT_DEBUG
                                qDebug() << "DataBaseManager::updateForeignIDAlarm Error :" << updateQuery.lastError().text();
#endif
                                return 0;
                            }
                        }
                    }
                }
                else
                {
#ifdef QT_DEBUG
                    qDebug() << "DataBaseManager::updateForeignIDAlarm Error :" << msg0x01Query.lastError().text();
#endif
                    return 0;

                }
            }
        }
    }
    else
    {
#ifdef QT_DEBUG
        qDebug() << "DataBaseManager::updateForeignIDAlarm Error :" << msg0xC1Query.lastError().text();
#endif
        return 0;
    }
    return 0;
}

QDate DataBaseManager::getMinDateMSG() /*noexcept*/
{
    QSqlQuery query(m_db);
    query.prepare("SELECT MIN([ArchiveMSG].[date]) "
                  "FROM [ArchiveMSG];");

    if(query.exec())
    {
        while(query.next())
        {
            return query.record().value("MIN([ArchiveMSG].[date])").toDate();
        }
    }
    return QDate();
}

QList<int> DataBaseManager::getSectorGroupUN(TypeUnitNodeSystem typeSystem) /*noexcept*/
{
    QList<int> result;
    QSqlQuery query(m_db);
    query.prepare("SELECT [ArchiveUnitNode].[area] "
                  "FROM [ArchiveUnitNode] "
                  "WHERE [ArchiveUnitNode].[typeSystemUN] = :typeSystem "
                  "GROUP BY [ArchiveUnitNode].[area] "
                  "ORDER BY [ArchiveUnitNode].[area];");
    query.bindValue(":typeSystem", (quint8)typeSystem);
    if(query.exec())
    {
        while(query.next())
        {
            result.append(query.record().value("area").toInt());
        }
    }
    return result;
}

QList<int> DataBaseManager::getNumberGroupUN(TypeUnitNodeSystem typeSystem) /*noexcept*/
{
    QList<int> result;
    QSqlQuery query(m_db);
    query.prepare("SELECT [ArchiveUnitNode].[number] "
                  "FROM [ArchiveUnitNode] "
                  "WHERE [ArchiveUnitNode].[typeSystemUN] = :typeSystem "
                  "GROUP BY [ArchiveUnitNode].[number] "
                  "ORDER BY [ArchiveUnitNode].[number];");
    query.bindValue(":typeSystem", (quint8)typeSystem);
    if(query.exec())
    {
        while(query.next())
        {
            result.append(query.record().value("number").toInt());
        }
    }
    return result;
}

QList<TypeUnitNodeDevice> DataBaseManager::getTypeGroupUN(TypeUnitNodeSystem typeSystem) /*noexcept*/
{
    QList<TypeUnitNodeDevice> result;
    QSqlQuery query(m_db);
    query.prepare("SELECT [ArchiveUnitNode].[typeUN] "
                  "FROM [ArchiveUnitNode] "
                  "WHERE [ArchiveUnitNode].[typeSystemUN] = :typeSystem "
                  "GROUP BY [ArchiveUnitNode].[typeUN] "
                  "ORDER BY [ArchiveUnitNode].[typeUN];");
    query.bindValue(":typeSystem", (quint8)typeSystem);
    if(query.exec())
    {
        while(query.next())
        {
            result.append((TypeUnitNodeDevice)query.record().value("typeUN").toInt());
        }
    }
    return result;
}


