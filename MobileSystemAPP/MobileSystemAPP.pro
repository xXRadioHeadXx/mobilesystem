#-------------------------------------------------
#
# Project created by QtCreator 2013-08-20T14:13:31
#
#-------------------------------------------------

CONFIG += c++11
#CONFIG += console

QT       += core gui sql serialport positioning

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
#qtHaveModule(printsupport): QT += printsupport

include(MV/mv.pri)
include(Graphics/graphics.pri)
include(KM/km.pri)
include(KS/ks.pri)
include(BSK/bsk.pri)
include(PKP/pkp.pri)

include(src/src.pri)


OBJECTS_DIR = obj
MOC_DIR = moc

TARGET = MobileSystemAPP


SOURCES += main.cpp \
           mainwindow.cpp \
           unitnode.cpp \
           databasemanager.cpp \
           dialogserialportsetting.cpp \
           dialogtablearchive.cpp \
#           soundalarm.cpp \
           apptranslator.cpp \
           dialogimageviwer.cpp #\
#           proxymetod.cpp \
#    listvaluecontainer.cpp
#    proxysorttreecontainerun.cpp


HEADERS  += mainwindow.h \
            unitnode.h \
            databasemanager.h \
            dialogserialportsetting.h \
            global_value_and_structure.h \
            dialogtablearchive.h \
#            soundalarm.h \
            apptranslator.h \
            dialogimageviwer.h #\
#            proxymetod.h \
#    listvaluecontainer.h
#    proxysorttreecontainerun.h

FORMS    += mainwindow.ui \
            dialogserialportsetting.ui \
            dialogtablearchive.ui \
            dialogimageviwer.ui

TRANSLATIONS += app_ru.ts \
                app_ru.qm


RESOURCES += img_resource.qrc

RC_FILE = main_icon.rc


OTHER_FILES += main_icon.rc



CONFIG(debug, debug|release) {
    DESTDIR = debug
    TARGET = MobileSystemAPP_debug
} else {
#    QMAKE_LFLAGS += -static -static-libgcc
    DESTDIR = ../bin/
    TARGET = MobileSystemAPP
}

#win32 {
#    LIBS += -lsetupapi -luuid -ladvapi32
#}
#unix:!macx {
#    LIBS += -ludev
#}

