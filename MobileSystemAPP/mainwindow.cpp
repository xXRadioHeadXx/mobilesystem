﻿#include <mainwindow.h>
#include <ui_mainwindow.h>

#include <QtCore/QVariant>
#include <QPixmap>

MainWindow::MainWindow(QWidget *parent) /*noexcept*/ :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    try
    {
    #ifdef QT_DEBUG
        qDebug() << "Debug Mode: TRUE";
    #endif

    #ifdef QT_DEBUG
    #else
    //    ui->groupBox_kvantSetting->setVisible(false);
    //    ui->groupBox_PortCommand->setVisible(false);
    //    ui->menuBar->setVisible(false);
    #endif

        ui->spinBox_0x42x43_Inst_T2_VK_KS->setVisible(false);
        ui->spinBox_0x42x43_Inst_T3_VK_KS->setVisible(false);
        ui->label_0x42x43_Inst_Mode_VK_3->setVisible(false);
        ui->label_0x42x43_Inst_Mode_VK_4->setVisible(false);
        ui->label_SpaserH_1->setVisible(false);
        ui->label_SpaserH_2->setVisible(false);

        this->currentSystemType = KMSystemType;
        this->ruTranslator = new QTranslator(this);
        this->ruTranslator->load("app_ru");

        //изменение размеров
    //    connect(ui->tabWidget,
    //            SIGNAL(currentChanged(int)),
    //            this,
    //            SLOT(uiElementResize()));


        connect(ui->splitter_main,
                SIGNAL(splitterMoved(int,int)),
                this,
                SLOT(setDefaultTableSize()));
        connect(ui->stackedWidget_3,
                SIGNAL(currentChanged(int)),
                this,
                SLOT(setDefaultTableSize()));

    //    connect(ui->splitter_horizontal,
    //            SIGNAL(splitterMoved(int,int)),
    //            this,
    //            SLOT(uiElementResize()));

    //    connect(ui->splitter_vertical,
    //            SIGNAL(splitterMoved(int,int)),
    //            this,
    //            SLOT(uiElementResize()));

    //    connect(ui->mainToolBar,
    //            SIGNAL(orientationChanged(Qt::Orientation)),
    //            this,
    //            SLOT(uiElementResize()));

    //    connect(ui->statusBar,
    //            SLOT(),
    //            this,
    //            SLOT(uiElementResize()));
        //изменение размеров


        this->m_dbManager = new DataBaseManager(this);
        this->m_dbManager->moveToThread(QApplication::instance()->thread());
        this->m_kmManager = new KamuflageManager(this->m_dbManager);
        this->m_ksManager = new KiparisManager(this->m_dbManager);
        this->m_bskManager = new BSKManager(this->m_dbManager);
        this->m_pkpManager = new PKPManager(this->m_dbManager);


        connect(this->m_kmManager,
                SIGNAL(portCondition(qint32)),
                this,
                SLOT(updatePortCondition(qint32)));
        connect(this->m_ksManager,
                SIGNAL(portCondition(qint32)),
                this,
                SLOT(updatePortCondition(qint32)));
        connect(this->m_bskManager,
                SIGNAL(portCondition(qint32)),
                this,
                SLOT(updatePortCondition(qint32)));
        connect(this->m_pkpManager,
                SIGNAL(portCondition(qint32)),
                this,
                SLOT(updatePortCondition(qint32)));

        connect(this->m_pkpManager,
                SIGNAL(newMSGPKP(QByteArray)),
                this->m_bskManager,
                SLOT(newMSGPKP(QByteArray)));


        this->modelArchive_KM_MSG = new TableModelArchiveMSG(this->m_dbManager,
                                                             KMSystemType,
                                                             this);
        ui->tableView_KM_MSG->setModel(this->modelArchive_KM_MSG);
        connect(this->modelArchive_KM_MSG,
                SIGNAL(needScrollToBottom()),
                ui->tableView_KM_MSG,
                SLOT(scrollToBottom()));

        this->modelArchive_KM_IMG = new TableModelArchiveIMG(this->m_dbManager,
                                                             KMSystemType,
                                                             this);
    //    connect(this->m_kmManager,
    //            SIGNAL(insertNewIMG(quint32)),
    //            this->modelArchive_KM_IMG,
    //            SLOT(updateListRecords(quint32)));
        ui->tableView_KM_IMG->setModel(this->modelArchive_KM_IMG);
        connect(this->modelArchive_KM_IMG,
                SIGNAL(needScrollToBottom()),
                ui->tableView_KM_IMG,
                SLOT(scrollToBottom()));
        connect(ui->tableView_KM_IMG,
                SIGNAL(doubleClicked(QModelIndex)),
                this->modelArchive_KM_IMG,
                SLOT(emitNeedViewIMG(QModelIndex)));
        connect(this->modelArchive_KM_IMG,
                SIGNAL(newRecord(quint32,quint32)),
                this,
                SLOT(newRecord_KMIMG(quint32,quint32)));


        this->modelArchive_KS_MSG = new TableModelArchiveMSG(this->m_dbManager,
                                                             KSSystemType,
                                                             this);
        ui->tableView_KS_MSG->setModel(this->modelArchive_KS_MSG);
        connect(this->modelArchive_KS_MSG,
                SIGNAL(needScrollToBottom()),
                ui->tableView_KS_MSG,
                SLOT(scrollToBottom()));

        this->modelArchive_KS_IMG = new TableModelArchiveIMG(this->m_dbManager,
                                                             KSSystemType,
                                                             this);
    //    connect(this->m_ksManager,
    //            SIGNAL(insertNewIMG(quint32)),
    //            this->modelArchive_KS_IMG,
    //            SLOT(updateListRecords(quint32)));
        ui->tableView_KS_IMG->setModel(this->modelArchive_KS_IMG);
        connect(this->modelArchive_KS_IMG,
                SIGNAL(needScrollToBottom()),
                ui->tableView_KS_IMG,
                SLOT(scrollToBottom()));
        connect(ui->tableView_KS_IMG,
                SIGNAL(doubleClicked(QModelIndex)),
                this->modelArchive_KS_IMG,
                SLOT(emitNeedViewIMG(QModelIndex)));
        connect(this->modelArchive_KS_IMG,
                SIGNAL(newRecord(quint32,quint32)),
                this,
                SLOT(newRecord_KSIMG(quint32,quint32)));

        this->modelArchive_BSK_MSG = new TableModelArchiveMSG(this->m_dbManager,
                                                              BSKSystemType,
                                                              this);
        ui->tableView_BSK_MSG->setModel(this->modelArchive_BSK_MSG);
        connect(this->modelArchive_BSK_MSG,
                SIGNAL(needScrollToBottom()),
                ui->tableView_BSK_MSG,
                SLOT(scrollToBottom()));

        this->modelTreeUN_KM = new TreeModelUnitNode(this->m_kmManager,
                                                     this->m_ksManager,
                                                     this->m_bskManager,
                                                     this->m_dbManager,
                                                     KMSystemType,
                                                     this);
        ui->treeView_UN_KM->setModel(this->modelTreeUN_KM);
        connect(this->m_kmManager,
                SIGNAL(addNewUN(UnitNode*)),
                this->modelTreeUN_KM,
                SLOT(appendNewUNInStructure(UnitNode*)));
        connect(this->m_kmManager,
                SIGNAL(updateUN(UnitNode*)),
                this->modelTreeUN_KM,
                SLOT(updateUNStructure(UnitNode*)));
    //    connect(this->m_kmManager,
    //            SIGNAL(moveUN(UnitNode*,UnitNode*,int,UnitNode*,int)),
    //            this->modelTreeUN_KM,
    //            SLOT(moveUNStructure(UnitNode*,UnitNode*,int,UnitNode*,int)));

        connect(this->m_kmManager,
                SIGNAL(updateUN(UnitNode*)),
                ui->treeView_UN_KM,
                SLOT(expandAll()));
        connect(this->m_kmManager,
                SIGNAL(addNewUN(UnitNode*)),
                ui->treeView_UN_KM,
                SLOT(expandAll()));

    //    QHeaderView *headerView_TreeView_UN_KM(ui->treeView_UN_KM->header());
    //    connect(headerView_TreeView_UN_KM,
    //            SIGNAL(sortIndicatorChanged(int,Qt::SortOrder)),
    //            this->modelTreeUN_KM,
    //            SLOT(sort(int,Qt::SortOrder)));



        this->modelTableUN_KM = new TableModelUnitNode(this->m_kmManager,
                                                       this->m_ksManager,
                                                       this->m_bskManager,
                                                       this->m_dbManager,
                                                       KMSystemType,
                                                       this);
        ui->tableView_UN_KM->setModel(this->modelTableUN_KM);
        connect(this->m_kmManager,
                SIGNAL(updateUN(UnitNode*)),
                this->modelTableUN_KM,
                SLOT(updateUN(UnitNode*)));
        connect(this->m_kmManager,
                SIGNAL(addNewUN(UnitNode*)),
                this->modelTableUN_KM,
                SLOT(updateUN(UnitNode*)));
        connect(this->modelTableUN_KM,
                SIGNAL(needSelectRow(int)),
                ui->tableView_UN_KM,
                SLOT(selectRow(int)));

        QHeaderView *headerView_TableView_UN_KM(ui->tableView_UN_KM->horizontalHeader());
        connect(headerView_TableView_UN_KM,
                SIGNAL(sortIndicatorChanged(int,Qt::SortOrder)),
                this->modelTableUN_KM,
                SLOT(sort(int,Qt::SortOrder)));


        this->modelTreeUN_KS = new TreeModelUnitNode(this->m_kmManager,
                                                     this->m_ksManager,
                                                     this->m_bskManager,
                                                     this->m_dbManager,
                                                     KSSystemType,
                                                     this);
        ui->treeView_UN_KS->setModel(this->modelTreeUN_KS);
        connect(this->m_ksManager,
                SIGNAL(addNewUN(UnitNode*)),
                this->modelTreeUN_KS,
                SLOT(appendNewUNInStructure(UnitNode*)));
        connect(this->m_ksManager,
                SIGNAL(updateUN(UnitNode*)),
                this->modelTreeUN_KS,
                SLOT(updateUNStructure(UnitNode*)));
    //    connect(this->m_ksManager,
    //            SIGNAL(moveUN(UnitNode*,UnitNode*,int,UnitNode*,int)),
    //            this->modelTreeUN_KS,
    //            SLOT(moveUNStructure(UnitNode*,UnitNode*,int,UnitNode*,int)));

        connect(this->m_ksManager,
                SIGNAL(updateUN(UnitNode*)),
                ui->treeView_UN_KS,
                SLOT(expandAll()));
        connect(this->m_ksManager,
                SIGNAL(addNewUN(UnitNode*)),
                ui->treeView_UN_KS,
                SLOT(expandAll()));

    //    QHeaderView *headerView_TreeView_UN_KS(ui->treeView_UN_KS->header());
    //    connect(headerView_TreeView_UN_KS,
    //            SIGNAL(sortIndicatorChanged(int,Qt::SortOrder)),
    //            this->modelTreeUN_KS,
    //            SLOT(sort(int,Qt::SortOrder)));

        this->modelTableUN_KS = new TableModelUnitNode(this->m_kmManager,
                                                       this->m_ksManager,
                                                       this->m_bskManager,
                                                       this->m_dbManager,
                                                       KSSystemType,
                                                       this);
        ui->tableView_UN_KS->setModel(this->modelTableUN_KS);
        connect(this->m_ksManager,
                SIGNAL(updateUN(UnitNode*)),
                this->modelTableUN_KS,
                SLOT(updateUN(UnitNode*)));
        connect(this->m_ksManager,
                SIGNAL(addNewUN(UnitNode*)),
                this->modelTableUN_KS,
                SLOT(updateUN(UnitNode*)));
        connect(this->modelTableUN_KS,
                SIGNAL(needSelectRow(int)),
                ui->tableView_UN_KS,
                SLOT(selectRow(int)));

        connect(this->m_ksManager,
                SIGNAL(updateUN(UnitNode*)),
                this,
                SLOT(possUpdateToolDialog_KS(UnitNode*)));

        QHeaderView *headerView_TableView_UN_KS(ui->tableView_UN_KS->horizontalHeader());
        connect(headerView_TableView_UN_KS,
                SIGNAL(sortIndicatorChanged(int,Qt::SortOrder)),
                this->modelTableUN_KS,
                SLOT(sort(int,Qt::SortOrder)));

        this->modelTreeUN_BSK = new TreeModelUnitNode(this->m_kmManager,
                                                      this->m_ksManager,
                                                      this->m_bskManager,
                                                      this->m_dbManager,
                                                      BSKSystemType,
                                                      this);
        ui->treeView_UN_BSK->setModel(this->modelTreeUN_BSK);
        connect(this->m_bskManager,
                SIGNAL(addNewUN(UnitNode*)),
                this->modelTreeUN_BSK,
                SLOT(appendNewUNInStructure(UnitNode*)));
        connect(this->m_bskManager,
                SIGNAL(updateUN(UnitNode*)),
                this->modelTreeUN_BSK,
                SLOT(updateUNStructure(UnitNode*)));
    //    connect(this->m_bskManager,
    //            SIGNAL(moveUN(UnitNode*,UnitNode*,int,UnitNode*,int)),
    //            this->modelTreeUN_BSK,
    //            SLOT(moveUNStructure(UnitNode*,UnitNode*,int,UnitNode*,int)));

        connect(this->m_bskManager,
                SIGNAL(updateUN(UnitNode*)),
                ui->treeView_UN_BSK,
                SLOT(expandAll()));
        connect(this->m_bskManager,
                SIGNAL(addNewUN(UnitNode*)),
                ui->treeView_UN_BSK,
                SLOT(expandAll()));

    //    QHeaderView *headerView_TreeView_UN_BSK(ui->treeView_UN_BSK->header());
    //    connect(headerView_TreeView_UN_BSK,
    //            SIGNAL(sortIndicatorChanged(int,Qt::SortOrder)),
    //            this->modelTreeUN_BSK,
    //            SLOT(sort(int,Qt::SortOrder)));

        this->modelTableUN_BSK = new TableModelUnitNode(this->m_kmManager,
                                                        this->m_ksManager,
                                                        this->m_bskManager,
                                                        this->m_dbManager,
                                                        BSKSystemType,
                                                        this);
        ui->tableView_UN_BSK->setModel(this->modelTableUN_BSK);
        connect(this->m_bskManager,
                SIGNAL(updateUN(UnitNode*)),
                this->modelTableUN_BSK,
                SLOT(updateUN(UnitNode*)));
        connect(this->m_bskManager,
                SIGNAL(addNewUN(UnitNode*)),
                this->modelTableUN_BSK,
                SLOT(updateUN(UnitNode*)));
        connect(this->modelTableUN_BSK,
                SIGNAL(needSelectRow(int)),
                ui->tableView_UN_BSK,
                SLOT(selectRow(int)));

        QHeaderView *headerView_TableView_UN_BSK(ui->tableView_UN_BSK->horizontalHeader());
        connect(headerView_TableView_UN_BSK,
                SIGNAL(sortIndicatorChanged(int,Qt::SortOrder)),
                this->modelTableUN_BSK,
                SLOT(sort(int,Qt::SortOrder)));

    //    connect(this->m_kmManager,
    //            SIGNAL(moveUNStructure(UnitNode*,
    //                                   UnitNode*,
    //                                   int,
    //                                   UnitNode*,
    //                                   int)),
    //            ui->treeView_UN_KM,
    //            SLOT(expandAll()));

    //    this->mapManager = new GraphicsMapManager(this->m_kmManager,
    //                                              this->m_ksManager,
    //                                              this->m_bskManager,
    //                                              this->m_dbManager,
    //                                              BSKSystemType,
    //                                              this);
    //    ui->graphicsView_MAP->setScene(this->mapManager->mapScene);
    //    connect(this->mapManager,
    //            SIGNAL(needUpdate()),
    //            ui->graphicsView_MAP,
    //            SLOT(update()));
    //    connect(this->mapManager,
    //            SIGNAL(needUpdate()),
    //            ui->graphicsView_MAP,
    //            SLOT(repaint()));

        //
        this->mapManager_KM = new GraphicsMapManager(this->m_kmManager,
                                                     this->m_ksManager,
                                                     this->m_bskManager,
                                                     this->m_dbManager,
                                                     KMSystemType,
                                                     this);
        ui->graphicsView_MAP_KM->setScene(this->mapManager_KM->mapScene);

        this->mapManager_KS = new GraphicsMapManager(this->m_kmManager,
                                                     this->m_ksManager,
                                                     this->m_bskManager,
                                                     this->m_dbManager,
                                                     KSSystemType,
                                                     this);
        ui->graphicsView_MAP_KS->setScene(this->mapManager_KS->mapScene);

        this->mapManager_BSK = new GraphicsMapManager(this->m_kmManager,
                                                      this->m_ksManager,
                                                      this->m_bskManager,
                                                      this->m_dbManager,
                                                      BSKSystemType,
                                                      this);
        ui->graphicsView_MAP_BSK->setScene(this->mapManager_BSK->mapScene);
        //

    //    ui->graphicsView_MAP->setScene(this->mapManager);

        imgScene = new QGraphicsScene(this);
        imgScene->setBackgroundBrush(QColor(255, 255, 255, 127));
        imgScene->setBackgroundBrush(QBrush(Qt::lightGray, Qt::CrossPattern));
        ui->graphicsView_IMG->setScene(imgScene);
        mainIMG = new QGraphicsPixmapItem;
        if(!imgScene->items().contains(mainIMG))
        {
            imgScene->addItem(mainIMG);
        }

        connect(this->mapManager_KM,
                SIGNAL(changeEditStatus(bool)),
                ui->pushButton_editMode_MAP_KM,
                SLOT(setChecked(bool)));
        connect(this->mapManager_KS,
                SIGNAL(changeEditStatus(bool)),
                ui->pushButton_editMode_MAP_KS,
                SLOT(setChecked(bool)));
        connect(this->mapManager_BSK,
                SIGNAL(changeEditStatus(bool)),
                ui->pushButton_editMode_MAP_BSK,
                SLOT(setChecked(bool)));


        //
        QPixmap basePXM(100, 100);
        basePXM.fill(Qt::lightGray);
        bpIMG1 =
        bpIMG2 =
        bpIMG3 =
        bpIMG4 =
        bpIMG5 = basePXM;
        ui->pushButton_IMG1->setIconSize(ui->pushButton_IMG1->size());
        ui->pushButton_IMG1->setIcon(QIcon(bpIMG1));
        ui->pushButton_IMG2->setIconSize(ui->pushButton_IMG2->size());
        ui->pushButton_IMG2->setIcon(QIcon(bpIMG2));
        ui->pushButton_IMG3->setIconSize(ui->pushButton_IMG3->size());
        ui->pushButton_IMG3->setIcon(QIcon(bpIMG3));
        ui->pushButton_IMG4->setIconSize(ui->pushButton_IMG4->size());
        ui->pushButton_IMG4->setIcon(QIcon(bpIMG4));
        ui->pushButton_IMG5->setIconSize(ui->pushButton_IMG5->size());
        ui->pushButton_IMG5->setIcon(QIcon(bpIMG5));

        //
        connect(this->modelArchive_KM_IMG,
                SIGNAL(needViewIMG(quint32,int)),
                this,
                SLOT(setIMGScene(quint32,int)));
        //
        connect(this->mapManager_KM,
                SIGNAL(selectUN(UnitNode*)),
                this,
                SLOT(setInitiatorUN(UnitNode*)));
        connect(this->mapManager_KS,
                SIGNAL(selectUN(UnitNode*)),
                this,
                SLOT(setInitiatorUN(UnitNode*)));
        connect(this->mapManager_BSK,
                SIGNAL(selectUN(UnitNode*)),
                this,
                SLOT(setInitiatorUN(UnitNode*)));
        //
        connect(ui->treeView_UN_KM,
                SIGNAL(clicked(QModelIndex)),
                this->modelTreeUN_KM,
                SLOT(clickedUN(QModelIndex)));
        connect(this->modelTreeUN_KM,
                SIGNAL(selectedUN(UnitNode*)),
                this,
                SLOT(setInitiatorUN(UnitNode*)));
        connect(this->modelTreeUN_KM,
                SIGNAL(selectedUN(UnitNode*)),
                this->mapManager_KM,
                SLOT(selectedUN(UnitNode*)));

        connect(ui->tableView_UN_KM,
                SIGNAL(clicked(QModelIndex)),
                this->modelTableUN_KM,
                SLOT(clickedUN(QModelIndex)));
        connect(this->modelTableUN_KM,
                SIGNAL(selectedUN(UnitNode*)),
                this,
                SLOT(setInitiatorUN(UnitNode*)));
        connect(this->modelTableUN_KM,
                SIGNAL(selectedUN(UnitNode*)),
                this->mapManager_KM,
                SLOT(selectedUN(UnitNode*)));
        //

        //
        connect(this->modelArchive_KS_IMG,
                SIGNAL(needViewIMG(quint32,int)),
                this,
                SLOT(setIMGScene(quint32,int)));

        //
        connect(ui->treeView_UN_KS,
                SIGNAL(clicked(QModelIndex)),
                this->modelTreeUN_KS,
                SLOT(clickedUN(QModelIndex)));
        connect(this->modelTreeUN_KS,
                SIGNAL(selectedUN(UnitNode*)),
                this,
                SLOT(setInitiatorUN(UnitNode*)));
        connect(this->modelTreeUN_KS,
                SIGNAL(selectedUN(UnitNode*)),
                this->mapManager_KS,
                SLOT(selectedUN(UnitNode*)));

        connect(ui->tableView_UN_KS,
                SIGNAL(clicked(QModelIndex)),
                this->modelTableUN_KS,
                SLOT(clickedUN(QModelIndex)));
        connect(this->modelTableUN_KS,
                SIGNAL(selectedUN(UnitNode*)),
                this,
                SLOT(setInitiatorUN(UnitNode*)));
        connect(this->modelTableUN_KS,
                SIGNAL(selectedUN(UnitNode*)),
                this->mapManager_KS,
                SLOT(selectedUN(UnitNode*)));

        connect(ui->treeView_UN_BSK,
                SIGNAL(clicked(QModelIndex)),
                this->modelTreeUN_BSK,
                SLOT(clickedUN(QModelIndex)));
        connect(this->modelTreeUN_BSK,
                SIGNAL(selectedUN(UnitNode*)),
                this,
                SLOT(setInitiatorUN(UnitNode*)));
        connect(this->modelTreeUN_BSK,
                SIGNAL(selectedUN(UnitNode*)),
                this->mapManager_BSK,
                SLOT(selectedUN(UnitNode*)));

        connect(ui->tableView_UN_BSK,
                SIGNAL(clicked(QModelIndex)),
                this->modelTableUN_BSK,
                SLOT(clickedUN(QModelIndex)));
        connect(this->modelTableUN_BSK,
                SIGNAL(selectedUN(UnitNode*)),
                this,
                SLOT(setInitiatorUN(UnitNode*)));
        connect(this->modelTableUN_BSK,
                SIGNAL(selectedUN(UnitNode*)),
                this->mapManager_BSK,
                SLOT(selectedUN(UnitNode*)));
        //

    //    connect(this->m_kmManager,
    //            SIGNAL(currentWorkModeChanged(int)),
    //            this,
    //            SLOT(updateStatusMode(qint32)));

        connect(this->m_kmManager,
                SIGNAL(insertNewMSG(quint32)),
                this,
                SLOT(updateStatusMSG(quint32)));

        connect(this->m_kmManager,
                SIGNAL(updateProgressShotLoad(UnitNode*,quint8,quint8,quint8)),
                this,
                SLOT(updateStatusProgress(UnitNode*,quint8,quint8,quint8)));

        connect(this->m_ksManager,
                SIGNAL(insertNewMSG(quint32)),
                this,
                SLOT(updateStatusMSG(quint32)));

    //    connect(this->m_ksManager,
    //            SIGNAL(progress14(UnitNode*,quint8,quint8,quint8)),
    //            this,
    //            SLOT(updateStatusProgress(UnitNode*,quint8,quint8,quint8)));

    //    connect(this->m_ksManager,
    //            SIGNAL(progress24(UnitNode*,quint8,quint8,quint8)),
    //            this,
    //            SLOT(updateStatusProgress(UnitNode*,quint8,quint8,quint8)));

        connect(this->m_ksManager,
                SIGNAL(updateProgressShotLoad(UnitNode*,quint8,quint8,quint8)),
                this,
                SLOT(updateStatusProgress(UnitNode*,quint8,quint8,quint8)));

        this->m_kmManager->m_btManager->setInitialListUN();
        this->m_ksManager->m_ksMsgManager->setInitialListUN();
        this->m_bskManager->m_bskMsgManager->setInitialListUN();

        this->modelTableUN_KM->updateListItem();
        this->modelTableUN_KS->updateListItem();
        this->modelTableUN_BSK->updateListItem();

        this->mapManager_KM->updateScene();
        this->mapManager_KS->updateScene();
        this->mapManager_BSK->updateScene();





        //всплывающие окна
        dialog_SerialPortSetting = new DialogSerialPortSetting(this);
    //    dialog_SerialPortSetting->setModal(true);
    //    dialog_SerialPortSetting->setWindowModality(Qt::ApplicationModal);
    //    dialog_SerialPortSetting->setParent(this);
    //    dialog_SerialPortSetting->setWindowModality(Qt::WindowModal);
        connect(dialog_SerialPortSetting,
                SIGNAL(startKM(QString)),
                m_kmManager,
                SLOT(openPort(QString)));
        connect(m_kmManager,
                SIGNAL(updatePortSetting(QList<QString>)),
                dialog_SerialPortSetting,
                SLOT(viewKMSetting(QList<QString>)));
        connect(dialog_SerialPortSetting,
                SIGNAL(stopKM()),
                m_kmManager,
                SLOT(closePort()));

        connect(dialog_SerialPortSetting,
                SIGNAL(startKS(QString)),
                m_ksManager,
                SLOT(openPort(QString)));
        connect(m_ksManager,
                SIGNAL(updatePortSetting(QList<QString>)),
                dialog_SerialPortSetting,
                SLOT(viewKSSetting(QList<QString>)));
        connect(dialog_SerialPortSetting,
                SIGNAL(stopKS()),
                m_ksManager,
                SLOT(closePort()));

        connect(dialog_SerialPortSetting,
                SIGNAL(startBSK(QString)),
                m_bskManager,
                SLOT(openPort(QString)));
        connect(m_bskManager,
                SIGNAL(updatePortSetting(QList<QString>)),
                dialog_SerialPortSetting,
                SLOT(viewBSKSetting(QList<QString>)));
        connect(dialog_SerialPortSetting,
                SIGNAL(stopBSK()),
                m_bskManager,
                SLOT(closePort()));

        connect(dialog_SerialPortSetting,
                SIGNAL(startPKP(QString)),
                m_pkpManager,
                SLOT(openPort(QString)));
        connect(m_pkpManager,
                SIGNAL(updatePortSetting(QList<QString>)),
                dialog_SerialPortSetting,
                SLOT(viewPKPSetting(QList<QString>)));
        connect(dialog_SerialPortSetting,
                SIGNAL(stopPKP()),
                m_pkpManager,
                SLOT(closePort()));
        connect(m_pkpManager,
                SIGNAL(newMSGPKP(QByteArray)),
                m_bskManager,
                SLOT(newMSGPKP(QByteArray)));


        dialog_TableArchive = new DialogTableArchive(this->m_dbManager,
                                                     this->m_kmManager,
                                                     this->m_ksManager,
                                                     this->m_bskManager,
                                                     this);
        connect(dialog_TableArchive,
                SIGNAL(imClosed(bool)),
                ui->actionViewArchiveInWindow,
                SLOT(setChecked(bool)));
        connect(this->dialog_TableArchive,
                SIGNAL(needViewIMG(quint32,int)),
                this,
                SLOT(setIMGScene(quint32,int)));

    //    dialog_TableArchive->setModal(true);

        dialog_ImageViwer = new DialogImageViwer(this);
        //всплывающие окна

        // контекстные меню
        this->tableContextMenu = new QMenu(this);
        this->tableContextMenu->addAction(ui->actionSetAutoScroll);
        this->tableContextMenu->addAction(ui->actionViewArchiveInWindow);

        this->tableOrTreeContextMenu = new QMenu(this);
        this->tableOrTreeContextMenu->addAction(ui->actionViewTableOrTree);
        this->tableOrTreeContextMenu->addAction(ui->actionSetAutoScroll);

        this->table_BSK_UN_ContextMenu = new QMenu(this);
        //
        ui->actionName_Target_BSK_UN->setEnabled(false);
        this->table_BSK_UN_ContextMenu->addAction(ui->actionName_Target_BSK_UN);
        //
        connect(ui->actionSwitch_On_BSK,
                SIGNAL(triggered()),
                ui->pushButton_SwitchON_0x41x02_BSK,
                SLOT(click()));
        this->table_BSK_UN_ContextMenu->addAction(ui->actionSwitch_On_BSK);
        //
        connect(ui->actionSwitch_Off_BSK,
                SIGNAL(triggered()),
                ui->pushButton_SwitchOFF_0x41x01_BSK,
                SLOT(click()));
        this->table_BSK_UN_ContextMenu->addAction(ui->actionSwitch_Off_BSK);
        //
        this->table_BSK_UN_ContextMenu->addAction(ui->actionReset_BSK);
        //
        connect(ui->actionSend_DK_BSK,
                SIGNAL(triggered()),
                ui->pushButton_SendDK_0x41x55_BSK,
                SLOT(click()));
        this->table_BSK_UN_ContextMenu->addAction(ui->actionSend_DK_BSK);
        this->table_BSK_UN_ContextMenu->addSeparator();
        this->table_BSK_UN_ContextMenu->addAction(ui->actionHiddingUN);

        this->table_KS_UN_ContextMenu = new QMenu(this);
        //
        ui->actionName_Target_KS_UN->setEnabled(false);
        this->table_KS_UN_ContextMenu->addAction(ui->actionName_Target_KS_UN);
        this->table_KS_UN_ContextMenu->addSeparator();
        this->table_KS_UN_ContextMenu->addAction(ui->actionVK1_FF_Inquiry_KS);
        this->table_KS_UN_ContextMenu->addAction(ui->actionVK1_Mode_1_KS);
        this->table_KS_UN_ContextMenu->addAction(ui->actionVK1_Mode_2_KS);
        this->table_KS_UN_ContextMenu->addSeparator();
        this->table_KS_UN_ContextMenu->addAction(ui->actionVK2_FF_Inquiry_KS);
        this->table_KS_UN_ContextMenu->addAction(ui->actionVK2_Mode_1_KS);
        this->table_KS_UN_ContextMenu->addAction(ui->actionVK2_Mode_2_KS);
        this->table_KS_UN_ContextMenu->addSeparator();
        this->table_KS_UN_ContextMenu->addAction(ui->actionVK1_Test_FF_Inquiry_KS);
        this->table_KS_UN_ContextMenu->addAction(ui->actionVK2_Test_FF_Inquiry_KS);
        this->table_KS_UN_ContextMenu->addSeparator();
        this->table_KS_UN_ContextMenu->addAction(ui->actionHiddingUN);


        this->table_KM_UN_ContextMenu = new QMenu(this);
        //
        ui->actionName_Target_KM_UN->setEnabled(false);
        this->table_KM_UN_ContextMenu->addAction(ui->actionName_Target_KM_UN);
        //
        this->table_KM_UN_ContextMenu->addAction(ui->actionVK1_FF_Inquiry_KM);
        this->table_KM_UN_ContextMenu->addAction(ui->actionVK1_Mode_1_KM);
        this->table_KM_UN_ContextMenu->addAction(ui->actionVK1_Mode_2_KM);
        this->table_KM_UN_ContextMenu->addSeparator();
        this->table_KM_UN_ContextMenu->addAction(ui->actionHiddingUN);


        // контекстные меню

        // строка состояния
        imgProgressBar = new QProgressBar(this);
        imgProgressBar->setMaximumWidth(150);
        workModeLabel = new QLabel;
        msgLabel = new QLabel;
        clockLabel = new QLabel;
        timerClock = new QTimer;
        timerUpdateProgress = new QTimer;
        timer_Request_List_D_VK_KS = new QTimer;

        ui->statusBar->addWidget(clockLabel);
        ui->statusBar->addWidget(workModeLabel);
        ui->statusBar->addWidget(imgProgressBar);
        ui->statusBar->addWidget(msgLabel);

    //    this->updateStatusMSG(0);
        this->updateStatusProgress(0, 0, 0, 0);

        connect(timerUpdateProgress,
                SIGNAL(timeout()),
                this,
                SLOT(hideStatusProgress()));

        connect(timerClock,
                SIGNAL(timeout()),
                this,
                SLOT(updateStatusClock()));
        timerClock->start(100);


        // строка состо¤ни¤

        // панель команд
        this->initiatorUN_KM =
        this->initiatorUN_KS =
        this->initiatorUN_BSK =
        this->initiatorUN =
        this->lastProgressUN = nullptr;
        ui->label_ID_View_KM->setText("***");
        ui->label_Name_View_KM->setText("***");
        ui->tabWidget->setCurrentIndex(2);

        connect(ui->tabWidget,
                SIGNAL(currentChanged(int)),
                this,
                SLOT(tabWidgetCurrentIndexChanged(int)));

        connect(ui->lineEdit_linkConsole,
                SIGNAL(textChanged(QString)),
                this,
                SLOT(updateBTLinkToSend(QString)));

        this->on_actionViewToolDialog_triggered(false);
        ui->tabWidget->setCurrentIndex(1);

        connect(this->m_kmManager,
                SIGNAL(updateSettingUN(UnitNode*,QByteArray)),
                this,
                SLOT(updateSettingUN_KM(UnitNode*,QByteArray)));

    //    ui->stackedWidget_4->setMinimumSize(380,
    //                                        450);
    //    this->tabConsole = ui->tab_Console;
    //    tabConsole->setVisible(false);

        // панель команд

        this->on_actionSetViewAllMSGArchive_triggered();
        this->on_actionSetViewMap_triggered();
        ui->actionViewLastIMG->setChecked(true);
        ui->actionSetAutoScroll->setChecked(true);
        ui->actionViewTableOrTree->setChecked(false);
        this->on_actionViewTableOrTree_triggered(false);
        statusMode = this->m_kmManager->m_btManager->currentWorkMode;

    //    ui->spinBox_0x1F_komplekt;
    //    ui->spinBox_Kit_Num_RM433_KS;

        ui->lineEdit_0x1F_BSK1->setValidator(new QIntValidator(001, 999,ui->lineEdit_0x1F_BSK1));
        ui->lineEdit_0x1F_BSK2->setValidator(new QIntValidator(001, 999,ui->lineEdit_0x1F_BSK2));
        ui->lineEdit_0x1F_BSK3->setValidator(new QIntValidator(001, 999,ui->lineEdit_0x1F_BSK3));
        ui->lineEdit_Add_ID_D_VK_KS->setValidator(new QIntValidator(001, 999,ui->lineEdit_Add_ID_D_VK_KS));
        connect(modelTableUN_KM,
                SIGNAL(needHideRow(int)),
                ui->tableView_UN_KM,
                SLOT(hideRow(int)));
    //    this->on_actionRussian_triggered();

        //
        this->m_bskManager->m_bskMsgManager->beeperFlag =
        this->m_ksManager->m_ksMsgManager->beeperFlag =
        this->m_kmManager->m_btManager->beeperFlag = ui->actionSound_switcher->isChecked();

        ui->actionSetCurrentSystemBSK->trigger();//setChecked(true);

        this->showMaximized();
        this->showNormal();
        this->inicialResize();
        this->uiElementResize();

    #ifdef QT_DEBUG
    #else
        ui->tabWidget->removeTab(2);
        ui->tab_Console->setVisible(false);
    #endif

    //    connect(ui->splitter_main,
    //            SIGNAL(splitterMoved(int,int)),
    //            this,
    //            SLOT(setTableSizeToContents()));
    //    connect(this,
    //            SLOT(resizeEvent(QResizeEvent*)),
    //            this,
    //            SLOT(setTableSizeToContents()));

        ui->actionOperatorWorkMode->trigger();
        while(ui->actionOperatorWorkMode->isChecked())
        {
            ui->actionOperatorWorkMode->trigger();
        }
        this->updateStatusWorkMode(ui->actionOperatorWorkMode->isChecked());

        this->setDefaultTableSize();
    }
    catch (std::runtime_error &rte)
    {
        qWarning("catch runtime_error %s [MainWindow::MainWindow]", rte.what());
    }
    catch (std::bad_alloc& ba)
    {
        qWarning("catch bad_alloc %s [MainWindow::MainWindow]", ba.what());
    }
    catch (std::exception &e)
    {
        qWarning("catch exception %s [MainWindow::MainWindow]", e.what());
    }
    catch (...)
    {
        qWarning("catch exception <unknown> [MainWindow::MainWindow]");
    }
}

MainWindow::~MainWindow() /*noexcept*/
{
    try
    {
    //    this->mapManager->zoomX1();

        delete tableContextMenu;
        delete tableOrTreeContextMenu;
        delete dialog_SerialPortSetting;
        delete dialog_TableArchive;
        delete dialog_ImageViwer;

        ui->graphicsView_MAP_KM->setScene(nullptr);
        delete mapManager_KM;
        ui->graphicsView_MAP_KS->setScene(nullptr);
        delete mapManager_KS;
        ui->graphicsView_MAP_BSK->setScene(nullptr);
        delete mapManager_BSK;
        delete modelTreeUN_KM;
        delete modelTableUN_KM;
        delete modelArchive_KM_IMG;
        delete modelArchive_KM_MSG;
        delete modelArchive_KS_MSG;
        delete modelArchive_KS_IMG;
        delete m_kmManager;
        delete m_ksManager;
        delete m_bskManager;

        timerClock->stop();
        delete timerClock;
        delete workModeLabel;
        delete clockLabel;
        delete msgLabel;
        delete imgProgressBar;

        delete mainIMG;
        delete imgScene;
        delete ui;

        delete m_dbManager;
    }
    catch(...)
    {
#ifdef QT_DEBUG
        qDebug() << "Assert(~MainWindow)";
#endif
        return;
    }
}

void MainWindow::inicialResize() /*noexcept*/
{


    int w3(ui->splitter_horizontal->width() / 3),
        h5(ui->splitter_vertical->height() / 5);

    QList<int> lw, lh;
    lw.append(w3 * 2 /*- 6*/);
    lw.append(w3);

    lh.append(h5);
    lh.append(h5 * 4 /*- 6*/);

    ui->splitter_horizontal->setSizes(lw);
    ui->splitter_vertical->setSizes(lh);
}

void MainWindow::uiTableResize() /*noexcept*/
{
#ifdef QT_DEBUG
    qDebug() << "uiTableResize() --> ";
#endif

    ui->treeView_UN_KM->resizeColumnToContents(0);
    ui->treeView_UN_KM->resizeColumnToContents(1);
    ui->treeView_UN_KM->resizeColumnToContents(2);
    ui->treeView_UN_KM->resizeColumnToContents(3);
#ifdef QT_DEBUG
    qDebug() << "treeView_UN_KM "
             << ui->treeView_UN_KM->columnWidth(0) << " "
             << ui->treeView_UN_KM->columnWidth(1) << " "
             << ui->treeView_UN_KM->columnWidth(2) << " "
             << ui->treeView_UN_KM->columnWidth(4);
#endif
    ui->tableView_UN_KM->resizeColumnsToContents();
#ifdef QT_DEBUG
    qDebug() << "tableView_UN_KM "
             << ui->tableView_UN_KM->columnWidth(0) << " "
             << ui->tableView_UN_KM->columnWidth(1) << " "
             << ui->tableView_UN_KM->columnWidth(2) << " "
             << ui->tableView_UN_KM->columnWidth(4);
#endif


    ui->tableView_KM_MSG->resizeColumnsToContents();
#ifdef QT_DEBUG
    qDebug() << "tableView_KM_MSG "
             << ui->tableView_KM_MSG->columnWidth(0) << " "
             << ui->tableView_KM_MSG->columnWidth(1) << " "
             << ui->tableView_KM_MSG->columnWidth(2) << " "
             << ui->tableView_KM_MSG->columnWidth(3) << " "
             << ui->tableView_KM_MSG->columnWidth(4) << " "
             << ui->tableView_KM_MSG->columnWidth(5) << " "
             << ui->tableView_KM_MSG->columnWidth(6) << " "
             << ui->tableView_KM_MSG->columnWidth(7);
#endif

    ui->tableView_KM_IMG->resizeColumnToContents(0);
    ui->tableView_KM_IMG->resizeColumnToContents(1);
    ui->tableView_KM_IMG->resizeColumnToContents(2);
    ui->tableView_KM_IMG->resizeColumnToContents(3);

    ui->tableView_KM_IMG->setColumnWidth(4, 80);
    ui->tableView_KM_IMG->setColumnWidth(5, 80);
    ui->tableView_KM_IMG->setColumnWidth(6, 80);
    ui->tableView_KM_IMG->setColumnWidth(7, 80);
    ui->tableView_KM_IMG->setColumnWidth(8, 80);
#ifdef QT_DEBUG
    qDebug() << "tableView_KM_IMG "
             << ui->tableView_KM_IMG->columnWidth(0) << " "
             << ui->tableView_KM_IMG->columnWidth(1) << " "
             << ui->tableView_KM_IMG->columnWidth(2) << " "
             << ui->tableView_KM_IMG->columnWidth(3) << " "
             << ui->tableView_KM_IMG->columnWidth(4) << " "
             << ui->tableView_KM_IMG->columnWidth(5) << " "
             << ui->tableView_KM_IMG->columnWidth(6) << " "
             << ui->tableView_KM_IMG->columnWidth(7) << " "
             << ui->tableView_KM_IMG->columnWidth(8);
#endif


    ui->tableView_KM_IMG->resizeRowsToContents();

    ui->treeView_UN_KS->resizeColumnToContents(0);
    ui->treeView_UN_KS->resizeColumnToContents(1);
    ui->treeView_UN_KS->resizeColumnToContents(2);
    ui->treeView_UN_KS->resizeColumnToContents(3);
#ifdef QT_DEBUG
    qDebug() << "treeView_UN_KS "
             << ui->treeView_UN_KS->columnWidth(0) << " "
             << ui->treeView_UN_KS->columnWidth(1) << " "
             << ui->treeView_UN_KS->columnWidth(2) << " "
             << ui->treeView_UN_KS->columnWidth(4);
#endif
    ui->tableView_UN_KS->resizeColumnsToContents();
#ifdef QT_DEBUG
    qDebug() << "tableView_UN_KS "
             << ui->tableView_UN_KS->columnWidth(0) << " "
             << ui->tableView_UN_KS->columnWidth(1) << " "
             << ui->tableView_UN_KS->columnWidth(2) << " "
             << ui->tableView_UN_KS->columnWidth(4);
#endif


    ui->tableView_KS_MSG->resizeColumnsToContents();
#ifdef QT_DEBUG
    qDebug() << "tableView_KS_MSG "
             << ui->tableView_KS_MSG->columnWidth(0) << " "
             << ui->tableView_KS_MSG->columnWidth(1) << " "
             << ui->tableView_KS_MSG->columnWidth(2) << " "
             << ui->tableView_KS_MSG->columnWidth(3) << " "
             << ui->tableView_KS_MSG->columnWidth(4) << " "
             << ui->tableView_KS_MSG->columnWidth(5) << " "
             << ui->tableView_KS_MSG->columnWidth(6) << " "
             << ui->tableView_KS_MSG->columnWidth(7);
#endif

    ui->tableView_KS_IMG->resizeColumnToContents(0);
    ui->tableView_KS_IMG->resizeColumnToContents(1);
    ui->tableView_KS_IMG->resizeColumnToContents(2);
    ui->tableView_KS_IMG->resizeColumnToContents(3);

    ui->tableView_KS_IMG->setColumnWidth(4, 80);
    ui->tableView_KS_IMG->setColumnWidth(5, 80);
    ui->tableView_KS_IMG->setColumnWidth(6, 80);
    ui->tableView_KS_IMG->setColumnWidth(7, 80);
    ui->tableView_KS_IMG->setColumnWidth(8, 80);
#ifdef QT_DEBUG
    qDebug() << "tableView_KM_IMG "
             << ui->tableView_KM_IMG->columnWidth(0) << " "
             << ui->tableView_KM_IMG->columnWidth(1) << " "
             << ui->tableView_KM_IMG->columnWidth(2) << " "
             << ui->tableView_KM_IMG->columnWidth(3) << " "
             << ui->tableView_KM_IMG->columnWidth(4) << " "
             << ui->tableView_KM_IMG->columnWidth(5) << " "
             << ui->tableView_KM_IMG->columnWidth(6) << " "
             << ui->tableView_KM_IMG->columnWidth(7) << " "
             << ui->tableView_KM_IMG->columnWidth(8);
#endif

    ui->tableView_KS_IMG->resizeRowsToContents();

    ui->treeView_UN_BSK->resizeColumnToContents(0);
    ui->treeView_UN_BSK->resizeColumnToContents(1);
    ui->treeView_UN_BSK->resizeColumnToContents(2);
    ui->treeView_UN_BSK->resizeColumnToContents(3);
#ifdef QT_DEBUG
    qDebug() << "treeView_UN_BSK "
             << ui->treeView_UN_BSK->columnWidth(0) << " "
             << ui->treeView_UN_BSK->columnWidth(1) << " "
             << ui->treeView_UN_BSK->columnWidth(2) << " "
             << ui->treeView_UN_BSK->columnWidth(4);
#endif
    ui->tableView_UN_BSK->resizeColumnsToContents();
#ifdef QT_DEBUG
    qDebug() << "tableView_UN_BSK "
             << ui->tableView_UN_BSK->columnWidth(0) << " "
             << ui->tableView_UN_BSK->columnWidth(1) << " "
             << ui->tableView_UN_BSK->columnWidth(2) << " "
             << ui->tableView_UN_BSK->columnWidth(4);
#endif


    ui->tableView_BSK_MSG->resizeColumnsToContents();
#ifdef QT_DEBUG
    qDebug() << "tableView_BSK_MSG "
             << ui->tableView_BSK_MSG->columnWidth(0) << " "
             << ui->tableView_BSK_MSG->columnWidth(1) << " "
             << ui->tableView_BSK_MSG->columnWidth(2) << " "
             << ui->tableView_BSK_MSG->columnWidth(3) << " "
             << ui->tableView_BSK_MSG->columnWidth(4) << " "
             << ui->tableView_BSK_MSG->columnWidth(5) << " "
             << ui->tableView_BSK_MSG->columnWidth(6) << " "
             << ui->tableView_BSK_MSG->columnWidth(7);
#endif

#ifdef QT_DEBUG
    qDebug() << "uiTableResize() <-- ";
#endif

}

void MainWindow::setTableSizeToContents() /*noexcept*/
{
    //KM MSG -->
    ui->tableView_KM_MSG->resizeColumnToContents(0);
    ui->tableView_KM_MSG->resizeColumnToContents(1);
    ui->tableView_KM_MSG->resizeColumnToContents(2);
    ui->tableView_KM_MSG->resizeColumnToContents(3);
    ui->tableView_KM_MSG->resizeColumnToContents(5);
    ui->tableView_KM_MSG->resizeColumnToContents(6);
    ui->tableView_KM_MSG->resizeColumnToContents(7);
    int commentColumnWidthtKM(ui->tableView_KM_MSG->width());
    commentColumnWidthtKM -= ui->tableView_KM_MSG->columnWidth(0);
    commentColumnWidthtKM -= ui->tableView_KM_MSG->columnWidth(1);
    commentColumnWidthtKM -= ui->tableView_KM_MSG->columnWidth(2);
    commentColumnWidthtKM -= ui->tableView_KM_MSG->columnWidth(3);
    commentColumnWidthtKM -= ui->tableView_KM_MSG->columnWidth(5);
    commentColumnWidthtKM -= ui->tableView_KM_MSG->columnWidth(6);
    commentColumnWidthtKM -= ui->tableView_KM_MSG->columnWidth(7);
    commentColumnWidthtKM -= 20;
//    if(700 > commentColumnWidthtKM)
//    {
//        ui->tableView_KM_MSG->resizeColumnToContents(4);
//    }
//    else
//    {
        ui->tableView_KM_MSG->setColumnWidth(4, commentColumnWidthtKM);
//    }
    //KM MSG <--

    //KS MSG -->
    ui->tableView_KS_MSG->resizeColumnToContents(0);
    ui->tableView_KS_MSG->resizeColumnToContents(1);
    ui->tableView_KS_MSG->resizeColumnToContents(2);
    ui->tableView_KS_MSG->resizeColumnToContents(3);
    ui->tableView_KS_MSG->resizeColumnToContents(5);
    ui->tableView_KS_MSG->resizeColumnToContents(6);
    ui->tableView_KS_MSG->resizeColumnToContents(7);
    int commentColumnWidthtKS(ui->tableView_KS_MSG->width());
    commentColumnWidthtKS -= ui->tableView_KS_MSG->columnWidth(0);
    commentColumnWidthtKS -= ui->tableView_KS_MSG->columnWidth(1);
    commentColumnWidthtKS -= ui->tableView_KS_MSG->columnWidth(2);
    commentColumnWidthtKS -= ui->tableView_KS_MSG->columnWidth(3);
    commentColumnWidthtKS -= ui->tableView_KS_MSG->columnWidth(5);
    commentColumnWidthtKS -= ui->tableView_KS_MSG->columnWidth(6);
    commentColumnWidthtKS -= ui->tableView_KS_MSG->columnWidth(7);
    commentColumnWidthtKS -= 20;
//    if(700 > commentColumnWidthtKS)
//    {
//        ui->tableView_KS_MSG->resizeColumnToContents(4);
//    }
//    else
//    {
        ui->tableView_KS_MSG->setColumnWidth(4, commentColumnWidthtKS);
//    }
    //KS MSG <--

    //BSK MSG -->
    ui->tableView_BSK_MSG->resizeColumnToContents(0);
    ui->tableView_BSK_MSG->resizeColumnToContents(1);
    ui->tableView_BSK_MSG->resizeColumnToContents(2);
    ui->tableView_BSK_MSG->resizeColumnToContents(3);
    ui->tableView_BSK_MSG->resizeColumnToContents(5);
    ui->tableView_BSK_MSG->resizeColumnToContents(6);
    ui->tableView_BSK_MSG->resizeColumnToContents(7);
    int commentColumnWidthtBSK(ui->tableView_BSK_MSG->width());
    commentColumnWidthtBSK -= ui->tableView_BSK_MSG->columnWidth(0);
    commentColumnWidthtBSK -= ui->tableView_BSK_MSG->columnWidth(1);
    commentColumnWidthtBSK -= ui->tableView_BSK_MSG->columnWidth(2);
    commentColumnWidthtBSK -= ui->tableView_BSK_MSG->columnWidth(3);
    commentColumnWidthtBSK -= ui->tableView_BSK_MSG->columnWidth(5);
    commentColumnWidthtBSK -= ui->tableView_BSK_MSG->columnWidth(6);
    commentColumnWidthtBSK -= ui->tableView_BSK_MSG->columnWidth(7);
    commentColumnWidthtBSK -= 20;
//    if(700 > commentColumnWidthtBSK)
//    {
//        ui->tableView_BSK_MSG->resizeColumnToContents(4);
//    }
//    else
//    {
        ui->tableView_BSK_MSG->setColumnWidth(4, commentColumnWidthtBSK);
//    }
    //BSK MSG <--
}

void MainWindow::setDefaultTableSize() /*noexcept*/
{
    ui->treeView_UN_KM->setColumnWidth(0, 150);
    ui->treeView_UN_KM->setColumnWidth(1, 65);
    ui->treeView_UN_KM->setColumnWidth(2, 75);
    ui->treeView_UN_KM->setColumnWidth(3, 90);//(3, 0);
    ui->treeView_UN_KM->setMinimumWidth(150 + 65 + 75 + 90);

    ui->tableView_UN_KM->setColumnWidth(0, 105);
    ui->tableView_UN_KM->setColumnWidth(1, 65);
    ui->tableView_UN_KM->setColumnWidth(2, 75);
    ui->tableView_UN_KM->setColumnWidth(3, 90);//(3, 0);
    ui->tableView_UN_KM->setMinimumWidth(105 + 65 + 75 + 90);

    ui->tableView_KM_MSG->setColumnWidth(0, 55);
    ui->tableView_KM_MSG->setColumnWidth(1, 90);
    ui->tableView_KM_MSG->setColumnWidth(2, 95);
    ui->tableView_KM_MSG->setColumnWidth(3, 65);
    ui->tableView_KM_MSG->setColumnWidth(4, 85);//550
    ui->tableView_KM_MSG->setColumnWidth(5, 75);
    ui->tableView_KM_MSG->setColumnWidth(6, 75);

    int commentColumnWidthtKM(ui->tableView_KM_MSG->columnWidth(7));
    ui->tableView_KM_MSG->setColumnWidth(4, commentColumnWidthtKM);
    ui->tableView_KM_MSG->setColumnWidth(7, 85);


    ui->tableView_KM_IMG->setColumnWidth(0, 55);
    ui->tableView_KM_IMG->setColumnWidth(1, 105);
    ui->tableView_KM_IMG->setColumnWidth(2, 75);
    ui->tableView_KM_IMG->setColumnWidth(3, 75);
    ui->tableView_KM_IMG->setColumnWidth(4, 80);
    ui->tableView_KM_IMG->setColumnWidth(5, 80);
    ui->tableView_KM_IMG->setColumnWidth(6, 80);
    ui->tableView_KM_IMG->setColumnWidth(7, 80);
    ui->tableView_KM_IMG->setColumnWidth(8, 80);

    for(int i(0), n(ui->tableView_KM_IMG->model()->rowCount()); i < n; i++)
    {
        ui->tableView_KM_IMG->setRowHeight(i, 60);
    }

    ui->treeView_UN_KS->setColumnWidth(0, 150);
    ui->treeView_UN_KS->setColumnWidth(1, 65);
    ui->treeView_UN_KS->setColumnWidth(2, 75);
    ui->treeView_UN_KS->setColumnWidth(3, 50);//(3, 0);
    ui->treeView_UN_KS->setMinimumWidth(150 + 65 + 75 + 50);

    ui->tableView_UN_KS->setColumnWidth(0, 105);
    ui->tableView_UN_KS->setColumnWidth(1, 65);
    ui->tableView_UN_KS->setColumnWidth(2, 75);
    ui->tableView_UN_KS->setColumnWidth(3, 50);//(3, 0);
    ui->tableView_UN_KS->setMinimumWidth(105 + 65 + 75 + 50);


    ui->tableView_KS_MSG->setColumnWidth(0, 55);
    ui->tableView_KS_MSG->setColumnWidth(1, 50);
    ui->tableView_KS_MSG->setColumnWidth(2, 95);
    ui->tableView_KS_MSG->setColumnWidth(3, 65);
    ui->tableView_KS_MSG->setColumnWidth(4, 85);
    ui->tableView_KS_MSG->setColumnWidth(5, 75);
    ui->tableView_KS_MSG->setColumnWidth(6, 75);

    int commentColumnWidthtKS(ui->tableView_KS_MSG->columnWidth(7));
    ui->tableView_KS_MSG->setColumnWidth(4, commentColumnWidthtKS);//492
    ui->tableView_KS_MSG->setColumnWidth(7, 85);

    ui->tableView_KS_IMG->setColumnWidth(0, 55);
    ui->tableView_KS_IMG->setColumnWidth(1, 105);
    ui->tableView_KS_IMG->setColumnWidth(2, 75);
    ui->tableView_KS_IMG->setColumnWidth(3, 75);
    ui->tableView_KS_IMG->setColumnWidth(4, 80);
    ui->tableView_KS_IMG->setColumnWidth(5, 80);
    ui->tableView_KS_IMG->setColumnWidth(6, 80);
    ui->tableView_KS_IMG->setColumnWidth(7, 80);
    ui->tableView_KS_IMG->setColumnWidth(8, 80);

    for(int i(0), n(ui->tableView_KS_IMG->model()->rowCount()); i < n; i++)
    {
        ui->tableView_KS_IMG->setRowHeight(i, 60);
    }

    ui->treeView_UN_BSK->setColumnWidth(0, 150);
    ui->treeView_UN_BSK->setColumnWidth(1, 65);
    ui->treeView_UN_BSK->setColumnWidth(2, 75);
    ui->treeView_UN_BSK->setColumnWidth(3, 50);//(3, 0);
    ui->treeView_UN_BSK->setMinimumWidth(150 + 65 + 75 + 50);

    ui->tableView_UN_BSK->setColumnWidth(0, 105);
    ui->tableView_UN_BSK->setColumnWidth(1, 65);
    ui->tableView_UN_BSK->setColumnWidth(2, 75);
    ui->tableView_UN_BSK->setColumnWidth(3, 50);//(3, 0);
    ui->treeView_UN_BSK->setMinimumWidth(105 + 65 + 75 + 50);

    ui->tableView_BSK_MSG->setColumnWidth(0, 55);
    ui->tableView_BSK_MSG->setColumnWidth(1, 50);
    ui->tableView_BSK_MSG->setColumnWidth(2, 95);
    ui->tableView_BSK_MSG->setColumnWidth(3, 65);
    ui->tableView_BSK_MSG->setColumnWidth(4, 85);//174
    ui->tableView_BSK_MSG->setColumnWidth(5, 75);
    ui->tableView_BSK_MSG->setColumnWidth(6, 75);

    int commentColumnWidthtBSK(ui->tableView_BSK_MSG->columnWidth(7));
    ui->tableView_BSK_MSG->setColumnWidth(4, commentColumnWidthtBSK);
    ui->tableView_BSK_MSG->setColumnWidth(7, 85);
}

void MainWindow::uiElementResize() /*noexcept*/
{
    if(!this->imgRecord.isEmpty() &&
       !mainPXM.isNull())
    {
        scaledMainPXM = mainPXM.scaled(ui->graphicsView_IMG->size(),
                                       Qt::KeepAspectRatio,
                                       Qt::FastTransformation);
        mainIMG->setPixmap(scaledMainPXM);
        mainIMG->setPos(-1 *(scaledMainPXM.width() / 2),
                        -1 *(scaledMainPXM.height() / 2));
    }

    this->fillPushButton_IMG();
}


void MainWindow::resizeEvent(QResizeEvent *e) /*noexcept*/
{
    this->uiElementResize();
//    this->setTableSizeToContents();

    this->setDefaultTableSize();
//    this->uiTableResize();
}

void MainWindow::on_action_viewDialogPortSetting_triggered() /*noexcept*/
{
    this->dialog_SerialPortSetting->show();
}

void MainWindow::on_actionLoadNewMap_triggered() /*noexcept*/
{
    QString fileName;
    //
    fileName = QFileDialog::getOpenFileName(0,
                                            tr("Choosing map dialogue"),//QString::fromLocal8Bit("Диалог выбора подложки карты"),
                                            "",
                                            tr("AllFiles (*.*);;Images (*.jpg *.bmp)"/*;; Database files (*.db *.db3)"*/));
    //
    if(!fileName.isEmpty())
    {
        QFile::remove("map.jpg");
        QImage img;
        img.load(fileName);
        img.save("map.jpg","JPG");
//        QFile::copy(fileName, "map.jpg");
        if(QFile::exists("map.jpg"))
        {
            this->mapManager_KM->setSubstrate("map.jpg");
            this->mapManager_KS->setSubstrate("map.jpg");
            this->mapManager_BSK->setSubstrate("map.jpg");
        }
        else
        {
            this->mapManager_KM->setSubstrate(fileName);
            this->mapManager_KS->setSubstrate(fileName);
            this->mapManager_BSK->setSubstrate(fileName);
        }
    }
}

void MainWindow::pushButton_zoomPlus_MAP() /*noexcept*/
{
    this->mapManager_KM->zoom(true);
    this->mapManager_KS->zoom(true);
    this->mapManager_BSK->zoom(true);
}
void MainWindow::on_pushButton_zoomPlus_MAP_KM_clicked()
{
    this->pushButton_zoomPlus_MAP();
}
void MainWindow::on_pushButton_zoomPlus_MAP_KS_clicked()
{
    this->pushButton_zoomPlus_MAP();
}
void MainWindow::on_pushButton_zoomPlus_MAP_BSK_clicked()
{
    this->pushButton_zoomPlus_MAP();
}

void MainWindow::pushButton_zoomMinus_MAP() /*noexcept*/
{
    this->mapManager_KM->zoom(false);
    this->mapManager_KS->zoom(false);
    this->mapManager_BSK->zoom(false);
}
void MainWindow::on_pushButton_zoomMinus_MAP_KM_clicked()
{
    this->pushButton_zoomMinus_MAP();
}
void MainWindow::on_pushButton_zoomMinus_MAP_KS_clicked()
{
    this->pushButton_zoomMinus_MAP();
}
void MainWindow::on_pushButton_zoomMinus_MAP_BSK_clicked()
{
    this->pushButton_zoomMinus_MAP();
}

void MainWindow::on_actionSetAutoScroll_triggered(bool checked) /*noexcept*/
{
    this->modelArchive_KS_MSG->setNeedScroll(checked);
    this->modelArchive_KS_IMG->setNeedScroll(checked);
    this->modelArchive_KM_MSG->setNeedScroll(checked);
    this->modelArchive_KM_IMG->setNeedScroll(checked);
    if(checked)
    {
        ui->tableView_KM_MSG->scrollToBottom();
        ui->tableView_KM_IMG->scrollToBottom();
        ui->tableView_KS_IMG->scrollToBottom();
        ui->tableView_KS_MSG->scrollToBottom();
        ui->tableView_BSK_MSG->scrollToBottom();
    }
}

//void MainWindow::on_actionSetViewKMMSGArchive_triggered() /*noexcept*/
//{
//    ui->actionSetViewAllMSGArchive->setChecked(false);
//    ui->actionSetViewBTMSGArchive->setChecked(false);
//    ui->actionSetViewIMGArchive->setChecked(false);
//    ui->actionSetViewKMMSGArchive->setChecked(true);

//    if(ui->actionViewArchiveInWindow->isChecked())
//    {
//        ui->stackedWidget_3->setCurrentIndex(0);

//        dialog_TableArchive->setTableModel(0);
//        dialog_TableArchive->show();
//    }
//    else
//    {
//        ui->stackedWidget_3->setCurrentIndex(0);
//    }
//}

//void MainWindow::on_actionSetViewBTMSGArchive_triggered() /*noexcept*/
//{
//    ui->actionSetViewAllMSGArchive->setChecked(false);
//    ui->actionSetViewBTMSGArchive->setChecked(true);
//    ui->actionSetViewIMGArchive->setChecked(false);
//    ui->actionSetViewKMMSGArchive->setChecked(false);

//    if(ui->actionViewArchiveInWindow->isChecked())
//    {
//        ui->stackedWidget_3->setCurrentIndex(1);

//        dialog_TableArchive->setTableModel(1);
//        dialog_TableArchive->show();
//    }
//    else
//    {
//        ui->stackedWidget_3->setCurrentIndex(1);
//    }
//}

void MainWindow::on_actionSetViewAllMSGArchive_triggered() /*noexcept*/
{
    ui->actionSetViewAllMSGArchive->setChecked(true);
    ui->actionSetViewIMGArchive->setChecked(false);

    switch((quint8)this->currentSystemType)
    {
        case (quint8)KMSystemType:
        {
            ui->stackedWidget_3->setCurrentIndex(0);
            break;
        };

        case (quint8)KSSystemType:
        {
            ui->stackedWidget_3->setCurrentIndex(2);
            break;
        };

        case (quint8)BSKSystemType:
        {
            ui->stackedWidget_3->setCurrentIndex(4);
            break;
        };
    }
}

void MainWindow::on_actionSetViewIMGArchive_triggered() /*noexcept*/
{
    ui->actionSetViewAllMSGArchive->setChecked(false);
    ui->actionSetViewIMGArchive->setChecked(true);

    switch((quint8)this->currentSystemType)
    {
        case (quint8)KMSystemType:
        {
            ui->stackedWidget_3->setCurrentIndex(1);
            break;
        };

        case (quint8)KSSystemType:
        {
            ui->stackedWidget_3->setCurrentIndex(3);
            break;
        };

        case (quint8)BSKSystemType:
        {
            ui->stackedWidget_3->setCurrentIndex(4);
            break;
        };
    }
}

void MainWindow::on_actionSetViewIMG_triggered() /*noexcept*/
{
    ui->actionSetViewIMG->setChecked(true);
    ui->actionSetViewMap->setChecked(false);

    ui->stackedWidget_2->setCurrentIndex(0);
}

void MainWindow::on_actionSetViewMap_triggered() /*noexcept*/
{
    ui->actionSetViewIMG->setChecked(false);
    ui->actionSetViewMap->setChecked(true);

    switch((quint8)this->currentSystemType)
    {
        case (quint8)KMSystemType:
        {
            ui->stackedWidget_2->setCurrentIndex(1);
            break;
        };

        case (quint8)KSSystemType:
        {
            ui->stackedWidget_2->setCurrentIndex(2);
            break;
        };

        case (quint8)BSKSystemType:
        {
            ui->stackedWidget_2->setCurrentIndex(3);
            break;
        };
    }
}

void MainWindow::closeEvent(QCloseEvent *event) /*noexcept*/
{
    event->ignore();
    QMessageBox ms;
    ms.setWindowTitle(tr("Closing dialogue"));

    QAbstractButton *yes = ms.addButton(tr("Yes"),QMessageBox::YesRole);
    QAbstractButton *no = ms.addButton(tr("No"),QMessageBox::NoRole);
    ms.setText(tr("Close program?"));
    ms.exec();
    if(ms.clickedButton() == yes)
    {
        event->accept();
    }
    if(ms.clickedButton() == no)
    {
        return;
    }
}

void MainWindow::setIMGScene(quint32 dbIndex, int number) /*noexcept*/
{
    this->imgRecord = m_dbManager->getIMGRecord(dbIndex);
    if(this->imgRecord.isEmpty())
        return;

    this->on_actionSetViewIMG_triggered();
//    ui->stackedWidget_2->setCurrentIndex(1);

    QPixmap basePXM(ui->pushButton_IMG1->size());
    basePXM.fill(Qt::lightGray);
    bpIMG1 =
    bpIMG2 =
    bpIMG3 =
    bpIMG4 =
    bpIMG5 = basePXM;

    ui->pushButton_IMG1->setIconSize(ui->pushButton_IMG1->size());
    ui->pushButton_IMG1->setIcon(QIcon(basePXM));
    ui->pushButton_IMG2->setIconSize(ui->pushButton_IMG2->size());
    ui->pushButton_IMG2->setIcon(QIcon(basePXM));
    ui->pushButton_IMG3->setIconSize(ui->pushButton_IMG3->size());
    ui->pushButton_IMG3->setIcon(QIcon(basePXM));
    ui->pushButton_IMG4->setIconSize(ui->pushButton_IMG4->size());
    ui->pushButton_IMG4->setIcon(QIcon(basePXM));
    ui->pushButton_IMG5->setIconSize(ui->pushButton_IMG5->size());
    ui->pushButton_IMG5->setIcon(QIcon(basePXM));
    bool clickAction(false);
    int recomendAction(0);

    for(int i(0); i < 5; i++)
    {
        QString fieldSizeIMG("sizeIMG%1"),
                fieldDataIMG("dataIMG%1");
        fieldSizeIMG = fieldSizeIMG.arg(i + 1);
        fieldDataIMG = fieldDataIMG.arg(i + 1);
//        qDebug() << "field" << 2 + i * 2 << " " << imgRecord.fieldName(2 + i * 2) << " " << imgRecord.value(2 + i * 2).toUInt();
        if(0 != this->imgRecord.value(fieldSizeIMG).toUInt())
//        if(0 != this->imgRecord.value(2 + i * 2).toUInt())
        {
            QPixmap invalidPXM(ui->pushButton_IMG1->size());
            invalidPXM.fill(Qt::red);
            switch(i)
            {
                case 0:
                {
                    QPixmap pxm;
                    if(pxm.loadFromData(this->imgRecord.value(fieldDataIMG).toByteArray()))
//                    if(pxm.loadFromData(this->imgRecord.value(3 + i * 2).toByteArray()))
                    {
                        if(!pxm.isNull())
                        {
                            bpIMG1 = pxm;
                            ui->pushButton_IMG1->setIcon(QIcon(pxm.scaled(ui->pushButton_IMG1->size(),
                                                                          Qt::KeepAspectRatio,
                                                                          Qt::FastTransformation)));
                            recomendAction = 1;
                            if(1 == number)
                            {
                                ui->pushButton_IMG1->click();
                                clickAction = true;
                            }
                        }
                    }
                    else
                    {
                        bpIMG1 = invalidPXM;
                        ui->pushButton_IMG1->setIcon(QIcon(invalidPXM));
                    }
                    continue;
                }
                case 1:
                {
                    QPixmap pxm;
                    if(pxm.loadFromData(this->imgRecord.value(fieldDataIMG).toByteArray()))
//                    if(pxm.loadFromData(this->imgRecord.value(3 + i * 2).toByteArray()))
                    {
                        if(!pxm.isNull())
                        {
                            bpIMG2 = pxm;
                            ui->pushButton_IMG2->setIcon(QIcon(pxm.scaled(ui->pushButton_IMG2->size(),
                                                                          Qt::KeepAspectRatio,
                                                                          Qt::FastTransformation)));
                            recomendAction = 2;
                            if(2 == number)
                            {
                                ui->pushButton_IMG2->click();
                                clickAction = true;
                            }
                        }
                    }
                    else
                    {
                        bpIMG2 = invalidPXM;
                        ui->pushButton_IMG2->setIcon(QIcon(invalidPXM));
                    }
                    continue;
                }
                case 2:
                {
                    QPixmap pxm;
                    if(pxm.loadFromData(this->imgRecord.value(fieldDataIMG).toByteArray()))
//                    if(pxm.loadFromData(this->imgRecord.value(3 + i * 2).toByteArray()))
                    {
                        if(!pxm.isNull())
                        {
                            bpIMG3 = pxm;
                            ui->pushButton_IMG3->setIcon(QIcon(pxm.scaled(ui->pushButton_IMG3->size(),
                                                                          Qt::KeepAspectRatio,
                                                                          Qt::FastTransformation)));
                            recomendAction = 3;
                            if(3 == number)
                            {
                                ui->pushButton_IMG3->click();
                                clickAction = true;
                            }
                        }
                    }
                    else
                    {
                        bpIMG3 = invalidPXM;
                        ui->pushButton_IMG3->setIcon(QIcon(invalidPXM));
                    }
                    continue;
                }
                case 3:
                {
                    QPixmap pxm;
                    if(pxm.loadFromData(this->imgRecord.value(fieldDataIMG).toByteArray()))
//                    if(pxm.loadFromData(this->imgRecord.value(3 + i * 2).toByteArray()))
                    {
                        if(!pxm.isNull())
                        {
                            bpIMG4 = pxm;
                            ui->pushButton_IMG4->setIcon(QIcon(pxm.scaled(ui->pushButton_IMG4->size(),
                                                                          Qt::KeepAspectRatio,
                                                                          Qt::FastTransformation)));
                            recomendAction = 4;
                            if(4 == number)
                            {
                                ui->pushButton_IMG4->click();
                                clickAction = true;
                            }

                        }
                    }
                    else
                    {
                        bpIMG4 = invalidPXM;
                        ui->pushButton_IMG4->setIcon(QIcon(invalidPXM));
                    }
                    continue;
                }
                case 4:
                {
                    QPixmap pxm;
                    if(pxm.loadFromData(this->imgRecord.value(fieldDataIMG).toByteArray()))
//                    if(pxm.loadFromData(this->imgRecord.value(3 + i * 2).toByteArray()))
                    {
                        if(!pxm.isNull())
                        {
                            bpIMG5 = pxm;
                            ui->pushButton_IMG5->setIcon(QIcon(pxm.scaled(ui->pushButton_IMG5->size(),
                                                                          Qt::KeepAspectRatio,
                                                                          Qt::FastTransformation)));
                            recomendAction = 5;
                            if(5 == number)
                            {
                                ui->pushButton_IMG5->click();
                                clickAction = true;
                            }

                        }
                    }
                    else
                    {
                        bpIMG5 = invalidPXM;
                        ui->pushButton_IMG5->setIcon(QIcon(invalidPXM));
                    }
                    continue;
                }
            }
        }
    }

    if(!clickAction &&
       0 != recomendAction)
    {
        switch(recomendAction)
        {
            case 1:
            {
                ui->pushButton_IMG1->click();
                break;
            };
            case 2:
            {
                ui->pushButton_IMG2->click();
                break;
            };
            case 3:
            {
                ui->pushButton_IMG3->click();
                break;
            };
            case 4:
            {
                ui->pushButton_IMG4->click();
                break;
            };
            case 5:
            {
                ui->pushButton_IMG5->click();
                break;
            };
        }
    }
}

void MainWindow::fillPushButton_IMG() /*noexcept*/
{
    if(!bpIMG1.isNull())
    {
        ui->pushButton_IMG1->setIconSize(ui->pushButton_IMG1->size());
        ui->pushButton_IMG1->setIcon(QIcon(bpIMG1.scaled(ui->pushButton_IMG1->size(),
                                                         Qt::KeepAspectRatio,
                                                         Qt::FastTransformation)));
    }

    if(!bpIMG2.isNull())
    {
        ui->pushButton_IMG2->setIconSize(ui->pushButton_IMG2->size());
        ui->pushButton_IMG2->setIcon(QIcon(bpIMG2.scaled(ui->pushButton_IMG2->size(),
                                                         Qt::KeepAspectRatio,
                                                         Qt::FastTransformation)));
    }

    if(!bpIMG3.isNull())
    {
        ui->pushButton_IMG3->setIconSize(ui->pushButton_IMG3->size());
        ui->pushButton_IMG3->setIcon(QIcon(bpIMG3.scaled(ui->pushButton_IMG3->size(),
                                                         Qt::KeepAspectRatio,
                                                         Qt::FastTransformation)));
    }

    if(!bpIMG4.isNull())
    {
        ui->pushButton_IMG4->setIconSize(ui->pushButton_IMG4->size());
        ui->pushButton_IMG4->setIcon(QIcon(bpIMG4.scaled(ui->pushButton_IMG4->size(),
                                                         Qt::KeepAspectRatio,
                                                         Qt::FastTransformation)));
    }

    if(!bpIMG5.isNull())
    {
        ui->pushButton_IMG1->setIconSize(ui->pushButton_IMG1->size());
        ui->pushButton_IMG1->setIcon(QIcon(bpIMG1.scaled(ui->pushButton_IMG1->size(),
                                                         Qt::KeepAspectRatio,
                                                         Qt::FastTransformation)));
    }

}

QString MainWindow::createTextIMGLable(int numIMG) /*noexcept*/
{
    if(this->imgRecord.isEmpty())
        return QString();

    QString sizeIMG("sizeIMG%1"),
            dataIMG("dataIMG%1");
    sizeIMG = sizeIMG.arg(numIMG);
    dataIMG = dataIMG.arg(numIMG);

    //ВК1 Уч1 Ном1 Тр / ТипСО Уч1 Ном1 / Дата Время
    QString labelStr;

    labelStr.append(tr("VK%1 "));
    labelStr = labelStr.arg(this->imgRecord.value("numberVK").toUInt());

    labelStr.append(tr("S%1 Number%2 "));
    labelStr = labelStr.arg(this->imgRecord.value("area").toUInt())
                       .arg(this->imgRecord.value("number").toUInt());

    switch(numIMG)
    {
        case 1:
        {
            labelStr.append(tr("AftA1 / "));
            break;
        }

        case 2:
        {
            labelStr.append(tr("AftA2 / "));
            break;
        }
        case 3:
        {
            labelStr.append(tr("AftA3 / "));
            break;
        }
        case 4:
        {
            labelStr.append(tr("A / "));
            break;
        }
        case 5:
        {
            labelStr.append(tr("PstA / "));
            break;
        }
    }

    QSqlRecord msg0xC4Record(this->m_dbManager->get_MSGRecord(this->imgRecord.value("foreignIDAlarm").toUInt()));
    if(!msg0xC4Record.isEmpty())
    {
        QByteArray msg0xC4(msg0xC4Record.value("msg").toByteArray());
        QByteArray idSO(msg0xC4.mid(14, 2));

        QByteArray selfD, otherD1, otherD2, operatorD;
        selfD.append((char)0x00);
        selfD.append((quint8)0x0B);
        otherD1.append((char)0x00);
        otherD1.append((quint8)0x01);
        otherD2.append((char)0x00);
        otherD2.append((quint8)0x02);
        operatorD.append((char)0x00);
        operatorD.append((char)0x00);

        if(selfD != idSO &&
           otherD1 != idSO &&
           otherD2 != idSO &&
           operatorD != idSO)
        {
            labelStr.append(tr("%1[%2] "));
            labelStr = labelStr.arg(ProxyMetod().getNameSO(idSO))
                               .arg(QString(idSO.toHex().toUpper()));
        }
        else
        {
            labelStr.append(tr("%1 "));
            labelStr = labelStr.arg(ProxyMetod().getNameSO(idSO));
        }
        labelStr.append("/ ");
    }

//    QSqlRecord msg0x01Record(this->m_dbManager->get_MSGRecord(this->imgRecord.value("foreignIDAlarm").toUInt()));
//    if(!msg0x01Record.isEmpty())
//    {
//        labelStr.append(tr("%1[%2] S%3 Num%4 "));

//        QByteArray msg0x01(msg0x01Record.value("msg").toByteArray());

//        labelStr = labelStr.arg(ProxyMetod().getNameSO(msg0x01.mid(12, 2)))
//                           .arg(QString(msg0x01.mid(12, 2).toHex().toUpper()))
//                           .arg((quint8)msg0x01.at(14))
//                           .arg((quint8)msg0x01.at(15));

//        labelStr.append("/ ");
//    }

    labelStr.append("%1 %2");

    labelStr = labelStr.arg(this->imgRecord.value("date").toDate().toString("dd.MM.yy"))
                       .arg(this->imgRecord.value("time").toTime().toString("hh:mm:ss"));

    return labelStr;
}

void MainWindow::on_pushButton_IMG1_clicked() /*noexcept*/
{
    if(this->imgRecord.isEmpty())
        return;
    if(0 != this->imgRecord.value("sizeIMG1").toUInt())
    {
        QPixmap invalidPXM(ui->pushButton_IMG1->size());
        invalidPXM.fill(Qt::red);
        QPixmap pxm;
        if(pxm.loadFromData(this->imgRecord.value("dataIMG1").toByteArray()))
        {
            if(!pxm.isNull())
            {
                mainPXM = pxm;
            }
        }
        else
        {
            mainPXM = invalidPXM;
        }

        scaledMainPXM = mainPXM.scaled(ui->graphicsView_IMG->size(),
                                       Qt::KeepAspectRatio,
                                       Qt::FastTransformation);
        mainIMG->setPixmap(scaledMainPXM);
        mainIMG->setPos(-1 *(scaledMainPXM.width() / 2),
                        -1 *(scaledMainPXM.height() / 2));

//        QString str("%1 %2/%3 %4 %5");
//        str = str.arg(this->imgRecord.value("nameUN").toString())
//                 .arg(this->imgRecord.value("area").toUInt())
//                 .arg(this->imgRecord.value("number").toUInt())
//                 .arg(this->imgRecord.value("date").toDate().toString("dd.MM.yy"))
//                 .arg(this->imgRecord.value("time").toTime().toString("hh:mm:ss"));
        ui->label_viewIMGLable->setText(this->createTextIMGLable(1));

        numCurrentShot = 1;
    }
}

void MainWindow::on_pushButton_IMG2_clicked() /*noexcept*/
{
    if(this->imgRecord.isEmpty())
        return;

    if(0 != this->imgRecord.value("sizeIMG2").toUInt())
    {
        QPixmap invalidPXM(ui->pushButton_IMG1->size());
        invalidPXM.fill(Qt::red);
        QPixmap pxm;
        if(pxm.loadFromData(this->imgRecord.value("dataIMG2").toByteArray()))
        {
            if(!pxm.isNull())
            {
                mainPXM = pxm;
            }
        }
        else
        {
            mainPXM = invalidPXM;
        }

        scaledMainPXM = mainPXM.scaled(ui->graphicsView_IMG->size(),
                                       Qt::KeepAspectRatio,
                                       Qt::FastTransformation);
        mainIMG->setPixmap(scaledMainPXM);
        mainIMG->setPos(-1 *(scaledMainPXM.width() / 2),
                        -1 *(scaledMainPXM.height() / 2));

//        QString str("%1 %2/%3 %4 %5");
//        str = str.arg(this->imgRecord.value("nameUN").toString())
//                 .arg(this->imgRecord.value("area").toUInt())
//                 .arg(this->imgRecord.value("number").toUInt())
//                 .arg(this->imgRecord.value("date").toDate().toString("dd.MM.yy"))
//                 .arg(this->imgRecord.value("time").toTime().toString("hh:mm:ss"));
//        ui->label_viewIMGLable->setText(str);
        ui->label_viewIMGLable->setText(this->createTextIMGLable(2));

        numCurrentShot = 2;
    }
}

void MainWindow::on_pushButton_IMG3_clicked() /*noexcept*/
{
    if(this->imgRecord.isEmpty())
        return;
    if(0 != this->imgRecord.value("sizeIMG3").toUInt())
    {
        QPixmap invalidPXM(ui->pushButton_IMG1->size());
        invalidPXM.fill(Qt::red);
        QPixmap pxm;
        if(pxm.loadFromData(this->imgRecord.value("dataIMG3").toByteArray()))
        {
            if(!pxm.isNull())
            {
                mainPXM = pxm;
            }
        }
        else
        {
            mainPXM = invalidPXM;
        }

        scaledMainPXM = mainPXM.scaled(ui->graphicsView_IMG->size(),
                                       Qt::KeepAspectRatio,
                                       Qt::FastTransformation);
        mainIMG->setPixmap(scaledMainPXM);
        mainIMG->setPos(-1 *(scaledMainPXM.width() / 2),
                        -1 *(scaledMainPXM.height() / 2));

//        QString str("%1 %2/%3 %4 %5");
//        str = str.arg(this->imgRecord.value("nameUN").toString())
//                 .arg(this->imgRecord.value("area").toUInt())
//                 .arg(this->imgRecord.value("number").toUInt())
//                 .arg(this->imgRecord.value("date").toDate().toString("dd.MM.yy"))
//                 .arg(this->imgRecord.value("time").toTime().toString("hh:mm:ss"));
//        ui->label_viewIMGLable->setText(str);
        ui->label_viewIMGLable->setText(this->createTextIMGLable(3));

        numCurrentShot = 3;
    }
}

void MainWindow::on_pushButton_IMG4_clicked() /*noexcept*/
{
    if(this->imgRecord.isEmpty())
        return;
    if(0 != this->imgRecord.value("sizeIMG4").toUInt())
    {
        QPixmap invalidPXM(ui->pushButton_IMG1->size());
        invalidPXM.fill(Qt::red);
        QPixmap pxm;
        if(pxm.loadFromData(this->imgRecord.value("dataIMG4").toByteArray()))
        {
            if(!pxm.isNull())
            {
                mainPXM = pxm;
            }
        }
        else
        {
            mainPXM = invalidPXM;
        }

        scaledMainPXM = mainPXM.scaled(ui->graphicsView_IMG->size(),
                                       Qt::KeepAspectRatio,
                                       Qt::FastTransformation);
        mainIMG->setPixmap(scaledMainPXM);
        mainIMG->setPos(-1 *(scaledMainPXM.width() / 2),
                        -1 *(scaledMainPXM.height() / 2));

//        QString str("%1 %2/%3 %4 %5");
//        str = str.arg(this->imgRecord.value("nameUN").toString())
//                 .arg(this->imgRecord.value("area").toUInt())
//                 .arg(this->imgRecord.value("number").toUInt())
//                 .arg(this->imgRecord.value("date").toDate().toString("dd.MM.yy"))
//                 .arg(this->imgRecord.value("time").toTime().toString("hh:mm:ss"));
//        ui->label_viewIMGLable->setText(str);
        ui->label_viewIMGLable->setText(this->createTextIMGLable(4));

        numCurrentShot = 4;
    }
}

void MainWindow::on_pushButton_IMG5_clicked() /*noexcept*/
{
    if(this->imgRecord.isEmpty())
        return;
    if(0 != this->imgRecord.value("sizeIMG5").toUInt())
    {
        QPixmap invalidPXM(ui->pushButton_IMG1->size());
        invalidPXM.fill(Qt::red);
        QPixmap pxm;
        if(pxm.loadFromData(this->imgRecord.value("dataIMG5").toByteArray()))
        {
            if(!pxm.isNull())
            {
                mainPXM = pxm;
            }
        }
        else
        {
            mainPXM = invalidPXM;
        }

        scaledMainPXM = mainPXM.scaled(ui->graphicsView_IMG->size(),
                                       Qt::KeepAspectRatio,
                                       Qt::FastTransformation);
        mainIMG->setPixmap(scaledMainPXM);
        mainIMG->setPos(-1 *(scaledMainPXM.width() / 2),
                        -1 *(scaledMainPXM.height() / 2));
//        QString str("%1 %2/%3 %4 %5");
//        str = str.arg(this->imgRecord.value("nameUN").toString())
//                 .arg(this->imgRecord.value("area").toUInt())
//                 .arg(this->imgRecord.value("number").toUInt())
//                 .arg(this->imgRecord.value("date").toDate().toString("dd.MM.yy"))
//                 .arg(this->imgRecord.value("time").toTime().toString("hh:mm:ss"));
//        ui->label_viewIMGLable->setText(str);
        ui->label_viewIMGLable->setText(this->createTextIMGLable(5));

        numCurrentShot = 5;
    }
}

void MainWindow::on_actionViewArchiveInWindow_triggered(bool checked) /*noexcept*/
{
    if(checked)
    {
        dialog_TableArchive->resetFilter();
        dialog_TableArchive->setDefaultTableSize();
        dialog_TableArchive->show();
    }
    else
    {
        dialog_TableArchive->close();
    }

}

void MainWindow::on_actionStandingInAQueueIMG_triggered() /*noexcept*/
{
    this->modelArchive_KM_IMG->emitViewlastQueueIMG();
}

void MainWindow::on_actionBuildNetTopology_triggered() /*noexcept*/
{
    if(KMSystemType == this->currentSystemType)
    {
        QMessageBox ms;
        ms.setWindowTitle(tr("Build network"));

        QAbstractButton *yes = ms.addButton(tr("Yes"),QMessageBox::YesRole);
        QAbstractButton *no = ms.addButton(tr("No"),QMessageBox::NoRole);
        ms.setText(tr("Build network?"));
        ms.exec();
        if(ms.clickedButton() == yes)
        {
            this->m_kmManager->organiseNetworkTopology();
        }
        if(ms.clickedButton() == no)
        {
            return;
        }
    }
    if(KSSystemType == this->currentSystemType)
    {
        QList<UnitNode*> list(this->m_ksManager->getListUnitNode());
        UnitNode* baseUN(nullptr);
        for(quint8 i(0), n(list.size()); i < n; i++)
        {
            if((quint8)CPP_KSDeviceType == (quint8)list.at(i)->_typeUN() &&
//               true == list.at(i)->flag_connection &&
               false == list.at(i)->flag_hidden)
            {
                baseUN = list.at(i);
                break;
            }
        }
        if(nullptr != baseUN)
        {
            this->m_ksManager->cmdRequest_0x41(baseUN,//this->initiatorUN_KS,
                                               0x0F);
        }
    }
}

//void MainWindow::on_treeView_UN_KM_clicked(const QModelIndex &index) /*noexcept*/
//{
//    if (!index.isValid())
//        return;

//    UnitNode *item = static_cast<UnitNode*>(index.internalPointer());


//    this->setInitiatorUN_KM(item);
//}

void MainWindow::newRecord_KMIMG(quint32 index, quint32 dbIndex) /*noexcept*/
{
#ifdef QT_DEBUG
        qDebug() << "MainWindow::newRecord_KMIMG(index(" << index << "), dbIndex(" << dbIndex << "))";
#endif

    if(-1 != ui->tableView_KM_IMG->rowAt(index))
    {
        ui->tableView_KM_IMG->setRowHeight(index, 60);
    }

    if(ui->actionViewLastIMG->isChecked())
    {
        this->setIMGScene(dbIndex);
        this->on_actionSetViewIMG_triggered();
//        ui->stackedWidget_2->setCurrentIndex(1);
    }
    if(!this->imgRecord.isEmpty())
    {
        if(this->imgRecord.value("id").toUInt() == dbIndex)
        {
            this->setIMGScene(dbIndex);
        }
    }
}

void MainWindow::newRecord_KSIMG(quint32 index, quint32 dbIndex) /*noexcept*/
{
#ifdef QT_DEBUG
        qDebug() << "MainWindow::newRecord_KSIMG(index(" << index << "), dbIndex(" << dbIndex << "))";
#endif

    if(-1 != ui->tableView_KS_IMG->rowAt(index))
    {
        ui->tableView_KS_IMG->setRowHeight(index, 60);
    }

    if(ui->actionViewLastIMG->isChecked())
    {
        this->setIMGScene(dbIndex);
        this->on_actionSetViewIMG_triggered();
//        ui->stackedWidget_2->setCurrentIndex(1);
    }
    if(!this->imgRecord.isEmpty())
    {
        if(this->imgRecord.value("id").toUInt() == dbIndex)
        {
            this->setIMGScene(dbIndex);
        }
    }
}

//void MainWindow::on_tableView_UN_KM_clicked(const QModelIndex &index) /*noexcept*/
//{
//    if (!index.isValid())
//        return;

//    UnitNode *item = this->m_kmManager->getListUnitNode().at(index.row());


//    this->setInitiatorUN_KM(item);
//}

void MainWindow::on_actionViewTableOrTree_triggered(bool checked) /*noexcept*/
{
    if(checked)
    {
        ui->stackedWidget_1->setCurrentIndex(1 + ((int)currentSystemType * 2));
    }
    else
    {
        ui->stackedWidget_1->setCurrentIndex(0 + ((int)currentSystemType * 2));
    }
//    qDebug() << "ui->stackedWidget_1->currentIndex(" << ui->stackedWidget_1->currentIndex() << ")";
}

//void MainWindow::on_stackedWidget_1_customContextMenuRequested(QPoint pos) /*noexcept*/
//{
//    this->tableOrTreeContextMenu->exec(pos +
//                                       this->pos() +
//                                       ui->centralWidget->pos() +
//                                       ui->splitter_horizontal->pos() +
//                                       ui->splitter_vertical->pos() +
//                                       ui->stackedWidget_1->pos() +
//                                       QPoint(12, 36));
//}

void MainWindow::updateStatusClock() /*noexcept*/
{
    if(clockLabel->isVisible())
    {
        clockLabel->setText(QTime::currentTime().toString("hh:mm:ss"));
    }
}

//void MainWindow::updateStatusMode(qint32 mode) /*noexcept*/
//{
//    if(statusMode == mode)
//        return;
//    statusMode = mode;
//    QString statusSTR;
//    switch(statusMode)
//    {
//        case Undefined:
//        {
//            statusSTR = tr("Mode[Undefined] ");
//            break;
//        };
//        case ConnectSubset:
//        {
//            statusSTR = tr("Mode[ConnectSubset] ");
//            break;
//        };
//        case DefinitionHeard:
//        {
//            statusSTR = tr("Mode[DefinitionHeard] ");
//            break;
//        };
//        case DisconnectHeard:
//        {
//            statusSTR = tr("Mode[DefinitionHeard] ");
//            break;
//        };
//        case DisconnectSubset:
//        {
//            statusSTR = tr("Mode[DisconnectSubset] ");
//            break;
//        };
//        case DutyMode:
//        {
//            statusSTR = tr("Mode[DutyMode] ");
//            break;
//        };
//        case ConnectVK:
//        {
//            statusSTR = tr("Mode[ConnectVK] ");
//            break;
//        };
//        case DefinitionTopology:
//        {
//            statusSTR = tr("Mode[DefinitionTopology] ");
//            break;
//        };
//    }
//    if(!statusMSG.isEmpty())
//    {
//        statusSTR.append("[%1] %2 [%3]");
//        statusSTR = statusSTR.arg(statusMSG.value("time").toTime().toString("hh:mm:ss"))
//                             .arg(statusMSG.value("ioType").toBool() ? "<<" : ">>")
//                             .arg(statusMSG.value("commentRUS").toString());
//    }
//    msgLabel->setText(statusSTR);
////    if(!ui->statusBar->children().contains(msgLabel))
////    {
////        ui->statusBar->addWidget(msgLabel);
////    }
//}

void MainWindow::updateStatusWorkMode(bool mode) /*noexcept*/
{
    QString str;
    if(mode)
    {
        str = tr("Editor work mode");
    }
    else
    {
        str = tr("Operator work mode");
    }
    workModeLabel->setText(str);
}

void MainWindow::updateStatusMSG(quint32 index) /*noexcept*/
{
    statusMSG = m_dbManager->get_MSGRecord(index);
    if(!statusMSG.isEmpty())
    {
        QString statusStr("[%1] %2 [%3]");

        statusStr = statusStr.arg(statusMSG.value("time").toTime().toString("hh:mm:ss"))
                             .arg(statusMSG.value("ioType").toBool() ? "<<" : ">>")
                             .arg(ui->actionRussian->isChecked() ? (statusMSG.value("commentRUS").toString()) : (statusMSG.value("commentENG").toString()));

        msgLabel->setText(statusStr);
    }
    if(msgLabel->text().isEmpty())
    {
        msgLabel->setVisible(false);
    }
    else
    {
        msgLabel->setVisible(true);
    }
}

void MainWindow::updateStatusProgress(UnitNode *unSender,
                                      quint8 nomS,
                                      quint8 numTrain,
                                      quint8 progress) /*noexcept*/
{
    lastProgressUN = unSender;
    lastProgressNumShot = nomS;
    lastProgressNumTrain = numTrain;
    timerUpdateProgress->stop();
    imgProgressBar->setValue(progress);
    if(0 == imgProgressBar->value() ||
       100 == imgProgressBar->value())
    {
        imgProgressBar->setVisible(false);
    }
    else
    {
        timerUpdateProgress->start(10000);
        imgProgressBar->setVisible(true);
    }
}

void MainWindow::hideStatusProgress() /*noexcept*/
{
    timerUpdateProgress->stop();
    imgProgressBar->setVisible(false);
    if(lastProgressUN)
    {
        switch((quint8)lastProgressUN->_typeSystem())
        {
            case (quint8)KMSystemType:
            {
                break;
            }
            case (quint8)KSSystemType:
            {
//                this->m_ksManager->m_portManager->refreshBuffer();
//                this->m_ksManager->cmdRequest_0x41(lastProgressUN,
//                                                   0x42,
//                                                   0x02,
//                                                   0x0A,
//                                                   0x00,
//                                                   0x00);
//                this->m_ksManager->cmdRequest_0x41(m_ksManager->m_ksMsgManager->m_lastAppendBase,
//                                                   0xC5,
//                                                   (quint8)lastProgressUN->_id().at(0),
//                                                   (quint8)lastProgressUN->_id().at(1),
//                                                   (lastProgressNumTrain * 0x10) + lastProgressNumShot,
//                                                   0x01);

                break;
            }
            case (quint8)BSKSystemType:
            {
                break;
            }
        }
    }
}

void MainWindow::tabWidgetCurrentIndexChanged(int index) /*noexcept*/
{
//    ui->tabWidget->setMaximumWidth(380);
//    ui->tabWidget->setMinimumWidth(380);
//    ui->stackedWidget_4->setMinimumWidth(390);
//    ui->stackedWidget_4->setMaximumWidth(500);
    return;
}

void MainWindow::updateBTLinkToSend(QString strLink) /*noexcept*/
{
    if(strLink.contains(";"))
    {
        QStringList strSplit(strLink.split(";"));
        this->consoleBTLink.clear();
        for(int i(0), n(strSplit.size()); i < n; i++)
        {
            this->consoleBTLink.append(strSplit.at(i).toUInt(0, 16));
        }
        return;
    }
    this->consoleBTLink.clear();
    this->consoleBTLink.append(strLink.toUInt(0, 16));

}

void MainWindow::setInitiatorUN(UnitNode *un) /*noexcept*/
{
    switch (un->_typeSystem())
    {
        case KMSystemType:
        {
            this->setInitiatorUN_KM(un);
            break;
        }
        case KSSystemType:
        {
            this->setInitiatorUN_KS(un);
            break;
        }
        case BSKSystemType:
        {
            this->setInitiatorUN_BSK(un);
            break;
        }
        default:
        {
            break;
        }
    }
}

void MainWindow::setInitiatorUN_KM(UnitNode *un) /*noexcept*/
{
    this->initiatorUN_KM = un;

    ui->groupBox_ConditionUN->setEnabled(true);
    ui->groupBox_RequestIMG->setEnabled(true);
    ui->groupBox_kameraSetting->setEnabled(true);
//    ui->groupBox_kvantSetting->setEnabled(true);
//    ui->groupBox_PortCommand->setEnabled(true);

    ui->pushButton_Inquiry->setEnabled(true);
    ui->pushButton_List->setEnabled(true);
    ui->pushButton_Kill->setEnabled(true);
    ui->pushButton_loopTxPowerRSSI->setEnabled(true);
    ui->pushButton_Set->setEnabled(true);

    this->updateSettingUN_KM(un, QByteArray());

    ui->lineEdit_linkConsole->clear();

    if(this->initiatorUN_KM)
    {
        ui->label_ID_View_KM->setText(this->initiatorUN_KM->id.toHex().toUpper());
        if(this->initiatorUN_KM->_abstractName().isEmpty())
        {
            ui->label_Name_View_KM->setText(this->initiatorUN_KM->name.toUpper());
        }
        else
        {
            ui->label_Name_View_KM->setText(QString(this->initiatorUN_KM->name.toUpper()) + " / " + QString(this->initiatorUN_KM->_abstractName()));
        }
//        ui->label_Name_View_KM->setText(this->initiatorUN_KM->name);

        if(VK_KMDeviceType == this->initiatorUN_KM->_typeUN())
        {
            ui->pushButton_Inquiry->setEnabled(false);
            ui->pushButton_List->setEnabled(false);
            ui->pushButton_Set->setEnabled(false);
        }

        if(RT_KMDeviceType == this->initiatorUN_KM->_typeUN())
        {
            if(this->initiatorUN_KM->btLink.isEmpty())
            {
                ui->pushButton_Kill->setEnabled(false);
                ui->pushButton_loopTxPowerRSSI->setEnabled(false);
            }
            else
            {
                ui->pushButton_Set->setEnabled(false);
            }
            ui->groupBox_RequestIMG->setEnabled(false);
            ui->groupBox_kameraSetting->setEnabled(false);
//            ui->groupBox_kvantSetting->setEnabled(false);
//            ui->groupBox_PortCommand->setEnabled(false);
        }

        if(Unknown_KMDeviceType == this->initiatorUN_KM->_typeUN())
        {
            if(this->initiatorUN_KM->btLink.isEmpty())
            {
                ui->pushButton_Kill->setEnabled(true);
                ui->pushButton_loopTxPowerRSSI->setEnabled(true);
            }
            else
            {
                ui->pushButton_Set->setEnabled(false);
            }
            ui->groupBox_ConditionUN->setEnabled(false);
            ui->groupBox_RequestIMG->setEnabled(false);
            ui->groupBox_kameraSetting->setEnabled(false);
//            ui->groupBox_kvantSetting->setEnabled(false);
//            ui->groupBox_PortCommand->setEnabled(false);
        }

        QString linkStr(this->initiatorUN_KM->btLink.toHex());
        while(!linkStr.isEmpty())
        {
            ui->lineEdit_linkConsole->setText(ui->lineEdit_linkConsole->text() + linkStr.left(2) + ";");
            linkStr.remove(0, 2);
        }
        ui->lineEdit_linkConsole->setText(ui->lineEdit_linkConsole->text() + "FF");



        return;
    }
    ui->label_ID_View_KM->setText("***");
    ui->label_Name_View_KM->setText("***");
    ui->tabWidget->setCurrentIndex(2);
//    this->tabWidgetCurrentIndexChanged(2);
    return;
}

void MainWindow::setInitiatorUN_KS(UnitNode *un) /*noexcept*/
{
    if(this->initiatorUN_KS == un)
        return;

    this->initiatorUN_KS = un;

    ui->groupBox_Selected_Device_KS->setEnabled(true);
    ui->groupBox_Change_Area_Number_KS->setEnabled(true);
    ui->groupBox_Select_VK_KS->setEnabled(true);
//    -->
    ui->groupBox_Command_KS->setEnabled(true);
    ui->pushButton_0xFD_Get_Shot_KS->setEnabled(true);
    ui->pushButton_0xFA_Get_Test_Shot_KS->setEnabled(true);
//    <--
    ui->groupBox_Inst_Mode_VK_KS->setEnabled(true);
    ui->groupBox_List_D_VK_KS->setEnabled(true);
    ui->groupBox_Add_D_VK_KS->setEnabled(true);
    ui->groupBox_Remove_D_VK_KS->setEnabled(true);
    ui->groupBox_Switch_OnOrOff_D_KS->setEnabled(true);
    ui->groupBox_RM433_KS->setEnabled(true);
    ui->spinBox_0x78_Area_KS->setValue(0);
    ui->spinBox_0x78_Number_KS->setValue(0);
    ui->spinBox_Kit_Num_RM433_KS->setValue(0);

    ui->pushButton_Switch_On_Self_D_KS->setEnabled(true);
    ui->pushButton_Switch_Off_Self_D_KS->setEnabled(true);
    ui->pushButton_Switch_On_Other_D1_KS->setEnabled(true);
    ui->pushButton_Switch_Off_Other_D1_KS->setEnabled(true);
    ui->pushButton_Switch_On_Other_D2_KS->setEnabled(true);
    ui->pushButton_Switch_Off_Other_D2_KS->setEnabled(true);

    if(this->initiatorUN_KS)
    {
        ui->label_ID_View_KS->setText(this->initiatorUN_KS->id.toHex().toUpper());
        if(this->initiatorUN_KS->_abstractName().isEmpty())
        {
            ui->label_Name_View_KS->setText(this->initiatorUN_KS->name.toUpper());
        }
        else
        {
            ui->label_Name_View_KS->setText(QString(this->initiatorUN_KS->name.toUpper()) + " / " + QString(this->initiatorUN_KS->_abstractName()));
        }

        UnitNode *parentUN(this->initiatorUN_KS->unTreeParent);
        if(nullptr != parentUN)
        {
            if(UnknownDeviceType != parentUN->_typeUN() ||
               CPP_KSDeviceType  == parentUN->_typeUN() ||
               RT01_KSDeviceType == parentUN->_typeUN() ||
               RT02_KSDeviceType == parentUN->_typeUN() ||
               VK09_KSDeviceType == parentUN->_typeUN() ||
               VK0A_KSDeviceType == parentUN->_typeUN() ||
               VK0B_KSDeviceType == parentUN->_typeUN())
            {
                ui->label_Name_View_Lead_KS->setText(parentUN->data(0).toString() + " " + parentUN->data(3).toString());
            }
            else
            {
                ui->label_Name_View_Lead_KS->setText("***");
            }
        }

        ui->spinBox_0x78_Area_KS->setValue(this->initiatorUN_KS->_numArea());
        ui->spinBox_0x78_Number_KS->setValue(this->initiatorUN_KS->_numNode());
        ui->spinBox_Kit_Num_RM433_KS->setValue(this->initiatorUN_KS->_numKit());

        if(CPP_KSDeviceType == this->initiatorUN_KS->_typeUN())
        {
            ui->groupBox_Select_VK_KS->setEnabled(false);
//            -->
//            ui->groupBox_Command_KS->setEnabled(false);
            ui->pushButton_0xFD_Get_Shot_KS->setEnabled(false);
            ui->pushButton_0xFA_Get_Test_Shot_KS->setEnabled(false);
//            <--
            ui->groupBox_Inst_Mode_VK_KS->setEnabled(false);
            ui->groupBox_List_D_VK_KS->setEnabled(false);
            ui->groupBox_Add_D_VK_KS->setEnabled(false);
            ui->groupBox_Remove_D_VK_KS->setEnabled(false);
            ui->groupBox_Switch_OnOrOff_D_KS->setEnabled(false);
            ui->groupBox_RM433_KS->setEnabled(false);

        }

        if(RT01_KSDeviceType == this->initiatorUN_KS->_typeUN() ||
           RT02_KSDeviceType == this->initiatorUN_KS->_typeUN())
        {
            ui->groupBox_Select_VK_KS->setEnabled(false);

//            -->
//            ui->groupBox_Command_KS->setEnabled(false);
            ui->pushButton_0xFD_Get_Shot_KS->setEnabled(false);
//            <--

            ui->groupBox_Inst_Mode_VK_KS->setEnabled(false);
            ui->groupBox_List_D_VK_KS->setEnabled(false);
            ui->groupBox_Add_D_VK_KS->setEnabled(false);
            ui->groupBox_Remove_D_VK_KS->setEnabled(false);
            ui->groupBox_Switch_OnOrOff_D_KS->setEnabled(false);
            ui->groupBox_RM433_KS->setEnabled(false);
        }

        if(VK09_KSDeviceType == this->initiatorUN_KS->_typeUN())
        {
            this->on_pushButton_Select_VK1_KS_clicked();
            ui->groupBox_Select_VK_KS->setEnabled(true);

//            timer_Request_List_D_VK_KS->singleShot(1500, ui->pushButton_Request_List_D_VK_KS, SLOT(click()));
        }

        if(VK0A_KSDeviceType == this->initiatorUN_KS->_typeUN() ||
           VK0B_KSDeviceType == this->initiatorUN_KS->_typeUN())
        {
            this->on_pushButton_Select_VK1_KS_clicked();
            ui->groupBox_Select_VK_KS->setEnabled(false);

//            timer_Request_List_D_VK_KS->singleShot(1500, ui->pushButton_Request_List_D_VK_KS, SLOT(click()));

        }

        //
        if(VK09_KSDeviceType == this->initiatorUN_KS->_typeUN() ||
           VK0A_KSDeviceType == this->initiatorUN_KS->_typeUN() ||
           VK0B_KSDeviceType == this->initiatorUN_KS->_typeUN())
        {
            int numVK(1);

            switch((initiatorUN_KS->listSettingVK.at(numVK - 1).modeVK / (quint8)0x10) & (quint8)0x0F)
            {
                case((quint8)0x03):
                {
                    ui->label_0x42x43_Inst_Mode_VK_KS->setText(tr("Mode[Day]"));
                    break;
                }
                case((quint8)0x0E):
                {
                    ui->label_0x42x43_Inst_Mode_VK_KS->setText(tr("Mode[Night]"));
                    break;
                }
                default:
                {
                    ui->label_0x42x43_Inst_Mode_VK_KS->setText(tr("Mode"));
                    break;
                }
            }

            switch(initiatorUN_KS->modeAnxiety)
            {
                case (quint8)0x00:
                {
                    ui->comboBox_0x41x28_Inst_Mode_VK_KS->setCurrentIndex(0);
                    break;
                }
                case (quint8)0x01:
                {
                    ui->comboBox_0x41x28_Inst_Mode_VK_KS->setCurrentIndex(1);
                    break;
                }
                default:
                {
                    break;
                }
            }

            switch(initiatorUN_KS->listSettingVK.at(numVK - 1).modeVK & 0x0F)
            {
                case 0x01:
                {
                    ui->comboBox_0x42x43_Inst_Mode_VK_KS->setCurrentIndex(0);
                    break;
                }
                case 0x02:
                {
                    ui->comboBox_0x42x43_Inst_Mode_VK_KS->setCurrentIndex(1);
                    break;
                }
                case 0x03:
                {
                    ui->comboBox_0x42x43_Inst_Mode_VK_KS->setCurrentIndex(2);
                    break;
                }
                default:
                {
                    break;
                }
            }

            ui->spinBox_0x42x43_Inst_T1_VK_KS->setValue(initiatorUN_KS->listSettingVK.at(numVK - 1).timeout1);
//            ui->spinBox_0x42x43_Inst_T2_VK_KS->setValue(initiatorUN_KS->listSettingVK.at(numVK - 1).timeout2);
//            ui->spinBox_0x42x43_Inst_T3_VK_KS->setValue(initiatorUN_KS->listSettingVK.at(numVK - 1).timeout3);
            ui->spinBox_0x42x43_Inst_T2_VK_KS->setValue(0);
            ui->spinBox_0x42x43_Inst_T3_VK_KS->setValue(1);

            if(VK09_KSDeviceType == this->initiatorUN_KS->_typeUN() ||
               VK0A_KSDeviceType == this->initiatorUN_KS->_typeUN())
            {
                ui->pushButton_Switch_On_Self_D_KS->setEnabled(false);
                ui->pushButton_Switch_Off_Self_D_KS->setEnabled(false);
            }
            else
            {
                ui->pushButton_Switch_On_Self_D_KS->setEnabled(true);
                ui->pushButton_Switch_Off_Self_D_KS->setEnabled(true);
                if(0xFF != initiatorUN_KS->flag_selfD)
                {
                    if(0x01 == initiatorUN_KS->flag_selfD)
                    {
                        ui->pushButton_Switch_On_Self_D_KS->setEnabled(false);
                        ui->pushButton_Switch_Off_Self_D_KS->setEnabled(true);
                    }
                    if(0x00 == initiatorUN_KS->flag_selfD)
                    {
                        ui->pushButton_Switch_On_Self_D_KS->setEnabled(true);
                        ui->pushButton_Switch_Off_Self_D_KS->setEnabled(false);
                    }
                }

            }

            ui->pushButton_Switch_On_Other_D1_KS->setEnabled(true);
            ui->pushButton_Switch_Off_Other_D1_KS->setEnabled(true);
            if(0xFF != this->initiatorUN_KS->flag_otherD1)
            {
                if(0x01 == this->initiatorUN_KS->flag_otherD1)
                {
                    ui->pushButton_Switch_On_Other_D1_KS->setEnabled(false);
                    ui->pushButton_Switch_Off_Other_D1_KS->setEnabled(true);
                }
                if(0x00 == this->initiatorUN_KS->flag_otherD1)
                {
                    ui->pushButton_Switch_On_Other_D1_KS->setEnabled(true);
                    ui->pushButton_Switch_Off_Other_D1_KS->setEnabled(false);
                }
            }

            ui->pushButton_Switch_On_Other_D2_KS->setEnabled(true);
            ui->pushButton_Switch_Off_Other_D2_KS->setEnabled(true);
            if(0xFF != this->initiatorUN_KS->flag_otherD2)
            {
                if(0x01 == initiatorUN_KS->flag_otherD2)
                {
                    ui->pushButton_Switch_On_Other_D2_KS->setEnabled(false);
                    ui->pushButton_Switch_Off_Other_D2_KS->setEnabled(true);
                }
                if(0x00 == initiatorUN_KS->flag_otherD2)
                {
                    ui->pushButton_Switch_On_Other_D2_KS->setEnabled(true);
                    ui->pushButton_Switch_Off_Other_D2_KS->setEnabled(false);
                }
            }
            //

            QList<QByteArray> listD(initiatorUN_KS->_listIDBSK((quint8)0x01));
            ui->comboBox_List_D_VK_KS->clear();
    //        ui->comboBox_Remove_D_VK_KS->clear();
            while(1 < ui->comboBox_Remove_D_VK_KS->count())
            {
                ui->comboBox_Remove_D_VK_KS->removeItem(1);
            }
            for(int i(0), n(listD.size()); i < n; i++)
            {
                if(listD.at(i).toHex().toUInt(0, 16))
                {
                    ui->comboBox_List_D_VK_KS->addItem(ProxyMetod().getNameSO(listD.at(i)) + "[" + listD.at(i).toHex().toUpper() + "]", QVariant(listD.at(i)));
                    ui->comboBox_Remove_D_VK_KS->addItem(ProxyMetod().getNameSO(listD.at(i)) + "[" + listD.at(i).toHex().toUpper() + "]", QVariant(listD.at(i)));
                }
            }
        }


    }
    else
    {
        ui->label_ID_View_KS->setText("***");
        ui->label_Name_View_KS->setText("***");
        ui->label_Name_View_Lead_KS->setText("***");
    }


}

void MainWindow::setInitiatorUN_BSK(UnitNode *un) /*noexcept*/
{
    this->initiatorUN_BSK = un;

    if(!this->initiatorUN_BSK)
    {
        ui->label_ID_View_BSK->setText("***");
        ui->label_Name_View_BSK->setText("***");
        ui->spinBox_0x78_Area_KS->setValue(0);
        ui->spinBox_0x78_Number_KS->setValue(0);
        ui->stackedWidget_Settings_BSK->setCurrentIndex(0);
        return;
    }

    ui->spinBox_0x78_Area_BSK->setValue(this->initiatorUN_BSK->_numArea());
    ui->spinBox_0x78_Number_BSK->setValue(this->initiatorUN_BSK->_numNode());

    ui->label_ID_View_BSK->setText(this->initiatorUN_BSK->id.toHex().toUpper());
    if(this->initiatorUN_BSK->_abstractName().isEmpty())
    {
        ui->label_Name_View_BSK->setText(this->initiatorUN_BSK->name.toUpper());
    }
    else
    {
        ui->label_Name_View_BSK->setText(QString(this->initiatorUN_BSK->name.toUpper()) + " / " + QString(this->initiatorUN_BSK->_abstractName()));
    }
//    ui->label_Name_View_BSK->setText(this->initiatorUN_BSK->name);

    //
    switch(this->initiatorUN_BSK->_typeUN())
    {
        case S_BSKDeviceType://БСК-С
        {
            ui->stackedWidget_Settings_BSK->setCurrentIndex(3);
            break;
        }

        case RLO_BSKDeviceType://БСК-РЛО
        {
            ui->stackedWidget_Settings_BSK->setCurrentIndex(2);
            break;
        }

        case RLD_BSKDeviceType://БСК-РЛД
        {
            ui->stackedWidget_Settings_BSK->setCurrentIndex(1);
            break;
        }

        case RWD_BSKDeviceType://БСК-РВД
        {
            ui->stackedWidget_Settings_BSK->setCurrentIndex(0);
            break;
        }

        case MSO_BSKDeviceType://БСК-МСО
        {
            ui->stackedWidget_Settings_BSK->setCurrentIndex(0);
            break;
        }

        case BVU_BSKDeviceType://БВУ
        {
            ui->stackedWidget_Settings_BSK->setCurrentIndex(4);
            break;
        }
        case VK_BSKDeviceType://БСК-ВК
        {
            ui->stackedWidget_Settings_BSK->setCurrentIndex(0);
            break;
        }

        case O_BSKDeviceType://БСК-О
        {
            ui->stackedWidget_Settings_BSK->setCurrentIndex(0);
            break;
        }

        case IK_BSKDeviceType://БСК-ИК
        {
            ui->stackedWidget_Settings_BSK->setCurrentIndex(0);
            break;
        }

        case RW_BSKDeviceType://БСК-РВ
        {
            ui->stackedWidget_Settings_BSK->setCurrentIndex(0);
            break;
        }
        case RVP_BSKDeviceType://БСК-РВП
        {
            ui->stackedWidget_Settings_BSK->setCurrentIndex(5);
            break;
        }

        case VSO_BSKDeviceType://БСК-ВСО
        {
            ui->stackedWidget_Settings_BSK->setCurrentIndex(0);
            break;
        }

        default:
        {
            ui->stackedWidget_Settings_BSK->setCurrentIndex(0);
            break;
        }
    }

}

void MainWindow::on_pushButton_0xA1_clicked() /*noexcept*/
{
    if(this->initiatorUN_KM)
    {
        this->m_kmManager->cmdRequest_0xA1(this->initiatorUN_KM);
    }

}

void MainWindow::on_pushButton_0xB1_clicked() /*noexcept*/
{
    if(this->initiatorUN_KM)
    {
        this->m_kmManager->cmdRequest_0xB1(this->initiatorUN_KM);
    }

}

//void MainWindow::on_pushButton_0xDE_clicked() /*noexcept*/
//{
//    if(this->initiatorUN_KM)
//    {
//        this->m_kmManager->cmdRequest_0xDE(this->initiatorUN_KM,
//                                           (quint16)ui->spinBox_0xDE_block->value(),
//                                           (quint16)ui->spinBox_0xDE_delay->value());
//    }

//}

void MainWindow::on_pushButton_0x1F_clicked() /*noexcept*/
{
    quint8 countBSK(0),
           idBSK1h(0),
           idBSK1l(0),
           idBSK2h(0),
           idBSK2l(0),
           idBSK3h(0),
           idBSK3l(0);
    idBSK1h = ui->lineEdit_0x1F_BSK1->text().toUInt(0, 16) / 0x0100;
    idBSK1h = idBSK1h & 0x0F;
    idBSK1l = ui->lineEdit_0x1F_BSK1->text().toUInt(0, 16) & 0x00FF;

    idBSK2h = ui->lineEdit_0x1F_BSK2->text().toUInt(0, 16) / 0x0100;
    idBSK2h = idBSK2h & 0x0F;
    idBSK2l = ui->lineEdit_0x1F_BSK2->text().toUInt(0, 16) & 0x00FF;

    idBSK3h = ui->lineEdit_0x1F_BSK3->text().toUInt(0, 16) / 0x0100;
    idBSK3h = idBSK3h & 0x0F;
    idBSK3l = ui->lineEdit_0x1F_BSK3->text().toUInt(0, 16) & 0x00FF;

    switch (ui->comboBox_0x1F_BSK1->currentIndex())
    {
        case 0:
        {
            idBSK1h = idBSK1l = 0;
        }
        break;

        case 1:
        {
            idBSK1h += 0x20;
        }
        break;

        case 2:
        {
            idBSK1h += 0x30;
        }
        break;

        case 3:
        {
            idBSK1h += 0x40;
        }
        break;

        case 4:
        {
            idBSK1h += 0x50;
        }
        break;

        case 5:
        {
            idBSK1h += 0x60;
        }
        break;

        case 6:
        {
            idBSK1h += 0x70;
        }
        break;

        case 7:
        {
            idBSK1h += 0xA0;
        }
        break;

        case 8:
        {
            idBSK1h += 0xB0;
        }
        break;

        case 9:
        {
            idBSK1h += 0xC0;
        }
        break;

        case 10:
        {
            idBSK1h += 0xD0;
        }
        break;

        case 11:
        {
            idBSK1h += 0xE0;
        }
        break;

        case 12:
        {
            idBSK1h += 0xF0;
        }
        break;


        default:
        {
            idBSK1h = idBSK1l = 0;
        }
        break;
    }

    switch (ui->comboBox_0x1F_BSK2->currentIndex())
    {
        case 0:
        {
            idBSK2h = idBSK2l = 0;
        }
        break;

        case 1:
        {
            idBSK2h += 0x20;
        }
        break;

        case 2:
        {
            idBSK2h += 0x30;
        }
        break;

        case 3:
        {
            idBSK2h += 0x40;
        }
        break;

        case 4:
        {
            idBSK2h += 0x50;
        }
        break;

        case 5:
        {
            idBSK2h += 0x60;
        }
        break;

        case 6:
        {
            idBSK2h += 0x70;
        }
        break;

        case 7:
        {
            idBSK2h += 0xA0;
        }
        break;

        case 8:
        {
            idBSK2h += 0xB0;
        }
        break;

        case 9:
        {
            idBSK2h += 0xC0;
        }
        break;

        case 10:
        {
            idBSK2h += 0xD0;
        }
        break;

        case 11:
        {
            idBSK2h += 0xE0;
        }
        break;

        case 12:
        {
            idBSK2h += 0xF0;
        }
        break;


        default:
        {
            idBSK2h = idBSK2l = 0;
        }
        break;
    }

    switch (ui->comboBox_0x1F_BSK3->currentIndex())
    {
        case 0:
        {
            idBSK3h = idBSK3l = 0;
        }
        break;

        case 1:
        {
            idBSK3h += 0x20;
        }
        break;

        case 2:
        {
            idBSK3h += 0x30;
        }
        break;

        case 3:
        {
            idBSK3h += 0x40;
        }
        break;

        case 4:
        {
            idBSK3h += 0x50;
        }
        break;

        case 5:
        {
            idBSK3h += 0x60;
        }
        break;

        case 6:
        {
            idBSK3h += 0x70;
        }
        break;

        case 7:
        {
            idBSK3h += 0xA0;
        }
        break;

        case 8:
        {
            idBSK3h += 0xB0;
        }
        break;

        case 9:
        {
            idBSK3h += 0xC0;
        }
        break;

        case 10:
        {
            idBSK3h += 0xD0;
        }
        break;

        case 11:
        {
            idBSK3h += 0xE0;
        }
        break;

        case 12:
        {
            idBSK3h += 0xF0;
        }
        break;


        default:
        {
            idBSK3h = idBSK3l = 0;
        }
        break;
    }

    countBSK = ((idBSK1h || idBSK1l) ? 1 : 0) +
               ((idBSK2h || idBSK2l) ? 1 : 0) +
               ((idBSK3h || idBSK3l) ? 1 : 0);

    if(this->initiatorUN_KM)
    {
        this->m_kmManager->cmdRequest_0x1F(this->initiatorUN_KM,
                                         ui->comboBox_0x1F_mode->currentIndex() + 1,
                                         ui->spinBox_0x1F_delay1->value(),
                                         ui->spinBox_0x1F_delay2->value(),
                                         ui->spinBox_0x1F_delay3->value(),
                                         (ui->checkBox_0x1F_vstrSO->isChecked() ? (quint8)1 : (quint8)0),
                                         countBSK,
                                         idBSK1h,
                                         idBSK1l,
                                         idBSK2h,
                                         idBSK2l,
                                         idBSK3h,
                                         idBSK3l,
                                         (ui->checkBox_0x1F_drugSO->isChecked() ? (quint8)1 : (quint8)0),
                                         (ui->checkBox_0x1F_logic->isChecked() ? (quint8)1 : (quint8)0),
                                         ui->spinBox_0x1F_komplekt->value());
    }

}

void MainWindow::on_pushButton_0x2F_clicked() /*noexcept*/
{
    if(this->initiatorUN_KM)
    {
        this->m_kmManager->cmdRequest_0x2F(this->initiatorUN_KM);

    }
}

void MainWindow::on_pushButton_Inquiry_clicked() /*noexcept*/
{
    if(this->initiatorUN_KM)
    {
        this->m_kmManager->oneBTInquiryToUN(this->initiatorUN_KM);
    }
}

void MainWindow::on_pushButton_List_clicked() /*noexcept*/
{
    if(this->initiatorUN_KM)
    {
        this->m_kmManager->oneBTListToUN(this->initiatorUN_KM);
    }
}

void MainWindow::on_pushButton_Kill_clicked() /*noexcept*/
{
    if(this->initiatorUN_KM)
    {
        if(this->initiatorUN_KM->unTreeParent)
        {
            if(RT_KMDeviceType == this->initiatorUN_KM->unTreeParent->_typeUN())
            {
                this->m_kmManager->oneBTKillToUN(this->initiatorUN_KM->unTreeParent,
                                                 this->initiatorUN_KM);
            }
        }
    }
}

void MainWindow::on_pushButton_loopTxPowerRSSI_clicked() /*noexcept*/
{
    if(this->initiatorUN_KM)
    {
        if(this->initiatorUN_KM->unTreeParent)
        {
            if(RT_KMDeviceType == this->initiatorUN_KM->unTreeParent->_typeUN())
            {
                this->m_kmManager->oneBTTxpowerToUN(this->initiatorUN_KM->unTreeParent,
                                                  this->initiatorUN_KM);
            }
        }
    }
}

void MainWindow::on_pushButton_Set_clicked() /*noexcept*/
{
    if(this->initiatorUN_KM)
    {
        this->m_kmManager->oneBTSetToUN(this->initiatorUN_KM);
    }
}

void MainWindow::on_lineEdit_Console_returnPressed() /*noexcept*/
{
    if(ui->lineEdit_Console->text().isEmpty())
        return;
    if(this->initiatorUN_KM)
    {
        if(this->consoleBTLink == this->initiatorUN_KM->btLink)
        {
            this->m_kmManager->sendMSG(this->initiatorUN_KM,
                                     0xFF,
                                     ui->lineEdit_Console->text().toUtf8());
            return;
        }
    }

    this->m_kmManager->sendMSG(this->consoleBTLink,
                             ui->lineEdit_Console->text().toUtf8());
    return;
}

void MainWindow::updateSettingUN_KM(UnitNode* un,
                                    QByteArray msg) /*noexcept*/
{
    if(this->initiatorUN_KM != un)
        return;

    ui->comboBox_0x1F_mode->setCurrentIndex(un->modeVK - 1);
    ui->spinBox_0x1F_delay1->setValue(un->timeout1);
    ui->spinBox_0x1F_delay2->setValue(un->timeout2);
    ui->spinBox_0x1F_delay3->setValue(un->timeout3);
    ui->checkBox_autoRequestB1->setChecked(un->autoRequestB1 ? true : false);
    ui->checkBox_0x1F_drugSO->setChecked((0x00 != un->otherSO) ? true : false);
    ui->checkBox_0x1F_logic->setChecked((0x00 != un->logic) ? true : false);
    ui->checkBox_0x1F_vstrSO->setChecked((0x00 != un->selfSO) ? true : false);
    QByteArray a1, a2, a3;
    a1.append(un->idBSK1h & 0x0F);
    a1.append(un->idBSK1l);
//    ui->lineEdit_0x1F_BSK1->setText(a1.toHex().right(3));
//    QString nameSO1(ProxyMetod.getNameSO(a1));
//    ui->lineEdit_0x1F_BSK1-)

    a2.append(un->idBSK2h & 0x0F);
    a2.append(un->idBSK2l);
    ui->lineEdit_0x1F_BSK2->setText(a2.toHex().right(3));

    a3.append(un->idBSK3h & 0x0F);
    a3.append(un->idBSK3l);
    ui->lineEdit_0x1F_BSK3->setText(a3.toHex().right(3));

    ui->spinBox_0x1F_komplekt->setValue(un->numComplex);
}

void MainWindow::on_checkBox_autoRequestB1_clicked(bool checked) /*noexcept*/
{
    if(this->initiatorUN_KM)
    {
        this->initiatorUN_KM->autoRequestB1 = checked;
    }
}

void MainWindow::on_actionViewToolDialog_triggered(bool checked) /*noexcept*/
{
        QList<int> sizes;
        if(!checked)
        {
            sizes.append(ui->splitter_main->width());
            sizes.append(0);
            ui->splitter_main->setChildrenCollapsible(true);
        }
        else
        {

            sizes.append(ui->splitter_main->width() / 2);
            sizes.append(ui->splitter_main->width() / 2);
            ui->splitter_main->setChildrenCollapsible(false);
        }
        ui->splitter_main->setSizes(sizes);
        this->setDefaultTableSize();
}

void MainWindow::updatePortCondition(qint32 condition) /*noexcept*/
{
    switch(condition)
    {
        case 1:
        {
            ui->action_viewDialogPortSetting->setIcon(QIcon(":/icon/antena_green_24x24.png"));
            return;
        }
        case -1:
        {
            ui->action_viewDialogPortSetting->setIcon(QIcon(":/icon/antena_off_24x24.png"));
            return;
        }
        case 0:
        {
            ui->action_viewDialogPortSetting->setIcon(QIcon(":/icon/antena_yell_24x24.png"));
            return;
        }
    }
}


void MainWindow::on_actionRussian_triggered() /*noexcept*/
{
    this->setRussian();
}

void MainWindow::setRussian() /*noexcept*/
{
    emit this->setTranslator(ruTranslator);

//    sleep(1);
    ui->actionEnglish->setChecked(false);
    ui->actionRussian->setChecked(true);
    ui->retranslateUi(this);
    dialog_SerialPortSetting->retranslate();
    dialog_TableArchive->retranslate();
    m_dbManager->setTypeLanguage("ru");

    QList<UnitNode*> listBTUN(m_kmManager->m_btManager->listBTUnitNode);
    while(!listBTUN.isEmpty())
    {
        listBTUN.first()->graphicsTxtItem->setHtml(listBTUN.first()->getGraphicsTextLabel());
        listBTUN.removeFirst();
    }

    QList<UnitNode*> listKSUN(m_ksManager->m_ksMsgManager->listKSUnitNode);
    while(!listKSUN.isEmpty())
    {
        listKSUN.first()->graphicsTxtItem->setHtml(listKSUN.first()->getGraphicsTextLabel());
        listKSUN.removeFirst();
    }

    QList<UnitNode*> listBSKUN(m_bskManager->m_bskMsgManager->listBSKUnitNode);
    while(!listBSKUN.isEmpty())
    {
        listBSKUN.first()->graphicsTxtItem->setHtml(listBSKUN.first()->getGraphicsTextLabel());
        listBSKUN.removeFirst();
    }

    this->updateStatusWorkMode(ui->actionOperatorWorkMode->isChecked());
}

void MainWindow::on_actionEnglish_triggered() /*noexcept*/
{
    this->setEnglish();
}

void MainWindow::setEnglish() /*noexcept*/
{
    emit this->resetTranslator(ruTranslator);

//    sleep(1);
    ui->actionEnglish->setChecked(true);
    ui->actionRussian->setChecked(false);
    ui->retranslateUi(this);
    dialog_SerialPortSetting->retranslate();
    dialog_TableArchive->retranslate();
    m_dbManager->setTypeLanguage("en");

    QList<UnitNode*> listBTUN(m_kmManager->m_btManager->listBTUnitNode);
    while(!listBTUN.isEmpty())
    {
        listBTUN.first()->graphicsTxtItem->setHtml(listBTUN.first()->getGraphicsTextLabel());
        listBTUN.removeFirst();
    }

    QList<UnitNode*> listKSUN(m_ksManager->m_ksMsgManager->listKSUnitNode);
    while(!listKSUN.isEmpty())
    {
        listKSUN.first()->graphicsTxtItem->setHtml(listKSUN.first()->getGraphicsTextLabel());
        listKSUN.removeFirst();
    }

    QList<UnitNode*> listBSKUN(m_bskManager->m_bskMsgManager->listBSKUnitNode);
    while(!listBSKUN.isEmpty())
    {
        listBSKUN.first()->graphicsTxtItem->setHtml(listBSKUN.first()->getGraphicsTextLabel());
        listBSKUN.removeFirst();
    }

    this->updateStatusWorkMode(ui->actionOperatorWorkMode->isChecked());
}

//void MainWindow::setEnglish()


void MainWindow::on_comboBox_0x1F_BSK1_currentIndexChanged(int index) /*noexcept*/
{
    ui->lineEdit_0x1F_BSK1->clear();
    if(0 == index)
    {
        ui->lineEdit_0x1F_BSK1->setEnabled(false);
    }
    else
    {
        ui->lineEdit_0x1F_BSK1->setEnabled(true);
    }
}

void MainWindow::on_comboBox_0x1F_BSK2_currentIndexChanged(int index) /*noexcept*/
{
    ui->lineEdit_0x1F_BSK2->clear();
    if(0 == index)
    {
        ui->lineEdit_0x1F_BSK2->setEnabled(false);
    }
    else
    {
        ui->lineEdit_0x1F_BSK2->setEnabled(true);
    }
}

void MainWindow::on_comboBox_0x1F_BSK3_currentIndexChanged(int index) /*noexcept*/
{
    ui->lineEdit_0x1F_BSK3->clear();
    if(0 == index)
    {
        ui->lineEdit_0x1F_BSK3->setEnabled(false);
    }
    else
    {
        ui->lineEdit_0x1F_BSK3->setEnabled(true);
    }
}

void MainWindow::on_pushButton_zoomPlus_IMG_clicked() /*noexcept*/
{
    if(mainPXM.isNull())
        return;

    qreal scaleValue(1.05);

//    scaleValue = ((qMax(mainPXM.width(), scaledMainPXM.width()) - qMin(mainPXM.width(), scaledMainPXM.width())) +
//                  (qMax(mainPXM.height(), scaledMainPXM.height()) - qMin(mainPXM.height(), scaledMainPXM.height()))) / 2.0;
//    scaleValue = scaleValue * 1.05;


    scaledMainPXM = mainPXM.scaled(scaledMainPXM.width() * scaleValue,
                                   scaledMainPXM.height() * scaleValue,
                                   Qt::KeepAspectRatio,
                                   Qt::FastTransformation);

    mainIMG->setPixmap(scaledMainPXM);
    mainIMG->setPos(mainIMG->x() * scaleValue,
                    mainIMG->y() * scaleValue);
    imgScene->setSceneRect(mainIMG->x(),
                           mainIMG->y(),
                           scaledMainPXM.width(),
                           scaledMainPXM.height());

}

void MainWindow::on_pushButton_zoomMinus_IMG_clicked() /*noexcept*/
{
    if(mainPXM.isNull())
        return;

    qreal scaleValue(0.95);

//    scaleValue = ((qMax(mainPXM.width(), scaledMainPXM.width()) - qMin(mainPXM.width(), scaledMainPXM.width())) +
//                  (qMax(mainPXM.height(), scaledMainPXM.height()) - qMin(mainPXM.height(), scaledMainPXM.height()))) / 2.0;
//    scaleValue = scaleValue * 0.95;

    scaledMainPXM = mainPXM.scaled(scaledMainPXM.width() * scaleValue,
                                   scaledMainPXM.height() * scaleValue,
                                   Qt::KeepAspectRatio,
                                   Qt::FastTransformation);

    mainIMG->setPixmap(scaledMainPXM);
    mainIMG->setPos(mainIMG->x() * scaleValue,
                    mainIMG->y() * scaleValue);
    imgScene->setSceneRect(mainIMG->x(),
                           mainIMG->y(),
                           scaledMainPXM.width(),
                           scaledMainPXM.height());

}

//void MainWindow::on_treeView_UN_KM_clicked(const QModelIndex &index) /*noexcept*/
//{

//}

//void MainWindow::on_tableView_UN_KM_clicked(const QModelIndex &index) /*noexcept*/
//{

//}

void MainWindow::changeCurrentSystemType(TypeUnitNodeSystem typeSystem) /*noexcept*/
{
    this->currentSystemType = typeSystem;

    this->on_pushButton_editMode_MAP_KM_clicked(false);
    this->on_pushButton_editMode_MAP_KS_clicked(false);
    this->on_pushButton_editMode_MAP_BSK_clicked(false);

    switch (this->currentSystemType)
    {
        case KMSystemType:
        {
            ui->actionSetViewIMGArchive->setEnabled(true);
            ui->actionSetViewIMG->setEnabled(true);

            if(ui->actionViewTableOrTree->isChecked())
            {
                ui->stackedWidget_1->setCurrentIndex(1);
            }
            else
            {
                ui->stackedWidget_1->setCurrentIndex(0);
            }

            if(ui->actionSetViewMap->isChecked())
            {
                ui->stackedWidget_2->setCurrentIndex(1);
            }
            else
            {
                ui->stackedWidget_2->setCurrentIndex(0);
            }

            if(ui->actionSetViewAllMSGArchive->isChecked())
            {
                ui->stackedWidget_3->setCurrentIndex(0);
            }
            else
            {
                ui->stackedWidget_3->setCurrentIndex(1);
            }

            ui->stackedWidget_4->setCurrentIndex(0);

            ui->tableView_KM_MSG->scrollToBottom();
            ui->tableView_KM_IMG->scrollToBottom();

            break;
        }

        case KSSystemType:
        {
            ui->actionSetViewIMGArchive->setEnabled(true);
            ui->actionSetViewIMG->setEnabled(true);

            if(ui->actionViewTableOrTree->isChecked())
            {
                ui->stackedWidget_1->setCurrentIndex(3);
            }
            else
            {
                ui->stackedWidget_1->setCurrentIndex(2);
            }

            if(ui->actionSetViewMap->isChecked())
            {
                ui->stackedWidget_2->setCurrentIndex(2);
            }
            else
            {
                ui->stackedWidget_2->setCurrentIndex(0);
            }

            if(ui->actionSetViewAllMSGArchive->isChecked())
            {
                ui->stackedWidget_3->setCurrentIndex(2);
            }
            else
            {
                ui->stackedWidget_3->setCurrentIndex(3);
            }

            ui->stackedWidget_4->setCurrentIndex(1);

            ui->tableView_KS_MSG->scrollToBottom();
            ui->tableView_KS_IMG->scrollToBottom();

            break;
        }

        case BSKSystemType:
        {
            if(ui->actionViewTableOrTree->isChecked())
            {
                ui->stackedWidget_1->setCurrentIndex(5);
            }
            else
            {
                ui->stackedWidget_1->setCurrentIndex(4);
            }

            ui->actionSetViewMap->trigger();
            ui->actionSetViewAllMSGArchive->trigger();

            ui->actionSetViewIMGArchive->setEnabled(false);
            ui->actionSetViewIMG->setEnabled(false);

            ui->stackedWidget_4->setCurrentIndex(2);

            ui->stackedWidget_3->setCurrentIndex(4);

            ui->tableView_BSK_MSG->scrollToBottom();

            break;
        }

        default:
        {
            break;
        }
    }

}

void MainWindow::on_actionSetCurrentSystemKM_triggered() /*noexcept*/
{
    ui->actionSetCurrentSystemKM->setChecked(true);
    ui->actionSetCurrentSystemKS->setChecked(false);
    ui->actionSetCurrentSystemBSK->setChecked(false);
    this->changeCurrentSystemType(KMSystemType);
}

void MainWindow::on_actionSetCurrentSystemKS_triggered() /*noexcept*/
{
    ui->actionSetCurrentSystemKM->setChecked(false);
    ui->actionSetCurrentSystemKS->setChecked(true);
    ui->actionSetCurrentSystemBSK->setChecked(false);
    this->changeCurrentSystemType(KSSystemType);
}

void MainWindow::on_actionSetCurrentSystemBSK_triggered() /*noexcept*/
{
    ui->actionSetCurrentSystemKM->setChecked(false);
    ui->actionSetCurrentSystemKS->setChecked(false);
    ui->actionSetCurrentSystemBSK->setChecked(true);
    this->changeCurrentSystemType(BSKSystemType);
}

void MainWindow::on_pushButton_Select_VK1_KS_clicked() /*noexcept*/
{
    ui->pushButton_Select_VK1_KS->setChecked(true);
    ui->pushButton_Select_VK2_KS->setChecked(false);
}

void MainWindow::on_pushButton_Select_VK2_KS_clicked() /*noexcept*/
{
    ui->pushButton_Select_VK1_KS->setChecked(false);
    ui->pushButton_Select_VK2_KS->setChecked(true);
}

void MainWindow::possUpdateToolDialog_KS(UnitNode *un) /*noexcept*/
{
    if(un != this->initiatorUN_KS)
    {
        return;
    }

    ui->label_ID_View_KS->setText(this->initiatorUN_KS->id.toHex().toUpper());
    if(this->initiatorUN_KS->_abstractName().isEmpty())
    {
        ui->label_Name_View_KS->setText(this->initiatorUN_KS->name.toUpper());
    }
    else
    {
        ui->label_Name_View_KS->setText(QString(this->initiatorUN_KS->name.toUpper()) + " / " + QString(this->initiatorUN_KS->_abstractName()));
    }

    UnitNode *parentUN(this->initiatorUN_KS->unTreeParent);
    if(nullptr != parentUN)
    {
        if(UnknownDeviceType != parentUN->_typeUN() ||
           CPP_KSDeviceType == parentUN->_typeUN() ||
           RT01_KSDeviceType == parentUN->_typeUN() ||
           RT02_KSDeviceType == parentUN->_typeUN() ||
           VK09_KSDeviceType == parentUN->_typeUN() ||
           VK0A_KSDeviceType == parentUN->_typeUN() ||
           VK0B_KSDeviceType == parentUN->_typeUN())
        {
            ui->label_Name_View_Lead_KS->setText(parentUN->data(0).toString() + " " + parentUN->data(3).toString());
        }
        else
        {
            ui->label_Name_View_Lead_KS->setText("***");
        }
    }

    ui->spinBox_0x78_Area_KS->setValue(this->initiatorUN_KS->_numArea());
    ui->spinBox_0x78_Number_KS->setValue(this->initiatorUN_KS->_numNode());
    ui->spinBox_Kit_Num_RM433_KS->setValue(this->initiatorUN_KS->_numKit());


    quint8 numVK(0x01);
    if(ui->pushButton_Select_VK1_KS->isChecked())
    {
        numVK = 0x01;
    }
    if(ui->pushButton_Select_VK2_KS->isChecked())
    {
        numVK = 0x02;
    }

    if(VK09_KSDeviceType == this->initiatorUN_KS->_typeUN() ||
       VK0A_KSDeviceType == this->initiatorUN_KS->_typeUN() ||
       VK0B_KSDeviceType == this->initiatorUN_KS->_typeUN())
    {
        switch((initiatorUN_KS->listSettingVK.at(numVK - 1).modeVK / (quint8)0x10) & (quint8)0x0F)
        {
            case((quint8)0x03):
            {
                ui->label_0x42x43_Inst_Mode_VK_KS->setText(tr("Mode[Day]"));
                break;
            }
            case((quint8)0x0E):
            {
                ui->label_0x42x43_Inst_Mode_VK_KS->setText(tr("Mode[Night]"));
                break;
            }
            default:
            {
                ui->label_0x42x43_Inst_Mode_VK_KS->setText(tr("Mode"));
                break;
            }
        }

        switch(initiatorUN_KS->modeAnxiety)
        {
            case (quint8)0x00:
            {
                ui->comboBox_0x41x28_Inst_Mode_VK_KS->setCurrentIndex(0);
                break;
            }
            case (quint8)0x01:
            {
                ui->comboBox_0x41x28_Inst_Mode_VK_KS->setCurrentIndex(1);
                break;
            }
            default:
            {
                break;
            }
        }

        switch(initiatorUN_KS->listSettingVK.at(numVK - 1).modeVK & 0x0F)
        {
            case 0x01:
            {
                ui->comboBox_0x42x43_Inst_Mode_VK_KS->setCurrentIndex(0);
                break;
            }
            case 0x02:
            {
                ui->comboBox_0x42x43_Inst_Mode_VK_KS->setCurrentIndex(1);
                break;
            }
            case 0x03:
            {
                ui->comboBox_0x42x43_Inst_Mode_VK_KS->setCurrentIndex(2);
                break;
            }
            default:
            {
                break;
            }
        }

        ui->spinBox_0x42x43_Inst_T1_VK_KS->setValue(initiatorUN_KS->listSettingVK.at(numVK - 1).timeout1);
    //    ui->spinBox_0x42x43_Inst_T2_VK_KS->setValue(initiatorUN_KS->listSettingVK.at(numVK - 1).timeout2);
    //    ui->spinBox_0x42x43_Inst_T3_VK_KS->setValue(initiatorUN_KS->listSettingVK.at(numVK - 1).timeout3);
        ui->spinBox_0x42x43_Inst_T2_VK_KS->setValue(0);
        ui->spinBox_0x42x43_Inst_T3_VK_KS->setValue(1);

        if(VK09_KSDeviceType == this->initiatorUN_KS->_typeUN() ||
           VK0A_KSDeviceType == this->initiatorUN_KS->_typeUN())
        {
            ui->pushButton_Switch_On_Self_D_KS->setEnabled(false);
            ui->pushButton_Switch_Off_Self_D_KS->setEnabled(false);
        }
        else
        {
            ui->pushButton_Switch_On_Self_D_KS->setEnabled(true);
            ui->pushButton_Switch_Off_Self_D_KS->setEnabled(true);
            if(0xFF != initiatorUN_KS->flag_selfD)
            {
                if(0x01 == initiatorUN_KS->flag_selfD)
                {
                    ui->pushButton_Switch_On_Self_D_KS->setEnabled(false);
                    ui->pushButton_Switch_Off_Self_D_KS->setEnabled(true);
                }
                if(0x00 == initiatorUN_KS->flag_selfD)
                {
                    ui->pushButton_Switch_On_Self_D_KS->setEnabled(true);
                    ui->pushButton_Switch_Off_Self_D_KS->setEnabled(false);
                }
            }

        }

        ui->pushButton_Switch_On_Other_D1_KS->setEnabled(true);
        ui->pushButton_Switch_Off_Other_D1_KS->setEnabled(true);
        if(0xFF != this->initiatorUN_KS->flag_otherD1)
        {
            if(0x01 == this->initiatorUN_KS->flag_otherD1)
            {
                ui->pushButton_Switch_On_Other_D1_KS->setEnabled(false);
                ui->pushButton_Switch_Off_Other_D1_KS->setEnabled(true);
            }
            if(0x00 == this->initiatorUN_KS->flag_otherD1)
            {
                ui->pushButton_Switch_On_Other_D1_KS->setEnabled(true);
                ui->pushButton_Switch_Off_Other_D1_KS->setEnabled(false);
            }
        }

        ui->pushButton_Switch_On_Other_D2_KS->setEnabled(true);
        ui->pushButton_Switch_Off_Other_D2_KS->setEnabled(true);
        if(0xFF != this->initiatorUN_KS->flag_otherD2)
        {
            if(0x01 == initiatorUN_KS->flag_otherD2)
            {
                ui->pushButton_Switch_On_Other_D2_KS->setEnabled(false);
                ui->pushButton_Switch_Off_Other_D2_KS->setEnabled(true);
            }
            if(0x00 == initiatorUN_KS->flag_otherD2)
            {
                ui->pushButton_Switch_On_Other_D2_KS->setEnabled(true);
                ui->pushButton_Switch_Off_Other_D2_KS->setEnabled(false);
            }
        }

        QList<QByteArray> listD(initiatorUN_KS->_listIDBSK(numVK));
        ui->comboBox_List_D_VK_KS->clear();
    //    ui->comboBox_Remove_D_VK_KS->clear();
        while(1 < ui->comboBox_Remove_D_VK_KS->count())
        {
            ui->comboBox_Remove_D_VK_KS->removeItem(1);
        }
        for(int i(0), n(listD.size()); i < n; i++)
        {
            if(listD.at(i).toHex().toUInt(0, 16))
            {
                ui->comboBox_List_D_VK_KS->addItem(ProxyMetod().getNameSO(listD.at(i)) + "[" + listD.at(i).toHex().toUpper() + "]", QVariant(listD.at(i)));
                ui->comboBox_Remove_D_VK_KS->addItem(ProxyMetod().getNameSO(listD.at(i)) + "[" + listD.at(i).toHex().toUpper() + "]", QVariant(listD.at(i)));
            }
        }
    }
}

void MainWindow::on_pushButton_0x78_Change_Area_Number_KS_clicked() /*noexcept*/
{
    if(this->initiatorUN_KS)
    {
        this->m_ksManager->cmdRequest_0x41(this->initiatorUN_KS,
                                           0x78,
                                           ui->spinBox_0x78_Area_KS->value(),
                                           ui->spinBox_0x78_Number_KS->value());
    }
}

void MainWindow::on_pushButton_0x42x43_Inst_Mode_VK_KS_clicked() /*noexcept*/
{
    if(this->initiatorUN_KS)
    {
        quint8 cmd;
        if(ui->pushButton_Select_VK1_KS->isChecked())
            cmd = 0x42;
        if(ui->pushButton_Select_VK2_KS->isChecked())
            cmd = 0x43;
        this->m_ksManager->cmdRequest_0x41(this->initiatorUN_KS,
                                           cmd,
                                           ui->comboBox_0x42x43_Inst_Mode_VK_KS->currentIndex() + 1,
                                           ui->spinBox_0x42x43_Inst_T1_VK_KS->value(),
//                                           ui->spinBox_0x42x43_Inst_T2_VK_KS->value(),
//                                           ui->spinBox_0x42x43_Inst_T3_VK_KS->value());
                                           0x00,
                                           0x01);
    }
}

void MainWindow::on_pushButton_0xFD_Get_Shot_KS_clicked() /*noexcept*/
{
    if(this->initiatorUN_KS)
    {
        quint8 numVK(0x01);
        if(ui->pushButton_Select_VK1_KS->isChecked())
            numVK = 0x01;
        if(ui->pushButton_Select_VK2_KS->isChecked())
            numVK = 0x02;
        this->m_ksManager->cmdRequest_0x41(this->initiatorUN_KS,
                                           0xFD,
                                           numVK);
    }
}

void MainWindow::on_pushButton_SwitchON_0x41x02_BSK_clicked() /*noexcept*/
{
    if(this->initiatorUN_BSK)
    {
        this->m_bskManager->request_0x41(this->initiatorUN_BSK,
                                         0x02);
    }
}

void MainWindow::on_pushButton_SwitchOFF_0x41x01_BSK_clicked() /*noexcept*/
{
    if(this->initiatorUN_BSK)
    {
        this->m_bskManager->request_0x41(this->initiatorUN_BSK,
                                         0x01);
    }
}

void MainWindow::on_pushButton_SendDK_0x41x55_BSK_clicked() /*noexcept*/
{
    if(this->initiatorUN_BSK)
    {
        this->m_bskManager->request_0x41(this->initiatorUN_BSK,
                                         0x55);
    }
}

void MainWindow::on_checkBox_BSK_S_Winter_clicked(bool checked) /*noexcept*/
{
    if(this->initiatorUN_BSK)
    {
        ui->checkBox_BSK_S_Summer->setChecked(!checked);
        this->m_bskManager->request_0x41(this->initiatorUN_BSK,
                                         0x75,
                                         0x00,
                                         checked ? 0x01 : 0x00);
    }
}

void MainWindow::on_checkBox_BSK_S_Summer_clicked(bool checked) /*noexcept*/
{
    if(this->initiatorUN_BSK)
    {
        ui->checkBox_BSK_S_Winter->setChecked(!checked);
        this->m_bskManager->request_0x41(this->initiatorUN_BSK,
                                         0x75,
                                         0x00,
                                         checked ? 0x00 : 0x01);
    }
}
void MainWindow::on_checkBox_BSK_S_Alarm_clicked(bool checked) /*noexcept*/
{
    if(this->initiatorUN_BSK)
    {
        this->m_bskManager->request_0x41(this->initiatorUN_BSK,
                                         0x75,
                                         0x02,
                                         checked ? 0x01 : 0x00);
    }
}

void MainWindow::on_checkBox_BSK_S_Odin_clicked(bool checked) /*noexcept*/
{
    if(this->initiatorUN_BSK)
    {
        this->m_bskManager->request_0x41(this->initiatorUN_BSK,
                                         0x75,
                                         0x03,
                                         checked ? 0x01 : 0x00);
    }
}

void MainWindow::on_checkBox_BSK_S_Group_clicked(bool checked) /*noexcept*/
{
    if(this->initiatorUN_BSK)
    {
        this->m_bskManager->request_0x41(this->initiatorUN_BSK,
                                         0x75,
                                         0x04,
                                         checked ? 0x01 : 0x00);
    }
}

void MainWindow::on_checkBox_BSK_S_TS_clicked(bool checked) /*noexcept*/
{
    if(this->initiatorUN_BSK)
    {
        this->m_bskManager->request_0x41(this->initiatorUN_BSK,
                                         0x75,
                                         0x05,
                                         checked ? 0x01 : 0x00);
    }
}

void MainWindow::on_checkBox_BSK_S_Far_TS_clicked(bool checked) /*noexcept*/
{
    if(this->initiatorUN_BSK)
    {
        this->m_bskManager->request_0x41(this->initiatorUN_BSK,
                                         0x75,
                                         0x06,
                                         checked ? 0x01 : 0x00);
    }
}

void MainWindow::on_checkBox_BSK_S_Unknown_clicked(bool checked) /*noexcept*/
{
    if(this->initiatorUN_BSK)
    {
        this->m_bskManager->request_0x41(this->initiatorUN_BSK,
                                         0x75,
                                         0x07,
                                         checked ? 0x01 : 0x00);
    }
}

void MainWindow::on_checkBox_BSK_S_Train_clicked(bool checked) /*noexcept*/
{
    if(this->initiatorUN_BSK)
    {
        this->m_bskManager->request_0x41(this->initiatorUN_BSK,
                                         0x75,
                                         0x08,
                                         checked ? 0x01 : 0x00);
    }
}

void MainWindow::on_checkBox_BSK_S_Direction_clicked(bool checked) /*noexcept*/
{
    if(this->initiatorUN_BSK)
    {
        this->m_bskManager->request_0x41(this->initiatorUN_BSK,
                                         0x75,
                                         0x09,
                                         checked ? 0x01 : 0x00);
    }
}

void MainWindow::on_pushButton_GetConfig_0x41x7B_BSK_clicked() /*noexcept*/
{
    if(this->initiatorUN_BSK)
    {
        this->m_bskManager->request_0x41(this->initiatorUN_BSK,
                                         0x7B);
    }
}

void MainWindow::on_pushButton_ResetSO_BSK_clicked() /*noexcept*/
{
    if(this->initiatorUN_BSK)
    {
//        this->initiatorUN_BSK->updateFlagAlarm(false);
        this->initiatorUN_BSK->offAlarmCondition();
        this->m_bskManager->emitInsertNewMSG(this->initiatorUN_BSK,
                                             0xFF,
                                             QString("Reset alarm").toUtf8(),
                                             true,
                                             Command_TypeGroupMSG);
    }
}

void MainWindow::on_pushButton_Request_List_D_VK_KS_clicked() /*noexcept*/
{
    if(this->initiatorUN_KS)
    {
        quint8 numVK(0x00);
//        if(ui->pushButton_Select_VK1_KS->isChecked())
//            numVK = 0x01;
//        if(ui->pushButton_Select_VK2_KS->isChecked())
//            numVK = 0x02;
        this->m_ksManager->cmdRequest_0x41(this->initiatorUN_KS,
                                           0x45,
                                           numVK);
    }
}

void MainWindow::on_pushButton_Add_D_VK_KS_clicked() /*noexcept*/
{
    if(this->initiatorUN_KS)
    {
        quint8 typeS(0x00);
        switch(ui->comboBox_Add_Type_D_VK_KS->currentIndex())
        {
            case 0://БСК-С		0x02
            {
                typeS = 0x02;
                break;
            }
            case 1://БСК-РЛО		0x03
            {
                typeS = 0x03;
                break;
            }
            case 2://БСК-РЛД		0x04
            {
                typeS = 0x04;
                break;
            }
            case 3://БСК-РВД		0x05
            {
                typeS = 0x05;
                break;
            }
            case 4://БСК-МСО		0x06
            {
                typeS = 0x06;
                break;
            }
            case 5://БСК-БВУ		0x07
            {
                typeS = 0x07;
                break;
            }           
            case 6://БСК-О		0x0A
            {
                typeS = 0x0A;
                break;
            }
            case 7://БСК-ИК		0x0B
            {
                typeS = 0x0B;
                break;
            }
            case 8://БСК-РВ		0x0C
            {
                typeS = 0x0C;
                break;
            }
            case 9://БСК-РВП	0x0D
            {
                typeS = 0x0D;
                break;
            }
            case 10://БСК-ПКСЧ	0x0E
            {
                typeS = 0x0E;
                break;
            }
            case 11://БСК-ВСО	0x0F
            {
                typeS = 0x0F;
                break;
            }
            default:
            {
                typeS = 0x00;
                break;
            }
        }

        quint16 nomS(ui->lineEdit_Add_ID_D_VK_KS->text().toUInt(0, 16));
        quint8 hNomS(nomS / 0x0100),
               lNomS(nomS & 0x00FF);

        quint8 numVK(0x00);
        if(ui->pushButton_Select_VK1_KS->isChecked())
            numVK = 0x01;
        if(ui->pushButton_Select_VK2_KS->isChecked())
            numVK = 0x02;
        QByteArray idBSK,
                   empetyIDBSK;

        empetyIDBSK.append((char)0x00);
        empetyIDBSK.append((char)0x00);

        idBSK.append(hNomS + (typeS * 0x10));
        idBSK.append(lNomS);

        if(this->initiatorUN_KS->_listIDBSK(numVK).contains(idBSK))
        {
            return;
        }

        int    indexBSK(this->initiatorUN_KS->_listIDBSK(numVK).indexOf(empetyIDBSK));

        if(-1 == indexBSK)
        {
            return;
        }

        this->m_ksManager->cmdRequest_0x41(this->initiatorUN_KS,
                                           0x47,
                                           numVK,
                                           0x00,//indexBSK + 1,
                                           hNomS + (typeS * 0x10),
                                           lNomS);

//        timer_Request_List_D_VK_KS->singleShot(2000, ui->pushButton_Request_List_D_VK_KS, SLOT(click()));
    }
}

void MainWindow::on_pushButton_Remove_D_VK_KS_clicked() /*noexcept*/
{
    if(this->initiatorUN_KS &&
       !ui->comboBox_Remove_D_VK_KS->currentText().isEmpty())
    {

        quint8 numVK(0x00);
        if(ui->pushButton_Select_VK1_KS->isChecked())
            numVK = 0x01;
        if(ui->pushButton_Select_VK2_KS->isChecked())
            numVK = 0x02;

        if(0 == ui->comboBox_Remove_D_VK_KS->currentIndex())
        {
            this->m_ksManager->cmdRequest_0x41(this->initiatorUN_KS,
                                               0x46,
                                               numVK);
            return;
        }
        QString strBSK(ui->comboBox_Remove_D_VK_KS->currentText());
        strBSK.chop(1);
        strBSK = strBSK.right(4);


        quint8 hIDBSK((quint16)strBSK.toUInt(0, 16) / 0x0100),
               lIDBSK((quint16)strBSK.toUInt(0, 16) & 0x00FF);

        QByteArray baIDBSK;
        baIDBSK.append(hIDBSK);
        baIDBSK.append(lIDBSK);

        quint8 indexBSK(this->initiatorUN_KS->_listIDBSK(numVK).indexOf(baIDBSK));

        this->m_ksManager->cmdRequest_0x41(this->initiatorUN_KS,
                                           0x48,
                                           numVK,
                                           0x00,//indexBSK + 1,
                                           hIDBSK,
                                           lIDBSK);

//        timer_Request_List_D_VK_KS->singleShot(2000, ui->pushButton_Request_List_D_VK_KS, SLOT(click()));
    }

}

void MainWindow::on_pushButton_fullScreen_IMG_clicked() /*noexcept*/
{
    if(mainPXM.isNull())
    {
        return;
    }

    dialog_ImageViwer->setMainPXM(mainPXM,
                                  ui->label_viewIMGLable->text(),
                                  this->imgRecord,
                                  numCurrentShot);

    dialog_ImageViwer->showFullScreen();
//    dialog_ImageViwer->setWindowState(dialog_ImageViwer->windowState() ^ Qt::WindowFullScreen);
}

void MainWindow::customContextMenuRequested_UN(UnitNode *un, QPoint pos) /*noexcept*/
{
    if(un)
    {
        QMenu *targetContextMenu(nullptr);
        switch(un->_typeSystem())
        {
            case KMSystemType:
            {
                this->table_KM_UN_ContextMenu->clear();
                ui->actionName_Target_KM_UN->setEnabled(false);
                QString nameTarget(un->data(0).toString() + "[" + un->data(3).toString().toUpper() + "]");
                if(un->_rssi())
                {
                    nameTarget.append(" RSSI[%1]");
                    nameTarget = nameTarget.arg(un->_rssi());
                }
                ui->actionName_Target_KM_UN->setText(nameTarget);
                ui->actionName_Target_KM_UN->setIcon(QIcon(un->getViewPXM()));
                this->table_KM_UN_ContextMenu->addAction(ui->actionName_Target_KM_UN);
                this->table_KM_UN_ContextMenu->addSeparator();
                if(un->flag_failure)
                {
                    this->table_KM_UN_ContextMenu->addAction(ui->actionResetFailure_KM);
                    this->table_KM_UN_ContextMenu->addSeparator();
                }
                if(un->flag_discharge)
                {
                    this->table_KM_UN_ContextMenu->addAction(ui->actionResetDischarge_KM);
                    this->table_KM_UN_ContextMenu->addSeparator();
                }
                if(un->flag_alarm)
                {
                    this->table_KM_UN_ContextMenu->addAction(ui->actionResetAlarm_KM);
                    this->table_KM_UN_ContextMenu->addSeparator();
                }

                if(RT_KMDeviceType == un->_typeUN())
                {
                    if(un->btLink.isEmpty())
                    {
                        this->table_KM_UN_ContextMenu->addAction(ui->actionBuildNetTopology);
                        this->table_KM_UN_ContextMenu->addSeparator();
                    }
                    this->table_KM_UN_ContextMenu->addAction(ui->action_requestInquiry_KM);
                    this->table_KM_UN_ContextMenu->addAction(ui->action_requestList_KM);
                    if(!un->btLink.isEmpty())
                    {
                        this->table_KM_UN_ContextMenu->addAction(ui->action_requestKill_KM);
                        this->table_KM_UN_ContextMenu->addAction(ui->action_requestTxpowerRssi_KM);
                    }
                }
                if(VK_KMDeviceType == un->_typeUN())
                {
                    this->table_KM_UN_ContextMenu->addAction(ui->actionVK1_FF_Inquiry_KM);
                    this->table_KM_UN_ContextMenu->addAction(ui->actionVK1_Mode_1_KM);
                    this->table_KM_UN_ContextMenu->addAction(ui->actionVK1_Mode_2_KM);
                    this->table_KM_UN_ContextMenu->addSeparator();
                    this->table_KM_UN_ContextMenu->addAction(ui->action_requestKill_KM);
                    this->table_KM_UN_ContextMenu->addAction(ui->action_requestTxpowerRssi_KM);
                }
                if(Unknown_KMDeviceType == un->_typeUN())
                {
                    this->table_KM_UN_ContextMenu->addAction(ui->action_requestInquiry_KM);
                    this->table_KM_UN_ContextMenu->addAction(ui->action_requestList_KM);
                    if(!un->btLink.isEmpty())
                    {
                        this->table_KM_UN_ContextMenu->addAction(ui->action_requestKill_KM);
                        this->table_KM_UN_ContextMenu->addAction(ui->action_requestTxpowerRssi_KM);
                    }
                }
                //
                if(ui->actionOperatorWorkMode->isChecked())
                {
                    this->table_KM_UN_ContextMenu->addSeparator();
                    //this->table_KM_UN_ContextMenu->addAction(ui->actionSetAsReferenceGeoPoint);
                    this->table_KM_UN_ContextMenu->addAction(ui->actionHiddingUN);
                }
                targetContextMenu = this->table_KM_UN_ContextMenu;
                break;
            };
            case KSSystemType:
            {
                this->table_KS_UN_ContextMenu->clear();
                ui->actionName_Target_KS_UN->setEnabled(false);
                QString nameTarget(un->data(0).toString() + "[" + un->data(3).toString().toUpper() + "]");
                if(un->_rssi())
                {
                    nameTarget.append(" RSSI[%1]");
                    nameTarget = nameTarget.arg(un->_rssi());
                }
                ui->actionName_Target_KS_UN->setText(nameTarget);
                ui->actionName_Target_KS_UN->setIcon(QIcon(un->getViewPXM()));
                this->table_KS_UN_ContextMenu->addAction(ui->actionName_Target_KS_UN);
                this->table_KS_UN_ContextMenu->addSeparator();

                if(un->flag_failure)
                {
                    this->table_KS_UN_ContextMenu->addAction(ui->actionResetFailure_KS);
                    this->table_KS_UN_ContextMenu->addSeparator();
                }
                if(un->flag_discharge)
                {
                    this->table_KS_UN_ContextMenu->addAction(ui->actionResetDischarge_KS);
                    this->table_KS_UN_ContextMenu->addSeparator();
                }
                if(un->flag_alarm)
                {
                    this->table_KS_UN_ContextMenu->addAction(ui->actionResetAlarm_KS);
                    this->table_KS_UN_ContextMenu->addSeparator();
                }

                this->table_KS_UN_ContextMenu->addAction(ui->actionBuildNetTopology_Target);
                this->table_KS_UN_ContextMenu->addSeparator();

                if(RT01_KSDeviceType == un->_typeUN() ||
                   RT02_KSDeviceType == un->_typeUN())
                {
                    if(ui->actionOperatorWorkMode->isChecked())
                    {
                        this->table_KS_UN_ContextMenu->addSeparator();
                        this->table_KS_UN_ContextMenu->addAction(ui->actionVK1_Test_FF_Inquiry_KS);
                        this->table_KS_UN_ContextMenu->addAction(ui->actionVK2_Test_FF_Inquiry_KS);
                    }
                }
                if(VK0A_KSDeviceType == un->_typeUN() ||
                   VK0B_KSDeviceType == un->_typeUN())
                {
                    this->table_KS_UN_ContextMenu->addAction(ui->actionVK1_FF_Inquiry_KS);
                    this->table_KS_UN_ContextMenu->addAction(ui->actionVK1_Mode_1_KS);
                    this->table_KS_UN_ContextMenu->addAction(ui->actionVK1_Mode_2_KS);
                    if(ui->actionOperatorWorkMode->isChecked())
                    {
                        this->table_KS_UN_ContextMenu->addSeparator();
                        this->table_KS_UN_ContextMenu->addAction(ui->actionVK1_Test_FF_Inquiry_KS);
                    }
                }
                if(VK09_KSDeviceType == un->_typeUN())
                {
                    this->table_KS_UN_ContextMenu->addAction(ui->actionVK1_FF_Inquiry_KS);
                    this->table_KS_UN_ContextMenu->addAction(ui->actionVK1_Mode_1_KS);
                    this->table_KS_UN_ContextMenu->addAction(ui->actionVK1_Mode_2_KS);
                    this->table_KS_UN_ContextMenu->addSeparator();
                    this->table_KS_UN_ContextMenu->addAction(ui->actionVK2_FF_Inquiry_KS);
                    this->table_KS_UN_ContextMenu->addAction(ui->actionVK2_Mode_1_KS);
                    this->table_KS_UN_ContextMenu->addAction(ui->actionVK2_Mode_2_KS);
                    if(ui->actionOperatorWorkMode->isChecked())
                    {
                        this->table_KS_UN_ContextMenu->addSeparator();
                        this->table_KS_UN_ContextMenu->addAction(ui->actionVK1_Test_FF_Inquiry_KS);
                        this->table_KS_UN_ContextMenu->addAction(ui->actionVK2_Test_FF_Inquiry_KS);
                    }
                }
                if(ui->actionOperatorWorkMode->isChecked())
                {
                    this->table_KS_UN_ContextMenu->addSeparator();
//                    this->table_KS_UN_ContextMenu->addAction(ui->actionSetAsReferenceGeoPoint);
                    this->table_KS_UN_ContextMenu->addAction(ui->actionHiddingUN);
                }
                targetContextMenu = this->table_KS_UN_ContextMenu;
                break;
            };
            case BSKSystemType:
            {
                this->table_BSK_UN_ContextMenu->clear();
                ui->actionName_Target_BSK_UN->setEnabled(false);
                QString nameTarget(un->data(0).toString() + "[" + un->data(3).toString().toUpper() + "]");
                if(un->_rssi())
                {
                    nameTarget.append(" RSSI[%1]");
                    nameTarget = nameTarget.arg(un->_rssi());
                }
                ui->actionName_Target_BSK_UN->setText(nameTarget);
                ui->actionName_Target_BSK_UN->setIcon(QIcon(un->getViewPXM()));
                this->table_BSK_UN_ContextMenu->addAction(ui->actionName_Target_BSK_UN);
                this->table_BSK_UN_ContextMenu->addSeparator();

                if(un->flag_failure)
                {
                    this->table_BSK_UN_ContextMenu->addAction(ui->actionResetFailure_BSK);
                    this->table_BSK_UN_ContextMenu->addSeparator();
                }
                if(un->flag_discharge)
                {
                    this->table_BSK_UN_ContextMenu->addAction(ui->actionResetDischarge_BSK);
                    this->table_BSK_UN_ContextMenu->addSeparator();
                }

                this->table_BSK_UN_ContextMenu->addAction(ui->actionSwitch_On_BSK);
                this->table_BSK_UN_ContextMenu->addAction(ui->actionSwitch_Off_BSK);
                if(un->flag_alarm)
                {
                    ui->actionReset_BSK->setIcon(QIcon(":/icon/warning_red_24x24.png"));
                    this->table_BSK_UN_ContextMenu->addAction(ui->actionReset_BSK);
                }
                else
                {
                    ui->actionReset_BSK->setIcon(QIcon(":/icon/warning_24x24.png"));
                }

                this->table_BSK_UN_ContextMenu->addAction(ui->actionSend_DK_BSK);
                if(ui->actionOperatorWorkMode->isChecked())
                {
                    this->table_BSK_UN_ContextMenu->addSeparator();
                    this->table_BSK_UN_ContextMenu->addAction(ui->actionSetAsReferenceGeoPoint);
                    this->table_BSK_UN_ContextMenu->addAction(ui->actionHiddingUN);
                }
                targetContextMenu = this->table_BSK_UN_ContextMenu;
                break;
            };
            default:
            {
                return;
            };
        }
        targetContextMenu->exec(pos);
    }
}

void MainWindow::on_tableView_UN_BSK_customContextMenuRequested(const QPoint &pos) /*noexcept*/
{
    UnitNode *un(modelTableUN_BSK->clickedUN(ui->tableView_UN_BSK->indexAt(pos)));
//    this->customContextMenuRequested_UN_BSK(un, pos);
    this->customContextMenuRequested_UN(un,
                                        pos +
                                        this->pos() +
                                        ui->centralWidget->pos() +
                                        ui->splitter_horizontal->pos() +
                                        ui->splitter_vertical->pos() +
                                        ui->stackedWidget_1->pos() +
                                        ui->tableView_UN_BSK->pos() +
                                        QPoint(0, ui->mainToolBar->height()) +
                                        QPoint(18, 25));
}

void MainWindow::on_treeView_UN_BSK_customContextMenuRequested(const QPoint &pos) /*noexcept*/
{
    UnitNode *un(modelTreeUN_BSK->clickedUN(ui->treeView_UN_BSK->indexAt(pos)));
//    this->customContextMenuRequested_UN_BSK(un, pos);
    this->customContextMenuRequested_UN(un,
                                        pos +
                                        this->pos() +
                                        ui->centralWidget->pos() +
                                        ui->splitter_horizontal->pos() +
                                        ui->splitter_vertical->pos() +
                                        ui->stackedWidget_1->pos() +
                                        ui->tableView_UN_BSK->pos() +
                                        QPoint(0, ui->mainToolBar->height()) +
                                        QPoint(18, 25));
}

//void MainWindow::customContextMenuRequested_UN_BSK(UnitNode *un, QPoint pos) /*noexcept*/
//{
//    if(un)
//    {
//        ui->actionName_Target_BSK_UN->setText(un->data(0).toString() + " " + un->data(3).toString().toUpper());
//        this->table_BSK_UN_ContextMenu->exec(pos +
//                                             this->pos() +
//                                             ui->centralWidget->pos() +
//                                             ui->splitter_horizontal->pos() +
//                                             ui->splitter_vertical->pos() +
//                                             ui->stackedWidget_1->pos() +
//                                             ui->tableView_UN_BSK->pos() +
//                                             QPoint(0, ui->mainToolBar->height()) +
//                                             QPoint(18, 25));

//    }
//}

void MainWindow::on_tableView_UN_KS_customContextMenuRequested(const QPoint &pos) /*noexcept*/
{
    UnitNode *un(modelTableUN_KS->clickedUN(ui->tableView_UN_KS->indexAt(pos)));
//    this->customContextMenuRequested_UN_KS(un, pos);
    this->customContextMenuRequested_UN(un,
                                        pos +
                                        this->pos() +
                                        ui->centralWidget->pos() +
                                        ui->splitter_horizontal->pos() +
                                        ui->splitter_vertical->pos() +
                                        ui->stackedWidget_1->pos() +
                                        ui->tableView_UN_BSK->pos() +
                                        QPoint(0, ui->mainToolBar->height()) +
                                        QPoint(18, 25));
}

void MainWindow::on_treeView_UN_KS_customContextMenuRequested(const QPoint &pos) /*noexcept*/
{
    UnitNode *un(modelTreeUN_KS->clickedUN(ui->treeView_UN_KS->indexAt(pos)));
//    this->customContextMenuRequested_UN_KS(un, pos);
    this->customContextMenuRequested_UN(un,
                                        pos +
                                        this->pos() +
                                        ui->centralWidget->pos() +
                                        ui->splitter_horizontal->pos() +
                                        ui->splitter_vertical->pos() +
                                        ui->stackedWidget_1->pos() +
                                        ui->tableView_UN_BSK->pos() +
                                        QPoint(0, ui->mainToolBar->height()) +
                                        QPoint(18, 25));
}

//void MainWindow::customContextMenuRequested_UN_KS(UnitNode *un, QPoint pos) /*noexcept*/
//{
//    if(un)
//    {
//        if(CPP_KSDeviceType == un->_typeUN() ||
//           RT_KSDeviceType == un->_typeUN())
//        {
//            return;
//        }
//        if(VK0A_KSDeviceType == un->_typeUN() ||
//           VK0B_KSDeviceType == un->_typeUN())
//        {
//            ui->actionVK1_FF_Inquiry_KS->setEnabled(true);
//            ui->actionVK1_Mode_1_KS->setEnabled(true);
//            ui->actionVK1_Mode_2_KS->setEnabled(true);
//            ui->actionVK1_Test_FF_Inquiry_KS->setEnabled(true);
//            ui->actionVK2_FF_Inquiry_KS->setEnabled(false);
//            ui->actionVK2_Mode_1_KS->setEnabled(false);
//            ui->actionVK2_Mode_2_KS->setEnabled(false);
//            ui->actionVK2_Test_FF_Inquiry_KS->setEnabled(false);
//        }
//        if(VK09_KSDeviceType == un->_typeUN())
//        {
//            ui->actionVK1_FF_Inquiry_KS->setEnabled(true);
//            ui->actionVK1_Mode_1_KS->setEnabled(true);
//            ui->actionVK1_Mode_2_KS->setEnabled(true);
//            ui->actionVK1_Test_FF_Inquiry_KS->setEnabled(true);
//            ui->actionVK2_FF_Inquiry_KS->setEnabled(true);
//            ui->actionVK2_Mode_1_KS->setEnabled(true);
//            ui->actionVK2_Mode_2_KS->setEnabled(true);
//            ui->actionVK2_Test_FF_Inquiry_KS->setEnabled(true);

//        }

//        ui->actionName_Target_KS_UN->setText(un->data(0).toString() + " " + un->data(3).toString().toUpper());
//        this->table_KS_UN_ContextMenu->exec(pos +
//                                             this->pos() +
//                                             ui->centralWidget->pos() +
//                                             ui->splitter_horizontal->pos() +
//                                             ui->splitter_vertical->pos() +
//                                             ui->stackedWidget_1->pos() +
//                                             ui->treeView_UN_KS->pos() +
//                                             QPoint(0, ui->mainToolBar->height()) +
//                                             QPoint(18, 25));

//    }
//}

void MainWindow::on_actionVK1_FF_Inquiry_KS_triggered() /*noexcept*/
{
    ui->pushButton_Select_VK1_KS->click();
    ui->pushButton_0xFD_Get_Shot_KS->click();
}

void MainWindow::on_actionVK1_Mode_1_KS_triggered() /*noexcept*/
{
    ui->pushButton_Select_VK1_KS->click();
    ui->comboBox_0x42x43_Inst_Mode_VK_KS->setCurrentIndex(0);
    ui->spinBox_0x42x43_Inst_T1_VK_KS->setValue(0x0A);
    ui->spinBox_0x42x43_Inst_T2_VK_KS->setValue(0x00);
    ui->spinBox_0x42x43_Inst_T3_VK_KS->setValue(0x01);
    ui->pushButton_0x42x43_Inst_Mode_VK_KS->click();
}

void MainWindow::on_actionVK1_Mode_2_KS_triggered() /*noexcept*/
{
    ui->pushButton_Select_VK1_KS->click();
    ui->comboBox_0x42x43_Inst_Mode_VK_KS->setCurrentIndex(1);
    ui->spinBox_0x42x43_Inst_T1_VK_KS->setValue(0x0A);
    ui->spinBox_0x42x43_Inst_T2_VK_KS->setValue(0x00);
    ui->spinBox_0x42x43_Inst_T3_VK_KS->setValue(0x01);
    ui->pushButton_0x42x43_Inst_Mode_VK_KS->click();
}

void MainWindow::on_actionVK2_FF_Inquiry_KS_triggered() /*noexcept*/
{
    ui->pushButton_Select_VK2_KS->click();
    ui->pushButton_0xFD_Get_Shot_KS->click();
}

void MainWindow::on_actionVK2_Mode_1_KS_triggered() /*noexcept*/
{
    ui->pushButton_Select_VK2_KS->click();
    ui->comboBox_0x42x43_Inst_Mode_VK_KS->setCurrentIndex(0);
    ui->spinBox_0x42x43_Inst_T1_VK_KS->setValue(0x0A);
    ui->spinBox_0x42x43_Inst_T2_VK_KS->setValue(0x00);
    ui->spinBox_0x42x43_Inst_T3_VK_KS->setValue(0x01);
    ui->pushButton_0x42x43_Inst_Mode_VK_KS->click();
}

void MainWindow::on_actionVK2_Mode_2_KS_triggered() /*noexcept*/
{
    ui->pushButton_Select_VK2_KS->click();
    ui->comboBox_0x42x43_Inst_Mode_VK_KS->setCurrentIndex(1);
    ui->spinBox_0x42x43_Inst_T1_VK_KS->setValue(0x0A);
    ui->spinBox_0x42x43_Inst_T2_VK_KS->setValue(0x00);
    ui->spinBox_0x42x43_Inst_T3_VK_KS->setValue(0x01);
    ui->pushButton_0x42x43_Inst_Mode_VK_KS->click();
}


void MainWindow::on_tableView_UN_KM_customContextMenuRequested(const QPoint &pos) /*noexcept*/
{
    UnitNode *un(modelTableUN_KM->clickedUN(ui->tableView_UN_KM->indexAt(pos)));
//    this->customContextMenuRequested_UN_KM(un, pos);
    this->customContextMenuRequested_UN(un,
                                        pos +
                                        this->pos() +
                                        ui->centralWidget->pos() +
                                        ui->splitter_horizontal->pos() +
                                        ui->splitter_vertical->pos() +
                                        ui->stackedWidget_1->pos() +
                                        ui->tableView_UN_BSK->pos() +
                                        QPoint(0, ui->mainToolBar->height()) +
                                        QPoint(18, 25));
}

void MainWindow::on_treeView_UN_KM_customContextMenuRequested(const QPoint &pos) /*noexcept*/
{
    UnitNode *un(modelTreeUN_KM->clickedUN(ui->treeView_UN_KM->indexAt(pos)));
//    this->customContextMenuRequested_UN_KM(un, pos);
    this->customContextMenuRequested_UN(un,
                                        pos +
                                        this->pos() +
                                        ui->centralWidget->pos() +
                                        ui->splitter_horizontal->pos() +
                                        ui->splitter_vertical->pos() +
                                        ui->stackedWidget_1->pos() +
                                        ui->tableView_UN_BSK->pos() +
                                        QPoint(0, ui->mainToolBar->height()) +
                                        QPoint(18, 25));
}

//void MainWindow::customContextMenuRequested_UN_KM(UnitNode *un, QPoint pos) /*noexcept*/
//{
//    if(un)
//    {
//        if(VK_KMDeviceType == un->_typeUN())
//        {
//            ui->actionName_Target_KM_UN->setText(un->data(0).toString() + " " + un->data(3).toString().toUpper());
//            this->table_KM_UN_ContextMenu->exec(pos +
//                                                this->pos() +
//                                                ui->centralWidget->pos() +
//                                                ui->splitter_horizontal->pos() +
//                                                ui->splitter_vertical->pos() +
//                                                ui->stackedWidget_1->pos() +
//                                                ui->treeView_UN_KM->pos() +
//                                                QPoint(0, ui->mainToolBar->height()) +
//                                                QPoint(18, 25));
//        }
//    }
//}

void MainWindow::on_actionVK1_FF_Inquiry_KM_triggered() /*noexcept*/
{
    ui->pushButton_0xB1->click();
}

void MainWindow::on_actionVK1_Mode_1_KM_triggered() /*noexcept*/
{
    ui->comboBox_0x1F_mode->setCurrentIndex(0);
    ui->pushButton_0x1F->click();
}

void MainWindow::on_actionVK1_Mode_2_KM_triggered() /*noexcept*/
{
    ui->comboBox_0x1F_mode->setCurrentIndex(1);
    ui->pushButton_0x1F->click();
}

void MainWindow::graphicsView_MAP_ContextMenu(const QPoint &pos) /*noexcept*/
{
    GraphicsMapManager *mapManager(nullptr);
    UnitNode *un(nullptr);
    switch((quint8)this->currentSystemType)
    {
        case (quint8)KMSystemType:
        {
            mapManager = this->mapManager_KM;
            un = mapManager->searchUNPointer(ui->graphicsView_MAP_KM->itemAt(pos));
            break;
        };
        case (quint8)KSSystemType:
        {
            mapManager = this->mapManager_KS;
            un = mapManager->searchUNPointer(ui->graphicsView_MAP_KS->itemAt(pos));
            break;
        };
        case (quint8)BSKSystemType:
        {
            mapManager = this->mapManager_BSK;
            un = mapManager->searchUNPointer(ui->graphicsView_MAP_BSK->itemAt(pos));
            break;
        };
    }

    if(!mapManager->editStatus)
    {
        if(un)
        {
            this->customContextMenuRequested_UN(un,
                                                pos +
                                                this->pos() +
                                                ui->centralWidget->pos() +
                                                ui->splitter_main->pos() +
                                                ui->splitter_horizontal->pos() +
                                                ui->splitter_vertical->pos() +
                                                ui->stackedWidget_2->pos() +
                                                ui->graphicsView_MAP_KM->pos() +
                                                QPoint(0, ui->mainToolBar->height()));
//            switch((quint8)un->_typeSystem())
//            {
//                case (quint8)KMSystemType:
//                {
//                    setInitiatorUN_KM(un);
//                    if(VK_KMDeviceType == un->_typeUN())
//                    {
//                        ui->actionName_Target_KM_UN->setText(un->data(0).toString() + " " + un->data(3).toString().toUpper());
//                        this->table_KM_UN_ContextMenu->exec(pos +
//                                                            this->pos() +
//                                                            ui->centralWidget->pos() +
//                                                            ui->splitter_main->pos() +
//                                                            ui->splitter_horizontal->pos() +
//                                                            ui->splitter_vertical->pos() +
//                                                            ui->stackedWidget_2->pos() +
//                                                            ui->graphicsView_MAP_KM->pos() +
//                                                            QPoint(0, ui->mainToolBar->height())/* +
//                                                            QPoint(18, 25)*/);
//                    }
//                    break;
//                };
//                case (quint8)KSSystemType:
//                {
//                    setInitiatorUN_KS(un);
//                    if(CPP_KSDeviceType == un->_typeUN() ||
//                       RT_KSDeviceType == un->_typeUN())
//                    {
//                        return;
//                    }
//                    if(VK0A_KSDeviceType == un->_typeUN() ||
//                       VK0B_KSDeviceType == un->_typeUN())
//                    {
//                        ui->actionVK1_FF_Inquiry_KS->setEnabled(true);
//                        ui->actionVK1_Mode_1_KS->setEnabled(true);
//                        ui->actionVK1_Mode_2_KS->setEnabled(true);
//                        ui->actionVK2_FF_Inquiry_KS->setEnabled(false);
//                        ui->actionVK2_Mode_1_KS->setEnabled(false);
//                        ui->actionVK2_Mode_2_KS->setEnabled(false);
//                    }
//                    if(VK09_KSDeviceType == un->_typeUN())
//                    {
//                        ui->actionVK1_FF_Inquiry_KS->setEnabled(true);
//                        ui->actionVK1_Mode_1_KS->setEnabled(true);
//                        ui->actionVK1_Mode_2_KS->setEnabled(true);
//                        ui->actionVK2_FF_Inquiry_KS->setEnabled(true);
//                        ui->actionVK2_Mode_1_KS->setEnabled(true);
//                        ui->actionVK2_Mode_2_KS->setEnabled(true);
//                    }

//                    ui->actionName_Target_KS_UN->setText(un->data(0).toString() + " " + un->data(3).toString().toUpper());
//                    this->table_KS_UN_ContextMenu->exec(pos +
//                                                        this->pos() +
//                                                        ui->centralWidget->pos() +
//                                                        ui->splitter_main->pos() +
//                                                        ui->splitter_horizontal->pos() +
//                                                        ui->splitter_vertical->pos() +
//                                                        ui->stackedWidget_2->pos() +
//                                                        ui->graphicsView_MAP_KS->pos() +
//                                                        QPoint(0, ui->mainToolBar->height())/* +
//                                                        QPoint(18, 25)*/);
//                    break;
//                };
//                case (quint8)BSKSystemType:
//                {
//                    setInitiatorUN_BSK(un);
//                    ui->actionName_Target_BSK_UN->setText(un->data(0).toString() + " " + un->data(3).toString().toUpper());
//                    this->table_BSK_UN_ContextMenu->exec(pos +
//                                                         this->pos() +
//                                                         ui->centralWidget->pos() +
//                                                         ui->splitter_main->pos() +
//                                                         ui->splitter_horizontal->pos() +
//                                                         ui->splitter_vertical->pos() +
//                                                         ui->stackedWidget_2->pos() +
//                                                         ui->graphicsView_MAP_BSK->pos() +
//                                                         QPoint(0, ui->mainToolBar->height())/* +
//                                                         QPoint(18, 25)*/);
//                    break;
//                };
//            }
        }
    }
    else
    {
        UnitNode *initiatorUN(nullptr);

        if(KMSystemType == mapManager->currentSystemType &&
           nullptr != initiatorUN_KM)
        {
            if(VK_KMDeviceType == initiatorUN_KM->_typeUN())
            {
                initiatorUN = initiatorUN_KM;
            }
            if(initiatorUN)
            {
                mapManager->catchSceneMouseEvent(initiatorUN, ui->graphicsView_MAP_KM->mapToScene(pos));
            }
            return;
        }

        if(KSSystemType == mapManager->currentSystemType &&
           nullptr != initiatorUN_KS)
        {
            if(VK09_KSDeviceType == initiatorUN_KS->_typeUN() ||
               VK0A_KSDeviceType == initiatorUN_KS->_typeUN() ||
               VK0B_KSDeviceType == initiatorUN_KS->_typeUN())
            {
                initiatorUN = initiatorUN_KS;
            }
            if(initiatorUN)
            {
                mapManager->catchSceneMouseEvent(initiatorUN, ui->graphicsView_MAP_KS->mapToScene(pos));
            }
            return;
        }

        if(BSKSystemType == mapManager->currentSystemType &&
           nullptr != initiatorUN_BSK)
        {
            initiatorUN = initiatorUN_BSK;
            if(initiatorUN)
            {
                mapManager->catchSceneMouseEvent(initiatorUN, ui->graphicsView_MAP_BSK->mapToScene(pos));
            }
            return;
        }
    }
}
void MainWindow::on_graphicsView_MAP_KM_customContextMenuRequested(const QPoint &pos)
{
    this->graphicsView_MAP_ContextMenu(pos);
}
void MainWindow::on_graphicsView_MAP_KS_customContextMenuRequested(const QPoint &pos)
{
    this->graphicsView_MAP_ContextMenu(pos);
}
void MainWindow::on_graphicsView_MAP_BSK_customContextMenuRequested(const QPoint &pos)
{
    this->graphicsView_MAP_ContextMenu(pos);
}


void MainWindow::pushButton_editMode_MAP() /*noexcept*/
{
    this->mapManager_KM->possChangeEditStatus(ui->pushButton_editMode_MAP_KM->isChecked());
    this->mapManager_KS->possChangeEditStatus(ui->pushButton_editMode_MAP_KS->isChecked());
    this->mapManager_BSK->possChangeEditStatus(ui->pushButton_editMode_MAP_BSK->isChecked());
}

void MainWindow::on_actionSound_switcher_triggered() /*noexcept*/
{
    this->m_bskManager->m_bskMsgManager->beeperFlag =
    this->m_ksManager->m_ksMsgManager->beeperFlag =
    this->m_kmManager->m_btManager->beeperFlag = ui->actionSound_switcher->isChecked();
}

void MainWindow::on_pushButton_0x0F_GetSetting_KS_clicked() /*noexcept*/
{
    if(this->initiatorUN_KS)
    {
        this->m_ksManager->cmdRequest_0x41(this->initiatorUN_KS,
                                           (quint8)0x0F);
    }
}

void MainWindow::on_pushButton_0x78_Change_Area_Number_BSK_clicked() /*noexcept*/
{
    if(this->initiatorUN_BSK)
    {
        this->m_bskManager->request_0x41(this->initiatorUN_BSK,
                                         0x78,
                                         ui->spinBox_0x78_Area_BSK->value(),
                                         ui->spinBox_0x78_Number_BSK->value());
    }
}

void MainWindow::on_actionVK1_Test_FF_Inquiry_KS_triggered() /*noexcept*/
{
    ui->pushButton_Select_VK1_KS->click();
    ui->pushButton_0xFA_Get_Test_Shot_KS->click();
}

void MainWindow::on_actionVK2_Test_FF_Inquiry_KS_triggered() /*noexcept*/
{
    ui->pushButton_Select_VK1_KS->click();
    ui->pushButton_0xFA_Get_Test_Shot_KS->click();
}

void MainWindow::on_pushButton_0xFA_Get_Test_Shot_KS_clicked() /*noexcept*/
{
    if(this->initiatorUN_KS)
    {
        quint8 numVK(0x01);
        if(ui->pushButton_Select_VK1_KS->isChecked())
            numVK = 0x01;
        if(ui->pushButton_Select_VK2_KS->isChecked())
            numVK = 0x02;
        this->m_ksManager->cmdRequest_0x41(this->initiatorUN_KS,
                                           0xFA,
                                           numVK);
    }
}

void MainWindow::on_actionHiddingUN_triggered()
{
    switch (this->currentSystemType)
    {
        case KMSystemType:
        {
            m_kmManager->emitInsertNewMSG(this->initiatorUN_KM,
                                          0xFF,
                                          QString("Deleted").toUtf8(),
                                          true,
                                          Command_TypeGroupMSG);
            this->initiatorUN_KM->updateFlagHidden(true);
            break;
        }
        case KSSystemType:
        {
            m_ksManager->emitInsertNewMSG(this->initiatorUN_KS,
                                          0xFF,
                                          QString("Deleted").toUtf8(),
                                          true,
                                          Command_TypeGroupMSG);
            this->initiatorUN_KS->updateFlagHidden(true);
            break;
        }
        case BSKSystemType:
        {
            m_bskManager->emitInsertNewMSG(this->initiatorUN_BSK,
                                           0xFF,
                                           QString("Deleted").toUtf8(),
                                           true,
                                           Command_TypeGroupMSG);
            this->initiatorUN_BSK->updateFlagHidden(true);
            break;
        }
        default:
            return;
    }
}

void MainWindow::on_actionOperatorWorkMode_triggered()
{
    this->updateStatusWorkMode(ui->actionOperatorWorkMode->isChecked());
}

void MainWindow::on_actionOperatorWorkMode_triggered(bool checked)
{
    if(checked)
    {
        ui->actionOperatorWorkMode->setText(tr("Editor work mode (switch on operator work mode)"));
        ui->actionOperatorWorkMode->setToolTip(tr("Editor work mode (switch on operator work mode)"));

        ui->actionLoadNewMap->setEnabled(true);
        ui->actionViewToolDialog->setEnabled(true);
//        ui->actionRussian->setEnabled(true);
//        ui->actionEnglish->setEnabled(true);
        ui->actionHiddingUN->setEnabled(true);
        ui->pushButton_editMode_MAP_KM->setEnabled(true);
        ui->pushButton_editMode_MAP_KS->setEnabled(true);
        ui->pushButton_editMode_MAP_BSK->setEnabled(true);

        ui->actionLoadNewMap->setVisible(true);
        ui->actionViewToolDialog->setVisible(true);
//        ui->actionRussian->setVisible(true);
//        ui->actionEnglish->setVisible(true);
        ui->actionHiddingUN->setVisible(true);
        ui->pushButton_editMode_MAP_KM->setVisible(true);
        ui->pushButton_editMode_MAP_KS->setVisible(true);
        ui->pushButton_editMode_MAP_BSK->setVisible(true);
    }
    else
    {
        ui->actionOperatorWorkMode->setText(tr("Operator work mode (switch on editor work mode)"));
        ui->actionOperatorWorkMode->setToolTip(tr("Operator work mode (switch on editor work mode)"));

        ui->actionLoadNewMap->setEnabled(false);
        ui->actionViewToolDialog->setEnabled(false);
//        ui->actionRussian->setEnabled(false);
//        ui->actionEnglish->setEnabled(false);
        ui->actionHiddingUN->setEnabled(false);
        ui->pushButton_editMode_MAP_KM->setEnabled(false);
        ui->pushButton_editMode_MAP_KS->setEnabled(false);
        ui->pushButton_editMode_MAP_BSK->setEnabled(false);

        ui->actionLoadNewMap->setVisible(false);
        ui->actionViewToolDialog->setVisible(false);
//        ui->actionRussian->setVisible(false);
//        ui->actionEnglish->setVisible(false);
        ui->actionHiddingUN->setVisible(false);
        ui->pushButton_editMode_MAP_KM->setVisible(false);
        ui->pushButton_editMode_MAP_KS->setVisible(false);
        ui->pushButton_editMode_MAP_BSK->setVisible(false);
    }
}

void MainWindow::on_action_requestInquiry_KM_triggered()
{
    ui->pushButton_Inquiry->click();
}

void MainWindow::on_action_requestList_KM_triggered()
{
    ui->pushButton_List->click();
}

void MainWindow::on_action_requestKill_KM_triggered()
{
    ui->pushButton_Kill->click();
}

void MainWindow::on_action_requestTxpowerRssi_KM_triggered()
{
    ui->pushButton_loopTxPowerRSSI->click();
}

void MainWindow::on_actionResetAlarm_KM_triggered()
{
    if(this->initiatorUN_KM)
    {
        this->initiatorUN_KM->offAlarmCondition();
        this->m_ksManager->emitInsertNewMSG(this->initiatorUN_KM,
                                            0xFF,
                                            QString("Reset alarm").toUtf8(),
                                            true,
                                            Command_TypeGroupMSG);
    }
}

void MainWindow::on_actionResetAlarm_KS_triggered()
{
    if(this->initiatorUN_KS)
    {
        this->initiatorUN_KS->offAlarmCondition();
        this->m_ksManager->emitInsertNewMSG(this->initiatorUN_KS,
                                             0xFF,
                                             QString("Reset alarm").toUtf8(),
                                             true,
                                            Command_TypeGroupMSG);
    }
}

void MainWindow::on_actionBigIconSize_triggered(bool checked)
{
    if(checked)
    {
        ui->mainToolBar->setIconSize(QSize(48, 48));
        ui->toolBar->setIconSize(QSize(48, 48));
    }
    else
    {
        ui->mainToolBar->setIconSize(QSize(24, 24));
        ui->toolBar->setIconSize(QSize(24, 24));
    }
}

void MainWindow::on_actionReset_BSK_triggered()
{
    if(this->initiatorUN_BSK)
    {
//        this->initiatorUN_BSK->updateFlagAlarm(false);
        this->initiatorUN_BSK->offAlarmCondition();
        this->m_bskManager->emitInsertNewMSG(this->initiatorUN_BSK,
                                             0xFF,
                                             QString("Reset alarm").toUtf8(),
                                             true,
                                             Command_TypeGroupMSG);
    }
}

void MainWindow::on_actionResetDischarge_KM_triggered()
{
    if(this->initiatorUN_KM)
    {
        this->initiatorUN_KM->updateFlagDischarge(false);
        this->m_kmManager->emitInsertNewMSG(this->initiatorUN_KM,
                                             0xFF,
                                             QString("Reset discharge").toUtf8(),
                                             true,
                                            Command_TypeGroupMSG);
    }
}

void MainWindow::on_actionResetDischarge_KS_triggered()
{
    if(this->initiatorUN_KS)
    {
        this->initiatorUN_KS->updateFlagDischarge(false);
        this->m_ksManager->emitInsertNewMSG(this->initiatorUN_KS,
                                             0xFF,
                                             QString("Reset discharge").toUtf8(),
                                             true,
                                            Command_TypeGroupMSG);
    }
}

void MainWindow::on_actionResetDischarge_BSK_triggered()
{
    if(this->initiatorUN_BSK)
    {
        this->initiatorUN_BSK->updateFlagDischarge(false);
        this->m_bskManager->emitInsertNewMSG(this->initiatorUN_BSK,
                                             0xFF,
                                             QString("Reset discharge").toUtf8(),
                                             true,
                                             Command_TypeGroupMSG);
    }
}

void MainWindow::on_actionResetFailure_KM_triggered()
{
    if(this->initiatorUN_KM)
    {
        this->initiatorUN_KM->updateFlagFailure(false);
        this->m_kmManager->emitInsertNewMSG(this->initiatorUN_KM,
                                             0xFF,
                                             QString("Reset failure").toUtf8(),
                                             true,
                                            Command_TypeGroupMSG);
    }
}

void MainWindow::on_actionResetFailure_KS_triggered()
{
    if(this->initiatorUN_KS)
    {
        this->initiatorUN_KS->updateFlagFailure(false);
        this->m_ksManager->emitInsertNewMSG(this->initiatorUN_KS,
                                             0xFF,
                                             QString("Reset failure").toUtf8(),
                                             true,
                                            Command_TypeGroupMSG);
    }
}

void MainWindow::on_actionResetFailure_BSK_triggered()
{
    if(this->initiatorUN_BSK)
    {
        this->initiatorUN_BSK->updateFlagFailure(false);
        this->m_bskManager->emitInsertNewMSG(this->initiatorUN_BSK,
                                             0xFF,
                                             QString("Reset failure").toUtf8(),
                                             true,
                                             Command_TypeGroupMSG);
    }
}

void MainWindow::on_pushButton_Switch_On_Self_D_KS_clicked()
{
    if(nullptr != this->initiatorUN_KS)
    {
        this->m_ksManager->cmdRequest_0x41(this->initiatorUN_KS,
                                           0x02,
                                           0x00,
                                           0x0B);
    }
}

void MainWindow::on_pushButton_Switch_Off_Self_D_KS_clicked()
{
    if(nullptr != this->initiatorUN_KS)
    {
        this->m_ksManager->cmdRequest_0x41(this->initiatorUN_KS,
                                           0x01,
                                           0x00,
                                           0x0B);
    }
}

void MainWindow::on_pushButton_Switch_On_Other_D1_KS_clicked()
{
    if(nullptr != this->initiatorUN_KS)
    {
        this->m_ksManager->cmdRequest_0x41(this->initiatorUN_KS,
                                           0x02,
                                           0x00,
                                           0x01);
    }
}

void MainWindow::on_pushButton_Switch_Off_Other_D1_KS_clicked()
{
    if(nullptr != this->initiatorUN_KS)
    {
        this->m_ksManager->cmdRequest_0x41(this->initiatorUN_KS,
                                           0x01,
                                           0x00,
                                           0x01);
    }
}

void MainWindow::on_pushButton_Switch_On_Other_D2_KS_clicked()
{
    if(nullptr != this->initiatorUN_KS)
    {
        this->m_ksManager->cmdRequest_0x41(this->initiatorUN_KS,
                                           0x02,
                                           0x00,
                                           0x02);
    }
}

void MainWindow::on_pushButton_Switch_Off_Other_D2_KS_clicked()
{
    if(nullptr != this->initiatorUN_KS)
    {
        this->m_ksManager->cmdRequest_0x41(this->initiatorUN_KS,
                                           0x01,
                                           0x00,
                                           0x02);
    }
}

void MainWindow::on_pushButton_Kit_Num_RM433_KS_clicked()
{
    if(nullptr != this->initiatorUN_KS)
    {
        this->m_ksManager->cmdRequest_0x41(this->initiatorUN_KS,
                                           0x03,
                                           (quint8)ui->spinBox_Kit_Num_RM433_KS->value());
    }
}

void MainWindow::on_pushButton_setRVPsensitivity_clicked()
{
    if(this->initiatorUN_BSK)
    {
        quint8 sensitivity = 100;
        sensitivity = ui->comboBox_Sensetivity_RVG_BSK->currentIndex();

//        qDebug() << "Set BSK-RVP sensitivity to " << sensitivity;

        if(sensitivity != 100)
        {
            this->m_bskManager->request_0x41(this->initiatorUN_BSK,
                                         0x40, sensitivity);
        }
    }
}

void MainWindow::on_pushButton_Sensetivity_RLD_BSK_clicked()
{
    if(this->initiatorUN_BSK)
    {
        this->m_bskManager->request_0x41(this->initiatorUN_BSK,
                                         0x40,
                                         ui->comboBox_Sensetivity_RLD_BSK->currentIndex());
    }
}

void MainWindow::on_pushButton_Area_RLO_BSK_clicked()
{
    if(this->initiatorUN_BSK)
    {
        this->m_bskManager->request_0x41(this->initiatorUN_BSK,
                                         0x30,
                                         0x20 + (ui->comboBox_Area_RLO_BSK->currentIndex() * 0x20));
    }
}

void MainWindow::on_comboBox_Add_Type_D_VK_KS_currentIndexChanged(int index)
{
    switch(index)
    {
        case 12:
        {
            ui->lineEdit_Add_ID_D_VK_KS->setText("001");
            ui->lineEdit_Add_ID_D_VK_KS->setEnabled(false);
            break;
        }
        case 13:
        {
            ui->lineEdit_Add_ID_D_VK_KS->setText("002");
            ui->lineEdit_Add_ID_D_VK_KS->setEnabled(false);
            break;
        }
        default:
        {
            ui->lineEdit_Add_ID_D_VK_KS->setText("");
            ui->lineEdit_Add_ID_D_VK_KS->setEnabled(true);
            break;
        }
    }

}

void MainWindow::on_comboBox_0x42x43_Inst_Mode_VK_KS_currentIndexChanged(int index)
{
    ui->spinBox_0x42x43_Inst_T1_VK_KS->setEnabled(true);
    ui->label_0x42x43_Inst_T1_VK_KS->setEnabled(true);
    if(0 != index)
    {
        ui->spinBox_0x42x43_Inst_T1_VK_KS->setEnabled(false);
        ui->label_0x42x43_Inst_T1_VK_KS->setEnabled(false);
    }
}

void MainWindow::on_pushButton_editMode_MAP_KM_clicked(bool checked)
{
    this->mapManager_KM->possChangeEditStatus(checked);

    if(checked)
    {
        ui->pushButton_editMode_MAP_KM->setToolTip(tr("Edit ending"));
        ui->pushButton_editMode_MAP_KM->setStatusTip(tr("Edit ending"));
        ui->pushButton_editMode_MAP_KM->setWhatsThis(tr("Edit ending"));
    }
    else
    {
        ui->pushButton_editMode_MAP_KM->setToolTip(tr("Edit"));
        ui->pushButton_editMode_MAP_KM->setStatusTip(tr("Edit"));
        ui->pushButton_editMode_MAP_KM->setWhatsThis(tr("Edit"));
    }
}

void MainWindow::on_pushButton_editMode_MAP_KS_clicked(bool checked)
{
    this->mapManager_KS->possChangeEditStatus(checked);

    if(checked)
    {
        ui->pushButton_editMode_MAP_KS->setToolTip(tr("Edit ending"));
        ui->pushButton_editMode_MAP_KS->setStatusTip(tr("Edit ending"));
        ui->pushButton_editMode_MAP_KS->setWhatsThis(tr("Edit ending"));
    }
    else
    {
        ui->pushButton_editMode_MAP_KS->setToolTip(tr("Edit"));
        ui->pushButton_editMode_MAP_KS->setStatusTip(tr("Edit"));
        ui->pushButton_editMode_MAP_KS->setWhatsThis(tr("Edit"));
    }
}

void MainWindow::on_pushButton_editMode_MAP_BSK_clicked(bool checked)
{
    this->mapManager_BSK->possChangeEditStatus(checked);

    if(checked)
    {
        ui->pushButton_editMode_MAP_BSK->setToolTip(tr("Edit ending"));
        ui->pushButton_editMode_MAP_BSK->setStatusTip(tr("Edit ending"));
        ui->pushButton_editMode_MAP_BSK->setWhatsThis(tr("Edit ending"));
    }
    else
    {
        ui->pushButton_editMode_MAP_BSK->setToolTip(tr("Edit"));
        ui->pushButton_editMode_MAP_BSK->setStatusTip(tr("Edit"));
        ui->pushButton_editMode_MAP_BSK->setWhatsThis(tr("Edit"));
    }
}

void MainWindow::on_actionBuildNetTopology_Target_triggered()
{
    if(KMSystemType == this->currentSystemType)
    {
        QMessageBox ms;
        ms.setWindowTitle(tr("Build network"));

        QAbstractButton *yes = ms.addButton(tr("Yes"), QMessageBox::YesRole);
        QAbstractButton *no = ms.addButton(tr("No"), QMessageBox::NoRole);
        ms.setText(tr("Build network?"));
        ms.exec();
        if(ms.clickedButton() == yes)
        {
            this->m_kmManager->organiseNetworkTopology();
        }
        if(ms.clickedButton() == no)
        {
            return;
        }
    }
    if(KSSystemType == this->currentSystemType)
    {
        if(nullptr != this->initiatorUN_KS)
        {
            if((quint8)CPP_KSDeviceType != this->initiatorUN_KS->_typeUN())
            {
                this->m_ksManager->cmdRequest_0x41(this->initiatorUN_KS,
                                                   0x0F);
                return;
            }
        }
    }
}

void MainWindow::on_action_0x60_GetConfig_PKP_triggered()
{
    this->m_pkpManager->request_0x60();
}

void MainWindow::on_pushButton_0x0B_Change_Num_Kit_BSK_clicked()
{
    this->m_bskManager->request_0x0B(ui->spinBox_0x0B_Num_Kit_BSK->value());
}

void MainWindow::on_pushButton_0x41x7C_Change_BSK_clicked()
{
    this->m_bskManager->request_0x41(0x00,
                                     ui->spinBox_0x0B_Num_Kit_BSK->value(),
                                     ui->spinBox_0x41x7C_Area_BSK->value(),
                                     ui->spinBox_0x41x7C_Number_BSK->value());
}

void MainWindow::on_stackedWidget_Settings_BSK_currentChanged(int arg1)
{
    if(1 != arg1 &&
       2 != arg1 &&
       3 != arg1 &&
       5 != arg1) {
        ui->stackedWidget_Settings_BSK->setVisible(false);
    }
    else {
        ui->stackedWidget_Settings_BSK->setVisible(true);
    }
}

void MainWindow::on_pushButton_0x41x28_Inst_Mode_VK_KS_clicked()
{
    if(this->initiatorUN_KS)
    {
        this->m_ksManager->cmdRequest_0x41(this->initiatorUN_KS,
                                           0x28,
                                           ui->comboBox_0x41x28_Inst_Mode_VK_KS->currentIndex());
    }
}
