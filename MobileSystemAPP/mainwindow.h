﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QListWidgetItem>
#include <QMenu>
#include <QMessageBox>
#include <QCloseEvent>
#include <QProgressBar>
#include <serialportmanagerkm.h>
#include <kamuflagemanager.h>
#include <kiparismanager.h>
#include <bskmanager.h>
#include <pkpmanager.h>
#include <proxymetod.h>
#include <tablemodelarchiveimg.h>
#include <tablemodelarchivemsg.h>
#include <treemodelunitnode.h>
#include <tablemodelunitnode.h>
#include <graphicsmapmanager.h>
//#include <graphicsimgmanager.h>
#include <databasemanager.h>
#include <dialogserialportsetting.h>
#include <dialogtablearchive.h>
#include <dialogimageviwer.h>
//#include <QtConcurrent>

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    TypeUnitNodeSystem currentSystemType;
    DataBaseManager *m_dbManager;
    KamuflageManager *m_kmManager;
    KiparisManager *m_ksManager;
    BSKManager *m_bskManager;
    PKPManager *m_pkpManager;
    TableModelArchiveMSG *modelArchive_KM_MSG;
    TableModelArchiveIMG *modelArchive_KM_IMG;
    TableModelArchiveMSG *modelArchive_KS_MSG;
    TableModelArchiveIMG *modelArchive_KS_IMG;
    TableModelArchiveMSG *modelArchive_BSK_MSG;
    TreeModelUnitNode *modelTreeUN_KM;
    TableModelUnitNode *modelTableUN_KM;
    TreeModelUnitNode *modelTreeUN_KS;
    TableModelUnitNode *modelTableUN_KS;
    TreeModelUnitNode *modelTreeUN_BSK;
    TableModelUnitNode *modelTableUN_BSK;
    GraphicsMapManager *mapManager_KM;
    GraphicsMapManager *mapManager_KS;
    GraphicsMapManager *mapManager_BSK;
//    GraphicsMapManager *mapManager;
    QWidget *tabConsole;
    QTimer *timerClock;
    QTimer *timerUpdateProgress;
    QTranslator *ruTranslator;
    QGraphicsScene *imgScene;
    QGraphicsPixmapItem *mainIMG;
    QPixmap mainPXM,
            scaledMainPXM,
            bpIMG1,
            bpIMG2,
            bpIMG3,
            bpIMG4,
            bpIMG5;

    QTimer *timer_Request_List_D_VK_KS;
//    GraphicsIMGManager *imgManager;

    int numCurrentShot;

    explicit MainWindow(QWidget *parent = nullptr) /*noexcept*/;
    virtual ~MainWindow() /*noexcept*/;

////    начальная инициализация
//    void
    void setRussian() /*noexcept*/;
    void setEnglish() /*noexcept*/;

    void setInitiatorUN_KM(UnitNode *un) /*noexcept*/;
    void setInitiatorUN_KS(UnitNode *un) /*noexcept*/;
    void setInitiatorUN_BSK(UnitNode *un) /*noexcept*/;

private:
    Ui::MainWindow *ui;

    DialogSerialPortSetting *dialog_SerialPortSetting;
    DialogImageViwer *dialog_ImageViwer;
    DialogTableArchive *dialog_TableArchive;
    QProgressBar *imgProgressBar;
    QLabel *msgLabel,
           *clockLabel,
           *workModeLabel;


    QMenu *tableContextMenu,
          *tableOrTreeContextMenu,
          *table_BSK_UN_ContextMenu,
          *table_KS_UN_ContextMenu,
          *table_KM_UN_ContextMenu;
    QSqlRecord imgRecord,
               statusMSG;
    qint32 statusMode;
    QString statusProgress;

    UnitNode *initiatorUN_KM,
             *initiatorUN_KS,
             *initiatorUN_BSK,
             *initiatorUN,
             *lastProgressUN;
    quint8 lastProgressNumShot,
           lastProgressNumTrain;
    QByteArray consoleBTLink;

    void closeEvent(QCloseEvent *event) /*noexcept*/;


public slots:
    void setTableSizeToContents() /*noexcept*/;

protected slots:
    void resizeEvent(QResizeEvent *e = nullptr) /*noexcept*/;
    void inicialResize() /*noexcept*/;
    void fillPushButton_IMG() /*noexcept*/;
    void setDefaultTableSize() /*noexcept*/;
    void uiTableResize() /*noexcept*/;
    void uiElementResize() /*noexcept*/;
    void setIMGScene(quint32 dbIndex, int number = 0) /*noexcept*/;
    void newRecord_KMIMG(quint32 index, quint32 dbIndex) /*noexcept*/;
    void newRecord_KSIMG(quint32 index, quint32 dbIndex) /*noexcept*/;
    void updateStatusClock() /*noexcept*/;
//    void updateStatusMode(qint32 mode) /*noexcept*/;
    void updateStatusWorkMode(bool mode) /*noexcept*/;
    void updateStatusMSG(quint32 index) /*noexcept*/;
    void updateStatusProgress(UnitNode *unSender,
                              quint8 nomS,
                              quint8 numTrain,
                              quint8 progress) /*noexcept*/;
    void hideStatusProgress() /*noexcept*/;
    void tabWidgetCurrentIndexChanged(int index) /*noexcept*/;
    void updateBTLinkToSend(QString strLink) /*noexcept*/;
    void updateSettingUN_KM(UnitNode *un,
                         QByteArray msg3F) /*noexcept*/;
    void updatePortCondition(qint32 condition) /*noexcept*/;
    void changeCurrentSystemType(TypeUnitNodeSystem typeSustem) /*noexcept*/;
    void setInitiatorUN(UnitNode *un) /*noexcept*/;

private slots:
    void on_actionViewToolDialog_triggered(bool checked) /*noexcept*/;
    void on_checkBox_autoRequestB1_clicked(bool checked) /*noexcept*/;
    void on_lineEdit_Console_returnPressed() /*noexcept*/;
    void on_pushButton_Set_clicked() /*noexcept*/;
    void on_pushButton_loopTxPowerRSSI_clicked() /*noexcept*/;
    void on_pushButton_Kill_clicked() /*noexcept*/;
    void on_pushButton_List_clicked() /*noexcept*/;
    void on_pushButton_Inquiry_clicked() /*noexcept*/;
    void on_pushButton_0x2F_clicked() /*noexcept*/;
    void on_pushButton_0x1F_clicked() /*noexcept*/;
//    void on_pushButton_0xDE_clicked() /*noexcept*/;
    void on_pushButton_0xB1_clicked() /*noexcept*/;
    void on_pushButton_0xA1_clicked() /*noexcept*/;
//    void on_stackedWidget_1_customContextMenuRequested(QPoint pos) /*noexcept*/;
    void on_actionViewTableOrTree_triggered(bool checked) /*noexcept*/;
    void on_actionBuildNetTopology_triggered() /*noexcept*/;
    void on_actionStandingInAQueueIMG_triggered() /*noexcept*/;
    void on_actionViewArchiveInWindow_triggered(bool checked) /*noexcept*/;
    QString createTextIMGLable(int numIMG) /*noexcept*/;
    void on_pushButton_IMG1_clicked() /*noexcept*/;
    void on_pushButton_IMG2_clicked() /*noexcept*/;
    void on_pushButton_IMG3_clicked() /*noexcept*/;
    void on_pushButton_IMG4_clicked() /*noexcept*/;
    void on_pushButton_IMG5_clicked() /*noexcept*/;
    void on_actionSetViewMap_triggered() /*noexcept*/;
    void on_actionSetViewIMG_triggered() /*noexcept*/;
    void on_actionSetViewIMGArchive_triggered() /*noexcept*/;
    void on_actionSetViewAllMSGArchive_triggered() /*noexcept*/;
//    void on_actionSetViewBTMSGArchive_triggered() /*noexcept*/;
//    void on_actionSetViewKMMSGArchive_triggered() /*noexcept*/;
    void on_actionSetAutoScroll_triggered(bool checked) /*noexcept*/;
//    void on_stackedWidget_3_customContextMenuRequested(QPoint pos) /*noexcept*/;
    void pushButton_zoomMinus_MAP() /*noexcept*/;
    void pushButton_zoomPlus_MAP() /*noexcept*/;
    void on_actionLoadNewMap_triggered() /*noexcept*/;
    void on_action_viewDialogPortSetting_triggered() /*noexcept*/;

    void on_actionRussian_triggered() /*noexcept*/;

    void on_actionEnglish_triggered() /*noexcept*/;

    void on_comboBox_0x1F_BSK1_currentIndexChanged(int index) /*noexcept*/;

    void on_comboBox_0x1F_BSK2_currentIndexChanged(int index) /*noexcept*/;

    void on_comboBox_0x1F_BSK3_currentIndexChanged(int index) /*noexcept*/;

    void on_pushButton_zoomPlus_IMG_clicked() /*noexcept*/;

    void on_pushButton_zoomMinus_IMG_clicked() /*noexcept*/;

//    void on_treeView_UN_KM_clicked(const QModelIndex &index) /*noexcept*/;

//    void on_tableView_UN_KM_clicked(const QModelIndex &index) /*noexcept*/;

    void on_actionSetCurrentSystemKM_triggered() /*noexcept*/;

    void on_actionSetCurrentSystemKS_triggered() /*noexcept*/;

    void on_actionSetCurrentSystemBSK_triggered() /*noexcept*/;

    void on_pushButton_Select_VK1_KS_clicked() /*noexcept*/;

    void on_pushButton_Select_VK2_KS_clicked() /*noexcept*/;

    void possUpdateToolDialog_KS(UnitNode *un) /*noexcept*/;

    void on_pushButton_0x78_Change_Area_Number_KS_clicked() /*noexcept*/;

    void on_pushButton_0x42x43_Inst_Mode_VK_KS_clicked() /*noexcept*/;

    void on_pushButton_0xFD_Get_Shot_KS_clicked() /*noexcept*/;

    void on_pushButton_SwitchON_0x41x02_BSK_clicked() /*noexcept*/;

    void on_pushButton_SwitchOFF_0x41x01_BSK_clicked() /*noexcept*/;

    void on_pushButton_SendDK_0x41x55_BSK_clicked() /*noexcept*/;

    void on_checkBox_BSK_S_Alarm_clicked(bool checked) /*noexcept*/;

    void on_checkBox_BSK_S_Odin_clicked(bool checked) /*noexcept*/;

    void on_checkBox_BSK_S_Group_clicked(bool checked) /*noexcept*/;

    void on_checkBox_BSK_S_TS_clicked(bool checked) /*noexcept*/;

    void on_checkBox_BSK_S_Far_TS_clicked(bool checked) /*noexcept*/;

    void on_checkBox_BSK_S_Unknown_clicked(bool checked) /*noexcept*/;

    void on_checkBox_BSK_S_Train_clicked(bool checked) /*noexcept*/;

    void on_checkBox_BSK_S_Direction_clicked(bool checked) /*noexcept*/;

    void on_checkBox_BSK_S_Winter_clicked(bool checked) /*noexcept*/;

    void on_checkBox_BSK_S_Summer_clicked(bool checked) /*noexcept*/;

    void on_pushButton_ResetSO_BSK_clicked() /*noexcept*/;

    void on_pushButton_Request_List_D_VK_KS_clicked() /*noexcept*/;

    void on_pushButton_Add_D_VK_KS_clicked() /*noexcept*/;

    void on_pushButton_Remove_D_VK_KS_clicked() /*noexcept*/;

    void on_pushButton_fullScreen_IMG_clicked() /*noexcept*/;

    void customContextMenuRequested_UN(UnitNode *un, QPoint pos) /*noexcept*/;

    void on_tableView_UN_BSK_customContextMenuRequested(const QPoint &pos) /*noexcept*/;

    void on_treeView_UN_BSK_customContextMenuRequested(const QPoint &pos) /*noexcept*/;

//    void customContextMenuRequested_UN_BSK(UnitNode *un, QPoint pos) /*noexcept*/;

    void on_tableView_UN_KS_customContextMenuRequested(const QPoint &pos) /*noexcept*/;

    void on_treeView_UN_KS_customContextMenuRequested(const QPoint &pos) /*noexcept*/;

//    void customContextMenuRequested_UN_KS(UnitNode *un, QPoint pos) /*noexcept*/;

    void on_actionVK1_FF_Inquiry_KS_triggered() /*noexcept*/;

    void on_actionVK1_Mode_1_KS_triggered() /*noexcept*/;

    void on_actionVK1_Mode_2_KS_triggered() /*noexcept*/;

    void on_actionVK2_FF_Inquiry_KS_triggered() /*noexcept*/;

    void on_actionVK2_Mode_2_KS_triggered() /*noexcept*/;

    void on_actionVK2_Mode_1_KS_triggered() /*noexcept*/;

    void on_tableView_UN_KM_customContextMenuRequested(const QPoint &pos) /*noexcept*/;

    void on_treeView_UN_KM_customContextMenuRequested(const QPoint &pos) /*noexcept*/;

//    void customContextMenuRequested_UN_KM(UnitNode *un, QPoint pos) /*noexcept*/;

    void on_actionVK1_FF_Inquiry_KM_triggered() /*noexcept*/;
    
    void on_actionVK1_Mode_1_KM_triggered() /*noexcept*/;
    
    void on_actionVK1_Mode_2_KM_triggered() /*noexcept*/;
    
    void graphicsView_MAP_ContextMenu(const QPoint &pos) /*noexcept*/;

    void pushButton_editMode_MAP() /*noexcept*/;

    void on_actionSound_switcher_triggered() /*noexcept*/;

    void on_pushButton_0x0F_GetSetting_KS_clicked() /*noexcept*/;

    void on_pushButton_GetConfig_0x41x7B_BSK_clicked() /*noexcept*/;

    void on_pushButton_0x78_Change_Area_Number_BSK_clicked() /*noexcept*/;

    void on_actionVK1_Test_FF_Inquiry_KS_triggered() /*noexcept*/;

    void on_actionVK2_Test_FF_Inquiry_KS_triggered() /*noexcept*/;

    void on_pushButton_0xFA_Get_Test_Shot_KS_clicked() /*noexcept*/;

    void on_actionHiddingUN_triggered();

    void on_actionOperatorWorkMode_triggered();

    void on_actionOperatorWorkMode_triggered(bool checked);

    void on_action_requestInquiry_KM_triggered();

    void on_action_requestList_KM_triggered();

    void on_action_requestKill_KM_triggered();

    void on_action_requestTxpowerRssi_KM_triggered();

    void on_actionResetAlarm_KM_triggered();

    void on_actionResetAlarm_KS_triggered();

    void on_pushButton_zoomPlus_MAP_KM_clicked();

    void on_pushButton_zoomPlus_MAP_KS_clicked();

    void on_pushButton_zoomPlus_MAP_BSK_clicked();

    void on_pushButton_zoomMinus_MAP_KM_clicked();

    void on_pushButton_zoomMinus_MAP_KS_clicked();

    void on_pushButton_zoomMinus_MAP_BSK_clicked();

    void on_graphicsView_MAP_KM_customContextMenuRequested(const QPoint &pos);

    void on_graphicsView_MAP_KS_customContextMenuRequested(const QPoint &pos);

    void on_graphicsView_MAP_BSK_customContextMenuRequested(const QPoint &pos);

    void on_actionBigIconSize_triggered(bool checked);

    void on_actionReset_BSK_triggered();

    void on_actionResetDischarge_KM_triggered();

    void on_actionResetDischarge_KS_triggered();

    void on_actionResetDischarge_BSK_triggered();

    void on_actionResetFailure_KM_triggered();

    void on_actionResetFailure_KS_triggered();

    void on_actionResetFailure_BSK_triggered();

    void on_pushButton_Switch_On_Self_D_KS_clicked();

    void on_pushButton_Switch_Off_Self_D_KS_clicked();

    void on_pushButton_Switch_On_Other_D1_KS_clicked();

    void on_pushButton_Switch_Off_Other_D1_KS_clicked();

    void on_pushButton_Switch_On_Other_D2_KS_clicked();

    void on_pushButton_Switch_Off_Other_D2_KS_clicked();

    void on_pushButton_Kit_Num_RM433_KS_clicked();

    void on_pushButton_setRVPsensitivity_clicked();

    void on_pushButton_Sensetivity_RLD_BSK_clicked();

    void on_pushButton_Area_RLO_BSK_clicked();

    void on_comboBox_Add_Type_D_VK_KS_currentIndexChanged(int index);

    void on_comboBox_0x42x43_Inst_Mode_VK_KS_currentIndexChanged(int index);

    void on_pushButton_editMode_MAP_KM_clicked(bool checked);

    void on_pushButton_editMode_MAP_KS_clicked(bool checked);

    void on_pushButton_editMode_MAP_BSK_clicked(bool checked);

    void on_actionBuildNetTopology_Target_triggered();

    void on_action_0x60_GetConfig_PKP_triggered();

    void on_pushButton_0x0B_Change_Num_Kit_BSK_clicked();

    void on_pushButton_0x41x7C_Change_BSK_clicked();

    void on_stackedWidget_Settings_BSK_currentChanged(int arg1);

    void on_pushButton_0x41x28_Inst_Mode_VK_KS_clicked();

signals:
    void setTranslator(QTranslator *ts);
    void resetTranslator(QTranslator *ts);
};

#endif // MAINWINDOW_H
