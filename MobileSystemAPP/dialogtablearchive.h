﻿#ifndef DIALOGTABLEARCHIVE_H
#define DIALOGTABLEARCHIVE_H

#include <QMainWindow>
#include <QAbstractItemModel>
//#include <tablemodelarchive_km_img.h>
//#include <tablemodelarchive_bt_km_msg.h>
//#include <tablemodelarchive_ks_img.h>
//#include <tablemodelarchive_ks_msg.h>
#include <tablemodelarchivemsg.h>
#include <tablemodelarchiveimg.h>
#include <proxymetod.h>

#include <kamuflagemanager.h>
#include <kiparismanager.h>
#include <bskmanager.h>


namespace Ui {
    class DialogTableArchive;
}

class DialogTableArchive : public QMainWindow
{
    Q_OBJECT

public:
    bool isFirstShow;
    explicit DialogTableArchive(DataBaseManager *db,
                       KamuflageManager *km,
                       KiparisManager *ks,
                       BSKManager *bsk,
                       QWidget *parent = nullptr) /*noexcept*/;

    virtual ~DialogTableArchive() /*noexcept*/;


    DataBaseManager *dbManager;
    KamuflageManager *kmManager;
    KiparisManager *ksManager;
    BSKManager *bskManager;
    TypeUnitNodeSystem currentSystemType;
    FilterStruct currentFilter;

    TableModelArchiveMSG *modelArchiveMSG_KM,
                         *modelArchiveMSG_KS,
                         *modelArchiveMSG_BSK;
    TableModelArchiveIMG *modelArchiveIMG_KM,
                         *modelArchiveIMG_KS;



    void setTableModel(int index) /*noexcept*/;
    void retranslate() /*noexcept*/;
    void setDefaultTableSize() /*noexcept*/;
    void resetFilter() /*noexcept*/;
    void applyFilter() /*noexcept*/;
    void needReset() /*noexcept*/;
    void viewCurrentFilter();

protected:
    virtual void closeEvent(QCloseEvent *event) /*noexcept*/ override;
protected slots:
//    void resizeEvent(QResizeEvent *event);


private:
    Ui::DialogTableArchive *ui;

private slots:
    void on_actionSetAutoScroll_triggered(bool checked) /*noexcept*/;

    void on_actionSetViewKMMSGArchive_triggered() /*noexcept*/;

    void on_actionSetViewKSMSGArchive_triggered() /*noexcept*/;

    void on_actionSetViewBSKMSGArchive_triggered() /*noexcept*/;

    void on_actionSetViewIMGArchive_KM_triggered() /*noexcept*/;

    void on_actionSetViewIMGArchive_KS_triggered() /*noexcept*/;

    void on_pushButton_ResetFilter_clicked() /*noexcept*/;

    void on_pushButton_ApplyFilter_clicked() /*noexcept*/;


    void on_tableView_Archive_doubleClicked(const QModelIndex &index);

signals:
    void imClosed(bool);
    void needViewIMG(quint32 dbIndex, int number);
};

#endif // DIALOGTABLEARCHIVE_H
