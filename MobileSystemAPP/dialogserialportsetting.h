﻿#ifndef DIALOGSERIALPORTSETTING_H
#define DIALOGSERIALPORTSETTING_H

#include <QDialog>
#include <QSerialPortInfo>
#include <global_value_and_structure.h>
#ifdef QT_DEBUG
#include <QDebug>
#endif


namespace Ui {
    class DialogSerialPortSetting;
}

class DialogSerialPortSetting : public QDialog
{
    Q_OBJECT

public:
    explicit DialogSerialPortSetting(QWidget *parent = nullptr) /*noexcept*/;
    void retranslate() /*noexcept*/;
    virtual ~DialogSerialPortSetting() /*noexcept*/;

public slots:
    void viewBSKSetting(QList<QString> listSetting) /*noexcept*/;
    void viewPKPSetting(QList<QString> listSetting) /*noexcept*/;
    void viewKSSetting(QList<QString> listSetting) /*noexcept*/;
    void viewKMSetting(QList<QString> listSetting) /*noexcept*/;

    void emitStartPort(QString portname, TypeUnitNodeSystem typeSystem) /*noexcept*/;
    void emitStopPort(TypeUnitNodeSystem typeSystem) /*noexcept*/;

    void fillComboBoxs();

private:
    Ui::DialogSerialPortSetting *ui;

signals:
    void startPort(QString portname, TypeUnitNodeSystem typeSystem);
    void stopPort(TypeUnitNodeSystem typeSystem);

    void startBSK(QString portname);
    void startPKP(QString portname);
    void startKS(QString portname);
    void startKM(QString portname);
    void stopBSK();
    void stopPKP();
    void stopKS();
    void stopKM();

private slots:
    void on_pushButton_stopKMPort_clicked() /*noexcept*/;
    void on_pushButton_stopPKPPort_clicked() /*noexcept*/;
    void on_pushButton_stopBSKPort_clicked() /*noexcept*/;
    void on_pushButton_startKMPort_clicked() /*noexcept*/;
    void on_pushButton_startPKPPort_clicked() /*noexcept*/;
    void on_pushButton_startBSKPort_clicked() /*noexcept*/;
    void on_pushButton_startKSPort_clicked() /*noexcept*/;
    void on_pushButton_stopKSPort_clicked() /*noexcept*/;
};

#endif // DIALOGSERIALPORTSETTING_H
