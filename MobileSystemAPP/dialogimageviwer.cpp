#include <dialogimageviwer.h>
#include <ui_dialogimageviwer.h>

DialogImageViwer::DialogImageViwer(QWidget *parent) /*noexcept*/ :
    QDialog(parent),
    ui(new Ui::DialogImageViwer)
{
    ui->setupUi(this);

    scaleFactor = 1;
//    this->setWindowState(this->windowState() ^ Qt::WindowFullScreen);
//    connect(this, SLOT())
}

DialogImageViwer::~DialogImageViwer() /*noexcept*/
{
    try
    {
        delete ui;
    }
    catch(...)
    {
#ifdef QT_DEBUG
        qDebug() << "Assert(~DialogImageViwer)";
#endif
        return;
    }
}

void DialogImageViwer::setMainPXM(QPixmap pxm,
                                  QString label,
                                  QSqlRecord record,
                                  int numShot) /*noexcept*/
{
    scaleFactor = 1;
    mainPXM = pxm;
    mainLable = label;
    imgRecord = record;
    numCurrentShot = numShot;


    QPixmap scaledMainPXM(mainPXM);

    scaledMainPXM = mainPXM.scaled(ui->scrollArea->size(),
                                   Qt::KeepAspectRatio,
                                   Qt::FastTransformation);

    scaleFactor = (double)scaledMainPXM.height() / (double)mainPXM.height();

    ui->label_viewIMGLable->setText(mainLable);
    ui->label_viewIMG->setPixmap(scaledMainPXM);
}

//void DialogImageViwer::closeEvent(QCloseEvent *event)
//{}

void DialogImageViwer::resizeEvent(QResizeEvent *e) /*noexcept*/
{
    if(mainPXM.isNull())
    {
        return;
    }

    QPixmap scaledMainPXM;
    scaledMainPXM = mainPXM.scaled(ui->scrollArea->size(),
                                   Qt::KeepAspectRatio,
                                   Qt::FastTransformation);

    double factor((double)scaledMainPXM.height() / (double)mainPXM.height());
    scaleFactor = factor;

    ui->label_viewIMG->setPixmap(scaledMainPXM);

    adjustScrollBar(ui->scrollArea->horizontalScrollBar(), factor);
    adjustScrollBar(ui->scrollArea->verticalScrollBar(), factor);
}

void DialogImageViwer::on_pushButton_zoomPlus_IMG_clicked() /*noexcept*/
{
    scaleImage(1.2);
}

void DialogImageViwer::on_pushButton_zoomMinus_IMG_clicked() /*noexcept*/
{
    scaleImage(0.8);
}

void DialogImageViwer::scaleImage(double factor) /*noexcept*/
{
    scaleFactor = scaleFactor * factor;
    ui->label_viewIMG->setPixmap(mainPXM.scaled(mainPXM.size() * scaleFactor,
                                                Qt::KeepAspectRatio,
                                                Qt::FastTransformation));

    adjustScrollBar(ui->scrollArea->horizontalScrollBar(), factor);
    adjustScrollBar(ui->scrollArea->verticalScrollBar(), factor);
}

void DialogImageViwer::adjustScrollBar(QScrollBar *scrollBar, double factor) /*noexcept*/
{
    if(!scrollBar)
        return;
    scrollBar->setValue(int(factor * scrollBar->value()
                            + ((factor - 1) * scrollBar->pageStep()/2)));
}

void DialogImageViwer::on_pushButton_Close_ImageView_clicked() /*noexcept*/
{
    this->close();
}

void DialogImageViwer::on_pushButton_Save_clicked()
{
    QString filename("VK%1[%2] num%3 %4 %5.jpg");
    filename = filename.arg(imgRecord.value("numberVK").toInt())
                       .arg(QString(imgRecord.value("idUN").toByteArray().toHex().toUpper()))
                       .arg(numCurrentShot)
                       .arg(imgRecord.value("date").toDate().toString("dd_MM_yy"))
                       .arg(imgRecord.value("time").toTime().toString("hh_mm_ss"));


    QString out_filename = QFileDialog::getSaveFileName(this,
                                                        tr("Save File"),
                                                        filename,
                                                        tr("AllFiles (*.*);;Images (*.jpg)"));

    if (out_filename != "")
    {
        QFile file(out_filename);
        if (!file.open(QIODevice::WriteOnly)) {
            // error message
        }
        else
        {
            QString strdata("dataIMG%1");
            strdata = strdata.arg(numCurrentShot);

            QByteArray ba(imgRecord.value(strdata).toByteArray());
            file.write(ba);
            file.close();
        }
    }
}
