﻿#ifndef DATABASEMANAGER_H
#define DATABASEMANAGER_H

#include <QObject>
#include <QFile>
#include <QtSql>

#include <QMutex>

#include <unitnode.h>
#include <proxymetod.h>


class DataBaseManager : public QObject
{
    Q_OBJECT
public:
    explicit DataBaseManager(QObject *parent = nullptr) /*noexcept*/;
    virtual ~DataBaseManager() /*noexcept*/;

    QByteArray lastInputMSG_KM,
               lastInputMSG_KS,
               lastInputMSG_BSK;
    QString m_typeLanguage;

    QMutex *m_resursLock;
    bool    m_resurceStatus;

    bool lockResource();
    void unlockResource();

    void setTypeLanguage(QString type) /*noexcept*/;
    QString typeLanguage() /*noexcept*/;


    QSqlDatabase m_db;
//    QMutex lockerWriteUpdate;
    void createDataBase() /*noexcept*/;



    quint32 addNewIMGPackFromBT(UnitNode* un) /*noexcept*/;

    quint32 addNewMSG(UnitNode* un,
                      quint8 type,
                      QByteArray msg,
                      bool ioType,
                      TypeUnitNodeSystem typeSystem,
                      TypeGroupMSG typeGroup,
                      bool flagHidden = false) /*noexcept*/;

    quint32 getUNID(QByteArray id,
                    TypeUnitNodeSystem typeSystem) /*noexcept*/;
    quint32 getUNID(QByteArray id,
                    TypeUnitNodeDevice type,
                    TypeUnitNodeSystem typeSystem,
                    QByteArray macUnitNode = "") /*noexcept*/;

    quint32 setIMGCheckedUp(quint32 index,
                            bool value = true) /*noexcept*/;

    quint32 addNewIMGPackFromBT(UnitNode* un,
                                /*quint8 type,*/
                                QByteArray msg/*,
                                bool ioType*/) /*noexcept*/;
    quint32 updateIMGPackFromBT(quint32 index,
                                UnitNode* un,
                                /*quint8 type,*/
                                QByteArray msg/*,
                                bool ioType*/) /*noexcept*/;

    quint32 addNewIMGPack(UnitNode* un) /*noexcept*/;
    quint32 addNewIMGPackFromKS(UnitNode* un) /*noexcept*/;
    quint32 updateIMGPackFromKS(UnitNode* un,
                                quint32 index = 0) /*noexcept*/;
	quint32 new_addNewIMGPackFromKS(UnitNode* un) /*noexcept*/;
	quint32 new_updateIMGPackFromKS(UnitNode* un,
                         		quint32 index = 0) /*noexcept*/;


    QSqlQuery getAllUN() /*noexcept*/;
    QList<quint32> getAllUNRecords() /*noexcept*/;
    QSqlQuery getUN(quint32 id) /*noexcept*/;
    QSqlRecord getUNRecord(quint32 id) /*noexcept*/;

    QSqlQuery getFilter_MSG(FilterStruct filter, TypeUnitNodeSystem typeSystem) /*noexcept*/;
    QList<quint32> getFilter_MSGRecords(FilterStruct filter, TypeUnitNodeSystem typeSystem) /*noexcept*/;
    QSqlRecord get_MSGRecord(quint32 idMSG) /*noexcept*/;

    QSqlQuery getFilterIMG(FilterStruct filter, TypeUnitNodeSystem typeSystem) /*noexcept*/;
    QList<quint32> getFilterIMGRecords(FilterStruct filter, TypeUnitNodeSystem typeSystem) /*noexcept*/;
    QSqlRecord getIMGRecord(quint32 idMSG) /*noexcept*/;
    QSqlRecord getLastQueueIMGRecord() /*noexcept*/;

    void loadUNFromDB(UnitNode* un) /*noexcept*/;

    quint64 updateForeignIDAlarm(UnitNode *un, quint64 idMSG0xC4) /*noexcept*/;
    QDate getMinDateMSG() /*noexcept*/;
    QList<int> getSectorGroupUN(TypeUnitNodeSystem typeSystem = KMSystemType) /*noexcept*/;
    QList<int> getNumberGroupUN(TypeUnitNodeSystem typeSystem = KMSystemType) /*noexcept*/;
    QList<TypeUnitNodeDevice> getTypeGroupUN(TypeUnitNodeSystem typeSystem = KMSystemType) /*noexcept*/;

public slots:
    quint32 addNewUN(UnitNode* un) /*noexcept*/;
    quint32 updateUN(UnitNode* un) /*noexcept*/;
    void uploadUNToDB(UnitNode* un) /*noexcept*/;

signals:
    void insertNewMSG(quint32 id);
    void insertNewIMG(quint32 id);
};

#endif // DATABASEMANAGER_H
