#ifndef APPTRANSLATOR_H
#define APPTRANSLATOR_H

#include <QObject>
#include <QApplication>
#include <mainwindow.h>
class AppTranslator : public QObject
{
    Q_OBJECT
public:
    QApplication *m_app;
    MainWindow *m_w;
//    explicit AppTranslator(QObject *parent = nullptr) /*noexcept*/;
    explicit AppTranslator(QApplication *app,
                           MainWindow *w,
                           QObject *parent = nullptr) /*noexcept*/;
    virtual ~AppTranslator() /*noexcept*/;

signals:

public slots:
    void installTranslator(QTranslator *tr) /*noexcept*/;
    void removeTranslator(QTranslator *tr) /*noexcept*/;
};

#endif // APPTRANSLATOR_H
