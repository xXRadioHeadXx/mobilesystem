﻿#include <dialogserialportsetting.h>
#include <ui_dialogserialportsetting.h>

DialogSerialPortSetting::DialogSerialPortSetting(QWidget *parent) /*noexcept*/ :
    QDialog(parent),
    ui(new Ui::DialogSerialPortSetting)
{
    ui->setupUi(this);

#ifdef Q_OS_WIN
    for(int i(0); i < 50; i++)
    {
        QString str("COM%1");
        str = str.arg(i + 1);
        ui->comboBox_selectBSKPort->addItem(str);
        ui->comboBox_selectPKPPort->addItem(str);
        ui->comboBox_selectKSPort->addItem(str);
        ui->comboBox_selectKMPort->addItem(str);
    }
#endif

#ifdef Q_OS_LINUX
    QList<QSerialPortInfo> infos = QSerialPortInfo::availablePorts();
    for (QSerialPortInfo info : infos)
    {
        QString str(info.portName());
        ui->comboBox_selectBSKPort->addItem(str);
        ui->comboBox_selectPKPPort->addItem(str);
        ui->comboBox_selectKSPort->addItem(str);
        ui->comboBox_selectKMPort->addItem(str);
    }
#endif
}

DialogSerialPortSetting::~DialogSerialPortSetting() /*noexcept*/
{
    try
    {
        delete ui;
    }
    catch(...)
    {
#ifdef QT_DEBUG
        qDebug() << "Assert(~DialogSerialPortSetting)";
#endif
        return;
    }
}

void DialogSerialPortSetting::viewBSKSetting(QList<QString> listSetting) /*noexcept*/
{
    QString str("%1 %2,%3%4%5");
    if(listSetting.isEmpty())
    {
        str = tr("Closed");
    }
    else
    {
        str = str.arg(listSetting.at(0))
                 .arg(listSetting.at(1))
                 .arg(listSetting.at(2))
                 .arg(listSetting.at(3))
                 .arg(listSetting.at(4));
    }

    ui->label_BSKPortSetting->setText(str);
}

void DialogSerialPortSetting::viewPKPSetting(QList<QString> listSetting) /*noexcept*/
{
    QString str("%1 %2,%3%4%5");
    if(listSetting.isEmpty())
    {
        str = tr("Closed");
    }
    else
    {
        str = str.arg(listSetting.at(0))
                 .arg(listSetting.at(1))
                 .arg(listSetting.at(2))
                 .arg(listSetting.at(3))
                 .arg(listSetting.at(4));
    }

    ui->label_PKPPortSetting->setText(str);
}

void DialogSerialPortSetting::viewKSSetting(QList<QString> listSetting) /*noexcept*/
{
    QString str("%1 %2,%3%4%5");
    if(listSetting.isEmpty())
    {
        str = tr("Closed");
    }
    else
    {
        str = str.arg(listSetting.at(0))
                 .arg(listSetting.at(1))
                 .arg(listSetting.at(2))
                 .arg(listSetting.at(3))
                 .arg(listSetting.at(4));
    }

    ui->label_KSPortSetting->setText(str);
}

void DialogSerialPortSetting::viewKMSetting(QList<QString> listSetting) /*noexcept*/
{
    QString str("%1 %2,%3%4%5");
    if(listSetting.isEmpty())
    {
        str = tr("Closed");
    }
    else
    {
        str = str.arg(listSetting.at(0))
                 .arg(listSetting.at(1))
                 .arg(listSetting.at(2))
                 .arg(listSetting.at(3))
                 .arg(listSetting.at(4));
    }

    ui->label_KMPortSetting->setText(str);
}

void DialogSerialPortSetting::on_pushButton_startBSKPort_clicked() /*noexcept*/
{
    emit this->startBSK(ui->comboBox_selectBSKPort->currentText());
}

void DialogSerialPortSetting::on_pushButton_startPKPPort_clicked() /*noexcept*/
{
    emit this->startPKP(ui->comboBox_selectPKPPort->currentText());
}

void DialogSerialPortSetting::on_pushButton_startKSPort_clicked() /*noexcept*/
{
    emit this->startKS(ui->comboBox_selectKSPort->currentText());
}

void DialogSerialPortSetting::on_pushButton_startKMPort_clicked() /*noexcept*/
{
    emit this->startKM(ui->comboBox_selectKMPort->currentText());
}

void DialogSerialPortSetting::on_pushButton_stopBSKPort_clicked() /*noexcept*/
{
    emit this->stopBSK();
}

void DialogSerialPortSetting::on_pushButton_stopPKPPort_clicked() /*noexcept*/
{
    emit this->stopPKP();
}

void DialogSerialPortSetting::on_pushButton_stopKSPort_clicked() /*noexcept*/
{
    emit this->stopKS();
}

void DialogSerialPortSetting::on_pushButton_stopKMPort_clicked() /*noexcept*/
{
    emit this->stopKM();
}

void DialogSerialPortSetting::retranslate() /*noexcept*/
{
    ui->retranslateUi(this);
}

void DialogSerialPortSetting::emitStartPort(QString portname, TypeUnitNodeSystem typeSystem) /*noexcept*/
{
    emit this->startPort(portname, typeSystem);
}

void DialogSerialPortSetting::emitStopPort(TypeUnitNodeSystem typeSystem) /*noexcept*/
{
    emit this->stopPort(typeSystem);
}

void DialogSerialPortSetting::fillComboBoxs()
{
    QList<QSerialPortInfo> infos = QSerialPortInfo::availablePorts();
    for (QSerialPortInfo info : infos)
    {
        QString str(info.portName());
        ui->comboBox_selectBSKPort->addItem(str);
        ui->comboBox_selectPKPPort->addItem(str);
        ui->comboBox_selectKSPort->addItem(str);
        ui->comboBox_selectKMPort->addItem(str);
    }
}

