<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>DialogImageViwer</name>
    <message>
        <location filename="dialogimageviwer.ui" line="14"/>
        <source>Dialog</source>
        <translation>Диалог</translation>
    </message>
    <message>
        <location filename="dialogimageviwer.ui" line="74"/>
        <location filename="dialogimageviwer.ui" line="77"/>
        <location filename="dialogimageviwer.ui" line="80"/>
        <location filename="dialogimageviwer.ui" line="83"/>
        <location filename="dialogimageviwer.ui" line="86"/>
        <source>Zoom out</source>
        <translation>Уменьшить</translation>
    </message>
    <message>
        <location filename="dialogimageviwer.ui" line="121"/>
        <location filename="dialogimageviwer.ui" line="124"/>
        <location filename="dialogimageviwer.ui" line="127"/>
        <location filename="dialogimageviwer.ui" line="130"/>
        <location filename="dialogimageviwer.ui" line="133"/>
        <source>Zoom in</source>
        <translation>Увеличить</translation>
    </message>
    <message>
        <location filename="dialogimageviwer.ui" line="168"/>
        <location filename="dialogimageviwer.ui" line="171"/>
        <location filename="dialogimageviwer.ui" line="174"/>
        <location filename="dialogimageviwer.ui" line="177"/>
        <location filename="dialogimageviwer.ui" line="180"/>
        <source>Save</source>
        <translation>Сохранить</translation>
    </message>
    <message>
        <location filename="dialogimageviwer.ui" line="212"/>
        <location filename="dialogimageviwer.ui" line="215"/>
        <location filename="dialogimageviwer.ui" line="218"/>
        <location filename="dialogimageviwer.ui" line="221"/>
        <location filename="dialogimageviwer.ui" line="224"/>
        <source>Close</source>
        <translation>Закрыть</translation>
    </message>
    <message>
        <source>CLose</source>
        <translation type="vanished">Закрыть</translation>
    </message>
    <message>
        <location filename="dialogimageviwer.cpp" line="123"/>
        <source>Save File</source>
        <translation>Сохранить Файл</translation>
    </message>
    <message>
        <location filename="dialogimageviwer.cpp" line="125"/>
        <source>AllFiles (*.*);;Images (*.jpg)</source>
        <translation>Все файлы (*.*);;Изображения (*.jpg)</translation>
    </message>
</context>
<context>
    <name>DialogSerialPortSetting</name>
    <message>
        <location filename="dialogserialportsetting.ui" line="29"/>
        <source>Serial ports selection and setting</source>
        <translation>Выбор и настройка последовательных портов</translation>
    </message>
    <message>
        <location filename="dialogserialportsetting.ui" line="44"/>
        <source>Pautina 433</source>
        <oldsource>Pautina</oldsource>
        <translatorcomment>Паутина</translatorcomment>
        <translation>Паутина 433</translation>
    </message>
    <message>
        <location filename="dialogserialportsetting.ui" line="124"/>
        <source>PCP</source>
        <translation>ПКП</translation>
    </message>
    <message>
        <location filename="dialogserialportsetting.ui" line="204"/>
        <source>Kamuflazh 868</source>
        <translation>Кумуфляж 868</translation>
    </message>
    <message>
        <location filename="dialogserialportsetting.ui" line="281"/>
        <source>Kamuflazh 2.4</source>
        <translation>Камуфляж 2.4</translation>
    </message>
    <message>
        <location filename="dialogserialportsetting.ui" line="76"/>
        <location filename="dialogserialportsetting.ui" line="156"/>
        <location filename="dialogserialportsetting.ui" line="236"/>
        <location filename="dialogserialportsetting.ui" line="307"/>
        <source>Start</source>
        <translation>Старт</translation>
    </message>
    <message>
        <location filename="dialogserialportsetting.ui" line="92"/>
        <location filename="dialogserialportsetting.ui" line="172"/>
        <location filename="dialogserialportsetting.ui" line="252"/>
        <location filename="dialogserialportsetting.ui" line="320"/>
        <source>Stop</source>
        <translation>Стоп</translation>
    </message>
    <message>
        <source>Kamuflazh</source>
        <translation type="vanished">Камуфляж</translation>
    </message>
    <message>
        <source>BSK</source>
        <translation type="vanished">БСК</translation>
    </message>
    <message>
        <location filename="dialogserialportsetting.ui" line="108"/>
        <location filename="dialogserialportsetting.ui" line="188"/>
        <location filename="dialogserialportsetting.ui" line="268"/>
        <location filename="dialogserialportsetting.ui" line="333"/>
        <location filename="dialogserialportsetting.cpp" line="55"/>
        <location filename="dialogserialportsetting.cpp" line="74"/>
        <location filename="dialogserialportsetting.cpp" line="93"/>
        <location filename="dialogserialportsetting.cpp" line="112"/>
        <source>Closed</source>
        <translation>Закрыт</translation>
    </message>
    <message>
        <source>Kiparis</source>
        <translation type="vanished">Кипарис</translation>
    </message>
</context>
<context>
    <name>DialogTableArchive</name>
    <message>
        <location filename="dialogtablearchive.ui" line="14"/>
        <source>MainWindow</source>
        <translation>Архив</translation>
    </message>
    <message>
        <source>Filter</source>
        <translation type="vanished">Фильтр</translation>
    </message>
    <message>
        <location filename="dialogtablearchive.ui" line="201"/>
        <source>Time</source>
        <translation>Время</translation>
    </message>
    <message>
        <location filename="dialogtablearchive.ui" line="106"/>
        <source>Date</source>
        <translation>Дата</translation>
    </message>
    <message>
        <location filename="dialogtablearchive.ui" line="369"/>
        <source>Number</source>
        <translation>Номер</translation>
    </message>
    <message>
        <location filename="dialogtablearchive.ui" line="511"/>
        <source>ID</source>
        <translation>ID</translation>
    </message>
    <message>
        <location filename="dialogtablearchive.ui" line="149"/>
        <location filename="dialogtablearchive.ui" line="241"/>
        <source>To</source>
        <translation>По</translation>
    </message>
    <message>
        <location filename="dialogtablearchive.ui" line="329"/>
        <source>Sector/Number</source>
        <translation>Уч/Номер</translation>
    </message>
    <message>
        <location filename="dialogtablearchive.ui" line="139"/>
        <location filename="dialogtablearchive.ui" line="231"/>
        <source>From</source>
        <translation>От</translation>
    </message>
    <message>
        <location filename="dialogtablearchive.ui" line="359"/>
        <source>Sector</source>
        <translation>Участок</translation>
    </message>
    <message>
        <location filename="dialogtablearchive.ui" line="280"/>
        <source>Type</source>
        <translation>Тип</translation>
    </message>
    <message>
        <location filename="dialogtablearchive.ui" line="408"/>
        <source>Type MSG</source>
        <translation>Тип Сообщения</translation>
    </message>
    <message>
        <location filename="dialogtablearchive.ui" line="438"/>
        <source>System</source>
        <translation>Система</translation>
    </message>
    <message>
        <location filename="dialogtablearchive.ui" line="448"/>
        <source>Connect</source>
        <translation>Связь</translation>
    </message>
    <message>
        <location filename="dialogtablearchive.ui" line="458"/>
        <source>Discharge</source>
        <translation>Разряд</translation>
    </message>
    <message>
        <location filename="dialogtablearchive.ui" line="468"/>
        <source>Alarm</source>
        <translation>Тревога</translation>
    </message>
    <message>
        <location filename="dialogtablearchive.ui" line="478"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="dialogtablearchive.ui" line="488"/>
        <source>Command</source>
        <translation>Команда</translation>
    </message>
    <message>
        <location filename="dialogtablearchive.ui" line="498"/>
        <source>FF</source>
        <translation>СК</translation>
    </message>
    <message>
        <location filename="dialogtablearchive.ui" line="560"/>
        <source>Reset</source>
        <translation>Сброс</translation>
    </message>
    <message>
        <location filename="dialogtablearchive.ui" line="573"/>
        <source>Apply</source>
        <translation>Принять</translation>
    </message>
    <message>
        <location filename="dialogtablearchive.ui" line="598"/>
        <source>toolBar</source>
        <translation>Панель инструментов</translation>
    </message>
    <message>
        <location filename="dialogtablearchive.ui" line="625"/>
        <source>Automatic scrolling</source>
        <translation>Автоматическая прокрутка</translation>
    </message>
    <message>
        <location filename="dialogtablearchive.ui" line="637"/>
        <location filename="dialogtablearchive.ui" line="640"/>
        <source>View Kamuflazh 2.4 archive of freeze-frames</source>
        <oldsource>View Kamuflazh archive of freeze-frames</oldsource>
        <translation>Показать архив кадров Камуфляж 2.4</translation>
    </message>
    <message>
        <location filename="dialogtablearchive.ui" line="667"/>
        <location filename="dialogtablearchive.ui" line="670"/>
        <source>View Kamuflazh 868 messages archive</source>
        <oldsource>View Kiparis messages archive</oldsource>
        <translation>Показать архив сообщений Камуфляж 868</translation>
    </message>
    <message>
        <location filename="dialogtablearchive.ui" line="682"/>
        <location filename="dialogtablearchive.ui" line="685"/>
        <source>View Pautina 433 messages archive</source>
        <oldsource>View Pautina messages archive</oldsource>
        <translation>Показать архив сообщений Паутина 433</translation>
    </message>
    <message>
        <location filename="dialogtablearchive.ui" line="697"/>
        <location filename="dialogtablearchive.ui" line="700"/>
        <source>View Kamuflazh 868 archive of freeze-frames</source>
        <oldsource>View Kiparis archive of freeze-frames</oldsource>
        <translation>Показать архив кадров Камуфляж 868</translation>
    </message>
    <message>
        <location filename="dialogtablearchive.ui" line="652"/>
        <location filename="dialogtablearchive.ui" line="655"/>
        <source>View Kamuflazh 2.4 messages archive</source>
        <oldsource>View &quot;Kamuflazh&quot; messages archive</oldsource>
        <translation>Показать вывод архива сообщений Камуфляж 2.4</translation>
    </message>
    <message>
        <location filename="dialogtablearchive.cpp" line="279"/>
        <location filename="dialogtablearchive.cpp" line="290"/>
        <location filename="dialogtablearchive.cpp" line="301"/>
        <location filename="dialogtablearchive.cpp" line="327"/>
        <location filename="dialogtablearchive.cpp" line="437"/>
        <location filename="dialogtablearchive.cpp" line="455"/>
        <location filename="dialogtablearchive.cpp" line="473"/>
        <location filename="dialogtablearchive.cpp" line="506"/>
        <source>All</source>
        <translation>Всё</translation>
    </message>
</context>
<context>
    <name>GraphicsMapManager</name>
    <message>
        <location filename="Graphics/graphicsmapmanager.cpp" line="744"/>
        <source>Choosing map dialogue</source>
        <translation>Диалог выбора подложки карты</translation>
    </message>
    <message>
        <location filename="Graphics/graphicsmapmanager.cpp" line="746"/>
        <source>AllFiles (*.*);;Images (*.jpg *.bmp)</source>
        <translation>Все файлы (*.*);;Изображения (*.jpg *.bmp)</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="1942"/>
        <location filename="mainwindow.ui" line="5622"/>
        <location filename="mainwindow.ui" line="5625"/>
        <source>Kamuflazh 2.4</source>
        <oldsource>Kamuflazh</oldsource>
        <translation>Камуфляж 2.4</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="600"/>
        <location filename="mainwindow.ui" line="603"/>
        <location filename="mainwindow.ui" line="606"/>
        <location filename="mainwindow.ui" line="609"/>
        <location filename="mainwindow.ui" line="612"/>
        <location filename="mainwindow.ui" line="798"/>
        <location filename="mainwindow.ui" line="801"/>
        <location filename="mainwindow.ui" line="804"/>
        <location filename="mainwindow.ui" line="985"/>
        <location filename="mainwindow.ui" line="988"/>
        <location filename="mainwindow.ui" line="991"/>
        <location filename="mainwindow.ui" line="1172"/>
        <location filename="mainwindow.ui" line="1175"/>
        <location filename="mainwindow.ui" line="1178"/>
        <location filename="mainwindow.ui" line="1181"/>
        <location filename="mainwindow.ui" line="1184"/>
        <source>Zoom in</source>
        <translation>Увеличить</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="647"/>
        <location filename="mainwindow.ui" line="650"/>
        <location filename="mainwindow.ui" line="653"/>
        <location filename="mainwindow.ui" line="656"/>
        <location filename="mainwindow.ui" line="659"/>
        <location filename="mainwindow.ui" line="839"/>
        <location filename="mainwindow.ui" line="842"/>
        <location filename="mainwindow.ui" line="845"/>
        <location filename="mainwindow.ui" line="1026"/>
        <location filename="mainwindow.ui" line="1029"/>
        <location filename="mainwindow.ui" line="1032"/>
        <location filename="mainwindow.ui" line="1219"/>
        <location filename="mainwindow.ui" line="1222"/>
        <location filename="mainwindow.ui" line="1225"/>
        <location filename="mainwindow.ui" line="1228"/>
        <location filename="mainwindow.ui" line="1231"/>
        <source>Zoom out</source>
        <translation>Уменьшить</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1812"/>
        <source>Bluetooth</source>
        <translation>Bluetooth</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1845"/>
        <location filename="mainwindow.ui" line="5931"/>
        <location filename="mainwindow.ui" line="5934"/>
        <source>inquiry</source>
        <translation>Запрос</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1864"/>
        <location filename="mainwindow.ui" line="5939"/>
        <location filename="mainwindow.ui" line="5942"/>
        <source>list</source>
        <translation>Лист</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1883"/>
        <location filename="mainwindow.ui" line="5947"/>
        <location filename="mainwindow.ui" line="5950"/>
        <source>kill</source>
        <translation>Отключить</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1902"/>
        <location filename="mainwindow.ui" line="5955"/>
        <location filename="mainwindow.ui" line="5958"/>
        <source>txpower/rssi</source>
        <translation>Измерить</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1921"/>
        <source>set</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1975"/>
        <source>Unit state inquiry</source>
        <translation>Запрос состояния блока</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2024"/>
        <source>State</source>
        <translation>Состояние</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2046"/>
        <source>Inquiry for freeze-frames from device</source>
        <translation>Запрос стоп-кадров от устройства</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2079"/>
        <location filename="mainwindow.ui" line="3845"/>
        <source>FF Inquiry</source>
        <translation>Запрос СК</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2095"/>
        <source>Automatic inquiry</source>
        <translation>Автодозапрос</translation>
    </message>
    <message>
        <source>FF transmission parameters</source>
        <translation type="vanished">Параметры передачи СК</translation>
    </message>
    <message>
        <source>Size</source>
        <translation type="vanished">Размер</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2389"/>
        <location filename="mainwindow.ui" line="3267"/>
        <location filename="mainwindow.ui" line="3563"/>
        <location filename="mainwindow.ui" line="4124"/>
        <location filename="mainwindow.ui" line="4287"/>
        <location filename="mainwindow.ui" line="4478"/>
        <location filename="mainwindow.ui" line="4682"/>
        <location filename="mainwindow.ui" line="4743"/>
        <location filename="mainwindow.ui" line="4904"/>
        <location filename="mainwindow.ui" line="5155"/>
        <location filename="mainwindow.ui" line="5269"/>
        <source>Change</source>
        <translation>Изменить</translation>
    </message>
    <message>
        <source>Timeout</source>
        <translation type="vanished">Задержка</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2130"/>
        <source>Parameters of video camera settings</source>
        <translation>Параметры настроек видеокамеры</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2289"/>
        <source>D2:</source>
        <translation>СО2:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2270"/>
        <source>T1:</source>
        <translation>Т1:</translation>
    </message>
    <message>
        <source>Blt D</source>
        <translation type="vanished">Встр СО</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2421"/>
        <source>D1:</source>
        <translation>СО1:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2544"/>
        <source>T2:</source>
        <translation>Т2:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2585"/>
        <source>D3:</source>
        <translation>СО3:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2714"/>
        <source>T3:</source>
        <translation>Т3:</translation>
    </message>
    <message>
        <source>Kit:</source>
        <translation type="vanished">Комплект:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2762"/>
        <source>And/Or</source>
        <translation>И/Или</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2823"/>
        <source>Other D</source>
        <translation>Внешнее СО</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2309"/>
        <location filename="mainwindow.ui" line="4039"/>
        <source>1-st (5 frames)</source>
        <translation>1й (5 кадров)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2314"/>
        <location filename="mainwindow.ui" line="4044"/>
        <source>2-nd (2 frames)</source>
        <translation>2й (2 кадра)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2319"/>
        <location filename="mainwindow.ui" line="4049"/>
        <source>3-rd (1 frame)</source>
        <translation>3й (1 кадр)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2402"/>
        <source>Mode:</source>
        <translation>Режим:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2191"/>
        <location filename="mainwindow.ui" line="2446"/>
        <location filename="mainwindow.ui" line="2610"/>
        <location filename="mainwindow.ui" line="3616"/>
        <source>BSK-S</source>
        <translation>БСК-С</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2196"/>
        <location filename="mainwindow.ui" line="2451"/>
        <location filename="mainwindow.ui" line="2615"/>
        <location filename="mainwindow.ui" line="3621"/>
        <source>BSK-RLO</source>
        <translation>БСК-РЛО</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2201"/>
        <location filename="mainwindow.ui" line="2456"/>
        <location filename="mainwindow.ui" line="2620"/>
        <location filename="mainwindow.ui" line="3626"/>
        <source>BSK-RLD</source>
        <translation>БСК-РЛД</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2206"/>
        <location filename="mainwindow.ui" line="2461"/>
        <location filename="mainwindow.ui" line="2625"/>
        <location filename="mainwindow.ui" line="3631"/>
        <source>BSK-RWD</source>
        <translation>БСК-РВД</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2211"/>
        <location filename="mainwindow.ui" line="2466"/>
        <location filename="mainwindow.ui" line="2630"/>
        <source>BSK-M</source>
        <translation>БСК-М</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2216"/>
        <location filename="mainwindow.ui" line="2471"/>
        <location filename="mainwindow.ui" line="2635"/>
        <location filename="mainwindow.ui" line="3641"/>
        <source>BSK-BVU</source>
        <translation>БСК-БВУ</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2221"/>
        <location filename="mainwindow.ui" line="2476"/>
        <location filename="mainwindow.ui" line="2640"/>
        <location filename="mainwindow.ui" line="3646"/>
        <source>BSK-O</source>
        <translation>БСК-О</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2226"/>
        <location filename="mainwindow.ui" line="2481"/>
        <location filename="mainwindow.ui" line="2645"/>
        <location filename="mainwindow.ui" line="3651"/>
        <source>BSK-IK</source>
        <translation>БСК-ИК</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2231"/>
        <location filename="mainwindow.ui" line="2486"/>
        <location filename="mainwindow.ui" line="2650"/>
        <location filename="mainwindow.ui" line="3656"/>
        <source>BSK-RW</source>
        <translation>БСК-РВ</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2236"/>
        <location filename="mainwindow.ui" line="2491"/>
        <location filename="mainwindow.ui" line="2655"/>
        <source>BSK-RWP</source>
        <translation>БСК-РВП</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2241"/>
        <location filename="mainwindow.ui" line="2496"/>
        <location filename="mainwindow.ui" line="2660"/>
        <source>BSK-XXX</source>
        <translation>БСК-ХХХ</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2246"/>
        <location filename="mainwindow.ui" line="2501"/>
        <location filename="mainwindow.ui" line="2665"/>
        <location filename="mainwindow.ui" line="3671"/>
        <source>BSK-VSO</source>
        <translation>БСК-ВСО</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2251"/>
        <location filename="mainwindow.ui" line="2506"/>
        <location filename="mainwindow.ui" line="2670"/>
        <source>D001</source>
        <translation>СО 0001</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2256"/>
        <location filename="mainwindow.ui" line="2511"/>
        <location filename="mainwindow.ui" line="2675"/>
        <source>D002</source>
        <translation>СО 0002</translation>
    </message>
    <message>
        <source>Port</source>
        <translation type="vanished">COM порт</translation>
    </message>
    <message>
        <source>ReOpen</source>
        <translation type="vanished">Переоткрыть</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2837"/>
        <source>Console</source>
        <translation>Консоль</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2886"/>
        <source>Console:</source>
        <translation>Консоль:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2921"/>
        <source>Link:</source>
        <translation>Ссылка:</translation>
    </message>
    <message>
        <source>Device:</source>
        <translation type="vanished">Устройство:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1745"/>
        <location filename="mainwindow.ui" line="1783"/>
        <location filename="mainwindow.ui" line="3364"/>
        <location filename="mainwindow.ui" line="3421"/>
        <location filename="mainwindow.ui" line="3438"/>
        <location filename="mainwindow.ui" line="4982"/>
        <location filename="mainwindow.ui" line="5020"/>
        <source>***</source>
        <translation>***</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1764"/>
        <location filename="mainwindow.ui" line="3402"/>
        <location filename="mainwindow.ui" line="5001"/>
        <source>Name:</source>
        <translation>Имя:</translation>
    </message>
    <message>
        <source>Close when it is completed</source>
        <translation type="vanished">Закрыть после выполнения</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="3929"/>
        <source>Select VK</source>
        <translation>Выбрать ВК</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="3956"/>
        <source>VK1</source>
        <translation>ВК1</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="3972"/>
        <source>VK2</source>
        <translation>ВК2</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="3774"/>
        <source>Command</source>
        <translation>Команда</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2531"/>
        <location filename="mainwindow.ui" line="4579"/>
        <location filename="mainwindow.ui" line="5719"/>
        <source>Get Setting</source>
        <translation>Запрос настроек</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="20"/>
        <source>Mobile Complex</source>
        <translation>Мобильный Комплекс</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1726"/>
        <location filename="mainwindow.ui" line="3383"/>
        <location filename="mainwindow.ui" line="4963"/>
        <source>ID:</source>
        <translation>ID:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="3240"/>
        <source>RM-433</source>
        <translation>РМ-433</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2769"/>
        <location filename="mainwindow.ui" line="3280"/>
        <source>Kit Number</source>
        <translation>Номер комплекта</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4007"/>
        <source>Change mode VK</source>
        <translation>Изменить режим ВК</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4025"/>
        <location filename="mainwindow.cpp" line="2790"/>
        <location filename="mainwindow.cpp" line="3962"/>
        <source>Mode</source>
        <translation>Режим</translation>
    </message>
    <message>
        <source>T1</source>
        <translation type="vanished">Т1</translation>
    </message>
    <message>
        <source>T2</source>
        <translation type="vanished">Т2</translation>
    </message>
    <message>
        <source>T3</source>
        <translation type="vanished">Т3</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="3087"/>
        <source>On/Off D</source>
        <oldsource>Switch On/Off D</oldsource>
        <translation>Вкл/Откл СО</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2746"/>
        <location filename="mainwindow.ui" line="3114"/>
        <source>Self D</source>
        <translation>Встроенное СО</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="3127"/>
        <location filename="mainwindow.ui" line="3166"/>
        <location filename="mainwindow.ui" line="3192"/>
        <source>On</source>
        <translation>Вкл</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="3153"/>
        <location filename="mainwindow.ui" line="3205"/>
        <location filename="mainwindow.ui" line="3218"/>
        <source>Off</source>
        <translation>Откл</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="3140"/>
        <location filename="mainwindow.ui" line="3676"/>
        <source>Other D1</source>
        <translation>Внешнее СО1</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="3179"/>
        <location filename="mainwindow.ui" line="3681"/>
        <source>Other D2</source>
        <translation>Внешнее СО2</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="3867"/>
        <source>List D from VK</source>
        <translation>Список привязанных СО</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="3907"/>
        <source>Update list</source>
        <translation>Обновить список</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="3585"/>
        <source>Add D to VK</source>
        <translation>Добавить привязку СО к ВК</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="3636"/>
        <source>BSK-MSO</source>
        <translation>БСК-МСО</translation>
    </message>
    <message>
        <source>BSK-VK</source>
        <translation type="vanished">БСК-ВК</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="3661"/>
        <source>BSK-RVP</source>
        <translation>БСК-РВП</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="3666"/>
        <source>BSK-PKSC</source>
        <translation>БСК-ПКСЧ</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="3695"/>
        <source>Add</source>
        <translation>Добавить</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="3714"/>
        <source>ID D</source>
        <translation>ID СО</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="3733"/>
        <source>Type D</source>
        <translation>Тип СО</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="3026"/>
        <source>Remove D from VK</source>
        <translation>Удалить привязку СО от ВК</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation type="vanished">Отключить</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1693"/>
        <location filename="mainwindow.ui" line="3331"/>
        <location filename="mainwindow.ui" line="4930"/>
        <source>Selected device:</source>
        <translation>Выбранное устройство:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="902"/>
        <location filename="mainwindow.ui" line="905"/>
        <location filename="mainwindow.ui" line="908"/>
        <location filename="mainwindow.ui" line="1089"/>
        <location filename="mainwindow.ui" line="1092"/>
        <location filename="mainwindow.ui" line="1095"/>
        <location filename="mainwindow.ui" line="1288"/>
        <location filename="mainwindow.ui" line="1291"/>
        <location filename="mainwindow.ui" line="1294"/>
        <location filename="mainwindow.ui" line="1297"/>
        <location filename="mainwindow.ui" line="1300"/>
        <location filename="mainwindow.cpp" line="5609"/>
        <location filename="mainwindow.cpp" line="5610"/>
        <location filename="mainwindow.cpp" line="5611"/>
        <location filename="mainwindow.cpp" line="5627"/>
        <location filename="mainwindow.cpp" line="5628"/>
        <location filename="mainwindow.cpp" line="5629"/>
        <location filename="mainwindow.cpp" line="5645"/>
        <location filename="mainwindow.cpp" line="5646"/>
        <location filename="mainwindow.cpp" line="5647"/>
        <source>Edit</source>
        <translation>Редактировать</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4553"/>
        <location filename="mainwindow.ui" line="5698"/>
        <location filename="mainwindow.ui" line="5701"/>
        <location filename="mainwindow.ui" line="5967"/>
        <location filename="mainwindow.ui" line="5970"/>
        <location filename="mainwindow.ui" line="5979"/>
        <location filename="mainwindow.ui" line="5982"/>
        <source>Reset Alarm</source>
        <translation>Сброс Тревоги</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="3463"/>
        <location filename="mainwindow.ui" line="4372"/>
        <source>Change area and number</source>
        <translation>Изменить участок и номер устройства</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="3499"/>
        <location filename="mainwindow.ui" line="4408"/>
        <location filename="mainwindow.ui" line="4699"/>
        <location filename="mainwindow.ui" line="5174"/>
        <source>Area</source>
        <translation>Участок</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="3534"/>
        <location filename="mainwindow.ui" line="4443"/>
        <location filename="mainwindow.ui" line="5215"/>
        <source>Number</source>
        <translation>Номер</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="3832"/>
        <source>Test FF Inquiry</source>
        <translation>Тест СК</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4500"/>
        <source>Command:</source>
        <translation>Команда:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4527"/>
        <location filename="mainwindow.ui" line="5686"/>
        <location filename="mainwindow.ui" line="5689"/>
        <source>Switch ON</source>
        <translation>Включить</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4540"/>
        <location filename="mainwindow.ui" line="5678"/>
        <location filename="mainwindow.ui" line="5681"/>
        <source>Switch OFF</source>
        <translation>Отключить</translation>
    </message>
    <message>
        <source>Reset</source>
        <translation type="vanished">Сброс</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4566"/>
        <location filename="mainwindow.ui" line="5706"/>
        <source>Send DK</source>
        <translation>ДК</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4595"/>
        <source>Settings</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4647"/>
        <source>min</source>
        <translation>минимальный</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4652"/>
        <source>midle</source>
        <translation>средний</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4657"/>
        <source>hight midl</source>
        <translation>выше среднего</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4662"/>
        <source>hight</source>
        <translation>высокий</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4633"/>
        <location filename="mainwindow.ui" line="4835"/>
        <source>Sensetivity</source>
        <translation>Чувствительность</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="703"/>
        <location filename="mainwindow.ui" line="706"/>
        <location filename="mainwindow.ui" line="709"/>
        <location filename="mainwindow.ui" line="712"/>
        <location filename="mainwindow.ui" line="715"/>
        <source>Full Screen</source>
        <translation>Во весь экран</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4071"/>
        <source>PrAI</source>
        <translation>Интервал ПредТр, с</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="3057"/>
        <source>All</source>
        <translation>Всё</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="3428"/>
        <source>Lead:</source>
        <translation>Ведущий:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4294"/>
        <source>SpaserH_1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4145"/>
        <source>AI</source>
        <translation>Интервал Тр, с</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4184"/>
        <source>PstAI</source>
        <translation>Интервал ПстТр, с</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4212"/>
        <source>SpaserH_2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4242"/>
        <source>Standard</source>
        <translation>Стандартный</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4247"/>
        <source>Increased Attention</source>
        <oldsource>Increased Anxiety</oldsource>
        <translation>Повышенное Внимание</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4713"/>
        <source>20</source>
        <translation>20</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4718"/>
        <source>40</source>
        <translation>40</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4723"/>
        <source>60</source>
        <translation>60</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4754"/>
        <source>Alarm</source>
        <translation>Тревога</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4761"/>
        <source>Unknown</source>
        <translation>Не Определённый</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4768"/>
        <source>One</source>
        <translation>Одиночный</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4775"/>
        <source>Train</source>
        <translation>Поезд</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4782"/>
        <source>Group</source>
        <translation>Группа</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4789"/>
        <source>Direction</source>
        <translation>Направление</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4796"/>
        <source>Transport</source>
        <translation>ТС</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4803"/>
        <source>Winter</source>
        <translation>Зима</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4810"/>
        <source>Far Transport</source>
        <translation>Удалённое ТС</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4817"/>
        <source>Summer</source>
        <translation>Лето</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4849"/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4854"/>
        <source>2</source>
        <translation>2</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4859"/>
        <source>3</source>
        <translation>3</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4864"/>
        <source>4</source>
        <translation>4</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4869"/>
        <source>5</source>
        <translation>5</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4874"/>
        <source>6</source>
        <translation>6</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4879"/>
        <source>7</source>
        <translation>7</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4884"/>
        <source>8</source>
        <translation>8</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="5036"/>
        <source>Configuration</source>
        <translation>Конфигурация</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="5069"/>
        <source>Num Kit</source>
        <translation>Номер комплекта</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="5305"/>
        <source>mainToolBar</source>
        <translation>Главная панель инструментов</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="5337"/>
        <source>toolBar</source>
        <translation>Панель инструментов</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="5369"/>
        <source>File</source>
        <translation>Файл</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="5379"/>
        <source>Options</source>
        <translation>Опции</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="5383"/>
        <source>Switch lenguage</source>
        <translation>Выбор языка</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="5404"/>
        <source>View</source>
        <translation>Вид</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="5415"/>
        <source>Tools</source>
        <translation>Инструменты</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="5434"/>
        <location filename="mainwindow.ui" line="5437"/>
        <source>Open dialogue for ports setting</source>
        <translation>Открыть диалог настройки портов</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="5449"/>
        <source>Download new map</source>
        <translation>Загрузить новую карту</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="5462"/>
        <source>Automatic scrolling</source>
        <translation>Автоматическая прокрутка</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="5474"/>
        <source>View archive of all messages</source>
        <translation>Показать вывод архива всех сообщений</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="5486"/>
        <source>View archive of freeze-frames</source>
        <translation>Показать архив стоп-кадров</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="6082"/>
        <location filename="mainwindow.ui" line="6085"/>
        <source>Get Config PKP</source>
        <translation>Получить конфигурацию ПКП</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="6094"/>
        <location filename="mainwindow.ui" line="6097"/>
        <source>Set as first reference geopoint</source>
        <translation>Установить первую опорную точку</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="6106"/>
        <location filename="mainwindow.ui" line="6109"/>
        <source>Set as second reference geopoint</source>
        <translation>Установить вторую опорную точку</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="6118"/>
        <location filename="mainwindow.ui" line="6121"/>
        <source>Set as reference geopoint</source>
        <translation>Установить опорную точку</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="3804"/>
        <location filename="mainwindow.ui" line="3807"/>
        <location filename="mainwindow.ui" line="3810"/>
        <location filename="mainwindow.ui" line="3813"/>
        <location filename="mainwindow.ui" line="3816"/>
        <location filename="mainwindow.ui" line="3819"/>
        <location filename="mainwindow.ui" line="5541"/>
        <location filename="mainwindow.ui" line="5544"/>
        <location filename="mainwindow.ui" line="6074"/>
        <location filename="mainwindow.ui" line="6077"/>
        <source>Connection test</source>
        <translation>Проверка связи</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="5637"/>
        <location filename="mainwindow.ui" line="5643"/>
        <source>Kamuflazh 868</source>
        <translation>Камуфляж 868</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="5640"/>
        <source>Kamuflazh 868</source>
        <comment>Kiparis</comment>
        <extracomment>Kiparis</extracomment>
        <translation>Камуфляж 868</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="5724"/>
        <location filename="mainwindow.ui" line="5727"/>
        <source>Name Target Kamuflazh 868 Device</source>
        <translation>Устройство Камуфляж 868</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="5819"/>
        <location filename="mainwindow.ui" line="5822"/>
        <source>Name Target Kamuflazh 2.4 Device</source>
        <translation>Устройство Камуфляж 2.4</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="5874"/>
        <location filename="mainwindow.ui" line="5877"/>
        <source>Sound switcher</source>
        <translation>Переключатель звука</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="5911"/>
        <location filename="mainwindow.ui" line="5914"/>
        <location filename="mainwindow.cpp" line="5272"/>
        <location filename="mainwindow.cpp" line="5273"/>
        <source>Operator work mode (switch on editor work mode)</source>
        <translation>Режим работы оператора (включить режим работы редактора)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="3071"/>
        <location filename="mainwindow.ui" line="5923"/>
        <location filename="mainwindow.ui" line="5926"/>
        <source>Delete</source>
        <translation>Удалить</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2415"/>
        <source>Operator work mode</source>
        <translation>Режим работы оператора</translation>
    </message>
    <message>
        <source>Hidding</source>
        <translation type="vanished">Скрыть</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="5990"/>
        <location filename="mainwindow.ui" line="5993"/>
        <source>Big icon size</source>
        <translation>Иконки большого размера</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="6002"/>
        <location filename="mainwindow.ui" line="6005"/>
        <location filename="mainwindow.ui" line="6014"/>
        <location filename="mainwindow.ui" line="6017"/>
        <location filename="mainwindow.ui" line="6026"/>
        <location filename="mainwindow.ui" line="6029"/>
        <source>Reset Discharge</source>
        <translation>Сброс Разряда</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="6038"/>
        <location filename="mainwindow.ui" line="6041"/>
        <location filename="mainwindow.ui" line="6050"/>
        <location filename="mainwindow.ui" line="6053"/>
        <location filename="mainwindow.ui" line="6062"/>
        <location filename="mainwindow.ui" line="6065"/>
        <source>Reset Failure</source>
        <translation>Сброс Неисправости</translation>
    </message>
    <message>
        <source>2.4</source>
        <translatorcomment>2.4</translatorcomment>
        <translation type="vanished">2.4</translation>
    </message>
    <message>
        <source>868</source>
        <translatorcomment>868</translatorcomment>
        <translation type="vanished">868</translation>
    </message>
    <message>
        <source>868</source>
        <comment>Kiparis</comment>
        <extracomment>Kiparis</extracomment>
        <translatorcomment>868</translatorcomment>
        <translation type="vanished">868</translation>
    </message>
    <message>
        <source>View &quot;Kamuflazh&quot; messages archive</source>
        <translation type="vanished">Показать вывод архива сообщений &quot;Камуфляж&quot;</translation>
    </message>
    <message>
        <source>View Bluetooth messages archive</source>
        <translation type="vanished">Показать архив сообщений Bluetooth</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="5498"/>
        <source>View group of pictures</source>
        <translation>Показать группу изображений</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="5510"/>
        <source>View map</source>
        <translation>Показать карту</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="5523"/>
        <source>View archive in new window</source>
        <translation>Вывод архива в отдельном окне</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="5532"/>
        <source>Recent queuing freeze-frames</source>
        <translation>Последние стоп-кадры, стоящие в очереди</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2200"/>
        <location filename="mainwindow.cpp" line="5656"/>
        <source>Build network</source>
        <translation>Построить сеть</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="5557"/>
        <source>Setting and commands tool</source>
        <translation>Инструмент настройки и команд</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="5570"/>
        <source>View received freeze-frame</source>
        <translation>Показать пришедший стоп-кадр</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="5582"/>
        <source>Russian</source>
        <translation>Русский (язык интерфейса)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="5594"/>
        <source>English</source>
        <translation>Английский  (язык интерфейса)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="5610"/>
        <source>Devices view Table/ Tree</source>
        <translation>Представление устройств Таблица/Дерево</translation>
    </message>
    <message>
        <source>Kamuflag</source>
        <translation type="vanished">Камуфляж</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="5711"/>
        <location filename="mainwindow.ui" line="5714"/>
        <source>Name Target Pautina 433 Device</source>
        <oldsource>Name Target BSK UN</oldsource>
        <translation>Устройство Паутина 433</translation>
    </message>
    <message>
        <source>Name Target KS UN</source>
        <translation type="vanished">Устройство КС</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="5736"/>
        <location filename="mainwindow.ui" line="5831"/>
        <source>VK1: FF Inquiry</source>
        <translation>ВК1: Запрос СК</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="5745"/>
        <location filename="mainwindow.ui" line="5840"/>
        <source>VK1: Mode 1</source>
        <translation>ВК1: Режим 1</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="5754"/>
        <location filename="mainwindow.ui" line="5849"/>
        <source>VK1: Mode 2</source>
        <translation>ВК1: Режим 2</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="5763"/>
        <location filename="mainwindow.ui" line="5766"/>
        <location filename="mainwindow.ui" line="5858"/>
        <location filename="mainwindow.ui" line="5861"/>
        <source>VK1: Mode 3</source>
        <translation>ВК1: Режим 3</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="5775"/>
        <location filename="mainwindow.ui" line="5778"/>
        <source>VK2: FF Inquiry</source>
        <translation>ВК2: Запрос СК</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="5787"/>
        <location filename="mainwindow.ui" line="5790"/>
        <source>VK2: Mode 1</source>
        <translation>ВК2: Режим 1</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="5799"/>
        <location filename="mainwindow.ui" line="5802"/>
        <source>VK2: Mode 2</source>
        <translation>ВК2: Режим 2</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="5811"/>
        <location filename="mainwindow.ui" line="5814"/>
        <source>VK2: Mode 3</source>
        <translation>ВК2: Режим 3</translation>
    </message>
    <message>
        <source>Name Target KM UN</source>
        <translation type="vanished">Устройство КМ</translation>
    </message>
    <message>
        <source>sound switcher</source>
        <translation type="vanished">Переключатель звука</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="5886"/>
        <source>VK1: Test FF Inquiry</source>
        <translation>ВК1: Тест СК</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="5895"/>
        <location filename="mainwindow.ui" line="5898"/>
        <source>VK2: Test FF Inquiry</source>
        <translation>ВК2: Тест СК</translation>
    </message>
    <message>
        <source>Kiparis</source>
        <translation type="vanished">Кипарис</translation>
    </message>
    <message>
        <source>Kiparis</source>
        <comment>Kiparis</comment>
        <extracomment>Kiparis</extracomment>
        <translation type="vanished">Кипарис</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="5670"/>
        <location filename="mainwindow.ui" line="5673"/>
        <source>Pautina 433</source>
        <oldsource>Pautina</oldsource>
        <translation>Паутина 433</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1373"/>
        <source>Choosing map dialogue</source>
        <translation>Диалог выбора подложки карты</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1375"/>
        <source>AllFiles (*.*);;Images (*.jpg *.bmp)</source>
        <translation>Все файлы (*.*);;Изображения (*.jpg *.bmp)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1586"/>
        <source>Closing dialogue</source>
        <translation>Диалог закрытия программы</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1588"/>
        <location filename="mainwindow.cpp" line="2202"/>
        <location filename="mainwindow.cpp" line="5658"/>
        <source>Yes</source>
        <translation>Да</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1589"/>
        <location filename="mainwindow.cpp" line="2203"/>
        <location filename="mainwindow.cpp" line="5659"/>
        <source>No</source>
        <translation>Нет</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1590"/>
        <source>Close program?</source>
        <translation>Закрыть программу?</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1878"/>
        <source>VK%1 </source>
        <translation>ВК%1 </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1881"/>
        <source>S%1 Number%2 </source>
        <translation>Уч%1 Номер%2 </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1889"/>
        <source>AftA1 / </source>
        <translation>ПредТр1 / </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1895"/>
        <source>AftA2 / </source>
        <translation>ПредТр2 / </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1900"/>
        <source>AftA3 / </source>
        <translation>ПредТр3 / </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1905"/>
        <source>A / </source>
        <translation>Тр / </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1910"/>
        <source>PstA / </source>
        <translation>ПостТр / </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1936"/>
        <source>%1[%2] </source>
        <translation>%1[%2] </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1942"/>
        <source>%1 </source>
        <translation>%1 </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2780"/>
        <location filename="mainwindow.cpp" line="3952"/>
        <source>Mode[Day]</source>
        <translation>Режим[День]</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2785"/>
        <location filename="mainwindow.cpp" line="3957"/>
        <source>Mode[Night]</source>
        <translation>Режим[Ночь]</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="5603"/>
        <location filename="mainwindow.cpp" line="5604"/>
        <location filename="mainwindow.cpp" line="5605"/>
        <location filename="mainwindow.cpp" line="5621"/>
        <location filename="mainwindow.cpp" line="5622"/>
        <location filename="mainwindow.cpp" line="5623"/>
        <location filename="mainwindow.cpp" line="5639"/>
        <location filename="mainwindow.cpp" line="5640"/>
        <location filename="mainwindow.cpp" line="5641"/>
        <source>Edit ending</source>
        <translation>Закончить редактирование</translation>
    </message>
    <message>
        <source>%1[%2] S%3 Num%4 </source>
        <translation type="vanished">%1[%2] Уч%3 Ном%4 </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2204"/>
        <location filename="mainwindow.cpp" line="5660"/>
        <source>Build network?</source>
        <translation>Построить сеть?</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2411"/>
        <source>Editor work mode</source>
        <translation>Режим работы редактора</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="5249"/>
        <location filename="mainwindow.cpp" line="5250"/>
        <source>Editor work mode (switch on operator work mode)</source>
        <translation>Режим работы редактора (включить режим работы оператора)</translation>
    </message>
    <message>
        <source>Edit mode</source>
        <translation type="vanished">Режим редактора</translation>
    </message>
    <message>
        <source>Work mode</source>
        <translation type="vanished">Рабочий режим</translation>
    </message>
</context>
<context>
    <name>ProxyMetod</name>
    <message>
        <location filename="src/proxymetod.cpp" line="3998"/>
        <location filename="src/proxymetod.cpp" line="4104"/>
        <source>System</source>
        <translation>Система</translation>
    </message>
    <message>
        <location filename="src/proxymetod.cpp" line="4010"/>
        <source>BSK-AWS</source>
        <oldsource>BSK-CPP</oldsource>
        <translation>БСК-АРМ</translation>
    </message>
    <message>
        <location filename="src/proxymetod.cpp" line="4014"/>
        <location filename="src/proxymetod.cpp" line="4363"/>
        <source>BSK-RT</source>
        <translation>БСК-РТ</translation>
    </message>
    <message>
        <location filename="src/proxymetod.cpp" line="4018"/>
        <location filename="src/proxymetod.cpp" line="4367"/>
        <source>BSK-S</source>
        <translation>БСК-С</translation>
    </message>
    <message>
        <location filename="src/proxymetod.cpp" line="4022"/>
        <location filename="src/proxymetod.cpp" line="4371"/>
        <source>BSK-RLO</source>
        <translation>БСК-РЛО</translation>
    </message>
    <message>
        <location filename="src/proxymetod.cpp" line="4026"/>
        <location filename="src/proxymetod.cpp" line="4375"/>
        <source>BSK-RLD</source>
        <translation>БСК-РЛД</translation>
    </message>
    <message>
        <location filename="src/proxymetod.cpp" line="4030"/>
        <location filename="src/proxymetod.cpp" line="4379"/>
        <source>BSK-RWD</source>
        <translation>БСК-РВД</translation>
    </message>
    <message>
        <location filename="src/proxymetod.cpp" line="4034"/>
        <location filename="src/proxymetod.cpp" line="4383"/>
        <source>BSK-MSO</source>
        <translation>БСК-МСО</translation>
    </message>
    <message>
        <location filename="src/proxymetod.cpp" line="4038"/>
        <location filename="src/proxymetod.cpp" line="4387"/>
        <source>BSK-BVU</source>
        <translation>БСК-БВУ</translation>
    </message>
    <message>
        <location filename="src/proxymetod.cpp" line="4042"/>
        <location filename="src/proxymetod.cpp" line="4391"/>
        <source>BSK-PKP</source>
        <translation>БСК-ПКП</translation>
    </message>
    <message>
        <location filename="src/proxymetod.cpp" line="4046"/>
        <location filename="src/proxymetod.cpp" line="4395"/>
        <source>BSK-VK</source>
        <translation>БСК-ВК</translation>
    </message>
    <message>
        <location filename="src/proxymetod.cpp" line="4050"/>
        <location filename="src/proxymetod.cpp" line="4399"/>
        <source>BSK-O</source>
        <translation>БСК-О</translation>
    </message>
    <message>
        <location filename="src/proxymetod.cpp" line="4054"/>
        <location filename="src/proxymetod.cpp" line="4403"/>
        <source>BSK-IK</source>
        <translation>БСК-ИК</translation>
    </message>
    <message>
        <location filename="src/proxymetod.cpp" line="4058"/>
        <location filename="src/proxymetod.cpp" line="4407"/>
        <source>BSK-RW</source>
        <translation>БСК-РВ</translation>
    </message>
    <message>
        <location filename="src/proxymetod.cpp" line="4062"/>
        <location filename="src/proxymetod.cpp" line="4411"/>
        <source>BSK-RVP</source>
        <translation>БСК-РВП</translation>
    </message>
    <message>
        <location filename="src/proxymetod.cpp" line="4066"/>
        <location filename="src/proxymetod.cpp" line="4415"/>
        <source>BSK-PKSC</source>
        <translation>БСК-ПКСЧ</translation>
    </message>
    <message>
        <location filename="src/proxymetod.cpp" line="4070"/>
        <location filename="src/proxymetod.cpp" line="4419"/>
        <source>BSK-VSO</source>
        <translation>БСК-ВСО</translation>
    </message>
    <message>
        <location filename="src/proxymetod.cpp" line="4078"/>
        <source>AWS</source>
        <translation>АРМ</translation>
    </message>
    <message>
        <location filename="src/proxymetod.cpp" line="4343"/>
        <source>ARM Command</source>
        <translation>Команда с АРМ</translation>
    </message>
    <message>
        <location filename="src/proxymetod.cpp" line="4348"/>
        <source>Other D1</source>
        <translation>Внешнее СО1</translation>
    </message>
    <message>
        <location filename="src/proxymetod.cpp" line="4353"/>
        <source>Other D2</source>
        <translation>Внешнее СО2</translation>
    </message>
    <message>
        <location filename="src/proxymetod.cpp" line="4660"/>
        <source>KK</source>
        <translation>КК</translation>
    </message>
    <message>
        <source>CPP</source>
        <translation type="vanished">ПУЦ</translation>
    </message>
    <message>
        <location filename="src/proxymetod.cpp" line="4083"/>
        <location filename="src/proxymetod.cpp" line="4097"/>
        <source>RT</source>
        <translation>РТ</translation>
    </message>
    <message>
        <location filename="src/proxymetod.cpp" line="4089"/>
        <location filename="src/proxymetod.cpp" line="4101"/>
        <source>VK</source>
        <translation>ВК</translation>
    </message>
    <message>
        <location filename="src/proxymetod.cpp" line="4631"/>
        <source>Unknown</source>
        <translation>Не Определённый</translation>
    </message>
    <message>
        <location filename="src/proxymetod.cpp" line="4635"/>
        <source>BAT</source>
        <translation>БАТ</translation>
    </message>
    <message>
        <location filename="src/proxymetod.cpp" line="4640"/>
        <source>AIP</source>
        <translation>АИП</translation>
    </message>
    <message>
        <location filename="src/proxymetod.cpp" line="4645"/>
        <source>MA</source>
        <translation>МА</translation>
    </message>
    <message>
        <location filename="src/proxymetod.cpp" line="4650"/>
        <source>USB</source>
        <translation>USB</translation>
    </message>
    <message>
        <location filename="src/proxymetod.cpp" line="4655"/>
        <source>BARS</source>
        <translation>БАРС</translation>
    </message>
    <message>
        <location filename="src/proxymetod.cpp" line="4357"/>
        <source>Self D</source>
        <oldsource>Self SO</oldsource>
        <translation>Встроенное СО</translation>
    </message>
    <message>
        <location filename="src/proxymetod.cpp" line="4423"/>
        <source>UN</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="MV/proxysorttreecontainerun.cpp" line="79"/>
        <location filename="MV/tablemodelunitnode.cpp" line="181"/>
        <location filename="MV/treemodelunitnode.cpp" line="143"/>
        <location filename="unitnode.cpp" line="1326"/>
        <source>Device</source>
        <translation>Устройство</translation>
    </message>
    <message>
        <location filename="MV/proxysorttreecontainerun.cpp" line="84"/>
        <location filename="MV/tablemodelunitnode.cpp" line="185"/>
        <location filename="MV/treemodelunitnode.cpp" line="148"/>
        <location filename="unitnode.cpp" line="1330"/>
        <source>S/Num</source>
        <oldsource>S/Number</oldsource>
        <translation>Уч/Ном</translation>
    </message>
    <message>
        <location filename="MV/proxysorttreecontainerun.cpp" line="88"/>
        <location filename="MV/tablemodelunitnode.cpp" line="189"/>
        <location filename="MV/treemodelunitnode.cpp" line="152"/>
        <location filename="unitnode.cpp" line="1334"/>
        <source>U, pow</source>
        <translation>U, пит</translation>
    </message>
    <message>
        <location filename="MV/proxysorttreecontainerun.cpp" line="92"/>
        <location filename="MV/tablemodelunitnode.cpp" line="193"/>
        <location filename="MV/treemodelunitnode.cpp" line="156"/>
        <location filename="unitnode.cpp" line="1338"/>
        <source>ID</source>
        <translation>ID</translation>
    </message>
    <message>
        <location filename="MV/proxysorttreecontainerun.cpp" line="102"/>
        <source>S</source>
        <translation>Уч</translation>
    </message>
    <message>
        <location filename="unitnode.cpp" line="1352"/>
        <source>Unknown</source>
        <translatorcomment>Не Изв. Устр.</translatorcomment>
        <translation>Не Определённый</translation>
    </message>
    <message>
        <location filename="unitnode.cpp" line="1370"/>
        <location filename="unitnode.cpp" line="1384"/>
        <location filename="unitnode.cpp" line="1395"/>
        <location filename="unitnode.cpp" line="1403"/>
        <source>N/A</source>
        <translation>Не Изв</translation>
    </message>
</context>
<context>
    <name>TableModelArchiveIMG</name>
    <message>
        <location filename="MV/tablemodelarchiveimg.cpp" line="242"/>
        <source>From</source>
        <translation>От Кого</translation>
    </message>
    <message>
        <location filename="MV/tablemodelarchiveimg.cpp" line="244"/>
        <source>Date</source>
        <translation>Дата</translation>
    </message>
    <message>
        <location filename="MV/tablemodelarchiveimg.cpp" line="246"/>
        <source>Time</source>
        <translation>Время</translation>
    </message>
    <message>
        <location filename="MV/tablemodelarchiveimg.cpp" line="248"/>
        <source>AfAl1</source>
        <translation>ПредТр1</translation>
    </message>
    <message>
        <location filename="MV/tablemodelarchiveimg.cpp" line="251"/>
        <source>AfAl2</source>
        <translation>ПредТр2</translation>
    </message>
    <message>
        <location filename="MV/tablemodelarchiveimg.cpp" line="254"/>
        <source>AfAl3</source>
        <translation>ПредТр3</translation>
    </message>
    <message>
        <location filename="MV/tablemodelarchiveimg.cpp" line="257"/>
        <source>Al</source>
        <translation>Тр</translation>
    </message>
    <message>
        <source>FF1</source>
        <translation type="vanished">СК1</translation>
    </message>
    <message>
        <source>FF2</source>
        <translation type="vanished">СК2</translation>
    </message>
    <message>
        <source>FF3</source>
        <translation type="vanished">СК3</translation>
    </message>
    <message>
        <location filename="MV/tablemodelarchiveimg.cpp" line="258"/>
        <source>FF4</source>
        <translation>СК4</translation>
    </message>
    <message>
        <location filename="MV/tablemodelarchiveimg.cpp" line="260"/>
        <source>PstAl</source>
        <translation>ПостТр</translation>
    </message>
    <message>
        <location filename="MV/tablemodelarchiveimg.cpp" line="261"/>
        <source>FF5</source>
        <translation>СК5</translation>
    </message>
</context>
<context>
    <name>TableModelArchiveMSG</name>
    <message>
        <location filename="MV/tablemodelarchivemsg.cpp" line="221"/>
        <source>Device</source>
        <translation>Устройство</translation>
    </message>
    <message>
        <source>Area/Number</source>
        <translation type="vanished">Уч/Ном</translation>
    </message>
    <message>
        <location filename="MV/tablemodelarchivemsg.cpp" line="223"/>
        <source>S/Num</source>
        <translation>Уч/Ном</translation>
    </message>
    <message>
        <location filename="MV/tablemodelarchivemsg.cpp" line="227"/>
        <source>Date</source>
        <translation>Дата</translation>
    </message>
    <message>
        <location filename="MV/tablemodelarchivemsg.cpp" line="229"/>
        <source>Time</source>
        <translation>Время</translation>
    </message>
    <message>
        <location filename="MV/tablemodelarchivemsg.cpp" line="231"/>
        <source>Time Corection</source>
        <translation>Коррекция</translation>
    </message>
    <message>
        <location filename="MV/tablemodelarchivemsg.cpp" line="225"/>
        <source>Message</source>
        <translation>Сообщение</translation>
    </message>
</context>
<context>
    <name>UnitNode</name>
    <message>
        <source>ID</source>
        <oldsource>VK</oldsource>
        <translation type="vanished">ID</translation>
    </message>
    <message>
        <source>N/A</source>
        <translation type="vanished">Не Изв</translation>
    </message>
    <message>
        <source>Device</source>
        <translation type="vanished">Устройство</translation>
    </message>
    <message>
        <source>S/Num</source>
        <translation type="vanished">Уч/Ном</translation>
    </message>
    <message>
        <source>U, pow</source>
        <translation type="vanished">U, пит</translation>
    </message>
    <message>
        <source>S/Number</source>
        <translation type="vanished">Уч/Номер</translation>
    </message>
</context>
</TS>
