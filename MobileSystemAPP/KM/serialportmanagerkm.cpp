﻿#include <serialportmanagerkm.h>



SerialPortManagerKM::SerialPortManagerKM(BTMsgManager *bt,
                                         QObject *parent) /*noexcept*/ :
    QObject(parent),
    btManager(bt)
{
    try
    {
        this->countRR = 0;
        this->m_ByteCollector = new QByteArray;
        this->listBuffer = new QList<TypeBuffer*>;
        prevousThreadReader = nullptr;
        prevousThreadAnalysis = nullptr;

        this->m_listPattern = new COSVariantList;
        connect(this->m_listPattern,
                SIGNAL(addedElenent()),
                this,
                SLOT(acceptPattern()));
        this->m_listRequest = new COSVariantList;
        connect(this->m_listRequest,
                SIGNAL(addedElenent()),
                this,
                SLOT(sendRequestMSG()));
        m_port = new QSerialPort;
        connect(m_port,
                SIGNAL(error(QSerialPort::SerialPortError)),
                this,
                SLOT(errorPort(QSerialPort::SerialPortError)));
        connect(m_port,
                SIGNAL(readyRead()),
                this,
                SLOT(process()));

        this->closePort();
    }
    catch (std::runtime_error &rte)
    {
        qWarning("catch runtime_error %s [SerialPortManagerKM::SerialPortManagerKM]", rte.what());
    }
    catch (std::bad_alloc& ba)
    {
        qWarning("catch bad_alloc %s [SerialPortManagerKM::SerialPortManagerKM]", ba.what());
    }
    catch (std::exception &e)
    {
        qWarning("catch exception %s [SerialPortManagerKM::SerialPortManagerKM]", e.what());
    }
    catch (...)
    {
        qWarning("catch exception <unknown> [SerialPortManagerKM::SerialPortManagerKM]");
    }
}

SerialPortManagerKM::~SerialPortManagerKM() /*noexcept*/
{
    try
    {
        this->closePort();

        disconnect(this->m_listPattern,
                SIGNAL(addedElenent()),
                this,
                SLOT(acceptPattern()));
        disconnect(this->m_listRequest,
                SIGNAL(addedElenent()),
                this,
                SLOT(sendRequestMSG()));
        this->m_listPattern->clear();
        this->m_listRequest->clear();


        if(prevousThreadReader)
        {
            prevousThreadReader->terminate();
            prevousThreadReader->wait();
            delete prevousThreadReader;
            prevousThreadReader = nullptr;
        }

        if(prevousThreadAnalysis)
        {
            prevousThreadAnalysis->terminate();
            prevousThreadAnalysis->wait();
            delete prevousThreadAnalysis;
            prevousThreadAnalysis = nullptr;
        }

        delete m_listPattern;
        delete m_listRequest;
        m_listPattern = nullptr;
        m_listRequest = nullptr;

    //    delete m_port;
        delete m_ByteCollector;

        for(int i(0), n(listBuffer->size()); i < n; i++)
        {
            listBuffer->at(i)->btLink.clear();
            listBuffer->at(i)->data.clear();
            listBuffer->at(i)->mainBuffer.clear();
        }
        qDeleteAll(listBuffer->begin(), listBuffer->end());
        listBuffer->clear();
        delete listBuffer;
    }
    catch(...)
    {
#ifdef QT_DEBUG
        qDebug() << "Assert(~SerialPortManagerKM)";
#endif
        return;
    }
}

void SerialPortManagerKM::emitPortCondition() /*noexcept*/
{
    qint32 condition((this->m_port->isOpen() ? ((qint32)1) : ((qint32)-1)));
    emit this->portCondition(condition);

}

//void SerialPortManagerKM::slotSettingsRestoredOnCloseChanged(bool restore) /*noexcept*/
//{
//#ifdef QT_DEBUG
//    qDebug() << " slotSettingsRestoredOnCloseChanged " << restore;
//#endif
//}

void SerialPortManagerKM::errorPort(QSerialPort::SerialPortError error) /*noexcept*/
{
#ifdef QT_DEBUG
    qDebug() << "SerialPortManagerKS::errorPort(" << error << m_port->errorString() << ")";
#endif
    return;
    switch(error)
    {
        case QSerialPort::NoError:
        {
            break;
        };
        case QSerialPort::DeviceNotFoundError:
        {
            break;
        };
        case QSerialPort::PermissionError:
        {
            break;
        };
        case QSerialPort::OpenError:
        {
            break;
        };
        case QSerialPort::NotOpenError:
        {
            break;
        };
        case QSerialPort::ParityError:
        {
            break;
        };
        case QSerialPort::FramingError:
        {
            break;
        };
        case QSerialPort::BreakConditionError:
        {
            break;
        };
        case QSerialPort::WriteError:
        {
            this->reopenPort();
            break;
        };
        case QSerialPort::ReadError:
        {
            this->reopenPort();
            break;
        };
        case QSerialPort::ResourceError:
        {
            this->reopenPort();
            break;
        };
        case QSerialPort::UnsupportedOperationError:
        {
            break;
        };
        case QSerialPort::TimeoutError:
        {
            this->reopenPort();
            break;
        };
        case QSerialPort::UnknownError:
        {
            this->reopenPort();
            break;
        };
        default:
        {
            break;
        };
    }
}


bool SerialPortManagerKM::reopenPort() /*noexcept*/
{
    if(m_portName != this->m_port->portName())
        return false;

    this->closePort();

    this->m_port->setPortName(m_portName);

    this->m_port->open(QSerialPort::ReadWrite);


    if(this->m_port->isOpen())
    {
        this->m_port->setBaudRate(m_rate);
        this->m_port->setDataBits(m_dataBit);
        this->m_port->setParity(m_parity);
        this->m_port->setStopBits(m_stopBit);
        this->m_port->setFlowControl(m_flow);
#ifdef QT_DEBUG
        qDebug() << "";
        qDebug() << "";
        qDebug() << "SerialPort " << m_portName << " isOpen(TRUE)";

        qDebug() << "= Current parameters =";
//        qDebug() << "Device name            : " << m_port->portName();
        qDebug() << "Device name            : " << m_port->portName();
        qDebug() << "Baud rate              : " << m_port->baudRate();
        qDebug() << "Data bits              : " << m_port->dataBits();
        qDebug() << "Parity                 : " << m_port->parity();
        qDebug() << "Stop bits              : " << m_port->stopBits();
        qDebug() << "Flow                   : " << m_port->flowControl();
//        this->m_port->setReadBufferSize(262144);
//        qDebug() << "BufferSize             : " << m_port->readBufferSize();
        qDebug() << "";
        qDebug() << "";
#endif



        this->m_ByteCollector->clear();
        this->emitPortCondition();

        return this->m_stopFlag = true;
    }
    else
    {
#ifdef QT_DEBUG
        qDebug() << "";
        qDebug() << "";
        qDebug() << "";
        qDebug() << "SerialPort " << m_portName << " isOpen(FALSE)";
        qDebug() << "";
#endif
        this->closePort();
        return this->m_stopFlag = false;
    }
}

//    закрытие порта
void SerialPortManagerKM::closePort() /*noexcept*/
{
    try
    {
        this->m_stopFlag = false;

        if(this->m_port)
        {
            if(this->m_port->isOpen())
            {
                this->m_port->close();
            }
            disconnect(m_port,
                    SIGNAL(error(QSerialPort::SerialPortError)),
                    this,
                    SLOT(errorPort(QSerialPort::SerialPortError)));
            disconnect(m_port,
                    SIGNAL(readyRead()),
                    this,
                    SLOT(process()));

            delete this->m_port;
            this->m_port = nullptr;

        }

        this->m_port = new QSerialPort;
        connect(m_port,
                SIGNAL(error(QSerialPort::SerialPortError)),
                this,
                SLOT(errorPort(QSerialPort::SerialPortError)));
        connect(m_port,
                SIGNAL(readyRead()),
                this,
                SLOT(process()));
        this->emitPortCondition();
    }
    catch (std::runtime_error &rte)
    {
        qWarning("catch runtime_error %s [SerialPortManagerKM::closePort]", rte.what());
    }
    catch (std::bad_alloc& ba)
    {
        qWarning("catch bad_alloc %s [SerialPortManagerKM::closePort]", ba.what());
    }
    catch (std::exception &e)
    {
        qWarning("catch exception %s [SerialPortManagerKM::closePort]", e.what());
    }
    catch (...)
    {
        qWarning("catch exception <unknown> [SerialPortManagerKM::closePort]");
    }
}

//    открытие порта
bool SerialPortManagerKM::openPort(QString portName,
                                 qint32 rate,
                                 QSerialPort::DataBits dataBit,
                                 QSerialPort::StopBits stopBit,
                                 QSerialPort::FlowControl flow,
                                 QSerialPort::Parity parity) /*noexcept*/
{    
    this->closePort();

    this->m_port->setPortName(m_portName = portName);

    this->m_port->open(QSerialPort::ReadWrite);


    if(this->m_port->isOpen())
    {
        this->m_port->setBaudRate(m_rate = rate);
        this->m_port->setDataBits(m_dataBit = dataBit);
        this->m_port->setParity(m_parity = parity);
        this->m_port->setStopBits(m_stopBit = stopBit);
        this->m_port->setFlowControl(m_flow = flow);
        this->m_port->setReadBufferSize(262144);

#ifdef QT_DEBUG
        qDebug() << "";
        qDebug() << "";
        qDebug() << "SerialPort " << portName << " isOpen(TRUE)";

        qDebug() << "= Current parameters =";
        qDebug() << "Device name            : " << m_port->portName();
        qDebug() << "Baud rate              : " << m_port->baudRate();
        qDebug() << "Data bits              : " << m_port->dataBits();
        qDebug() << "Parity                 : " << m_port->parity();
        qDebug() << "Stop bits              : " << m_port->stopBits();
        qDebug() << "Flow                   : " << m_port->flowControl();
        qDebug() << "BufferSize             : " << m_port->readBufferSize();
        qDebug() << "";
        qDebug() << "";
#endif



        this->m_ByteCollector->clear();
        this->listBuffer->clear();
        this->emitPortCondition();

        return this->m_stopFlag = true;
    }
    else
    {
#ifdef QT_DEBUG
        qDebug() << "";
        qDebug() << "";
        qDebug() << "";
        qDebug() << "SerialPort " << portName << " isOpen(FALSE)";
        qDebug() << "";
#endif
        this->closePort();
        return this->m_stopFlag = false;
    }


}

//    отправка простого BT сообщения
void SerialPortManagerKM::sendBTMSG() /*noexcept*/
{
    this->sendBTMSG(this->btManager->msgToSend,
                    this->btManager->btLinkMSGToSend);
}


void SerialPortManagerKM::sendBTMSG(QString msg,
                                  QByteArray chanel) /*noexcept*/
{
    this->sendBTMSG(msg.toUtf8(),
                    chanel);
}

void SerialPortManagerKM::sendBTMSG(QByteArray msg,
                                  QByteArray chanel) /*noexcept*/
{
    if(this->m_port->isOpen())
    {
        QByteArray btLink(chanel);
        QByteArray baMSG(msg);
        if(chanel.size())
        {
            while(!chanel.isEmpty())
            {
                quint8 ch(chanel.right(1).at(0));
                chanel.chop(1);

                quint16 sizeMSG(baMSG.size());
                baMSG.prepend((quint8)(0x00FF & sizeMSG));
                baMSG.prepend((quint8)((0xFF00 & sizeMSG) >> 8));
                baMSG.prepend(ch);
                baMSG.prepend((quint8)0xBF);
                baMSG.append(ch ^ 0xFF);
            }
        }
#ifdef QT_DEBUG
        if(0xA5 == (quint8)msg.at(0))
        {
            qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " [" << btLink.toHex() << "] " << " out>> " << msg.toHex() << endl << baMSG.toHex();
        }
        else
        {
            qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " [" << btLink.toHex() << "] " << " out>> " << msg << endl << baMSG.toHex();
        }
#endif

        this->m_port->write(baMSG);

//        while(baMSG.size())
//        {
//            this->m_port->write(baMSG.left(500));
//            baMSG.remove(0, 500);
//            if(baMSG.size())
//            {
//                this->msleep(80);
//            }
//        }
    }
    return;
}

void SerialPortManagerKM::needRun() /*noexcept*/
{
    countRR++;
}

//void SerialPortManagerKM::run() /*noexcept*/
//{
//    while(0 < countRR)
//    {
//        this->process();
//        countRR--;
//    }
//}

//    основной поток
void SerialPortManagerKM::process() /*noexcept*/
{
    try
    {
        if(!this->m_stopFlag)
        {
            return;
        }

        QByteArray newData(this->m_port->readAll());

        ThreadReaderKM *newThreadReader(new ThreadReaderKM(this->listBuffer,
                                                           this->m_ByteCollector,
                                                           newData,
                                                           m_listPattern,
                                                           m_listRequest/*,
                                                           this*/));
        if(prevousThreadReader)
        {
            connect(newThreadReader,
                    SIGNAL(started()),
                    prevousThreadReader,
                    SLOT(deleteLater()));
            connect(prevousThreadReader,
                    SIGNAL(finished()),
                    newThreadReader,
                    SLOT(start()));
            if(prevousThreadReader->isFinished())
            {
                newThreadReader->start();
            }
            prevousThreadReader = newThreadReader;
        }
        else
        {
            newThreadReader->start();
            prevousThreadReader = newThreadReader;
        }
    }
    catch (std::runtime_error &rte)
    {
        qWarning("catch runtime_error %s [SerialPortManagerKM::process]", rte.what());
    }
    catch (std::bad_alloc& ba)
    {
        qWarning("catch bad_alloc %s [SerialPortManagerKM::process]", ba.what());
    }
    catch (std::exception &e)
    {
        qWarning("catch exception %s [SerialPortManagerKM::process]", e.what());
    }
    catch (...)
    {
        qWarning("catch exception <unknown> [SerialPortManagerKM::process]");
    }

    return;
}

void SerialPortManagerKM::sendRequestMSG() /*noexcept*/
{
    if(this->m_port->isOpen())
    {
//        while(!m_listRequest->m_list.isEmpty())
        while(!m_listRequest->isEmpty())
        {
//            QByteArray msg(m_listRequest->m_list.first().toByteArray());
//            m_listRequest->m_list.removeFirst();
            QByteArray btLink(m_listRequest->first().toList().at(1).toByteArray());
            QByteArray baMSG(m_listRequest->first().toList().at(0).toByteArray());
            if(btLink.size())
            {
                while(!btLink.isEmpty())
                {
                    quint8 ch(btLink.right(1).at(0));
                    btLink.chop(1);

                    quint16 sizeMSG(baMSG.size());
                    baMSG.prepend((quint8)(0x00FF & sizeMSG));
                    baMSG.prepend((quint8)((0xFF00 & sizeMSG) >> 8));
                    baMSG.prepend(ch);
                    baMSG.prepend((quint8)0xBF);
                    baMSG.append(ch ^ 0xFF);
                }
            }
            m_listRequest->removeFirst();
            if(baMSG.isEmpty())
                continue;

            this->m_port->write(baMSG);
        }
    }
}

bool SerialPortManagerKM::refreshBuffer() /*noexcept*/
{
    //
    emit this->portCondition(0);
    //
    bool result(false);
    this->m_stopFlag = false;
    result = this->openPort(m_portName,
                            m_rate,
                            m_dataBit,
                            m_stopBit,
                            m_flow,
                            m_parity);

    this->m_ByteCollector->clear();
    this->m_stopFlag = true;
    return result;
}

void SerialPortManagerKM::acceptPattern() /*noexcept*/
{
    //
    try
    {
        while(!m_listPattern->isEmpty())
        {
            QByteArray link(m_listPattern->first().toList().at(1).toByteArray());
            QByteArray pattern(m_listPattern->first().toList().at(0).toByteArray());
            m_listPattern->removeFirst();

            if(pattern.isEmpty())
                continue;

            ThreadAnalysisKM *newThreadAnalysis(new ThreadAnalysisKM(btManager,
                                                                     link,
                                                                     pattern/*,
                                                                     this*/));

            if(prevousThreadAnalysis)
            {
                connect(newThreadAnalysis,
                        SIGNAL(started()),
                        prevousThreadAnalysis,
                        SLOT(deleteLater()));
                connect(prevousThreadAnalysis,
                        SIGNAL(finished()),
                        newThreadAnalysis,
                        SLOT(start()));
                if(prevousThreadAnalysis->isFinished())
                {
                    newThreadAnalysis->start();
                }
                prevousThreadAnalysis = newThreadAnalysis;
            }
            else
            {
                newThreadAnalysis->start();
                prevousThreadAnalysis = newThreadAnalysis;
            }
        }
    }
    catch (std::runtime_error &rte)
    {
        qWarning("catch runtime_error %s [SerialPortManagerKM::acceptPattern]", rte.what());
    }
    catch (std::bad_alloc& ba)
    {
        qWarning("catch bad_alloc %s [SerialPortManagerKM::acceptPattern]", ba.what());
    }
    catch (std::exception &e)
    {
        qWarning("catch exception %s [SerialPortManagerKM::acceptPattern]", e.what());
    }
    catch (...)
    {
        qWarning("catch exception <unknown> [SerialPortManagerKM::acceptPattern]");
    }
    return;
    //
}
//void SerialPortManagerKM::clearListThreadAnalysis() /*noexcept*/
//{
//    //удаление
//    for(int i(0); i < this->listThreadAnalysis.size();)
//    {
//        if(this->listThreadAnalysis.at(i)->isFinished() &&
//           !this->listThreadAnalysis.at(i)->isRunning())
//        {
////            this->listThreadAnalysis.at(i)->killSelf();
////            this->listThreadAnalysis.at(i)->terminate();
////            this->listThreadAnalysis.at(i)->wait();
//            delete this->listThreadAnalysis.at(i);
//            this->listThreadAnalysis.removeAt(i);
//        }
//        else
//        {
//            i++;
//        }
//    }

//    //запуск
//    for(int i(0), n(this->listThreadAnalysis.size()); i < n; i++)
//    {
//        if(!this->listThreadAnalysis.at(i)->isFinished() &&
//           !this->listThreadAnalysis.at(i)->isRunning())
//        {
//            this->listThreadAnalysis.at(i)->start();
//            break;
//        }
//        else
//        {
//            if(this->listThreadAnalysis.at(i)->isRunning())
//            {
//                break;
//            }
//        }
//    }

//    return;
////    qDebug() << "CA() la(" << this->listThreadAnalysis.size() << ") lr(" << this->listThreadReader.size() << ")";

//}

//void SerialPortManagerKM::clearListThreadReader() /*noexcept*/
//{
//    //удаление
//    for(int i(0); i < this->listThreadReader.size();)
//    {
//        if(this->listThreadReader.at(i)->isFinished() &&
//           !this->listThreadReader.at(i)->isRunning())
//        {
////            this->listThreadReader.at(i)->killSelf();
////            this->listThreadReader.at(i)->terminate();
////            this->listThreadReader.at(i)->wait();
////            delete this->listThreadReader.at(i);
//            this->listThreadReader.removeAt(i);
//        }
//        else
//        {
//            i++;
//        }
//    }

//    //запуск
//    for(int i(0), n(this->listThreadReader.size()); i < n; i++)
//    {
//        if(!this->listThreadReader.at(i)->isFinished() &&
//           !this->listThreadReader.at(i)->isRunning())
//        {
//            this->listThreadReader.at(i)->start();
//            break;
//        }
//        else
//        {
//            if(this->listThreadReader.at(i)->isRunning())
//            {
//                break;
//            }
//        }
//    }

//    return;
////    qDebug() << "CR() la(" << this->listThreadAnalysis.size() << ") lr(" << this->listThreadReader.size() << ")";
//}

QString SerialPortManagerKM::portName() /*noexcept*/
{
    return m_port->portName();
}

qint32 SerialPortManagerKM::baudRate() /*noexcept*/
{
    return m_port->baudRate();
}

qint32 SerialPortManagerKM::dataBits() /*noexcept*/
{
    return m_port->dataBits();
}

qint32 SerialPortManagerKM::parity() /*noexcept*/
{
    return m_port->parity();
}

qint32 SerialPortManagerKM::stopBits() /*noexcept*/
{
    return m_port->stopBits();
}

bool SerialPortManagerKM::isOpen() /*noexcept*/
{
    return m_port->isOpen();
}
