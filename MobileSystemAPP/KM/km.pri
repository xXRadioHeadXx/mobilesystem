QT += core serialport
CONFIG += c++11

INCLUDEPATH += $$PWD/

HEADERS += \
    $$PWD/serialportmanagerkm.h \
    $$PWD/kamuflagemanager.h \
    $$PWD/btmsgmanager.h \
    $$PWD/threadanalysiskm.h \
    $$PWD/threadreaderkm.h 

SOURCES += \
    $$PWD/serialportmanagerkm.cpp \
    $$PWD/kamuflagemanager.cpp \
    $$PWD/btmsgmanager.cpp \
    $$PWD/threadanalysiskm.cpp \
    $$PWD/threadreaderkm.cpp 
