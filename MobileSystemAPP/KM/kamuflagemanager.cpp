﻿#include <kamuflagemanager.h>

KamuflageManager::KamuflageManager(DataBaseManager *db,
                                   QObject *parent) /*noexcept*/ :
    QObject(parent),
    dbManager(db)
{
    try
    {
        this->m_btManager = new BTMsgManager(dbManager,
                                             this);
        this->m_portManager = new SerialPortManagerKM(m_btManager,
                                                      this);

        this->targetVK = nullptr;

        this->repeatB4 =
        this->refreshPortIteration = 0;
        timerPostponedNeedSendB3 = new QTimer(this);
        timerFirstPostponedNeedSendB3 = new QTimer(this);
        timerPostponedNeedSendB4 = new QTimer(this);
        timerFirstPostponedNeedSendB4 = new QTimer(this);

        connect(&this->timerRequestB4,
                SIGNAL(timeout()),
                this,
                SLOT(timeoutB4()));


        connect(&this->timerEndC3,
                SIGNAL(timeout()),
                this,
                SLOT(tmeoutEndC3()));

        connect(this->m_btManager,
                SIGNAL(currentWorkModeChanged(int)),
                this,
                SIGNAL(currentWorkModeChanged(int)));

        connect(this->m_btManager,
                SIGNAL(startWaitEndC3(int)),
                this,
                SLOT(ssStartWaitEndC3(int)));

        connect(this->m_btManager,
                SIGNAL(stopWaitEndC3()),
                this,
                SLOT(ssStopWaitEndC3()));

    //    connect(this->m_portManager,
    //            SIGNAL(newPattern(QByteArray,QByteArray)),
    //            this,
    //            SLOT(analisePattern(QByteArray,QByteArray)));

        connect(this->m_portManager,
                SIGNAL(portCondition(qint32)),
                this,
                SIGNAL(portCondition(qint32)));

    //    connect(this->m_portManager,
    //            SIGNAL(newBTMSG(QByteArray,QByteArray)),
    //            this->m_btManager,
    //            SLOT(newBTMSG(QByteArray,QByteArray)));

    //    connect(this->m_portManager,
    //            SIGNAL(newKMMSG(QByteArray,QByteArray)),
    //            this->m_btManager,
    //            SLOT(newKmMSG(QByteArray,QByteArray)));

        connect(this->m_btManager,
                SIGNAL(newMSGToSend()),
                this->m_portManager,
                SLOT(sendBTMSG()));

        connect(this->m_btManager,
                SIGNAL(insertNewMSG(quint32)),
                this,
                SIGNAL(insertNewMSG(quint32)));

    //    connect(this->m_btManager,
    //            SIGNAL(insertNewIMG(quint32)),
    //            this,
    //            SIGNAL(insertNewIMG(quint32)));

    //    connect(this->m_btManager,
    //            SIGNAL(message(UnitNode*,quint8,QByteArray,bool)),
    //            this,
    //            SIGNAL(unitMSG(UnitNode*,quint8,QByteArray,bool)));

    //    connect(this->m_btManager,
    //            SIGNAL(message(UnitNode*,quint8,QByteArray,bool)),
    //            this,
    //            SLOT(controlConnect(UnitNode*,quint8,QByteArray,bool)));

        connect(&this->timerPortCheck,
                SIGNAL(timeout()),
                this,
                SLOT(timeoutControlConnect()));

        connect(&this->timerWaitAnswer,
                SIGNAL(timeout()),
                this,
                SLOT(timeoutWaitAnswer()));

    //    connect(this->m_portManager->m_port,
    //            SIGNAL(readyRead()),
    //            this,
    //            SLOT(startReadThread()));

        connect(this->m_btManager,
                SIGNAL(newMSGToSend(QByteArray,QByteArray)),
                this->m_portManager,
                SLOT(sendBTMSG(QByteArray,QByteArray)));

        connect(this->m_btManager,
                SIGNAL(ssPostponedCommandCALL(int)),
                this,
                SLOT(ssPostponedCommandCALL(int)));

        connect(this->m_btManager,
                SIGNAL(ssPostponedCommandKILL(int)),
                this,
                SLOT(ssPostponedCommandKILL(int)));

        connect(this->m_btManager,
                SIGNAL(ssPostponedCommandLIST(int)),
                this,
                SLOT(ssPostponedCommandLIST(int)));

        connect(this->m_btManager,
                SIGNAL(ssPostponedCommandINQUIRY(int)),
                this,
                SLOT(ssPostponedCommandINQUIRY(int)));

        connect(this->m_btManager,
                SIGNAL(ssPostponedCommandRSSI(int)),
                this,
                SLOT(ssPostponedCommandRSSI(int)));

        connect(this->m_btManager,
                SIGNAL(ssPostponedCommandTXPOWER(int)),
                this,
                SLOT(ssPostponedCommandTXPOWER(int)));

        connect(this->m_btManager,
                SIGNAL(ssDisconnectOfListConnect(int)),
                this,
                SLOT(ssDisconnectOfListConnect(int)));

        connect(this->m_btManager,
                SIGNAL(ssKillConnection(int)),
                this,
                SLOT(ssKillConnection(int)));

        connect(&this->timerRequestB1,
                SIGNAL(timeout()),
                this,
                SLOT(catchTimeoutB1()));

        connect(&this->timerRequestC0,
                SIGNAL(timeout()),
                this,
                SLOT(catchTimeoutC0()));

        connect(this->m_btManager,
                SIGNAL(ssPostponedB1(int)),
                this,
                SLOT(ssPostponedB1(int)));

        connect(this->m_btManager,
                SIGNAL(ssPostponedCommandC0Start(int, UnitNode*)),
                this,
                SLOT(ssPostponedCommandC0Start(int, UnitNode*)));

        connect(this->m_btManager,
                SIGNAL(ssPostponedCommandC0Stop()),
                this,
                SLOT(ssPostponedCommandC0Stop()));

        connect(this->m_btManager,
                SIGNAL(ssPostponedCommandB1Start(int, UnitNode*)),
                this,
                SLOT(ssPostponedCommandB1Start(int, UnitNode*)));

        connect(this->m_btManager,
                SIGNAL(ssPostponedCommandB1Stop()),
                this,
                SLOT(ssPostponedCommandB1Stop()));

        connect(&this->timerRequestB3,
                SIGNAL(timeout()),
                this,
                SLOT(timeoutB3()));

    //    connect(&this->timerRequestB4,
    //            SIGNAL(timeout()),
    //            this,
    //            SLOT(catchTimeoutB4()));

        connect(this->m_btManager,
                SIGNAL(ssPostponedB3(int)),
                this,
                SLOT(ssPostponedB3(int)));

    //    connect(this->m_btManager,
    //            SIGNAL(waitEndC3(int)),
    //            this,
    //            SLOT(waitEndC3(int)));

    //    connect(this->m_btManager,
    //            SIGNAL(ssPostponedB4(int)),
    //            this,
    //            SLOT(ssPostponedB4(int)));

        connect(this->m_btManager,
                SIGNAL(ssReInterrogateBT(int)),
                this,
                SLOT(ssReInterrogateBT(int)));

        connect(this->m_btManager,
                SIGNAL(ssPostponedCommandB3Start(int, UnitNode*)),
                this,
                SLOT(ssPostponedCommandB3Start(int, UnitNode*)));

        connect(this->m_btManager,
                SIGNAL(ssPostponedCommandB4Start(int, UnitNode*)),
                this,
                SLOT(ssPostponedCommandB4Start(int, UnitNode*)));

        connect(this->m_btManager,
                SIGNAL(ssPostponedCommandB3Stop()),
                this,
                SLOT(ssPostponedCommandB3Stop()));

        connect(this->m_btManager,
                SIGNAL(ssPostponedCommandB4Stop()),
                this,
                SLOT(ssPostponedCommandB4Stop()));

        connect(this->m_btManager,
                SIGNAL(addNewUN(UnitNode*)),
                this,
                SIGNAL(addNewUN(UnitNode*)));

        connect(this->m_btManager,
                SIGNAL(updateSettingUN(UnitNode*,QByteArray)),
                this,
                SIGNAL(updateSettingUN(UnitNode*,QByteArray)));

    //    connect(this->m_btManager,
    //            SIGNAL(addNewUN(UnitNode*)),
    //            this,
    //            SLOT(saveUNListToXML()));

        connect(this->m_btManager,
                SIGNAL(updateUN(UnitNode*)),
                this,
                SIGNAL(updateUN(UnitNode*)));

        connect(this->m_btManager,
                SIGNAL(moveUN(UnitNode*,
                              UnitNode*,
                              int,
                              UnitNode*,
                              int)),
                this,
                SIGNAL(moveUN(UnitNode*,
                              UnitNode*,
                              int,
                              UnitNode*,
                              int)));

    //    connect(this->m_btManager,
    //            SIGNAL(updateUN(UnitNode*)),
    //            this,
    //            SLOT(saveUNListToXML()));

        connect(this->m_btManager,
                SIGNAL(connectUN(UnitNode*,
                                 UnitNode*)),
                this,
                SIGNAL(connectUN(UnitNode*,
                                 UnitNode*)));

        connect(this->m_btManager,
                SIGNAL(disconnectUN(UnitNode*,
                                    UnitNode*)),
                this,
                SIGNAL(disconnectUN(UnitNode*,
                                    UnitNode*)));

        connect(this->m_btManager,
                SIGNAL(ssOffStatusKMExchange(int)),
                this,
                SLOT(ssOffStatusKMExchange(int)));

        connect(this->m_btManager,
                SIGNAL(ssPostponedNeedSendB3(UnitNode*,int)),
                this,
                SLOT(postponedNeedSendB3(UnitNode*,int)));

    //    connect(this->m_btManager,
    //            SIGNAL(ssPostponedNeedSendB4(UnitNode*,int)),
    //            this,
    //            SLOT(postponedNeedSendB4(UnitNode*,int)));

    //    connect()

        connect(this->m_btManager,
                SIGNAL(updateProgressShotLoad(UnitNode*,quint8,quint8,quint8)),
                this,
                SIGNAL(updateProgressShotLoad(UnitNode*,quint8,quint8,quint8)));

    //    connect(this->m_btManager,
    //            SIGNAL(setAlarmCondition(UnitNode*)),
    //            this,
    //            SLOT(setAlarmCondition(UnitNode*)));

    //    this->m_portManager->inst(m_btManager);

    //    connect(this->m_portManager,
    //            SIGNAL(needRead()),
    //            this,
    //            SLOT(needRead()));
    }
    catch (std::runtime_error &rte)
    {
        qWarning("catch runtime_error %s [KamuflageManager::KamuflageManager]", rte.what());
    }
    catch (std::bad_alloc& ba)
    {
        qWarning("catch bad_alloc %s [KamuflageManager::KamuflageManager]", ba.what());
    }
    catch (std::exception &e)
    {
        qWarning("catch exception %s [KamuflageManager::KamuflageManager]", e.what());
    }
    catch (...)
    {
        qWarning("catch exception <unknown> [KamuflageManager::KamuflageManager]");
    }
}

KamuflageManager::~KamuflageManager() /*noexcept*/
{
    try
    {
        this->m_portManager->m_stopFlag = false;
        delete timerPostponedNeedSendB3;
        delete timerPostponedNeedSendB4;
        delete timerFirstPostponedNeedSendB4;
        delete timerFirstPostponedNeedSendB3;
        while(!listTimerOffAlarmCondition.isEmpty())
        {
            listTimerOffAlarmCondition.first()->stop();
            delete listTimerOffAlarmCondition.first();
            listTimerOffAlarmCondition.removeFirst();
        }

        targetVK =
        unPowerCheckM =
        unPowerCheckS = nullptr;

        delete m_portManager;
        delete m_btManager;
    }
    catch(...)
    {
#ifdef QT_DEBUG
        qDebug() << "Assert(~KamuflageManager)";
#endif
        return;
    }
}

void KamuflageManager::needRead() /*noexcept*/
{
//    this->m_portManager->needRun();
//    if(!this->m_portManager->isRunning())
//    {
//        qDebug() << "KamuflageManager::needRead( start " << this->m_portManager->countRR << ")";
//        this->m_portManager->start();
//    }
//    else
//    {
//        qDebug() << "KamuflageManager::needRead(" << this->m_portManager->countRR << ")";
//    }
}

void KamuflageManager::ssPostponedCommandCALL(int ms) /*noexcept*/
{
    this->timerPostponedCommandCALL.stop();
    this->timerPostponedCommandCALL.singleShot(ms,
                                               this->m_btManager,
                                               SLOT(postponedCommandCALL()));
}

void KamuflageManager::ssPostponedCommandKILL(int ms) /*noexcept*/
{
    this->timerPostponedCommandKILL.stop();
    this->timerPostponedCommandKILL.singleShot(ms,
                                               this->m_btManager,
                                               SLOT(postponedCommandKILL()));
}

void KamuflageManager::ssPostponedCommandLIST(int ms) /*noexcept*/
{
    this->timerPostponedCommandLIST.stop();
    this->timerPostponedCommandLIST.singleShot(ms,
                                               this->m_btManager,
                                               SLOT(postponedCommandLIST()));
}

void KamuflageManager::ssPostponedCommandINQUIRY(int ms) /*noexcept*/
{
    this->timerPostponedCommandINQUIRY.stop();
    this->timerPostponedCommandINQUIRY.singleShot(ms,
                                                  this->m_btManager,
                                                  SLOT(postponedCommandINQUIRY()));
}

void KamuflageManager::ssPostponedCommandTXPOWER(int ms) /*noexcept*/
{
    this->timerPostponedCommandTXPOWER.stop();
    this->timerPostponedCommandTXPOWER.singleShot(ms,
                                                  this->m_btManager,
                                                  SLOT(postponedCommandTXPOWER()));
}
void KamuflageManager::ssPostponedCommandRSSI(int ms) /*noexcept*/
{
    this->timerPostponedCommandRSSI.stop();
    this->timerPostponedCommandRSSI.singleShot(ms,
                                               this->m_btManager,
                                               SLOT(postponedCommandRSSI()));
}
void KamuflageManager::ssDisconnectOfListConnect(int ms) /*noexcept*/
{
    this->timerDisconnectOfListConnect.stop();
    this->timerDisconnectOfListConnect.singleShot(ms,
                                                  this->m_btManager,
                                                  SLOT(disconnectOfListConnect()));
}
void KamuflageManager::ssKillConnection(int ms) /*noexcept*/
{
    this->timerKillConnection.stop();
    this->timerKillConnection.singleShot(ms,
                                         this->m_btManager,
                                         SLOT(killConnection()));
}

void KamuflageManager::postponedNeedSendB3(UnitNode *unSender,
                                             int timeout) /*noexcept*/
{
    try
    {
        delete timerPostponedNeedSendB3;
        delete timerFirstPostponedNeedSendB3;

        timerPostponedNeedSendB3 = new QTimer(this);
        timerFirstPostponedNeedSendB3 = new QTimer(this);

        connect(timerPostponedNeedSendB3,
                SIGNAL(timeout()),
                unSender,
                SLOT(emitB3()));
        timerPostponedNeedSendB3->start(timeout);

        connect(timerFirstPostponedNeedSendB3,
                SIGNAL(timeout()),
                unSender,
                SLOT(emitB3()));
        timerFirstPostponedNeedSendB3->start(300);
    }
    catch (std::runtime_error &rte)
    {
        qWarning("catch runtime_error %s [KamuflageManager::postponedNeedSendB3]", rte.what());
    }
    catch (std::bad_alloc& ba)
    {
        qWarning("catch bad_alloc %s [KamuflageManager::postponedNeedSendB3]", ba.what());
    }
    catch (std::exception &e)
    {
        qWarning("catch exception %s [KamuflageManager::postponedNeedSendB3]", e.what());
    }
    catch (...)
    {
        qWarning("catch exception <unknown> [KamuflageManager::postponedNeedSendB3]");
    }
}

void KamuflageManager::ssPostponedCommandB3Start(int timeout,
                                                 UnitNode* unSender) /*noexcept*/
{
    if(this->timerRequestB4.isActive() ||
       this->timerEndC3.isActive())
        return;
//    qDebug() << "ssPostponedCommandB3Start(" << timeout << ", " << unSender->name << ")";

    this->ssPostponedCommandB1Stop();
    this->ssPostponedCommandB3Stop();
    this->ssPostponedCommandB4Stop();

    this->m_btManager->setTopVK(unSender);

    this->timeoutB3();

    this->timerRequestB3.start(timeout);
}

void KamuflageManager::ssPostponedCommandB3Stop() /*noexcept*/
{
//    qDebug() << "ssPostponedCommandB3Stop";

    while(this->timerRequestB3.isActive())
        this->timerRequestB3.stop();
}

void KamuflageManager::ssPostponedCommandB4Start(int timeout,
                                                 UnitNode* unSender) /*noexcept*/
{
    if(nullptr == unSender)
        return;
//    qDebug() << "ssPostponedCommandB4Start(" << timeout << ", " << unSender->name << ")";
    this->repeatB4 = 0;

    while(this->timerRequestB4.isActive())
        this->timerRequestB4.stop();

    this->timeoutB4();

    while(!this->timerRequestB4.isActive())
        this->timerRequestB4.start(timeout);
}

void KamuflageManager::ssPostponedCommandB4Stop() /*noexcept*/
{
//    qDebug() << "ssPostponedCommandB4Stop";
    while(this->timerRequestB4.isActive())
        this->timerRequestB4.stop();
}

void KamuflageManager::ssPostponedB3(int ms) /*noexcept*/
{
//    catchTimeoutB3();
    this->timerPostponedB3.stop();
    this->timerPostponedB3.singleShot(ms,
                                      this,
                                      SLOT(catchTimeoutB3()));
}


void KamuflageManager::catchTimeoutB3() /*noexcept*/
{
//    if(5 < this->iterationB3)
//    {
        this->ssPostponedCommandB3Stop();
//    }
//    this->iterationB3++;
    this->targetVK->emitB3();
//    qDebug() << "!!! TimeoutCatch(1) !!!";
}

void KamuflageManager::ssPostponedCommandC0Start(int timeout,
                                                 UnitNode* unSender) /*noexcept*/
{
    this->iterationC0 = 0;
    if(this->timerRequestC0.isActive())
    {
        this->timerRequestC0.stop();
    }
//    disconnect(&this->timerRequestC0,
//               0,
//               0,
//               0);
    this->targetVK = unSender;
    this->timerRequestC0.start(timeout + 300);
}


void KamuflageManager::ssPostponedCommandB1Start(int timeout,
                                                 UnitNode* unSender) /*noexcept*/
{
//    this->iterationB1 = 0;
    if(this->timerRequestB1.isActive())
    {
        this->timerRequestB1.stop();
    }
    disconnect(&this->timerRequestB1,
               0,
               0,
               0);
    this->targetVK = unSender;
    this->timerRequestB1.start(timeout + 300);
}

void KamuflageManager::ssPostponedCommandB1Stop() /*noexcept*/
{
    this->timerRequestB1.stop();
    disconnect(&this->timerRequestB1,
               0,
               0,
               0);
}

void KamuflageManager::ssPostponedCommandC0Stop() /*noexcept*/
{
    this->timerRequestC0.stop();
//    disconnect(&this->timerRequestC0,
//               0,
//               0,
//               0);
}

void KamuflageManager::ssPostponedB1(int ms) /*noexcept*/
{
    this->timerPostponedB3.stop();
    this->timerPostponedB3.singleShot(ms,
                                      this->targetVK,
                                      SLOT(emitB1()));
}

void KamuflageManager::catchTimeoutC0() /*noexcept*/
{
    if(5 > this->iterationC0)
    {
        this->m_btManager->requestC0(this->targetVK);
//        this->targetVK->emitC0();

    }
    if(5 == this->iterationC0)
    {
        this->iterationC0 = 0;
        this->ssPostponedCommandC0Stop();
        this->targetVK->emitB1();
        return;//
        this->targetVK->emitB1_VK1();
    }
    if(5 < this->iterationC0)
    {
        this->iterationC0 = 0;
        this->ssPostponedCommandC0Stop();
    }
    this->iterationC0++;
}


void KamuflageManager::catchTimeoutB1() /*noexcept*/
{
//    if(5 < this->iterationB1)
//    {
//    }
//    this->iterationB1++;
    this->ssPostponedCommandB1Stop();
    this->targetVK->emitB1();
    return;//
    this->targetVK->emitB1_VK1();
}

void KamuflageManager::ssPostponedA1(int ms) /*noexcept*/
{
    this->timerPostponedA1.stop();
    this->timerPostponedA1.singleShot(ms,
                                      this->m_btManager,
                                      SLOT(postponedRequestA1()));
}

//void KamuflageManager::ssPostponedF1(int ms) /*noexcept*/
//{

//}

void KamuflageManager::ssReInterrogateBT(int ms) /*noexcept*/
{
    this->timerReInterrogateBT.stop();
    this->timerReInterrogateBT.singleShot(ms,
                                          this->m_btManager,
                                          SLOT(reInterrogateBT()));
}

void KamuflageManager::ssOffStatusKMExchange(int timeout) /*noexcept*/
{
    this->m_btManager->statusKMExchange = true;
    if(this->timerOffStatusKMExchange.isActive())
    {
        this->timerOffStatusKMExchange.stop();
    }
    this->timerOffStatusKMExchange.singleShot(timeout,
                                              this->m_btManager,
                                              SLOT(offStatusKMExchange()));
}

void KamuflageManager::closePort() /*noexcept*/
{
    while(this->timerPortCheck.isActive())
        this->timerPortCheck.stop();

    while(this->timerWaitAnswer.isActive())
        this->timerWaitAnswer.stop();

    this->m_portManager->closePort();

    emit this->updatePortSetting(this->getPortSetting());

    this->emitInsertNewMSG(0x00,
                           0xFF,
                           QString("Port closed succeeded").toUtf8(),
                           true,
                           System_TypeGroupMSG);
}

bool KamuflageManager::openPort(QString portName,
                                 qint32 rate,
                                 QSerialPort::DataBits dataBits,
                                 QSerialPort::StopBits stopBits,
                                 QSerialPort::FlowControl flow,
                                 QSerialPort::Parity parity) /*noexcept*/
{
//    this->m_portManager->inst(this->m_btManager);
    if(this->m_portManager->openPort(portName,
                                    rate,
                                    dataBits,
                                    stopBits,
                                    flow,
                                    parity))
    {
        emitInsertNewMSG(0x00,
                         0xFF,
                         QString("Port " + portName + " opening succeeded").toUtf8(),
                         true,
                         System_TypeGroupMSG);
        this->m_portManager->btManager = this->m_btManager;
//        this->m_portManager->start();
        this->m_btManager->requestSET();
        emit this->updatePortSetting(this->getPortSetting());
        return true;
    }

    emitInsertNewMSG(0x00,
                     0xFF,
                     QString("Port " + portName + " opening failed").toUtf8(),
                     true,
                     System_TypeGroupMSG);
    emit this->updatePortSetting(QList<QString>());
    return false;
}

QList<QString> KamuflageManager::getPortSetting() /*noexcept*/
{
    QList<QString> listSetting;

    if(this->m_portManager->m_port)
    {
        if(this->m_portManager->isOpen())
        {
            listSetting.append(this->m_portManager->portName());
            QString strBaud("%1");
            strBaud = strBaud.arg(this->m_portManager->baudRate());
            listSetting.append(strBaud);
            QString strData("%1");
            strData = strData.arg(this->m_portManager->dataBits());
            listSetting.append(strData);
            listSetting.append((0 != this->m_portManager->parity()) ? "Y" : "N");
            QString strStop("%1");
            strStop = strStop.arg(this->m_portManager->stopBits());
            listSetting.append(strStop);
            return listSetting;
        }
    }


    return listSetting;
}

void KamuflageManager::sendMSG(UnitNode* unSender,
                               quint8 type,
                               QByteArray msg) /*noexcept*/
{
    if(unSender)
    {
        if(0xFF == type)
        {
            QByteArray adr(unSender->btLink);
            adr.append(0xFF);
            this->sendMSG(adr,
                          msg);
        }
        else
        {
            this->sendMSG(unSender->btLink,
                          msg);
        }
    }
}
void KamuflageManager::sendMSG(QByteArray adress,
                               QByteArray msg) /*noexcept*/
{
    this->m_portManager->sendBTMSG(msg,
                                  adress);
}

void KamuflageManager::oneBTInquiryToUN(UnitNode* unSender) /*noexcept*/
{
    if(!unSender)
        return;
    this->timerReInterrogateBT.stop();
    this->m_btManager->setCurrentWorkMode(DutyMode);
    this->m_btManager->statusKMExchange = false;
    this->m_btManager->listCurrentInterrogatedBT.clear();
    this->m_btManager->listInterrogatedBT.clear();
    this->m_btManager->oneReInterrogateBT(unSender);
}

void KamuflageManager::oneBTListToUN(UnitNode* unSender) /*noexcept*/
{
    this->timerReInterrogateBT.stop();
    this->m_btManager->setCurrentWorkMode(DutyMode);
    this->m_btManager->statusKMExchange = false;
    this->m_btManager->listCurrentInterrogatedBT.clear();
    this->m_btManager->listInterrogatedBT.clear();
    this->m_btManager->lastListM = unSender;
    this->m_btManager->requestLIST(unSender);
}

void KamuflageManager::oneBTSetToUN(UnitNode* unSender) /*noexcept*/
{
    this->timerReInterrogateBT.stop();
    this->m_btManager->setCurrentWorkMode(DutyMode);
    this->m_btManager->statusKMExchange = false;
    this->m_btManager->listCurrentInterrogatedBT.clear();
    this->m_btManager->listInterrogatedBT.clear();
    this->m_btManager->requestSET(unSender);
}

void KamuflageManager::oneToD5Kill(UnitNode* unMaster,
                                   UnitNode* unSlave) /*noexcept*/
{
    this->timerReInterrogateBT.stop();
    this->m_btManager->setCurrentWorkMode(DutyMode);
    this->m_btManager->statusKMExchange = false;
    this->m_btManager->listCurrentInterrogatedBT.clear();
    this->m_btManager->listInterrogatedBT.clear();
    this->m_btManager->requestD5(unSlave);
    this->m_btManager->requestKILL(unMaster,
                                   unSlave);
}

void KamuflageManager::oneBTKillToUN(UnitNode* unMaster,
                                     UnitNode* unSlave) /*noexcept*/
{
    this->timerReInterrogateBT.stop();
    this->m_btManager->setCurrentWorkMode(DutyMode);
    this->m_btManager->statusKMExchange = false;
    this->m_btManager->listCurrentInterrogatedBT.clear();
    this->m_btManager->listInterrogatedBT.clear();

    this->m_btManager->lastKillM = unMaster;
    this->m_btManager->lastKillS = unSlave;
    this->m_btManager->requestKILL(unMaster,
                                   unSlave);
}

void KamuflageManager::allBTInquiryToUN() /*noexcept*/
{
    this->timerReInterrogateBT.stop();
    this->m_btManager->setCurrentWorkMode(DutyMode);
    this->m_btManager->statusKMExchange = false;
    this->m_btManager->reInterrogateBT();
}

void KamuflageManager::cmdRequest_0xA1(UnitNode* unSender) /*noexcept*/
{
    this->m_btManager->targetA1 = unSender;
    this->ssPostponedA1(_dt_);
}

void KamuflageManager::cmdRequest_0xB1(UnitNode *unSender) /*noexcept*/
{
    this->ssPostponedCommandB1Stop();
    this->ssPostponedCommandB3Stop();
    this->m_btManager->requestShot(unSender);
//    unSender->needSendB1(15000);
}

void KamuflageManager::cmdRequest_0x2F(UnitNode* unSender) /*noexcept*/
{
    this->m_btManager->request2F(unSender);
}

void KamuflageManager::cmdRequest_0x1F(UnitNode *unSender,
                                            quint8 mode,
                                            quint8 t1,
                                            quint8 t2,
                                            quint8 t3,
                                            quint8 selfSO,
                                            quint8 BSK,
                                            quint8 idBSK1h,
                                            quint8 idBSK1l,
                                            quint8 idBSK2h,
                                            quint8 idBSK2l,
                                            quint8 idBSK3h,
                                            quint8 idBSK3l,
                                            quint8 outSO,
                                            quint8 AndOr,
                                            quint8 controllPKP) /*noexcept*/
{
    this->m_btManager->request1F(unSender,
                                mode,
                                t1,
                                t2,
                                t3,
                                selfSO,
                                BSK,
                                idBSK1h,
                                idBSK1l,
                                idBSK2h,
                                idBSK2l,
                                idBSK3h,
                                idBSK3l,
                                outSO,
                                AndOr,
                                controllPKP);
}

void KamuflageManager::cmdRequest_0xDE(UnitNode* unSender,
                                                quint16 blockSZ,
                                                quint16 waitSZ) /*noexcept*/
{
    this->m_btManager->requestDE(unSender,
                                blockSZ,
                                waitSZ);
}

void KamuflageManager::organiseNetworkTopology() /*noexcept*/
{
    this->m_btManager->buildNewTopology();
}

QList<UnitNode*> KamuflageManager::getListUnitNode() /*noexcept*/
{
    return this->m_btManager->listBTUnitNode;
}

QList<BTMsgManager::CommunicationItem> KamuflageManager::getlistCommunicationItem() /*noexcept*/
{
    return this->m_btManager->listCommunicationItem;
}


QList<QList<UnitNode*>> KamuflageManager::getListShortWayToVK() /*noexcept*/
{
    return this->m_btManager->listShortWayToVK;
}

QList<QList<UnitNode*>> KamuflageManager::getListCurrentUsedWayToVK() /*noexcept*/
{
    return this->m_btManager->listCurrentUsedWayToVK;
}

void KamuflageManager::oneBTTxpowerToUN() /*noexcept*/
{
    this->m_btManager->requestTXPOWER(this->unPowerCheckM,
                                     this->unPowerCheckS->btLink.right(1));
}

void KamuflageManager::oneBTTxpowerToUN(UnitNode* unMaster,
                                        UnitNode* unSlave) /*noexcept*/
{
    this->unPowerCheckM = unMaster;
    this->unPowerCheckS = unSlave;
    this->oneBTTxpowerToUN();
}

void KamuflageManager::startReadThread() /*noexcept*/
{
}

void KamuflageManager::repeatMSG() /*noexcept*/
{
    this->m_btManager->repealLastMSG();
}

bool KamuflageManager::refreshPort() /*noexcept*/
{
    emit this->updatePortSetting(QList<QString>());

//    this->m_portManager->inst(this->m_btManager);
    if(this->m_portManager->refreshBuffer())
    {
        emitInsertNewMSG(0x00,
                         0xFF,
                         "Port opening succeeded",
                         true,
                         System_TypeGroupMSG);
        emit this->updatePortSetting(this->getPortSetting());
        return true;
    }
    else
    {
        emitInsertNewMSG(0x00,
                         0xFF,
                         "Port opening failed",
                         true,
                         System_TypeGroupMSG);
        emit this->updatePortSetting(this->getPortSetting());
        return false;
    }
}

void KamuflageManager::timeoutB4() /*noexcept*/
{
    this->repeatB4++;
    if(10 == this->repeatB4)
    {
        this->refreshPort();
    }
    if(15 < this->repeatB4)
    {
        this->ssPostponedCommandB4Stop();
    }

    for(int i(0), n(this->m_btManager->listBTUnitNode.size()); i < n; i++)
    {
        if(this->m_btManager->listBTUnitNode.at(i)->flag_connection &&
           VK_KMDeviceType == this->m_btManager->listBTUnitNode.at(i)->_typeUN() &&
           this->m_btManager->listBTUnitNode.at(i)->listIMGVK.size())
        {
            this->m_btManager->listBTUnitNode.at(i)->emitB4();
            break;
        }
    }
//    qDebug() << "KamuflageManager::timeoutB4()";
}

void KamuflageManager::timeoutB3() /*noexcept*/
{
    this->ssPostponedCommandB3Stop();

    for(int i(0), n(this->m_btManager->listBTUnitNode.size()); i < n; i++)
    {
        if(this->m_btManager->listBTUnitNode.at(i)->flag_connection &&
           VK_KMDeviceType == this->m_btManager->listBTUnitNode.at(i)->_typeUN() &&
           this->m_btManager->listBTUnitNode.at(i)->listIMGVK.size())
        {
//            this->m_btManager->listUnitNode.at(i)->listIMG[0].waitStatus = true;
            this->m_btManager->listBTUnitNode.at(i)->emitB3();
//            qDebug() << "KamuflageManager::timeoutB3()";
            return;
        }
    }

}

void KamuflageManager::ssStartWaitEndC3(int timeout) /*noexcept*/
{
    while(this->timerEndC3.isActive())
        this->timerEndC3.stop();

    this->timerEndC3.start(timeout);
//    qDebug() << "KamuflageManager::ssStartWaitEndC3()";
}

void KamuflageManager::ssStopWaitEndC3() /*noexcept*/
{
    while(this->timerEndC3.isActive())
        this->timerEndC3.stop();
//    qDebug() << "KamuflageManager::ssStopWaitEndC3()";
}

void KamuflageManager::tmeoutEndC3() /*noexcept*/
{
    this->ssPostponedCommandB3Stop();
    this->ssPostponedCommandB4Stop();

    for(int i(0), n(this->m_btManager->listBTUnitNode.size()); i < n; i++)
    {
        if(this->m_btManager->listBTUnitNode.at(i)->flag_connection &&
           VK_KMDeviceType == this->m_btManager->listBTUnitNode.at(i)->_typeUN() &&
           this->m_btManager->listBTUnitNode.at(i)->listIMGVK.size())
        {
            this->ssStopWaitEndC3();
            this->repeatB4 = 0;
            this->m_btManager->listBTUnitNode.at(i)->listIMGVK.first().doneStatus = false;
            this->ssPostponedCommandB4Start(_dtB4_,
                                            this->m_btManager->listBTUnitNode.at(i));
//            qDebug() << "KamuflageManager::tmeoutEndC3()";
            return;
        }
    }
}

//void KamuflageManager::saveUNListToXML() /*noexcept*/
//{
//    QList<UnitNode*> listUN(this->getListUnitNode());
//    if(listUN.size())
//    {
//        QDomDocument doc("ListUnitNode");
//        QDomElement  domElement = doc.createElement("listUnitNode");
//        doc.appendChild(domElement);
//        for(int i(0), n(listUN.size()); i < n; i++)
//        {
//            domElement.appendChild(this->unDomElement(doc, i + 1, listUN.at(i)));
//        }
//        QFile xmlFile("unitnodelist.xml");
//        if(xmlFile.open(QIODevice::WriteOnly))
//        {
//            QTextStream(&xmlFile) << doc.toString();
//            xmlFile.close();
//        }
//    }
//}

//QDomElement KamuflageManager::unDomElement(QDomDocument domDoc,
//                                           int nNumber,
//                                           UnitNode *un) /*noexcept*/
//{
//    QDomElement domElement = makeElement(domDoc,
//                                         "UnitNode",
//                                         QString().setNum(nNumber));

//    domElement.appendChild(makeElement(domDoc, "id", "", un->id.toHex()));
//    domElement.appendChild(makeElement(domDoc, "mac", "", un->mac));
//    domElement.appendChild(makeElement(domDoc, "name", "", un->name));
//    domElement.appendChild(makeElement(domDoc, "type", "", QString("%1").arg(un->type, 0, 10)));
//    domElement.appendChild(makeElement(domDoc, "addressValid", "", QString("%1").arg(un->btLinkValid, 0, 10)));
//    domElement.appendChild(makeElement(domDoc, "address", "", un->btLink.isEmpty() ? "NULL" : un->btLink.toHex()));

//    return domElement;
//}

//QDomElement KamuflageManager::makeElement(QDomDocument domDoc,
//                                          QString strName,
//                                          QString strAttr,
//                                          QString strText) /*noexcept*/
//{
//    QDomElement domElement = domDoc.createElement(strName);

//    if(!strAttr.isEmpty())
//    {
//        QDomAttr domAttr = domDoc.createAttribute("number");
//        domAttr.setValue(strAttr);
//        domElement.setAttributeNode(domAttr);
//    }

//    if(!strText.isEmpty())
//    {
//        QDomText domText = domDoc.createTextNode(strText);
//        domElement.appendChild(domText);
//    }

//    return domElement;
//}

//void KamuflageManager::loadTopologyUN() /*noexcept*/
//{
//    this->m_btManager->loadUNListFromXML();
//    emit this->updateUN(0);
//}

void KamuflageManager::controlConnect(UnitNode* unSender,
                                      quint8 type,
                                      QByteArray msg,
                                      bool ioType) /*noexcept*/
{
    if(!ioType)
        return;

    while(this->timerPortCheck.isActive())
        this->timerPortCheck.stop();

    while(this->timerWaitAnswer.isActive())
        this->timerWaitAnswer.stop();

    this->timerPortCheck.start(15000);
    unSender->flag_connection;
    type = 0;
    msg.clear();
}

void KamuflageManager::timeoutControlConnect() /*noexcept*/
{
    while(this->timerWaitAnswer.isActive())
        this->timerWaitAnswer.stop();

    this->m_btManager->requestAT();

    this->timerWaitAnswer.start(2000);
}

void KamuflageManager::timeoutWaitAnswer() /*noexcept*/
{
    while(this->timerPortCheck.isActive())
        this->timerPortCheck.stop();

    while(this->timerWaitAnswer.isActive())
        this->timerWaitAnswer.stop();

    UnitNode *baseUN(this->m_btManager->possGetUnitNode(""));
//    if(baseUN)
//    {
//        this->m_btManager->requestD5(baseUN);
//    }

    if(this->refreshPort()) //gam
//    if(true)
    {
        this->refreshPortIteration = 0;
    }
    else
    {
        this->refreshPortIteration++;
    }

    if(refreshPortIteration < 3)
    {
        this->timeoutControlConnect();
    }
    else
    {
        this->refreshPortIteration = 0;

//        UnitNode *baseUN(this->m_btManager->possGetUnitNode(""));
        if(baseUN)
        {
            baseUN->updateFlagConnection(false);
            this->m_btManager->extractionFromStructure(baseUN);
        }
    }
}

//void KamuflageManager::setAlarmCondition(UnitNode *un) /*noexcept*/
//{
//    QTimer *timerOffAlarmCondition(new QTimer(this));

//    connect(timerOffAlarmCondition,
//            SIGNAL(timeout()),
//            un,
//            SLOT(offAlarmCondition()));

//    connect(timerOffAlarmCondition,
//            SIGNAL(timeout()),
//            timerOffAlarmCondition,
//            SLOT(stop()));

//    connect(timerOffAlarmCondition,
//            SIGNAL(timeout()),
//            this,
//            SLOT(offAlarmCondition()));

//    this->listTimerOffAlarmCondition.append(timerOffAlarmCondition);

//    un->setAlarmCondition();

//    timerOffAlarmCondition->start(3000);

//}

//void KamuflageManager::offAlarmCondition() /*noexcept*/
//{
//    for(int i(0); i < this->listTimerOffAlarmCondition.size();)
//    {
//        if(this->listTimerOffAlarmCondition.at(i)->isActive())
//        {
//            i++;
//        }
//        else
//        {
//            delete this->listTimerOffAlarmCondition.at(i);
//            this->listTimerOffAlarmCondition.removeAt(i);
//        }
//    }
//}

quint32 KamuflageManager::emitInsertNewMSG(UnitNode *unSender,
                                           quint8 codeMSG,
                                           QByteArray msg,
                                           bool ioType,
                                           TypeGroupMSG typeGroup,
                                           bool flagHidden) /*noexcept*/
{
    quint32 indexDBMsg(0);
    indexDBMsg = this->dbManager->addNewMSG(unSender,
                                            codeMSG,
                                            msg,
                                            ioType,
                                            KMSystemType,
                                            typeGroup,
                                            flagHidden);
//    emit this->insertNewMSG(indexDBMsg);
    return indexDBMsg;
}

//quint32 KamuflageManager::emitInsertNewMSG(quint32 indexDBMsg) /*noexcept*/
//{
//    emit this->insertNewMSG(indexDBMsg);
//    return indexDBMsg;
//}

