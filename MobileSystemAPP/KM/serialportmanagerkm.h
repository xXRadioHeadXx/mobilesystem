﻿#ifndef SERIALPORTMANAGERKM_H
#define waitTime 50
#define SERIALPORTMANAGERKM_H

#include <QThread>
#include <QSerialPort>
#include <btmsgmanager.h>
#include <threadanalysiskm.h>
#include <threadreaderkm.h>

class SerialPortManagerKM : public QObject
{
    Q_OBJECT
public:

    ThreadReaderKM *prevousThreadReader;
    ThreadAnalysisKM *prevousThreadAnalysis;
    COSVariantList *m_listPattern;
    COSVariantList *m_listRequest;
    QString m_portName;
    qint32 m_rate;
    QSerialPort::DataBits m_dataBit;
    QSerialPort::StopBits m_stopBit;
    QSerialPort::FlowControl m_flow;
    QSerialPort::Parity m_parity;


//    QFile fileTempBuffer;
//    QList<ThreadAnalysisKM*> listThreadAnalysis;
//    QList<ThreadReaderKM*> listThreadReader;
    quint8 countRR;
//    QMutex *portResurseMutex;
    QSerialPort *m_port;// порт
    bool m_stopFlag;//главный флаг остановки
    QByteArray *m_ByteCollector;
    BTMsgManager *btManager;
//    QTimer *timer_stopAnalyseThread,
//           *timer_stopReadThread;

    QList<TypeBuffer*> *listBuffer;

    explicit SerialPortManagerKM(BTMsgManager *bt,
                                 QObject *parent = nullptr) /*noexcept*/;

//    void inst(BTMsgManager *bt) /*noexcept*/;
    virtual ~SerialPortManagerKM() /*noexcept*/;


//    начальная инициализация
//    void inst() /*noexcept*/;
//    получение порта в польдование
    bool tryReceivePort(int iter = 1000) /*noexcept*/;
//    освобождение порта
    void tryReleasePort() /*noexcept*/;

//    открытие порта
    bool openPort(QString portName = "COM1",
                  qint32 rate = 576000,//QSerialPort::BaudRate576000,
                  QSerialPort::DataBits dataBit = QSerialPort::Data8,
                  QSerialPort::StopBits stopBit = QSerialPort::OneStop,
                  QSerialPort::FlowControl flow = QSerialPort::NoFlowControl,
                  QSerialPort::Parity parity = QSerialPort::NoParity) /*noexcept*/;
    bool reopenPort() /*noexcept*/;
//    отправка простого BT сообщения
    void sendBTMSG(QString msg,
                   QByteArray chanel) /*noexcept*/;

    void needRun() /*noexcept*/;

signals:
//    void needRead();
//    void protocolMessage(quint8 type,
//                         QByteArray adress,
//                         QByteArray msg);
////    void readedCount(int size);
//    void message(UnitNode *unSender,
//                 quint8 type,
//                 QByteArray msg);
//    void newKMMSG(QByteArray btLink,
//                  QByteArray pattern);
//    void newBTMSG(QByteArray btLink,
//                  QByteArray pattern);
//    void newPattern(QByteArray btLink,
//                    QByteArray pattern);
    void portCondition(qint32 condition);

protected slots:
//    void run();

public slots:
    void process() /*noexcept*/;
    void sendRequestMSG() /*noexcept*/;
    void sendBTMSG() /*noexcept*/;
    void sendBTMSG(QByteArray msg,
                   QByteArray chanel) /*noexcept*/;

//    void slotDataErrorPolicyChanged(QSerialPort::DataErrorPolicy policy);
//    void slotError(QSerialPort::SerialPortError error);
    bool refreshBuffer() /*noexcept*/;
//    void slotEvent(QString status,
//                   QDateTime currentDT);
//    void slotSettingsRestoredOnCloseChanged(bool restore);
//    void analisePattern(QByteArray btLink,
//                        QByteArray pattern) /*noexcept*/;
    void acceptPattern() /*noexcept*/;
//    void clearListThreadAnalysis();
//    void clearListThreadReader();
//    void removeFinishedThreadAnalysis() /*noexcept*/;
//    void removeFinishedThreadReader() /*noexcept*/;

    //    закрытие порта
    void closePort() /*noexcept*/;
    void emitPortCondition() /*noexcept*/;
    void errorPort(QSerialPort::SerialPortError error) /*noexcept*/;


    QString portName() /*noexcept*/;
    qint32 baudRate() /*noexcept*/;
    qint32 dataBits() /*noexcept*/;
    qint32 parity() /*noexcept*/;
    qint32 stopBits() /*noexcept*/;
    bool isOpen() /*noexcept*/;
};


#endif // SERIALPORTMANAGERKM_H
