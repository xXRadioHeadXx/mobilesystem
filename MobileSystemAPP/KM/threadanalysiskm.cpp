﻿#include <threadanalysiskm.h>

//ThreadAnalysisKM::ThreadAnalysisKM(QObject *parent) /*noexcept*/ :
//    QThread(parent)
//{
//}

ThreadAnalysisKM::ThreadAnalysisKM(BTMsgManager *bt,
                                   QByteArray link,
                                   QByteArray patt,
                                   QObject *parent) /*noexcept*/ :
    QThread(parent),
    btManager(bt),
    m_btLink(link),
    pattern(patt)
{
}

ThreadAnalysisKM::~ThreadAnalysisKM() /*noexcept*/
{
    try
    {
        btManager = nullptr;
        m_btLink.clear();
        pattern.clear();
    }
    catch(...)
    {
#ifdef QT_DEBUG
        qDebug() << "Assert(~ThreadAnalysisKM)";
#endif
        return;
    }
}

void ThreadAnalysisKM::run() /*noexcept*/
{
    if(0xA5 == (quint8)this->pattern.at(0))
    {
        this->btManager->newKmMSG(m_btLink,
                                  pattern);
    }
    else
    {
        this->btManager->newBTMSG(m_btLink,
                                  pattern);
    }
    this->quit();
    return;
}
