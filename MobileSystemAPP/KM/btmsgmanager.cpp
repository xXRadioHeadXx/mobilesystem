﻿#include <btmsgmanager.h>

BTMsgManager::BTMsgManager(DataBaseManager *db,
                           QObject *parent) /*noexcept*/ :
    QObject(parent),
    dbManager(db)
{
    CrcTable.append(0x00);
    CrcTable.append(0x31);
    CrcTable.append(0x62);
    CrcTable.append(0x53);
    CrcTable.append(0xC4);
    CrcTable.append(0xF5);
    CrcTable.append(0xA6);
    CrcTable.append(0x97);

    CrcTable.append(0xB9);
    CrcTable.append(0x88);
    CrcTable.append(0xDB);
    CrcTable.append(0xEA);
    CrcTable.append(0x7D);
    CrcTable.append(0x4C);
    CrcTable.append(0x1F);
    CrcTable.append(0x2E);

    CrcTable.append(0x43);
    CrcTable.append(0x72);
    CrcTable.append(0x21);
    CrcTable.append(0x10);
    CrcTable.append(0x87);
    CrcTable.append(0xB6);
    CrcTable.append(0xE5);
    CrcTable.append(0xD4);

    CrcTable.append(0xFA);
    CrcTable.append(0xCB);
    CrcTable.append(0x98);
    CrcTable.append(0xA9);
    CrcTable.append(0x3E);
    CrcTable.append(0x0F);
    CrcTable.append(0x5C);
    CrcTable.append(0x6D);

    CrcTable.append(0x86);
    CrcTable.append(0xB7);
    CrcTable.append(0xE4);
    CrcTable.append(0xD5);
    CrcTable.append(0x42);
    CrcTable.append(0x73);
    CrcTable.append(0x20);
    CrcTable.append(0x11);

    CrcTable.append(0x3F);
    CrcTable.append(0x0E);
    CrcTable.append(0x5D);
    CrcTable.append(0x6C);
    CrcTable.append(0xFB);
    CrcTable.append(0xCA);
    CrcTable.append(0x99);
    CrcTable.append(0xA8);

    CrcTable.append(0xC5);
    CrcTable.append(0xF4);
    CrcTable.append(0xA7);
    CrcTable.append(0x96);
    CrcTable.append(0x01);
    CrcTable.append(0x30);
    CrcTable.append(0x63);
    CrcTable.append(0x52);

    CrcTable.append(0x7C);
    CrcTable.append(0x4D);
    CrcTable.append(0x1E);
    CrcTable.append(0x2F);
    CrcTable.append(0xB8);
    CrcTable.append(0x89);
    CrcTable.append(0xDA);
    CrcTable.append(0xEB);

    CrcTable.append(0x3D);
    CrcTable.append(0x0C);
    CrcTable.append(0x5F);
    CrcTable.append(0x6E);
    CrcTable.append(0xF9);
    CrcTable.append(0xC8);
    CrcTable.append(0x9B);
    CrcTable.append(0xAA);

    CrcTable.append(0x84);
    CrcTable.append(0xB5);
    CrcTable.append(0xE6);
    CrcTable.append(0xD7);
    CrcTable.append(0x40);
    CrcTable.append(0x71);
    CrcTable.append(0x22);
    CrcTable.append(0x13);

    CrcTable.append(0x7E);
    CrcTable.append(0x4F);
    CrcTable.append(0x1C);
    CrcTable.append(0x2D);
    CrcTable.append(0xBA);
    CrcTable.append(0x8B);
    CrcTable.append(0xD8);
    CrcTable.append(0xE9);

    CrcTable.append(0xC7);
    CrcTable.append(0xF6);
    CrcTable.append(0xA5);
    CrcTable.append(0x94);
    CrcTable.append(0x03);
    CrcTable.append(0x32);
    CrcTable.append(0x61);
    CrcTable.append(0x50);

    CrcTable.append(0xBB);
    CrcTable.append(0x8A);
    CrcTable.append(0xD9);
    CrcTable.append(0xE8);
    CrcTable.append(0x7F);
    CrcTable.append(0x4E);
    CrcTable.append(0x1D);
    CrcTable.append(0x2C);

    CrcTable.append(0x02);
    CrcTable.append(0x33);
    CrcTable.append(0x60);
    CrcTable.append(0x51);
    CrcTable.append(0xC6);
    CrcTable.append(0xF7);
    CrcTable.append(0xA4);
    CrcTable.append(0x95);

    CrcTable.append(0xF8);
    CrcTable.append(0xC9);
    CrcTable.append(0x9A);
    CrcTable.append(0xAB);
    CrcTable.append(0x3C);
    CrcTable.append(0x0D);
    CrcTable.append(0x5E);
    CrcTable.append(0x6F);

    CrcTable.append(0x41);
    CrcTable.append(0x70);
    CrcTable.append(0x23);
    CrcTable.append(0x12);
    CrcTable.append(0x85);
    CrcTable.append(0xB4);
    CrcTable.append(0xE7);
    CrcTable.append(0xD6);

    CrcTable.append(0x7A);
    CrcTable.append(0x4B);
    CrcTable.append(0x18);
    CrcTable.append(0x29);
    CrcTable.append(0xBE);
    CrcTable.append(0x8F);
    CrcTable.append(0xDC);
    CrcTable.append(0xED);

    CrcTable.append(0xC3);
    CrcTable.append(0xF2);
    CrcTable.append(0xA1);
    CrcTable.append(0x90);
    CrcTable.append(0x07);
    CrcTable.append(0x36);
    CrcTable.append(0x65);
    CrcTable.append(0x54);

    CrcTable.append(0x39);
    CrcTable.append(0x08);
    CrcTable.append(0x5B);
    CrcTable.append(0x6A);
    CrcTable.append(0xFD);
    CrcTable.append(0xCC);
    CrcTable.append(0x9F);
    CrcTable.append(0xAE);

    CrcTable.append(0x80);
    CrcTable.append(0xB1);
    CrcTable.append(0xE2);
    CrcTable.append(0xD3);
    CrcTable.append(0x44);
    CrcTable.append(0x75);
    CrcTable.append(0x26);
    CrcTable.append(0x17);

    CrcTable.append(0xFC);
    CrcTable.append(0xCD);
    CrcTable.append(0x9E);
    CrcTable.append(0xAF);
    CrcTable.append(0x38);
    CrcTable.append(0x09);
    CrcTable.append(0x5A);
    CrcTable.append(0x6B);

    CrcTable.append(0x45);
    CrcTable.append(0x74);
    CrcTable.append(0x27);
    CrcTable.append(0x16);
    CrcTable.append(0x81);
    CrcTable.append(0xB0);
    CrcTable.append(0xE3);
    CrcTable.append(0xD2);

    CrcTable.append(0xBF);
    CrcTable.append(0x8E);
    CrcTable.append(0xDD);
    CrcTable.append(0xEC);
    CrcTable.append(0x7B);
    CrcTable.append(0x4A);
    CrcTable.append(0x19);
    CrcTable.append(0x28);

    CrcTable.append(0x06);
    CrcTable.append(0x37);
    CrcTable.append(0x64);
    CrcTable.append(0x55);
    CrcTable.append(0xC2);
    CrcTable.append(0xF3);
    CrcTable.append(0xA0);
    CrcTable.append(0x91);

    CrcTable.append(0x47);
    CrcTable.append(0x76);
    CrcTable.append(0x25);
    CrcTable.append(0x14);
    CrcTable.append(0x83);
    CrcTable.append(0xB2);
    CrcTable.append(0xE1);
    CrcTable.append(0xD0);

    CrcTable.append(0xFE);
    CrcTable.append(0xCF);
    CrcTable.append(0x9C);
    CrcTable.append(0xAD);
    CrcTable.append(0x3A);
    CrcTable.append(0x0B);
    CrcTable.append(0x58);
    CrcTable.append(0x69);

    CrcTable.append(0x04);
    CrcTable.append(0x35);
    CrcTable.append(0x66);
    CrcTable.append(0x57);
    CrcTable.append(0xC0);
    CrcTable.append(0xF1);
    CrcTable.append(0xA2);
    CrcTable.append(0x93);

    CrcTable.append(0xBD);
    CrcTable.append(0x8C);
    CrcTable.append(0xDF);
    CrcTable.append(0xEE);
    CrcTable.append(0x79);
    CrcTable.append(0x48);
    CrcTable.append(0x1B);
    CrcTable.append(0x2A);

    CrcTable.append(0xC1);
    CrcTable.append(0xF0);
    CrcTable.append(0xA3);
    CrcTable.append(0x92);
    CrcTable.append(0x05);
    CrcTable.append(0x34);
    CrcTable.append(0x67);
    CrcTable.append(0x56);

    CrcTable.append(0x78);
    CrcTable.append(0x49);
    CrcTable.append(0x1A);
    CrcTable.append(0x2B);
    CrcTable.append(0xBC);
    CrcTable.append(0x8D);
    CrcTable.append(0xDE);
    CrcTable.append(0xEF);

    CrcTable.append(0x82);
    CrcTable.append(0xB3);
    CrcTable.append(0xE0);
    CrcTable.append(0xD1);
    CrcTable.append(0x46);
    CrcTable.append(0x77);
    CrcTable.append(0x24);
    CrcTable.append(0x15);

    CrcTable.append(0x3B);
    CrcTable.append(0x0A);
    CrcTable.append(0x59);
    CrcTable.append(0x68);
    CrcTable.append(0xFF);
    CrcTable.append(0xCE);
    CrcTable.append(0x9D);
    CrcTable.append(0xAC);

    this->defBS = 0;
    this->countRepeatMSG = 0;
    this->currentCheckedSubsetOfCommunication.clear();
    this->currentSubset.clear();
    this->setCurrentWorkMode(Undefined);
    this->MaxCountBlock = 1;
    this->reInterrogateStatus = true;
    this->statusKMExchange = false;
    try
    {
        this->m_treeRootUN = new UnitNode;
        this->m_treeRootUN->updateType(KMSystemType, UnknownDeviceType);
        this->beeper = new SoundAlarm(this);
    }
    catch (std::runtime_error &rte)
    {
        qWarning("catch runtime_error %s [BTMsgManager::BTMsgManager]", rte.what());
    }
    catch (std::bad_alloc& ba)
    {
        qWarning("catch bad_alloc %s [BTMsgManager::BTMsgManager]", ba.what());
    }
    catch (std::exception &e)
    {
        qWarning("catch exception %s [BTMsgManager::BTMsgManager]", e.what());
    }
    catch (...)
    {
        qWarning("catch exception <unknown> [BTMsgManager::BTMsgManager]");
    }


//    this->setInitialListUN();

//    connect(this,
//            SIGNAL(updateUN(UnitNode*)),
//            dbManager,
//            SLOT(uploadUNToDB(UnitNode*)));
}

//BTMsgManager::BTMsgManager(QObject *parent) /*noexcept*/ :
//    QObject(parent)
//{
//    this->defBS = 0;
//    this->countRepeatMSG = 0;
//    this->currentCheckedSubsetOfCommunication.clear();
//    this->currentSubset.clear();
//    this->setCurrentWorkMode(Undefined);
//    this->MaxCountBlock = 1;
//    this->statusKMExchange = false;
//    this->beeper = new SoundAlarm(this);
//}

BTMsgManager::~BTMsgManager() /*noexcept*/
{
    try
    {
        dbManager = 0;
        delete m_treeRootUN;
        this->beeper->terminate();
        delete this->beeper;
        this->beeper = 0;

        listCurrentInitialSubset.clear();
        listInitialSubset.clear();
        listShortWayToVK.clear();
        listCurrentUsedWayToVK.clear();
        listInitialWayToVK.clear();
        listSpareWayToVK.clear();
        currentCheckedSubsetOfCommunication.clear();
        currentSubset.clear();
        currentWayVK.clear();
        currentCheckedWayVK.clear();
        listInterrogatedBT.clear();
        listCurrentInterrogatedBT.clear();
        listDefinitionSyperfloursBT.clear();
        listCallBT.clear();
        listSuperfluous.clear();
        listDisconnect.clear();
        macTargetKM =
        targetA1 =
        targetF1 =
        definitionInitiator =
        definitionChannel =
        lastCallM =
        lastCallS =
        lastKillM =
        lastKillS =
        lastListM =
        lastInquiryM =
        lastTxpowerM =
        lastRssiM = nullptr;

        qDeleteAll(listBTUnitNode.begin(), listBTUnitNode.end());
        listBTUnitNode.clear();
    }
    catch(...)
    {
#ifdef QT_DEBUG
        qDebug() << "Assert(~BTMsgManager)";
#endif
        return;
    }
}

void BTMsgManager::setInitialListUN() /*noexcept*/
{
//    QList<quint32> listUNIndex(dbManager->getAllUNRecords());
    auto listUNIndex(dbManager->getAllUNRecords());
    if(listUNIndex.isEmpty())
    {
        return;
    }
    while(!listUNIndex.isEmpty())
    {
        try
        {
            QSqlRecord unRecord(dbManager->getUNRecord(listUNIndex.first()));
            listUNIndex.removeFirst();
            if(unRecord.isEmpty())
                return;

            if(KMSystemType != (TypeUnitNodeSystem)unRecord.value("typeSystemUN").toUInt())
            {
                continue;
            }

            UnitNode *newUN(new UnitNode(this));
            newUN->id = unRecord.value("idUN").toByteArray();
            newUN->mac = unRecord.value("macUN").toByteArray();
            newUN->name = unRecord.value("nameUN").toByteArray();
            newUN->abstractName = unRecord.value("abstractNameUN").toByteArray();
            newUN->typeUN = (TypeUnitNodeDevice)unRecord.value("typeUN").toUInt();
            newUN->typeSystem = (TypeUnitNodeSystem)unRecord.value("typeSystemUN").toUInt();
            newUN->btLink = unRecord.value("link").toByteArray();
            newUN->flag_connection = unRecord.value("linkValid").toBool();
            newUN->graphicsPxmItem->setX(unRecord.value("xPos").toInt());
            newUN->graphicsPxmItem->setY(unRecord.value("yPos").toInt());
            newUN->voltage = unRecord.value("voltage").toUInt();
            newUN->numArea = unRecord.value("area").toUInt();
            newUN->numNode = unRecord.value("number").toUInt();
            newUN->flag_discharge = unRecord.value("discharge").toBool();
            newUN->dbIndex = unRecord.value("id").toUInt();
            newUN->dbIndexParent = unRecord.value("idParentUN").toUInt();
    //        newUN->graphicsTxtItem->setX(unRecord.value("xPos").toInt());
    //        newUN->graphicsTxtItem->setY(unRecord.value("yPos").toInt() + 20);
    //        newUN->graphicsTxtItem->setHtml(newUN->getGraphicsTextLabel());

            newUN->geoAltitude = unRecord.value("geoAltitude").toDouble();

            newUN->geoLatitudeFlag = unRecord.value("geoLatitudeFlag").toUInt();
            newUN->geoLatitude = unRecord.value("geoLatitude").toDouble();
            newUN->geoLatitudeDegrees = unRecord.value("geoLatitudeDegrees").toDouble();
            newUN->geoLatitudeMinutes = unRecord.value("geoLatitudeMinutes").toDouble();
            newUN->geoLatitudeSeconds = unRecord.value("geoLatitudeSeconds").toDouble();

            newUN->geoLongitudeFlag = unRecord.value("geoLongitudeFlag").toUInt();
            newUN->geoLongitude = unRecord.value("geoLongitude").toDouble();
            newUN->geoLongitudeDegrees = unRecord.value("geoLongitudeDegrees").toDouble();
            newUN->geoLongitudeMinutes = unRecord.value("geoLongitudeMinutes").toDouble();
            newUN->geoLongitudeSeconds = unRecord.value("geoLongitudeSeconds").toDouble();

            newUN->flag_hidden = ((0 != unRecord.value("hidden").toInt()) ? true : false);

            for(int i(0); i < 2; i++)
            {
                QString strMode("modeVK%1");
                strMode = strMode.arg(i + 1);
                newUN->listSettingVK[i].modeVK = unRecord.value(strMode).toUInt();

                QByteArray nullIDBSK;
                nullIDBSK.append((char)0x00);
                nullIDBSK.append((char)0x00);
                for(int j(0); j < 20; j++)
                {
                    QByteArray idBSK;
                    QString strBSK("ConnectVK%1_%2");
                    strBSK = strBSK.arg(i + 1)
                                   .arg(j + 1);

                    idBSK.append(unRecord.value(strBSK).toByteArray());

                    if(idBSK.isEmpty() || idBSK == nullIDBSK)
                    {
                        newUN->listSettingVK[i].listIDBSK[j] = nullIDBSK;
                    }
                    else
                    {
                        newUN->listSettingVK[i].listIDBSK[j] = idBSK;
                    }
                }

                for(int j(0); j < 5; j++)
                {
                    QString strX("xPosLFlangP_%1"), strY("yPosLFlangP_%1");
                    strX = strX.arg(j + 1);
                    strY = strY.arg(j + 1);
                    newUN->listLFlangP[j]->setPos(unRecord.value(strX).toInt(),
                                                  unRecord.value(strY).toInt());
                }

                for(int j(0); j < 5; j++)
                {
                    QString strX("xPosRFlangP_%1"), strY("yPosRFlangP_%1");
                    strX = strX.arg(j + 1);
                    strY = strY.arg(j + 1);
                    newUN->listRFlangP[j]->setPos(unRecord.value(strX).toInt(),
                                                  unRecord.value(strY).toInt());
                }
            }

            if((quint8)Unknown_KMDeviceType != (quint8)newUN->_typeUN())
            {
    //            newUN->graphicsPxmItem->setFlag(QGraphicsItem::ItemIsMovable);
                newUN->graphicsPxmItem->setFlag(QGraphicsItem::ItemIsSelectable);
                newUN->graphicsPxmItem->setFlag(QGraphicsItem::ItemIsFocusable);
                newUN->graphicsPxmItem->setZValue(1);
            }

            connect(newUN,
                    SIGNAL(timeoutB1(UnitNode *)),
                    this,
                    SLOT(requestB1(UnitNode*)));

            connect(newUN,
                    SIGNAL(timeoutB3(UnitNode*,
                                     quint8,
                                     quint8,
                                     quint32,
                                     quint32)),
                    this,
                    SLOT(requestB3(UnitNode*,
                                   quint8,
                                   quint8,
                                   quint32,
                                   quint32)));
            connect(newUN,
                    SIGNAL(timeoutB4(UnitNode*,
                                     quint8,
                                     quint8,
                                     quint32,
                                     quint32)),
                    this,
                    SLOT(requestB4(UnitNode*,
                                   quint8,
                                   quint8,
                                   quint32,
                                   quint32)));

            connect(newUN,
                    SIGNAL(updateUN(UnitNode*)),
                    this,
                    SIGNAL(updateUN(UnitNode*)));

            connect(newUN,
                    SIGNAL(uploadUN(UnitNode*)),
                    dbManager,
                    SLOT(uploadUNToDB(UnitNode*)));


    //        connect(newUN,
    //                SIGNAL(updateUN(UnitNode*)),
    //                dbManager,
    //                SLOT(updateUN(UnitNode*)));

            this->listBTUnitNode.append(newUN);
        }
        catch (std::runtime_error &rte)
        {
            qWarning("catch runtime_error %s [BTMsgManager::setInitialListUN]", rte.what());
        }
        catch (std::bad_alloc& ba)
        {
            qWarning("catch bad_alloc %s [BTMsgManager::setInitialListUN]", ba.what());
        }
        catch (std::exception &e)
        {
            qWarning("catch exception %s [BTMsgManager::setInitialListUN]", e.what());
        }
        catch (...)
        {
            qWarning("catch exception <unknown> [BTMsgManager::setInitialListUN]");
        }
    }

    for(int i(0), n(this->listBTUnitNode.size()); i < n; i++)
    {
        if(0 == this->listBTUnitNode.at(i)->dbIndexParent)
        {
            this->involvingInStructure(this->m_treeRootUN,
                                       this->listBTUnitNode.at(i));
            emit this->addNewUN(this->listBTUnitNode.at(i));
        }
        else
        {
            for(int j(0); j < n; j++)
            {
                if(this->listBTUnitNode.at(j)->dbIndex == this->listBTUnitNode.at(i)->dbIndexParent)
                {
                    this->involvingInStructure(this->listBTUnitNode.at(j),
                                               this->listBTUnitNode.at(i));
                    emit this->addNewUN(this->listBTUnitNode.at(i));
                    break;
                }
            }
        }
    }
}

//UnitNode* BTMsgManager::getUnitNode(QByteArray id) /*noexcept*/
//{
//    for(int i(0), n(this->listUnitNode.size()); i < n; i++)
//    {
//        if(id == this->listUnitNode[i]->id)
//        {
//            return this->listUnitNode[i];
//        }
//    }
//    return nullptr;
//}

UnitNode* BTMsgManager::getUnitNode(QByteArray id,
                                    UnitNode *parentUN) /*noexcept*/
{
    UnitNode *res(nullptr);
    if(!parentUN)
    {
        parentUN = this->m_treeRootUN;
    }

    if(parentUN->id == id)
    {
        return parentUN;
    }

    for(int i(0), n(parentUN->childCount()); i < n; i++)
    {

        res = this->getUnitNode(id,
                                parentUN->child(i));
        if(res)
            return res;
    }

    return nullptr;
}

QList<UnitNode*> BTMsgManager::possGetListUnitNodeChilde(UnitNode* unParent) /*noexcept*/
{
    return this->possGetListUnitNodeChilde(unParent->_mac());
}

QList<UnitNode*> BTMsgManager::possGetListUnitNodeChilde(QByteArray someKey) /*noexcept*/
{
    UnitNode* unParent(0);

    if(someKey.count(':') && !unParent)
    {
//        unParent = 0;
        for(int i(0), n(this->listBTUnitNode.size()); i < n; i++)
        {
            if(someKey == this->listBTUnitNode[i]->_mac())
            {
                unParent = this->listBTUnitNode[i];
            }
        }
    }

    if(someKey.count('_') && !unParent)
    {
//        unParent = 0;
        for(int i(0), n(this->listBTUnitNode.size()); i < n; i++)
        {
            if(someKey == this->listBTUnitNode[i]->name)
            {
                unParent = this->listBTUnitNode[i];
            }
        }
    }

    if(someKey.count(0xFF) && !unParent)
    {
//        unParent = 0;
        someKey.chop(1);
        for(int i(0), n(this->listBTUnitNode.size()); i < n; i++)
        {
            if(someKey == this->listBTUnitNode[i]->btLink &&
               this->listBTUnitNode[i]->flag_connection)
            {
                unParent = this->listBTUnitNode[i];
            }
        }
    }

    QList<UnitNode*> listChilde;
    if(unParent)
    {
        for(int i(0), n(this->listBTUnitNode.size()); i < n; i++)
        {
            if(unParent->btLink.size() < this->listBTUnitNode[i]->btLink.size())
            {
                if(unParent->btLink == this->listBTUnitNode[i]->btLink.left(unParent->btLink.size()))
                {
                    listChilde.append(this->listBTUnitNode[i]);
                }
            }
        }
    }
    return listChilde;
}

QList<UnitNode*> BTMsgManager::getListUnitNodeChilde(QByteArray id) /*noexcept*/
{

    UnitNode *unParent(this->getUnitNode(id));
    if(unParent)
        return this->possGetListUnitNodeChilde(unParent);
    QList<UnitNode*> l;
    return l;
}

QList<UnitNode*> BTMsgManager::getListUnitNodeChilde(UnitNode* unParent) /*noexcept*/
{
    if(unParent)
        return this->possGetListUnitNodeChilde(unParent->_mac());
    QList<UnitNode*> l;
    return l;
}

//void BTMsgManager::updateLastAppendDase(UnitNode *un) /*noexcept*/
//{

//}

UnitNode* BTMsgManager::possGetUnitNode(QByteArray someKey) /*noexcept*/
{
    if(someKey.count(':'))
    {
        for(int i(0), n(this->listBTUnitNode.size()); i < n; i++)
        {
            if(someKey == this->listBTUnitNode[i]->_mac())
            {
                return this->listBTUnitNode[i];
            }
        }
        return nullptr;
    }
    if(someKey.count('_'))
    {
        for(int i(0), n(this->listBTUnitNode.size()); i < n; i++)
        {
            if(someKey == this->listBTUnitNode[i]->name)
            {
                return this->listBTUnitNode[i];
            }
        }
        return nullptr;
    }

    if(someKey.count(0xFF))
    {
        someKey.chop(1);
    }
    for(int i(0), n(this->listBTUnitNode.size()); i < n; i++)
    {
        if(someKey == this->listBTUnitNode[i]->btLink &&
           this->listBTUnitNode[i]->flag_connection)
        {
            return this->listBTUnitNode[i];
        }
    }
    return nullptr;
}

UnitNode* BTMsgManager::possAddUnitNode(QByteArray mac,
                                        QByteArray name) /*noexcept*/
{
#ifdef QT_DEBUG
    qDebug() << "BTMsgManager::possAddUnitNode(" << mac << "," << name << ")";
#endif

    if(mac.isEmpty())
    {
        return nullptr;
    }
//    bool addKey(true);
    for(int i(0), n(this->listBTUnitNode.size()); i < n; i++)
    {
        if(mac == this->listBTUnitNode.at(i)->_mac())
        {
            this->listBTUnitNode.at(i)->updateName(name);
            //
            this->listBTUnitNode.at(i)->updateDBIndex(this->dbManager->updateUN(this->listBTUnitNode[i]));
            //
            emit this->updateUN(this->listBTUnitNode.at(i));
            return this->listBTUnitNode.at(i);
        }
    }

    try
    {
        UnitNode *newUnitNode(new UnitNode(this));
        newUnitNode->clear();
        newUnitNode->updateType(KMSystemType, Unknown_KMDeviceType);
        newUnitNode->updateMAC(mac);
        newUnitNode->updateName(name);

        this->listBTUnitNode.append(newUnitNode);
        connect(newUnitNode,
                SIGNAL(timeoutB1(UnitNode *)),
                this,
                SLOT(requestB1(UnitNode*)));
        connect(newUnitNode,
                SIGNAL(timeoutB3(UnitNode*,
                                 quint8,
                                 quint8,
                                 quint32,
                                 quint32)),
                this,
                SLOT(requestB3(UnitNode*,
                               quint8,
                               quint8,
                               quint32,
                               quint32)));
        connect(newUnitNode,
                SIGNAL(timeoutB4(UnitNode*,
                                 quint8,
                                 quint8,
                                 quint32,
                                 quint32)),
                this,
                SLOT(requestB4(UnitNode*,
                               quint8,
                               quint8,
                               quint32,
                               quint32)));


    //    connect(newUnitNode,
    //            SIGNAL(updateUN(UnitNode*)),
    //            dbManager,
    //            SLOT(updateUN(UnitNode*)));

        //
        newUnitNode->updateDBIndex(this->dbManager->addNewUN(newUnitNode));
        //

        this->involvingInStructure(this->m_treeRootUN,
                                   newUnitNode);
        connect(newUnitNode,
                SIGNAL(updateUN(UnitNode*)),
                this,
                SIGNAL(updateUN(UnitNode*)));

        connect(newUnitNode,
                SIGNAL(uploadUN(UnitNode*)),
                dbManager,
                SLOT(uploadUNToDB(UnitNode*)));

        emit this->addNewUN(newUnitNode);

        if(newUnitNode->_dbIndex())
        {
            emit this->updateUN(newUnitNode);
        }
        return newUnitNode;
    }
    catch (std::runtime_error &rte)
    {
        qWarning("catch runtime_error %s [BTMsgManager::possAddUnitNode]", rte.what());
    }
    catch (std::bad_alloc& ba)
    {
        qWarning("catch bad_alloc %s [BTMsgManager::possAddUnitNode]", ba.what());
    }
    catch (std::exception &e)
    {
        qWarning("catch exception %s [BTMsgManager::possAddUnitNode]", e.what());
    }
    catch (...)
    {
        qWarning("catch exception <unknown> [BTMsgManager::possAddUnitNode]");
    }

    return nullptr;
}

bool BTMsgManager::possAddCommunicationItem(UnitNode* unMaster,
                                            UnitNode* unSlave) /*noexcept*/
{
    //есть такой Item уже или нет -->
    for(quint8 i(0), n(this->listCommunicationItem.size()); i < n; i++)
    {
        if(((this->listCommunicationItem.at(i).unMaster == unMaster) &&
           (this->listCommunicationItem.at(i).unSlave == unSlave)) ||
           ((this->listCommunicationItem.at(i).unMaster == unSlave) &&
            (this->listCommunicationItem.at(i).unSlave == unMaster)))
        {
            return false;
        }
    }
    //есть такой Item уже или нет <--

    //добавление Item -->
    if(unMaster && unSlave)
    {
        CommunicationItem newItem;
        newItem.unMaster = unMaster;
        newItem.unSlave = unSlave;
        this->listCommunicationItem.append(newItem);
        return true;
    }
    //добавление Item <--
    return false;
}

bool BTMsgManager::possAddUsedCommunicationItem(UnitNode* unMaster,
                                                UnitNode* unSlave) /*noexcept*/
{
    //есть такой Item уже или нет -->
    bool addKey(true);
    for(quint8 i(0), n(this->usedCommunicationItem.size()); i < n; i++)
    {
        if(((this->usedCommunicationItem.at(i).unMaster == unMaster) &&
           (this->usedCommunicationItem.at(i).unSlave == unSlave)) ||
           ((this->usedCommunicationItem.at(i).unMaster == unSlave) &&
            (this->usedCommunicationItem.at(i).unSlave == unMaster)))
        {
            addKey = false;
            break;
        }
    }
    //есть такой Item уже или нет <--

    //добавление Item -->
    if(addKey)
    {
        CommunicationItem newItem;
        newItem.unMaster = unMaster;
        newItem.unSlave = unSlave;
        this->usedCommunicationItem.append(newItem);
    }
    //добавление Item <--
    return addKey;
}


bool BTMsgManager::possAddBTToListInquiry(UnitNode* unMaster,
                                          UnitNode* unSlave) /*noexcept*/
{
    if(!unMaster->listInquiry.count(unSlave))
    {
        unMaster->listInquiry.append(unSlave);
        return true;
    }
    return false;
}

bool BTMsgManager::updateBTTxPower(UnitNode* unMaster,
                                   UnitNode* unSlave,
                                   int tx) /*noexcept*/
{
    for(quint8 i(0), n(this->listCommunicationItem.size()); i < n; i++)
    {
        if(((this->listCommunicationItem.at(i).unMaster == unMaster) &&
           (this->listCommunicationItem.at(i).unSlave == unSlave)) ||
           ((this->listCommunicationItem.at(i).unMaster == unSlave) &&
            (this->listCommunicationItem.at(i).unSlave == unMaster)))
        {
            this->listCommunicationItem[i].txPower = tx;
            unSlave->updateTxPower(tx);
            break;
        }
    }

    return false;
}

bool BTMsgManager::updateBTRssi(UnitNode* unMaster,
                                UnitNode* unSlave,
                                int rssi) /*noexcept*/
{
    for(quint8 i(0), n(this->listCommunicationItem.size()); i < n; i++)
    {
        if(((this->listCommunicationItem.at(i).unMaster == unMaster) &&
           (this->listCommunicationItem.at(i).unSlave == unSlave)) ||
           ((this->listCommunicationItem.at(i).unMaster == unSlave) &&
            (this->listCommunicationItem.at(i).unSlave == unMaster)))
        {
            this->listCommunicationItem[i].rssi = rssi;
            unSlave->updateRssi(rssi);
            break;
        }
    }

    return false;
}


bool BTMsgManager::possAddBTToListConnect(UnitNode* unMaster,
                                          UnitNode* unSlave) /*noexcept*/
{
    if(!unMaster->listConnect.contains(unSlave))
    {
        unMaster->listConnect.append(unSlave);
        return true;
    }
    return false;
}

void BTMsgManager::setTxPower(UnitNode* unMaster,
                              UnitNode* unSlave,
                              int txPower) /*noexcept*/
{
    if(unMaster && unSlave)
    {
        unSlave->updateTxPower(txPower);
        this->possAddCommunicationItem(unMaster,
                                       unSlave);

        for(quint8 i(0), n(this->listCommunicationItem.size()); i < n; i++)
        {
            if(((this->listCommunicationItem.at(i).unMaster == unMaster) &&
               (this->listCommunicationItem.at(i).unSlave == unSlave)) ||
               ((this->listCommunicationItem.at(i).unMaster == unSlave) &&
                (this->listCommunicationItem.at(i).unSlave == unMaster)))
            {
                this->listCommunicationItem[i].txPower = txPower;
                break;
            }
        }
    }
}

void BTMsgManager::setRSSI(UnitNode* unMaster,
                           UnitNode* unSlave,
                           int rssi) /*noexcept*/
{
    if(unMaster && unSlave)
    {
        unSlave->updateRssi(rssi);
        this->possAddCommunicationItem(unMaster,
                                       unSlave);

        for(quint8 i(0), n(this->listCommunicationItem.size()); i < n; i++)
        {
            if(((this->listCommunicationItem.at(i).unMaster == unMaster) &&
               (this->listCommunicationItem.at(i).unSlave == unSlave)) ||
               ((this->listCommunicationItem.at(i).unMaster == unSlave) &&
                (this->listCommunicationItem.at(i).unSlave == unMaster)))
            {
                this->listCommunicationItem[i].rssi = rssi;
                break;
            }
        }
    }
}

void BTMsgManager::newBTMSG(QByteArray btLink,
                            QByteArray msg) /*noexcept*/
{
    emit this->ssOffStatusKMExchange(_pt_/*15000*/);

    for(int i(0), n(2); i < n; i++)
    {
        if((quint8)0x0A == (quint8)msg.right(1)[0] ||
           (quint8)0x0D == (quint8)msg.right(1)[0])
        {
             msg.chop(1);
        }
    }

    if(btLink.size())
    {
        if((quint8)0xFF == (quint8)btLink.right(1)[0])
        {
             btLink.chop(1);
        }
    }


    //поиск BT устройства по пути -->
    UnitNode* unSender(this->possGetUnitNode(btLink));
    //поиск BT устройства по пути <--

    if(unSender)//ЕСЛИ ОНО ЕСТЬ
    {
        unSender->updateFlagHidden(false);
        unSender->updateFlagConnection(true);
//        QList<QByteArray> msgSegment(msg.split(' '));//части сообщения
        auto msgSegment(msg.split(' '));//части сообщения

        bool needSaveMSG(false);

        if(msgSegment.first() == "OK")
        {
#ifdef QT_DEBUG
            qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " [" << btLink.toHex() << "] " << "  in<< " << ProxyMetod().sumQListQByteArray(msgSegment);
#endif
            return;
        }

        if(msgSegment.first() == "SET")
        {
            this->analyzerSET(unSender,
                              msgSegment);
            needSaveMSG = true;
        }

        if(msgSegment.first() == "INQUIRY_PARTIAL")
        {
//            this->analyzerINQUIRY_PARTIAL(unSender,
//                                          msgSegment);
            needSaveMSG = true;
        }

        if(msgSegment.first() == "INQUIRY")
        {
            this->analyzerINQUIRY(unSender,
                                  msgSegment);
            needSaveMSG = true;
        }

        if(msgSegment.first() == "NAME")
        {
            this->analyzerNAME(unSender,
                               msgSegment);
            needSaveMSG = true;
        }

        if(msgSegment.first() == "CALL")
        {
//            this->analyzerCALL(unSender,
//                               msgSegment);
            needSaveMSG = true;
        }

        if(msgSegment.first() == "CONNECT")
        {
            this->analyzerCONNECT(unSender,
                                  msgSegment);
            needSaveMSG = true;
        }

        if(msgSegment.first() == "LIST")
        {
            this->analyzerLIST(unSender,
                               msgSegment);
            needSaveMSG = true;
        }

        if(msgSegment.first() == "TXPOWER")
        {
            this->analyzerTXPOWER(unSender,
                                  msgSegment);
            needSaveMSG = true;
        }

        if(msgSegment.first() == "RSSI")
        {
            this->analyzerRSSI(unSender,
                               msgSegment);
            needSaveMSG = true;
        }

        if(msgSegment.first() == "NO")
        {
            this->analyzerNO(unSender,
                             msgSegment);
            needSaveMSG = true;
        }

        if(msgSegment.first() == "SYNTAX")
        {
//            this->analyzerSYNTAX(unSender,
//                                 msgSegment);
            needSaveMSG = true;
        }


        if(needSaveMSG)
        {
            emitInsertNewMSG(unSender,
                             0xFF,
                             msg,
                             true,
                             Other_TypeGroupMSG);
        }

    }
    else
    {
//        QList<QByteArray> msgSegment(msg.split(' '));//части сообщения
        auto msgSegment(msg.split(' '));//части сообщения
#ifdef QT_DEBUG
        qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " [" << btLink.toHex() << "] " << "  in<< " << msg;
#endif

        if(3 < msgSegment.size())
        {
            if("SET" == msgSegment.at(0) &&
//               "BT" == msgSegment.at(1) &&
               "NAME" == msgSegment.at(2))
            {
                UnitNode *unSender(this->possAddUnitNode(macBaseUN,
                                                         msgSegment.at(3)));
                if(unSender)
                {
                    unSender->updateFlagHidden(false);
                    unSender->updateBTLink(btLink);
                    unSender->updateDBIndex(this->dbManager->updateUN(unSender));

                    emitInsertNewMSG(unSender,
                                     0xFF,
                                     msg,
                                     true,
                                     System_TypeGroupMSG);
                }
                else
                {
                    emitInsertNewMSG(0x00,
                                     0xFF,
                                     msg,
                                     true,
                                     System_TypeGroupMSG);
                }
                return;
            }
            if("SET" == msgSegment.at(0) &&
//               "BT" == msgSegment.at(1) &&
               "BDADDR" == msgSegment.at(2))
            {
                macBaseUN = msgSegment.at(3);
//                UnitNode* unSender(this->possAddUnitNode("", msgSegment.at(3)));
//                if(unSender)
//                {
//                    unSender->updateBTLink(btLink);
//                    unSender->updateDBIndex(this->dbManager->updateUN(unSender));
//                }
                emitInsertNewMSG(0x00,
                                 0xFF,
                                 msg,
                                 true,
                                 System_TypeGroupMSG);
                return;
            }
        }

#ifdef QT_DEBUG
        qDebug() << "missing msg " << msg;
#endif
    }
}

void BTMsgManager::analyzerSET(UnitNode* unSender,
                               QList<QByteArray> msgSegment) /*noexcept*/
{
#ifdef QT_DEBUG
    qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " [" << unSender->btLink.toHex() << "] " << "  in<< " << ProxyMetod().sumQListQByteArray(msgSegment);
#endif
//    emitInsertNewMSG(unSender,
//                     0xFF,
//                     ProxyMetod().sumQListQByteArray(msgSegment),
//                     true,
//                     System_TypeGroupMSG);

    if(3 < msgSegment.size())
    {
        if("BT" == msgSegment.at(1) &&
           "NAME" == msgSegment.at(2))
        {
            this->possAddUnitNode(unSender->_mac(),
                                  msgSegment.at(3));
            return;
        }
    }
}

//void BTMsgManager::analyzerINQUIRY_PARTIAL(UnitNode* unSender,
//                                           QList<QByteArray> msgSegment) /*noexcept*/
//{
//#ifdef QT_DEBUG
//    qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " [" << unSender->btLink.toHex() << "] " << "  in<< " << ProxyMetod().sumQListQByteArray(msgSegment);
//#endif
//    return;
//}

void BTMsgManager::analyzerINQUIRY(UnitNode* unSender,
                                   QList<QByteArray> msgSegment) /*noexcept*/
{
#ifdef QT_DEBUG
    qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " [" << unSender->btLink.toHex() << "] " << "  in<< " << ProxyMetod().sumQListQByteArray(msgSegment);
#endif
//    emitInsertNewMSG(unSender,
//                     0xFF,
//                     ProxyMetod().sumQListQByteArray(msgSegment),
//                     true,
//                     Connect_TypeGroupMSG);

    if(2 == msgSegment.size())//INQUIRY {count}
    {
        unSender->listInquiry.clear();
        unSender->expectInquryCount = QString(msgSegment.at(1)).toUInt();

        if(0 == unSender->expectInquryCount)
        {

            if(DutyMode == this->workMode())
            {
                if(this->listCurrentInterrogatedBT.size())
                {
                    this->listCurrentInterrogatedBT.removeFirst();
                }
                this->lastInquiryM = nullptr;
                if(this->listCurrentInterrogatedBT.size())
                {
                    this->emitReInterrogateBT(_dt_);
                }
                else
                {
                    //
                    this->setCurrentWorkMode(DutyMode);
                    //
//                    this->emitReInterrogateBT(_pt_);
                }
                return;
            }

            if(ConnectSubset == this->workMode())
            {
                if(this->listInitialSubset.size())
                {
                    this->listInitialSubset.removeFirst();
                }

                this->detourTact();
                return;
            }

            if(DefinitionHeard == this->workMode())
            {
                if(this->listInitialSubset.size())
                {
                    this->listInitialSubset.removeFirst();
                }
                this->detourTact();
                return;
            }

            if(ConnectVK == this->workMode())
            {//не получилось переход к другому варианту
//                //удаление провального множества переход к другомуимножеству -->
//                for(int i(0), n(this->listInitialWayToVK.size()); i < n; i++)
//                {
//                    if(this->listInitialWayToVK.at(i) == this->currentWayVK)
//                    {
//                        if(this->listInitialWayToVK.size())
//                        {
//                            this->listInitialWayToVK.removeAt(i);
//                            //попробовать другой путь
//                            this->connectOptimalWayToVK(this->listInitialWayToVK.first());
//                            return;
//                        }
//                        else
//                        {
//                            //конец подключения камер
//                        }
//                    }
//                }
//                //удаление провального множества переход к другому множеству <--

                return;
            }
        }
    }

}
void BTMsgManager::analyzerNAME(UnitNode* unSender,
                                QList<QByteArray> msgSegment) /*noexcept*/
{
#ifdef QT_DEBUG
    qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " [" << unSender->btLink.toHex() << "] " << "  in<< " << ProxyMetod().sumQListQByteArray(msgSegment);
#endif
//    emitInsertNewMSG(unSender,
//                     0xFF,
//                     ProxyMetod().sumQListQByteArray(msgSegment),
//                     true,
//                     Connect_TypeGroupMSG);

   unSender->expectInquryCount--;

    if(3 < msgSegment.size())
    {
        for(int i(3), n(msgSegment.size()); i < n; i++)
        {
            msgSegment[2].append(msgSegment[i]);
        }
    }

    msgSegment[2].remove(0, 1);
    msgSegment[2].chop(1);

//    //фильтр имён -->
//    if(("VK_0104200005" == msgSegment.at(2).left(13)) ||
//       ("VK_0101920008" == msgSegment.at(2).left(13)) ||
//       ("RT_S_0204200002" == msgSegment.at(2).left(15)) ||
//       ("RT_S_0204200002" == msgSegment.at(2).left(15)))
    if(("VK_01" == msgSegment.at(2).left(5)) ||
       ("RT_S_02" == msgSegment.at(2).left(7)) ||
       ("PKP_03" == msgSegment.at(2).left(6)))
    {
        UnitNode *newAddUN(this->possAddUnitNode(msgSegment.at(1),
                                                 msgSegment.at(2)));
        if(newAddUN)
        {
            this->possAddBTToListInquiry(unSender,
                                         newAddUN);
        }
//        this->possAddBTToListInquiry(unSender,
//                                     this->possAddUnitNode(msgSegment.at(1),
//                                                           msgSegment.at(2)));
#ifdef QT_DEBUG
        for(int i(0), n(this->listBTUnitNode.size()); i < n; i++)
        {
            qDebug() << this->listBTUnitNode[i]->_mac();
        }
#endif
    }
    //фильтр имён <--



    //окончание приёма после запроса INQUIRY {timeout} NAME -->
    if(!unSender->expectInquryCount)
    {
        if(DutyMode == this->workMode())
        {
            this->lastInquiryM = nullptr;
            for(int i(0), n(unSender->listInquiry.size()); i < n; i++)
            {
                this->listDefinitionSyperfloursBT.append(unSender->listInquiry[i]);
            }

            if(this->listDefinitionSyperfloursBT.size())
            {
                this->lastListM = this->lastCallM = unSender;
                this->lastCallS = this->listDefinitionSyperfloursBT.first();

                emit this->ssPostponedCommandCALL(_dt_);
            }
            else
            {
                if(this->listCurrentInterrogatedBT.size())
                {
                    this->listCurrentInterrogatedBT.removeFirst();//.removeOne(unSender->mac);
                }
                if(this->listCurrentInterrogatedBT.size())
                {
                    this->emitReInterrogateBT(_dt_);
                }
                else
                {
                    //
                    this->setCurrentWorkMode(DutyMode);
                    //
//                    this->emitReInterrogateBT(_pt_);
                }
            }
            return;
        }

        if(ConnectSubset == this->workMode())
        {
            //пора отправлять отложенную команду
            for(int i(0), n(unSender->listInquiry.size()); i < n; i++)
            {
                if(unSender->listInquiry[i] == this->currentCheckedSubsetOfCommunication.at(1))
                {
                    this->lastCallM = this->currentCheckedSubsetOfCommunication.first();
                    this->lastCallS = this->currentCheckedSubsetOfCommunication.at(1);
                    this->lastListM = this->lastCallM;

                    emit this->ssPostponedCommandCALL(_dt_);

                    return;
                }
            }

            //совсем всё плохо искомого BT для подключения не найденно -->
            if(this->listInitialSubset.size())
            {
                this->listInitialSubset.removeFirst();
            }
            this->detourTact();
            //совсем всё плохо искомого BT для подключения не найденно <--
        }

        if(DefinitionHeard == this->workMode())
        {
            //окончание приёма после запроса INQUIRY {timeout} NAME
            //пора отправлять отложенную команду
            this->definitionInitiator = unSender;
            this->listCallBT.clear();

            for(int i(0), n(unSender->listInquiry.size()); i < n; i++)
            {
                this->listCallBT.append(unSender->listInquiry[i]);
            }

            this->definitionListCallBT();
        }

        if(ConnectVK == this->workMode())
        {
            //пора отправлять отложенную команду
            for(int i(0), n(unSender->listInquiry.size()); i < n; i++)
            {
                if(unSender->listInquiry[i] == this->currentCheckedWayVK.at(1))
                {
                    this->lastCallM = this->currentCheckedWayVK.first();
                    this->lastCallS = this->currentCheckedWayVK.at(1);
                    this->lastListM = this->lastCallM;

                    emit this->ssPostponedCommandCALL(_dt_);

                    return;
                }
            }

            //совсем всё плохо искомого BT для подключения не найденно -->

            //удаление провального множества переход к другомуимножеству -->
            for(int i(0), n(this->listInitialWayToVK.size()); i < n; i++)
            {
                if(this->listInitialWayToVK.at(i) == this->currentWayVK)
                {
                    this->listInitialWayToVK.removeAt(i);
                    if(this->listInitialWayToVK.size())
                    {
                        //попробовать другой путь
                        this->connectOptimalWayToVK(this->listInitialWayToVK.first());
                        return;
                    }
                    else
                    {
                        this->setCurrentWorkMode(DutyMode);
                        //конец подключения камер
                    }
                }
            }
            //удаление провального множества переход к другому множеству <--

            //перейти к другому варианту

            //совсем всё плохо искомого BT для подключения не найденно <--
        }
    }
    //окончание приёма после запроса INQUIRY {timeout} NAME <--
}

//void BTMsgManager::analyzerCALL(UnitNode* unSender,
//                                QList<QByteArray> msgSegment) /*noexcept*/
//{
//#ifdef QT_DEBUG
//    qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " [" << unSender->btLink.toHex() << "] " << "  in<< " << ProxyMetod().sumQListQByteArray(msgSegment);
//#endif
//    return;
//}

void BTMsgManager::analyzerCONNECT(UnitNode* unSender,
                                   QList<QByteArray> msgSegment) /*noexcept*/
{
#ifdef QT_DEBUG
    qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " [" << unSender->btLink.toHex() << "] " << "  in<< " << ProxyMetod().sumQListQByteArray(msgSegment);
#endif
//    emitInsertNewMSG(unSender,
//                     0xFF,
//                     ProxyMetod().sumQListQByteArray(msgSegment),
//                     true,
//                     Connect_TypeGroupMSG);

    if(this->lastCallM == unSender)
    {
        this->lastCallS->updateFlagConnection(true);
        //
        UnitNode *objPtr(this->lastCallS),
                 *sourceParent(objPtr->unTreeParent);
        int sourceChild(objPtr->unTreeParent->listChilde.indexOf(objPtr));

        this->involvingInStructure(this->lastCallM,
                                   this->lastCallS);

        UnitNode *destinationParent(objPtr->unTreeParent);
        int destinationChild(objPtr->unTreeParent->listChilde.indexOf(objPtr));
        if(sourceParent != destinationParent)
        {
            emit this->moveUN(objPtr,
                              sourceParent,
                              sourceChild,
                              destinationParent,
                              destinationChild);
        }
        //
        emit this->connectUN(this->lastCallM,
                             this->lastCallS);
    }
    else
    {
#ifdef QT_DEBUG
        qDebug() << "no waited connect !!!";
#endif
        QByteArray slaveBTLink(unSender->btLink);
        slaveBTLink.append(QString(msgSegment.at(2)).toInt());
        UnitNode* unSlave(this->possGetUnitNode(slaveBTLink));
        if(unSlave)
        {
            //
            UnitNode *objPtr(unSlave),
                     *sourceParent(objPtr->unTreeParent);
            int sourceChild(objPtr->unTreeParent->listChilde.indexOf(objPtr));

            this->involvingInStructure(unSender,
                                       unSlave);

            UnitNode *destinationParent(objPtr->unTreeParent);
            int destinationChild(objPtr->unTreeParent->listChilde.indexOf(objPtr));
            if(sourceParent != destinationParent)
            {
                emit this->moveUN(objPtr,
                                  sourceParent,
                                  sourceChild,
                                  destinationParent,
                                  destinationChild);
            }
            //
            emit this->connectUN(unSender,
                                 unSlave);
        }
    }


    if(Undefined != this->workMode() &&
       DutyMode != this->workMode())
    {
        this->lastListM = unSender;
        emit this->ssPostponedCommandLIST(_dt_/**3*/);
    }

    if(DutyMode == this->workMode())
    {
        if(this->lastCallM == unSender)
        {
            this->lastListM = unSender;
            emit this->ssPostponedCommandLIST(_dt_/**3*/);
        }
    }

    return;
}

void BTMsgManager::analyzerNO(UnitNode* unSender,
                              QList<QByteArray> msgSegment) /*noexcept*/
{
#ifdef QT_DEBUG
    qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " [" << unSender->btLink.toHex() << "] " << "  in<< " << ProxyMetod().sumQListQByteArray(msgSegment);
//    emitInsertNewMSG(unSender,
//                     0xFF,
//                     ProxyMetod().sumQListQByteArray(msgSegment),
//                     true,
//                     Connect_TypeGroupMSG);
    qDebug() << "analyzerNO -->";
#endif
    QByteArray slaveBTLink(unSender->btLink);
    slaveBTLink.append(QString(msgSegment.at(2)).toInt());
    UnitNode* unSlave(this->possGetUnitNode(slaveBTLink));
    if(unSlave)
    {
//        unSlave->btLink.clear();
        this->extractionFromStructure(unSlave);
//        unSlave->updateBTLinkValid(false);
        emit this->disconnectUN(unSender,
                                unSlave);
#ifdef QT_DEBUG
        qDebug() << "NO CONNECT [" << unSlave->_mac() << "]";
#endif
    }

    if(Undefined != this->workMode() &&
       DutyMode != this->workMode())
    {
#ifdef QT_DEBUG
        qDebug() << "WORK MODE [" << this->workMode() << "]";
#endif
        if(DisconnectSubset == this->workMode())
        {
            if(this->listDisconnect.size())
            {
                this->listDisconnect.removeLast();
            }

            if(1 < this->listDisconnect.size())
            {
                this->lastKillM = this->listDisconnect[this->listDisconnect.size() - 2];
                this->lastKillS = this->listDisconnect[this->listDisconnect.size() - 1];

                emit this->ssPostponedCommandKILL(_dt_);
            }
            else
            {
                this->detourTact();
            }

#ifdef QT_DEBUG
            qDebug() << "analyzerNO <--";
#endif
            return;
        }

        this->lastListM = unSender;
        emit this->ssPostponedCommandLIST(_dt_);
    }

    if(DutyMode == this->workMode())
    {


        if(this->lastCallM == unSender)
        {
            this->lastListM = unSender;
            emit this->ssPostponedCommandLIST(_dt_);
#ifdef QT_DEBUG
            qDebug() << "analyzerNO <--";
#endif
            return;
        }
        else
        {
            if(unSlave)//ЕСЛИ ОНО ЕСТЬ
            {


                this->listSpareWayToVK.clear();

                //поиск VK с которыми потеряна связь -->
                QList<UnitNode*> listLostVK;
                for(int i(0); i < this->listCurrentUsedWayToVK.size(); i++)
                {
                    if(this->listCurrentUsedWayToVK[i].contains(unSlave) &&
                       this->listCurrentUsedWayToVK[i].contains(unSender))
                    {
                        if(this->listCurrentUsedWayToVK.size())
                        {
                            listLostVK.append(this->listCurrentUsedWayToVK[i].last());
                        }
                        if(i < this->listCurrentUsedWayToVK.size())
                        {
                            this->listCurrentUsedWayToVK.removeAt(i);
                        }
                        i--;
                    }
                }
#ifdef QT_DEBUG
                qDebug() << "Lost VK [" << listLostVK << "]";
#endif

                //поиск VK с которыми потеряна связь <--

                for(int i(0), n(listLostVK.size()); i < n; i++)
                {
                    for(int j(0), m(this->listShortWayToVK.size()); j < m; j++)
                    {
                        if(this->listShortWayToVK[j].last() == listLostVK[i])
                        {
                            this->listSpareWayToVK.append(this->listShortWayToVK.at(j));
                        }
                    }
                }
#ifdef QT_DEBUG
                qDebug() << "Variant Lost Way To VK [" << this->listSpareWayToVK << "]";
#endif

                if(this->lastKillM == unSender &&
                   this->lastKillS == unSlave)
                {
                    this->lastKillM = this->lastKillS = nullptr;
                    this->lastListM = unSender;
                    emit this->ssPostponedCommandLIST(_dt_);
#ifdef QT_DEBUG
                    qDebug() << "analyzerNO <--";
#endif
                    return;
                }
                else
                {
                    this->connectSpareWayToVK();
                }

            }
        }

    }

#ifdef QT_DEBUG
    qDebug() << "analyzerNO <--";
#endif
    return;
}

void BTMsgManager::analyzerLIST(UnitNode* unSender,
                                QList<QByteArray> msgSegment) /*noexcept*/
{
#ifdef QT_DEBUG
    qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " [" << unSender->btLink.toHex() << "] " << "  in<< " << ProxyMetod().sumQListQByteArray(msgSegment);
#endif
//    emitInsertNewMSG(unSender,
//                     0xFF,
//                     ProxyMetod().sumQListQByteArray(msgSegment),
//                     true,
//                     Connect_TypeGroupMSG);

    if(2 == msgSegment.size())//LIST {count}
    {
        unSender->listConnect.clear();
        unSender->expectConnectCount = QString(msgSegment.at(1)).toUInt();

        if(0 == unSender->expectConnectCount)//нет ниодного соединения
        {
            //
            this->extractionChildeFromStructure(unSender);
            //


            if(this->checkedCommunication.unMaster &&
               this->checkedCommunication.unSlave)
            {//проверка соединений после Call
                this->checkedCommunication.unMaster = nullptr;
                this->checkedCommunication.unSlave = nullptr;

                unSender->expectConnect = 0;

                if(DutyMode == this->workMode())
                {
                    if(this->listDefinitionSyperfloursBT.size())
                    {
                        this->listDefinitionSyperfloursBT.removeFirst();
                    }

                    if(this->listDefinitionSyperfloursBT.size())
                    {
                        this->lastListM = this->lastCallM = this->listCurrentInterrogatedBT.first();
                        this->lastCallS = this->listDefinitionSyperfloursBT.first();

                        emit this->ssPostponedCommandCALL(_dt_);
                    }
                    else
                    {
                        if(this->listCurrentInterrogatedBT.size())
                        {
                            this->listCurrentInterrogatedBT.removeFirst();
                        }
                        if(this->listCurrentInterrogatedBT.size())
                        {
                            this->emitReInterrogateBT(_dt_);
                        }
                        else
                        {
                            //
                            this->setCurrentWorkMode(DutyMode);
                            //
//                            this->emitReInterrogateBT(_pt_);
                        }
                    }
                    return;
                }

                if(ConnectSubset == this->workMode())
                {
                    //либо перезапрос
                    if(this->listInitialSubset.size())
                    {
                        this->listInitialSubset.removeFirst();
                    }
                    this->detourTact();
                    return;
                }

                if(DefinitionHeard == this->workMode())
                {
                    if(this->listCallBT.size())
                    {
                        this->listCallBT.removeFirst();
                    }
                    this->definitionListCallBT();
                    return;
                }

                if(ConnectVK == this->workMode())
                {
                    //удаление провального множества и переход к следующему -->
                    for(int i(0), n(this->listInitialWayToVK.size()); i < n; i++)
                    {
                        if(this->listInitialWayToVK.at(i) == this->currentWayVK)
                        {
                            this->listInitialWayToVK.removeAt(i);
                        }
                    }
                    if(this->listInitialWayToVK.size())
                    {
                        //попробовать другой путь
                        this->connectOptimalWayToVK(this->listInitialWayToVK.first());
                        return;
                    }
                    else
                    {
                        QString msg("Buillded network");
                        this->setCurrentWorkMode(DutyMode);
                        emitInsertNewMSG(0x00,
                                         0x00,
                                         msg.toUtf8(),
                                         true,
                                         System_TypeGroupMSG);
                        return;
                        //конец подключения камер
                    }
                    //удаление провального множества и переход к следующему <--
                    return;
                }
            }
            else
            {
                if(ConnectSubset == this->workMode())
                {
                    this->lastInquiryM = this->currentCheckedSubsetOfCommunication.first();
                    emit this->ssPostponedCommandINQUIRY(_dt_);
                }

                if(DefinitionHeard == this->workMode())
                {
                    if(this->listCallBT.size())
                    {
                        this->listCallBT.removeFirst();
                    }
                    else
                    {

                        this->lastInquiryM = this->currentSubset.last();

                        emit this->ssPostponedCommandINQUIRY(_dt_);

                        return;
                    }

                    this->definitionListCallBT();
                    return;
                }

                if(ConnectVK == this->workMode())
                {
                    this->lastInquiryM = this->currentCheckedWayVK.first();
                    emit this->ssPostponedCommandINQUIRY(_dt_);
                    return;
                }
            }
        }
    }
    else//LIST {arg1} ... {argN}
    {
        QByteArray newBTLink(unSender->btLink);

        newBTLink.append(QString(msgSegment.at(1)).toUInt());

        UnitNode* unConnected(this->possGetUnitNode(msgSegment.at(10)));
        if(!unConnected)
        {
            unConnected = this->possAddUnitNode(msgSegment.at(10));
        }
        unConnected->updateBTLink(newBTLink);
        //
        unConnected->updateDBIndex(this->dbManager->updateUN(unConnected));
        //
        //
        UnitNode *objPtr(unConnected),
                 *sourceParent(objPtr->unTreeParent);
        int sourceChild(objPtr->unTreeParent->listChilde.indexOf(objPtr));

        this->involvingInStructure(unSender,
                                   unConnected);

        UnitNode *destinationParent(objPtr->unTreeParent);
        int destinationChild(objPtr->unTreeParent->listChilde.indexOf(objPtr));
        if(sourceParent != destinationParent)
        {
            emit this->moveUN(objPtr,
                              sourceParent,
                              sourceChild,
                              destinationParent,
                              destinationChild);
        }
        //

        this->possAddBTToListConnect(unSender,
                                     unConnected);



        // -->
        if(unSender->listConnect.size() == unSender->expectConnectCount)
        {//пора отправлять отложенную команду
         //т.к. LIST завершился

            //
            for(int i(0); i < unSender->childCount(); i++)
            {
                if(!unSender->listConnect.contains(unSender->child(i)))
                {
                    this->extractionFromStructure(unSender->child(i));
                    i--;
                }
            }
            //

            if(this->checkedCommunication.unMaster &&
               this->checkedCommunication.unSlave)
            {
                if(DutyMode == this->workMode())
                {
                    for(int i(0), n(unSender->listConnect.size()); i < n; i++)
                    {
                        if(unSender->listConnect[i] == this->listDefinitionSyperfloursBT.first())
                        {
                            //прописываем камеру
                            if((quint8)VK_KMDeviceType == (quint8)unSender->listConnect[i]->_typeUN())
                            {
                                QList<UnitNode*> wayToVK;
                                wayToVK.append(this->listBTUnitNode[0]);

                                for(int jj(0), mm(unSender->listConnect[i]->btLink.size()); jj < mm; jj++)
                                {
                                    wayToVK.append(this->possGetUnitNode(unSender->listConnect[i]->btLink.left(jj + 1)));
                                }

                                this->listCurrentUsedWayToVK.append(wayToVK);
                                this->listShortWayToVK.append(wayToVK);

                                QString msg("connected VK from way ");
                                for(int jj(0), mm(this->listCurrentUsedWayToVK.last().size()); jj < mm; jj++)
                                {
                                    if(0 == jj)
                                    {
                                        msg.append("(<");
                                    }
                                    else
                                    {
                                        msg.append(">|<");
                                    }
                                    msg.append(QString(this->listCurrentUsedWayToVK.last().at(jj)->_mac()));
                                }
                                msg.append(">)");

//                                emit insertNewMSG(this->dbManager->addNewMSGFromBT(0x00,
//                                                                 0x00,
//                                                                 msg.toUtf8(),
//                                                                 true));
//                                emit this->message(0x00,
//                                                   0x00,
//                                                   msg.toUtf8(),
//                                                   true);
                                emit this->connectedVK(wayToVK.last());
                            }
                            //


                            this->checkedCommunication.unMaster = nullptr;
                            this->checkedCommunication.unSlave = nullptr;

                            this->lastTxpowerM =
                            this->lastRssiM = unSender;

                            this->lastTxpowerChS =
                            this->lastRssiChS = unSender->listConnect[i]->btLink.right(1);

                            emit this->ssPostponedCommandTXPOWER(_dt_);
                            return;
                        }
                    }

                    if(this->listDefinitionSyperfloursBT.size())
                    {
                        this->listDefinitionSyperfloursBT.removeFirst();
                    }

                    if(this->listDefinitionSyperfloursBT.size())
                    {
                        this->lastListM = this->lastCallM = this->listCurrentInterrogatedBT.first();
                        this->lastCallS = this->listDefinitionSyperfloursBT.first();

                        emit this->ssPostponedCommandCALL(_dt_);
                    }
                    else
                    {
                        if(this->listCurrentInterrogatedBT.size())
                        {
                            this->listCurrentInterrogatedBT.removeFirst();
                        }
                        if(this->listCurrentInterrogatedBT.size())
                        {
                            this->emitReInterrogateBT(_dt_);
                        }
                        else
                        {
                            //
                            this->setCurrentWorkMode(DutyMode);
                            //
//                            this->emitReInterrogateBT(_pt_);
                        }
                    }
                    return;
                }



                if(ConnectSubset == this->workMode())
                {
                    for(int i(0), n(unSender->listConnect.size()); i < n; i++)
                    {//поиск запрошенных подключениу -->
                        if(unSender->listConnect[i] == this->checkedCommunication.unSlave)
                        {

                            this->checkedCommunication.unMaster = nullptr;
                            this->checkedCommunication.unSlave = nullptr;

                            if(this->currentCheckedSubsetOfCommunication.size())
                            {
                                this->currentCheckedSubsetOfCommunication.removeFirst();
                            }
                            if(1 < this->currentCheckedSubsetOfCommunication.size())
                            {
                                this->lastListM = this->currentCheckedSubsetOfCommunication.first();

                                emit this->ssPostponedCommandLIST(_dt_);

                                return;
                            }
                            else
                            {
                                this->definitionHeard();
                                return;
                            }
                        }
                    }//поиск запрошенных подключениу <--

                    //совсем всё плохо call не прошёл или ещё хуже -->
                    this->checkedCommunication.unMaster = nullptr;
                    this->checkedCommunication.unSlave = nullptr;

                    unSender->expectConnect = 0;

                    if(this->listInitialSubset.size())
                    {
                        this->listInitialSubset.removeFirst();
                    }
                    this->detourTact();
                    return;
                    //совсем всё плохо call не прошёл или ещё хуже <--
                }

                if(DefinitionHeard == this->workMode())
                {
                    for(int i(0), n(unSender->listConnect.size()); i < n; i++)
                    {//поиск запрошенных подключениу -->
                        if(unSender->listConnect[i] == this->checkedCommunication.unSlave)
                        {
                            this->checkedCommunication.unMaster = nullptr;
                            this->checkedCommunication.unSlave = nullptr;
                            this->lastTxpowerM =
                            this->lastRssiM = unSender;

                            this->lastTxpowerChS =
                            this->lastRssiChS =  unSender->listConnect[i]->btLink.right(1);


                            emit this->ssPostponedCommandTXPOWER(_dt_);

                            return;
                        }
                    }

                }

                if(ConnectVK == this->workMode())
                {
                    for(int i(0), n(unSender->listConnect.size()); i < n; i++)
                    {//поиск запрошенных подключениу -->
                        if(unSender->listConnect[i] == this->checkedCommunication.unSlave)
                        {
//                            //
//                            this->request0A(unSender->listConnect[i]);
//                            //

                            this->checkedCommunication.unMaster = nullptr;
                            this->checkedCommunication.unSlave = nullptr;

                            if(this->currentCheckedWayVK.size())
                            {
                                this->currentCheckedWayVK.removeFirst();
                            }
                            if(1 < this->currentCheckedWayVK.size())
                            {
                                this->lastListM = this->currentCheckedWayVK.first();

                                emit this->ssPostponedCommandLIST(_dt_);

                                return;
                            }
                            else
                            {//закончилось успешно

                                this->listCurrentUsedWayToVK.append(this->currentWayVK);

                                QString msg("connected VK from way ");
                                for(int j(0), m(this->listCurrentUsedWayToVK.last().size()); j < m; j++)
                                {
                                    if(0 == j)
                                    {
                                        msg.append("(<");
                                    }
                                    else
                                    {
                                        msg.append(">|<");
                                    }
                                    msg.append(QString(this->listCurrentUsedWayToVK.last().at(j)->_mac()));
                                }
                                msg.append(">)");

//                                emit insertNewMSG(this->dbManager->addNewMSGFromBT(0x00,
//                                                                 0x00,
//                                                                 msg.toUtf8(),
//                                                                 true));
//                                emit this->message(0x00,
//                                                   0x00,
//                                                   msg.toUtf8(),
//                                                   true);
                                emit this->connectedVK(this->currentWayVK.last());
                                //

                                //переход к множеству с другой камерой -->
                                for(int j(0); j < this->listInitialWayToVK.size();)
                                {
                                    if(this->listInitialWayToVK.at(j) == this->currentWayVK ||
                                       this->listInitialWayToVK.at(j).last() == this->currentWayVK.last())
                                    {
                                        this->listInitialWayToVK.removeAt(j);
                                    }
                                    else
                                    {
                                        j++;
                                    }
                                }
                                if(this->listInitialWayToVK.size())
                                {
                                    //попробовать другой путь
                                    this->connectOptimalWayToVK(this->listInitialWayToVK.first());
                                    return;
                                }
                                else
                                {
                                    this->setCurrentWorkMode(DutyMode);
                                    //конец подключения камер
                                }
                                //переход к множеству с другой камерой <--
                                return;
                            }
                        }
                    }//поиск запрошенных подключениу <--

                    //совсем всё плохо call не прошёл или ещё хуже -->
                    this->checkedCommunication.unMaster = nullptr;
                    this->checkedCommunication.unSlave = nullptr;
                    unSender->expectConnect = 0;

                    //перейти к ругому пути

                    return;
                    //совсем всё плохо call не прошёл или ещё хуже <--
                }
            }
            else
            {
                if(ConnectSubset == this->workMode())
                {
                    this->listSuperfluous.clear();
                    for(int i(0), n(unSender->listConnect.size()); i < n; i++)
                    {//поиск не нужных подключениу -->
                        if(unSender->listConnect[i] != this->currentCheckedSubsetOfCommunication.at(1))
                        {
                            this->listSuperfluous.append(unSender->listConnect[i]);
                        }
                    }//поиск не нужных подключениу <--


                    if(this->listSuperfluous.size())
                    {//удаление не нужных подключениу -->
                        this->lastKillM = unSender;
                        this->killSuperfluous();
                        return;
                    }//удаление не нужных подключениу <--
                    else
                    {//проверка на наличие искомого соединения -->
                        if(unSender->listConnect.size())
                        {//тот самый частный случай когда подключен только один и нужный
                            if(this->currentCheckedSubsetOfCommunication.size())
                            {
                                this->currentCheckedSubsetOfCommunication.removeFirst();
                            }
                            if(1 < this->currentCheckedSubsetOfCommunication.size())
                            {
                                this->lastListM = this->currentCheckedSubsetOfCommunication.first();
                                emit this->ssPostponedCommandLIST(_dt_);

                                return;
                            }
                            else
                            {
                                this->definitionHeard();
                                return;
                            }
                        }
                        else
                        {//подключений вообще нет
                            this->requestINQUIRY(this->currentCheckedSubsetOfCommunication.first());
                            return;
                        }

                    }//проверка на наличие искомого соединения <--

                }

                if(DefinitionHeard == this->workMode())
                {
                    this->listSuperfluous.clear();
                    for(int i(0), n(unSender->listConnect.size()); i < n; i++)
                    {//поиск имеющихся подключениу -->
                        this->listSuperfluous.append(unSender->listConnect[i]);
                    }//поиск имеющихся подключениу <--

                    if(this->listSuperfluous.size())
                    {//удаление не нужных подключениу -->
                        this->lastKillM = unSender;
                        this->killSuperfluous();
                        return;
                    }//удаление не нужных подключениу <--
                    else
                    {//поиск возможных подключений -->
                        this->lastListM = this->currentSubset.last();

                        emit this->ssPostponedCommandINQUIRY(_dt_);

                        return;
                    }//поиск возможных подключений <--
                }

                if(ConnectVK == this->workMode())
                {
                    if(unSender->listConnect.size())
                    {//тот самый частный случай когда подключен только один и нужный
                        for(int i(0), n(unSender->listConnect.size()); i < n; i++)
                        {//поиск искомых подключений -->
                            if(unSender->listConnect[i] == this->currentCheckedWayVK.at(1))
                            {
//                                //
//                                this->request0A(unSender->listConnect[i]);
//                                //
                                if(this->currentCheckedWayVK.size())
                                {
                                    this->currentCheckedWayVK.removeFirst();
                                }
                                if(1 < this->currentCheckedWayVK.size())
                                {
                                    this->lastListM = this->currentCheckedWayVK.first();
                                    emit this->ssPostponedCommandLIST(_dt_);

                                    return;
                                }
                                else
                                {//закончилось успешно
                                    this->listCurrentUsedWayToVK.append(this->currentWayVK);

                                    QString msg("connected VK from way ");
                                    for(int j(0), m(this->listCurrentUsedWayToVK.last().size()); j < m; j++)
                                    {
                                        if(0 == j)
                                        {
                                            msg.append("(<");
                                        }
                                        else
                                        {
                                            msg.append(">|<");
                                        }
                                        msg.append(QString(this->listCurrentUsedWayToVK.last().at(j)->_mac()));
                                    }
                                    msg.append(">)");

//                                    emit insertNewMSG(this->dbManager->addNewMSGFromBT(0x00,
//                                                                     0x00,
//                                                                     msg.toUtf8(),
//                                                                     true));
//                                    emit this->message(0x00,
//                                                       0x00,
//                                                       msg.toUtf8(),
//                                                       true);
                                    emit this->connectedVK(this->currentWayVK.last());


                                    //
                                    //переход к множеству с другой камерой -->
                                    for(int j(0); j < this->listInitialWayToVK.size();)
                                    {
                                        if(this->listInitialWayToVK.at(j) == this->currentWayVK ||
                                           this->listInitialWayToVK.at(j).last() == this->currentWayVK.last())
                                        {
                                            this->listInitialWayToVK.removeAt(j);
                                        }
                                        else
                                        {
                                            j++;
                                        }
                                    }
                                    if(this->listInitialWayToVK.size())
                                    {
                                        //попробовать другой путь
                                        this->connectOptimalWayToVK(this->listInitialWayToVK.first());
                                        return;
                                    }
                                    else
                                    {
                                        this->setCurrentWorkMode(DutyMode);
                                        //конец подключения камер
                                    }
                                    //переход к множеству с другой камерой <--
                                    return;
                                }
                            }
                        }//поиск искомых подключений <--

                        this->requestINQUIRY(this->currentCheckedWayVK.first());
                        return;
                    }
                    else
                    {//подключений вообще нет
                        this->requestINQUIRY(this->currentCheckedWayVK.first());
                        return;
                    }
                }
            }
        }
        // <--
    }
}

void BTMsgManager::analyzerTXPOWER(UnitNode* unSender,
                                   QList<QByteArray> msgSegment) /*noexcept*/
{
#ifdef QT_DEBUG
    qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " [" << unSender->btLink.toHex() << "] " << "  in<< " << ProxyMetod().sumQListQByteArray(msgSegment);
#endif
    UnitNode *unSlave(this->possGetUnitNode(msgSegment.at(1)));
//    emitInsertNewMSG(unSender,
//                     0xFF,
//                     ProxyMetod().sumQListQByteArray(msgSegment),
//                     true,
//                     Connect_TypeGroupMSG);


    this->setTxPower(unSender,
                     unSlave,
                     QString(msgSegment.at(2)).toInt());

    this->lastRssiM = unSender;
    if(this->lastRssiChS.isEmpty() && unSlave)
    {
        this->lastRssiChS.append(unSlave->btLink.right(1));
    }

    emit this->ssPostponedCommandRSSI(_dt_);
}

void BTMsgManager::analyzerRSSI(UnitNode* unSender,
                                QList<QByteArray> msgSegment) /*noexcept*/
{
#ifdef QT_DEBUG
    qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " [" << unSender->btLink.toHex() << "] " << "  in<< " << ProxyMetod().sumQListQByteArray(msgSegment);
#endif
//    emitInsertNewMSG(unSender,
//                     0xFF,
//                     ProxyMetod().sumQListQByteArray(msgSegment),
//                     true,
//                     Connect_TypeGroupMSG);

    this->setRSSI(unSender,
                  this->possGetUnitNode(msgSegment.at(1)),
                  QString(msgSegment.at(2)).toInt());
    auto unS(this->possGetUnitNode(msgSegment.at(1)));
    if(unS)
    {
        unS->updateRssi(QString(msgSegment.at(2)).toInt() * (-1));
    }

    if(DutyMode == this->workMode())
    {
        if(this->listDefinitionSyperfloursBT.size())
        {
            this->listDefinitionSyperfloursBT.removeFirst();
        }

        if(this->listDefinitionSyperfloursBT.size())
        {
            this->lastListM = this->lastCallM = unSender;
            this->lastCallS = this->listDefinitionSyperfloursBT.first();

            emit this->ssPostponedCommandCALL(_dt_);
        }
        else
        {
            if(this->listCurrentInterrogatedBT.size())
            {
                this->listCurrentInterrogatedBT.removeFirst();
            }

            if(this->listCurrentInterrogatedBT.size())
            {
                this->emitReInterrogateBT(_dt_);
            }
            else
            {
                //
                this->setCurrentWorkMode(DutyMode);
                //
            }
        }
    }

    if(DefinitionHeard == this->workMode())
    {
        this->lastKillM = unSender;
        this->lastKillS = this->possGetUnitNode(msgSegment.at(1));

        emit this->ssPostponedCommandKILL(_dt_);
    }
}

void BTMsgManager::analyzerSYNTAX(UnitNode* unSender,
                                  QList<QByteArray> msgSegment) /*noexcept*/
{
#ifdef QT_DEBUG
    qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " [" << unSender->btLink.toHex() << "] " << "  in<< " << ProxyMetod().sumQListQByteArray(msgSegment);
#endif

    if(3 > this->countRepeatMSG)
    {
        if(!this->msgToSend.isEmpty())
        {
            emit this->newMSGToSend();
            if((quint8)0xA5 == (quint8)this->msgToSend.at(0))
            {
                emitInsertNewMSG(this->possGetUnitNode(this->btLinkMSGToSend),
                                 this->msgToSend.at(8),
                                 this->msgToSend,
                                 false,
                                 Error_TypeGroupMSG);
            }
            else
            {
                emitInsertNewMSG(this->possGetUnitNode(this->btLinkMSGToSend),
                                 0x00,
                                 this->msgToSend,
                                 false,
                                 Error_TypeGroupMSG);
            }
        }
        this->countRepeatMSG++;
    }
    else
        this->countRepeatMSG = 0;
}

void BTMsgManager::newKmMSG(QByteArray btLink,
                            QByteArray msg) /*noexcept*/
{
    emit this->ssOffStatusKMExchange(_pt_/*30000*/);
#ifdef QT_DEBUG
    if(100 > msg.size())
    {
        qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " KmMSG [" << msg.toHex() << "]";
    }
    else
    {
        qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " KmMSG [" << msg.left(10).toHex() << "..." << msg.size() << "..." << msg.right(10).toHex() << "]";
    }
#endif


    //поиск KM устройства по id -->
    UnitNode *unSender(this->getUnitNode(msg.mid(2, 5)));
    if(unSender)
    {
        unSender->updateVoltage(msg.at(7));
        unSender->updateBTLink(btLink);
        unSender->updateFlagHidden(false);
        if((quint8)0xC3 != (quint8)msg.at(8))
        {
            emitInsertNewMSG(unSender,
                             msg.at(8),
                             msg,
                             true,
                             Other_TypeGroupMSG);
        }

        switch((quint8)msg.at(8))
        {
            case (quint8)0x0A:
                {
                    this->analyzer0A(unSender/*,
                                     msg*/);
                    return;
                }
            case (quint8)0xAA:
                {
//                    this->request0A(unSender);
                    this->analyzerAA(unSender,
                                     msg);
                    return;
                }
            case (quint8)0xC1:
                {
                    this->request0A(unSender);
                    emit this->ssPostponedCommandC0Stop();
                    emit this->ssPostponedCommandB1Stop();


                    this->analyzerC1(unSender,
                                     msg);
                    return;
                }
            case (quint8)0xC3:
                {
                    emit this->ssPostponedCommandB3Stop();

                    if(Undefined != this->workMode() &&
                       DutyMode != this->workMode())
                        return;

                    emit this->ssPostponedCommandC0Stop();

                    this->analyzerC3(unSender,
                                     msg);
                    return;
                }
            case (quint8)0xC5:
                {
                    this->analyzerC5(unSender/*,
                                     msg*/);
                    return;
                }
            case (quint8)0x3F:
                {
                    this->analyzer3F(unSender,
                                     msg);
                    return;
                }
            case (quint8)0xE1:
                {
                    emit this->ssPostponedCommandC0Start(500, unSender);

                    this->analyzerE1(unSender/*,
                                     msg*/);
                    return;
                }
            default:
                {
                    return;
                }
        }

    }
    else
    {
        if(btLink.isEmpty())
            this->requestSET();
        return;
    }
    //поиск KM устройства по пути <--
}

//ф-и формирование сети -->

WorkMode BTMsgManager::workMode() /*noexcept*/
{
    return this->currentWorkMode;
}

void BTMsgManager::setCurrentWorkMode(WorkMode mode) /*noexcept*/
{
    this->definitionInitiator =
    this->lastCallM =
    this->lastCallS =
    this->lastKillM =
    this->lastKillS =
    this->lastListM =
    this->lastInquiryM =
    this->lastTxpowerM =
    this->lastRssiM = nullptr;
    this->lastTxpowerChS.clear();
    this->lastRssiChS.clear();
    this->checkedCommunication.unMaster = nullptr;
    this->checkedCommunication.unSlave = nullptr;

    this->currentWorkMode = mode;
#ifdef QT_DEBUG
    qDebug() << "/**** " << (qint32)mode << "****/";
#endif
    emit this->currentWorkModeChanged(mode);
    if(DutyMode == mode)
    {
        this->emitReInterrogateBT(_pt_);
    }
}

void BTMsgManager::buildNewTopology() /*noexcept*/
{
#ifdef QT_DEBUG
    qDebug() << "constructContiguityMatrix() -->";
#endif

    this->countRepeatMSG = 0;
    this->checkedCommunication.unMaster = nullptr;
    this->checkedCommunication.unSlave = nullptr;
    this->listCommunicationItem.clear();
    this->listInitialSubset.clear();
    this->usedCommunicationItem.clear();
    this->listShortWayToVK.clear();
//    this->listShortWayToCCP.clear();

    UnitNode* base(nullptr);

    for(int i(0), n(this->listBTUnitNode.size()); i < n; i++)
    {
        if(this->listBTUnitNode.at(i)->flag_connection &&
           this->listBTUnitNode.at(i)->btLink.isEmpty())
        {
            base = this->listBTUnitNode.at(i);
        }
    }

    if(base)
    {
        this->listCommunicationItem.clear();
        this->listInitialSubset.clear();

        QList<UnitNode*> firstSubset;
        firstSubset.append(base);
        this->listInitialSubset.append(firstSubset);

        this->detourTact();

#ifdef QT_DEBUG
        qDebug() << "constructContiguityMatrix() <--";
#endif
        return;
    }

#ifdef QT_DEBUG
    qDebug() << "constructContiguityMatrix() <--";
#endif
}

void BTMsgManager::constructCurrentTopology() /*noexcept*/
{
#ifdef QT_DEBUG
    qDebug() << "constructCurrentTopology() -->";
#endif
    this->checkedCommunication.unMaster = nullptr;
    this->checkedCommunication.unSlave = nullptr;
    this->listCommunicationItem.clear();
    this->listInitialSubset.clear();
    this->usedCommunicationItem.clear();
    this->listShortWayToVK.clear();

    QByteArray adr;
    UnitNode* base(this->possGetUnitNode(adr));
    if(base)
    {
        this->listBTUnitNode.clear();
        this->listBTUnitNode.append(base);

        this->listCommunicationItem.clear();
        this->listInitialSubset.clear();

#ifdef QT_DEBUG
        qDebug() << "constructCurrentTopology() <--";
#endif
        return;
    }

#ifdef QT_DEBUG
    qDebug() << "constructCurrentTopology() <--";
#endif
}

void BTMsgManager::detourTact() /*noexcept*/
{
#ifdef QT_DEBUG
    qDebug() << "detourTact() -->";
#endif

    this->lastCallM =
    this->lastCallS =
    this->lastKillM =
    this->lastKillS =
    this->lastListM = nullptr;
    this->checkedCommunication.unMaster = nullptr;
    this->checkedCommunication.unSlave = nullptr;

    if(this->listInitialSubset.size())
    {
        this->connectSubset(this->listInitialSubset.first());
    }
    else
    {
        this->setCurrentWorkMode(Undefined);

#ifdef QT_DEBUG
        qDebug() << "FINISH";
#endif
//21 11 14 -->
//        QList<QList<UnitNode*>> revers(this->listShortWayToVK);
//        this->listShortWayToVK.clear();
////        this->listShortWayToCCP.clear();
//        for(int i(0), n(revers.size()); i < n; i++)
//        {
//            this->listShortWayToVK.prepend(revers.at(i));
//        }
//21 11 14 <--
        for(int i(0), n(this->listShortWayToVK.size()); i < n; i++)
        {
            QString msg("[%1] qality(");
            msg = msg.arg(i + 1);
            float max = 0.0;
            for(int j(0), m(this->definitionSubset(this->listShortWayToVK.at(i)).size()); j < m; j++)
            {
                if(0 == j)
                {
                    msg.append("%1");
                }
                else
                {
                    msg.append(", %1");
                }
                msg = msg.arg(this->definitionSubset(this->listShortWayToVK.at(i)).at(j));
                max = qMax(this->definitionSubset(this->listShortWayToVK.at(i)).at(j), max);
            }
            msg.append(" <%1>)");
            msg = msg.arg(max);

            for(int j(0), m(this->listShortWayToVK.at(i).size()); j < m; j++)
            {
                if(0 == j)
                {
                    msg.append(" (<");
                }
                else
                {
                    msg.append(">|<");
                }
                msg.append(QString(this->listShortWayToVK.at(i).at(j)->_mac()));
            }
            msg.append(">)");

            emitInsertNewMSG(0x00,
                             0x00,
                             msg.toUtf8(),
                             true,
                             System_TypeGroupMSG);
#ifdef QT_DEBUG
            qDebug() << msg;
#endif
        }

        this->connectOptimalWayToVK();
        //построение путей
    }
#ifdef QT_DEBUG
    qDebug() << "detourTact() <--";
#endif
}

QList<float> BTMsgManager::definitionSubset(QList<UnitNode*> subset) /*noexcept*/
{
    QList<float> listQuality;
    for(int i(0), n(subset.size() - 1); i < n; i++)
    {
        for(int j(0), m(this->listCommunicationItem.size()); j < m; j++)
        {
            if((this->listCommunicationItem.at(j).unSlave == subset.at(i) &&
                this->listCommunicationItem.at(j).unMaster == subset.at(i + 1)) ||
               (this->listCommunicationItem.at(j).unMaster == subset.at(i) &&
                this->listCommunicationItem.at(j).unSlave == subset.at(i + 1)))
            {
                if(-45 < this->listCommunicationItem.at(j).rssi &&
                   5 > this->listCommunicationItem.at(j).txPower)
                {
                    listQuality.append(0.0);
                    break;
                }
                if(-45 < this->listCommunicationItem.at(j).rssi &&
                   10 > this->listCommunicationItem.at(j).txPower)
                {
                    listQuality.append(1.0);
                    break;
                }
                if(-45 < this->listCommunicationItem.at(j).rssi &&
                   20 > this->listCommunicationItem.at(j).txPower)
                {
                    listQuality.append(2.0);
                    break;
                }
                if(-60 > this->listCommunicationItem.at(j).rssi &&
                   19 < this->listCommunicationItem.at(j).txPower)
                {
                    listQuality.append(4.0);
                    break;
                }
                if(-40 > this->listCommunicationItem.at(j).rssi &&
                   19 < this->listCommunicationItem.at(j).txPower)
                {
                    listQuality.append(3.0);
                    break;
                }
                if(-45 < this->listCommunicationItem.at(j).rssi)
                {
                    listQuality.append(3.0);
                    break;
                }
                if(-15 < this->listCommunicationItem.at(j).txPower)
                {
                    listQuality.append(3.0);
                    break;
                }
            }
        }
    }
    return listQuality;
}


void BTMsgManager::connectSubset(QList<UnitNode*> subset) /*noexcept*/
{
#ifdef QT_DEBUG
    qDebug() << "connectSubset(" << subset << ") -->";
#endif
    this->currentCheckedSubsetOfCommunication.clear();
    this->currentSubset.clear();
    this->currentCheckedSubsetOfCommunication = this->currentSubset = subset;

    this->setCurrentWorkMode(ConnectSubset);

    if(1 == subset.size())
    {
        if(subset.first() == this->listBTUnitNode.first())
        {
            this->definitionHeard();
//            emit this->finishedConnectionCurrentSubset();
        }
    }
    if(1 < subset.size())
    {

        this->checkedCommunication.unMaster = nullptr;
        this->checkedCommunication.unSlave = nullptr;

        this->lastListM = this->currentCheckedSubsetOfCommunication.first();
        emit this->ssPostponedCommandLIST(_dt_);
    }
#ifdef QT_DEBUG
    qDebug() << "connectSubset() <--";
#endif
}

void BTMsgManager::postponedCommandCALL() /*noexcept*/
{
    this->requestCALL(this->lastCallM,
                      this->lastCallS);
}

void BTMsgManager::postponedCommandKILL() /*noexcept*/
{
    this->requestKILL(this->lastKillM,
                      this->lastKillS);
}

void BTMsgManager::postponedCommandLIST() /*noexcept*/
{
    this->requestLIST(this->lastListM);
}

void BTMsgManager::postponedCommandINQUIRY() /*noexcept*/
{
    this->requestINQUIRY(this->lastInquiryM);
}


void BTMsgManager::postponedCommandTXPOWER() /*noexcept*/
{
    this->requestTXPOWER(this->lastTxpowerM,
                         this->lastTxpowerChS);
}

void BTMsgManager::postponedCommandRSSI() /*noexcept*/
{
    this->requestRSSI(this->lastRssiM,
                      this->lastRssiChS);
}

void BTMsgManager::definitionHeard() /*noexcept*/
{
#ifdef QT_DEBUG
    qDebug() << "definitionHeard() -->";
#endif
    this->setCurrentWorkMode(DefinitionHeard);
    this->checkedCommunication.unMaster = nullptr;
    this->checkedCommunication.unSlave = nullptr;

    this->lastListM = this->currentSubset.last();
    emit this->ssPostponedCommandLIST(_dt_);
#ifdef QT_DEBUG
    qDebug() << "definitionHeard() <--";
#endif
}

void BTMsgManager::definitionListCallBT() /*noexcept*/
{
#ifdef QT_DEBUG
    qDebug() << "definitionListCallBT() -->";
#endif
    if(this->listCallBT.size())
    {
        this->lastCallM = this->definitionInitiator;
        this->lastCallS = this->listCallBT.first();
        this->lastListM = this->lastCallM;

        emit this->ssPostponedCommandCALL(_dt_);
    }
    else
    {
        this->updateInitialSubset();
        //финал
    }
#ifdef QT_DEBUG
    qDebug() << "definitionListCallBT() <--";
#endif
}

void BTMsgManager::updateInitialSubset() /*noexcept*/
{
#ifdef QT_DEBUG
    qDebug() << "updateInitialSubset() -->";
#endif

    this->setCurrentWorkMode(Undefined);

    for(int i(0), n(this->listInitialSubset.size()); i < n; i++)
    {
        for(int j(0), m(this->listInitialSubset.at(i).size() - 1); j < m; j++)
        {
            this->possAddUsedCommunicationItem(this->listInitialSubset.at(i).at(j),
                                               this->listInitialSubset.at(i).at(j + 1));
        }
    }



    //финал
    QList<UnitNode*> newElementSubset, oldSubset(this->listInitialSubset.first());
//    this->listInitialSubset.first();
    for(int i(0), n(this->listCommunicationItem.size()); i < n; i++)
    {
        if(this->listCommunicationItem.at(i).unMaster == oldSubset.last())
        {
            newElementSubset.append(this->listCommunicationItem.at(i).unSlave);
        }
        if(this->listCommunicationItem.at(i).unSlave == oldSubset.last())
        {
            newElementSubset.append(this->listCommunicationItem.at(i).unMaster);
        }
    }

//1# mac не должен повторяться
    for(int i(0), n(newElementSubset.size()); i < n; /*i++*/)
    {
        if(oldSubset.count(newElementSubset.at(i)))
        {
            n--;
            newElementSubset.removeAt(i);
        }
        else
        {
            i++;
        }
    }




//3# уникальность связи
    for(int i(0), n(newElementSubset.size()); i < n; /*i++*/)
    {
        if(this->possAddUsedCommunicationItem(oldSubset.last(),
                                              newElementSubset.at(i)))
        {
            i++;
        }
        else
        {
            n--;
            newElementSubset.removeAt(i);
        }
    }

    //добавление новых множеств
    for(int i(0), n(newElementSubset.size()); i < n; i++)
    {
//        QList<UnitNode*> newSubset(oldSubset);
        auto newSubset(oldSubset);
        newSubset.append(newElementSubset.at(i));
        this->listInitialSubset.append(newSubset);
    }
    if(this->listInitialSubset.size())
    {
        this->listInitialSubset.removeFirst();
    }

    //2# mac VK стоит только в конце.
    //у даляем все заканчивающиеся на VK из рассмотрения, но запоминаем как кротчайший путь.
    for(int i(0), n(this->listInitialSubset.size()); i < n; i++)
    {
        for(quint8 j(0), m(this->listBTUnitNode.size()); j < m; j++)
        {
            if(this->listInitialSubset.at(i).last() == this->listBTUnitNode[j] &&
               (quint8)VK_KMDeviceType == (quint8)this->listBTUnitNode[j]->_typeUN())
            {
                this->listShortWayToVK.append(this->listInitialSubset.at(i));
                this->listInitialSubset.removeAt(i);
                n--;
                i--;
                break;
            }
//            if(this->listInitialSubset.at(i).last() == this->listUnitNode.at(j).mac &&
//               "PKP_03" == this->listUnitNode.at(j).name.left(6))
//            {
//                this->listShortWayToCCP.append(this->listInitialSubset.at(i));
//                this->listInitialSubset.removeAt(i);
//                n--;
//                i--;
//                break;
//            }
        }
    }

    this->disconnectSubset();
#ifdef QT_DEBUG
    qDebug() << "updateInitialSubset() <--";
#endif
}

void BTMsgManager::disconnectSubset() /*noexcept*/
{
#ifdef QT_DEBUG
    qDebug() << "disconnectSubset() -->";
#endif
    this->listDisconnect.clear();

    this->setCurrentWorkMode(DisconnectSubset);

    this->listDisconnect = this->currentSubset.mid(0,
                                                   this->currentSubset.size() - this->currentCheckedSubsetOfCommunication.size() + 2);

    if(1 < this->listDisconnect.size())
    {
        this->lastKillM = this->listDisconnect.at(this->listDisconnect.size() - 2);
        this->lastKillS = this->listDisconnect.at(this->listDisconnect.size() - 1);

        emit this->ssPostponedCommandKILL(_dt_);
    }
    else
    {
        this->detourTact();
    }

#ifdef QT_DEBUG
    qDebug() << "disconnectSubset() <--";
#endif
}

QByteArray BTMsgManager::add0xFFtoBTLink(QByteArray btLink) /*noexcept*/
{
    if(btLink.size())
    {
        if((quint8)0xFF != (quint8)btLink.at(btLink.size() - 1))
        {
            btLink.append((quint8)0xFF);
        }
    }
    else
    {
        btLink.append((quint8)0xFF);
    }
    return btLink;
}

void BTMsgManager::killSuperfluous() /*noexcept*/
{
    this->checkedCommunication.unMaster = this->lastListM = this->lastKillM;
    this->checkedCommunication.unSlave = nullptr;
    this->requestKILL(this->lastKillM,
                      this->listSuperfluous.first());

    this->lastListM = this->lastKillM;
}

void BTMsgManager::connectSpareWayToVK() /*noexcept*/
{
    if(this->listSpareWayToVK.size())
    {
        this->listInitialWayToVK = this->listSpareWayToVK;
        this->connectOptimalWayToVK(this->listInitialWayToVK.first());
    }
}

void BTMsgManager::connectOptimalWayToVK() /*noexcept*/
{
    if(this->listShortWayToVK.size())
    {
        this->listCurrentUsedWayToVK.clear();
        this->listInitialWayToVK = this->listShortWayToVK;
        this->connectOptimalWayToVK(this->listInitialWayToVK.first());
    }
    else
    {
        QString msg("Builded network");
        this->setCurrentWorkMode(DutyMode);
        emitInsertNewMSG(0x00,
                         0x00,
                         msg.toUtf8(),
                         true,
                         System_TypeGroupMSG);
    }
}

void BTMsgManager::connectOptimalWayToVK(QList<UnitNode*> subset) /*noexcept*/
{
//    qDebug() << "connectVK(" << subset << ") -->";
    this->currentWayVK = this->currentCheckedWayVK = subset;

    this->setCurrentWorkMode(ConnectVK);

    this->checkedCommunication.unMaster = nullptr;
    this->checkedCommunication.unSlave = nullptr;

    this->lastListM = this->currentCheckedWayVK.first();
    emit this->ssPostponedCommandLIST(_dt_);
//    qDebug() << "connectSubset() <--";
}

void BTMsgManager::oneReInterrogateBT(UnitNode *unSender) /*noexcept*/
{
    if(this->statusKMExchange)
    {
//        qDebug() <<"bt signal 59 -->";
//        this->emitReInterrogateBT(15000);
        this->emitReInterrogateBT(_pt_);
//        qDebug() <<"bt signal 59 <--";
        return;
    }

    if(this->listCurrentInterrogatedBT.size())
    {
        this->lastInquiryM = this->listCurrentInterrogatedBT.first();
        emit this->ssPostponedCommandINQUIRY(_dt_);
    }
    else
    {
        this->listCurrentInterrogatedBT.clear();
        this->listInterrogatedBT.clear();

        if(((quint8)RT_KMDeviceType == (quint8)unSender->typeUN) &&
            unSender->flag_connection)
        {
            this->listInterrogatedBT.prepend(unSender);
        }

        this->listCurrentInterrogatedBT = this->listInterrogatedBT;
        this->emitReInterrogateBT(_dt_);
    }
}

void BTMsgManager::reInterrogateBT() /*noexcept*/
{
    if(this->statusKMExchange)
    {
//        qDebug() <<"bt signal 59 -->";
//        this->emitReInterrogateBT(15000);
        this->emitReInterrogateBT(_pt_);
//        qDebug() <<"bt signal 59 <--";
        return;
    }

    if(this->listCurrentInterrogatedBT.size())
    {
        this->lastInquiryM = this->listCurrentInterrogatedBT.first();
        emit this->ssPostponedCommandINQUIRY(_dt_);
    }
    else
    {
        this->listCurrentInterrogatedBT.clear();
        this->listInterrogatedBT.clear();
        for(int i(0); i < this->listBTUnitNode.size(); i++)
        {
            if(((quint8)RT_KMDeviceType == (quint8)this->listBTUnitNode.at(i)->_typeUN()) &&
               this->listBTUnitNode.at(i)->flag_connection)
            {
                this->listInterrogatedBT.prepend(this->listBTUnitNode.at(i));
            }
        }
        this->listCurrentInterrogatedBT = this->listInterrogatedBT;
        this->emitReInterrogateBT(_dt_);
    }
}

 void BTMsgManager::offStatusKMExchange() /*noexcept*/
 {
     this->statusKMExchange = false;
 }

void BTMsgManager::beginDutyMode() /*noexcept*/
{
    //
    this->setCurrentWorkMode(DutyMode);
    //
    this->emitReInterrogateBT(_pt_);
}

void BTMsgManager::requestLIST(UnitNode* unSender,
                               bool postponed) /*noexcept*/
{
    if(unSender)
    {
//        this->request0A(unSender); //хуй

        QByteArray msg("LIST");
        unSender->lastMSGToSend = msg;
        this->msgToSend = msg;
        this->btLinkMSGToSend = this->add0xFFtoBTLink(unSender->btLink);
        if(!postponed)
        {
            emit this->newMSGToSend();
            emitInsertNewMSG(unSender,
                             0xFF,
                             this->msgToSend,
                             false,
                             Connect_TypeGroupMSG);
        }
    }
}

void BTMsgManager::requestINQUIRY(UnitNode* unSender,
                                  bool postponed) /*noexcept*/
{
    if(unSender)
    {
//        this->request0A(unSender); //хуй

        QByteArray msg("INQUIRY 6 NAME");
        unSender->lastMSGToSend = msg;
        this->msgToSend = msg;
        this->btLinkMSGToSend = this->add0xFFtoBTLink(unSender->btLink);
        if(!postponed)
        {
            emit this->newMSGToSend();
            emitInsertNewMSG(unSender,
                             0xFF,
                             this->msgToSend,
                             false,
                             Connect_TypeGroupMSG);
        }
    }
}


void BTMsgManager::requestCALL(UnitNode* unMaster,
                               UnitNode* unSlave,
                               bool postponed) /*noexcept*/
{
    if(unMaster && unSlave)
    {
        this->lastCallM = this->checkedCommunication.unMaster = unMaster;
        this->lastCallS = this->checkedCommunication.unSlave = unSlave;

        QByteArray msg;
        msg.append("CALL ");
        msg.append(unSlave->_mac());
        msg.append(" 1101 RFCOMM");
        unMaster->lastMSGToSend = msg;
        this->msgToSend = msg;

        this->btLinkMSGToSend = this->add0xFFtoBTLink(unMaster->btLink);

        if(!postponed)
        {
            emit this->newMSGToSend();
            emitInsertNewMSG(unMaster,
                             0xFF,
                             this->msgToSend,
                             false,
                             Connect_TypeGroupMSG);
        }
    }
}

void BTMsgManager::requestCLOSE(UnitNode* unMaster,
                                QByteArray ch,
                                bool postponed) /*noexcept*/
{
    if(unMaster)
    {
        QByteArray msg;
        msg.append("CLOSE ");
        QString str("%1");
        str = str.arg((quint8)ch.at(0));
        msg.append(str.toUtf8());
        unMaster->lastMSGToSend = msg;
        this->msgToSend = msg;
        this->btLinkMSGToSend = this->add0xFFtoBTLink(unMaster->btLink);
        if(!postponed)
        {
            emit this->newMSGToSend();
            emitInsertNewMSG(unMaster,
                             0xFF,
                             this->msgToSend,
                             false,
                             Connect_TypeGroupMSG);
        }
    }
}

void BTMsgManager::requestTXPOWER(UnitNode* unMaster,
                                  QByteArray ch,
                                  bool postponed) /*noexcept*/
{
    if(unMaster)
    {
        QByteArray msg;
        msg.append("TXPOWER ");
        QString str("%1");
        str = str.arg((quint8)ch.at(0));
        msg.append(str.toUtf8());
        unMaster->lastMSGToSend = msg;
        this->msgToSend = msg;
        this->btLinkMSGToSend = this->add0xFFtoBTLink(unMaster->btLink);
        if(!postponed)
        {
            emit this->newMSGToSend();
            emitInsertNewMSG(unMaster,
                             0xFF,
                             this->msgToSend,
                             false,
                             Connect_TypeGroupMSG);
        }
    }
}

void BTMsgManager::requestRSSI(UnitNode* unMaster,
                               QByteArray ch,
                               bool postponed) /*noexcept*/
{
    if(unMaster)
    {
        QByteArray msg;
        msg.append("RSSI ");
        QString str("%1");
        str = str.arg((quint8)ch.at(0));
        msg.append(str.toUtf8());
        unMaster->lastMSGToSend = msg;
        this->msgToSend = msg;
        this->btLinkMSGToSend = this->add0xFFtoBTLink(unMaster->btLink);
        if(!postponed)
        {
            emit this->newMSGToSend();
            emitInsertNewMSG(unMaster,
                             0xFF,
                             this->msgToSend,
                             false,
                             Connect_TypeGroupMSG);
        }
    }
}

void BTMsgManager::requestSET(UnitNode* unSender,
                              bool postponed) /*noexcept*/
{
    if(unSender)
    {
        QByteArray msg("SET");
        unSender->lastMSGToSend = msg;
        this->msgToSend = msg;
        this->btLinkMSGToSend = this->add0xFFtoBTLink(unSender->btLink);
        if(!postponed)
        {
            emit this->newMSGToSend();
            emitInsertNewMSG(unSender,
                             0xFF,
                             this->msgToSend,
                             false,
                             Other_TypeGroupMSG);
        }
    }
}

void BTMsgManager::requestSET(bool postponed) /*noexcept*/
{
//    this->request0A(unSender);

    QByteArray msg("SET");
    this->msgToSend = msg;
    this->btLinkMSGToSend.clear();
    this->btLinkMSGToSend.append((quint8)0xFF);
    if(!postponed)
    {
        emit this->newMSGToSend();
        if(this->listBTUnitNode.size())
        {
            emitInsertNewMSG(this->listBTUnitNode.first(),
                             0xFF,
                             this->msgToSend,
                             false,
                             Other_TypeGroupMSG);
        }
        else
        {
            emitInsertNewMSG(0x00,
                             0xFF,
                             this->msgToSend,
                             false,
                             Other_TypeGroupMSG);
        }
    }
}

void BTMsgManager::requestAT(UnitNode* unSender,
                              bool postponed) /*noexcept*/
{
    if(unSender)
    {
        QByteArray msg("AT");
        this->msgToSend = msg;
        this->btLinkMSGToSend = this->add0xFFtoBTLink(unSender->btLink);
        if(!postponed)
        {
            emit this->newMSGToSend();
        }
        return;
    }
    else
    {
        QByteArray msg("AT");
        this->msgToSend = msg;
        this->btLinkMSGToSend.clear();
        this->btLinkMSGToSend.append((quint8)0xFF);
        if(!postponed)
        {
            emit this->newMSGToSend();
        }
    }

}

void BTMsgManager::requestKILL(bool postponed) /*noexcept*/

{
    this->requestKILL(this->lastCallM,
                      this->lastCallS,
                      postponed);
}

void BTMsgManager::requestKILL(UnitNode* unMaster,
                               UnitNode* unSlave,
                               bool postponed) /*noexcept*/

{
    if(unMaster && unSlave)
    {
//        this->request0A(unMaster); //хуй

        QByteArray msg;
        msg.append("KILL ");
        msg.append(unSlave->_mac());
        unMaster->lastMSGToSend = msg;
        this->msgToSend = msg;
        this->btLinkMSGToSend = this->add0xFFtoBTLink(unMaster->btLink);
        if(!postponed)
        {
            emit this->newMSGToSend();
            emitInsertNewMSG(unMaster,
                             0xFF,
                             this->msgToSend,
                             false,
                             Connect_TypeGroupMSG);
        }
    }
}

//ф-и формирование сети <--

QByteArray BTMsgManager::setCRC8(QByteArray pattern) /*noexcept*/
{
    int n(pattern.size() - 1);
    pattern[n] = (quint8)0x00;
    for(int i(1); i < n; i++)
    {
        pattern[n] = CrcTable.at((quint8)(pattern.at(n) ^ (quint8)pattern.at(i))); // Табличный метод
    }
    return pattern;
}


QByteArray BTMsgManager::setByteStaffing(QByteArray pattern) /*noexcept*/
{
    for(int i(1); i < pattern.size(); i++)
    {
        if((quint8)0x1B == (quint8)pattern[i])
        {
            pattern.insert(i, (quint8)0x3B);
            pattern.insert(i, (quint8)0x1B);
            pattern.remove(i + 2, 1);
        }
        if((quint8)0xA5 == (quint8)pattern[i])
        {
            pattern.insert(i, (quint8)0xC5);
            pattern.insert(i, (quint8)0x1B);
            pattern.remove(i + 2, 1);
        }
        if((quint8)0xBF == (quint8)pattern[i])
        {
            pattern.insert(i, (quint8)0xDF);
            pattern.insert(i, (quint8)0x1B);
            pattern.remove(i + 2, 1);
        }

    }
    return pattern;
}

void BTMsgManager::requestBlockShot(UnitNode* unSender) /*noexcept*/
{
    if(unSender && unSender->listIMGVK.size())
    {
        unSender->currentNumShotVK = unSender->listIMGVK.first().numShot;
        unSender->currentNumBlockVK = 1;
        unSender->beginVK = 0;
        unSender->sizeBlockVK = unSender->listIMGVK.first().releaseSize / MaxCountBlock;

        emit this->ssPostponedCommandB3Start(15000,
                                             unSender);
        emit this->ssPostponedB3(_dt_);
    }
}

void BTMsgManager::requestShot(UnitNode *unSender) /*noexcept*/
{
    if(unSender)
    {
        this->requestB1(unSender);
        emit this->ssPostponedCommandC0Start(500,
                                             unSender);
//        //
//        emit this->ssPostponedCommandB1Start(5000,
//                                             unSender);
    }
}

void BTMsgManager::request0A(UnitNode *unSender) /*noexcept*/
{
    if(unSender)
    {
        QByteArray msg0A;
        msg0A.append((quint8)0xA5);
        if((quint8)VK_KMDeviceType == (quint8)unSender->_typeUN())
        {
            msg0A.append((quint8)0xBA);
        }
        if((quint8)RT_KMDeviceType == (quint8)unSender->_typeUN())
        {
            msg0A.append((quint8)0xDA);
        }
        msg0A.append(unSender->id);
        msg0A.append((quint8)0xC3);
        msg0A.append((quint8)0x0A);
        msg0A.append((char)0x00);
        msg0A.append((char)0x00);
        msg0A.append((char)0x00);
        msg0A.append((quint8)0xFF);
        msg0A = this->setCRC8(msg0A);
        unSender->lastMSGToSend = msg0A;

        emit this->newMSGToSend(this->setByteStaffing(msg0A),
                                unSender->btLink);
        emitInsertNewMSG(unSender,
                         0x0A,
                         msg0A,
                         false,
                         Command_TypeGroupMSG);
//        qDebug() <<"bt signal 83 <--";

    }
}

void BTMsgManager::requestA1(UnitNode *unSender) /*noexcept*/
{
    if(unSender)
    {
        QByteArray msgA1;
        msgA1.append((quint8)0xA5);
        if((quint8)VK_KMDeviceType == (quint8)unSender->_typeUN())
        {
            msgA1.append((quint8)0xBA);
        }
        if((quint8)RT_KMDeviceType == (quint8)unSender->_typeUN())
        {
            msgA1.append((quint8)0xDA);
        }
        msgA1.append(unSender->id);
        msgA1.append((quint8)0xC3);
        msgA1.append((quint8)0xA1);
        msgA1.append((char)0x00);
        msgA1.append((char)0x00);
        msgA1.append((char)0x00);
        msgA1.append((quint8)0xFF);
        msgA1 = this->setCRC8(msgA1);
        unSender->lastMSGToSend = msgA1;
        this->msgToSend = this->setByteStaffing(msgA1);
        this->btLinkMSGToSend = unSender->btLink;

        emit this->newMSGToSend(this->msgToSend,
                                unSender->btLink);
        emitInsertNewMSG(unSender,
                         0xA1,
                         msgA1,
                         false,
                         Command_TypeGroupMSG);
    }
}


void BTMsgManager::requestB1(UnitNode *unSender) /*noexcept*/
{
    if(unSender)
    {
        QByteArray msgB1;
        msgB1.append((quint8)0xA5);
        msgB1.append((quint8)0xBA);
        msgB1.append(unSender->id);
        msgB1.append((quint8)0xC3);
        msgB1.append((quint8)0xB1);
        msgB1.append((char)0x00);
        msgB1.append((char)0x00);
        msgB1.append((char)0x00);
        msgB1.append((quint8)0xFF);
        msgB1 = this->setCRC8(msgB1);
        unSender->lastMSGToSend = msgB1;
        this->msgToSend = this->setByteStaffing(msgB1);
        this->btLinkMSGToSend = unSender->btLink;
        emit this->newMSGToSend(this->msgToSend,
                                unSender->btLink);
        emitInsertNewMSG(unSender,
                         0xB1,
                         msgB1,
                         false,
                         Command_TypeGroupMSG);
    }
}

void BTMsgManager::requestB5(UnitNode *unSender) /*noexcept*/
{
    if(unSender)
    {
        QByteArray msgB5;
        msgB5.append((quint8)0xA5);
        msgB5.append((quint8)0xBA);
        msgB5.append(unSender->id);
        msgB5.append((quint8)0xC3);
        msgB5.append((quint8)0xB5);
        msgB5.append((char)0x00);
        msgB5.append((char)0x00);
        msgB5.append((char)0x00);
        msgB5.append((quint8)0xFF);
        msgB5 = this->setCRC8(msgB5);
        unSender->lastMSGToSend = msgB5;
        this->msgToSend = this->setByteStaffing(msgB5);
        this->btLinkMSGToSend = unSender->btLink;
        emit this->newMSGToSend(this->msgToSend,
                                unSender->btLink);
        emitInsertNewMSG(unSender,
                         0xB5,
                         msgB5,
                         false,
                         Command_TypeGroupMSG);
    }
}

void BTMsgManager::requestC0(UnitNode *unSender) /*noexcept*/
{
    if(unSender)
    {
        QByteArray msgC0;
        msgC0.append((quint8)0xA5);
        msgC0.append((quint8)0xBA);
        msgC0.append(unSender->id);
        msgC0.append((quint8)0xC3);
        msgC0.append((quint8)0xC0);
        msgC0.append((char)0x00);
        msgC0.append((char)0x00);
        msgC0.append((char)0x00);
        msgC0.append((quint8)0xFF);
        msgC0 = this->setCRC8(msgC0);
        unSender->lastMSGToSend = msgC0;

        this->msgToSend = this->setByteStaffing(msgC0);
        this->btLinkMSGToSend = unSender->btLink;
        emit this->newMSGToSend(this->msgToSend,
                                unSender->btLink);
//        emitInsertNewMSG(unSender,
//                         0xC0,
//                         msgC0,
//                         false,
//                         Command_TypeGroupMSG);
    }
}

void BTMsgManager::requestB4(UnitNode* unSender,
                             quint8 nomShot,
                             quint8 nomBlock,
                             quint32 begin,
                             quint32 size) /*noexcept*/
{
    if(unSender)
    {
        QByteArray msgB4;
        msgB4.append((quint8)0xA5);
        msgB4.append((quint8)0xBA);
        msgB4.append(unSender->id);
        msgB4.append((quint8)0xC3);
        msgB4.append((quint8)0xB4);
        msgB4.append((char)0x00);
        msgB4.append((char)0x00);
        msgB4.append((quint8)0x08);
        msgB4.append(nomShot);
        msgB4.append(nomBlock);
        msgB4.append((quint8)((begin & 0x00FF0000) / 0x00010000));
        msgB4.append((quint8)((begin & 0x0000FF00) / 0x00000100));
        msgB4.append((quint8)(begin & 0x000000FF));
        msgB4.append((quint8)((size & 0x00FF0000) / 0x00010000));
        msgB4.append((quint8)((size & 0x0000FF00) / 0x00000100));
        msgB4.append((quint8)(size & 0x000000FF));
        msgB4.append((quint8)0xFF);
        msgB4 = this->setCRC8(msgB4);
        unSender->lastMSGToSend = msgB4;

        this->msgToSend = this->setByteStaffing(msgB4);
        this->btLinkMSGToSend = unSender->btLink;

        emit this->newMSGToSend(this->msgToSend,
                                unSender->btLink);
//        emitInsertNewMSG(unSender,
//                         0xB4,
//                         msgB4,
//                         false,
//                         Command_TypeGroupMSG);
    }
}

void BTMsgManager::requestB3(UnitNode* unSender,
                             quint8 nomShot,
                             quint8 nomBlock,
                             quint32 begin,
                             quint32 size) /*noexcept*/
{
    if(unSender)
    {
        QByteArray msgB3;
        msgB3.append((quint8)0xA5);
        msgB3.append((quint8)0xBA);
        msgB3.append(unSender->id);
        msgB3.append((quint8)0xC3);
        msgB3.append((quint8)0xB3);
        msgB3.append((char)0x00);
        msgB3.append((char)0x00);
        msgB3.append((quint8)0x08);
        msgB3.append(nomShot);
        msgB3.append(nomBlock);
        msgB3.append((quint8)((begin & 0x00FF0000) / 0x00010000));
        msgB3.append((quint8)((begin & 0x0000FF00) / 0x00000100));
        msgB3.append((quint8)(begin & 0x000000FF));
        msgB3.append((quint8)((size & 0x00FF0000) / 0x00010000));
        msgB3.append((quint8)((size & 0x0000FF00) / 0x00000100));
        msgB3.append((quint8)(size & 0x000000FF));
        msgB3.append((quint8)0xFF);
        msgB3 = this->setCRC8(msgB3);
        unSender->lastMSGToSend = msgB3;

        this->msgToSend = this->setByteStaffing(msgB3);
        this->btLinkMSGToSend = unSender->btLink;

        emit this->newMSGToSend(this->msgToSend,
                                unSender->btLink);
        emitInsertNewMSG(unSender,
                         0xB3,
                         msgB3,
                         false,
                         Command_TypeGroupMSG);
    }
}

void BTMsgManager::request2F(UnitNode *unSender) /*noexcept*/
{
    if(unSender)
    {
        QByteArray msg2F;
        msg2F.append((quint8)0xA5);
        msg2F.append((quint8)0xBA);
        msg2F.append(unSender->id);
        msg2F.append((quint8)0xC3);
        msg2F.append((quint8)0x2F);
        msg2F.append((char)0x00);
        msg2F.append((char)0x00);
        msg2F.append((char)0x00);
        msg2F.append((quint8)0xFF);
        msg2F = this->setCRC8(msg2F);
        unSender->lastMSGToSend = msg2F;

        this->msgToSend = this->setByteStaffing(msg2F);
        this->btLinkMSGToSend = unSender->btLink;
        emit this->newMSGToSend(this->msgToSend,
                                unSender->btLink);
        emitInsertNewMSG(unSender,
                         0x2F,
                         msg2F,
                         false,
                         Command_TypeGroupMSG);
    }
}

void BTMsgManager::request1F(UnitNode* unSender,
                             quint8 mode,
                             quint8 t1,
                             quint8 t2,
                             quint8 t3,
                             quint8 selfSO,
                             quint8 BSK,
                             quint8 idBSK1h,
                             quint8 idBSK1l,
                             quint8 idBSK2h,
                             quint8 idBSK2l,
                             quint8 idBSK3h,
                             quint8 idBSK3l,
                             quint8 outSO,
                             quint8 AndOr,
                             quint8 controllPKP) /*noexcept*/
{
    if(unSender)
    {
        unSender->listIMGVK.clear();
        unSender->pack.listIMGItem.clear();
        unSender->pack.readyStatus = false;

        QByteArray msg1F;
        msg1F.append((quint8)0xA5);
        msg1F.append((quint8)0xBA);
        msg1F.append(unSender->id);
        msg1F.append((quint8)0xC3);
        msg1F.append((quint8)0x1F);
        msg1F.append((char)0x00);
        msg1F.append((char)0x00);
        msg1F.append((quint8)0x0F);
        msg1F.append(mode);
        msg1F.append(t1);
        msg1F.append(t2);
        msg1F.append(t3);
        msg1F.append(selfSO);
        msg1F.append(BSK);
        msg1F.append(idBSK1h);
        msg1F.append(idBSK1l);
        msg1F.append(idBSK2h);
        msg1F.append(idBSK2l);
        msg1F.append(idBSK3h);
        msg1F.append(idBSK3l);
        msg1F.append(outSO);
        msg1F.append(AndOr);
        msg1F.append(controllPKP);
        msg1F.append((quint8)0xFF);
        msg1F = this->setCRC8(msg1F);
        unSender->lastMSGToSend = msg1F;

        this->msgToSend = this->setByteStaffing(msg1F);
        this->btLinkMSGToSend = unSender->btLink;

        emit this->newMSGToSend(this->msgToSend,
                                unSender->btLink);
        emitInsertNewMSG(unSender,
                         0x1F,
                         msg1F,
                         false,
                         Command_TypeGroupMSG);
    }
}

void BTMsgManager::requestDE(UnitNode* unSender,
                             quint16 blockSZ,
                             quint16 waitSZ) /*noexcept*/
{
    if(unSender)
    {
        QByteArray msgDE;
        msgDE.append((quint8)0xA5);
        msgDE.append((quint8)0xBA);
        msgDE.append(unSender->id);
        msgDE.append((quint8)0xC3);
        msgDE.append((quint8)0xDE);
        msgDE.append((char)0x00);
        msgDE.append((char)0x00);
        msgDE.append((char)0x04);
        msgDE.append((blockSZ & 0xFF00) / 0x0100),//BlockSizeH
        msgDE.append(blockSZ & 0x00FF),//BlockSizeL
        msgDE.append((waitSZ & 0xFF00) / 0x0100),//TimeoutH
        msgDE.append(waitSZ & 0x00FF),//TimeoutL
        msgDE.append((quint8)0xFF);

        msgDE = this->setCRC8(msgDE);
        unSender->lastMSGToSend = msgDE;

        this->msgToSend = this->setByteStaffing(msgDE);
        this->btLinkMSGToSend = unSender->btLink;
        emit this->newMSGToSend(this->msgToSend,
                                unSender->btLink);
        emitInsertNewMSG(unSender,
                         0xDE,
                         msgDE,
                         false,
                         Command_TypeGroupMSG);
    }
}

void BTMsgManager::requestD5(UnitNode* unSender) /*noexcept*/
{
    if(unSender)
    {
        QByteArray msgD5;
        msgD5.append((quint8)0xA5);
        msgD5.append((quint8)0xDA);
        msgD5.append(unSender->id);
        msgD5.append((quint8)0xC3);
        msgD5.append((quint8)0xD5);
        msgD5.append((char)0x00);
        msgD5.append((char)0x00);
        msgD5.append((char)0x00);
        msgD5.append((quint8)0xFF);
        msgD5 = this->setCRC8(msgD5);
        unSender->lastMSGToSend = msgD5;

        this->msgToSend = this->setByteStaffing(msgD5);
        this->btLinkMSGToSend = unSender->btLink;
        emit this->newMSGToSend(this->msgToSend,
                                unSender->btLink);
        emitInsertNewMSG(unSender,
                         0xD5,
                         msgD5,
                         false,
                         Command_TypeGroupMSG);
    }
}

void BTMsgManager::request3A(UnitNode* unSender) /*noexcept*/
{
    if(unSender)
    {
        QByteArray msg3A;
        msg3A.append((quint8)0xA5);
        msg3A.append((quint8)0xAC);
        msg3A.append(unSender->id);
        msg3A.append((quint8)0xC3);
        msg3A.append((quint8)0x3A);
        msg3A.append((char)0x00);
        msg3A.append((char)0x00);
        msg3A.append((char)0x00);
        msg3A.append((quint8)0xFF);
        msg3A = this->setCRC8(msg3A);
        unSender->lastMSGToSend = msg3A;

        this->msgToSend = this->setByteStaffing(msg3A);
        this->btLinkMSGToSend = unSender->btLink;
        emit this->newMSGToSend(this->msgToSend,
                                unSender->btLink);
        emitInsertNewMSG(unSender,
                         0x3A,
                         msg3A,
                         false,
                         Command_TypeGroupMSG);
    }
}

void BTMsgManager::request3B(UnitNode* unSender) /*noexcept*/
{
    if(unSender)
    {
        QByteArray msg3B;
        msg3B.append((quint8)0xA5);
        msg3B.append((quint8)0xCA);
        msg3B.append(unSender->id);
        msg3B.append((quint8)0xC3);
        msg3B.append((quint8)0x3B);
        msg3B.append((char)0x00);
        msg3B.append((char)0x00);
        msg3B.append((char)0x00);
        msg3B.append((quint8)0xFF);
        msg3B = this->setCRC8(msg3B);
        unSender->lastMSGToSend = msg3B;

        this->msgToSend = this->setByteStaffing(msg3B);
        this->btLinkMSGToSend = unSender->btLink;
        emit this->newMSGToSend(this->msgToSend,
                                unSender->btLink);
        emitInsertNewMSG(unSender,
                         0x3B,
                         msg3B,
                         false,
                         Command_TypeGroupMSG);
    }
}

void BTMsgManager::postponedRequestA1() /*noexcept*/
{
    this->requestA1(this->targetA1);
}

void BTMsgManager::analyzer0A(UnitNode* unSender/*,
                              QByteArray msg*/) /*noexcept*/
{
//    emitInsertNewMSG(unSender,
//                     msg.at(8),
//                     msg,
//                     true,
//                     Command_TypeGroupMSG);
    if(Undefined != this->workMode() &&
       DutyMode != this->workMode())
        return;
    if(unSender)
    {
        if(!unSender->lastMSGToSend.isEmpty())
        {
            if((quint8)0xA5 == (quint8)unSender->lastMSGToSend.at(0))
            {
                if((quint8)0x1F == (quint8)unSender->lastMSGToSend.at(8))
                {
                    this->request2F(unSender);
                }
//                if((quint8)0xB5 == (quint8)unSender->lastMSGToSend.at(8) &&
//                   unSender->autoRequestB1)
//                {
//                        this->requestShot(unSender);
//                }
            }
        }
    }
}

void BTMsgManager::analyzerAA(UnitNode* unSender,
                              QByteArray msg) /*noexcept*/
{
    if(unSender)
    {
//        emitInsertNewMSG(unSender,
//                         msg.at(8),
//                         msg,
//                         true,
//                         Command_TypeGroupMSG);
        if(0x0A == (0x0F & msg.at(12)) ||
           0x0B == (0x0F & msg.at(12)))
        {
            unSender->updateFlagDischarge(true);
        }

        if(0x02 == (0x0F & msg.at(12)))
        {
            unSender->updateFlagDischarge(false);
        }

//24 02 2016
//        if(0x0C == (0x0F & msg.at(12)) ||
//           0x0D == (0x0F & msg.at(12)))
//        {//Off
//            this->extractionFromStructure(unSender);
//            if(unSender->getUnitNodeTreeParent())
//            {
//                emit this->disconnectUN(unSender->getUnitNodeTreeParent(),
//                                        unSender);
//            }
//        }

        if((quint8)VK_KMDeviceType == (quint8)unSender->_typeUN())
            this->request0A(unSender);
    }
}

void BTMsgManager::analyzerE1(UnitNode* unSender/*,
                              QByteArray msg*/) /*noexcept*/
{
#ifdef QT_DEBUG
    qDebug() << "analyzerE1 -->";
#endif
//    emitInsertNewMSG(unSender,
//                     msg.at(8),
//                     msg,
//                     true,
//                     Alarm_TypeGroupMSG);
    if(!this->beeper->isRunning() &&
       this->beeperFlag)
    {
        this->beeper->start();
    }

    unSender->setAlarmCondition();
//    emit this->setAlarmCondition(unSender);

    if(Undefined != this->workMode() &&
       DutyMode != this->workMode())
        return;

//    this->requestC0(unSender);
#ifdef QT_DEBUG
    qDebug() << "analyzerE1 <--";
#endif
}

void BTMsgManager::analyzerC1(UnitNode* unSender,
                              QByteArray msg) /*noexcept*/
{
//    emit this->ssPostponedCommandB4Stop();
#ifdef QT_DEBUG
    qDebug() << "analyzerC1() -->";
#endif
    if(unSender)
    {
//        unSender->listIMG.clear();

//        unSender->pack.listIMGItem.clear();
        unSender->pack.readyStatus = false;
        unSender->pack.listIMGItem.clear();
        unSender->pack.idC1MSG = emitInsertNewMSG(unSender,
                                                  msg.at(8),
                                                  msg,
                                                  true,
                                                  FF_TypeGroupMSG);

        if(Undefined != this->workMode() &&
           DutyMode != this->workMode())
            return;

        this->defBS = 0;
        quint8 keyUpdate(0);
//        //
//        unSender->listIMG.clear();
//        //
        for(int i(0); i < 5; i++)
        {
            quint32 size(((quint8)msg.at(12 + (i * 3)) * 0x00010000) + ((quint8)msg.at(13 + (i * 3)) * 0x00000100) + ((quint8)msg.at(14 + (i * 3))));
            size = size & 0x00FFFFFF;
            if(size)
            {
                keyUpdate += (unSender->reserveIMG(i + 1, size, 1) ? 1 : 0);
//                unSender->reserveIMG(i + 1,
//                                     size);
                emit this->updateProgressShotLoad(unSender,
                                                  i + 1,
                                                  0,
                                                  0);
#ifdef QT_DEBUG
                qDebug() << "nom shot(" << unSender->listIMGVK.last().numShot << ") size(" << unSender->listIMGVK.last().releaseSize << ")";
#endif
            }
            else
            {
                keyUpdate += (unSender->deduceIMG(i + 1, 1) ? 1 : 0);
            }
        }
        //
        if(unSender->listIMGVK.size() &&
           0 != keyUpdate)
        {
            unSender->currentNumShotVK = unSender->listIMGVK.first().numShot;
            unSender->currentNumBlockVK = 0;
            unSender->beginVK = 0;
            unSender->sizeBlockVK = unSender->listIMGVK.first().releaseSize;// / MaxCountBlock;

            emit this->ssPostponedCommandB3Start(1000,
                                                 unSender);

//            emit this->ssPostponedCommandB4Start(1000,
//                                                 unSender);
        }
        else
        {
#ifdef QT_DEBUG
            qDebug() << "!!! Ignor C1 !!!";
#endif
        }
    }
#ifdef QT_DEBUG
    qDebug() << "analyzerC1() <--";
#endif
}

void BTMsgManager::analyzerC3(UnitNode* unSender,
                              QByteArray msg) /*noexcept*/
{
    if(18 >= msg.size())
    {
        return;
    }

    //
    if(unSender && true)
    {
        emit this->ssPostponedCommandB3Stop();
        emit this->ssPostponedCommandB4Stop();
        emit this->stopWaitEndC3();

        QByteArray block(msg);
        block.chop(1);
        block.remove(0, 17);

        if(this->defBS < (quint32)block.size())
            this->defBS = block.size();

        quint8 nomShot(msg.at(12)),
               nomBlock(msg.at(13));
        quint32 defaultSizeBlock(this->defBS);
        quint32 sizeBlock(((quint8)msg.at(14) * 0x00010000) + ((quint8)msg.at(15) * 0x00000100) + ((quint8)msg.at(16)));
        quint32 beginBlock((nomBlock - 1) * defaultSizeBlock);
        quint32 endBlock(beginBlock + sizeBlock);

        if((quint8)0 == nomShot ||
           (quint8)0 == nomBlock ||
           sizeBlock != (quint32)block.size())
        {
            return;
        }
#ifdef QT_DEBUG
        qDebug() << "analyzerC3(ns " << nomShot << " nb " << nomBlock << ") -->";
#endif

        //поиск выделенного места под кадр
        for(int i(0); i < unSender->listIMGVK.size(); i++)
        {
            if(nomShot == unSender->listIMGVK.at(i).numShot)
            {

                if(unSender->listIMGVK.at(i).listPiece.size() < nomBlock)
                {
                    //добавление недостающих в массиве пустых кусочков
                    PieceIMG nullPiece;
                    nullPiece.begin = nullPiece.end = 0;
                    nullPiece.piece.clear();
                    for(int j(0),m(nomBlock - unSender->listIMGVK.at(i).listPiece.size() - 1); j < m; j++)
                    {
                        unSender->listIMGVK[i].listPiece.append(nullPiece);
                    }
                    PieceIMG piece;
                    piece.begin = beginBlock;
                    piece.end = endBlock;
                    piece.piece = block;
                    unSender->listIMGVK[i].listPiece.append(piece);

                    unSender->listIMGVK[i].currentSize += sizeBlock;
                }
                else
                {
                    if(0 == unSender->listIMGVK[i].listPiece[nomBlock - 1].begin &&
                       0 == unSender->listIMGVK[i].listPiece[nomBlock - 1].end &&
                       unSender->listIMGVK[i].listPiece[nomBlock - 1].piece.isEmpty())
                    {
                        PieceIMG piece;
                        piece.begin = beginBlock;
                        piece.end = endBlock;
                        piece.piece = block;
                        unSender->listIMGVK[i].listPiece[nomBlock - 1] = piece;

                        unSender->listIMGVK[i].currentSize += sizeBlock;
                    }
                    else
                    {
                        PieceIMG piece;
                        piece.begin = beginBlock;
                        piece.end = endBlock;
                        piece.piece = block;
                        unSender->listIMGVK[i].listPiece[nomBlock - 1] = piece;
                    }
                }

                //точное определение
                unSender->listIMGVK[i].currentSize = 0;
                for(int j(0), m(unSender->listIMGVK[i].listPiece.size()); j < m; j++)
                {
                    unSender->listIMGVK[i].currentSize += unSender->listIMGVK[i].listPiece[j].piece.size();
                }
                //
#ifdef QT_DEBUG
                qDebug() << "releaseSize/currentSize( " << unSender->listIMGVK[i].releaseSize << " / " << unSender->listIMGVK[i].currentSize << ") [" << beginBlock << ";" << endBlock << "]";
#endif
                if(unSender->listIMGVK[i].releaseSize > unSender->listIMGVK[i].currentSize)
                {
                    if(unSender->listIMGVK[i].doneStatus)
                    {
                        emit this->startWaitEndC3(2000);
                    }
                    else
                    {
                        emit this->ssPostponedCommandB4Start(1000,
                                                             unSender);
                    }

                    quint8 progressValue(((100 <= (unSender->listIMGVK[i].currentSize * 100) / unSender->listIMGVK[i].releaseSize) ? 99 : ((unSender->listIMGVK[i].currentSize * 100) / unSender->listIMGVK[i].releaseSize)));

                    emit this->updateProgressShotLoad(unSender,
                                                      nomShot,
                                                      0,
                                                      progressValue);
#ifdef QT_DEBUG
                    qDebug() << "analyzerC3 <--(1)";
#endif
                    return;
                }

                if(unSender->listIMGVK[i].releaseSize <= unSender->listIMGVK[i].currentSize)
                {
                    QByteArray dataIMG;
                    for(int j(0); j < unSender->listIMGVK.at(i).listPiece.size(); j++)
                    {
                        dataIMG.append(unSender->listIMGVK.at(i).listPiece.at(j).piece);
                    }

                    //
                    IMGItem newImgItem;
                    newImgItem.img = dataIMG;
                    newImgItem.num = unSender->listIMGVK.at(i).numShot;
                    newImgItem.size = unSender->listIMGVK.at(i).releaseSize;
                    unSender->listIMGVK.removeAt(i);
                    unSender->pack.listIMGItem.append(newImgItem);
                    //

                    if(!unSender->listIMGVK.size())
                    {
//                        unSender->offAlarmCondition();

                        this->requestB5(unSender);
                        unSender->pack.readyStatus = true;
                    }
                    emit this->ssPostponedCommandB3Start(1000,
                                                         unSender);

                    emit this->updateProgressShotLoad(unSender,
                                                      nomShot,
                                                      0,
                                                      0);

                    QByteArray msgC4(msg.left(17).append(dataIMG).append(msg.right(1)));
                    msgC4[8] = (quint8)0xC4;
                    this->dbManager->addNewIMGPackFromBT(unSender,
                                                         /*0xC4,*/
                                                         msgC4/*,
                                                         true*/);
//                            emit insertNewIMG();


#ifdef QT_DEBUG
                    QString fname(QDir::current().currentPath());
                    fname.append("/img/");
                    fname.append(QDateTime::currentDateTime().toString("dd_MM_yy [hh_mm_ss]") + "KM UN" + unSender->id.toHex() + ".jpg");
                    QFile f_ou(fname);
                    f_ou.open(QIODevice::WriteOnly);
                    f_ou.write(dataIMG);
                    f_ou.close();
                    qDebug() << "analyzerC3 <--(4)";
#else
                    QString fname(QDir::current().currentPath());
                    fname.append("/img/");
                    fname.append(QDateTime::currentDateTime().toString("dd_MM_yy [hh_mm_ss]") + "KM UN" + unSender->id.toHex() + ".jpg");
                    QFile f_ou(fname);
                    f_ou.open(QIODevice::WriteOnly);
                    f_ou.write(dataIMG);
                    f_ou.close();
#endif
                    return;
                }
            }
        }
    }
    return;
}

void BTMsgManager::analyzerC5(UnitNode* unSender/*,
                              QByteArray msg*/) /*noexcept*/
{
//    emitInsertNewMSG(unSender,
//                     msg.at(8),
//                     msg,
//                     true,
//                     FF_TypeGroupMSG);

    if(Undefined != this->workMode() &&
       DutyMode != this->workMode())
        return;

    if(unSender)
    {
        if(unSender->autoRequestB1)
        {
            this->requestShot(unSender);
        }
    }
}

void BTMsgManager::analyzer3F(UnitNode* unSender,
                              QByteArray msg) /*noexcept*/
{
//    emitInsertNewMSG(unSender,
//                     msg.at(8),
//                     msg,
//                     true,
//                     Command_TypeGroupMSG);

    if(Undefined != this->workMode() &&
       DutyMode != this->workMode())
        return;

    if(unSender && (27 <= msg.size()))
    {
        unSender->modeVK = (quint8)msg.at(12);
        unSender->timeout1 = (quint8)msg.at(13);
        unSender->timeout2 = (quint8)msg.at(14);
        unSender->timeout3 = (quint8)msg.at(15);
        unSender->selfSO = (quint8)msg.at(16);
        unSender->bskSO = (quint8)msg.at(17);
        unSender->idBSK1h = (quint8)msg.at(18);
        unSender->idBSK1l = (quint8)msg.at(19);
        unSender->idBSK2h = (quint8)msg.at(20);
        unSender->idBSK2l = (quint8)msg.at(21);
        unSender->idBSK3h = (quint8)msg.at(22);
        unSender->idBSK3l = (quint8)msg.at(23);
        unSender->otherSO = (quint8)msg.at(24);
        unSender->logic = (quint8)msg.at(25);
        unSender->numComplex = (quint8)msg.at(26);

        emit updateSettingUN(unSender,
                             msg);
    }

}

//void BTMsgManager::analyzer3A(UnitNode* unSender,
//                              QByteArray msg) /*noexcept*/
//{
////    emitInsertNewMSG(unSender,
////                     msg.at(8),
////                     msg,
////                     true,
///                      FF_TypeGroupMSG);

//    if(Undefined != this->workMode() &&
//       DutyMode != this->workMode())
//        return;

//}

//void BTMsgManager::analyzer3B(UnitNode* unSender,
//                              QByteArray msg) /*noexcept*/
//{
////    emitInsertNewMSG(unSender,
////                     msg.at(8),
////                     msg,
////                     true,
///                      FF_TypeGroupMSG);

//    if(Undefined != this->workMode() &&
//       DutyMode != this->workMode())
//        return;

//}

void BTMsgManager::repealLastMSG() /*noexcept*/
{
    if(!this->msgToSend.isEmpty())
    {
        emit this->newMSGToSend();
        if((quint8)0xA5 == (quint8)this->msgToSend.at(0))
        {
            emitInsertNewMSG(this->possGetUnitNode(this->btLinkMSGToSend),
                             this->msgToSend.at(8),
                             this->msgToSend,
                             false,
                             System_TypeGroupMSG);
        }
        else
        {
            emitInsertNewMSG(this->possGetUnitNode(this->btLinkMSGToSend),
                             0x00,
                             this->msgToSend,
                             false,
                             System_TypeGroupMSG);
        }
    }
}

void BTMsgManager::sortListUN() /*noexcept*/
{
    return;
    QList<UnitNode*> listRT, listVK;

    for(int i(0), n(this->listBTUnitNode.size()); i < n; i++)
    {
        if((quint8)RT_KMDeviceType == (quint8)this->listBTUnitNode.at(i)->_typeUN())
        {
            if(listRT.size())
            {
                bool addKey(true);
                for(int j(0), m(listRT.size()); j < m; j++)
                {
                    if(listRT.at(j)->btLink.size() > this->listBTUnitNode.at(i)->btLink.size())
                    {
                        listRT.insert(j, this->listBTUnitNode.at(i));
                        addKey = false;
                        break;
                    }
                }
                if(addKey)
                {
                    listRT.append(this->listBTUnitNode.at(i));
                }
            }
            else
            {
                listRT.append(this->listBTUnitNode.at(i));
            }
        }
    }

    for(int i(0), n(this->listBTUnitNode.size()); i < n; i++)
    {
        if((quint8)VK_KMDeviceType == (quint8)this->listBTUnitNode.at(i)->_typeUN())
        {
            if(listVK.size())
            {
                bool addKey(true);
                for(int j(0), m(listVK.size()); j < m; j++)
                {
                    if(listVK.at(j)->btLink.size() > this->listBTUnitNode.at(i)->btLink.size())
                    {
                        listVK.insert(j, this->listBTUnitNode.at(i));
                        addKey = false;
                        break;
                    }
                }
                if(addKey)
                {
                    listVK.append(this->listBTUnitNode.at(i));
                }
            }
            else
            {
                listVK.append(this->listBTUnitNode.at(i));
            }
        }
    }

    listRT.append(listVK);
    this->listBTUnitNode.clear();
    this->listBTUnitNode.append(listRT);
    emit this->updateUN(0);
}

void BTMsgManager::setTopVK(UnitNode* un) /*noexcept*/
{
    return;
    QList<UnitNode*> listRT, listVK;

    for(int i(0), n(this->listBTUnitNode.size()); i < n; i++)
    {
        if((quint8)RT_KMDeviceType == (quint8)this->listBTUnitNode.at(i)->_typeUN())
        {
            if(listRT.size())
            {
                bool addKey(true);
                for(int j(0), m(listRT.size()); j < m; j++)
                {
                    if(listRT.at(j)->btLink.size() > this->listBTUnitNode.at(i)->btLink.size())
                    {
                        listRT.insert(j, this->listBTUnitNode.at(i));
                        addKey = false;
                        break;
                    }
                }
                if(addKey)
                {
                    listRT.append(this->listBTUnitNode.at(i));
                }
            }
            else
            {
                listRT.append(this->listBTUnitNode.at(i));
            }
        }
    }

    for(int i(0), n(this->listBTUnitNode.size()); i < n; i++)
    {
        if((quint8)VK_KMDeviceType == (quint8)this->listBTUnitNode.at(i)->_typeUN())
        {
            if(listVK.size())
            {
                bool addKey(true);
                for(int j(0), m(listVK.size()); j < m; j++)
                {
                    if(listVK.at(j)->btLink.size() > this->listBTUnitNode.at(i)->btLink.size())
                    {
                        listVK.insert(j, this->listBTUnitNode.at(i));
                        addKey = false;
                        break;
                    }
                }
                if(addKey)
                {
                    listVK.append(this->listBTUnitNode.at(i));
                }
            }
            else
            {
                listVK.append(this->listBTUnitNode.at(i));
            }
        }
    }

    listVK.removeAll(un);
    listVK.prepend(un);

    listRT.append(listVK);
    this->listBTUnitNode.clear();
    this->listBTUnitNode.append(listRT);
    emit this->updateUN(0);
}

void BTMsgManager::extractionChildeFromStructure(UnitNode *un) /*noexcept*/
{
    while(!un->listChilde.isEmpty())
    {
        UnitNode *objPtr(un->listChilde.first()),
                 *sourceParent(un);
        int sourceChild(0);
        //
        this->extractionFromStructure(un->listChilde.first());
        //
        UnitNode *destinationParent(objPtr->unTreeParent);
        int destinationChild(objPtr->unTreeParent->listChilde.indexOf(objPtr));

        emit this->moveUN(objPtr,
                          sourceParent,
                          sourceChild,
                          destinationParent,
                          destinationChild);
    }

    un->listChilde.clear();
}

void BTMsgManager::extractionFromStructure(UnitNode *un) /*noexcept*/
{
    while(!un->listChilde.isEmpty())
    {
        this->extractionFromStructure(un->listChilde.first());
//        un->listChilde.removeFirst();
    }

    if(un->unTreeParent)
    {
        if(un->unTreeParent->listChilde.contains(un))
        {
            un->unTreeParent->listChilde.removeOne(un);
        }
    }
    this->m_treeRootUN->appendChild(un);
    un->listChilde.clear();
//    un->btLink.clear();
    un->updateFlagConnection(false);
}

void BTMsgManager::involvingInStructure(UnitNode *unMaster,
                                          UnitNode *unSlave) /*noexcept*/
{
    if(unMaster)
    {
        if(!this->m_treeRootUN->listChilde.isEmpty())
        {
            if(!this->m_treeRootUN->listChilde.removeOne(unSlave))
            {
                for(int i(0); i < this->listBTUnitNode.size(); i++)
                {
                    this->listBTUnitNode.at(i)->listChilde.removeOne(unSlave);
                }
            }
        }
        unMaster->appendChild(unSlave);
    }
}

void BTMsgManager::emitReInterrogateBT(int timeout) /*noexcept*/
{
    if(this->reInterrogateStatus)
    {
        emit this->ssReInterrogateBT(timeout);
    }
}

quint32 BTMsgManager::emitInsertNewMSG(UnitNode *unSender,
                                       quint8 codeMSG,
                                       QByteArray msg,
                                       bool ioType,
                                       TypeGroupMSG typeGroup,
                                       bool flagHidden) /*noexcept*/
{
    quint32 indexDBMsg(0);
    indexDBMsg = this->dbManager->addNewMSG(unSender,
                                            codeMSG,
                                            msg,
                                            ioType,
                                            KMSystemType,
                                            typeGroup,
                                            flagHidden);
//    emit this->insertNewMSG(indexDBMsg);
    return indexDBMsg;
}

//quint32 BTMsgManager::emitInsertNewMSG(quint32 indexDBMsg) /*noexcept*/
//{
//    emit this->insertNewMSG(indexDBMsg);
//    return indexDBMsg;
//}
