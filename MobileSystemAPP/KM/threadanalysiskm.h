﻿#ifndef THREADANALYSISKM_H
#define THREADANALYSISKM_H

#include <QThread>
#include <btmsgmanager.h>

class ThreadAnalysisKM : public QThread
{
    Q_OBJECT
public:
    explicit ThreadAnalysisKM(BTMsgManager *bt,
                              QByteArray link,
                              QByteArray patt,
                              QObject *parent = nullptr) /*noexcept*/;
    virtual ~ThreadAnalysisKM() /*noexcept*/;
    BTMsgManager *btManager;
    QByteArray m_btLink,
               pattern;

signals:

protected slots:
    void run() /*noexcept*/;
};

#endif // THREADANALYSISKM_H
