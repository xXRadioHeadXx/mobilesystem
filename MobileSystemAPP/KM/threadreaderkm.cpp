﻿#include <threadreaderkm.h>

//ThreadReaderKM::ThreadReaderKM(QObject *parent) /*noexcept*/ :
//    QThread(parent)
//{
//}

ThreadReaderKM::ThreadReaderKM(QList<TypeBuffer*> *buffer,
                               QByteArray *collector,
                               QByteArray data,
                               COSVariantList *listPattern,
                               COSVariantList *listRequest,
                               QObject *parent) /*noexcept*/ :
    QThread(parent),
    listBuffer(buffer),
    m_ByteCollector(collector),
    m_listPattern(listPattern),
    m_listRequest(listRequest),
    newData(data)
{
    CrcTable.append(0x00);
    CrcTable.append(0x31);
    CrcTable.append(0x62);
    CrcTable.append(0x53);
    CrcTable.append(0xC4);
    CrcTable.append(0xF5);
    CrcTable.append(0xA6);
    CrcTable.append(0x97);

    CrcTable.append(0xB9);
    CrcTable.append(0x88);
    CrcTable.append(0xDB);
    CrcTable.append(0xEA);
    CrcTable.append(0x7D);
    CrcTable.append(0x4C);
    CrcTable.append(0x1F);
    CrcTable.append(0x2E);

    CrcTable.append(0x43);
    CrcTable.append(0x72);
    CrcTable.append(0x21);
    CrcTable.append(0x10);
    CrcTable.append(0x87);
    CrcTable.append(0xB6);
    CrcTable.append(0xE5);
    CrcTable.append(0xD4);

    CrcTable.append(0xFA);
    CrcTable.append(0xCB);
    CrcTable.append(0x98);
    CrcTable.append(0xA9);
    CrcTable.append(0x3E);
    CrcTable.append(0x0F);
    CrcTable.append(0x5C);
    CrcTable.append(0x6D);

    CrcTable.append(0x86);
    CrcTable.append(0xB7);
    CrcTable.append(0xE4);
    CrcTable.append(0xD5);
    CrcTable.append(0x42);
    CrcTable.append(0x73);
    CrcTable.append(0x20);
    CrcTable.append(0x11);

    CrcTable.append(0x3F);
    CrcTable.append(0x0E);
    CrcTable.append(0x5D);
    CrcTable.append(0x6C);
    CrcTable.append(0xFB);
    CrcTable.append(0xCA);
    CrcTable.append(0x99);
    CrcTable.append(0xA8);

    CrcTable.append(0xC5);
    CrcTable.append(0xF4);
    CrcTable.append(0xA7);
    CrcTable.append(0x96);
    CrcTable.append(0x01);
    CrcTable.append(0x30);
    CrcTable.append(0x63);
    CrcTable.append(0x52);

    CrcTable.append(0x7C);
    CrcTable.append(0x4D);
    CrcTable.append(0x1E);
    CrcTable.append(0x2F);
    CrcTable.append(0xB8);
    CrcTable.append(0x89);
    CrcTable.append(0xDA);
    CrcTable.append(0xEB);

    CrcTable.append(0x3D);
    CrcTable.append(0x0C);
    CrcTable.append(0x5F);
    CrcTable.append(0x6E);
    CrcTable.append(0xF9);
    CrcTable.append(0xC8);
    CrcTable.append(0x9B);
    CrcTable.append(0xAA);

    CrcTable.append(0x84);
    CrcTable.append(0xB5);
    CrcTable.append(0xE6);
    CrcTable.append(0xD7);
    CrcTable.append(0x40);
    CrcTable.append(0x71);
    CrcTable.append(0x22);
    CrcTable.append(0x13);

    CrcTable.append(0x7E);
    CrcTable.append(0x4F);
    CrcTable.append(0x1C);
    CrcTable.append(0x2D);
    CrcTable.append(0xBA);
    CrcTable.append(0x8B);
    CrcTable.append(0xD8);
    CrcTable.append(0xE9);

    CrcTable.append(0xC7);
    CrcTable.append(0xF6);
    CrcTable.append(0xA5);
    CrcTable.append(0x94);
    CrcTable.append(0x03);
    CrcTable.append(0x32);
    CrcTable.append(0x61);
    CrcTable.append(0x50);

    CrcTable.append(0xBB);
    CrcTable.append(0x8A);
    CrcTable.append(0xD9);
    CrcTable.append(0xE8);
    CrcTable.append(0x7F);
    CrcTable.append(0x4E);
    CrcTable.append(0x1D);
    CrcTable.append(0x2C);

    CrcTable.append(0x02);
    CrcTable.append(0x33);
    CrcTable.append(0x60);
    CrcTable.append(0x51);
    CrcTable.append(0xC6);
    CrcTable.append(0xF7);
    CrcTable.append(0xA4);
    CrcTable.append(0x95);

    CrcTable.append(0xF8);
    CrcTable.append(0xC9);
    CrcTable.append(0x9A);
    CrcTable.append(0xAB);
    CrcTable.append(0x3C);
    CrcTable.append(0x0D);
    CrcTable.append(0x5E);
    CrcTable.append(0x6F);

    CrcTable.append(0x41);
    CrcTable.append(0x70);
    CrcTable.append(0x23);
    CrcTable.append(0x12);
    CrcTable.append(0x85);
    CrcTable.append(0xB4);
    CrcTable.append(0xE7);
    CrcTable.append(0xD6);

    CrcTable.append(0x7A);
    CrcTable.append(0x4B);
    CrcTable.append(0x18);
    CrcTable.append(0x29);
    CrcTable.append(0xBE);
    CrcTable.append(0x8F);
    CrcTable.append(0xDC);
    CrcTable.append(0xED);

    CrcTable.append(0xC3);
    CrcTable.append(0xF2);
    CrcTable.append(0xA1);
    CrcTable.append(0x90);
    CrcTable.append(0x07);
    CrcTable.append(0x36);
    CrcTable.append(0x65);
    CrcTable.append(0x54);

    CrcTable.append(0x39);
    CrcTable.append(0x08);
    CrcTable.append(0x5B);
    CrcTable.append(0x6A);
    CrcTable.append(0xFD);
    CrcTable.append(0xCC);
    CrcTable.append(0x9F);
    CrcTable.append(0xAE);

    CrcTable.append(0x80);
    CrcTable.append(0xB1);
    CrcTable.append(0xE2);
    CrcTable.append(0xD3);
    CrcTable.append(0x44);
    CrcTable.append(0x75);
    CrcTable.append(0x26);
    CrcTable.append(0x17);

    CrcTable.append(0xFC);
    CrcTable.append(0xCD);
    CrcTable.append(0x9E);
    CrcTable.append(0xAF);
    CrcTable.append(0x38);
    CrcTable.append(0x09);
    CrcTable.append(0x5A);
    CrcTable.append(0x6B);

    CrcTable.append(0x45);
    CrcTable.append(0x74);
    CrcTable.append(0x27);
    CrcTable.append(0x16);
    CrcTable.append(0x81);
    CrcTable.append(0xB0);
    CrcTable.append(0xE3);
    CrcTable.append(0xD2);

    CrcTable.append(0xBF);
    CrcTable.append(0x8E);
    CrcTable.append(0xDD);
    CrcTable.append(0xEC);
    CrcTable.append(0x7B);
    CrcTable.append(0x4A);
    CrcTable.append(0x19);
    CrcTable.append(0x28);

    CrcTable.append(0x06);
    CrcTable.append(0x37);
    CrcTable.append(0x64);
    CrcTable.append(0x55);
    CrcTable.append(0xC2);
    CrcTable.append(0xF3);
    CrcTable.append(0xA0);
    CrcTable.append(0x91);

    CrcTable.append(0x47);
    CrcTable.append(0x76);
    CrcTable.append(0x25);
    CrcTable.append(0x14);
    CrcTable.append(0x83);
    CrcTable.append(0xB2);
    CrcTable.append(0xE1);
    CrcTable.append(0xD0);

    CrcTable.append(0xFE);
    CrcTable.append(0xCF);
    CrcTable.append(0x9C);
    CrcTable.append(0xAD);
    CrcTable.append(0x3A);
    CrcTable.append(0x0B);
    CrcTable.append(0x58);
    CrcTable.append(0x69);

    CrcTable.append(0x04);
    CrcTable.append(0x35);
    CrcTable.append(0x66);
    CrcTable.append(0x57);
    CrcTable.append(0xC0);
    CrcTable.append(0xF1);
    CrcTable.append(0xA2);
    CrcTable.append(0x93);

    CrcTable.append(0xBD);
    CrcTable.append(0x8C);
    CrcTable.append(0xDF);
    CrcTable.append(0xEE);
    CrcTable.append(0x79);
    CrcTable.append(0x48);
    CrcTable.append(0x1B);
    CrcTable.append(0x2A);

    CrcTable.append(0xC1);
    CrcTable.append(0xF0);
    CrcTable.append(0xA3);
    CrcTable.append(0x92);
    CrcTable.append(0x05);
    CrcTable.append(0x34);
    CrcTable.append(0x67);
    CrcTable.append(0x56);

    CrcTable.append(0x78);
    CrcTable.append(0x49);
    CrcTable.append(0x1A);
    CrcTable.append(0x2B);
    CrcTable.append(0xBC);
    CrcTable.append(0x8D);
    CrcTable.append(0xDE);
    CrcTable.append(0xEF);

    CrcTable.append(0x82);
    CrcTable.append(0xB3);
    CrcTable.append(0xE0);
    CrcTable.append(0xD1);
    CrcTable.append(0x46);
    CrcTable.append(0x77);
    CrcTable.append(0x24);
    CrcTable.append(0x15);

    CrcTable.append(0x3B);
    CrcTable.append(0x0A);
    CrcTable.append(0x59);
    CrcTable.append(0x68);
    CrcTable.append(0xFF);
    CrcTable.append(0xCE);
    CrcTable.append(0x9D);
    CrcTable.append(0xAC);
}

ThreadReaderKM::~ThreadReaderKM() /*noexcept*/
{
    try
    {
        listBuffer = nullptr;
        m_ByteCollector = nullptr;
    }
    catch(...)
    {
#ifdef QT_DEBUG
        qDebug() << "Assert(~ThreadReaderKM)";
#endif
        return;
    }
}

//обработка главного буфера
void ThreadReaderKM::analizMainBuffer() /*noexcept*/
{
//    qDebug() << "analizMainBuffer() -->";

    while(true)//пока есте что разбирать
    {
        if(!this->m_ByteCollector->size())
        {
//            qDebug() << "return(!this->m_ByteCollector->size()) --x";
//            qDebug() << "analizMainBuffer() <--";
            return;
        }

//        удаляем всё перед BF -->
        int indexBF(0);
        QByteArray trashSegment;
        indexBF = this->m_ByteCollector->indexOf(0xBF, 0);
        if(0 < indexBF)
        {
//            qDebug() << "!!! Remove Trash !!!";
            trashSegment = this->m_ByteCollector->left(indexBF);
            this->m_ByteCollector->remove(0, indexBF);
#ifdef QT_DEBUG
            qDebug() << "TrashsSEGMENT (" << trashSegment.toHex() << ")";
#endif
        }
        else
        {
            if(-1 == indexBF)
            {
                trashSegment = this->m_ByteCollector->left(this->m_ByteCollector->size());
                this->m_ByteCollector->clear();
#ifdef QT_DEBUG
                qDebug() << "TrashsSEGMENT (" << trashSegment.toHex() << ")";
#endif
            }
        }
//        удаляем всё перед BF <--

        //отправление мусора на обработку -->
        if(trashSegment.size())
        {
            //добавление принятого пакета в буффер для этих адресов -->
            bool keyAdd(true);
            for(int i(0); i < this->listBuffer->size(); i++)
            {
                if(this->listBuffer->at(i)->btLink.isEmpty())
                {
                    keyAdd = false;
                    this->listBuffer->at(i)->data.append(trashSegment);
                }
            }
            try
            {
                if(keyAdd)
                {
                    TypeBuffer *newBuffer;
                    newBuffer = new TypeBuffer;
                    newBuffer->btLink.clear();
                    newBuffer->data.append(trashSegment);
                    this->listBuffer->append(newBuffer);
                }
                //добавление принятого пакета в буффер для этих адресов <--
                this->detourBuffer();
                continue;
            }
            catch (std::runtime_error &rte)
            {
                qWarning("catch runtime_error %s [ThreadReaderKM::analizMainBuffer]", rte.what());
            }
            catch (std::bad_alloc& ba)
            {
                qWarning("catch bad_alloc %s [ThreadReaderKM::analizMainBuffer]", ba.what());
            }
            catch (std::exception &e)
            {
                qWarning("catch exception %s [ThreadReaderKM::analizMainBuffer]", e.what());
            }
            catch (...)
            {
                qWarning("catch exception <unknown> [ThreadReaderKM::analizMainBuffer]");
            }
            return;
        }
        //отправление мусора на обработку <--

//        проверяем по размеру -->
        if(6 > this->m_ByteCollector->size())//нужны ещё байты. а пока выходим.
        {
//            qDebug() << "return(6 > m_ByteCollector.size(" << this->m_ByteCollector->size() << ")) --x  m_ByteCollector(" << this->m_ByteCollector->left(5).toHex() << "... ["  << this->m_ByteCollector->size() << "] ..." << this->m_ByteCollector->right(5).toHex() << ")";
//            qDebug() << "analizMainBuffer() <--";
            return;
        }

        qint32 dataSize((quint8)this->m_ByteCollector->at(3) + 0x100 * ((quint8)this->m_ByteCollector->at(2) & 0x03));//размер поля данных
        if((5 + dataSize) > this->m_ByteCollector->size())//нужны ещё байты. а пока выходим.
        {
//            qDebug() << "return((5 + dataSize(" << dataSize << ")) > this->m_ByteCollector->size(" << this->m_ByteCollector->size() << ")) --x  m_ByteCollector(" << this->m_ByteCollector->left(5).toHex() << "... ["  << this->m_ByteCollector->size() << "] ..." << this->m_ByteCollector->right(5).toHex() << ")";
//            qDebug() << "analizMainBuffer() <--";
            return;
        }
//        проверяем по размеру <--

//        проверка контрольной суммы -->
        quint8 arg1((quint8)this->m_ByteCollector->at(4 + dataSize)),
               arg2((quint8)0xFF ^ (quint8)this->m_ByteCollector->at(1));
        if(arg1 != arg2)
//        if((quint8)this->m_ByteCollector[4 + dataSize] != ((quint8)0xFF ^ (quint8)this->m_ByteCollector[1]))
        {//удалить BF или очистить буфер и продолжить разбор дальше
//            qDebug() << "this->m_ByteCollector[4 + dataSize(" << dataSize << ")](" << arg1 << ") != (0xFF ^ this->m_ByteCollector[1])(" << arg2 << ")";
#ifdef QT_DEBUG
            qDebug() << "!!!   FrameLoss2   !!!";
            qDebug() << "(" << 5 + dataSize << ")" << this->m_ByteCollector->left(5 + dataSize).toHex();
//            qDebug() << "this->m_ByteCollector->size(" << this->m_ByteCollector->size() << ")";
            qDebug() << "(" << this->m_ByteCollector->size() << ")" << this->m_ByteCollector->toHex();
#endif
            indexBF = this->m_ByteCollector->indexOf(0xBF, 4 + dataSize);
            indexBF = this->m_ByteCollector->indexOf(0xBF);
            if(0 < indexBF)
            {
                this->m_ByteCollector->remove(0, indexBF);
                if(this->m_ByteCollector->size())
                {
//                    qDebug() << "continue(m_ByteCollector[4 + dataSize] != (0xFF ^ m_ByteCollector[1])) --x  packet(" << this->m_ByteCollector->left(5).toHex() << "... ["  << this->m_ByteCollector->size() << "] ..." << this->m_ByteCollector->right(5).toHex() << ")";
//                    qDebug() << "        (0 < indexBF) true";
//                    qDebug() << "        (this->m_ByteCollector->size()) true";
                    continue;
                }
                else
                {
//                    qDebug() << "return(m_ByteCollector[4 + dataSize] != (0xFF ^ m_ByteCollector[1])) --x  packet(" << this->m_ByteCollector->left(5).toHex() << "... ["  << this->m_ByteCollector->size() << "] ..." << this->m_ByteCollector->right(5).toHex() << ")";
//                    qDebug() << "        (0 < indexBF) true";
//                    qDebug() << "        (this->m_ByteCollector->size()) false";
//                    qDebug() << "analizMainBuffer() <--";
                    return;
                }
            }
            else
            {
                this->m_ByteCollector->clear();
//                qDebug() << "return(m_ByteCollector[4 + dataSize] != (0xFF ^ m_ByteCollector[1])) --x  packet(" << this->m_ByteCollector->left(5).toHex() << "... ["  << this->m_ByteCollector->size() << "] ..." << this->m_ByteCollector->right(5).toHex() << ")";
//                qDebug() << "        (0 < indexBF) false";
//                qDebug() << "analizMainBuffer() <--";
                return;
            }
        }
        else
        {//удалить BF и отправить его в анализатор фреймов
            QByteArray packet(this->m_ByteCollector->left(5 + dataSize));

            this->m_ByteCollector->remove(0, packet.size()/*5 + dataSize*/);

            //добавление принятого пакета в буффер для этих адресов -->
            bool keyAdd(true);
            for(int i(0); i < this->listBuffer->size(); i++)
            {
                if(1 == this->listBuffer->at(i)->btLink.size())
                {
                    if(this->listBuffer->at(i)->btLink.at(0) == packet.at(1))
                    {
                        keyAdd = false;
                        this->listBuffer->at(i)->mainBuffer.append(packet);
                    }
                }
            }
            try
            {
                if(keyAdd)
                {
                    TypeBuffer *newBuffer;
                    newBuffer = new TypeBuffer;
                    newBuffer->btLink.append(packet.at(1));
                    newBuffer->mainBuffer.append(packet);
                    this->listBuffer->append(newBuffer);
                }
                //добавление принятого пакета в буффер для этих адресов <--

    //            qDebug() << "Accept packet: " << packet.left(5).toHex() << "... [" <<  packet.size() << "] ..." <<  packet.right(5).toHex();
                this->detourBuffer();
            }
            catch (std::runtime_error &rte)
            {
                qWarning("catch runtime_error %s [ThreadReaderKM::analizMainBuffer]_2", rte.what());
            }
            catch (std::bad_alloc& ba)
            {
                qWarning("catch bad_alloc %s [ThreadReaderKM::analizMainBuffer]_2", ba.what());
            }
            catch (std::exception &e)
            {
                qWarning("catch exception %s [ThreadReaderKM::analizMainBuffer]_2", e.what());
            }
            catch (...)
            {
                qWarning("catch exception <unknown> [ThreadReaderKM::analizMainBuffer]_2");
            }

            indexBF = this->m_ByteCollector->indexOf(0xBF);
            if(0 < indexBF)
            {
                if(this->m_ByteCollector->size())
                {
//                    qDebug() << "continue(m_ByteCollector[4 + dataSize] != (0xFF ^ m_ByteCollector[1])) --x  packet(" << this->m_ByteCollector->left(5).toHex() << "... ["  << this->m_ByteCollector->size() << "] ..." << this->m_ByteCollector->right(5).toHex() << ")";
//                    qDebug() << "        (0 < indexBF) true";
//                    qDebug() << "        (this->m_ByteCollector->size()) true";
                    continue;
                }
                else
                {
//                    qDebug() << "return(m_ByteCollector[4 + dataSize] != (0xFF ^ m_ByteCollector[1])) --x  packet(" << this->m_ByteCollector->left(5).toHex() << "... ["  << this->m_ByteCollector->size() << "] ..." << this->m_ByteCollector->right(5).toHex() << ")";
//                    qDebug() << "        (0 < indexBF) true";
//                    qDebug() << "        (this->m_ByteCollector->size()) false";
//                    qDebug() << "analizMainBuffer() <--";
                    return;
                }
            }
        }
//        проверка контрольной суммы <--

    }
}

void ThreadReaderKM::detourBuffer() /*noexcept*/
{
//    qDebug() << "detourBuffer() -->";
    bool key(true);

    while(key)
    {
        key = false;
        for(int ci(0); ci < this->listBuffer->size(); ci++)
        {
            while(this->listBuffer->at(ci)->mainBuffer.size())
            {
                key = true;

                //размер поля данных
                qint32 dataSize((quint8)this->listBuffer->at(ci)->mainBuffer.at(3) + 0x100 * ((quint8)this->listBuffer->at(ci)->mainBuffer.at(2) & 0x03));
                QByteArray packet(this->listBuffer->at(ci)->mainBuffer.left(5 + dataSize));
                this->listBuffer->at(ci)->mainBuffer.remove(0, 5 + dataSize);

                packet.remove(0, 4);
                packet.chop(1);
                this->listBuffer->at(ci)->data.append(packet);
            }

            if(/*key || */this->listBuffer->at(ci)->data.size())
            {
                //обработка данных -->
                while(this->listBuffer->at(ci)->data.size())//пока есте что разбирать
                {
                    //принятие решения о удалении начальных байтов -->
                    if((quint8)0xA5 != (quint8)this->listBuffer->at(ci)->data.at(0) &&
                       (quint8)0xBF != (quint8)this->listBuffer->at(ci)->data.at(0))
                    {
    //                    qDebug() << "MSG Pattern this->listBuffer->[" << ci << "].data " << this->listBuffer->at(ci)->data.toHex();
                        int indexA5(this->listBuffer->at(ci)->data.indexOf((quint8)0xA5,0));
                        int indexBF(this->listBuffer->at(ci)->data.indexOf((quint8)0xBF));
                        int indexMin(-1);
                        QByteArray sep;
                        sep.append((quint8)0x0D);
                        sep.append((quint8)0x0A);
                        if(-1 == indexA5 &&
                           -1 == indexBF)
                        {
                            indexMin = this->listBuffer->at(ci)->data.indexOf(sep);
                            if(-1 != indexMin)
                            {
                                QByteArray pattern(this->listBuffer->at(ci)->data.left(indexMin + 2));
                                this->listBuffer->at(ci)->data.remove(0,
                                                                      pattern.size());
                                //отправить на обработку!!!-->
    //                            qDebug() << "detourBuffer(newBTMSG)";
                                QVariantList packPattern;
                                packPattern.append(QVariant(pattern));
                                packPattern.append(QVariant(this->listBuffer->at(ci)->btLink));
                                this->m_listPattern->appendUnique(QVariant(packPattern));
//                                this->m_listPattern->possAppend(QVariant(packPattern));
//                                emit this->pattern(this->listBuffer->at(ci)->btLink,
//                                                   pattern);
                                //отправить на обработку!!!<--
                                continue;
                            }
                            else
                            {
                                break;
                            }
                        }
                        else
                        {
                            if(-1 == indexA5 &&
                               -1 != indexBF)
                            {
                                indexMin = indexBF;
                            }
                            if(-1 != indexA5 &&
                               -1 == indexBF)
                            {
                                indexMin = indexA5;
                            }
                            if(-1 != indexA5 &&
                               -1 != indexBF)
                            {
                                indexMin = qMin(indexA5, indexBF);
                            }

                            QByteArray pattern(this->listBuffer->at(ci)->data.left(indexMin));
                            this->listBuffer->at(ci)->data.remove(0,
                                                             pattern.size());

                            //отправить на обработку!!!-->
    //                        qDebug() << "detourBuffer(newBTMSG)";
//                            this->btManager->newBTMSG(this->listBuffer->at(ci)->btLink,
//                                                      pattern);
                            QVariantList packPattern;
                            packPattern.append(QVariant(pattern));
                            packPattern.append(QVariant(this->listBuffer->at(ci)->btLink));
                            this->m_listPattern->appendUnique(QVariant(packPattern));
//                            this->m_listPattern->possAppend(QVariant(packPattern));
//                            emit this->pattern(this->listBuffer->at(ci)->btLink,
//                                               pattern);
                            //отправить на обработку!!!<--
                            continue;
                        }
                        break;
                    }
                    if((quint8)0xBF == (quint8)this->listBuffer->at(ci)->data.at(0))
                    {
    //                    qDebug() << "BT Pattern this->listBuffer->[" << ci << "].data " << this->listBuffer->at(ci)->data.toHex();
                        //проверяем по размеру -->
                        if(6 > this->listBuffer->at(ci)->data.size())
                        {//нужны ещё байты. а пока выходим.
//                            qDebug() << "return(6 > this->listBuffer->[" << ci << "].data.size()) --x  this->listBuffer->[" << ci << "].data(" << this->listBuffer->at(ci)->data.left(5).toHex() << "...[" <<this->listBuffer->at(ci)->data.size() << "]..." << this->listBuffer->at(ci)->data.right(5).toHex() << ")";
                            break;
                        }

                        qint32 size((quint8)this->listBuffer->at(ci)->data[3] + 0x100 * ((quint8)this->listBuffer->at(ci)->data.at(2) & 0x03));//размер поля данных
                        if((5 + size) > this->listBuffer->at(ci)->data.size())
                        {//нужны ещё байты. а пока выходим.
//                            qDebug() << "return((5 + size) > this->listBuffer->[" << ci << "].data.size()) --x  this->listBuffer->[" << ci << "].data(" << this->m_ByteCollector->left(5).toHex() << "... ["  << this->m_ByteCollector->size() << "] ..." << this->m_ByteCollector->right(5).toHex() << ")";
                            break;
                        }
                        //проверяем по размеру <--

                        quint8 arg3((quint8)this->listBuffer->at(ci)->data.at(4 + size)),
                               arg4((quint8)0xFF ^ (quint8)this->listBuffer->at(ci)->data.at(1));
                        if(arg3 != arg4)
    //                    if((quint8)this->listBuffer->at(ci)->data.at(4 + size) != ((quint8)0xFF ^ (quint8)this->listBuffer->at(ci)->data.at(1)))
                        {//удалить BF или очистить буфер и продолжить разбор дальше
//                            qDebug() << "this->listBuffer->[" << ci << ".data[4 + size(" << size << ")](" << arg3 << ") != (0xFF ^ this->listBuffer->[" << ci << "].data[1](" << this->listBuffer->at(ci)->data[1] << "))(" << arg4 << ")";
#ifdef QT_DEBUG
                            qDebug() << "!!!   FrameLoss3   !!!";
#endif
                            this->listBuffer->at(ci)->data.remove(0,
                                                             5 + size);
                            continue;
                        }
                        else
                        {
                            QByteArray pattern(this->listBuffer->at(ci)->data.left(5 + size));
                            this->listBuffer->at(ci)->data.remove(0,
                                                             5 + size);

                            QByteArray searchBTLink(this->listBuffer->at(ci)->btLink + pattern.at(1));
                            bool keyAdd(true);
                            for(int fi(0); fi < this->listBuffer->size(); fi++)
                            {
                                if(searchBTLink == this->listBuffer->at(fi)->btLink)
                                {
                                    keyAdd = false;
                                    this->listBuffer->at(fi)->mainBuffer.append(pattern);
                                }
                            }
                            try
                            {
                                if(keyAdd)
                                {
                                    TypeBuffer *newBuffer;
                                    newBuffer = new TypeBuffer;
                                    newBuffer->btLink = searchBTLink;
                                    newBuffer->mainBuffer.append(pattern);
                                    this->listBuffer->append(newBuffer);
                                }
                            }
                            catch (std::runtime_error &rte)
                            {
                                qWarning("catch runtime_error %s [ThreadReaderKM::detourBuffer]", rte.what());
                            }
                            catch (std::bad_alloc& ba)
                            {
                                qWarning("catch bad_alloc %s [ThreadReaderKM::detourBuffer]", ba.what());
                            }
                            catch (std::exception &e)
                            {
                                qWarning("catch exception %s [ThreadReaderKM::detourBuffer]", e.what());
                            }
                            catch (...)
                            {
                                qWarning("catch exception <unknown> [ThreadReaderKM::detourBuffer]");
                            }
                            //отправить на обработку!!!-->
                            //отправить на обработку!!!<--
                            continue;
                        }
                    }
                    if((quint8)0xA5 == (quint8)this->listBuffer->at(ci)->data.at(0))
                    {
//                        qDebug() << "KM Pattern this->listBuffer->[" << ci << "].data " << this->listBuffer->at(ci)->data.left(10).toHex() << "... ["  << this->listBuffer->at(ci)->data.size() << "] ..." << this->listBuffer->at(ci)->data.right(10).toHex();
                        //проверяем по размеру -->
                        quint32 dataSize(this->listBuffer->at(ci)->data.size());
                        if(13 > dataSize)
                        {//нужны ещё байты. а пока выходим.
//                            qDebug() << "return(13 > dataSize|" << dataSize << "|) --x  this->listBuffer->["<< ci << "].data(" << this->listBuffer->at(ci)->data.left(5).toHex() << "... ["  << this->listBuffer->at(ci)->data.size() << "] ..." << this->listBuffer->at(ci)->data.right(5).toHex() << ")";
                            break;
                        }

                        quint32 size((quint8)this->listBuffer->at(ci)->data.at(11) + 0x100 * (quint8)this->listBuffer->at(ci)->data.at(10) + 0x10000 * (quint8)this->listBuffer->at(ci)->data.at(9)),//размер поля данных
                                count1B(this->listBuffer->at(ci)->data.count((quint8)0x1B));
                        size = size & 0x00FFFFFF;

//                        if((13 + size) > (this->listBuffer->at(ci)->data.size() - this->listBuffer->at(ci)->data.count((quint8)0x1B)))
                        if((13 + size) > (dataSize - count1B))
                        {//нужны ещё байты. а пока выходим.
//                            qDebug() << "return((13 + size|" << size << "|) > (dataSize|" << dataSize << "| - count1B|" << count1B << "|)) --x  this->listBuffer->[" << ci << "].data(" << this->listBuffer->at(ci)->data.left(5).toHex() << "... ["  << this->listBuffer->at(ci)->data.size() << "] ..." << this->listBuffer->at(ci)->data.right(5).toHex() << ")";
                            if(1 < this->listBuffer->at(ci)->data.count((quint8)0xA5))
                            {
//                                QFile ffff(QTime::currentTime().toString("hh mm ss CorruptPattern"));
//                                ffff.open(QIODevice::WriteOnly);
//                                ffff.write(this->listBuffer->at(ci)->data.left(this->listBuffer->at(ci)->data.indexOf((quint8)0xA5,1) + 1));
//                                ffff.close();

                                this->listBuffer->at(ci)->data.remove(0,
                                                                 this->listBuffer->at(ci)->data.indexOf((quint8)0xA5,
                                                                                                   1));
//                                qDebug() << "Rmove segment from this->listBuffer->[" << ci << "].data";
                                continue;
                            }
                            break;
                        }
                        //проверяем по размеру <--

                        QByteArray pattern(this->listBuffer->at(ci)->data);
                        this->listBuffer->at(ci)->data.clear();
                        //ребайтстаффинг -->
                        for(qint32 i(1); i < pattern.size(); i++)
                        {
                            if(0x1B ==(quint8)pattern.at(i))
                            {
                                pattern.remove(i, 1);
                                if(i < pattern.size())
                                {
                                    pattern[i] = pattern.at(i) - 0x20;
                                }
                            }
                        }
                        //ребайтстаффинг <--

                        pattern = pattern.left(13 + size);

                        //проверка crc -->
                        quint8 crc8(0x00);
                        for(qint32 i(1), n(pattern.size() - 1); i < n; i++)
                        {
//                            crc8 -= (quint8)pattern.at(i);
                            crc8 = CrcTable.at((crc8 ^ (quint8)pattern.at(i))); // Табличный метод
                        }
                        //проверка crc <--

                        if(crc8 == (quint8)pattern.right(1).at(0))
                        {
                            //отправить на обработку!!!-->
//                            qDebug() << "detourBuffer(newKmMSG)"  << pattern.left(100).toHex();
                            QVariantList packPattern;
                            packPattern.append(QVariant(pattern));
                            packPattern.append(QVariant(this->listBuffer->at(ci)->btLink));
                            this->m_listPattern->appendUnique(QVariant(packPattern));
//                            this->m_listPattern->possAppend(QVariant(packPattern));
//                            emit this->pattern(this->listBuffer->at(ci)->btLink,
//                                               pattern);
                            //отправить на обработку!!!<--
                        }
                        else
                        {
#ifdef QT_DEBUG
                            QByteArray crcBA;
                            crcBA.append(crc8);
                            qDebug() << "ThreadReaderKM !!! CRC FALSE !!! (" << pattern.left(15).toHex() << "... ["  << pattern.size() << "] ..." << pattern.right(5).toHex() << ")  right[" << crcBA.toHex() << "]";
#endif
//                            QString fname("CRC_False_KM" + QTime::currentTime().toString("_hh_mm_ss_zzz"));
//                            QFile f_ou(fname);
//                            f_ou.open(QIODevice::WriteOnly);
//                            f_ou.write(pattern);
//                            f_ou.close();
                        }
                    }
                    //принятие решения о удалении начальных байтов <--
                }
                //обработка данных <--
            }

        }
    }
//    qDebug() << "detourBuffer() <--";
}

void ThreadReaderKM::run() /*noexcept*/
{
    this->m_ByteCollector->append(newData);
    this->analizMainBuffer();
    this->quit();
}
