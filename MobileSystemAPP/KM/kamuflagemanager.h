﻿#ifndef KAMUFLAGEMANAGER_H
#define KAMUFLAGEMANAGER_H

#include <QObject>
#include <serialportmanagerkm.h>

class KamuflageManager : public QObject
{
    Q_OBJECT
public:

    //объект чтения и записи в порт
    SerialPortManagerKM *m_portManager;
    QThread m_threadPortManager;
    //объект обработки БТ и КМ сообщений
    BTMsgManager *m_btManager;
    //таймеры задержек и итераций
    QTimer timerPostponedCommandLIST,
           timerPostponedCommandTXPOWER,
           timerPostponedCommandRSSI,
           timerDisconnectOfListConnect,
           timerKillConnection,
           timerPostponedCommandCALL,
           timerPostponedCommandKILL,
           timerPostponedCommandINQUIRY,
           timerRequestC0,
           timerPostponedC0,
           timerRequestB1,
           timerPostponedB1,
           timerRequestB3,
           timerPostponedB3,
           timerRequestB4,
           timerPostponedB4,
           timerPostponedA1,
           timerEndC3,
//           timerPostponedF1,
           timerReInterrogateBT,
           timerPortCheck,
           timerWaitAnswer,
           timerByteSec,
           timerOffStatusKMExchange,
           *timerPostponedNeedSendB3,
           *timerPostponedNeedSendB4,
           *timerFirstPostponedNeedSendB4,
           *timerFirstPostponedNeedSendB3;
    QList<QTimer *> listTimerOffAlarmCondition;
    //ссылка на камеру от которой запрашиваются кадры на текущий момент
//    QList<ThreadAnalysis*> listThreadAnalysis;
    UnitNode *targetVK,
             *unPowerCheckM,
             *unPowerCheckS;
    int repeatB4,
        refreshPortIteration,
        iterationC0;
    DataBaseManager *dbManager;


//    explicit KamuflageManager(QObject *parent = nullptr);
    KamuflageManager(DataBaseManager *db,
                     QObject *parent = nullptr) /*noexcept*/;
    virtual ~KamuflageManager() /*noexcept*/;

signals:
    //сигнал передающий принятые сообщения
    void unitMSG(UnitNode* unSender,//от кого (может быть ровно 0)
                 quint8 type,//тип сообщения (0x00 для отладки, 0xFF от блютуза, 0хXX код сообщения от КМ устройства)
                 QByteArray msg,
                 bool ioType);
    //добавление в список устройств
    void addNewUN(UnitNode* un);
    //обновление элемента списка устройств
    void updateUN(UnitNode * un);
    //
    void moveUN(UnitNode *objPtr,
                UnitNode *sourceParent,
                int sourceChild,
                UnitNode *destinationParent,
                int destinationChild);
    //установленно подключение между элементами
    void connectUN(UnitNode *unMaster,
                   UnitNode *unSlave);
    //разорвано подключение между элементами
    void disconnectUN(UnitNode *unMaster,
                      UnitNode *unSlave);
//    void progressC3(UnitNode *unSender,
//                    quint8 nomS,
//                    quint8 progress);
    void updateProgressShotLoad(UnitNode *unSender,
                                quint8 nomShot,
                                quint8 nomTrain,
                                quint8 progress);
    void insertNewMSG(quint32 index);
    void insertNewIMG(quint32 index);
    void updatePortSetting(QList<QString> listSetting);
    void updateSettingUN(UnitNode *un,
                         QByteArray msg3F);
    void portCondition(qint32 condition);
    void currentWorkModeChanged(int mode);


public slots:

//    void analisePattern(QByteArray address,
//                        QByteArray pattern);
//    void clearListThreadAnalysis(ThreadAnalysis *thr = nullptr);
    void needRead() /*noexcept*/;
    void controlConnect(UnitNode* unSender,
                        quint8 type,
                        QByteArray msg,
                        bool ioType) /*noexcept*/;
    void timeoutControlConnect() /*noexcept*/;
    void timeoutWaitAnswer() /*noexcept*/;
//    void loadTopologyUN();
//    void saveUNListToXML();
//    QDomElement unDomElement(QDomDocument domDoc,
//                             int nNumber,
//                             UnitNode *un);
//    QDomElement makeElement(QDomDocument domDoc,
//                            QString strName,
//                            QString strAttr = QString(),
//                            QString strText = QString());
    void repeatMSG() /*noexcept*/;
    bool refreshPort() /*noexcept*/;
    //открытие порта
    void closePort() /*noexcept*/;
    bool openPort(QString portName = "COM1",
                  qint32 rate = 576000,//QSerialPort::BaudRate576000,
                  QSerialPort::DataBits dataBit = QSerialPort::Data8,
                  QSerialPort::StopBits stopBit = QSerialPort::OneStop,
                  QSerialPort::FlowControl flow = QSerialPort::NoFlowControl,
                  QSerialPort::Parity parity = QSerialPort::NoParity) /*noexcept*/;

    QList<QString> getPortSetting() /*noexcept*/;

    //передача сообщения
    void sendMSG(UnitNode* unSender,
                 quint8 type,
                 QByteArray msg) /*noexcept*/;
    void sendMSG(QByteArray adress,
                 QByteArray msg) /*noexcept*/;

    //внеплановый вызов поиска доступных для подключения устройств
    //отправление INQUIRY на все ретрансляторы
    //обход по ретранслятором прерывается при обнаружении и подключении нового устройства
    //нельзя вызвать в течение 30с с последнего обмена сообщениями по КМ протоколу
    void oneBTInquiryToUN(UnitNode* unSender) /*noexcept*/;
    void oneBTListToUN(UnitNode* unSender) /*noexcept*/;
    void oneBTSetToUN(UnitNode* unSender) /*noexcept*/;
    void oneBTKillToUN(UnitNode* unMaster,
                   UnitNode* unSlave) /*noexcept*/;
    void oneToD5Kill(UnitNode* unMaster,
                     UnitNode* unSlave) /*noexcept*/;
    void allBTInquiryToUN() /*noexcept*/;
    //отправление А1
    void cmdRequest_0xA1(UnitNode* unSender) /*noexcept*/;
    //запрос кадра
    void cmdRequest_0xB1(UnitNode* unSender) /*noexcept*/;
    //отправление 1F на камеру
    void cmdRequest_0x1F(UnitNode* unSender,
                              quint8 mode,
                              quint8 t1,
                              quint8 t2,
                              quint8 t3,
                              quint8 selfSO,
                              quint8 BSK,
                              quint8 idBSK1h,
                              quint8 idBSK1l,
                              quint8 idBSK2h,
                              quint8 idBSK2l,
                              quint8 idBSK3h,
                              quint8 idBSK3l,
                              quint8 outSO,
                              quint8 AndOr,
                              quint8 controllPKP) /*noexcept*/;
    //отправление DE
    void cmdRequest_0xDE(UnitNode* unSender,
                                  quint16 blockSZ,
                                  quint16 waitSZ) /*noexcept*/;
    void cmdRequest_0x2F(UnitNode* unSender) /*noexcept*/;
    void oneBTTxpowerToUN() /*noexcept*/;
    void oneBTTxpowerToUN(UnitNode* unMaster,
                          UnitNode* unSlave) /*noexcept*/;
    //запуск построения матрицы смежности
    //построение топологии
    void organiseNetworkTopology() /*noexcept*/;

    //геты-->
    //запрос списка устройств
    QList<UnitNode*> getListUnitNode() /*noexcept*/;
    //запрос списка пар устройств способных подключиться друг к другу
    QList<BTMsgManager::CommunicationItem> getlistCommunicationItem() /*noexcept*/;
    //запрос подмножества устройств (путь от бызы до камеры)
    //потей от базы до камеры может быть больше одного
    QList<QList<UnitNode*>> getListShortWayToVK() /*noexcept*/;
    //текущие пути до камер
    QList<QList<UnitNode*>> getListCurrentUsedWayToVK() /*noexcept*/;
    //геты<--

    void startReadThread() /*noexcept*/;

    void ssPostponedCommandLIST(int ms) /*noexcept*/;
    void ssPostponedCommandTXPOWER(int ms) /*noexcept*/;
    void ssPostponedCommandRSSI(int ms) /*noexcept*/;
    void ssDisconnectOfListConnect(int ms) /*noexcept*/;
    void ssKillConnection(int ms) /*noexcept*/;
    void ssPostponedCommandKILL(int ms) /*noexcept*/;
    void ssPostponedCommandCALL(int ms) /*noexcept*/;
    void ssPostponedCommandINQUIRY(int ms) /*noexcept*/;
    void ssPostponedB1(int ms) /*noexcept*/;
    void ssPostponedA1(int ms) /*noexcept*/;
//    void ssPostponedF1(int ms);
    void ssPostponedCommandB1Start(int timeout,
                                   UnitNode* unSender) /*noexcept*/;
    void ssPostponedCommandB1Stop() /*noexcept*/;
    void catchTimeoutB1() /*noexcept*/;
    void ssPostponedCommandC0Start(int timeout,
                                   UnitNode* unSender) /*noexcept*/;
    void ssPostponedCommandC0Stop() /*noexcept*/;
    void catchTimeoutC0() /*noexcept*/;
    void ssPostponedB3(int ms) /*noexcept*/;
    void ssPostponedCommandB3Start(int timeout,
                                   UnitNode* unSender) /*noexcept*/;
    void ssPostponedCommandB4Start(int timeout,
                                   UnitNode* unSender) /*noexcept*/;
    void postponedNeedSendB3(UnitNode *unSender,
                               int timeout) /*noexcept*/;
    void ssPostponedCommandB3Stop() /*noexcept*/;
    void ssPostponedCommandB4Stop() /*noexcept*/;
    void catchTimeoutB3() /*noexcept*/;
    void ssReInterrogateBT(int timeout) /*noexcept*/;
    void ssOffStatusKMExchange(int timeout) /*noexcept*/;
    void timeoutB3() /*noexcept*/;
    void timeoutB4() /*noexcept*/;
    void ssStartWaitEndC3(int timeout) /*noexcept*/;
    void ssStopWaitEndC3() /*noexcept*/;
    void tmeoutEndC3() /*noexcept*/;
//    void setAlarmCondition(UnitNode *un) /*noexcept*/;
//    void offAlarmCondition() /*noexcept*/;
    quint32 emitInsertNewMSG(UnitNode *unSender,
                          quint8 codeMSG,
                          QByteArray msg,
                          bool ioType,
                          TypeGroupMSG typeGroup,
                          bool flagHidden = false) /*noexcept*/;
//    quint32 emitInsertNewMSG(quint32 indexDBMsg);

};

#endif // KAMUFLAGEMANAGER_H
