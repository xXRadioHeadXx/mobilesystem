﻿  #ifndef BTMSGMANAGER_H
#define BTMSGMANAGER_H

#include <QObject>
#include <QTime>
//#include <QStack>
#include <QStringList>

#ifdef QT_DEBUG
#include <QDebug>
#include <QFile>
#endif

#include <QPixmap>
#include <QMutex>
#include <global_value_and_structure.h>
#include <databasemanager.h>
#include <soundalarm.h>

class BTMsgManager : public QObject
{
    Q_OBJECT
public:

    DataBaseManager *dbManager;
    UnitNode *m_treeRootUN,
             *m_lastAppendBase;
    SoundAlarm *beeper;
    bool beeperFlag;
    QMutex m_resursLock;
    int MaxCountBlock,
        countRepeatMSG;
    quint32 defBS;

    QList<quint8> CrcTable;

    QByteArray macBaseUN;
    QList<UnitNode*> listBTUnitNode;

    bool statusKMExchange;
    bool reInterrogateStatus;
    //переменные формирование сети -->

    WorkMode currentWorkMode;

    QList<QList<UnitNode*>> listCurrentInitialSubset,
                              listInitialSubset,//стэк множеств для открытия новых элементов
                              listShortWayToVK,
//                              listShortWayToCCP,
                              listCurrentUsedWayToVK,
//                              listCurrentUsedWayToCCP,
                              listInitialWayToVK,
//                              listInitialWayToCCP,
                              listSpareWayToVK/*,
                              listSpareWayToCCP*/;

    struct CommunicationItem//структура элемента канала передачи
    {
        UnitNode *unMaster,
                 *unSlave;
        int txPower, rssi;
    };

    CommunicationItem checkedCommunication;
    QList<CommunicationItem> listCommunicationItem,//элементы матрицы смежности
                             usedCommunicationItem;

    QList<UnitNode*> currentCheckedSubsetOfCommunication,
                      currentSubset,
                      currentWayVK,
//                      currentWayCCP,
                      currentCheckedWayVK,
//                      currentCheckedWayCCP,
                      listInterrogatedBT,
                      listCurrentInterrogatedBT,
                      listDefinitionSyperfloursBT;





    QByteArray msgToSend;
    QByteArray btLinkMSGToSend;

    QByteArray lastTxpowerChS,
               lastRssiChS;
    UnitNode *definitionInitiator,
             *definitionChannel,
             *lastCallM,
             *lastCallS,
             *lastKillM,
             *lastKillS,
             *lastListM,
             *lastInquiryM,
             *lastTxpowerM,
             *lastRssiM;

    UnitNode *targetA1,
             *targetF1;

    QList<UnitNode*> listCallBT,
                     listSuperfluous,
                     listDisconnect;


    //переменные формирование сети <--

//    BTMsgManager(QObject *parent = nullptr) /*noexcept*/;
    explicit BTMsgManager(DataBaseManager *db,
                 QObject *parent = nullptr) /*noexcept*/;
    virtual ~BTMsgManager() /*noexcept*/;
    UnitNode* macTargetKM;
    void setInitialListUN() /*noexcept*/;
    QByteArray setCRC8(QByteArray pattern) /*noexcept*/;
    QByteArray setByteStaffing(QByteArray pattern) /*noexcept*/;
    QByteArray crutchFromAndrey(QByteArray pattern,
                                int index = 1) /*noexcept*/;


public slots:
    //ф-и формирование сети -->
//    void updateLastAppendDase(UnitNode *un);
    void repealLastMSG() /*noexcept*/;
    WorkMode workMode() /*noexcept*/;
    void setCurrentWorkMode(WorkMode mode) /*noexcept*/;
    void constructCurrentTopology() /*noexcept*/;
    void buildNewTopology() /*noexcept*/;
    void detourTact() /*noexcept*/;
    QList<float> definitionSubset(QList<UnitNode*> subset) /*noexcept*/;
    void connectSubset(QList<UnitNode*> subset) /*noexcept*/;
    void definitionHeard() /*noexcept*/;
    void postponedCommandCALL() /*noexcept*/;
    void postponedCommandKILL() /*noexcept*/;
    void postponedCommandLIST() /*noexcept*/;
    void postponedCommandINQUIRY() /*noexcept*/;
    void postponedCommandTXPOWER() /*noexcept*/;
    void postponedCommandRSSI() /*noexcept*/;
    void definitionListCallBT() /*noexcept*/;
    void updateInitialSubset() /*noexcept*/;
    void disconnectSubset() /*noexcept*/;
    void killSuperfluous() /*noexcept*/;
    void connectSpareWayToVK() /*noexcept*/;
    void connectOptimalWayToVK() /*noexcept*/;
    void connectOptimalWayToVK(QList<UnitNode*> subset) /*noexcept*/;
    void oneReInterrogateBT(UnitNode *unSender) /*noexcept*/;
    void reInterrogateBT() /*noexcept*/;
    void offStatusKMExchange() /*noexcept*/;

    void beginDutyMode() /*noexcept*/;


    QByteArray add0xFFtoBTLink(QByteArray btLink) /*noexcept*/;
    void requestLIST(UnitNode* unSender,
                     bool postponed = false) /*noexcept*/;
    void requestINQUIRY(UnitNode* unSender,
                        bool postponed = false) /*noexcept*/;
    void requestCALL(UnitNode* unMaster,
                     UnitNode* unSlave,
                     bool postponed = false) /*noexcept*/;
    void requestCLOSE(UnitNode* unMaster,
                      QByteArray ch,
                      bool postponed = false) /*noexcept*/;
    void requestTXPOWER(UnitNode* unMaster,
                        QByteArray ch,
                        bool postponed = false) /*noexcept*/;
    void requestRSSI(UnitNode* unMaster,
                     QByteArray ch,
                     bool postponed = false) /*noexcept*/;
    void requestSET(UnitNode* unSender,
                    bool postponed = false) /*noexcept*/;
    void requestAT(UnitNode* unSender = nullptr,
                    bool postponed = false) /*noexcept*/;
    void requestSET(bool postponed = false) /*noexcept*/;
    void requestKILL(UnitNode* unMaster,
                     UnitNode* unSlave,
                     bool postponed = false) /*noexcept*/;
    void requestKILL(bool postponed = false) /*noexcept*/;
    //ф-и формирование сети <--

    //км комманды -->
    void requestBlockShot(UnitNode* unSender) /*noexcept*/;
    void requestShot(UnitNode *unSender) /*noexcept*/;
    void request0A(UnitNode *unSender) /*noexcept*/;
    void requestA1(UnitNode *unSender) /*noexcept*/;
    void requestB1(UnitNode *unSender) /*noexcept*/;
    void requestB5(UnitNode *unSender) /*noexcept*/;
    void requestC0(UnitNode *unSender) /*noexcept*/;
    void requestB4(UnitNode* unSender,
                   quint8 nomShot,
                   quint8 nomBlock,
                   quint32 begin,
                   quint32 size) /*noexcept*/;
    void requestB3(UnitNode* unSender,
                   quint8 nomShot,
                   quint8 nomBlock,
                   quint32 begin,
                   quint32 size) /*noexcept*/;
    void request2F(UnitNode *unSender) /*noexcept*/;
    void request1F(UnitNode* unSender,
                   quint8 mode,
                   quint8 t1,
                   quint8 t2,
                   quint8 t3,
                   quint8 selfSO,
                   quint8 BSK,
                   quint8 idBSK1h,
                   quint8 idBSK1l,
                   quint8 idBSK2h,
                   quint8 idBSK2l,
                   quint8 idBSK3h,
                   quint8 idBSK3l,
                   quint8 outSO,
                   quint8 AndOr,
                   quint8 controllPKP) /*noexcept*/;
    void requestDE(UnitNode* unSender,
                   quint16 blockSZ,
                   quint16 waitSZ) /*noexcept*/;
    void requestD5(UnitNode* unSender) /*noexcept*/;
    void request3A(UnitNode* unSender) /*noexcept*/;
    void request3B(UnitNode* unSender) /*noexcept*/;

    void postponedRequestA1() /*noexcept*/;
    //км комманды <--



    void sortListUN() /*noexcept*/;
    void setTopVK(UnitNode* unMaster) /*noexcept*/;

    bool possAddCommunicationItem(UnitNode* unMaster,
                                  UnitNode* unSlave) /*noexcept*/;

    bool possAddUsedCommunicationItem(UnitNode* unMaster,
                                      UnitNode* unSlave) /*noexcept*/;

    UnitNode* possAddUnitNode(QByteArray mac,
                              QByteArray name = "Unknown") /*noexcept*/;

//    UnitNode* getUnitNode(QByteArray id);
    UnitNode* getUnitNode(QByteArray id,
                          UnitNode *parentUN = nullptr) /*noexcept*/;


    QList<UnitNode*> possGetListUnitNodeChilde(QByteArray someKey) /*noexcept*/;

    QList<UnitNode*> possGetListUnitNodeChilde(UnitNode* unParent) /*noexcept*/;

    UnitNode* possGetUnitNode(QByteArray someKey) /*noexcept*/;

    QList<UnitNode*> getListUnitNodeChilde( UnitNode* unParent) /*noexcept*/;

    QList<UnitNode*> getListUnitNodeChilde(QByteArray id) /*noexcept*/;

    bool possAddBTToListInquiry(UnitNode* unMaster,
                                UnitNode* unSlave) /*noexcept*/;

    bool updateBTTxPower(UnitNode* unMaster,
                         UnitNode* unSlave,
                         int tx) /*noexcept*/;

    bool updateBTRssi(UnitNode* unMaster,
                      UnitNode* unSlave,
                      int rssi) /*noexcept*/;

    bool possAddBTToListConnect(UnitNode* unMaster,
                                UnitNode* unSlave) /*noexcept*/;

    void setTxPower(UnitNode* unMaster,
                    UnitNode* unSlave,
                    int txPower) /*noexcept*/;

    void setRSSI(UnitNode* unMaster,
                 UnitNode* unSlave,
                 int rssi) /*noexcept*/;

    void analyzer0A(UnitNode* unSender/*,
                    QByteArray msg*/) /*noexcept*/;

    void analyzerAA(UnitNode* unSender,
                    QByteArray msg) /*noexcept*/;

    void analyzerC1(UnitNode* unSender,
                    QByteArray msg) /*noexcept*/;

    void analyzerC3(UnitNode* unSender,
                    QByteArray msg) /*noexcept*/;

    void analyzerC5(UnitNode* unSender/*,
                    QByteArray msg*/) /*noexcept*/;

    void analyzer3F(UnitNode* unSender,
                    QByteArray msg) /*noexcept*/;

    void analyzerE1(UnitNode* unSender/*,
                    QByteArray msg*/) /*noexcept*/;

//    void analyzer3A(UnitNode* unSender,
//                    QByteArray msg) /*noexcept*/;

//    void analyzer3B(UnitNode* unSender,
//                    QByteArray msg) /*noexcept*/;

    void analyzerSET(UnitNode* unSender,
                     QList<QByteArray> msgSegment) /*noexcept*/;

//    void analyzerINQUIRY_PARTIAL(UnitNode* unSender,
//                                 QList<QByteArray> msgSegment) /*noexcept*/;

    void analyzerINQUIRY(UnitNode* unSender,
                         QList<QByteArray> msgSegment) /*noexcept*/;

    void analyzerNAME(UnitNode* unSender,
                      QList<QByteArray> msgSegment) /*noexcept*/;

//    void analyzerCALL(UnitNode* unSender,
//                      QList<QByteArray> msgSegment) /*noexcept*/;

    void analyzerCONNECT(UnitNode* unSender,
                         QList<QByteArray> msgSegment) /*noexcept*/;

    void analyzerLIST(UnitNode* unSender,
                      QList<QByteArray> msgSegment) /*noexcept*/;

    void analyzerTXPOWER(UnitNode* unSender,
                         QList<QByteArray> msgSegment) /*noexcept*/;

    void analyzerRSSI(UnitNode* unSender,
                      QList<QByteArray> msgSegment) /*noexcept*/;

    void analyzerNO(UnitNode* unSender,
                    QList<QByteArray> msgSegment) /*noexcept*/;

    void analyzerSYNTAX(UnitNode *unSender,
                        QList<QByteArray> msgSegment) /*noexcept*/;

    void newBTMSG(QByteArray btLink,
                  QByteArray msg) /*noexcept*/;
    void newKmMSG(QByteArray btLink,
                  QByteArray msg) /*noexcept*/;
//    bool loadUNListFromXML();


    void extractionChildeFromStructure(UnitNode *un) /*noexcept*/;
    void extractionFromStructure(UnitNode *un) /*noexcept*/;
    void involvingInStructure(UnitNode *unMaster,
                              UnitNode *unSlave) /*noexcept*/;
    void emitReInterrogateBT(int timeout) /*noexcept*/;

    quint32 emitInsertNewMSG(UnitNode *unSender,
                             quint8 codeMSG,
                             QByteArray msg,
                             bool ioType,
                             TypeGroupMSG typeGroup,
                             bool flagHidden = false) /*noexcept*/;
//    quint32 emitInsertNewMSG(quint32 indexDBMsg) /*noexcept*/;

//    void sendedBTMSG(QByteArray btLink,
//                     QByteArray msg) /*noexcept*/;

signals:
    void insertNewIMG(quint32 index);
    void insertNewMSG(quint32 index);
    void addNewUN(UnitNode *objPtr);
    void updateUN(UnitNode *objPtr);
    void moveUN(UnitNode *objPtr,
                UnitNode *sourceParent,
                int sourceChild,
                UnitNode *destinationParent,
                int destinationChild);
    void connectUN(UnitNode *unMaster,
                   UnitNode *unSlave);
    void disconnectUN(UnitNode *unMaster,
                      UnitNode *unSlave);
    void newMSGToSend();
    void newMSGToSend(QByteArray msg,
                      QByteArray btLink);
    void currentWorkModeChanged(int mode);
    void connectedVK(UnitNode * objPtr);

    void ssPostponedCommandCALL(int timeout);
    void ssPostponedCommandKILL(int timeout);
    void ssPostponedCommandLIST(int timeout);
    void ssPostponedCommandINQUIRY(int timeout);
    void ssPostponedCommandTXPOWER(int timeout);
    void ssPostponedCommandRSSI(int timeout);
    void ssDisconnectOfListConnect(int timeout);
    void ssKillConnection(int timeout);
    void ssPostponedCommandB1Start(int timeout,
                                   UnitNode *unSender);
    void ssPostponedCommandB1Stop();
    void ssPostponedCommandC0Start(int timeout,
                                   UnitNode *unSender);
    void ssPostponedCommandC0Stop();
    void ssPostponedCommandB3Stop();
    void ssPostponedCommandB4Stop();
    void ssPostponedCommandB3Start(int timeout,
                                   UnitNode* unSender);
    void ssPostponedCommandB4Start(int timeout,
                                   UnitNode* unSender);

    void protocolMessage(quint8 type,
                         QByteArray adress,
                         QByteArray msg);
    void message(UnitNode *unSender,
                 quint8 type,
                 QByteArray msg,
                 bool ioType);
    void message(UnitNode *unSender,
                 quint8 type,
                 QByteArray msg,
                 bool ioType,
                 quint32 idDBMSG);
//    void progressC3(UnitNode *unSender,
//                    quint8 nomS,
//                    quint8 progress);
    void updateProgressShotLoad(UnitNode *unSender,
                                quint8 nomShot,
                                quint8 nomTrain,
                                quint8 progress);
    void ssPostponedB1(int timeout);
    void ssPostponedB3(int timeout);
    void ssPostponedB4(int timeout);
    void ssReInterrogateBT(int timeout);
    void ssPostponedA1(int timeout);
    void ssPostponedNeedSendB3(UnitNode *unSender,
                               int timeout);
    void ssPostponedNeedSendB4(UnitNode *unSender,
                               int timeout);
    void lastSent(QByteArray mac,
                  QByteArray btLink,
                  QByteArray msg,
                  int timeout);
    void ssOffStatusKMExchange(int timeout);
    void startWaitEndC3(int timeout);
    void stopWaitEndC3();
    void updateSettingUN(UnitNode *un,
                         QByteArray msg3F);
    void setAlarmCondition(UnitNode *un);
};

#endif // BTMSGMANAGER_H
