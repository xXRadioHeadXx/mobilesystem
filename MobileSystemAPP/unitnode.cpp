﻿#include <unitnode.h>

UnitNode::UnitNode(QObject *parent) /*noexcept*/ :
    QObject(parent)
{
    try
    {
        graphicsTxtItem = nullptr;
        graphicsPxmItem = nullptr;
        this->clear();
        timerPostponedNeedSendB4VK = new QTimer(this);
        timerPostponedNeedSendB3VK = new QTimer(this);
        timerPostponedNeedSendB1VK = new QTimer(this);
        timerPostponedNeedSendB4_VK1 = new QTimer(this);
        timerPostponedNeedSendB3_VK1 = new QTimer(this);
        timerPostponedNeedSendB1_VK1 = new QTimer(this);
        timerPostponedNeedSendB4_VK2 = new QTimer(this);
        timerPostponedNeedSendB3_VK2 = new QTimer(this);
        timerPostponedNeedSendB1_VK2 = new QTimer(this);
    //    timerAlarm = new QTimer(this);

        connect(timerPostponedNeedSendB1VK,
                SIGNAL(timeout()),
                this,
                SLOT(emitB1()));

        connect(timerPostponedNeedSendB3VK,
                SIGNAL(timeout()),
                this,
                SLOT(emitB3()));

        connect(timerPostponedNeedSendB4VK,
                SIGNAL(timeout()),
                this,
                SLOT(emitB4()));

        connect(timerPostponedNeedSendB1_VK1,
                SIGNAL(timeout()),
                this,
                SLOT(emitB1_VK1()));

        connect(timerPostponedNeedSendB3_VK1,
                SIGNAL(timeout()),
                this,
                SLOT(emitB3_VK1()));

        connect(timerPostponedNeedSendB4_VK1,
                SIGNAL(timeout()),
                this,
                SLOT(emitB4_VK1()));

        connect(timerPostponedNeedSendB1_VK2,
                SIGNAL(timeout()),
                this,
                SLOT(emitB1_VK2()));

        connect(timerPostponedNeedSendB3_VK2,
                SIGNAL(timeout()),
                this,
                SLOT(emitB3_VK2()));

        connect(timerPostponedNeedSendB4_VK2,
                SIGNAL(timeout()),
                this,
                SLOT(emitB4_VK2()));

    //    connect(timerAlarm,
    //            SIGNAL(timeout()),
    //            this,
    //            SLOT(offAlarmCondition()));
    }
    catch (std::runtime_error &rte)
    {
        qWarning("catch runtime_error %s [UnitNode::UnitNode]", rte.what());
    }
    catch (std::bad_alloc& ba)
    {
        qWarning("catch bad_alloc %s [UnitNode::UnitNode]", ba.what());
    }
    catch (std::exception &e)
    {
        qWarning("catch exception %s [UnitNode::UnitNode]", e.what());
    }
    catch (...)
    {
        qWarning("catch exception <unknown> [UnitNode::UnitNode]");
    }
}

UnitNode::~UnitNode() /*noexcept*/
{
    try
    {
        this->clear();
        delete timerPostponedNeedSendB4VK;
        delete timerPostponedNeedSendB3VK;
        delete timerPostponedNeedSendB1VK;
        delete timerPostponedNeedSendB4_VK1;
        delete timerPostponedNeedSendB3_VK1;
        delete timerPostponedNeedSendB1_VK1;
        delete timerPostponedNeedSendB4_VK2;
        delete timerPostponedNeedSendB3_VK2;
        delete timerPostponedNeedSendB1_VK2;
//        delete timerAlarm;
    }

    catch(...)
    {
#ifdef QT_DEBUG
        qDebug() << "~Assert()";
#endif
        return;
    }
}

void UnitNode::clear() /*noexcept*/
{
    try
    {
        //
        listSettingVK.clear();
        listSettingVK.append(SettingVK());
        listSettingVK.append(SettingVK());

        QByteArray nullBSK;
        nullBSK.append((char)0x00);
        nullBSK.append((char)0x00);
        listSettingVK[0].listIDBSK.clear();
        listSettingVK[1].listIDBSK.clear();
        for(int i(0); i < 20; i++)
        {
            listSettingVK[0].listIDBSK.append(nullBSK);
            listSettingVK[1].listIDBSK.append(nullBSK);
        }

        listSettingVK[0].bskSO =
        listSettingVK[1].bskSO =
        listSettingVK[0].logic =
        listSettingVK[1].logic =
        listSettingVK[0].timeout2 =
        listSettingVK[1].timeout2 =
        listSettingVK[0].timeout3 =
        listSettingVK[1].timeout3 = 0x00;

        listSettingVK[0].timeout1 =
        listSettingVK[1].timeout1 = 0x0A;

        listSettingVK[0].otherSO =
        listSettingVK[1].otherSO =
        listSettingVK[0].selfSO =
        listSettingVK[1].selfSO =
        listSettingVK[0].numComplex =
        listSettingVK[1].numComplex = 0x01;

        listSettingVK[0].flag_connection =
        listSettingVK[1].flag_connection = true;

        listSettingVK[0].flag_discharge =
        listSettingVK[1].flag_discharge =
        listSettingVK[0].flag_alarm =
        listSettingVK[1].flag_alarm =
        listSettingVK[0].flag_attention =
        listSettingVK[1].flag_attention =
        listSettingVK[0].flag_failure =
        listSettingVK[1].flag_failure =
        listSettingVK[0].flag_off =
        listSettingVK[1].flag_off = false;

        listSettingVK[0].modeDayNight =
        listSettingVK[1].modeDayNight = 0x00;

        listSettingVK[0].luminanceLevel =
        listSettingVK[1].luminanceLevel =
        listSettingVK[0].luminanceThreshold =
        listSettingVK[1].luminanceThreshold = -1;

        flag_selfD =
        flag_otherD1 =
        flag_otherD2 = 0xFF;

        listTrainIMG.clear();
        listTrainIMG.append(TrainIMG());
        listTrainIMG.append(TrainIMG());

        listTrainIMG[0].listColleclorIMG.clear();
        listTrainIMG[1].listColleclorIMG.clear();

        listTrainIMG[0].mainIDMSG0xC1 =
        listTrainIMG[1].mainIDMSG0xC1 = 0x00;

        listTrainIMG[0].doneStatus =
        listTrainIMG[1].doneStatus = false;
        //
        unTreeParent = nullptr;
        listChilde.clear();
        //свойства для идентификации -->
        id.clear(); //имя устройства bt
        mac.clear();//уникальный идентификатор
        name = "Unknown";//.clear();//преобретённое имя
        btLink.clear();;//последовательный набор линков для доступа
        btLink = "";
        typeUN = UnknownDeviceType;      //1-VK, 2-RT, 3-PKP
        numKit =
        voltage =       //задел
        numArea =    //под
        numNode = 0; //будующее
        //свойства для идентификации <--

        //свойства для сопряжения с другими -->
        unitMode = Undefined;
        flag_discharge =
        flag_connection =
        flag_attention =
        flag_failure =
        flag_off =
        flag_hidden = false;   ///эти флаги тоже надо обнулить

        txPower = rssi = 0;
        dbIndexParent = dbIndex = 0;

        expectInquryCount = 0;
        listInquiry.clear();

        expectConnect = 0;
    //    expectConnect.address.clear();
    //    expectConnect.mac.clear();
    //    expectConnect.name.clear();
    //    expectConnect.rssi =
    //    expectConnect.txPower = 0;
        expectConnectCount = 0;
        listConnect.clear();
        //свойства для сопряжения с другими <--

        //свойства для работы с изображениями -->
        currentNumShotVK =
        lastNumBlockVK =
        currentNumBlockVK = 0;
        beginVK =
        sizeBlockVK = 0;
        listIMGVK.clear();
        timeout1 = 10;
        timeout2 =
        timeout3 =
        idBSK1h =
        idBSK1l =
        idBSK2h =
        idBSK2l =
        idBSK3h =
        idBSK3l =
        modeAnxiety =0;
        modeVK =
        selfSO =
        bskSO =
        otherSO =
        logic =
        numComplex = 1;

        wayVK.clear();
        //свойства для работы с изображениями <--

        exceptPXM = QPixmap(":/icon/faq.png");
    //    if(QFile::exists("map.jpg"))
    //    {
    //        exceptPXM = QPixmap("map.jpg");
    //    }
    //    else
    //    {
    //        exceptPXM = QPixmap(200, 200);
    //        exceptPXM.fill(Qt::lightGray);
    //    }

        viewPXM.load(":/icon/faq.png");
        graphicsTxtItem = new GraphicsTextItem;
        graphicsPxmItem = new GraphicsPixmapItem;
        graphicsPxmItem->setSlaveItem(graphicsTxtItem);
        graphicsPxmItem->setSlaveItem(graphicsTxtItem);
        for(int i(0); i < 5; i++)
        {
            listLFlangP.append(new GraphicsPixmapItem);
            listRFlangP.append(new GraphicsPixmapItem);
            listLFlangP.last()->setPixmap(QPixmap(":/icon/front_green_5x5.png"));
            listRFlangP.last()->setPixmap(QPixmap(":/icon/front_green_5x5.png"));
        }
        circleZoneSO = new GraphicsPixmapItem;
        ellipsFrontZone = nullptr;//new QGraphicsEllipseItem;

        graphicsPxmItem->setSlaveLFlang(listLFlangP);
        graphicsPxmItem->setSlaveRFlang(listRFlangP);
        graphicsPxmItem->setPixmap(viewPXM);
        flag_alarm = false;
        autoRequestB1 = false;
    }
    catch (std::runtime_error &rte)
    {
        qWarning("catch runtime_error %s [UnitNode::clear]", rte.what());
    }
    catch (std::bad_alloc& ba)
    {
        qWarning("catch bad_alloc %s [UnitNode::clear]", ba.what());
    }
    catch (std::exception &e)
    {
        qWarning("catch exception %s [UnitNode::clear]", e.what());
    }
    catch (...)
    {
        qWarning("catch exception <unknown> [UnitNode::clear]");
    }

}

QByteArray UnitNode::updateBTLink(QByteArray newBTLink) /*noexcept*/
{
    if(!newBTLink.isEmpty())
    {
        if((quint8)0xFF == (quint8)newBTLink.right(1)[0])
        {
            newBTLink.chop(1);
        }
    }
    if(this->btLink != newBTLink ||
       true != this->flag_connection)
    {
        this->flag_connection = true;
        this->btLink = newBTLink;
        this->emitUpdateUN();
#ifdef QT_DEBUG
        qDebug() << "updateBTLink(" << this->mac << ") [" <<  this->btLink.toHex() << "]-->[" << newBTLink.toHex() << "]";
#endif
    }
    return this->btLink;
}

quint8 UnitNode::updateNumberArea(quint8 num) /*noexcept*/
{
    if(this->_numArea() != num)
    {
        this->numArea = num;
        this->emitUpdateUN();
        this->emitUploadUN();
    }
    return this->numArea;
}

quint8 UnitNode::updateNumberKit(quint8 num) /*noexcept*/
{
    if(this->_numKit() != num)
    {
        this->numKit = num;
        this->emitUpdateUN();
//        this->emitUploadUN();
    }
    return this->numKit;
}


quint8 UnitNode::updateNumberNode(quint8 num) /*noexcept*/
{
    if(this->_numNode() != num)
    {
        this->numNode = num;
        this->emitUpdateUN();
        this->emitUploadUN();
    }
    return this->numNode;
}

bool UnitNode::updateFlagConnection(bool connectionValid) /*noexcept*/
{
    if(this->flag_connection != connectionValid)
    {
        this->flag_connection = connectionValid;
        if(!this->flag_connection)
        {
            this->btLink.clear();
            this->rssi = 0;
            this->txPower = 0;
        }
        this->emitUpdateUN();
//        emit this->updateUN(this);
    }
    return this->flag_connection;
}

QByteArray UnitNode::updateAbstractName(QByteArray name) /*noexcept*/
{
    if(this->abstractName != name)
    {
        this->abstractName = name;
        this->emitUpdateUN();
        this->emitUploadUN();
    }
    return this->abstractName;
}

QByteArray UnitNode::updateMAC(QByteArray value) /*noexcept*/
{
    if(this->_mac() != value &&
       !value.isEmpty())
    {
        this->mac = value;
//        this->emitUpdateUN();
    }
    return this->mac;
}

QByteArray UnitNode::updateName(QByteArray name) /*noexcept*/
{
    if(this->name == name ||
       "Unknown" == name ||
       name.isEmpty())
    {
        return this->name;
    }
    this->name = name;
    if(KMSystemType == this->typeSystem)
    {
        if(this->name.size() > 3)
        {
            if("PKP" == this->name.left(3))
            {
                this->typeUN = PKP_KMDeviceType;
            }
            if("RT" == this->name.left(2))
            {
                this->typeUN = RT_KMDeviceType;
            }
            if("VK" == this->name.left(2))
            {
                this->typeUN = VK_KMDeviceType;
            }
            if("Unknown" == this->name)
            {
                this->typeUN = Unknown_KMDeviceType;
            }
            if(VK_KMDeviceType == this->_typeUN() ||
               RT_KMDeviceType == this->_typeUN())
            {
//                this->graphicsPxmItem->setFlag(QGraphicsItem::ItemIsMovable);
                this->graphicsPxmItem->setFlag(QGraphicsItem::ItemIsSelectable);
                this->graphicsPxmItem->setFlag(QGraphicsItem::ItemIsFocusable);
                this->graphicsPxmItem->setZValue(1);
            }
        }

        if(this->name.size() >= 10)
        {
            QByteArray id,
                       id_1(name.right(10)),
                       id_2(name.right(8)),
                       id_3(name.right(6)),
                       id_4(name.right(4)),
                       id_5(name.right(2));
            id_1.chop(8);
            id_2.chop(6);
            id_3.chop(4);
            id_4.chop(2);
            id.append(id_1.toUInt(0, 16));
            id.append(id_2.toUInt(0, 16));
            id.append(id_3.toUInt(0, 16));
            id.append(id_4.toUInt(0, 16));
            id.append(id_5.toUInt(0, 16));

            this->updateID(id);
        }
    }

    if(KSSystemType == this->typeSystem)
    {

    }

    if(BSKSystemType == this->typeSystem)
    {

    }
    return this->name;
}

quint32 UnitNode::updateDBIndexParent(quint32 indexParent) /*noexcept*/
{
    if(this->dbIndexParent != indexParent)
    {
        this->dbIndexParent = indexParent;
        this->emitUpdateUN();
    }
    return this->dbIndexParent;
}

quint32 UnitNode::updateDBIndex(quint32 index) /*noexcept*/
{
    if(0 != index &&
       this->dbIndex != index)
    {
        this->dbIndex = index;
        this->emitUpdateUN();
    }
    return this->dbIndex;
}

QByteArray UnitNode::updateID(QByteArray id) /*noexcept*/
{
    if(this->id != id)
    {
        this->id = id;
        this->emitUpdateUN();
    }
    return this->id;
}

TypeUnitNodeDevice UnitNode::updateTypeUN(TypeUnitNodeDevice value) /*noexcept*/
{
    if(this->_typeUN() != value)
    {
        this->typeUN = value;
        if(UnknownDeviceType != this->_typeUN())
        {
        }
        else
        {
            this->graphicsPxmItem->setPixmap(exceptPXM);
        }
        this->emitUpdateUN();
    }
    return this->typeUN;
}

TypeUnitNodeSystem UnitNode::updateTypeSystem(TypeUnitNodeSystem value) /*noexcept*/
{
    if(this->typeSystem != value)
    {
        this->typeSystem = value;
//        this->emitUpdateUN();
    }
    return this->typeSystem;
}

void UnitNode::updateType(TypeUnitNodeSystem sysType,
                          TypeUnitNodeDevice unType) /*noexcept*/
{
    this->updateTypeSystem(sysType);
    this->updateTypeUN(unType);
//    this->emitUpdateUN();
}

int UnitNode::updateTxPower(int value) /*noexcept*/
{
    if(this->txPower != value)
    {
        this->txPower = value;
        this->emitUpdateUN();
//        emit this->updateUN(this);
    }
    return this->txPower;
}

int UnitNode::updateRssi(int value) /*noexcept*/
{
    if(this->rssi != value)
    {
        this->rssi = value;
        this->emitUpdateUN();
    }
    return this->rssi;
}

bool UnitNode::updateFlagDischarge(bool value) /*noexcept*/
{
    if(this->flag_discharge != value)
    {
        this->flag_discharge = value;
        this->emitUpdateUN();
    }
    return this->flag_discharge;
}

bool UnitNode::updateFlagAlarm(bool value) /*noexcept*/
{
    if(this->flag_alarm != value)
    {
        this->flag_alarm = value;
        this->emitUpdateUN();
    }
    return this->flag_alarm;
}

bool UnitNode::updateFlagAttention(bool value) /*noexcept*/
{
    if(this->flag_attention != value)
    {
        this->flag_attention = value;
        this->emitUpdateUN();
    }
    return flag_attention;
}

bool UnitNode::updateFlagFailure(bool value) /*noexcept*/
{
    if(this->flag_failure != value)
    {
        this->flag_failure = value;
        this->emitUpdateUN();
    }
    return this->flag_failure;
}

bool UnitNode::updateFlagOff(bool value) /*noexcept*/
{
    if(this->flag_off != value)
    {
        this->flag_off = value;
        this->emitUpdateUN();
    }
    return this->flag_off;
}

int UnitNode::updateVoltage(int value) /*noexcept*/
{
    if(0 != value &&
       this->voltage != value)
    {
        this->voltage = value;
        this->emitUpdateUN();
    }
    return this->voltage;
}

quint8 UnitNode::updateAnxiety(quint8 value) /*noexcept*/
{
    if(this->modeAnxiety != value)
    {
        this->modeAnxiety = value;
        this->emitUpdateUN();
    }
    return this->modeAnxiety;
}

quint8 UnitNode::updateModeDayNight(quint8 numVK,
                                    quint8 value) /*noexcept*/
{
    if(0x01 != numVK &&
       0x02 != numVK)
        return 0;
    numVK--;

    if(this->listSettingVK.at(numVK).modeDayNight != value)
    {
        this->listSettingVK[numVK].modeDayNight = value;
        this->emitUpdateUN();
    }
    return this->listSettingVK.at(numVK).modeDayNight;
}


int UnitNode::updateLuminanceLevel(quint8 numVK,
                                   int value) /*noexcept*/
{
    if(0x01 != numVK &&
       0x02 != numVK)
        return -1;
    numVK--;

    if(this->listSettingVK.at(numVK).luminanceLevel != value)
    {
        this->listSettingVK[numVK].luminanceLevel = value;
        this->emitUpdateUN();
    }
    return this->listSettingVK.at(numVK).luminanceLevel;
}

int UnitNode::updateLuminanceThreshold(quint8 numVK,
                                       int value) /*noexcept*/
{
    if(0x01 != numVK &&
       0x02 != numVK)
        return -1;
    numVK--;

    if(this->listSettingVK.at(numVK).luminanceThreshold != value)
    {
        this->listSettingVK[numVK].luminanceThreshold = value;
        this->emitUpdateUN();
    }
    return this->listSettingVK.at(numVK).luminanceThreshold;
}

void UnitNode::updateListIDBSK(quint8 numVK,
                               quint8 indexSO,
                               QByteArray idSO) /*noexcept*/
{
    if(0x01 != numVK &&
       0x02 != numVK)
        return;
    numVK--;

    if(1 > indexSO &&
       20 < indexSO)
        return;
    indexSO--;

    if((this->listSettingVK.at(numVK).listIDBSK.size() - 1) < indexSO)
        return;
//    qDebug() << "UnitNode::updateListIDBSK(" << numVK << "," << indexSO << "," << idSO.toHex().toUpper() << ")";
    this->listSettingVK[numVK].listIDBSK[indexSO] = idSO;
    this->emitUploadUN();
    this->emitUpdateUN();
    return;
}

void UnitNode::removeListIDBSK(quint8 numVK,
                               quint8 indexSO,
                               QByteArray idSO) /*noexcept*/
{
    if(0x01 != numVK &&
       0x02 != numVK)
        return;
    numVK--;

    if(1 > indexSO &&
       20 < indexSO)
        return;
    indexSO--;

//    qDebug() << "UnitNode::updateListIDBSK(" << numVK << "," << indexSO << "," << idSO.toHex().toUpper() << ")";
    QByteArray nullID;
    nullID.append((char)0x00);
    nullID.append((char)0x00);
    while(true)
    {
        int index(this->listSettingVK.at(numVK).listIDBSK.indexOf(idSO));
        if(-1 == index)
        {
            this->emitUploadUN();
            this->emitUpdateUN();
            return;
        }
        else
        {
            this->listSettingVK[numVK].listIDBSK[index] = nullID;
        }
    }
}

void UnitNode::clearListIDBSK(quint8 numVK) /*noexcept*/
{
    if(0x01 != numVK &&
       0x02 != numVK)
        return;
    numVK--;

    QByteArray nullID;
    nullID.append((char)0x00);
    nullID.append((char)0x00);

    for(int i(0), n(this->listSettingVK.at(numVK).listIDBSK.size()); i < n; i++)
    {
        this->listSettingVK[numVK].listIDBSK[i] = nullID;
    }
    this->emitUploadUN();
    this->emitUpdateUN();
}

bool UnitNode::updateFlagHidden(bool value) /*noexcept*/
{
    if(this->flag_hidden != value)
    {
        this->flag_hidden = value;
        this->emitUploadUN();
        this->emitUpdateUN();
    }
    return this->flag_hidden;
}

quint8 UnitNode::updateFlagSelfD(quint8 value) /*noexcept*/
{
    if(value != this->flag_selfD)
    {
        this->flag_selfD = value;
        this->emitUpdateUN();
    }
    return this->flag_selfD;
}

quint8 UnitNode::updateFlagOtherD1(quint8 value) /*noexcept*/
{
    if(value != this->flag_otherD1)
    {
        this->flag_otherD1 = value;
        this->emitUpdateUN();
    }
    return this->flag_otherD1;
}

quint8 UnitNode::updateFlagOtherD2(quint8 value) /*noexcept*/
{
    if(value != this->flag_otherD2)
    {
        this->flag_otherD2 = value;
        this->emitUpdateUN();
    }
    return this->flag_otherD2;
}


void UnitNode::needSendB4(int ms_repeat) /*noexcept*/
{
    if(this->timerPostponedNeedSendB4VK->isActive())
        this->timerPostponedNeedSendB4VK->stop();
    if(this->timerPostponedNeedSendB3VK->isActive())
        this->timerPostponedNeedSendB3VK->stop();
    if(this->timerPostponedNeedSendB1VK->isActive())
        this->timerPostponedNeedSendB1VK->stop();

    this->emitB4();
//    this->timerPostponedNeedSendB4->singleShot(ms_repeat,
//                                               this,
//                                               SLOT(emitB4()));
}

void UnitNode::needSendB3(int ms_repeat) /*noexcept*/
{
    if(this->timerPostponedNeedSendB4VK->isActive())
        this->timerPostponedNeedSendB4VK->stop();
    if(this->timerPostponedNeedSendB3VK->isActive())
        this->timerPostponedNeedSendB3VK->stop();
    if(this->timerPostponedNeedSendB1VK->isActive())
        this->timerPostponedNeedSendB1VK->stop();

    this->emitB3();
    this->timerPostponedNeedSendB3VK->singleShot(ms_repeat,
                                               this,
                                               SLOT(emitB3()));
}

void UnitNode::needSendB1(int ms_repeat) /*noexcept*/
{
    if(this->timerPostponedNeedSendB4VK->isActive())
        this->timerPostponedNeedSendB4VK->stop();
    if(this->timerPostponedNeedSendB3VK->isActive())
        this->timerPostponedNeedSendB3VK->stop();
    if(this->timerPostponedNeedSendB1VK->isActive())
        this->timerPostponedNeedSendB1VK->stop();

    this->emitB1();
//    this->timerPostponedNeedSendB1->singleShot(ms_repeat,
//                                               this,
//                                               SLOT(emitB1()));
}

void UnitNode::stopTimerPostponedNeedSendB4() /*noexcept*/
{
    if(timerPostponedNeedSendB4VK->isActive())
        timerPostponedNeedSendB4VK->stop();
}

void UnitNode::stopTimerPostponedNeedSendB3() /*noexcept*/
{
    if(timerPostponedNeedSendB3VK->isActive())
        timerPostponedNeedSendB3VK->stop();
}

void UnitNode::stopTimerPostponedNeedSendB1() /*noexcept*/
{
    if(timerPostponedNeedSendB1VK->isActive())
        timerPostponedNeedSendB1VK->stop();
}

void UnitNode::emitB1() /*noexcept*/
{
    emit this->timeoutB1(this);
}

void UnitNode::emitB1_VK1() /*noexcept*/
{
    emit this->timeoutB1(this,
                         0x01);
}

void UnitNode::emitB1_VK2() /*noexcept*/
{
    emit this->timeoutB1(this,
                         0x02);
}

//void UnitNode::emitC0()
//{
//    emit this->timeoutC0(this);
//}


void UnitNode::emitB3() /*noexcept*/
{
    if(this->listIMGVK.size())
    {
        emit this->timeoutB3(this,
                             this->listIMGVK.first().numShot,
                             0,
                             0,
                             this->listIMGVK.first().releaseSize);
    }
    return;
    if(this->listTrainIMG.at(0).listColleclorIMG.size())
    {
        emit this->timeoutB3(this,
                             this->listTrainIMG.at(0).listColleclorIMG.first().numShot,
                             0,
                             0,
                             this->listTrainIMG.at(0).listColleclorIMG.first().releaseSize);
    }
}

void UnitNode::emitB3_VK1() /*noexcept*/
{
    if(this->listTrainIMG.at(0).listColleclorIMG.size())
    {
        emit this->timeoutB3(this,
                             0x01,
                             this->listTrainIMG.at(0).listColleclorIMG.first().numShot,
                             0,
                             0,
                             this->listTrainIMG.at(0).listColleclorIMG.first().releaseSize);
    }
}

void UnitNode::emitB3_VK2() /*noexcept*/
{
    if(this->listTrainIMG.at(1).listColleclorIMG.size())
    {
        emit this->timeoutB3(this,
                             0x01,
                             this->listTrainIMG.at(1).listColleclorIMG.first().numShot,
                             0,
                             0,
                             this->listTrainIMG.at(1).listColleclorIMG.first().releaseSize);
    }
}

void UnitNode::emitB4() /*noexcept*/
{
    quint32 defBS(0);
    if(this->listIMGVK.size())
    {
        for(quint8 i(0), n((quint8)this->listIMGVK.first().listPiece.size()); i < n; i++)
        {
            if(defBS < (quint32)this->listIMGVK.first().listPiece.at(i).piece.size())
                defBS = (quint32)this->listIMGVK.first().listPiece.at(i).piece.size();
            if(this->listIMGVK.first().listPiece.at(i).piece.isEmpty())
            {
                if(0 == defBS)
                    defBS = 500;
                if((quint32)(i * defBS + defBS) > this->listIMGVK.first().releaseSize)
                {
                    emit this->timeoutB4(this,
                                         (quint8)this->listIMGVK.first().numShot,
                                         this->lastNumBlockVK = i + 1,
                                         i * defBS,
                                         this->listIMGVK.first().releaseSize - i * defBS);
                }
                else
                {
                    emit this->timeoutB4(this,
                                         (quint8)this->listIMGVK.first().numShot,
                                         this->lastNumBlockVK = i + 1,
                                         i * defBS,
                                         defBS);

                }
                return;
            }
        }

        quint8 i((quint8)this->listIMGVK.first().listPiece.size());
        if((quint32)(i * defBS + defBS) > this->listIMGVK.first().releaseSize)
        {
            emit this->timeoutB4(this,
                                 (quint8)this->listIMGVK.first().numShot,
                                 this->lastNumBlockVK = i + 1,
                                 i * defBS,
                                 this->listIMGVK.first().releaseSize - i * defBS);
        }
        else
        {
            emit this->timeoutB4(this,
                                 (quint8)this->listIMGVK.first().numShot,
                                 this->lastNumBlockVK = i + 1,
                                 i * defBS,
                                 defBS);

        }
    }
    return;
    if(this->listTrainIMG.at(0).listColleclorIMG.size())
    {
        for(quint8 i(0), n((quint8)this->listTrainIMG.at(0).listColleclorIMG.first().listPiece.size()); i < n; i++)
        {
            if(defBS < (quint32)this->listTrainIMG.at(0).listColleclorIMG.first().listPiece.at(i).piece.size())
                defBS = (quint32)this->listTrainIMG.at(0).listColleclorIMG.first().listPiece.at(i).piece.size();
            if(this->listTrainIMG.at(0).listColleclorIMG.first().listPiece.at(i).piece.isEmpty())
            {
                if(0 == defBS)
                    defBS = 500;
                if((quint32)(i * defBS + defBS) > this->listTrainIMG.at(0).listColleclorIMG.first().releaseSize)
                {
                    emit this->timeoutB4(this,
                                         0x01,
                                         (quint8)this->listTrainIMG.at(0).listColleclorIMG.first().numShot,
                                         this->listTrainIMG[0].listColleclorIMG[0].lastNumBlock = i + 1,
                                         i * defBS,
                                         this->listTrainIMG.at(0).listColleclorIMG.first().releaseSize - i * defBS);
                }
                else
                {
                    emit this->timeoutB4(this,
                                         0x01,
                                         (quint8)this->listTrainIMG.at(0).listColleclorIMG.first().numShot,
                                         this->listTrainIMG[0].listColleclorIMG[0].lastNumBlock = i + 1,
                                         i * defBS,
                                         defBS);

                }
                return;
            }
        }

        quint8 i((quint8)this->listTrainIMG.at(0).listColleclorIMG.first().listPiece.size());
        if((quint32)(i * defBS + defBS) > this->listTrainIMG.at(0).listColleclorIMG.first().releaseSize)
        {
            emit this->timeoutB4(this,
                                 (quint8)this->listTrainIMG.at(0).listColleclorIMG.first().numShot,
                                 this->listTrainIMG[0].listColleclorIMG[0].lastNumBlock = i + 1,
                                 i * defBS,
                                 this->listTrainIMG.at(0).listColleclorIMG.first().releaseSize - i * defBS);
        }
        else
        {
            emit this->timeoutB4(this,
                                 (quint8)this->listTrainIMG.at(0).listColleclorIMG.first().numShot,
                                 this->listTrainIMG[0].listColleclorIMG[0].lastNumBlock = i + 1,
                                 i * defBS,
                                 defBS);

        }

    }
}

void UnitNode::emitB4_VK1() /*noexcept*/
{
    quint32 defBS(0);
    if(this->listTrainIMG.at(0).listColleclorIMG.size())
    {
        for(quint8 i(0), n((quint8)this->listTrainIMG.at(0).listColleclorIMG.first().listPiece.size()); i < n; i++)
        {
            if(defBS < (quint32)this->listTrainIMG.at(0).listColleclorIMG.first().listPiece.at(i).piece.size())
                defBS = (quint32)this->listTrainIMG.at(0).listColleclorIMG.first().listPiece.at(i).piece.size();
            if(this->listTrainIMG.at(0).listColleclorIMG.first().listPiece.at(i).piece.isEmpty())
            {
                if(0 == defBS)
                    defBS = 500;
                if((quint32)(i * defBS + defBS) > this->listTrainIMG.at(0).listColleclorIMG.first().releaseSize)
                {
                    emit this->timeoutB4(this,
                                         0x01,
                                         (quint8)this->listTrainIMG.at(0).listColleclorIMG.first().numShot,
                                         this->listTrainIMG[0].listColleclorIMG[0].lastNumBlock = i + 1,
                                         i * defBS,
                                         this->listTrainIMG.at(0).listColleclorIMG.first().releaseSize - i * defBS);
                }
                else
                {
                    emit this->timeoutB4(this,
                                         0x01,
                                         (quint8)this->listTrainIMG.at(0).listColleclorIMG.first().numShot,
                                         this->listTrainIMG[0].listColleclorIMG[0].lastNumBlock = i + 1,
                                         i * defBS,
                                         defBS);

                }
                return;
            }
        }

        quint8 i((quint8)this->listTrainIMG.at(0).listColleclorIMG.first().listPiece.size());
        if((quint32)(i * defBS + defBS) > this->listTrainIMG.at(0).listColleclorIMG.first().releaseSize)
        {
            emit this->timeoutB4(this,
                                 (quint8)this->listTrainIMG.at(0).listColleclorIMG.first().numShot,
                                 this->listTrainIMG[0].listColleclorIMG[0].lastNumBlock = i + 1,
                                 i * defBS,
                                 this->listTrainIMG.at(0).listColleclorIMG.first().releaseSize - i * defBS);
        }
        else
        {
            emit this->timeoutB4(this,
                                 (quint8)this->listTrainIMG.at(0).listColleclorIMG.first().numShot,
                                 this->listTrainIMG[0].listColleclorIMG[0].lastNumBlock = i + 1,
                                 i * defBS,
                                 defBS);
        }
    }
}

void UnitNode::emitB4_VK2() /*noexcept*/
{
    quint32 defBS(0);
    if(this->listTrainIMG.at(0).listColleclorIMG.size())
    {
        for(quint8 i(0), n((quint8)this->listTrainIMG.at(0).listColleclorIMG.first().listPiece.size()); i < n; i++)
        {
            if(defBS < (quint32)this->listTrainIMG.at(0).listColleclorIMG.first().listPiece.at(i).piece.size())
                defBS = (quint32)this->listTrainIMG.at(0).listColleclorIMG.first().listPiece.at(i).piece.size();
            if(this->listTrainIMG.at(0).listColleclorIMG.first().listPiece.at(i).piece.isEmpty())
            {
                if(0 == defBS)
                    defBS = 500;
                if((quint32)(i * defBS + defBS) > this->listTrainIMG.at(0).listColleclorIMG.first().releaseSize)
                {
                    emit this->timeoutB4(this,
                                         0x01,
                                         (quint8)this->listTrainIMG.at(0).listColleclorIMG.first().numShot,
                                         this->listTrainIMG[0].listColleclorIMG[0].lastNumBlock = i + 1,
                                         i * defBS,
                                         this->listTrainIMG.at(0).listColleclorIMG.first().releaseSize - i * defBS);
                }
                else
                {
                    emit this->timeoutB4(this,
                                         0x01,
                                         (quint8)this->listTrainIMG.at(0).listColleclorIMG.first().numShot,
                                         this->listTrainIMG[0].listColleclorIMG[0].lastNumBlock = i + 1,
                                         i * defBS,
                                         defBS);

                }
                return;
            }
        }

        quint8 i((quint8)this->listTrainIMG.at(0).listColleclorIMG.first().listPiece.size());
        if((quint32)(i * defBS + defBS) > this->listTrainIMG.at(0).listColleclorIMG.first().releaseSize)
        {
            emit this->timeoutB4(this,
                                 (quint8)this->listTrainIMG.at(0).listColleclorIMG.first().numShot,
                                 this->listTrainIMG[0].listColleclorIMG[0].lastNumBlock = i + 1,
                                 i * defBS,
                                 this->listTrainIMG.at(0).listColleclorIMG.first().releaseSize - i * defBS);
        }
        else
        {
            emit this->timeoutB4(this,
                                 (quint8)this->listTrainIMG.at(0).listColleclorIMG.first().numShot,
                                 this->listTrainIMG[0].listColleclorIMG[0].lastNumBlock = i + 1,
                                 i * defBS,
                                 defBS);
        }
    }
}

bool UnitNode::deduceIMG(quint8 numShot,
                         quint8 numVK) /*noexcept*/
{
    for(int i(0); i < this->listIMGVK.size();)
    {
        if(numShot == this->listIMGVK.at(i).numShot)
        {
            this->listIMGVK.removeAt(i);
            return true;
        }
        else
        {
            i++;
        }
    }
    return false;
    //
    numVK--;
    for(int i(0); i < this->listTrainIMG.at(numVK).listColleclorIMG.size();)
    {
        if(numShot == this->listTrainIMG.at(numVK).listColleclorIMG.at(i).numShot)
        {
            this->listTrainIMG[numVK].listColleclorIMG.removeAt(i);
            return true;
        }
        else
        {
            i++;
        }
    }
    return false;
}

bool UnitNode::reserveIMG(quint8 numShot,
                          quint32 size,
                          quint8 numVK) /*noexcept*/
{
    for(int i(0); i < this->listIMGVK.size();)
    {
        if(numShot == this->listIMGVK.at(i).numShot)
        {
            if(this->listIMGVK[i].releaseSize == size)
            {
                this->listIMGVK[i].doneStatus = true;
                this->listIMGVK[i].currentSize =
                this->listIMGVK[i].lastNumBlock =
                this->listIMGVK[i].currentNumBlock = 0;
                this->listIMGVK[i].releaseSize = size;
                this->listIMGVK[i].releaseIMG.clear();
                this->listIMGVK[i].numShot = numShot;
                return false;
            }
            else
            {
                this->listIMGVK.removeAt(i);
                break;
            }
        }
        else
        {
            i++;
        }
    }

    ColleclorIMG newIMG;
    newIMG.doneStatus = true;
    newIMG.currentSize =
    newIMG.lastNumBlock =
    newIMG.currentNumBlock = 0;
    newIMG.releaseSize = size;
    newIMG.releaseIMG.clear();
    newIMG.numShot = numShot;
    this->listIMGVK.append(newIMG);
    return true;
    //
    numVK--;
    for(int i(0); i < this->listTrainIMG.at(numVK).listColleclorIMG.size();)
    {
        if(numShot == this->listTrainIMG.at(numVK).listColleclorIMG.at(i).numShot)
        {
            if(this->listTrainIMG.at(numVK).listColleclorIMG.at(i).releaseSize == size)
            {
                this->listTrainIMG[numVK].listColleclorIMG[i].doneStatus = true;
                this->listTrainIMG[numVK].listColleclorIMG[i].currentSize =
                this->listTrainIMG[numVK].listColleclorIMG[i].lastNumBlock =
                this->listTrainIMG[numVK].listColleclorIMG[i].currentNumBlock = 0;
                this->listTrainIMG[numVK].listColleclorIMG[i].releaseSize = size;
                this->listTrainIMG[numVK].listColleclorIMG[i].releaseIMG.clear();
                this->listTrainIMG[numVK].listColleclorIMG[i].numShot = numShot;
                return false;
            }
            else
            {
                this->listTrainIMG[numVK].listColleclorIMG.removeAt(i);
                break;
            }
        }
        else
        {
            i++;
        }
    }

    ColleclorIMG newColleclorIMG;
    newColleclorIMG.doneStatus = true;
    newColleclorIMG.currentSize =
    newColleclorIMG.lastNumBlock =
    newColleclorIMG.currentNumBlock = 0;
    newColleclorIMG.releaseSize = size;
    newColleclorIMG.releaseIMG.clear();
    newColleclorIMG.numShot = numShot;
    this->listTrainIMG[numVK].listColleclorIMG.append(newColleclorIMG);
    return true;
}

void UnitNode::appendChild(UnitNode *un) /*noexcept*/
{
    if(!un)
    {
#ifdef QT_DEBUG
        qDebug() << "UnitNode::appendChild Error " << this->listChilde.size();
#endif
        return;
    }
    if(!this->listChilde.contains(un))
    {
        this->listChilde.append(un);
        un->unTreeParent = this;
        un->updateDBIndexParent(this->dbIndex);
//        un->graphicsItem->setParentItem(this->graphicsItem);
//        un->graphicsItem->setZValue(un->unTreeParent->graphicsItem->zValue() + 1.0);
    }
//    qDebug() << "UnitNode::appendChild " << this->listChilde.size();
}

UnitNode* UnitNode::child(int num) /*noexcept*/
{
    return listChilde.value(num, 0);
}

int UnitNode::childCount() const /*noexcept*/
{
    return this->listChilde.size();
}

int UnitNode::row() const /*noexcept*/
{
    if (unTreeParent)
        return unTreeParent->listChilde.indexOf(const_cast<UnitNode*>(this));
    return 0;
}

int UnitNode::columnCount() const /*noexcept*/
{
    return 4;//itemData.count();
}

QVariant UnitNode::data(int column) const /*noexcept*/
{
    if(UnknownDeviceType == this->typeUN /*&&
       this->id.isEmpty() &&
       this->name.isEmpty()*/)
    {
        switch(column)
        {
            case 0:
            {
                return QVariant(QObject::tr("Device"));
            }
            case 1:
            {
                return QVariant(QObject::tr("S/Num"));
            }
            case 2:
            {
                return QVariant(QObject::tr("U, pow"));
            }
            case 3:
            {
                return QVariant(QObject::tr("ID"));
            }
        }
    }
    else
    {
        switch(column)
        {
            case 0:
            {

                if(this->id.isEmpty() ||
                   this->name.isEmpty())
                {
                    return QVariant(QObject::tr("Unknown"));
                }

                QString str;
                str.append(ProxyMetod().getNameDevice(this->id, this->typeSystem));
                if(!this->abstractName.isEmpty())
                {
                   str.append("\n");
                   str.append(QString(this->abstractName));
                }
                return str;
            }
            case 1:
            {

                if(this->id.isEmpty() ||
                   this->name.isEmpty())
                {
                    return QVariant(QObject::tr("N/A"));
                }

                QString str("%1/%2");
                str = str.arg(this->numArea)
                         .arg(this->numNode);
                return QVariant(str);
            }
            case 2:
            {

                if(this->id.isEmpty() ||
                   this->name.isEmpty())
                {
                    return QVariant(QObject::tr("N/A"));
                }

                if(this->voltage)
                {
                    QString str("%1.%2");
                    str = str.arg(this->voltage / 0x10)
                             .arg(this->voltage & 0x0F);
                    return QVariant(str);
                }

                return QVariant(QObject::tr("N/A"));
            }
            case 3:
            {

                if(this->id.isEmpty() ||
                   this->name.isEmpty())
                {
                    return QVariant(QObject::tr("N/A"));
                }

                return QVariant(this->id.toHex().toUpper());
            }
        }

    }

    return 0;
}

//QVariant UnitNode::data(int column) const /*noexcept*/
//{
//    if(0x00 == this->typeSystem)
//    {
//        if(0x01 == this->typeUN  ||
//           0x02 == this->typeUN ||
//           0x03 == this->typeUN)
//        {
//            switch(column)
//            {
//                case 0:
//                {
//                    if(this->abstractName.isEmpty())
//                    {
//                        if(0x01 == this->typeUN)
//                        {
//                            return QVariant(QObject::tr("VK") + " [" + QString::fromLocal8Bit("Р") + this->id.right(2).toHex() + "]");
//                        }
//                        if(0x02 == this->typeUN)
//                        {
//                            return QVariant(QObject::tr("RT") + " [" + QString::fromLocal8Bit("Р") + this->id.right(2).toHex() + "]");
//                        }
//                    }
//                    else
//                    {
//                        if(0x01 == this->typeUN)
//                        {
//                            return QVariant(this->abstractName + "\n" + QObject::tr("VK") + " [" + QString::fromLocal8Bit("Р") + this->id.right(2).toHex() + "]");
//                        }
//                        if(0x02 == this->typeUN)
//                        {
//                            return QVariant(this->abstractName + "\n" + QObject::tr("RT") + " [" + QString::fromLocal8Bit("Р") + this->id.right(2).toHex() + "]");
//                        }
//                    }

//                    return QVariant(this->abstractName + "\nid[" + this->id.toHex() + "]");
//                }
//                case 1:
//                {
//                    if(this->voltage)
//                    {
//                        QString str("%1.%2");
//                        str = str.arg(this->voltage / 0x10)
//                                 .arg(this->voltage & 0x0F);
//                        return QVariant(str);
//                    }

//                    return QVariant(QObject::tr("N/A"));
//                }
//                case 2:
//                {
//                    QString str("%1/%2");
//                    str = str.arg(this->numArea)
//                             .arg(this->numNode);
//                    return QVariant(str);
//                }
//                case 3:
//                {
//                    return QVariant(this->name);
//                }
//            }
//        }
//    }

//    if(0x01 == this->typeSystem)
//    {
//        if(0x00 == this->typeUN  ||
//           0x01 == this->typeUN  ||
//           0x09 == this->typeUN ||
//           0x0A == this->typeUN)
//        {
//            switch(column)
//            {
//                case 0:
//                {
//                    if(this->abstractName.isEmpty())
//                    {
//                        if(0x00 == this->typeUN)
//                        {
//                            return QVariant(tr("AWS") + " [" + this->id.right(2).toHex() + "]");
//                        }
//                        if(0x09 == this->typeUN ||
//                           0x0A == this->typeUN)
//                        {
//                            return QVariant(QObject::tr("VK") + " [" + this->id.right(2).toHex() + "]");
//                        }
//                        if(0x01 == this->typeUN)
//                        {
//                            return QVariant(QObject::tr("RT") + " [" + this->id.right(2).toHex() + "]");
//                        }
//                    }
//                    else
//                    {
//                        if(0x00 == this->typeUN)
//                        {
//                            return QVariant(QObject::tr("AWS") + " [" + this->id.right(2).toHex() + "]");
//                        }
//                        if(0x09 == this->typeUN ||
//                           0x0A == this->typeUN)
//                        {
//                            return QVariant(this->abstractName + "\n" + QObject::tr("VK") + " [" + this->id.right(2).toHex() + "]");
//                        }
//                        if(0x01 == this->typeUN)
//                        {
//                            return QVariant(this->abstractName + "\n" + QObject::tr("RT") + " [" + this->id.right(2).toHex() + "]");
//                        }
//                    }

//                    return QVariant(this->abstractName + "\nid[" + this->id.toHex() + "]");
//                }
//                case 1:
//                {
//                    if(this->voltage)
//                    {
//                        QString str("%1.%2");
//                        str = str.arg(this->voltage / 0x10)
//                                 .arg(this->voltage & 0x0F);
//                        return QVariant(str);
//                    }

//                    return QVariant(QObject::tr("N/A"));
//                }
//                case 2:
//                {
//                    QString str("%1/%2");
//                    str = str.arg(this->numArea)
//                             .arg(this->numNode);
//                    return QVariant(str);
//                }
//                case 3:
//                {
//                    return QVariant(this->name);
//                }
//            }
//        }
//    }

//    if(0x02 == this->typeSystem)
//    {
//        if(0xFF != this->typeUN)
//        {
//            switch(column)
//            {
//                case 0:
//                {
//                    QString strType;
//                    switch(this->typeUN)
//                    {
//                        case (quint8)0x00:
//                        {
//                            strType = QObject::tr("BSK-AWS");
//                            break;
//                        }
//                        case (quint8)0x01:
//                        {
//                            strType = QObject::tr("BSK-RT");
//                            break;
//                        }
//                        case (quint8)0x02:
//                        {
//                            strType = QObject::tr("BSK-S");
//                            break;
//                        }
//                        case (quint8)0x03:
//                        {
//                            strType = QObject::tr("BSK-RLO");
//                            break;
//                        }
//                        case (quint8)0x04:
//                        {
//                            strType = QObject::tr("BSK-RLD");
//                            break;
//                        }
//                        case (quint8)0x05:
//                        {
//                            strType = QObject::tr("BSK-RVD");
//                            break;
//                        }
//                        case (quint8)0x06:
//                        {
//                            strType = QObject::tr("BSK-MSO");
//                            break;
//                        }
//                        case (quint8)0x07:
//                        {
//                            strType = tr("BSK-BVU");
//                            break;
//                        }
//                        case (quint8)0x09:
//                        {
//                            strType = QObject::tr("BSK-VK");
//                            break;
//                        }
//                        case (quint8)0x0A:
//                        {
//                            strType = QObject::tr("BSK-O");
//                            break;
//                        }
//                        case (quint8)0x0B:
//                        {
//                            strType = QObject::tr("BSK-IK ");
//                            break;
//                        }
//                        case (quint8)0x0D:
//                        {
//                            strType = QObject::tr("BSK-RVP");
//                            break;
//                        }
//                        case (quint8)0x0F:
//                        {
//                            strType = QObject::tr("BSK-VSO");
//                            break;
//                        }
//                        case (quint8)0x0C:
//                        {
//                            strType = QObject::tr("BSK-RV");
//                            break;
//                        }
//                        default:
//                        {
//                            strType = QObject::tr("BSK-SO");
//                            break;
//                        }
//                    }

//                    if(this->abstractName.isEmpty())
//                    {
//                        return QVariant(strType + " [" + this->id.right(2).toHex() + "]");
//                    }
//                    else
//                    {
//                        return QVariant(strType + " [" + this->id.right(2).toHex() + "]");
//                    }

//                    return QVariant(this->abstractName + "\nid[" + this->id.toHex() + "]");
//                }
//                case 1:
//                {
//                    if(this->voltage)
//                    {
//                        QString str("%1.%2");
//                        str = str.arg(this->voltage / 0x10)
//                                 .arg(this->voltage & 0x0F);
//                        return QVariant(str);
//                    }

//                    return QVariant(QObject::tr("N/A"));
//                }
//                case 2:
//                {
//                    QString sQObject::tr("%1/%2");
//                    str = str.arg(this->numArea)
//                             .arg(this->numNode);
//                    return QVariant(str);
//                }
//                case 3:
//                {
//                    return QVariant(this->name);
//                }
//            }
//        }
//    }

//    if(0xFF == this->typeUN &&
//       this->id.isEmpty() &&
//       this->name.isEmpty())
//    {
//        switch(column)
//        {
//            case 0:
//            {
//                return QVariant(QObject::tr("Device"));
//            }
//            case 1:
//            {
//                return QVariant(QObject::tr("U, pow"));
//            }
//            case 2:
//            {
//                return QVariant(QObject::tr("S/Num"));
//            }
//            case 3:
//            {
//                return QVariant(QObject::tr("Name"));
//            }
//        }
//    }

//    return 0;
//}

UnitNode* UnitNode::getUnitNodeTreeParent() /*noexcept*/
{
    return unTreeParent;
}

QPixmap UnitNode::setGIPXM(QPixmap pxm) /*noexcept*/
{
    if(pxm.isNull())
        return pxm;

//    pxm = pxm.scaled(20,
//                     20,
//                     Qt::KeepAspectRatio,
//                     Qt::FastTransformation);
//    pxm = pxm.scaled(20,
//                     20,
//                     Qt::KeepAspectRatio,
//                     Qt::SmoothTransformation);

    this->graphicsPxmItem->setPixmap(pxm);

    return pxm;
}

QPixmap UnitNode::setFrontGIPXM(QPixmap pxm) /*noexcept*/
{
    if(pxm.isNull())
        return pxm;

    if(BSKSystemType == this->_typeSystem() &&
       S_BSKDeviceType == this->_typeUN())
    {
        circleZoneSO->setPixmap(pxm);
    }
    else
    {
//        pxm = pxm.scaled(5,
//                         5,
//                         Qt::KeepAspectRatio,
//                         Qt::FastTransformation);
//        pxm = pxm.scaled(5,
//                         5,
//                         Qt::KeepAspectRatio,
//                         Qt::SmoothTransformation);

        for(auto item: listLFlangP)
        {
            item->setPixmap(pxm);
        }
        for(auto item: listRFlangP)
        {
            item->setPixmap(pxm);
        }
    }
    return pxm;
}

LinePackStruct UnitNode::possAddLine(GraphicsPixmapItem *begin,
                                          GraphicsPixmapItem *end) /*noexcept*/
{
    try
    {
        //    if((nullptr == begin &&
        //        nullptr == end) ||
        //       begin == end)
        //        return LinePackStruct;

            for(auto linePack: listLine)
            {
                if(linePack.begin == begin && linePack.end == end)
                {
                    linePack.line->setLine(QLineF(begin->pos().x() + (qreal)begin->pixmap().width() / 2,
                                                  begin->pos().y() + (qreal)begin->pixmap().height() / 2,
                                                  end->pos().x() + (qreal)end->pixmap().width() / 2,
                                                  end->pos().y() + (qreal)end->pixmap().height() / 2));
                    linePack.line->setPen(this->getViewFrontCircleZonePen());
        //            linePack.line->setPen(this->getViewPen());
                    return linePack;
                }
            }

            LinePackStruct newLine;
            newLine.begin = begin;
            newLine.end = end;
            newLine.line = new QGraphicsLineItem;
            newLine.line->setLine(QLineF(begin->pos().x() + (qreal)begin->pixmap().width() / 2,
                                         begin->pos().y() + (qreal)begin->pixmap().height() / 2,
                                         end->pos().x() + (qreal)end->pixmap().width() / 2,
                                         end->pos().y() + (qreal)end->pixmap().height() / 2));
            newLine.line->setPen(this->getViewFrontCircleZonePen());
        //    newLine.line->setPen(this->getViewPen());

            listLine.append(newLine);
            return newLine;
    }
    catch (std::runtime_error &rte)
    {
        qWarning("catch runtime_error %s [UnitNode::possAddLine]", rte.what());
    }
    catch (std::bad_alloc& ba)
    {
        qWarning("catch bad_alloc %s [UnitNode::possAddLine]", ba.what());
    }
    catch (std::exception &e)
    {
        qWarning("catch exception %s [UnitNode::possAddLine]", e.what());
    }
    catch (...)
    {
        qWarning("catch exception <unknown> [UnitNode::possAddLine]");
    }
    return LinePackStruct();
}

QGraphicsEllipseItem * UnitNode::possAddEllipse(QPointF p1,
                                                QPointF p2) /*noexcept*/
{
    try
    {
        QPointF o_point1(p1),
                o_point2(p2),
                oxy_point1(0.0, 0.0),
                oxy_point2(o_point2.x() - o_point1.x(), o_point2.y() - o_point1.y());
        QLineF o_line(p1, p2),
               oxy_line(oxy_point1, oxy_point2),
               oxo_line(oxy_point1, QPointF(oxy_line.length(), 0.0));
        qreal angle(oxy_line.angleTo(oxo_line)),
              dx(o_point1.x()),
              dy(o_point1.y());


        if(nullptr == this->ellipsFrontZone)
        {
            this->ellipsFrontZone = new QGraphicsEllipseItem(oxy_point1.x(),
                                                             oxy_point1.y() - (this->graphicsPxmItem->pixmap().height() / 2),
                                                             oxo_line.length(),
                                                             this->graphicsPxmItem->pixmap().height());
        }
        else
        {
            this->ellipsFrontZone->setPos(oxy_point1.x(),
                                          oxy_point1.y());
            qreal reversAngle(this->ellipsFrontZone->rotation());
            reversAngle = 360 - reversAngle;
            this->ellipsFrontZone->setRotation(reversAngle);
            this->ellipsFrontZone->setRect(oxy_point1.x(),
                                           oxy_point1.y() - (this->graphicsPxmItem->pixmap().height() / 2),
                                           oxo_line.length(),
                                           this->graphicsPxmItem->pixmap().height());


        }

        this->ellipsFrontZone->setPen(this->getViewFrontCircleZonePen());
        this->ellipsFrontZone->setBrush(this->getViewFrontCircleZoneBrush());

        this->ellipsFrontZone->setRotation(angle);
        this->ellipsFrontZone->moveBy(dx, dy);
        return this->ellipsFrontZone;
    }
    catch (std::runtime_error &rte)
    {
        qWarning("catch runtime_error %s [UnitNode::possAddEllipse]", rte.what());
    }
    catch (std::bad_alloc& ba)
    {
        qWarning("catch bad_alloc %s [UnitNode::possAddEllipse]", ba.what());
    }
    catch (std::exception &e)
    {
        qWarning("catch exception %s [UnitNode::possAddEllipse]", e.what());
    }
    catch (...)
    {
        qWarning("catch exception <unknown> [UnitNode::possAddEllipse]");
    }
    return nullptr;
}


QPen UnitNode::getViewPen() /*noexcept*/
{
    QPen pen;
    switch (this->_typeSystem())
    {
        case KMSystemType:
        {
            pen.setStyle(Qt::DashLine);
            break;
        }
        case KSSystemType:
        {
            pen.setStyle(Qt::DashLine);
            break;
        }
        case BSKSystemType:
        {
            pen.setStyle(Qt::SolidLine);
            break;
        }
        default:
        {
            pen.setStyle(Qt::DashDotDotLine);
            break;
        }
    }

    pen.setWidthF(2.5);
    if(this->flag_off)
    {
        pen.setColor(QColor(0xCF, 0xCF, 0xCF));//gray
        return pen;
    }

    if(!this->flag_connection)
    {
        pen.setColor(QColor(0xCF, 0xCF, 0xCF));//gray
        return pen;
    }

    if(this->flag_alarm)
    {
        pen.setColor(QColor(0xE3, 0x06, 0x13));//red
        return pen;
    }

    if(this->flag_failure)
    {
        pen.setColor(QColor(0xFF, 0xDD, 0x0E));//yello
        return pen;
    }

    if(this->flag_discharge)
    {
        pen.setColor(QColor(0x0F, 0x70, 0xB7));//blue
        return pen;
    }

    pen.setColor(QColor(0x00, 0x96, 0x40));//green
    return pen;
}


QPixmap UnitNode::getViewPXM() /*noexcept*/
{
    if(UnknownDeviceType == this->_typeUN())
    {
        return this->setGIPXM(exceptPXM);
    }

    if(KMSystemType == this->_typeSystem())
    {
        if(VK_KMDeviceType == this->_typeUN())
        {
            if(this->flag_off)
            {
                viewPXM.load(":/icon/camera_gray_20x20.png");
                return this->setGIPXM(viewPXM);
            }

            if(!this->flag_connection)
            {
                viewPXM.load(":/icon/camera_q_20x20.png");
                return this->setGIPXM(viewPXM);
            }

            if(this->flag_alarm)
            {
                viewPXM.load(":/icon/camera_alarm_20x20.png");
                return this->setGIPXM(viewPXM);
            }

            if(this->flag_failure)
            {
                viewPXM.load(":/icon/camera_yell_20x20.png");
                return this->setGIPXM(viewPXM);
            }

            if(this->flag_discharge)
            {
                viewPXM.load(":/icon/camera_blue_20x20.png");
                return this->setGIPXM(viewPXM);
            }

            viewPXM.load(":/icon/camera_green_20x20.png");
            return this->setGIPXM(viewPXM);
        }

        if(RT_KMDeviceType == this->_typeUN())
        {
            if(this->flag_off)
            {
                viewPXM.load(":/icon/rt_gray_20x20.png");
                return this->setGIPXM(viewPXM);
            }

            if(!this->flag_connection)
            {
                viewPXM.load(":/icon/rt_q_20x20.png");
                return this->setGIPXM(viewPXM);
            }

            if(this->flag_alarm)
            {
                viewPXM.load(":/icon/rt_alarm_20x20.png");
                return this->setGIPXM(viewPXM);
            }

            if(this->flag_failure)
            {
                viewPXM.load(":/icon/rt_yell_20x20.png");
                return this->setGIPXM(viewPXM);
            }

            if(this->flag_discharge)
            {
                viewPXM.load(":/icon/rt_blue_20x20.png");
                return this->setGIPXM(viewPXM);
            }

            viewPXM.load(":/icon/rt_green_20x20.png");
            return this->setGIPXM(viewPXM);
        }
    }

    if(KSSystemType == this->_typeSystem())
    {
        if(VK09_KSDeviceType == this->_typeUN() ||
           VK0A_KSDeviceType == this->_typeUN() ||
           VK0B_KSDeviceType == this->_typeUN())
        {
            if(this->flag_off)
            {
                viewPXM.load(":/icon/camera_gray_20x20.png");
                return this->setGIPXM(viewPXM);
            }

            if(!this->flag_connection)
            {
                viewPXM.load(":/icon/camera_q_20x20.png");
                return this->setGIPXM(viewPXM);
            }

            if(this->flag_alarm)
            {
                viewPXM.load(":/icon/camera_alarm_20x20.png");
                return this->setGIPXM(viewPXM);
            }

            if(this->flag_failure)
            {
                viewPXM.load(":/icon/camera_yell_20x20.png");
                return this->setGIPXM(viewPXM);
            }

            if(this->flag_discharge)
            {
                viewPXM.load(":/icon/camera_blue_20x20.png");
                return this->setGIPXM(viewPXM);
            }

            viewPXM.load(":/icon/camera_green_20x20.png");
            return this->setGIPXM(viewPXM);
        }

        if(RT01_KSDeviceType == this->_typeUN() ||
           RT02_KSDeviceType == this->_typeUN())
        {
            if(this->flag_off)
            {
                viewPXM.load(":/icon/rt_gray_20x20.png");
                return this->setGIPXM(viewPXM);
            }

            if(!this->flag_connection)
            {
                viewPXM.load(":/icon/rt_q_20x20.png");
                return this->setGIPXM(viewPXM);
            }

            if(this->flag_alarm)
            {
                viewPXM.load(":/icon/rt_alarm_20x20.png");
                return this->setGIPXM(viewPXM);
            }

            if(this->flag_failure)
            {
                viewPXM.load(":/icon/rt_yell_20x20.png");
                return this->setGIPXM(viewPXM);
            }

            if(this->flag_discharge)
            {
                viewPXM.load(":/icon/rt_blue_20x20.png");
                return this->setGIPXM(viewPXM);
            }

            viewPXM.load(":/icon/rt_green_20x20.png");
            return this->setGIPXM(viewPXM);
        }

        if(CPP_KSDeviceType == this->_typeUN())
        {
            if(this->flag_off)
            {
                viewPXM.load(":/icon/cpp_gray_20x20.png");
                return this->setGIPXM(viewPXM);
            }

            if(!this->flag_connection)
            {
                viewPXM.load(":/icon/cpp_q_20x20.png");
                return this->setGIPXM(viewPXM);
            }

            if(this->flag_alarm)
            {
                viewPXM.load(":/icon/cpp_alarm_20x20.png");
                return this->setGIPXM(viewPXM);
            }

            if(this->flag_failure)
            {
                viewPXM.load(":/icon/cpp_q_20x20.png");
                return this->setGIPXM(viewPXM);
            }

            if(this->flag_discharge)
            {
                viewPXM.load(":/icon/cpp_blue_20x20.png");
                return this->setGIPXM(viewPXM);
            }


            viewPXM.load(":/icon/cpp_green_20x20.png");
            return this->setGIPXM(viewPXM);
        }
    }

    if(BSKSystemType == this->_typeSystem())
    {
        if(CPP_BSKDeviceType == this->_typeUN())
        {
            if(this->flag_off)
            {
                viewPXM.load(":/icon/cpp_gray_20x20.png");
                return this->setGIPXM(viewPXM);
            }

            if(!this->flag_connection)
            {
                viewPXM.load(":/icon/cpp_q_20x20.png");
                return this->setGIPXM(viewPXM);
            }

            if(this->flag_alarm)
            {
                viewPXM.load(":/icon/cpp_red_20x20.png");
                return this->setGIPXM(viewPXM);
            }

            if(this->flag_failure)
            {
                viewPXM.load(":/icon/cpp_yell_20x20.png");
                return this->setGIPXM(viewPXM);
            }

            if(this->flag_discharge)
            {
                viewPXM.load(":/icon/cpp_blue_20x20.png");
                return this->setGIPXM(viewPXM);
            }


            viewPXM.load(":/icon/cpp_green_20x20.png");
            return this->setGIPXM(viewPXM);
        }

        if(RT_BSKDeviceType == this->_typeUN())
        {
            if(this->flag_off)
            {
                viewPXM.load(":/icon/rt_gray_20x20.png");
                return this->setGIPXM(viewPXM);
            }

            if(!this->flag_connection)
            {
                viewPXM.load(":/icon/rt_q_20x20.png");
                return this->setGIPXM(viewPXM);
            }

            if(this->flag_alarm)
            {
                viewPXM.load(":/icon/rt_red_20x20.png");
                return this->setGIPXM(viewPXM);
            }

            if(this->flag_failure)
            {
                viewPXM.load(":/icon/rt_yell_20x20.png");
                return this->setGIPXM(viewPXM);
            }

            if(this->flag_discharge)
            {
                viewPXM.load(":/icon/rt_blue_20x20.png");
                return this->setGIPXM(viewPXM);
            }


            viewPXM.load(":/icon/rt_green_20x20.png");
            return this->setGIPXM(viewPXM);
        }

        if(this->flag_off)
        {
            viewPXM.load(":/icon/SO_gray_20x20.png");
            return this->setGIPXM(viewPXM);
        }

        if(!this->flag_connection)
        {
            viewPXM.load(":/icon/SO_q_20x20.png");
            return this->setGIPXM(viewPXM);
        }

        if(this->flag_alarm)
        {
            viewPXM.load(":/icon/SO_red_20x20.png");
            return this->setGIPXM(viewPXM);
        }

        if(this->flag_failure)
        {
            viewPXM.load(":/icon/SO_yell_20x20.png");
            return this->setGIPXM(viewPXM);
        }

        if(this->flag_discharge)
        {
            viewPXM.load(":/icon/SO_blue_20x20.png");
            return this->setGIPXM(viewPXM);
        }


        viewPXM.load(":/icon/SO_green_20x20.png");
        return this->setGIPXM(viewPXM);
    }

    viewPXM.load(":/icon/faq_20x20.png");
    return this->setGIPXM(viewPXM);
}

QBrush UnitNode::getViewFrontCircleZoneBrush() /*noexcept*/
{
    QBrush brush;
    brush.setStyle(Qt::Dense4Pattern);
    if(this->flag_off)
    {
        brush.setColor(QColor(0xCF, 0xCF, 0xCF));//gray
        return brush;
    }

    if(!this->flag_connection)
    {
        brush.setColor(QColor(0xCF, 0xCF, 0xCF));//gray
        return brush;
    }

    if(this->flag_alarm)
    {
        brush.setColor(QColor(0xE3, 0x06, 0x13));//red
        return brush;
    }

    if(this->flag_failure)
    {
        brush.setColor(QColor(0xFF, 0xDD, 0x0E));//yello
        return brush;
    }

    if(this->flag_discharge)
    {
        brush.setColor(QColor(0x0F, 0x70, 0xB7));//blue
        return brush;
    }

    brush.setColor(QColor(0x00, 0x96, 0x40));//green
    return brush;
    }

QPen UnitNode::getViewFrontCircleZonePen() /*noexcept*/
{
    QPen pen;
    pen.setStyle(Qt::DashLine);
    if(this->flag_off)
    {
        pen.setColor(QColor(0xCF, 0xCF, 0xCF));//gray
        return pen;
    }

    if(!this->flag_connection)
    {
        pen.setColor(QColor(0xCF, 0xCF, 0xCF));//gray
        return pen;
    }

    if(this->flag_alarm)
    {
        pen.setColor(QColor(0xE3, 0x06, 0x13));//red
        return pen;
    }

    if(this->flag_failure)
    {
        pen.setColor(QColor(0xFF, 0xDD, 0x0E));//yello
        return pen;
    }

    if(this->flag_discharge)
    {
        pen.setColor(QColor(0x0F, 0x70, 0xB7));//blue
        return pen;
    }

    pen.setColor(QColor(0x00, 0x96, 0x40));//green
    return pen;
}

QPixmap UnitNode::getViewFrontPXM() /*noexcept*/
{
    if(UnknownDeviceType == this->_typeUN())
    {
        return this->setFrontGIPXM(exceptPXM);
    }

    QPixmap viewFrontPXM;
    if(KMSystemType == this->_typeSystem())
    {
        if(VK_KMDeviceType == this->_typeUN())
        {
            if(this->flag_off)
            {
                viewFrontPXM.load(":/icon/front_gray.png_5x5");
                return this->setFrontGIPXM(viewFrontPXM);
            }

            if(!this->flag_connection)
            {
                viewFrontPXM.load(":/icon/front_gray.png_5x5");
                return this->setFrontGIPXM(viewFrontPXM);
            }

            if(this->flag_alarm)
            {
                viewFrontPXM.load(":/icon/front_red.png_5x5");
                return this->setFrontGIPXM(viewFrontPXM);
            }

            if(this->flag_failure)
            {
                viewFrontPXM.load(":/icon/front_yell.png_5x5");
                return this->setFrontGIPXM(viewFrontPXM);
            }

            if(this->flag_discharge)
            {
                viewFrontPXM.load(":/icon/front_blue.png_5x5");
                return this->setFrontGIPXM(viewFrontPXM);
            }


            viewFrontPXM.load(":/icon/front_green.png_5x5");
            return this->setFrontGIPXM(viewFrontPXM);
        }
    }

    if(KSSystemType == this->_typeSystem())
    {
        if(VK09_KSDeviceType == this->_typeUN() ||
           VK0A_KSDeviceType == this->_typeUN() ||
           VK0B_KSDeviceType == this->_typeUN())
        {
            if(this->flag_off)
            {
                viewFrontPXM.load(":/icon/front_gray_5x5.png");
                return this->setFrontGIPXM(viewFrontPXM);
            }

            if(!this->flag_connection)
            {
                viewFrontPXM.load(":/icon/front_gray_5x5.png");
                return this->setFrontGIPXM(viewFrontPXM);
            }

            if(this->flag_alarm)
            {
                viewFrontPXM.load(":/icon/front_red_5x5.png");
                return this->setFrontGIPXM(viewFrontPXM);
            }

            if(this->flag_failure)
            {
                viewFrontPXM.load(":/icon/front_yell_5x5.png");
                return this->setFrontGIPXM(viewFrontPXM);
            }

            if(this->flag_discharge)
            {
                viewFrontPXM.load(":/icon/front_blue_5x5.png");
                return this->setFrontGIPXM(viewFrontPXM);
            }


            viewFrontPXM.load(":/icon/front_green_5x5.png");
            return this->setFrontGIPXM(viewFrontPXM);
        }
    }

    if(BSKSystemType == this->_typeSystem())
    {
        if(S_BSKDeviceType == this->_typeUN())
        {
            if(this->flag_off)
            {
                viewFrontPXM.load(":/icon/circle_zone_gray.png");
                return this->setFrontGIPXM(viewFrontPXM);
            }

            if(!this->flag_connection)
            {
                viewFrontPXM.load(":/icon/circle_zone_gray.png");
                return this->setFrontGIPXM(viewFrontPXM);
            }

            if(this->flag_alarm)
            {
                viewFrontPXM.load(":/icon/circle_zone_red.png");
                return this->setFrontGIPXM(viewFrontPXM);
            }

            if(this->flag_failure)
            {
                viewFrontPXM.load(":/icon/circle_zone_yell.png");
                return this->setFrontGIPXM(viewFrontPXM);
            }

            if(this->flag_discharge)
            {
                viewFrontPXM.load(":/icon/circle_zone_blue.png");
                return this->setFrontGIPXM(viewFrontPXM);
            }

            viewFrontPXM.load(":/icon/circle_zone_green.png");
            return this->setFrontGIPXM(viewFrontPXM);
        }

        if(this->flag_off)
        {
            viewFrontPXM.load(":/icon/front_gray_5x5.png");
            return this->setFrontGIPXM(viewFrontPXM);
        }

        if(!this->flag_connection)
        {
            viewFrontPXM.load(":/icon/front_gray_5x5.png");
            return this->setFrontGIPXM(viewFrontPXM);
        }

        if(this->flag_alarm)
        {
            viewFrontPXM.load(":/icon/front_red_5x5.png");
            return this->setFrontGIPXM(viewFrontPXM);
        }

        if(this->flag_failure)
        {
            viewFrontPXM.load(":/icon/front_yell_5x5.png");
            return this->setFrontGIPXM(viewFrontPXM);
        }

        if(this->flag_discharge)
        {
            viewFrontPXM.load(":/icon/front_blue_5x5.png");
            return this->setFrontGIPXM(viewFrontPXM);
        }


        viewFrontPXM.load(":/icon/front_green_5x5.png");
        return this->setFrontGIPXM(viewFrontPXM);
    }

    viewFrontPXM.load(":/icon/faq_20x20.png");
    return this->setFrontGIPXM(viewFrontPXM);
}

QPixmap UnitNode::getViewRssiPXM() /*noexcept*/
{
    QPixmap viewRssiPXM;
    if(KMSystemType == this->_typeSystem())
    {
        if(0 == this->_rssi())
        {
            viewRssiPXM.load(":/icon/signal_cellelar_not_value_20x20.png");
            return viewRssiPXM;
        }
        if(40 >= this->_rssi())
        {
            viewRssiPXM.load(":/icon/signal_cellelar_4_bar_20x20.png");
            return viewRssiPXM;
        }
        if(40 < this->_rssi() && 50 >= this->_rssi())
        {
            viewRssiPXM.load(":/icon/signal_cellelar_3_bar_20x20.png");
            return viewRssiPXM;
        }
        if(50 < this->_rssi() && 60 >= this->_rssi())
        {
            viewRssiPXM.load(":/icon/signal_cellelar_2_bar_20x20.png");
            return viewRssiPXM;
        }
        if(60 < this->_rssi() && 66 >= this->_rssi())
        {
            viewRssiPXM.load(":/icon/signal_cellelar_1_bar_20x20.png");
            return viewRssiPXM;
        }
        if(66 < this->_rssi() && 70 >= this->_rssi())
        {
            viewRssiPXM.load(":/icon/signal_cellelar_0_bar_20x20.png");
            return viewRssiPXM;
        }
        if(70 < this->_rssi())
        {
            viewRssiPXM.load(":/icon/signal_cellelar_off_20x20.png");
            return viewRssiPXM;
        }
    }
    if(KSSystemType == this->_typeSystem())
    {
        if(0 == this->_rssi())
        {
            viewRssiPXM.load(":/icon/signal_cellelar_not_value_20x20.png");
            return viewRssiPXM;
        }
        if(40 >= this->_rssi())
        {
            viewRssiPXM.load(":/icon/signal_cellelar_4_bar_20x20.png");
            return viewRssiPXM;
        }
        if(40 < this->_rssi() && 50 >= this->_rssi())
        {
            viewRssiPXM.load(":/icon/signal_cellelar_3_bar_20x20.png");
            return viewRssiPXM;
        }
        if(50 < this->_rssi() && 60 >= this->_rssi())
        {
            viewRssiPXM.load(":/icon/signal_cellelar_2_bar_20x20.png");
            return viewRssiPXM;
        }
        if(60 < this->_rssi() && 66 >= this->_rssi())
        {
            viewRssiPXM.load(":/icon/signal_cellelar_1_bar_20x20.png");
            return viewRssiPXM;
        }
        if(66 < this->_rssi() && 70 >= this->_rssi())
        {
            viewRssiPXM.load(":/icon/signal_cellelar_0_bar_20x20.png");
            return viewRssiPXM;
        }
        if(70 < this->_rssi())
        {
            viewRssiPXM.load(":/icon/signal_cellelar_off_20x20.png");
            return viewRssiPXM;
        }

    }
    if(BSKSystemType == this->_typeSystem())
    {}

    return viewRssiPXM;
}

void UnitNode::emitUpdateUN() /*noexcept*/
{
//    Отсюда не получается
//    QString str(this->getGraphicsTextLabel());
//    this->graphicsTxtItem->setHtml(str);

    emit this->updateUN(this);
}

void UnitNode::emitUploadUN() /*noexcept*/
{
    emit this->uploadUN(this);
}

QString UnitNode::getGraphicsTextLabel() /*noexcept*/
{
//    QString str("%1 %2/%3 %4.%5");
    QString str("%1 [%2/%3]");
    str = str.arg(this->data(0).toString())
             .arg(this->_numArea())
             .arg(this->_numNode());
//             .arg(this->_voltage() / 0x10)
//             .arg(this->_voltage() & 0x0F);
    return str;
}

void UnitNode::setAlarmCondition() /*noexcept*/
{
#ifdef QT_DEBUG
        qDebug() << "setAlarmCondition()";
#endif
//    if(this->timerAlarm->isActive())
//    {
//        this->timerAlarm->stop();
//    }
    this->updateFlagAlarm(true);
//    this->timerAlarm->start(3000);
}

void UnitNode::offAlarmCondition() /*noexcept*/
{
#ifdef QT_DEBUG
        qDebug() << "offAlarmCondition()";
#endif
//    if(this->timerAlarm->isActive())
//    {
//        this->timerAlarm->stop();
//    }
    this->updateFlagAlarm(false);
}

void UnitNode::updatePosLine(QList<QRectF> list) /*noexcept*/
{
    this->updatePosLine();
}

void UnitNode::updatePosLine() /*noexcept*/
{
    for(auto pack: this->listLine)
    {
        //
        QLineF line(pack.line->line());
        line.setP1(QPointF(pack.begin->x() + pack.begin->pixmap().width() / 2, pack.begin->y() + pack.begin->pixmap().height() / 2));
        line.setP2(QPointF(pack.end->x() + pack.end->pixmap().width() / 2, pack.end->y() + pack.end->pixmap().height() / 2));
        pack.line->setLine(line);
        //

        this->possAddEllipse(line.p1(),
                             line.p2());
    }

    circleZoneSO->setPos(graphicsPxmItem->x() + graphicsPxmItem->pixmap().width() / 2 - circleZoneSO->pixmap().width() / 2,
                         graphicsPxmItem->y() + graphicsPxmItem->pixmap().height() / 2 - circleZoneSO->pixmap().height() / 2);

//    graphicsTxtItem->setPos(graphicsPxmItem->pos().x() - graphicsPxmItem->pixmap().width() / 2,
//                            graphicsPxmItem->pos().y() + 25.0);
}


QByteArray UnitNode::_id() /*noexcept*/
{
    return this->id;
}

quint64 UnitNode::_id_toUInt() /*noexcept*/
{
    QByteArray val_id(_id());
    quint64 val(0);
    for(int i(0), n(val_id.size()); i < n; i++)
    {
        val = val + (quint64)val_id.at(i) * (quint64)qPow(0x100, n - 1 - i);
    }
    return val;
}

int UnitNode::_id_toInt() /*noexcept*/
{
    QByteArray val_id(_id());
    int val(0);
    for(int i(0), n(val_id.size()); i < n; i++)
    {
        val = val + val_id.at(i) * qPow(0x100, n - 1 - i);
    }
    return val;
}

QByteArray UnitNode::_mac() /*noexcept*/
{
    return this->mac;
}

QByteArray UnitNode::_name() /*noexcept*/
{
    return this->name;
}

QByteArray UnitNode::_abstractName() /*noexcept*/
{
    return this->abstractName;
}

quint32 UnitNode::_dbIndex() /*noexcept*/
{
    return this->dbIndex;
}

quint32 UnitNode::_dbIndexParent() /*noexcept*/
{
    return this->dbIndexParent;
}

TypeUnitNodeDevice UnitNode::_typeUN() /*noexcept*/
{
    return this->typeUN;
}

TypeUnitNodeSystem UnitNode::_typeSystem() /*noexcept*/
{
    return this->typeSystem;
}

quint8 UnitNode::_voltage() /*noexcept*/
{
    return this->voltage;
}

quint8 UnitNode::_rssi() /*noexcept*/
{
    return this->rssi;
}

quint8 UnitNode::_txpower() /*noexcept*/
{
    return this->txPower;
}


quint8 UnitNode::_numArea() /*noexcept*/
{
    return this->numArea;
}

quint8 UnitNode::_numNode() /*noexcept*/
{
    return this->numNode;
}

quint8 UnitNode::_numKit() /*noexcept*/
{
    return this->numKit;
}

QList<QByteArray> UnitNode::_listIDBSK(quint8 numVK) /*noexcept*/
{
    return this->listSettingVK.at(numVK - 1).listIDBSK;
}

bool UnitNode::_hidden() /*noexcept*/
{
    return this->flag_hidden;
}

double UnitNode::updateGeoAltitude(double value) /*noexcept*/
{
    if(this->geoAltitude != value)
    {
        this->geoAltitude = value;
        this->emitUpdateUN();
        this->emitUploadUN();
    }
    return this->geoAltitude;
}

quint8 UnitNode::updateGeoLatitudeFlag(quint8 value) /*noexcept*/
{
    if(this->geoLatitudeFlag != value)
    {
        this->geoLatitudeFlag = value;
        this->emitUpdateUN();
        this->emitUploadUN();
    }
    return this->geoLatitudeFlag;
}

//    double updateGeoLatitude(double value) /*noexcept*/;
void UnitNode::updateGeoLatitude(double degrees, double minutes, double seconds, quint8 flag) /*noexcept*/
{
    if(this->geoLatitudeDegrees != degrees ||
       this->geoLatitudeMinutes != minutes ||
       this->geoLatitudeSeconds != seconds ||
       this->geoLatitudeFlag != flag)
    {
        this->geoLatitudeDegrees = degrees;
        this->geoLatitudeMinutes = minutes;
        this->geoLatitudeSeconds = seconds;
        this->geoLatitudeFlag = flag;

        this->geoLatitude = this->geoLatitudeDegrees * (double)3600.0 + this->geoLatitudeMinutes * (double)60.0 + (this->geoLatitudeSeconds / (double)100.0);

        this->emitUpdateUN();
        this->emitUploadUN();
    }
}

double UnitNode::updateGeoLatitudeDegrees(double value) /*noexcept*/
{
    if(this->geoLatitudeDegrees != value)
    {
        this->geoLatitudeDegrees = value;
        this->emitUpdateUN();
        this->emitUploadUN();
    }
    return this->geoLatitudeDegrees;
}

double UnitNode::updateGeoLatitudeMinutes(double value) /*noexcept*/
{
    if(this->geoLatitudeMinutes != value)
    {
        this->geoLatitudeMinutes = value;
        this->emitUpdateUN();
        this->emitUploadUN();
    }
    return this->geoLatitudeMinutes;
}

double UnitNode::updateGeoLatitudeSeconds(double value) /*noexcept*/
{
    if(this->geoLatitudeSeconds != value)
    {
        this->geoLatitudeSeconds = value;
        this->emitUpdateUN();
        this->emitUploadUN();
    }
    return this->geoLatitudeSeconds;
}

quint8 UnitNode::updateGeoLongitudeFlag(quint8 value) /*noexcept*/
{
    if(this->geoLongitudeFlag != value)
    {
        this->geoLongitudeFlag = value;
        this->emitUpdateUN();
        this->emitUploadUN();
    }
    return this->geoLongitudeFlag;
}
//    double updateGeoLongitude(double value) /*noexcept*/;
void UnitNode::updateGeoLongitude(double degrees, double minutes, double seconds, quint8 flag) /*noexcept*/
{
    if(this->geoLongitudeDegrees != degrees ||
       this->geoLongitudeMinutes != minutes ||
       this->geoLongitudeSeconds != seconds ||
       this->geoLongitudeFlag != flag)
    {
        this->geoLongitudeDegrees = degrees;
        this->geoLongitudeMinutes = minutes;
        this->geoLongitudeSeconds = seconds;
        this->geoLongitudeFlag = flag;

        this->geoLongitude   = this->geoLongitudeDegrees * (double)3600.0 + this->geoLongitudeMinutes * (double)60.0 + (this->geoLongitudeSeconds / (double)100.0);

        this->emitUpdateUN();
        this->emitUploadUN();
    }
}

double UnitNode::updateGeoLongitudeDegrees(double value) /*noexcept*/
{
    if(this->geoLongitudeDegrees != value)
    {
        this->geoLongitudeDegrees = value;
        this->emitUpdateUN();
        this->emitUploadUN();
    }
    return this->geoLongitudeDegrees;
}

double UnitNode::updateGeoLongitudeMinutes(double value) /*noexcept*/
{
    if(this->geoLongitudeMinutes != value)
    {
        this->geoLongitudeMinutes = value;
        this->emitUpdateUN();
        this->emitUploadUN();
    }
    return this->geoLongitudeMinutes;
}

double UnitNode::updateGeoLongitudeSeconds(double value) /*noexcept*/
{
    if(this->geoLongitudeSeconds != value)
    {
        this->geoLongitudeSeconds = value;
        this->emitUpdateUN();
        this->emitUploadUN();
    }
    return this->geoLongitudeSeconds;
}

