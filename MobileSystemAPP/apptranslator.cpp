#include <apptranslator.h>

//AppTranslator::AppTranslator(QObject *parent) /*noexcept*/ : QObject(parent)
//{

//}

AppTranslator::AppTranslator(QApplication *app,
                             MainWindow *w,
                             QObject *parent) /*noexcept*/ :
    QObject(parent),
    m_app(app),
    m_w(w)
{
    connect(m_w,
            SIGNAL(setTranslator(QTranslator*)),
            this,
            SLOT(installTranslator(QTranslator*)));

    connect(m_w,
            SIGNAL(resetTranslator(QTranslator*)),
            this,
            SLOT(removeTranslator(QTranslator*)));
}

AppTranslator::~AppTranslator()
{
    try
    {

    }
    catch(...)
    {
#ifdef QT_DEBUG
        qDebug() << "Assert(~AppTranslator)";
#endif
        return;
    }
}

void AppTranslator::installTranslator(QTranslator *tr) /*noexcept*/
{
    m_app->installTranslator(tr);
}

void AppTranslator::removeTranslator(QTranslator *tr) /*noexcept*/
{
    m_app->removeTranslator(tr);
}
