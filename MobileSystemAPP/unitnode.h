﻿#ifndef UNITNODE_H
#define UNITNODE_H

#include <QObject>
#include <QTimer>
#include <global_value_and_structure.h>
#include <proxymetod.h>
#ifdef QT_DEBUG
#include <QDebug>
#endif

#include <QGeoCoordinate>
#include <QGeoPositionInfo>
#include <QGraphicsScene>
#include <graphicspixmapitem.h>
#include <graphicstextitem.h>
#include <QFileDialog>
#include <QtMath>

//QList<int> qwerty;

enum WorkMode
{
    Undefined = -1,
    ConnectSubset = 0,
    DefinitionHeard = 1,
    DisconnectHeard = 2,
    DisconnectSubset = 3,
    DutyMode = 4,
    ConnectVK = 5,
    DefinitionTopology = 6
};

struct PieceIMG
{
    quint32 begin = 0;
    quint32 end = 0;
    QByteArray piece;
};


struct IMGItem
{
    QByteArray img;
    quint32 size;
    quint8 num;
};


struct TypeBuffer
{
    QByteArray btLink,
               mainBuffer,
               data;
};



struct ColleclorIMG
{
    quint32 idMSG0xC1;
    QByteArray releaseIMG;
    QList<PieceIMG> listPiece;
    quint32 releaseSize;
    quint32 currentSize;
    quint8 numTrain,
           numShot,
           lastNumBlock,
           currentNumBlock;
    bool doneStatus;
};

struct IMGCollectorCell
{
    quint32 id_This_MSG0xC1;
    quint32 id_Main_MSG0xC1;
    QByteArray idUN;
    QByteArray releaseIMG;
    QList<PieceIMG> listPiece;
    quint32 blockCount;
    quint32 blockSize;
    quint32 releaseSize;
    quint32 currentSize;
    quint8 numVK,
           numTrain,
           numShot,
           lastNumBlock,
           currentNumBlock;
    bool doneStatus;

    IMGCollectorCell()
    {
        id_This_MSG0xC1 = 0;
        id_Main_MSG0xC1 = 0;
//        idUN.clear();
//        releaseIMG.clear();
//        listPiece.clear();
        releaseSize = 0;
        currentSize = 0;
        blockSize = 0;
        numVK = 0x00;
        numTrain = 0xFF;
        numShot = 0xFF;
        lastNumBlock = 0x00;
        currentNumBlock = 0x00;
        doneStatus = false;
    }
};


struct IMGpack
{
    bool readyStatus;
    quint32 idC1MSG;
    QList<IMGItem> listIMGItem;
};

struct TrainIMG
{
    bool doneStatus,
         requestStatus;
    quint32 mainIDMSG0xC1;
    quint8 currentNumShot,
           numTrain;
    QList<ColleclorIMG> listColleclorIMG;
};

struct SettingVK
{
    quint8 modeVK, //режим камеры
           timeout1,
           timeout2,
           timeout3,
           selfSO, //встроенное СО
           bskSO, //колв БСК СО
           otherSO, //другие СО
           logic, //и - или
           numComplex, //номер комплекса РМ433
           modeDayNight;
    bool flag_connection, //наличие связи
         flag_discharge, //загруженность
         flag_alarm, //тревога
         flag_attention, //внимание
         flag_failure, //отказ
         flag_off; //вкл.откл
    QList<QByteArray> listIDBSK;
    int luminanceLevel,
        luminanceThreshold;
};

struct LinePackStruct
{
    QGraphicsLineItem *line;
    GraphicsPixmapItem *end, *begin;
};


class UnitNode : public QObject
{
    Q_OBJECT
public:
    explicit UnitNode(QObject *parent = nullptr) /*noexcept*/;
    virtual ~UnitNode() /*noexcept*/;


    QByteArray lastMSGToSend,
               lastMSGToAccept;
    bool autoRequestB1;

    /// свойства для идентификации -->
    QByteArray id,     //имя устройства BT из протокола
               name,   //имя
               abstractName;   //преобретённое имя
    QGeoPositionInfo geoInfo;
    QGeoCoordinate geoCoordinate;
    QByteArray shirota, dolgota,
               flagShirota, flagDolgota;
    quint32 dbIndex,   //индекс из БД
            dbIndexParent;
    TypeUnitNodeDevice typeUN;    //01-btVK, 02-btRT, 03-btPKP
                                  //09,0A-ksVK, 01-ksRT
                                  //0X-БСК
    TypeUnitNodeSystem typeSystem;//00-bt, 01-ks, 02-bsk
    bool flag_hidden;

    QByteArray updateID(QByteArray id) /*noexcept*/;
    QByteArray updateName(QByteArray name) /*noexcept*/;
    QByteArray updateAbstractName(QByteArray name) /*noexcept*/;
    QByteArray updateMAC(QByteArray value) /*noexcept*/;
    TypeUnitNodeDevice updateTypeUN(TypeUnitNodeDevice value) /*noexcept*/;
    TypeUnitNodeSystem updateTypeSystem(TypeUnitNodeSystem value) /*noexcept*/;
    void updateType(TypeUnitNodeSystem sysType,
                    TypeUnitNodeDevice unType) /*noexcept*/;
    quint32 updateDBIndex(quint32 index) /*noexcept*/;
    quint32 updateDBIndexParent(quint32 indexParent) /*noexcept*/;
    /// свойства для идентификации <--

    ///базовые свойства устройства -->
    quint8 voltage, //заряд
           numArea, //участок
           numNode, //номер
           numKit;  //номер комплекта
    bool flag_connection; //наличие связи
    bool flag_discharge; //загруженность
    bool flag_alarm; //тревога
    bool flag_attention; //внимание
    bool flag_failure; //отказ
    bool flag_off; //вкл.откл
    quint8 flag_selfD,
           flag_otherD1,
           flag_otherD2;

    qint32 txPower,
           rssi;
    QPixmap viewPXM, //отображаемая иконка
            exceptPXM; //отображаемая иконка корневого item'а
    GraphicsPixmapItem *graphicsPxmItem;
    GraphicsTextItem *graphicsTxtItem;
    QList<GraphicsPixmapItem *> listLFlangP, listRFlangP;
    GraphicsPixmapItem * circleZoneSO;
    QList<LinePackStruct> listLine;
    QGraphicsEllipseItem *ellipsFrontZone;

    // Geo ХУЕТА -->
    double geoAltitude;

    quint8 geoLatitudeFlag;
    double geoLatitude;
    double geoLatitudeDegrees;
    double geoLatitudeMinutes;
    double geoLatitudeSeconds;

    quint8 geoLongitudeFlag;
    double geoLongitude;
    double geoLongitudeDegrees;
    double geoLongitudeMinutes;
    double geoLongitudeSeconds;
    // Geo ХУЕТА <--

    quint8 updateNumberArea(quint8 num) /*noexcept*/;
    quint8 updateNumberNode(quint8 num) /*noexcept*/;
    quint8 updateNumberKit(quint8 num) /*noexcept*/;
    bool updateFlagConnection(bool connectionValid) /*noexcept*/;
    bool updateFlagDischarge(bool value) /*noexcept*/;
    bool updateFlagAlarm(bool value) /*noexcept*/;
    bool updateFlagAttention(bool value) /*noexcept*/;
    bool updateFlagFailure(bool value) /*noexcept*/;
    bool updateFlagOff(bool value) /*noexcept*/;
    quint8 updateFlagSelfD(quint8 value) /*noexcept*/;
    quint8 updateFlagOtherD1(quint8 value) /*noexcept*/;
    quint8 updateFlagOtherD2(quint8 value) /*noexcept*/;
    int updateVoltage(int value) /*noexcept*/;
    int updateTxPower(int value) /*noexcept*/;
    int updateRssi(int value) /*noexcept*/;
    void updateListIDBSK(quint8 numVK,
                         quint8 indexSO,
                         QByteArray idSO) /*noexcept*/;
    void removeListIDBSK(quint8 numVK,
                         quint8 indexSO,
                         QByteArray idSO) /*noexcept*/;
    void clearListIDBSK(quint8 numVK) /*noexcept*/;
    bool updateFlagHidden(bool value) /*noexcept*/;

    double updateGeoAltitude(double value) /*noexcept*/;

    quint8 updateGeoLatitudeFlag(quint8 value) /*noexcept*/;
//    double updateGeoLatitude(double value) /*noexcept*/;
    void updateGeoLatitude(double degrees, double minutes, double seconds, quint8 flag) /*noexcept*/;
    double updateGeoLatitudeDegrees(double value) /*noexcept*/;
    double updateGeoLatitudeMinutes(double value) /*noexcept*/;
    double updateGeoLatitudeSeconds(double value) /*noexcept*/;

    quint8 updateGeoLongitudeFlag(quint8 value) /*noexcept*/;
//    double updateGeoLongitude(double value) /*noexcept*/;
    void updateGeoLongitude(double degrees, double minutes, double seconds, quint8 flag) /*noexcept*/;
    double updateGeoLongitudeDegrees(double value) /*noexcept*/;
    double updateGeoLongitudeMinutes(double value) /*noexcept*/;
    double updateGeoLongitudeSeconds(double value) /*noexcept*/;

    QPen getViewPen() /*noexcept*/;
    QPixmap getViewPXM() /*noexcept*/;
    QBrush getViewFrontCircleZoneBrush() /*noexcept*/;
    QPen getViewFrontCircleZonePen() /*noexcept*/;
    QPixmap getViewFrontPXM() /*noexcept*/;
    QPixmap getViewRssiPXM() /*noexcept*/;
    QPixmap setGIPXM(QPixmap pxm) /*noexcept*/;
    QPixmap setFrontGIPXM(QPixmap pxm) /*noexcept*/;
    LinePackStruct possAddLine(GraphicsPixmapItem *begin,
                               GraphicsPixmapItem *end) /*noexcept*/;
    QGraphicsEllipseItem *possAddEllipse(QPointF p1,
                                         QPointF p2) /*noexcept*/;
    ///базовые свойства устройства <--

    //BSK___
    ///дополнительно для БСК -->
    QByteArray  parameters;  //характерные для каждого типа СО настройки

    //функция определения значка устройства по установленным флагам
    void ChooseIcon(UnitNode* device) /*noexcept*/;

    //в конструкторе определять размер QByteArray в зависимости от типа датчика
    ///дополнительно для БСК <--



    ///базовые свойства BT устройства -->
    QByteArray mac,    //уникальный идентификатор BT
               btLink;//последовательный набор линков для доступа
    WorkMode unitMode;

    QByteArray updateBTLink(QByteArray newBTLink) /*noexcept*/;

    //свойства для сопряжения с другими -->
    quint8 expectInquryCount;
    QList<UnitNode*> listInquiry;

    UnitNode* expectConnect;
    quint8 expectConnectCount;
    QList<UnitNode*> listConnect;
    //свойства для сопряжения с другими <--

    //свойства инсталляции BT VK -->

    QList<SettingVK> listSettingVK;

    quint8 modeVK,
           modeAnxiety,
           timeout1,
           timeout2,
           timeout3,
           selfSO,
           bskSO,
           idBSK1h,
           idBSK1l,
           idBSK2h,
           idBSK2l,
           idBSK3h,
           idBSK3l,
           otherSO,
           logic,
           numComplex;
    //свойства инсталляции BT VK <--
    ///базовые свойства BT устройства <--





    /// свойства для работы с изображениями -->
    QList<TrainIMG> listTrainIMG;

    QList<IMGCollectorCell> listIMGCollectorCell;

    quint8 currentNumShotVK,
           lastNumBlockVK,
           currentNumBlockVK;
    quint32 beginVK,
            sizeBlockVK;
    QList<ColleclorIMG> listIMGVK;

    QList<QByteArray> wayVK;
    QTimer *timerPostponedNeedSendB4VK,
           *timerPostponedNeedSendB3VK,
           *timerPostponedNeedSendB1VK/*,
           *timerAlarm*/;
    QTimer *timerPostponedNeedSendB4_VK1,
           *timerPostponedNeedSendB3_VK1,
           *timerPostponedNeedSendB1_VK1,
           *timerPostponedNeedSendB4_VK2,
           *timerPostponedNeedSendB3_VK2,
           *timerPostponedNeedSendB1_VK2;
    IMGpack pack;

    QString getGraphicsTextLabel() /*noexcept*/;
    bool deduceIMG(quint8 numShot,
                   quint8 numVK = 0x01) /*noexcept*/;
    bool reserveIMG(quint8 numShot,
                    quint32 size,
                    quint8 numVK = 0x01) /*noexcept*/;
    /// свойства для работы с изображениями <--


    /// относящиеся в моделяи представления -->
    UnitNode *unTreeParent; //родительское устройство
    QList<UnitNode*> listChilde; //список детей

    UnitNode* child(int num) /*noexcept*/;
    UnitNode* getUnitNodeTreeParent() /*noexcept*/;

    void appendChild(UnitNode *un) /*noexcept*/;
    int childCount() const /*noexcept*/;
    int columnCount() const /*noexcept*/;

    QVariant data(int column) const /*noexcept*/;
    int row() const /*noexcept*/;
    /// относящиеся в моделяи представления <--


    void clear() /*noexcept*/;
    void stopTimerPostponedNeedSendB4() /*noexcept*/;
    void stopTimerPostponedNeedSendB3() /*noexcept*/;
    void stopTimerPostponedNeedSendB1() /*noexcept*/;

    QByteArray _id() /*noexcept*/;
    int _id_toInt() /*noexcept*/;
    quint64 _id_toUInt() /*noexcept*/;
    QByteArray _mac() /*noexcept*/;
    QByteArray _name() /*noexcept*/;
    QByteArray _abstractName() /*noexcept*/;
    quint32 _dbIndex() /*noexcept*/;
    quint32 _dbIndexParent() /*noexcept*/;
    TypeUnitNodeDevice _typeUN() /*noexcept*/;
    TypeUnitNodeSystem _typeSystem() /*noexcept*/;
    quint8 _voltage() /*noexcept*/;
    quint8 _rssi() /*noexcept*/;
    quint8 _txpower() /*noexcept*/;
    quint8 _numArea() /*noexcept*/;
    quint8 _numNode() /*noexcept*/;
    quint8 _numKit() /*noexcept*/;
    QList<QByteArray> _listIDBSK(quint8 numVK) /*noexcept*/;
    bool _hidden() /*noexcept*/;



    quint8 updateAnxiety(quint8 value);
    quint8 updateModeDayNight(quint8 numVK, quint8 value);
    int updateLuminanceLevel(quint8 numVK, int value);
    int updateLuminanceThreshold(quint8 numVK, int value);

signals:
    void timeoutB1(UnitNode* unSender);
    void timeoutB3(UnitNode* unSender,
                   quint8 numShot,
                   quint8 numBlock,
                   quint32 beginVK,
                   quint32 size);
    void timeoutB4(UnitNode* unSender,
                   quint8 numShot,
                   quint8 numBlock,
                   quint32 beginVK,
                   quint32 size);
    void timeoutB1(UnitNode* unSender,
                   quint8 numVK);
    void timeoutB3(UnitNode* unSender,
                   quint8 numVK,
                   quint8 numShot,
                   quint8 numBlock,
                   quint32 beginVK,
                   quint32 size);
    void timeoutB4(UnitNode* unSender,
                   quint8 numVK,
                   quint8 numShot,
                   quint8 numBlock,
                   quint32 beginVK,
                   quint32 size);
    void updateUN(UnitNode* unSender);
    void uploadUN(UnitNode* unSender);

public slots:
    void emitUpdateUN() /*noexcept*/;
    void emitUploadUN() /*noexcept*/;
    void emitB1() /*noexcept*/;
    void emitB3() /*noexcept*/;
    void emitB4() /*noexcept*/;
    void emitB1_VK1() /*noexcept*/;
    void emitB1_VK2() /*noexcept*/;
    void emitB3_VK1() /*noexcept*/;
    void emitB3_VK2() /*noexcept*/;
    void emitB4_VK1() /*noexcept*/;
    void emitB4_VK2() /*noexcept*/;
    void needSendB3(int ms_repeat) /*noexcept*/;
    void needSendB1(int ms_repeat) /*noexcept*/;
    void needSendB4(int ms_repeat) /*noexcept*/;
    void setAlarmCondition() /*noexcept*/;
    void offAlarmCondition() /*noexcept*/;
    void updatePosLine(QList<QRectF> list) /*noexcept*/;
    void updatePosLine() /*noexcept*/;

    
};

#endif // UNITNODE_H
