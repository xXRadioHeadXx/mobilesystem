QT += core gui
CONFIG += c++11

INCLUDEPATH += $$PWD/

HEADERS += \
        $$PWD/graphicsmapmanager.h \
        $$PWD/graphicspixmapitem.h \
        $$PWD/graphicstextitem.h
	
SOURCES += \
        $$PWD/graphicsmapmanager.cpp \
        $$PWD/graphicspixmapitem.cpp \
        $$PWD/graphicstextitem.cpp
