﻿#include <graphicstextitem.h>


GraphicsTextItem::GraphicsTextItem(QGraphicsItem *parent) :
        QGraphicsTextItem(parent)
{
}

void GraphicsTextItem::paint(QPainter * painter,
                             const QStyleOptionGraphicsItem * option,
                             QWidget * widget)
{
    int off = 10;
    QPainterPath path;
    QFont drawFont;
    drawFont = this->font();
    drawFont.setPixelSize(11);
    QString str(this->toPlainText());
//    QStringList listStr(str.split(' '));

//    if(3 == listStr.size())
//    {
//        str.clear();
//        str.append("<p>");
//        str.append(listStr.at(0));
//        str.append(listStr.at(1));
//        str.append("</p>");
//        str.append("<p>");
//        str.append(listStr.at(2));
//        str.append("</p>");
//    }

    path.addText(off, drawFont.pointSize() + off, drawFont, str);
    painter->setRenderHints(QPainter::Antialiasing);
//    painter->strokePath(path, QPen(QColor(Qt::lightGray), 2));
//    painter->fillPath(path, QBrush(Qt::black));
    painter->strokePath(path, QPen(QColor(Qt::black), 2));
    painter->fillPath(path, QBrush(Qt::white));
//    resize(path.boundingRect().size().toSize().width() + off * 2,
//           path.boundingRect().size().toSize().height() + off * 2);
}
