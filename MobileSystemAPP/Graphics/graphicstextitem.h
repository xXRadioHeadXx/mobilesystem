﻿#ifndef GRAPHICSTEXTITEM_H
#define GRAPHICSTEXTITEM_H

#include <QGraphicsItem>
#include <QGraphicsTextItem>
#include <QFont>
#include <QPainterPath>
#include <QPainter>

class GraphicsTextItem : public QGraphicsTextItem
{
public:
    GraphicsTextItem(QGraphicsItem *parent = nullptr);
    virtual void paint(QPainter * painter,
                       const QStyleOptionGraphicsItem * option,
                       QWidget * widget);
};

#endif // GRAPHICSTEXTITEM_H
