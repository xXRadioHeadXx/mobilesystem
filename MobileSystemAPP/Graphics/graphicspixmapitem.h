﻿#ifndef GRAPHICSPIXMAPITEM_H
#define GRAPHICSPIXMAPITEM_H

#include <QGraphicsItem>
#include <QGraphicsPixmapItem>
#include <QGraphicsSceneHoverEvent>

class GraphicsPixmapItem : /*public QObject,*/ public QGraphicsPixmapItem
{
//    Q_OBJECT
public:
    GraphicsPixmapItem(QGraphicsItem *parent = nullptr);
    void setSlaveItem(QGraphicsItem *item);
    void setSlaveLFlang(QList<GraphicsPixmapItem*> listItem);
    void setSlaveRFlang(QList<GraphicsPixmapItem *> listItem);


protected:
    virtual void hoverMoveEvent(QGraphicsSceneHoverEvent *event) override;
    virtual QVariant itemChange(QGraphicsItem::GraphicsItemChange change, const QVariant &value) override;

private:
    QGraphicsItem *slaveItem;
    QList<QGraphicsItem*> slaveLFlang, slaveRFlang;

//signals:
//    void changePos();
};

#endif // GRAPHICSPIXMAPITEM_H
