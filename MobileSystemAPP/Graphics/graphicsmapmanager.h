﻿#ifndef GRAPHICSMAPMANAGER_H
#define GRAPHICSMAPMANAGER_H

#include <QObject>
#include <kamuflagemanager.h>
#include <kiparismanager.h>
#include <bskmanager.h>
#include <databasemanager.h>

class GraphicsMapManager : public QObject
{
    Q_OBJECT
public:
//     GraphicsMapManager(QObject *parent = nullptr) /*noexcept*/;
    explicit GraphicsMapManager(KamuflageManager *km,
                                KiparisManager *ks,
                                BSKManager *bsk,
                                DataBaseManager *db,
                                TypeUnitNodeSystem typeSystem,
                                QObject *parent = nullptr) /*noexcept*/;
    virtual ~GraphicsMapManager() /*noexcept*/;

    QGraphicsPixmapItem *mapItem;
    UnitNode *rootItem;
    KamuflageManager *kmManager;
    KiparisManager *ksManager;
    BSKManager *bskManager;
    DataBaseManager *dbManager;
    QGraphicsScene *mapScene;
    bool needSaveChanges,
         editStatus;
    TypeUnitNodeSystem currentSystemType;
    QList<UnitNode*> listItem;

    void updateScene() /*noexcept*/;
//    void zoom(bool sign, UnitNode *un = nullptr) /*noexcept*/;
    void zoom(bool sign) /*noexcept*/;
    void zoomX1(qreal x = 0, UnitNode *un = nullptr) /*noexcept*/;
    void loadSubstrate() /*noexcept*/;
    void setSubstrate(QString filePXM) /*noexcept*/;
    void setSubstrate(QPixmap pxm) /*noexcept*/;
    void saveChanges(bool value) /*noexcept*/;
    void updateCurrentSystemType(TypeUnitNodeSystem value) /*noexcept*/;
    void possChangeEditStatus(bool statusValue) /*noexcept*/;
    UnitNode* searchUNPointer(QGraphicsItem *gItem, UnitNode *un = nullptr) /*noexcept*/;
    void catchSceneMouseEvent(UnitNode *un, QPointF pos);
    void filterHiddenUN();

signals:
    void changeEditStatus(bool statusValue);
    void selectUN(UnitNode *un);
    void needUpdate();

public slots:
    void updateUN(UnitNode *un) /*noexcept*/;
    void unselectAll(UnitNode *un = nullptr) /*noexcept*/;
    void selectedUN(UnitNode *un) /*noexcept*/;
    void changeSelectUN() /*noexcept*/;
    void updatePosLine(QList<QRectF> list);
};

#endif // GRAPHICSMAPMANAGER_H
