﻿#include <graphicsmapmanager.h>

GraphicsMapManager::GraphicsMapManager(KamuflageManager *km,
                                       KiparisManager *ks,
                                       BSKManager *bsk,
                                       DataBaseManager *db,
                                       TypeUnitNodeSystem typeSystem,
                                       QObject *parent) /*noexcept*/ :
  QObject(parent),
  kmManager(km),
  ksManager(ks),
  bskManager(bsk),
  dbManager(db),
  currentSystemType(typeSystem)
{
    try
    {
        mapScene = new QGraphicsScene;

        switch((quint8)currentSystemType)
        {
            case (quint8)KMSystemType:
            {
                rootItem = kmManager->m_btManager->m_treeRootUN;
                listItem = kmManager->getListUnitNode();
                connect(kmManager,
                        SIGNAL(updateUN(UnitNode*)),
                        this,
                        SLOT(updateUN(UnitNode*)));
                break;
            }
            case (quint8)KSSystemType:
            {
                rootItem = ksManager->m_ksMsgManager->m_treeRootUN;
                listItem = ksManager->getListUnitNode();
                connect(ksManager,
                        SIGNAL(updateUN(UnitNode*)),
                        this,
                        SLOT(updateUN(UnitNode*)));
                break;
            }
            case (quint8)BSKSystemType:
            {
                rootItem = bskManager->m_bskMsgManager->m_treeRootUN;
                listItem = bskManager->getListUnitNode();
                connect(bskManager,
                        SIGNAL(updateUN(UnitNode*)),
                        this,
                        SLOT(updateUN(UnitNode*)));
                break;
            }
            case (quint8)UnknownDeviceType:
            {   rootItem = nullptr;
                listItem = kmManager->getListUnitNode();
                listItem.append(ksManager->getListUnitNode());
                listItem.append(bskManager->getListUnitNode());
                break;
            }
            default:
                break;
        }
        filterHiddenUN();

    //    rootItem = kmManager->m_btManager->m_treeRootUN;
    //    mapScene = new QGraphicsScene(this);/*GraphicsSceneMap(kmManager,
    //                                    dbManager);*/
        needSaveChanges = false;
        editStatus = false;

        // a white semi-transparent foreground
        mapScene->setBackgroundBrush(QColor(255, 255, 255, 127));
    //    this->setBackgroundBrush(QColor(255, 255, 255, 127));

        connect(mapScene,
                SIGNAL(changed(QList<QRectF>)),
                this,
                SLOT(updatePosLine(QList<QRectF>)));

        // a grid foreground
        mapScene->setBackgroundBrush(QBrush(Qt::lightGray, Qt::CrossPattern));
    //    this->setBackgroundBrush(QBrush(Qt::lightGray, Qt::CrossPattern));

    //    connect(kmManager,
    //            SIGNAL(updateUN(UnitNode*)),
    //            this,
    //            SLOT(insertGIUNInScene(UnitNode*)));
    //    connect(ksManager,
    //            SIGNAL(updateUN(UnitNode*)),
    //            this,
    //            SLOT(insertGIUNInScene(UnitNode*)));
    //    connect(bskManager,
    //            SIGNAL(updateUN(UnitNode*)),
    //            this,
    //            SLOT(insertGIUNInScene(UnitNode*)));


        mapItem = new QGraphicsPixmapItem;
        if(QFile::exists("map.jpg"))
        {
            this->setSubstrate("map.jpg");
        }

        this->updateScene();

        connect(mapScene,
                SIGNAL(selectionChanged()),
                this,
                SLOT(changeSelectUN()));
    //    connect(this,
    //            SIGNAL(selectionChanged()),
    //            this,
    //            SLOT(changeSelectUN()));

    //    this->insertGIUNInScene();
    }
    catch (std::runtime_error &rte)
    {
        qWarning("catch runtime_error %s [GraphicsMapManager::GraphicsMapManager]", rte.what());
    }
    catch (std::bad_alloc& ba)
    {
        qWarning("catch bad_alloc %s [GraphicsMapManager::GraphicsMapManager]", ba.what());
    }
    catch (std::exception &e)
    {
        qWarning("catch exception %s [GraphicsMapManager::GraphicsMapManager]", e.what());
    }
    catch (...)
    {
        qWarning("catch exception <unknown> [GraphicsMapManager::GraphicsMapManager]");
    }
}

GraphicsMapManager::~GraphicsMapManager() /*noexcept*/
{
    try
    {
        //
        QList<UnitNode *> listAllUN;
        switch ((quint8)currentSystemType)
        {
            case (quint8)KMSystemType:
            {
                listAllUN.append(kmManager->getListUnitNode());
                break;
            };
            case (quint8)KSSystemType:
            {
                listAllUN.append(ksManager->getListUnitNode());
                break;
            };
            case (quint8)BSKSystemType:
            {
                listAllUN.append(bskManager->getListUnitNode());
                break;
            };
            case (quint8)UnknownDeviceType:
            {
                listAllUN.append(kmManager->getListUnitNode());
                listAllUN.append(ksManager->getListUnitNode());
                listAllUN.append(bskManager->getListUnitNode());
                break;
            };
        }

        qreal scaleValue(1.0 / mapItem->scale());
        for(UnitNode *un: listAllUN)
        {
            un->graphicsPxmItem->setPos(un->graphicsPxmItem->pos() * scaleValue);
            un->graphicsTxtItem->setPos(un->graphicsPxmItem->x() - un->graphicsPxmItem->pixmap().width() / 2, //un->graphicsTxtItem->x() * 1.05,
                                        un->graphicsPxmItem->y() + 25.0); //un->graphicsTxtItem->y() * 1.05);

            for(auto item: un->listLFlangP)
            {
                item->setPos(item->pos() * scaleValue);
            }
            for(auto item: un->listRFlangP)
            {
                item->setPos(item->pos() * scaleValue);
            }

            un->circleZoneSO->setPos(un->circleZoneSO->pos() * scaleValue);

            this->dbManager->uploadUNToDB(un);
        }
        //
        auto listExistItem(mapScene->items());
        //удыление элементов со сцены
        for(auto &item: listExistItem)
        {
            mapScene->removeItem(item);
        }

        disconnect(kmManager,
                   SIGNAL(updateUN(UnitNode*)),
                   this,
                   SLOT(updateUN(UnitNode*)));
        disconnect(ksManager,
                   SIGNAL(updateUN(UnitNode*)),
                   this,
                   SLOT(updateUN(UnitNode*)));
        disconnect(bskManager,
                   SIGNAL(updateUN(UnitNode*)),
                   this,
                   SLOT(updateUN(UnitNode*)));
        disconnect(mapScene,
                   SIGNAL(selectionChanged()),
                   this,
                   SLOT(changeSelectUN()));

        kmManager = nullptr;
        ksManager = nullptr;
        bskManager = nullptr;
        dbManager = nullptr;
        rootItem = nullptr;
        listItem.clear();
        delete mapScene;
    }
    catch(...)
    {
#ifdef QT_DEBUG
        qDebug() << "Assert(~GraphicsMapManager)";
#endif
        return;
    }
}

void GraphicsMapManager::updateUN(UnitNode *un) /*noexcept*/
{
    if((quint8)currentSystemType != (quint8)un->_typeSystem() &&
       (quint8)UnknownDeviceType != (quint8)currentSystemType)
    {
        return;
    }

    if(!listItem.contains(un))
    {
        switch ((quint8)currentSystemType)
        {
            case (quint8)KMSystemType:
            {
                rootItem = kmManager->m_btManager->m_treeRootUN;
                listItem = kmManager->getListUnitNode();
                break;
            }
            case (quint8)KSSystemType:
            {
                rootItem = ksManager->m_ksMsgManager->m_treeRootUN;
                listItem = ksManager->getListUnitNode();
                break;
            }
            case (quint8)BSKSystemType:
            {
                rootItem = bskManager->m_bskMsgManager->m_treeRootUN;
                listItem = bskManager->getListUnitNode();
                break;
            }
            case (quint8)UnknownDeviceType:
            {   rootItem = nullptr;
                listItem = kmManager->getListUnitNode();
                listItem.append(ksManager->getListUnitNode());
                listItem.append(bskManager->getListUnitNode());
                break;
            }
            default:
                break;
        }
    }
    filterHiddenUN();

    if(listItem.contains(un))
    {
        un->graphicsPxmItem->setPixmap(un->getViewPXM());

        un->graphicsPxmItem->setZValue(5);
        if(!mapScene->items().contains(un->graphicsPxmItem))
        {
            mapScene->addItem(un->graphicsPxmItem);
        }

        un->graphicsTxtItem->setZValue(4);
        un->graphicsTxtItem->setPos(un->graphicsPxmItem->x() - un->graphicsPxmItem->pixmap().width() / 2, //un->graphicsTxtItem->x() * 1.05,
                                      un->graphicsPxmItem->y() + 25.0); //un->graphicsTxtItem->y() * 1.05);
        un->graphicsTxtItem->setHtml(un->getGraphicsTextLabel());
        if(!mapScene->items().contains(un->graphicsTxtItem))
        {
            mapScene->addItem(un->graphicsTxtItem);
        }

        if(KMSystemType == un->_typeSystem() &&
           VK_KMDeviceType == un->_typeUN() &&
           QPointF(0.0f, 0.0f) != un->listLFlangP.last()->pos())
        {
            un->listLFlangP.last()->setPixmap(un->getViewFrontPXM());

            un->listLFlangP.last()->setZValue(3);
            if(!mapScene->items().contains(un->listLFlangP.last()))
            {
                mapScene->addItem(un->listLFlangP.last());
            }
            LinePackStruct line(un->possAddLine(un->graphicsPxmItem,
                                                  un->listLFlangP.last()));
            line.line->setZValue(2);
            if(!mapScene->items().contains(line.line))
            {
                mapScene->addItem(line.line);
            }
            QGraphicsItem *ell(un->possAddEllipse(line.line->line().p1(),
                                                    line.line->line().p2()));
            ell;
            if(!mapScene->items().contains(ell))
            {
                mapScene->addItem(ell);
            }
        }

        if(KSSystemType == un->_typeSystem() &&
           (
                    VK09_KSDeviceType == un->_typeUN() ||
                    VK0A_KSDeviceType == un->_typeUN() ||
                    VK0B_KSDeviceType == un->_typeUN()
           ) &&
           QPointF(0.0f, 0.0f) != un->listLFlangP.last()->pos())
        {
            un->listLFlangP.last()->setPixmap(un->getViewFrontPXM());

            un->listLFlangP.last()->setZValue(3);
            if(!mapScene->items().contains(un->listLFlangP.last()))
            {
                mapScene->addItem(un->listLFlangP.last());
            }

            LinePackStruct line(un->possAddLine(un->graphicsPxmItem,
                                                  un->listLFlangP.last()));
            line.line->setZValue(2);
            if(!mapScene->items().contains(line.line))
            {
                mapScene->addItem(line.line);
            }
            QGraphicsItem *ell(un->possAddEllipse(line.line->line().p1(),
                                                    line.line->line().p2()));
            if(!mapScene->items().contains(ell))
            {
                mapScene->addItem(ell);
            }
        }
        if(KSSystemType == un->_typeSystem() &&
           VK09_KSDeviceType == un->_typeUN() &&
           QPointF(0.0f, 0.0f) != un->listRFlangP.last()->pos())
        {
            un->listRFlangP.last()->setPixmap(un->getViewFrontPXM());

            un->listRFlangP.last()->setZValue(3);
            if(!mapScene->items().contains(un->listRFlangP.last()))
            {
                mapScene->addItem(un->listRFlangP.last());
            }

            LinePackStruct line(un->possAddLine(un->graphicsPxmItem,
                                                  un->listRFlangP.last()));
            line.line->setZValue(2);
            if(!mapScene->items().contains(line.line))
            {
                mapScene->addItem(line.line);
            }
            QGraphicsItem *ell(un->possAddEllipse(line.line->line().p1(),
                                                    line.line->line().p2()));
            if(!mapScene->items().contains(ell))
            {
                mapScene->addItem(ell);
            }
        }

        if(BSKSystemType == un->_typeSystem() &&
           CPP_BSKDeviceType != un->_typeUN() &&
           RT_BSKDeviceType != un->_typeUN() &&
           PKSC_BSKDeviceType != un->_typeUN() &&
           PKP_BSKDeviceType != un->_typeUN() &&
           VK_BSKDeviceType != un->_typeUN())
        {
            if(QPointF(0.0f, 0.0f) != un->listLFlangP.last()->pos())
            {
                if(S_BSKDeviceType != un->_typeUN())
                {
                    un->listLFlangP.last()->setPixmap(un->getViewFrontPXM());

                    un->listLFlangP.last()->setZValue(3);
                    if(!mapScene->items().contains(un->listLFlangP.last()))
                    {
                        mapScene->addItem(un->listLFlangP.last());
                    }

                    LinePackStruct line(un->possAddLine(un->graphicsPxmItem,
                                                          un->listLFlangP.last()));
                    line.line->setZValue(2);
                    if(!mapScene->items().contains(line.line))
                    {
                        mapScene->addItem(line.line);
                    }
                    QGraphicsItem *ell(un->possAddEllipse(line.line->line().p1(),
                                                            line.line->line().p2()));
                    if(!mapScene->items().contains(ell))
                    {
                        mapScene->addItem(ell);
                    }
                }
                else
                {
                    QLineF radius(un->graphicsPxmItem->x() + 10.0,
                                 un->graphicsPxmItem->y() + 10.0,
                                 un->listLFlangP.last()->x() + 2.5,
                                 un->listLFlangP.last()->y() + 2.5);
                    un->circleZoneSO->setPixmap(un->getViewFrontPXM().scaled(radius.length() * 2,
                                                                                 radius.length() * 2,
                                                                                 Qt::KeepAspectRatio,
                                                                                 Qt::FastTransformation));
//                                                                                 Qt::SmoothTransformation));
                    un->circleZoneSO->setPos(un->graphicsPxmItem->x() + 10.0 - radius.length(),
                                               un->graphicsPxmItem->y() + 10.0 - radius.length());

                    un->circleZoneSO->setZValue(1);
                    if(!mapScene->items().contains(un->circleZoneSO))
                    {
                        mapScene->addItem(un->circleZoneSO);
                    }
                }
            }
            if(QPointF(0.0f, 0.0f) != un->listRFlangP.last()->pos())
            {
                un->listRFlangP.last()->setPixmap(un->getViewFrontPXM());

                un->listRFlangP.last()->setZValue(3);

                if(!mapScene->items().contains(un->listRFlangP.last()))
                {
                    mapScene->addItem(un->listRFlangP.last());
                }

                LinePackStruct line(un->possAddLine(un->graphicsPxmItem,
                                                      un->listRFlangP.last()));
                line.line->setZValue(2);
                if(!mapScene->items().contains(line.line))
                {
                    mapScene->addItem(line.line);
                }
                QGraphicsItem *ell(un->possAddEllipse(line.line->line().p1(),
                                                        line.line->line().p2()));
                if(!mapScene->items().contains(ell))
                {
                    mapScene->addItem(ell);
                }
            }
        }

        return;
    }

    this->updateScene();
}

void GraphicsMapManager::updateScene() /*noexcept*/
{
    auto listExistItem(mapScene->items());

    //удыление элементов со сцены
    for(auto &item: listExistItem)
    {
        mapScene->removeItem(item);
    }

    switch(currentSystemType)
    {
        case KMSystemType:
        {
            rootItem = kmManager->m_btManager->m_treeRootUN;
            listItem = kmManager->getListUnitNode();
            break;
        }
        case KSSystemType:
        {
            rootItem = ksManager->m_ksMsgManager->m_treeRootUN;
            listItem = ksManager->getListUnitNode();
            break;
        }
        case BSKSystemType:
        {
            rootItem = bskManager->m_bskMsgManager->m_treeRootUN;
            listItem = bskManager->getListUnitNode();
            break;
        }
        case UnknownDeviceType:
        {   rootItem = nullptr;
            listItem = kmManager->getListUnitNode();
            listItem.append(ksManager->getListUnitNode());
            listItem.append(bskManager->getListUnitNode());
            break;
        }
        default:
            break;
    }
    filterHiddenUN();
    //
//        this->mapScene->blockSignals(true);
    //
    mapItem->setZValue(0);
    if(!mapScene->items().contains(mapItem))
    {
        mapScene->addItem(mapItem);
    }

    for(auto &item: listItem)
    {
        item->graphicsPxmItem->setPixmap(item->getViewPXM());

        item->graphicsPxmItem->setZValue(5);
        if(!mapScene->items().contains(item->graphicsPxmItem))
        {
            mapScene->addItem(item->graphicsPxmItem);
        }

        item->graphicsTxtItem->setZValue(4);
        item->graphicsTxtItem->setPos(item->graphicsPxmItem->x() - item->graphicsPxmItem->pixmap().width() / 2, //un->graphicsTxtItem->x() * 1.05,
                                      item->graphicsPxmItem->y() + 25.0); //un->graphicsTxtItem->y() * 1.05);
        item->graphicsTxtItem->setHtml(item->getGraphicsTextLabel());
        if(!mapScene->items().contains(item->graphicsTxtItem))
        {
            mapScene->addItem(item->graphicsTxtItem);
        }

        if(KMSystemType == item->_typeSystem() &&
           VK_KMDeviceType == item->_typeUN() &&
           QPointF(0.0f, 0.0f) != item->listLFlangP.last()->pos())
        {
            item->listLFlangP.last()->setPixmap(item->getViewFrontPXM());

            item->listLFlangP.last()->setZValue(3);
            if(!mapScene->items().contains(item->listLFlangP.last()))
            {
                mapScene->addItem(item->listLFlangP.last());
            }
            LinePackStruct line(item->possAddLine(item->graphicsPxmItem,
                                                  item->listLFlangP.last()));
            line.line->setZValue(2);
            if(!mapScene->items().contains(line.line))
            {
                mapScene->addItem(line.line);
            }
            QGraphicsItem *ell(item->possAddEllipse(line.line->line().p1(),
                                                    line.line->line().p2()));
            if(!mapScene->items().contains(ell))
            {
                mapScene->addItem(ell);
            }
        }

        if(KSSystemType == item->_typeSystem() &&
           (
                    VK09_KSDeviceType == item->_typeUN() ||
                    VK0A_KSDeviceType == item->_typeUN() ||
                    VK0B_KSDeviceType == item->_typeUN()
           ) &&
           QPointF(0.0f, 0.0f) != item->listLFlangP.last()->pos())
        {
            item->listLFlangP.last()->setPixmap(item->getViewFrontPXM());

            item->listLFlangP.last()->setZValue(3);
            if(!mapScene->items().contains(item->listLFlangP.last()))
            {
                mapScene->addItem(item->listLFlangP.last());
            }

            LinePackStruct line(item->possAddLine(item->graphicsPxmItem,
                                                  item->listLFlangP.last()));
            line.line->setZValue(2);
            if(!mapScene->items().contains(line.line))
            {
                mapScene->addItem(line.line);
            }
            QGraphicsItem *ell(item->possAddEllipse(line.line->line().p1(),
                                                    line.line->line().p2()));
            if(!mapScene->items().contains(ell))
            {
                mapScene->addItem(ell);
            }
        }
        if(KSSystemType == item->_typeSystem() &&
           VK09_KSDeviceType == item->_typeUN() &&
           QPointF(0.0f, 0.0f) != item->listRFlangP.last()->pos())
        {
            item->listRFlangP.last()->setPixmap(item->getViewFrontPXM());

            item->listRFlangP.last()->setZValue(3);
            if(!mapScene->items().contains(item->listRFlangP.last()))
            {
                mapScene->addItem(item->listRFlangP.last());
            }

            LinePackStruct line(item->possAddLine(item->graphicsPxmItem,
                                                  item->listRFlangP.last()));
            line.line->setZValue(2);
            if(!mapScene->items().contains(line.line))
            {
                mapScene->addItem(line.line);
            }
            QGraphicsItem *ell(item->possAddEllipse(line.line->line().p1(),
                                                    line.line->line().p2()));
            if(!mapScene->items().contains(ell))
            {
                mapScene->addItem(ell);
            }
        }

        if(BSKSystemType == item->_typeSystem() &&
           CPP_BSKDeviceType != item->_typeUN() &&
           RT_BSKDeviceType != item->_typeUN() &&
           PKSC_BSKDeviceType != item->_typeUN() &&
           PKP_BSKDeviceType != item->_typeUN() &&
           VK_BSKDeviceType != item->_typeUN())
        {
            if(QPointF(0.0f, 0.0f) != item->listLFlangP.last()->pos())
            {
                if(S_BSKDeviceType != item->_typeUN())
                {
                    item->listLFlangP.last()->setPixmap(item->getViewFrontPXM());

                    item->listLFlangP.last()->setZValue(3);
                    if(!mapScene->items().contains(item->listLFlangP.last()))
                    {
                        mapScene->addItem(item->listLFlangP.last());
                    }

                    LinePackStruct line(item->possAddLine(item->graphicsPxmItem,
                                                          item->listLFlangP.last()));
                    line.line->setZValue(2);
                    if(!mapScene->items().contains(line.line))
                    {
                        mapScene->addItem(line.line);
                    }
                    QGraphicsItem *ell(item->possAddEllipse(line.line->line().p1(),
                                                            line.line->line().p2()));
                    if(!mapScene->items().contains(ell))
                    {
                        mapScene->addItem(ell);
                    }
                }
                else
                {
                    QLineF radius(item->graphicsPxmItem->x() + 10.0,
                                 item->graphicsPxmItem->y() + 10.0,
                                 item->listLFlangP.last()->x() + 2.5,
                                 item->listLFlangP.last()->y() + 2.5);
                    item->circleZoneSO->setPixmap(item->getViewFrontPXM().scaled(radius.length() * 2,
                                                                                 radius.length() * 2,
                                                                                 Qt::KeepAspectRatio,
                                                                                 Qt::FastTransformation));
//                                                                                 Qt::SmoothTransformation));
                    item->circleZoneSO->setPos(item->graphicsPxmItem->x() + 10.0 - radius.length(),
                                               item->graphicsPxmItem->y() + 10.0 - radius.length());

                    item->circleZoneSO->setZValue(1);
                    if(!mapScene->items().contains(item->circleZoneSO))
                    {
                        mapScene->addItem(item->circleZoneSO);
                    }
                }
            }
            if(QPointF(0.0f, 0.0f) != item->listRFlangP.last()->pos())
            {
                item->listRFlangP.last()->setPixmap(item->getViewFrontPXM());

                item->listRFlangP.last()->setZValue(3);

                if(!mapScene->items().contains(item->listRFlangP.last()))
                {
                    mapScene->addItem(item->listRFlangP.last());
                }

                LinePackStruct line(item->possAddLine(item->graphicsPxmItem,
                                                      item->listRFlangP.last()));
                line.line->setZValue(2);
                if(!mapScene->items().contains(line.line))
                {
                    mapScene->addItem(line.line);
                }
                QGraphicsItem *ell(item->possAddEllipse(line.line->line().p1(),
                                                        line.line->line().p2()));
                if(!mapScene->items().contains(ell))
                {
                    mapScene->addItem(ell);
                }
            }
        }
    }
    QList<QRectF> listRect;
    //
//    this->mapScene->blockSignals(false);
    //
    this->updatePosLine(listRect);
}

void GraphicsMapManager::unselectAll(UnitNode *un) /*noexcept*/
{
    if(!un)
    {
        un = this->rootItem;
    }
    un->graphicsPxmItem->setSelected(false);
    for(int i(0), n(un->childCount()); i < n; i++)
    {
        this->unselectAll(un->child(i));
    }
}

void GraphicsMapManager::selectedUN(UnitNode *un) /*noexcept*/
{
    if((quint8)currentSystemType != (quint8)un->_typeSystem() &&
       (quint8)UnknownDeviceType != (quint8)currentSystemType)
    {
        return;
    }
    mapScene->clearSelection();
    un->graphicsPxmItem->setSelected(true);
}

void GraphicsMapManager::changeSelectUN() /*noexcept*/
{
    for(auto &item: listItem)
    {
        if(item->graphicsPxmItem->isSelected() ||
           item->graphicsTxtItem->isSelected())
        {
            emit this->selectUN(item);
            break;
        }
    }

}

void GraphicsMapManager::loadSubstrate() /*noexcept*/
{
    QString fileName;
    //
    fileName = QFileDialog::getOpenFileName(0,
                                            tr("Choosing map dialogue"),//QString::fromLocal8Bit("Диалог выбора подложки карты"),
                                            "",
                                            tr("AllFiles (*.*);;Images (*.jpg *.bmp)"/*;; Database files (*.db *.db3)"*/));
    //
    if(!fileName.isEmpty())
    {
        QFile::remove("map.jpg");
//        QFile::copy(fileName, "map.jpg");
        QImage img;
        img.load(fileName);
        img.save("map.jpg","JPG");
        if(QFile::exists("map.jpg"))
        {
            this->setSubstrate("map.jpg");
        }
        else
        {
            this->setSubstrate(fileName);
        }

    }
}

void GraphicsMapManager::setSubstrate(QString filePXM) /*noexcept*/
{
    QPixmap pxm;
    if(pxm.load(filePXM))
    {
        this->setSubstrate(pxm);
    }
}

void GraphicsMapManager::setSubstrate(QPixmap pxm) /*noexcept*/
{
//    this->mapScene->blockSignals(true);
    mapItem->setPixmap(pxm);
    mapItem->setPos(-1 *(pxm.width() / 2),
                    -1 *(pxm.height() / 2));
//    this->mapScene->blockSignals(false);
}

UnitNode* GraphicsMapManager::searchUNPointer(QGraphicsItem *gItem, UnitNode *un) /*noexcept*/
{
    UnitNode *target(0);
    if(!un)
    {
        un = this->rootItem;
    }

    if(un->graphicsPxmItem == gItem)
    {
        target = un;
    }
    else
    {
        for(int i(0), n(un->childCount()); i < n; i++)
        {
            target = this->searchUNPointer(gItem, un->child(i));
            if(target)
            {
                break;
            }
        }
    }
    return target;
}

void GraphicsMapManager::zoom(bool sign) /*noexcept*/
{

//    this->mapScene->blockSignals(true);
    qreal scaleValue(1.0);
    if(sign)
    {
        scaleValue = 1.05;
    }
    else
    {
        scaleValue = 0.95;
    }

    mapItem->setScale(mapItem->scale() * scaleValue);
    mapItem->setPos(-1 *(mapItem->sceneBoundingRect().width() / 2),
                    -1 *(mapItem->sceneBoundingRect().height() / 2));
//    mapItem->setPos(mapItem->pos() * scaleValue);

    QList<UnitNode *> listAllUN;
    switch(currentSystemType)
    {
        case KMSystemType:
        {
            listAllUN.append(kmManager->getListUnitNode());
            break;
        }
        case KSSystemType:
        {
            listAllUN.append(ksManager->getListUnitNode());
            break;
        }
        case BSKSystemType:
        {
            listAllUN.append(bskManager->getListUnitNode());
            break;
        }
        case UnknownDeviceType:
        {
            listAllUN.append(kmManager->getListUnitNode());
            listAllUN.append(ksManager->getListUnitNode());
            listAllUN.append(bskManager->getListUnitNode());
            break;
        }
    }

    for(UnitNode *un: listAllUN)
    {
        un->graphicsPxmItem->setPos(un->graphicsPxmItem->pos() * scaleValue);
        for(auto item: un->listLFlangP)
        {
            item->setPos(item->pos() * scaleValue);
        }
        for(auto item: un->listRFlangP)
        {
            item->setPos(item->pos() * scaleValue);
        }
        for(auto pack: un->listLine)
        {
            QLineF line(pack.line->line());
            line.setP1(QPointF(pack.begin->x() + pack.begin->pixmap().width() / 2, pack.begin->y() + pack.begin->pixmap().height() / 2));
            line.setP2(QPointF(pack.end->x() + pack.end->pixmap().width() / 2, pack.end->y() + pack.end->pixmap().height() / 2));
            pack.line->setLine(line);
            pack.line->setPos(pack.line->pos() * scaleValue);
            un->possAddEllipse(pack.line->line().p1(),
                               pack.line->line().p2());

//            mapScene->addItem(un->possAddEllipse(pack.line->line().p1(),
//                                                 pack.line->line().p2()));
        }

        //
        un->circleZoneSO->setPos(un->circleZoneSO->pos() * scaleValue);
        un->circleZoneSO->setPixmap(un->getViewFrontPXM().scaled(un->circleZoneSO->pixmap().size() * scaleValue,
                                                                 Qt::KeepAspectRatio,
                                                                 Qt::FastTransformation));
//                                                                 Qt::SmoothTransformation));
        //
        un->graphicsTxtItem->setPos(un->graphicsPxmItem->x() - un->graphicsPxmItem->pixmap().width() / 2, //un->graphicsTxtItem->x() * 1.05,
                                    un->graphicsPxmItem->y() + 25.0); //un->graphicsTxtItem->y() * 1.05);
    }
//    this->mapScene->blockSignals(false);
}

void GraphicsMapManager::zoomX1(qreal x, UnitNode *un) /*noexcept*/
{
//    this->mapScene->blockSignals(true);
    if(!un)
    {
        un = this->rootItem;
        x = 1 / mapItem->scale();
        mapItem->setScale(1.0);
        mapItem->setPos(mapItem->pos() * x);
    }
    else
    {
        un->graphicsPxmItem->setPos(un->graphicsPxmItem->pos() * x);
        un->graphicsTxtItem->setPos(un->graphicsPxmItem->x() - un->graphicsPxmItem->pixmap().width() / 2, //un->graphicsTxtItem->x() * 1.05,
                                    un->graphicsPxmItem->y() + 25.0); //un->graphicsTxtItem->y() * 1.05);

        for(auto item: un->listLFlangP)
        {
            item->setPos(item->pos() * x);
        }
        for(auto item: un->listRFlangP)
        {
            item->setPos(item->pos() * x);
        }

        un->circleZoneSO->setPos(un->circleZoneSO->pos() * x);

        this->dbManager->uploadUNToDB(un);
    }

    for(int i(0), n(un->childCount()); i < n; i++)
    {
        this->zoomX1(x, un->child(i));
    }
//    this->mapScene->blockSignals(false);
}

void GraphicsMapManager::saveChanges(bool value) /*noexcept*/
{
    needSaveChanges = value;
}

void GraphicsMapManager::updateCurrentSystemType(TypeUnitNodeSystem value) /*noexcept*/
{
    if(currentSystemType == value)
        return;
    currentSystemType = value;
    this->updateScene();
}

void GraphicsMapManager::possChangeEditStatus(bool statusValue) /*noexcept*/
{
    //условия запрета смены режима -->
    //условия запрета смены режима <--
    editStatus = statusValue;

    //вызов обработчиков смены статуса режима редактирования -->
    if(editStatus)
    {
        for(auto &item: listItem)
        {
            item->graphicsPxmItem->setFlag(QGraphicsItem::ItemIsMovable);
            for(auto &frontItem: item->listLFlangP)
            {
                frontItem->setFlag(QGraphicsItem::ItemIsMovable);
            }
            for(auto &frontItem: item->listRFlangP)
            {
                frontItem->setFlag(QGraphicsItem::ItemIsMovable);
            }
        }
    }
    else
    {
        for(auto &item: listItem)
        {
            item->graphicsPxmItem->setFlag(QGraphicsItem::ItemIsMovable, false);
            for(auto &frontItem: item->listLFlangP)
            {
                frontItem->setFlag(QGraphicsItem::ItemIsMovable, false);
            }
            for(auto &frontItem: item->listRFlangP)
            {
                frontItem->setFlag(QGraphicsItem::ItemIsMovable, false);
            }
        }
    }
    //вызов обработчиков смены статуса режима редактирования <--


    emit this->changeEditStatus(editStatus);
}

void GraphicsMapManager::catchSceneMouseEvent(UnitNode *un, QPointF pos)
{
    if((quint8)currentSystemType != (quint8)un->_typeSystem() &&
       (quint8)UnknownDeviceType != (quint8)currentSystemType)
    {
        return;
    }
//    this->mapScene->blockSignals(true);
    switch ((quint8)un->_typeSystem())
    {
        case (quint8)KMSystemType:
        {
            switch ((quint8)un->_typeUN())
            {
                case (quint8)VK_KMDeviceType:
                {
                    un->listLFlangP.last()->setPos(pos);
                    break;
                }

                default:
                {
                    break;
                }
            }

            break;
        }

        case (quint8)KSSystemType:
        {
            switch ((quint8)un->_typeUN())
            {
                case (quint8)VK09_KSDeviceType:
                {
                    if(QPointF(0.0f, 0.0f) == un->listLFlangP.last()->pos())
                    {
                        un->listLFlangP.last()->setPos(pos);
                    }
                    else
                    {
                        un->listRFlangP.last()->setPos(pos);
                    }
                    break;
                }

                case (quint8)VK0A_KSDeviceType:
                {
                    un->listLFlangP.last()->setPos(pos);
                    break;
                }

                case (quint8)VK0B_KSDeviceType:
                {
                    un->listLFlangP.last()->setPos(pos);
                    break;
                }

                default:
                {
                    break;
                }
            }

            break;
        }

        case (quint8)BSKSystemType:
        {
            switch ((quint8)un->_typeUN())
            {
                case (quint8)S_BSKDeviceType:
                {
                    un->listLFlangP.last()->setPos(pos);
                    break;
                }

                case (quint8)RLO_BSKDeviceType:
                {
                    un->listLFlangP.last()->setPos(pos);
                    break;
                }

                case (quint8)RLD_BSKDeviceType:
                {
                    un->listLFlangP.last()->setPos(pos);
                    break;
                }

                case (quint8)RWD_BSKDeviceType:
                {
                    un->listLFlangP.last()->setPos(pos);
                    break;
                }

                case (quint8)MSO_BSKDeviceType:
                {
                    un->listLFlangP.last()->setPos(pos);
                    break;
                }

                case (quint8)O_BSKDeviceType:
                {
                    un->listLFlangP.last()->setPos(pos);
                    break;
                }

                case (quint8)IK_BSKDeviceType:
                {
                    un->listLFlangP.last()->setPos(pos);
                    break;
                }

                case (quint8)RW_BSKDeviceType:
                {
                    un->listLFlangP.last()->setPos(pos);
                    break;
                }

                case (quint8)RVP_BSKDeviceType:
                {
                    un->listLFlangP.last()->setPos(pos);
                    break;
                }

                case (quint8)VSO_BSKDeviceType:
                {
                    if(QPointF(0.0f, 0.0f) == un->listLFlangP.last()->pos())
                    {
                        un->listLFlangP.last()->setPos(pos);
                    }
                    else
                    {
                        un->listRFlangP.last()->setPos(pos);
                    }
                    break;
                }

                default:
                {
                    break;
                }
            }

            break;
        }

        default:
        {
            break;
        }
    }
//    this->mapScene->blockSignals(false);

    this->updateScene();
}

void GraphicsMapManager::filterHiddenUN()
{
    for(int i(0); i < listItem.size();)
    {
        if(listItem.at(i)->_hidden())
        {
            listItem.removeAt(i);
        }
        else
        {
            i++;
        }
    }
}

void GraphicsMapManager::updatePosLine(QList<QRectF> list)
{
    for(auto &item: listItem)
    {
        item->updatePosLine(list);
    }
}
