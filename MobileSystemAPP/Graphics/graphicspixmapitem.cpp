﻿#include <graphicspixmapitem.h>

GraphicsPixmapItem::GraphicsPixmapItem(QGraphicsItem *parent) :
//    QObject(nullptr),
    QGraphicsPixmapItem(parent)
{
    slaveItem = nullptr;
    this->setFlag(QGraphicsItem::ItemSendsScenePositionChanges);
}

void GraphicsPixmapItem::hoverMoveEvent(QGraphicsSceneHoverEvent *event)
{
//    emit this->changePos();
    if(slaveItem)
        slaveItem->setPos(event->pos().x() - this->pixmap().width() / 2,
                          event->pos().y() + 25.0);
}

void GraphicsPixmapItem::setSlaveItem(QGraphicsItem *item)
{
    slaveItem = item;
}

void GraphicsPixmapItem::setSlaveLFlang(QList<GraphicsPixmapItem*> listItem)
{
    slaveLFlang.clear();
    for(auto item: listItem)
    {
        slaveLFlang.append(item);
    }
}

void GraphicsPixmapItem::setSlaveRFlang(QList<GraphicsPixmapItem*> listItem)
{
    slaveRFlang.clear();
    for(auto item: listItem)
    {
        slaveRFlang.append(item);
    }
}


QVariant GraphicsPixmapItem::itemChange(QGraphicsItem::GraphicsItemChange change, const QVariant &value)
{
    if(change == QGraphicsItem::ItemPositionChange && slaveItem)
    {
        QPointF newPos = value.toPointF();
        if(slaveItem)
            slaveItem->setPos(newPos.x() - this->pixmap().width() / 2,
                              newPos.y() + 25.0);
        return newPos;
    }
    return QGraphicsItem::itemChange(change, value);
}
