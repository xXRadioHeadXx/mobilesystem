#ifndef THREADANALYSISBSK_H
#define THREADANALYSISBSK_H

#include <QThread>
#include <bskmsgmanager.h>

class ThreadAnalysisBSK : public QThread
{
    Q_OBJECT
public:
    QMutex *m_mutexMSGAnalysis;
    BSKMsgManager *bskMsgManager;
    QByteArray pattern;

    explicit ThreadAnalysisBSK(BSKMsgManager *bsk,
                               QByteArray patternMsg,
                               QMutex *mutexMSGAnalysis,
                               QObject *parent = nullptr) /*noexcept*/;
    virtual ~ThreadAnalysisBSK() /*noexcept*/;

signals:

protected slots:
    void run() /*noexcept*/;

public slots:
};

#endif // THREADANALYSISBSK_H
