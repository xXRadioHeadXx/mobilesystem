﻿#ifndef BSKMSGMANAGER_H
#define BSKMSGMANAGER_H

#include <QObject>
#include <databasemanager.h>
#include <soundalarm.h>

class BSKMsgManager : public QObject
{
    Q_OBJECT
public:

    DataBaseManager *dbManager;
    UnitNode *m_treeRootUN,//корневой элемент структуры
             *m_lastAppendBase;
    SoundAlarm *beeper;
    bool beeperFlag;
    bool    m_resurceStatus;

    quint8 m_numMSG,
           m_numLastMSG,
           m_codeLastMSG;

//    QMutex *m_resursLock;
    QList<UnitNode* > listBSKUnitNode;

    QByteArray lastMSGToSend;
    UnitNode* lastUNToSend;

    QByteArray msgToSend,
               idUNToSend;
    UnitNode *unToSend;
    quint8 currentKitNum;

//    explicit BSKMsgManager(QObject *parent = nullptr);
    explicit BSKMsgManager(DataBaseManager *db,
                           QObject *parent = nullptr) /*noexcept*/;
    ~BSKMsgManager();

    bool lockResource();
    void unlockResource();

    void setInitialListUN() /*noexcept*/;

    //вывод ведомых элементов из структуры
    void extractionChildeFromStructure(UnitNode *un) /*noexcept*/;

    //вывод элемента из структуры
    void extractionFromStructure(UnitNode *un) /*noexcept*/;

    //ввод в структуру
    void involvingInStructure(UnitNode *unMaster,
                              UnitNode *unSlave) /*noexcept*/;

    //вероятный захват элемента
    UnitNode* possGetUnitNode(QByteArray id) /*noexcept*/;

    //вероятное добавление или захват элемента
    UnitNode* possAddUnitNode(QByteArray id) /*noexcept*/;

    //контрольная сумма
    QByteArray setCRC8XOR(QByteArray pattern) /*noexcept*/;


    ///
    bool CheckSum(QByteArray pattern) /*noexcept*/;              //BSK___ проверка контрольной суммы
    ///


    void newMSG(QByteArray pattern) /*noexcept*/;
    void newMSGPKP(QByteArray pattern) /*noexcept*/;
    void updateStatusBSKExchange(bool status) /*noexcept*/;

    //обработчики сообщений -->
    void analyzer_0x9F_0x01(QByteArray pattern, UnitNode *un = nullptr) /*noexcept*/; //тревога без классификации
    void analyzer_0x9F_0x02(QByteArray pattern, UnitNode *un = nullptr) /*noexcept*/; //тревога с классификацией
    void analyzer_0x9F_0x11(QByteArray pattern, UnitNode *un = nullptr) /*noexcept*/; //подтверждение принятой тревоги ПКП в дежурном режиме
    void analyzer_0x9F_0x20(QByteArray pattern, UnitNode *un = nullptr) /*noexcept*/; //есть связь
    void analyzer_0x9F_0x40(QByteArray pattern, UnitNode *un = nullptr) /*noexcept*/; //неисправность
    void analyzer_0x9F_0x42(QByteArray pattern, UnitNode *un = nullptr) /*noexcept*/; //норма батареи передатчика БСК-РЛД
    void analyzer_0x9F_0x43(QByteArray pattern, UnitNode *un = nullptr) /*noexcept*/; //разряд батареи передатчика БСК-РЛД
    void analyzer_0x9F_0x44(QByteArray pattern, UnitNode *un = nullptr) /*noexcept*/; //команда не дошла
    void analyzer_0x9F_0x90(QByteArray pattern, UnitNode *un = nullptr) /*noexcept*/; //подключение устройства (питание подключено)
    void analyzer_0x9F_0x91(QByteArray pattern, UnitNode *un = nullptr) /*noexcept*/; //отключение устройства (питание отключено)
    void analyzer_0x9F_0xBA(QByteArray pattern, UnitNode *un = nullptr) /*noexcept*/; //эта команда частично копирует команду ПИТАНИЕ ПОДКЛЮЧЕНО (0xBA нужна для сброса разряда без перевключения устройства)
    void analyzer_0x9F_0xBB(QByteArray pattern, UnitNode *un = nullptr) /*noexcept*/; //разряд ИПА
    void analyzer_0x9F_0xBD(QByteArray pattern, UnitNode *un = nullptr) /*noexcept*/; //отключение по разряду ИПА
    void analyzer_0x9F_0xC0(QByteArray pattern, UnitNode *un = nullptr) /*noexcept*/; //нет связи
    void analyzer_0x9F_0xCC(QByteArray pattern, UnitNode *un = nullptr) /*noexcept*/; //большой обработчик подтверждения команд
    void analyzer_0x9F_0xD0(QByteArray pattern, UnitNode *un = nullptr) /*noexcept*/; //норма СО
    void analyzer_0x9F_0xD1(QByteArray pattern, UnitNode *un = nullptr) /*noexcept*/; //нет связи со звеном/ПММ/МП БСК-С/МСО/РВ
    void analyzer_0x9F_0xD2(QByteArray pattern, UnitNode *un = nullptr) /*noexcept*/; //неисправность звена/ПММ/МП БСК-С/МСО/РВ
    void analyzer_0x9F_0xD3(QByteArray pattern, UnitNode *un = nullptr) /*noexcept*/; //разряд батареи звена/ПММ/МП БСК-С/МСО/РВ

    void analyzer_A1(QByteArray pattern, UnitNode *un = nullptr) /*noexcept*/;   //команда 0xA1
    void analyzer_CE(QByteArray pattern, UnitNode *un = nullptr) /*noexcept*/;   //команда 0xCE
    void analyzer_0x60_PKP(QByteArray pattern) /*noexcept*/;//PKP
    //обработчики сообщений <--


    //формирование команд -->
    void request_0x60_PKP() /*noexcept*/;//PKP
    void request_0x0B(quint8 numKit) /*noexcept*/;
    void requrst_0x0D() /*noexcept*/;
    void requrst_0x55() /*noexcept*/;
    void requrst_0xCE() /*noexcept*/;
    void request_0x41(UnitNode *un,
                      quint8 command = 0x00,
                      quint8 d1 = 0x00,
                      quint8 d2 = 0x00,
                      quint8 d3 = 0x00) /*noexcept*/;



    void set_RVP_sensitivity(UnitNode *device, quint8 sensitivity) /*noexcept*/; //команда установки чувствительноси РВП
    void set_S_reg(UnitNode *device, quint8 addr, bool value) /*noexcept*/; //команда смены регистра
    void get_config(UnitNode *device) /*noexcept*/;                  //запрос конфигурации устройства

    void set_configuration(quint8 kit_num, quint8 area_num, quint8 so_num) /*noexcept*/;//команда сконфигурировать подключаемое СО
    void set_kit_number(quint8 kit_num) /*noexcept*/;                //команда установить номер комплекта

    //void clear_state(UnitNode *device); //обнуление флага тревоги

    //сброс
    //установка регистров сейсмики и параметров других СО
    //включить/отключить
    //передать конфигурацию
    //установить номер комплекта
    //установить номер участка/СО
    //формирование команд <--

    quint32 emitInsertNewMSG(UnitNode *unSender,
                             quint8 codeMSG,
                             QByteArray msg,
                             bool ioType,
                             TypeGroupMSG typeGroup,
                             bool flagHidden = false) /*noexcept*/;
    void emitTicPattern();
//    quint32 emitInsertNewMSG(quint32 indexDBMsg) /*noexcept*/;

public slots:


signals:
    void addNewUN(UnitNode *newUN);
    void updateUN(UnitNode *un);

    void ssUpdateStatusBSKExchange(int timeout);

//    void insertNewMSG(quint32 indexDBMsg);
    void sendBSKcommand(QByteArray command);//BSK___
    void setAlarmCondition(UnitNode *un);
    void ticPattern();

};

#endif // BSKMSGMANAGER_H
