#include <threadreaderbsk.h>

ThreadReaderBSK::ThreadReaderBSK(QSerialPort *port,
                                 QByteArray *collector,
                                 QByteArray data,
                                 COSVariantList *listPattern,
                                 COSVariantList *listRequest,
                                 QMutex *mutexPattern,
                                 QMutex *mutexRequest,
                                 QObject *parent) /*noexcept*/ :
    QThread(parent),
    m_port(port),
    m_ByteCollector(collector),
    newData(data),
    m_listPattern(listPattern),
    m_listRequest(listRequest),
    m_mutexPattern(mutexPattern),
    m_mutexRequest(mutexRequest)
{
}

ThreadReaderBSK::~ThreadReaderBSK() /*noexcept*/
{
    try
    {
        m_port = nullptr;
        m_ByteCollector = nullptr;

        newData.clear();
        m_listPattern = nullptr;
        m_listRequest = nullptr;
        m_mutexPattern = nullptr;
        m_mutexRequest = nullptr;
    }
    catch(...)
    {
#ifdef QT_DEBUG
        qDebug() << "Assert(~ThreadReaderBSK)";
#endif
        return;
    }
}



void ThreadReaderBSK::run() /*noexcept*/
{
    try
    {
        this->m_ByteCollector->append(newData);
        this->analizMainBuffer();

    //    qWarning("ThreadReaderBSK::run <--");

        this->quit();
    }
    catch (std::runtime_error &rte)
    {
        qWarning("catch runtime_error %s [ThreadReaderBSK::run]", rte.what());
    }
    catch (std::bad_alloc& ba)
    {
        qWarning("catch bad_alloc %s [ThreadReaderBSK::run]", ba.what());
    }
    catch (std::exception &e)
    {
        qWarning("catch exception %s [ThreadReaderBSK::run]", e.what());
    }
    catch (...)
    {
        qWarning("catch exception <unknown> [ThreadReaderBSK::run]");
    }
}


//обработка главного буфера
void ThreadReaderBSK::analizMainBuffer() /*noexcept*/
{
    while(true)
    {
//      удаляем всё перед A5 -->
        int indexA5(0);
        indexA5 = this->m_ByteCollector->indexOf(0xA5, 0);
        if(0 < indexA5)
        {
//            qDebug() << "!!! Remove Trash !!!";
            this->m_ByteCollector->remove(0, indexA5);
        }
        if(-1 == indexA5)
        {
            this->m_ByteCollector->clear();
        }
//      удаляем всё перед A5 <--

//      проверяем по размеру -->
        quint32 size(this->m_ByteCollector->size());
        if(6 > size)//нужны ещё байты. а пока выходим.
        {
//            qDebug() << "return(7 > m_ByteCollector.size(" << this->m_ByteCollector->size() << ")) --x  m_ByteCollector(" << this->m_ByteCollector->left(5).toHex() << "... ["  << this->m_ByteCollector->size() << "] ..." << this->m_ByteCollector->right(5).toHex() << ")";
//            qDebug() << "analizMainBuffer() <--";
            return;
        }

        quint8 dataSize((quint8)this->m_ByteCollector->at(3));//размер поля данных;

        if((6 + (int)dataSize) > (int)size)
        {//нужны ещё байты. а пока выходим.
            return;
        }
//      проверяем по размеру <--

        QByteArray pattern;
        if(1 < this->m_ByteCollector->count((quint8)0xA5))
        {
            qint32 indexSecondA5(this->m_ByteCollector->indexOf((quint8)0xA5, 1));
            pattern = this->m_ByteCollector->left(indexSecondA5);
            this->m_ByteCollector->remove(0, indexSecondA5);
            if(6 + dataSize > pattern.size())
            {
                continue;
            }
//                                qDebug() << "Rmove segment from this->listBuffer->[" << ci << "].data";
        }
        else
        {
            pattern = this->m_ByteCollector->left(this->m_ByteCollector->size());
            this->m_ByteCollector->clear();
        }

//      ребайтстаффинг -->
//      ребайтстаффинг <--

        pattern = pattern.left(6 + dataSize);

//      проверка crc -->
        quint8 crc8(0x00);
        for(qint32 i(1), n(pattern.size() - 1); i < n; i++)
        {
            crc8 = crc8 ^ (quint8)pattern.at(i);
        }
//      проверка crc <--

        if(crc8 == (quint8)pattern.right(1).at(0))
        {//отправить на обработку!!!-->
            possRequestReciept(pattern);

            bool mx_stat(false);
            while(false == mx_stat)
            {
                mx_stat = m_mutexPattern->tryLock(100);
            }

            this->m_listPattern->appendUnique(QVariant(pattern));

            if(true == mx_stat)
            {
                m_mutexPattern->unlock();
            }
        }//отправить на обработку!!!<--
        else
        {
#ifdef QT_DEBUG
            qDebug() << "CRC FALSE !!! (" << pattern.toHex() << ")  right[" << crc8 << "]";
#endif
        }

        continue;
    }
}

QByteArray ThreadReaderBSK::setCRC(QByteArray msg) /*noexcept*/
{
    msg[msg.size() - 1] = 0x00;
    for(int i(1), n(msg.size() - 1); i < n; i++)
    {
        msg[n] = (quint8)msg.at(n) ^ (quint8)msg.at(i);
    }
    return msg;
}

QByteArray ThreadReaderBSK::setPreambula(QByteArray msg) /*noexcept*/
{
    msg.prepend((quint8)0xFF);
    msg.prepend((quint8)0xFF);
    msg.prepend((quint8)0xFF);
    msg.prepend((quint8)0xFF);
    msg.prepend((quint8)0xFF);
    msg.prepend((quint8)0xFF);
    msg.prepend((quint8)0xFF);
    msg.prepend((quint8)0xFF);
    msg.prepend((quint8)0xFF);
    msg.prepend((quint8)0xFF);
    msg.prepend((quint8)0xFF);
    msg.prepend((quint8)0xFF);
    msg.prepend((quint8)0xFF);
    msg.prepend((quint8)0xFF);
    msg.prepend((quint8)0xFF);

    msg.append((quint8)0x22);
    msg.append((char)0x00);
    return msg;
}


void ThreadReaderBSK::possRequestReciept(QByteArray pattern)
{

}
