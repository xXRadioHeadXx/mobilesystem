#ifndef SERIALPORTMANAGERBSK_H
#define SERIALPORTMANAGERBSK_H

#include <QObject>
#include <QSerialPort>
#include <QMutex>

#ifdef QT_DEBUG
#include <QDebug>
#endif

#include <QTime>
#include <bskmsgmanager.h>
#include <threadanalysisbsk.h>
#include <threadreaderbsk.h>


class SerialPortManagerBSK : public QObject
{
    Q_OBJECT
public:

    QString m_portName;
    qint32 m_rate;
    QSerialPort::DataBits m_dataBit;
    QSerialPort::StopBits m_stopBit;
    QSerialPort::FlowControl m_flow;
    QSerialPort::Parity m_parity;
    quint8 lastNumPattern,
           lastCodePattern;
    bool repeatExcept;


    BSKMsgManager *bskMsgManager;
//    QMutex *portResurseMutex;
    QSerialPort *m_port;// порт
    bool m_stopFlag;//главный флаг остановки

//    QList<ThreadAnalysisBSK*> listThreadAnalysis;
//    QList<ThreadReaderBSK*> listThreadReader;
    ThreadReaderBSK *prevousThreadReader;
    ThreadAnalysisBSK *prevousThreadAnalysis;
    COSVariantList *m_listPattern;
    COSVariantList *m_listRequest;

    QByteArray *m_ByteCollector;

    QMutex *m_mutexPattern;
    QMutex *m_mutexRequest;
    QMutex *m_mutexMSGAnalysis;

//    SerialPortManagerBSK(QObject *parent = nullptr);
    explicit SerialPortManagerBSK(BSKMsgManager *bsk,
                                  QMutex *mutexPattern,
                                  QMutex *mutexRequest,
                                  QMutex *mutexMSGAnalysis,
                                  QObject *parent = nullptr) /*noexcept*/;
    virtual ~SerialPortManagerBSK() /*noexcept*/;

//    открытие порта
    bool openPort(QString portName = "COM1",
                  qint32 rate = 38400,
                  QSerialPort::DataBits dataBit = QSerialPort::Data8,
                  QSerialPort::StopBits stopBit = QSerialPort::TwoStop,
                  QSerialPort::FlowControl flow = QSerialPort::NoFlowControl,
                  QSerialPort::Parity parity = QSerialPort::NoParity) /*noexcept*/;
    bool reopenPort() /*noexcept*/;

signals:
    void portCondition(qint32 condition);

public slots:
    void process() /*noexcept*/;
    void closePort() /*noexcept*/;
//    отправка простого BT сообщения
    void sendBSKMSG() /*noexcept*/;
    void sendBSKMSG(QByteArray msg) /*noexcept*/;
    void sendRequestMSG() /*noexcept*/;

    bool refreshBuffer() /*noexcept*/;
//    void analisePattern(QByteArray pattern) /*noexcept*/;
    void acceptPattern() /*noexcept*/;
    void emitPortCondition() /*noexcept*/;
//    void removeFinishedThreadAnalysis() /*noexcept*/;
//    void removeFinishedThreadReader() /*noexcept*/;
    void errorPort(QSerialPort::SerialPortError error) /*noexcept*/;

    bool isOpen() /*noexcept*/;
    QString portName() /*noexcept*/;
    qint32 baudRate() /*noexcept*/;
    qint32 dataBits() /*noexcept*/;
    qint32 parity() /*noexcept*/;
    qint32 stopBits() /*noexcept*/;

};

#endif // SERIALPORTMANAGERBSK_H
