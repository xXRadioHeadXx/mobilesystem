﻿#include <bskmsgmanager.h>
//#include <QDebug>//BSK___



/*
Типы СО:
БСК-С		0x02

БСК-RT		0x01

БСК-С		0x02
БСК-РЛО		0x03
БСК-РЛД		0x04
БСК-РВД		0x05
БСК-МСО		0x06
БСК-БВУ		0x07
БСК-ВК		0x09
БСК-О		0x0A
БСК-ИК		0x0B
БСК-РВ		0x0C
БСК-РВП		0x0D
БСК-ПКСЧ	0x0E
БСК-ВСО		0x0F
*/


/*
приоритеты состояний:
1. включено/отключено
2. нет связи
3. тревога
4. неисправность
5. разряд
*/




BSKMsgManager::BSKMsgManager(DataBaseManager *db,
                             QObject *parent) /*noexcept*/ :
    QObject(parent),
    dbManager(db)
{
    try
    {
        this->m_numLastMSG = 0xFF;
        this->m_codeLastMSG = 0xFF;
        this->m_numMSG = 0x00;
        this->m_lastAppendBase = nullptr;

//        this->m_resursLock = new QMutex;

        this->m_treeRootUN = new UnitNode;      //BSK___ к этому элементу потомком подцепить ПУЦ
        this->m_treeRootUN->updateType(BSKSystemType, UnknownDeviceType);
        this->beeper = new SoundAlarm;
        currentKitNum = 0x01;

        this->lastUNToSend = nullptr;
        this->lastMSGToSend.clear();
        this->m_resurceStatus = false;
    }
    catch (std::runtime_error &rte)
    {
        qWarning("catch runtime_error %s [BSKMsgManager::BSKMsgManager]", rte.what());
    }
    catch (std::bad_alloc& ba)
    {
        qWarning("catch bad_alloc %s [BSKMsgManager::BSKMsgManager]", ba.what());
    }
    catch (std::exception &e)
    {
        qWarning("catch exception %s [BSKMsgManager::BSKMsgManager]", e.what());
    }
    catch (...)
    {
        qWarning("catch exception <unknown> [BSKMsgManager::BSKMsgManager]");
    }
}

BSKMsgManager::~BSKMsgManager()
{
    try
    {
        for(int i(0), n(this->listBSKUnitNode.size()); i < n; i++)
        {
            this->listBSKUnitNode.at(i)->updateFlagConnection(false);
            this->listBSKUnitNode.at(i)->emitUploadUN();
        }
//        delete m_resursLock;
        delete m_treeRootUN;
        delete beeper;
    }
    catch(...)
    {
#ifdef QT_DEBUG
        qDebug() << "Assert(~BSKMsgManager)";
#endif
        return;
    }
}

bool BSKMsgManager::lockResource()
{
    if(false == m_resurceStatus)
    {
//        while(false == m_resursLock->tryLock()){};
        m_resurceStatus = true;
        return true;
    }
    else
    {
        qWarning("Assert(softly lock) [BSKMsgManager::lockResurce]");
    }
    return false;
}

void BSKMsgManager::unlockResource()
{
    if(true == m_resurceStatus)
    {
//        m_resursLock->unlock();
        m_resurceStatus = false;
    }
    else
    {
        qWarning("Assert(softly unlock) [BSKMsgManager::unlockResurce]");
    }
}

void BSKMsgManager::setInitialListUN() /*noexcept*/
{
//    QList<quint32> listUNIndex(dbManager->getAllUNRecords());
    auto listUNIndex(dbManager->getAllUNRecords());
    if(listUNIndex.isEmpty())
    {
        return;
    }
    while(!listUNIndex.isEmpty())
    {
        try
        {
            QSqlRecord unRecord(dbManager->getUNRecord(listUNIndex.first()));
            listUNIndex.removeFirst();
            if(unRecord.isEmpty())
                return;

            if(BSKSystemType != (TypeUnitNodeSystem)unRecord.value("typeSystemUN").toUInt())
            {
                continue;
            }

            UnitNode *newUN(new UnitNode(this));
            newUN->id = unRecord.value("idUN").toByteArray();
            newUN->mac = unRecord.value("macUN").toByteArray();
            newUN->name = unRecord.value("nameUN").toByteArray();
            newUN->abstractName = unRecord.value("abstractNameUN").toByteArray();
            newUN->typeUN = (TypeUnitNodeDevice)unRecord.value("typeUN").toUInt();
            newUN->typeSystem = (TypeUnitNodeSystem)unRecord.value("typeSystemUN").toUInt();
            newUN->btLink = unRecord.value("link").toByteArray();
            newUN->flag_connection = unRecord.value("linkValid").toBool();
            newUN->graphicsPxmItem->setX(unRecord.value("xPos").toInt());
            newUN->graphicsPxmItem->setY(unRecord.value("yPos").toInt());
            if((quint8)RT_BSKDeviceType == (quint8)newUN->_typeUN())
            {
                newUN->voltage = 0xC6;
            }
            else
            {
                newUN->voltage = unRecord.value("voltage").toUInt();
            }
    //        newUN->voltage = unRecord.value("voltage").toUInt();
            newUN->numArea = unRecord.value("area").toUInt();
            newUN->numNode = unRecord.value("number").toUInt();
            newUN->flag_discharge = unRecord.value("discharge").toBool();
            newUN->dbIndex = unRecord.value("id").toUInt();
            newUN->dbIndexParent = unRecord.value("idParentUN").toUInt();
            newUN->flag_hidden = ((0 != unRecord.value("hidden").toInt()) ? true : false);
    //        newUN->graphicsTxtItem->setX(unRecord.value("xPos").toInt());
    //        newUN->graphicsTxtItem->setY(unRecord.value("yPos").toInt() + 20);
    //        newUN->graphicsTxtItem->setHtml(newUN->getGraphicsTextLabel());

            newUN->geoAltitude = unRecord.value("geoAltitude").toDouble();

            newUN->geoLatitudeFlag = unRecord.value("geoLatitudeFlag").toUInt();
            newUN->geoLatitude = unRecord.value("geoLatitude").toDouble();
            newUN->geoLatitudeDegrees = unRecord.value("geoLatitudeDegrees").toDouble();
            newUN->geoLatitudeMinutes = unRecord.value("geoLatitudeMinutes").toDouble();
            newUN->geoLatitudeSeconds = unRecord.value("geoLatitudeSeconds").toDouble();

            newUN->geoLongitudeFlag = unRecord.value("geoLongitudeFlag").toUInt();
            newUN->geoLongitude = unRecord.value("geoLongitude").toDouble();
            newUN->geoLongitudeDegrees = unRecord.value("geoLongitudeDegrees").toDouble();
            newUN->geoLongitudeMinutes = unRecord.value("geoLongitudeMinutes").toDouble();
            newUN->geoLongitudeSeconds = unRecord.value("geoLongitudeSeconds").toDouble();

            newUN->flag_hidden = ((0 != unRecord.value("hidden").toInt()) ? true : false);

            for(int i(0); i < 2; i++)
            {
                QString strMode("modeVK%1");
                strMode = strMode.arg(i + 1);
                newUN->listSettingVK[i].modeVK = unRecord.value(strMode).toUInt();

                QByteArray nullIDBSK;
                nullIDBSK.append((char)0x00);
                nullIDBSK.append((char)0x00);
                for(int j(0); j < 20; j++)
                {
                    QByteArray idBSK;
                    QString strBSK("ConnectVK%1_%2");
                    strBSK = strBSK.arg(i + 1)
                                   .arg(j + 1);

                    idBSK.append(unRecord.value(strBSK).toByteArray());

                    if(idBSK.isEmpty() || idBSK == nullIDBSK)
                    {
                        newUN->listSettingVK[i].listIDBSK[j] = nullIDBSK;
                    }
                    else
                    {
                        newUN->listSettingVK[i].listIDBSK[j] = idBSK;
                    }
                }

                for(int j(0); j < 5; j++)
                {
                    QString strX("xPosLFlangP_%1"), strY("yPosLFlangP_%1");
                    strX = strX.arg(j + 1);
                    strY = strY.arg(j + 1);
                    newUN->listLFlangP[j]->setPos(unRecord.value(strX).toInt(),
                                                  unRecord.value(strY).toInt());
                }

                for(int j(0); j < 5; j++)
                {
                    QString strX("xPosRFlangP_%1"), strY("yPosRFlangP_%1");
                    strX = strX.arg(j + 1);
                    strY = strY.arg(j + 1);
                    newUN->listRFlangP[j]->setPos(unRecord.value(strX).toInt(),
                                                  unRecord.value(strY).toInt());
                }
            }

            if(UnknownDeviceType != newUN->_typeUN())
            {
    //            newUN->graphicsPxmItem->setFlag(QGraphicsItem::ItemIsMovable);
                newUN->graphicsPxmItem->setFlag(QGraphicsItem::ItemIsSelectable);
                newUN->graphicsPxmItem->setFlag(QGraphicsItem::ItemIsFocusable);
                newUN->graphicsPxmItem->setZValue(1);
            }

            connect(newUN,
                    SIGNAL(updateUN(UnitNode*)),
                    this,
                    SIGNAL(updateUN(UnitNode*)));

            connect(newUN,
                    SIGNAL(uploadUN(UnitNode*)),
                    dbManager,
                    SLOT(uploadUNToDB(UnitNode*)));


    //        connect(newUN,
    //                SIGNAL(updateUN(UnitNode*)),
    //                dbManager,
    //                SLOT(updateUN(UnitNode*)));

            this->listBSKUnitNode.append(newUN);
        }
        catch (std::runtime_error &rte)
        {
            qWarning("catch runtime_error %s [BSKMsgManager::setInitialListUN]", rte.what());
        }
        catch (std::bad_alloc& ba)
        {
            qWarning("catch bad_alloc %s [BSKMsgManager::setInitialListUN]", ba.what());
        }
        catch (std::exception &e)
        {
            qWarning("catch exception %s [BSKMsgManager::setInitialListUN]", e.what());
        }
        catch (...)
        {
            qWarning("catch exception <unknown> [BSKMsgManager::setInitialListUN]");
        }
    }

    for(int i(0), n(this->listBSKUnitNode.size()); i < n; i++)
    {
        if(0 == this->listBSKUnitNode.at(i)->dbIndexParent)
        {
            this->involvingInStructure(this->m_treeRootUN,
                                       this->listBSKUnitNode.at(i));
            emit this->addNewUN(this->listBSKUnitNode.at(i));
        }
        else
        {
            for(int j(0); j < n; j++)
            {
                if(this->listBSKUnitNode.at(j)->dbIndex == this->listBSKUnitNode.at(i)->dbIndexParent)
                {
                    this->involvingInStructure(this->listBSKUnitNode.at(j),
                                               this->listBSKUnitNode.at(i));
                    emit this->addNewUN(this->listBSKUnitNode.at(i));
                    break;
                }
            }
        }
    }
}

UnitNode* BSKMsgManager::possGetUnitNode(QByteArray id) /*noexcept*/
{
    for(int i(0), n(this->listBSKUnitNode.size()); i < n; i++)
    {
        if(id == this->listBSKUnitNode.at(i)->id)
        {
            return this->listBSKUnitNode.at(i);
        }
    }
    return nullptr;
}


UnitNode* BSKMsgManager::possAddUnitNode(QByteArray id) /*noexcept*/
{
    if(id.isEmpty())    //eсли ID пустой
        return nullptr;       //возвращаем 0
    if(!(((quint8)id.at(0) & 0x0F) + (quint8)id.at(1)))
        return nullptr;
    QByteArray name;    //неиспользуемая переменная
    bool addKey(true);  //флаг добавления



    //попытка найти в листе устройство с таким же ID
    for(int i(0), n(this->listBSKUnitNode.size()); i < n; i++)
    {
        if(id == this->listBSKUnitNode.at(i)->id)   //если нашли
        {
            addKey = false;                         //снимаем флаг добавления
            return this->listBSKUnitNode.at(i);     //возвращаем найденный unit node
        }
    }

    if(addKey)  //если флаг добавления установлен
    {
        try
        {
            UnitNode *newUnitNode(new UnitNode);    //указатель на новый unit node
            newUnitNode->clear();                   //очистка

            TypeUnitNodeDevice type((TypeUnitNodeDevice)(((quint8)id.at(0) & (quint8)0xF0) / (quint8)0x10));
            QString strType;
            switch(type)
            {
                case CPP_BSKDeviceType:
                {
                    strType = "AWS";
                    break;
                }
                case RT_BSKDeviceType:
                {
                    strType = "RT";
                    break;
                }
                case S_BSKDeviceType:
                {
                    strType = "S";
                    break;
                }
                case RLO_BSKDeviceType:
                {
                    strType = "RLO";
                    break;
                }
                case RLD_BSKDeviceType:
                {
                    strType = "RLD";
                    break;
                }
                case RWD_BSKDeviceType:
                {
                    strType = "RVD";
                    break;
                }
                case MSO_BSKDeviceType:
                {
                    strType = "MSO";
                    break;
                }
                case BVU_BSKDeviceType:
                {
                    strType = "BVU";
                    break;
                }
                case PKP_BSKDeviceType:
                {
                    strType = "PKP";
                    break;
                }
                case VK_BSKDeviceType:
                {
                    strType = "VK";
                    break;
                }
                case O_BSKDeviceType:
                {
                    strType = "O";
                    break;
                }
                case IK_BSKDeviceType:
                {
                    strType = "IK";
                    break;
                }
                case RW_BSKDeviceType:
                {
                    strType = "RW";
                    break;
                }
                case RVP_BSKDeviceType:
                {
                    strType = "RVP";
                    break;
                }
                case PKSC_BSKDeviceType:
                {
                    strType = "PCSC";
                    break;
                }
                case VSO_BSKDeviceType:
                {
                    strType = "VSO";
                    break;
                }
                default:
                {
                    strType = "SO";
                    break;
                }
            }
            QString strID(id.toHex());
            strID = strID.toUpper();

            QByteArray newName;
            newName.append("BSK_");
            newName.append(strType);
            newName.append("_");
            newName.append(strID);

            newUnitNode->updateName(newName);
            newUnitNode->updateType(BSKSystemType, type);
            newUnitNode->updateID(id);

            this->listBSKUnitNode.append(newUnitNode);//добавление в лист

            //
            newUnitNode->updateDBIndex(this->dbManager->addNewUN(newUnitNode));
            //

            this->involvingInStructure(this->m_treeRootUN,
                                       newUnitNode);

            connect(newUnitNode,
                    SIGNAL(updateUN(UnitNode*)),
                    this,
                    SIGNAL(updateUN(UnitNode*)));

            connect(newUnitNode,
                    SIGNAL(uploadUN(UnitNode*)),
                    dbManager,
                    SLOT(uploadUNToDB(UnitNode*)));

            //для проверки добавления устройств в БД
            /*
            newUnitNode->name = "test_name";//BSK___
            newUnitNode->typeUN = 1;//BSK___
            newUnitNode->id = id;//BSK___
            newUnitNode->flag_alarm = 1;//BSK___
            */
            emit this->addNewUN(newUnitNode);
            return newUnitNode;
        }
        catch (std::runtime_error &rte)
        {
            qWarning("catch runtime_error %s [BSKMsgManager::possAddUnitNode]", rte.what());
        }
        catch (std::bad_alloc& ba)
        {
            qWarning("catch bad_alloc %s [BSKMsgManager::possAddUnitNode]", ba.what());
        }
        catch (std::exception &e)
        {
            qWarning("catch exception %s [BSKMsgManager::possAddUnitNode]", e.what());
        }
        catch (...)
        {
            qWarning("catch exception <unknown> [BSKMsgManager::possAddUnitNode]");
        }
    }
    return nullptr;
}

void BSKMsgManager::extractionChildeFromStructure(UnitNode *un) /*noexcept*/
{
    while(!un->listChilde.isEmpty())
    {
        this->extractionFromStructure(un->listChilde.first());
    }

    un->listChilde.clear();
}

void BSKMsgManager::extractionFromStructure(UnitNode *un) /*noexcept*/
{
    while(!un->listChilde.isEmpty())
    {
        this->extractionFromStructure(un->listChilde.first());
//        un->listChilde.removeFirst();
    }

    if(un->unTreeParent)
    {
        if(un->unTreeParent->listChilde.contains(un))
        {
            un->unTreeParent->listChilde.removeOne(un);
        }
    }
    this->m_treeRootUN->appendChild(un);
    un->listChilde.clear();
    un->updateFlagConnection(false);
}

void BSKMsgManager::involvingInStructure(UnitNode *unMaster,
                                         UnitNode *unSlave) /*noexcept*/
{
    if(unMaster)
    {
        if(!this->m_treeRootUN->listChilde.removeOne(unSlave))
        {
            for(int i(0); i < this->listBSKUnitNode.size(); i++)
            {
                if(this->listBSKUnitNode.at(i)->listChilde.removeOne(unSlave))
                    break;
            }
        }
        unMaster->appendChild(unSlave);
    }
}

//контрольная сумма
QByteArray BSKMsgManager::setCRC8XOR(QByteArray pattern) /*noexcept*/
{
    int n(pattern.size() - 1);
    pattern[n] = (quint8)0x00;
    for(int i(1); i < n; i++)
    {
        pattern[n] = pattern.at(n) ^ pattern.at(i);
    }
    return pattern;
}

void BSKMsgManager::updateStatusBSKExchange(bool status) /*noexcept*/
{
    emit this->ssUpdateStatusBSKExchange(5000);
}

//BSK___проверка контрольной суммы
bool BSKMsgManager::CheckSum(QByteArray pattern) /*noexcept*/
{
    int     n(pattern.size() - 1);
    quint8  sample = 0;

    for(int i(1); i < n; i++)
    {
        sample = sample ^ pattern.at(i);
    }

    if((quint8)pattern.at(n) == sample)
    {
        return true;   //OK
    }
    return false;   //not OK
}
//анализ принятого сообщения от ПКП
void BSKMsgManager::newMSGPKP(QByteArray pattern) /*noexcept*/
{
    auto un(this->possAddUnitNode(pattern.mid(7, 2)));
    if(nullptr == un)
        return;

    if((quint8)0x60 != (quint8)pattern.at(4) ||
       43 > pattern.size())
        return;

    un->updateNumberArea((quint8)pattern.at(9));
    un->updateNumberNode((quint8)pattern.at(10));

    quint8 latitude_f((quint8)pattern.at(40)); // 0x2E: N // 0x33: S
    double latitude_d((pattern.at(11) - 0x10)*10 + pattern.at(12) - 0x10);
    double latitude_m((pattern.at(14) - 0x10)*10 + pattern.at(15) - 0x10);
    double latitude_s((((pattern.at(17) - 0x10)*10 + pattern.at(18) - 0x10)*60) + (((pattern.at(19) - 0x10)*10 + pattern.at(20) - 0x10)));

    quint8 longitude_f((quint8)pattern.at(41)); // 0x25: E // 0x37: W
    double longitude_d((pattern.at(21) - 0x10)*100 + (pattern.at(22) - 0x10)*10 + pattern.at(23) - 0x10);
    double longitude_m((pattern.at(25) - 0x10)*10 + pattern.at(26) - 0x10);
    double longitude_s((((pattern.at(28) - 0x10)*10 + pattern.at(29) - 0x10)*60) + (((pattern.at(30) - 0x10)*10 + pattern.at(31) - 0x10)));

    un->updateGeoLatitude(latitude_d, latitude_m, latitude_s, latitude_f);
    un->updateGeoLongitude(longitude_d, longitude_m, longitude_s, longitude_f);

}


//анализ принятого сообщения___
void BSKMsgManager::newMSG(QByteArray pattern) /*noexcept*/
{
    this->emitTicPattern();

    bool needUnlock(false);
    needUnlock = this->lockResource();

    bool flag_phantom(false);


    //наличие соединения
    this->updateStatusBSKExchange(true);

    //ПКП____________________________________
    if((quint8)0x80 == ((quint8)pattern.at(1) & 0xF0) && //проверка, от ПКП ли пакет (код 0x80)
       (quint8)0xFF == (quint8)pattern.at(2))
    {

        switch((quint8)pattern.at(4))
        {
            case (quint8)0x60://PKP
            {
                this->analyzer_0x60_PKP(pattern);


                return;
            }
        }
    }

    //ПУЦ____________________________________
    if(
            (
                (quint8)0x40 == ((quint8)pattern.at(1) & 0xF0) && //проверка, нам ли пакет и от базы ли он
                (quint8)0xFF == (quint8)pattern.at(2)
            ) ||
            (quint8)0xCE == (quint8)pattern.at(4) //0xCE - исключение
      )
    {       
        //qDebug() << "size?       " << (quint16)pattern.at(3);
        //qDebug() << "array size? " << pattern.size();

        this->requrst_0x0D();

        switch((quint8)pattern.at(4))
        {
            case (quint8)0xCE:
            {

//                qDebug() << "0XCE command";
                this->analyzer_CE(pattern);
                break;
            }

            case (quint8)0xA1:
            {

                this->analyzer_A1(pattern);
                break;


                //информация о ПУЦ (добавить в класс)
            }

            case (quint8)0x9F://поймали 0x9F
            {

                //проверка на фантом --> **********
                flag_phantom = 0;
                if(((quint8)pattern.at(3+1) == 0x9F) && ((quint8)pattern.at(4+1) != 0xCC) && ((quint8)pattern.at(4+1) != 0x20) && ((quint8)pattern.at(4+1) != 0xC0)    && ((quint8)pattern.at(4+1) != 0x19) && ((quint8)pattern.at(4+1) != 0x1A) && ((quint8)pattern.at(4+1) != 0x1B) && ((quint8)pattern.at(4+1) != 0x1C) && ((quint8)pattern.at(4+1) != 0x1E) && ((quint8)pattern.at(4+1) != 0x1F))
                {
                    if(((quint8)pattern.at(9+1)>15) || ((quint8)pattern.at(10+1)>40) || (((quint8)pattern.at(6+1)&0xf) > 9) || (((quint8)pattern.at(7+1)&0xf) > 9) )
                    {
                        if((quint8)pattern.at(4+1) != 0x17)//на эту команду не распространяется проверка фантомов
                        {
                            flag_phantom = 1;
                        }
                    }
                }

                if(((quint8)pattern.at(3+1) == 0x9F) && (((quint8)pattern.at(4+1) == 0x20) || ((quint8)pattern.at(4+1) == 0xC0))     && ((quint8)pattern.at(4+1) != 0x19) && ((quint8)pattern.at(4+1) != 0x1A) && ((quint8)pattern.at(4+1) != 0x1B) && ((quint8)pattern.at(4+1) != 0x1C) && ((quint8)pattern.at(4+1) != 0x1E) && ((quint8)pattern.at(4+1) != 0x1F))
                {
                    if(
                        ((((quint8)pattern.at(6+1)&0xf) > 9) || (((quint8)pattern.at(7+1)&0xf) > 9) || ((((quint8)pattern.at(7+1)>>4)&0xf) > 9)) ||
                        ((((quint8)pattern.at(8+1)&0xf) > 9) || (((quint8)pattern.at(9+1)&0xf) > 9) || ((((quint8)pattern.at(9+1)>>4)&0xf) > 9))
                      )
                      {
                            if((quint8)pattern.at(4+1) != 0x17)//на эту команду не распространяется проверка фантомов
                            {
                                flag_phantom = 1;
                                //CStream->AddPhantom(ch[7]|ch[6]8, ch[9], ch[10], L" ", ch[4], ch, len+1);	//запись фантома_
                            }
                      }
                }
                if(flag_phantom)
                {
                    break;
                }
                //проверка на фантом <-- **********

                //найти устройство -->
                UnitNode *un(nullptr);
                if((quint8)0xC0 != (quint8)pattern.at(5) &&
                   (quint8)0x20 != (quint8)pattern.at(5) &&
                   (quint8)0x11 != (quint8)pattern.at(5))
                {
                    un = this->possAddUnitNode(pattern.mid(7, 2));
                }
                else
                {
                    un = this->possAddUnitNode(pattern.mid(9, 2));
                }

                if(!un)
                {
                    break;
                }

                un->updateFlagConnection(true);
                un->updateFlagHidden(false);

                if((quint8)0xCC != (quint8)pattern.at(5) &&
                   (quint8)0xC0 != (quint8)pattern.at(5) &&
                   (quint8)0x20 != (quint8)pattern.at(5))
                {
                    un->updateVoltage(pattern.at(9));
                }

                //найти устройство <--

                switch((quint8)pattern.at(5))
                {
                    case (quint8)0x01://тревога без классификации
                    {

                        this->analyzer_0x9F_0x01(pattern, un);
                        break;
                    }
                    case (quint8)0x02://тревога с классификацией
                    {

                        this->analyzer_0x9F_0x02(pattern, un);
                        break;
                    }
                    case (quint8)0x11://тревога принята на пкп
                    {

//                        this->analyzer_0x9F_0x11(pattern, un);
                        break;
                    }
                    case (quint8)0x20://есть связь
                    {
                        this->analyzer_0x9F_0x20(pattern, un);
                        break;
                    }
                    case (quint8)0x21://передача счёткика смены ведущего
                    {
//                        this->analyzer_0x9F_0x21(pattern, un);
                        break;
                    }
                    case (quint8)0x22://передача счёткика потери синхронизации с ведущим
                    {
//                        this->analyzer_0x9F_0x22(pattern, un);
                        break;
                    }
                    case (quint8)0x23://счёткика потери сети обнулён
                    {
//                        this->analyzer_0x9F_0x23(pattern, un);
                        break;
                    }
                    case (quint8)0x40://неисправность
                    {
                        this->analyzer_0x9F_0x40(pattern, un);
                        break;
                    }
                    case (quint8)0x42://норма батареи передатчика РЛД
                    {
                        this->analyzer_0x9F_0x42(pattern, un);
                        break;
                    }
                    case (quint8)0x43://разряд батареи передатчика РЛД
                    {
                        this->analyzer_0x9F_0x43(pattern, un);
                        break;
                    }
                    case (quint8)0x44://разряд батареи передатчика РЛД
                    {
                        this->analyzer_0x9F_0x44(pattern, un);
                        break;
                    }
                    case (quint8)0x90://питание подключено (доработать для ПУЦ)
                    {
                        this->analyzer_0x9F_0x90(pattern, un);
                        break;
                    }
                    case (quint8)0x91://питание подключено (доработать для ПУЦ)
                    {
                        this->analyzer_0x9F_0x91(pattern, un);
                        break;
                    }
                    case (quint8)0xBA://разряд
                    {
                        this->analyzer_0x9F_0xBA(pattern, un);
                        break;
                    }
                    case (quint8)0xBB://разряд
                    {
                        this->analyzer_0x9F_0xBB(pattern, un);
                        break;
                    }
                    case (quint8)0xBD://отключение по разряду ИПА
                    {

                        this->analyzer_0x9F_0xBD(pattern, un);
                        break;
                    }
                    case (quint8)0xC0://нет связи
                    {
                        this->analyzer_0x9F_0xC0(pattern, un);
                        break;
                    }
                    case (quint8)0xCC://подтверждение выполнения команд
                    {
    //                        qDebug() << "0xCC command";
                        this->analyzer_0x9F_0xCC(pattern, un);
                        break;
                    }
                    case (quint8)0xD0://норма СО
                    {
                        this->analyzer_0x9F_0xD0(pattern, un);
                        break;
                    }
                    case (quint8)0xD1://норма СО
                    {
                        this->analyzer_0x9F_0xD1(pattern, un);
                        break;
                    }
                    case (quint8)0xD2://норма СО
                    {
                        this->analyzer_0x9F_0xD2(pattern, un);
                        break;
                    }
                    case (quint8)0xD3://норма СО
                    {
                        this->analyzer_0x9F_0xD3(pattern, un);
                        break;
                    }

                    default:
                    {
#ifdef QT_DEBUG
                        if(100 > pattern.size())
                        {
                            qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " KsMSG  INPUT  << [command 9F_XX] [" << pattern.toHex() << "]";
                        }
                        else
                        {
                            qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " KsMSG  INPUT  << [command 9F_XX] [" << pattern.left(20).toHex() << "..." << pattern.size() << "..." << pattern.right(10).toHex() << "]";
                        }
#endif
                        this->emitInsertNewMSG(un,
                                               pattern.at(4),
                                               pattern,
                                               true,
                                               Error_TypeGroupMSG,
                                               true);
                        break;
                    }
                }
            }

            default:
            {
    #ifdef QT_DEBUG
                if(100 > pattern.size())
                {
                    qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " KsMSG  INPUT  << [command 9F_XX] [" << pattern.toHex() << "]";
                }
                else
                {
                    qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " KsMSG  INPUT  << [command 9F_XX] [" << pattern.left(20).toHex() << "..." << pattern.size() << "..." << pattern.right(10).toHex() << "]";
                }
    #endif
                this->emitInsertNewMSG(nullptr,
                                       pattern.at(4),
                                       pattern,
                                       true,
                                       Error_TypeGroupMSG,
                                       true);
                break;
            };
        }
    }

    if(needUnlock){this->unlockResource();}

//#ifdef QT_DEBUG
//    qDebug() << "Something is wrong with it: " << pattern.toHex();
//#endif
}






//отправляемые устройствам команды ********************************

///добавить команды выбора номера комплекта и конфигурации (номер участка и СО)
//квитанция по приёму
void BSKMsgManager::requrst_0x0D() /*noexcept*/
{
     QByteArray msg0D;

     msg0D.append((quint8)0xA5);
     msg0D.append((quint8)0xFF);
     msg0D.append((quint8)0x40);
     msg0D.append((char)0x00);
     msg0D.append((quint8)0x0D);
     msg0D.append((char)0x00);

     msg0D = setCRC8XOR(msg0D);

     msg0D.append((quint8)0xFF);

     emit this->sendBSKcommand(msg0D);
//     qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " BskMSG OUTPUT << [command    0D] [" << msg0D.toHex() << "]";
}

void BSKMsgManager::request_0x60_PKP() /*noexcept*///PKP
{}

//команда установить номер комплекта
void BSKMsgManager::set_kit_number(quint8 kit_num) /*noexcept*/
{
    QByteArray msg0B;

    msg0B.append((quint8)0xA5);
    msg0B.append((quint8)0xFF);
    msg0B.append((quint8)0x40);
    msg0B.append((quint8)0x01);
    msg0B.append((quint8)0x0B);
    msg0B.append(kit_num);
    msg0B.append((char)0x00);

    msg0B = setCRC8XOR(msg0B);

    emit this->sendBSKcommand(msg0B);
#ifdef QT_DEBUG
    qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " BskMSG OUTPUT << [command    0B] [" << msg0B.toHex() << "]";
#endif
}

//команда сконфигурировать подключенное СО
void BSKMsgManager::set_configuration(quint8 kit_num, quint8 area_num, quint8 so_num) /*noexcept*/
{
    QByteArray command;

    command.append((quint8)0xA5);
    command.append((quint8)0xFF);
    command.append((quint8)0x40);
    command.append((quint8)0x06);
    command.append((quint8)0x41);
    command.append((quint8)0xFE);
    command.append((quint8)0xFE);
    command.append((quint8)0x7C);
    command.append((quint8)kit_num);
    command.append((quint8)area_num);
    command.append((quint8)so_num);
    command.append((char)0x00);

    command = setCRC8XOR(command);

    emit this->sendBSKcommand(command);
#ifdef QT_DEBUG
    qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " BskMSG OUTPUT << [command 41_7C] [" << command.toHex() << "]";
#endif
}

//настройка датчиков
//команда установки чувствительноси РВП
void BSKMsgManager::set_RVP_sensitivity(UnitNode *device, quint8 sensitivity) /*noexcept*/
{
    /*
    sensitivity == 1 - установить чувствительность 1
    ...
    sensitivity == 8 - установить чувствительность 8
    */

    if(!device)
    {
        return;
    }

    if(RVP_BSKDeviceType != device->_typeUN())
    {
        return;
    }

    QByteArray command;

    command.append((quint8)0xA5);
    command.append((quint8)0xFF);
    command.append((quint8)0x40);
    command.append((quint8)0x06);
    command.append((quint8)0x41); //Передать команду
    command.append(device->_id());
    command.append((quint8)0x40); //Установить чувствительность
    command.append((quint8)(sensitivity - 1)); //чувствительность
    command.append((char)0x00);
    command.append((char)0x00);
    command.append((char)0x00);

    command = setCRC8XOR(command);

    emit this->sendBSKcommand(command);
#ifdef QT_DEBUG
    qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " BskMSG OUTPUT << [command 41_40] [" << command.toHex() << "]";
    qDebug() << "set RVP sensitivity to " << sensitivity;
#endif
}

void BSKMsgManager::request_0x0B(quint8 numKit) /*noexcept*/
{
    QByteArray msg0x0B;

    msg0x0B.append((quint8)0xA5);
    msg0x0B.append((quint8)0xFF);
    msg0x0B.append((quint8)0x40);
    msg0x0B.append((quint8)0x01);
    msg0x0B.append((quint8)0x0B); //Передать команду
    msg0x0B.append(numKit);
    msg0x0B.append((char)0x00);

    msg0x0B = setCRC8XOR(msg0x0B);

    emit this->sendBSKcommand(msg0x0B);
#ifdef QT_DEBUG
    qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " BskMSG OUTPUT << [command    0B] [" << msg0x0B.toHex() << "]";
#endif
    this->emitInsertNewMSG(0x00,
                           msg0x0B.at(4),
                           msg0x0B,
                           true,
                           Command_TypeGroupMSG);
}

void BSKMsgManager::request_0x41(UnitNode *un,
                                 quint8 command,
                                 quint8 d1,
                                 quint8 d2,
                                 quint8 d3) /*noexcept*/
{
    QByteArray msg0x41;

    msg0x41.append((quint8)0xA5);
    msg0x41.append((quint8)0xFF);
    msg0x41.append((quint8)0x40);
    msg0x41.append((quint8)0x06);
    msg0x41.append((quint8)0x41); //Передать команду
    if(nullptr != un) {
        msg0x41.append(un->_id());
    }
    else {
        msg0x41.append((quint8)0xFE);
        msg0x41.append((quint8)0xFE);
    }
    msg0x41.append(command); //Поменять значение регистра
    msg0x41.append(d1);
    msg0x41.append(d2);
    msg0x41.append(d3);
    msg0x41.append((char)0x00);

    msg0x41 = setCRC8XOR(msg0x41);

    emit this->sendBSKcommand(msg0x41);
#ifdef QT_DEBUG
    qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " BskMSG OUTPUT << [command 41_XX] [" << msg0x41.toHex() << "]";
#endif
    this->emitInsertNewMSG(un,
                           msg0x41.at(4),
                           msg0x41,
                           true,
                           Command_TypeGroupMSG);

}

void BSKMsgManager::set_S_reg(UnitNode *device,
                              quint8 addr,
                              bool value) /*noexcept*/
{
    if(!device)
    {
        return;
    }

    if(S_BSKDeviceType != device->_typeUN())
    {
        return;
    }

    QByteArray command;

    command.append((quint8)0xA5);
    command.append((quint8)0xFF);
    command.append((quint8)0x40);
    command.append((quint8)0x06);
    command.append((quint8)0x41); //Передать команду
    command.append(device->_id());
    command.append((quint8)0x75); //Поменять значение регистра
    command.append(addr);
    command.append((value ? ((quint8)0x01) : ((char)0x00)));
    command.append((char)0x00);
    command.append((char)0x00);

    command = setCRC8XOR(command);

    emit this->sendBSKcommand(command);
#ifdef QT_DEBUG
    qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " BskMSG OUTPUT << [command 41_75] [" << command.toHex() << "]";
    qDebug() << "set S reg[" << addr << "]:" << value;
#endif
}




//команда запроса конфигурации устройства
void BSKMsgManager::get_config(UnitNode *device) /*noexcept*/
{
    if(!device)
    {
        return;
    }

    if(CPP_BSKDeviceType == device->_typeUN())
    {
        return;
    }

    QByteArray command;

    command.append((quint8)0xA5);
    command.append((quint8)0xFF);
    command.append((quint8)0x40);
    command.append((quint8)0x06);
    command.append((quint8)0x41);
    command.append(device->_id());
    command.append((quint8)0x7B);		//код команды передачи конфигурации
    command.append((char)0x00);
    command.append((char)0x00);
    command.append((char)0x00);
    command.append((char)0x00);

    command = setCRC8XOR(command);

    emit this->sendBSKcommand(command);
#ifdef QT_DEBUG
    qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " BskMSG OUTPUT << [command 41_7B] [" << command.toHex() << "]";
    qDebug() << "send configuration request for " << device->id.toHex();
#endif
}


//отправляемые устройствам команды ********************************


//обработчики сообщений БСК **********************************************
//добавить фильтр фантомов (+)
//добавить защиту от повторов пакетов
//таймер связи с ПУЦ
//следить, чтобы страрший полубайт старшего байта ID соответствовал типам СО
//отображать напряжение
//старого БВУ не будет - будет ВСО

//PKP (пакет от ПКП с конфигурацией одного устройства)
void BSKMsgManager::analyzer_0x60_PKP(QByteArray pattern) /*noexcept*/
{
#ifdef QT_DEBUG
    if(100 > pattern.size())
    {
        qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " BskMSG INPUT  << [command    0x60 PKP] [" << pattern.toHex() << "]";
    }
    else
    {
        qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " BskMSG INPUT  << [command    0x60 PKP] [" << pattern.left(20).toHex() << "..." << pattern.size() << "..." << pattern.right(10).toHex() << "]";
    }

#endif
}

void BSKMsgManager::analyzer_CE(QByteArray pattern, UnitNode *un) /*noexcept*/
{
#ifdef QT_DEBUG
    if(100 > pattern.size())
    {
        qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " BskMSG INPUT  << [command    CE] [" << pattern.toHex() << "]";
    }
    else
    {
        qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " BskMSG INPUT  << [command    CE] [" << pattern.left(20).toHex() << "..." << pattern.size() << "..." << pattern.right(10).toHex() << "]";
    }
#endif

    //1 получить ID ПУЦ из пакета
    un = this->possAddUnitNode(pattern.mid(5, 2));

    if(!un)
    {
        return;
    }

    un->updateFlagConnection(true);

    int countConnections(pattern.at(7));
    auto listDisconnected(this->listBSKUnitNode);
    for(int i(0); i < countConnections; i++)
    {

        UnitNode *unSlave(this->possAddUnitNode(pattern.mid(8 + 4 * i, 2))),
                 *unMaster(this->possAddUnitNode(pattern.mid(10 + 4 * i, 2)));

        this->involvingInStructure(unMaster,
                                   unSlave);

        unMaster->updateFlagConnection(true);
        unSlave->updateFlagConnection(true);
        listDisconnected.removeOne(unMaster);
        unMaster->updateFlagHidden(false);
        listDisconnected.removeOne(unSlave);
        unSlave->updateFlagHidden(false);
    }
    for(auto item: listDisconnected)
    {
        item->updateFlagConnection(false);
    }
}


//обработчик команды 0xA1 ******************************************************************
void BSKMsgManager::analyzer_A1(QByteArray pattern, UnitNode *un) /*noexcept*/
{
//    if(100 > pattern.size())
//    {
//        qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " BskMSG INPUT  << [command    A1] [" << pattern.toHex() << "]";
//    }
//    else
//    {
//        qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " BskMSG INPUT  << [command    A1] [" << pattern.left(20).toHex() << "..." << pattern.size() << "..." << pattern.right(10).toHex() << "]";
//    }

    //1 получить ID ПУЦ из пакета
    QByteArray id;
    id.append(pattern.at(7));
    id.append(pattern.at(8));

    un = this->possAddUnitNode(pattern.mid(7, 2));

    if(!un)
    {
        return;
    }
    un->updateFlagHidden(false);

    un->updateFlagConnection(true);
    un->updateVoltage(pattern.at(9));
    currentKitNum = un->numComplex = pattern.at(10);

//    this->emitInsertNewMSG(un,
//                           pattern.at(4),
//                           pattern,
//                           true,
//                           Command_TypeGroupMSG);

//    qDebug() << "command 0xA1: " << pattern.toHex() << " ID == " << id.toHex();
}






//отключение по разряду ИПА ******************************************************************
void BSKMsgManager::analyzer_0x9F_0xBD(QByteArray pattern, UnitNode *un) /*noexcept*/
{    
#ifdef QT_DEBUG
    if(100 > pattern.size())
    {
        qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " BskMSG INPUT  << [command 9F_BD] [" << pattern.toHex() << "]";
    }
    else
    {
        qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " BskMSG INPUT  << [command 9F_BD] [" << pattern.left(20).toHex() << "..." << pattern.size() << "..." << pattern.right(10).toHex() << "]";
    }
#endif

    this->emitInsertNewMSG(un,
                           pattern.at(4),
                           pattern,
                           true,
                           Discharge_TypeGroupMSG);

    //3 установить флаги
    un->updateNumberArea(pattern.at(10));
    un->updateNumberNode(pattern.at(11));
    un->updateFlagDischarge(true);
    un->updateFlagOff(true);
    un->updateFlagConnection(false);
}

//тревога с классификацией ******************************************************************
void BSKMsgManager::analyzer_0x9F_0x02(QByteArray pattern, UnitNode *un) /*noexcept*/
{
    if(!this->beeper->isRunning() &&
       this->beeperFlag)
    {
        this->beeper->start();
    }

#ifdef QT_DEBUG
    if(100 > pattern.size())
    {
        qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " BskMSG INPUT  << [command 9F_02] [" << pattern.toHex() << "]";
    }
    else
    {
        qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " BskMSG INPUT  << [command 9F_02] [" << pattern.left(20).toHex() << "..." << pattern.size() << "..." << pattern.right(10).toHex() << "]";
    }
#endif

    this->emitInsertNewMSG(un,
                           pattern.at(4),
                           pattern,
                           true,
                           Alarm_TypeGroupMSG);

    //3 установить флаги
    un->updateNumberArea(pattern.at(10));
    un->updateNumberNode(pattern.at(11));
    un->updateFlagConnection(true);
    un->setAlarmCondition();



//    if((un) && (id[0]>>4 == 2))//если удалось получить устройство и указатель ненулевой и ID соответствует сейсмике
//    {

//        switch((quint8)pattern.at(11+1))
//        {
//            case 1: txt_message_str = "Внимание.";	break;
//            case 2: txt_message_str = "Тревога."; 	break;
//            case 3: txt_message_str = "Внимание. Класс нарушителя не определен.";	break;
//            case 4: txt_message_str = "Тревога. Одиночный.";	break;
//            case 5: txt_message_str = "Тревога. Группа.";	break;
//            case 6: txt_message_str = "Тревога. Транспортное средство.";	break;
//            case 7: txt_message_str = "Внимание. Удаленное транспортное средство."; break;
//            case 8: txt_message_str = "Внимание. Поезд."; break;
//            case 9: txt_message_str = "Внимание. Животное.";	break;

//            case 10: txt_message_str = "Внимание. Тяжелое транспортное средство."; break;
//            case 11: txt_message_str = "Внимание."; break;
//            case 12: txt_message_str = "Направление на объект.";	break;
//            case 13: txt_message_str = "Направление с объекта."; 	break;

//            case 14: txt_message_str = "Тревога. Класс нарушителя не определен."; break;
//            case 15: txt_message_str = "Внимание. Одиночный."; break;

//            case 19: txt_message_str = "Внимание. Группа."; break;
//            case 20: txt_message_str = "Внимание. Транспортное средство."; break;
//            case 21: txt_message_str = "Тревога. Удаленное транспортное средство."; 	break;
//            case 22: txt_message_str = "Тревога. Поезд."; break;
//            case 23: txt_message_str = "Тревога. Животное."; break;
//            case 24: txt_message_str = "Тревога. Тяжелое транспортное средство."; break;
//        }


//        //номер участка и номер СО


//        qDebug() << "Alarm on Seismic " << id.toHex();
//    }
}




//тревога без классификации ******************************************************************
void BSKMsgManager::analyzer_0x9F_0x01(QByteArray pattern, UnitNode *un) /*noexcept*/
{    
    if(!this->beeper->isRunning() &&
       this->beeperFlag)
    {
        this->beeper->start();
    }

#ifdef QT_DEBUG
    if(100 > pattern.size())
    {
        qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " BskMSG INPUT  << [command 9F_01] [" << pattern.toHex() << "]";
    }
    else
    {
        qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " BskMSG INPUT  << [command 9F_01] [" << pattern.left(20).toHex() << "..." << pattern.size() << "..." << pattern.right(10).toHex() << "]";
    }
#endif

    //4 запись в БД
    this->emitInsertNewMSG(un,
                           pattern.at(4),
                           pattern,
                           true,
                           Alarm_TypeGroupMSG);
    //3 установить флаги
    un->updateNumberArea(pattern.at(10));
    un->updateNumberNode(pattern.at(11));
    un->updateFlagConnection(true);
    un->setAlarmCondition();//установка флага тревоги
}

//неисправность ******************************************************************
void BSKMsgManager::analyzer_0x9F_0x40(QByteArray pattern, UnitNode *un) /*noexcept*/
{
#ifdef QT_DEBUG
    if(100 > pattern.size())
    {
        qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " BskMSG INPUT  << [command 9F_40] [" << pattern.toHex() << "]";
    }
    else
    {
        qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " BskMSG INPUT  << [command 9F_40] [" << pattern.left(20).toHex() << "..." << pattern.size() << "..." << pattern.right(10).toHex() << "]";
    }
#endif

    //4 запись в БД
    this->emitInsertNewMSG(un,
                           pattern.at(4),
                           pattern,
                           true,
                           Other_TypeGroupMSG);
    //3 установить флаги
    un->updateNumberArea(pattern.at(10));
    un->updateNumberNode(pattern.at(11));
    un->updateFlagFailure(true);//установка флага неисправности
}

//Эта команда частично копирует команду ПИТАНИЕ ПОДКЛЮЧЕНО (0xBA нужна для сброса разряда без перевключения устройства)
void BSKMsgManager::analyzer_0x9F_0xBA(QByteArray pattern, UnitNode *un) /*noexcept*/
{
#ifdef QT_DEBUG
    if(100 > pattern.size())
    {
        qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " BskMSG INPUT  << [command 9F_BA] [" << pattern.toHex() << "]";
    }
    else
    {
        qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " BskMSG INPUT  << [command 9F_BA] [" << pattern.left(20).toHex() << "..." << pattern.size() << "..." << pattern.right(10).toHex() << "]";
    }
#endif
    this->emitInsertNewMSG(un,
                           pattern.at(4),
                           pattern,
                           true,
                           Discharge_TypeGroupMSG);

    un->updateFlagConnection(true);
    un->updateNumberArea((quint8)pattern.at(10));
    un->updateNumberNode((quint8)pattern.at(11));
    un->updateFlagDischarge(false);
}

//разряд ******************************************************************
void BSKMsgManager::analyzer_0x9F_0xBB(QByteArray pattern, UnitNode *un) /*noexcept*/
{
#ifdef QT_DEBUG
    if(100 > pattern.size())
    {
        qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " BskMSG INPUT  << [command 9F_BB] [" << pattern.toHex() << "]";
    }
    else
    {
        qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " BskMSG INPUT  << [command 9F_BB] [" << pattern.left(20).toHex() << "..." << pattern.size() << "..." << pattern.right(10).toHex() << "]";
    }
#endif

    //4 запись в БД
    this->emitInsertNewMSG(un,
                           pattern.at(4),
                           pattern,
                           true,
                           Discharge_TypeGroupMSG);

    //3 установить флаги
    un->updateFlagConnection(true);
    un->updateNumberArea(pattern.at(10));
    un->updateNumberNode(pattern.at(11));
    un->updateFlagDischarge(true);
}





//норма СО ******************************************************************
void BSKMsgManager::analyzer_0x9F_0xD0(QByteArray pattern, UnitNode *un) /*noexcept*/
{
#ifdef QT_DEBUG
    if(100 > pattern.size())
    {
        qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " BskMSG INPUT  << [command 9F_D0] [" << pattern.toHex() << "]";
    }
    else
    {
        qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " BskMSG INPUT  << [command 9F_D0] [" << pattern.left(20).toHex() << "..." << pattern.size() << "..." << pattern.right(10).toHex() << "]";
    }
#endif

    //4 запись в БД
    this->emitInsertNewMSG(un,
                           pattern.at(4),
                           pattern,
                           true,
                           Other_TypeGroupMSG);

    //3 установить флаги
//    un->updateFlagAlarm(false);
    un->updateFlagAttention(false);
    un->updateFlagFailure(false);
    un->updateFlagOff(false);
}

void BSKMsgManager::analyzer_0x9F_0xD1(QByteArray pattern, UnitNode *un) /*noexcept*/
{
#ifdef QT_DEBUG
    if(100 > pattern.size())
    {
        qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " BskMSG INPUT  << [command 9F_D1] [" << pattern.toHex() << "]";
    }
    else
    {
        qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " BskMSG INPUT  << [command 9F_D1] [" << pattern.left(20).toHex() << "..." << pattern.size() << "..." << pattern.right(10).toHex() << "]";
    }
#endif

    //4 запись в БД
    this->emitInsertNewMSG(un,
                           pattern.at(4),
                           pattern,
                           true,
                           Other_TypeGroupMSG);

    //3 установить флаги
    un->updateFlagConnection(false);
    un->updateNumberArea(pattern.at(10));
    un->updateNumberNode(pattern.at(11));
}

void BSKMsgManager::analyzer_0x9F_0xD2(QByteArray pattern, UnitNode *un) /*noexcept*/
{
#ifdef QT_DEBUG
    if(100 > pattern.size())
    {
        qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " BskMSG INPUT  << [command 9F_D2] [" << pattern.toHex() << "]";
    }
    else
    {
        qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " BskMSG INPUT  << [command 9F_D2] [" << pattern.left(20).toHex() << "..." << pattern.size() << "..." << pattern.right(10).toHex() << "]";
    }
#endif

    //4 запись в БД
    this->emitInsertNewMSG(un,
                           pattern.at(4),
                           pattern,
                           true,
                           Other_TypeGroupMSG);

    //3 установить флаги
    un->updateFlagFailure(true);
    un->updateNumberArea(pattern.at(10));
    un->updateNumberNode(pattern.at(11));
}

void BSKMsgManager::analyzer_0x9F_0xD3(QByteArray pattern, UnitNode *un) /*noexcept*/
{
#ifdef QT_DEBUG
    if(100 > pattern.size())
    {
        qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " BskMSG INPUT  << [command 9F_D3] [" << pattern.toHex() << "]";
    }
    else
    {
        qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " BskMSG INPUT  << [command 9F_D3] [" << pattern.left(20).toHex() << "..." << pattern.size() << "..." << pattern.right(10).toHex() << "]";
    }
#endif

    //4 запись в БД
    this->emitInsertNewMSG(un,
                           pattern.at(4),
                           pattern,
                           true,
                           Other_TypeGroupMSG);


    //3 установить флаги
    un->updateFlagDischarge(true);
    un->updateNumberArea(pattern.at(10));
    un->updateNumberNode(pattern.at(11));
}

//есть связь ******************************************************************
void BSKMsgManager::analyzer_0x9F_0x20(QByteArray pattern, UnitNode *un) /*noexcept*/
{
#ifdef QT_DEBUG
    if(100 > pattern.size())
    {
        qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " BskMSG INPUT  << [command 9F_20] [" << pattern.toHex() << "]";
    }
    else
    {
        qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " BskMSG INPUT  << [command 9F_20] [" << pattern.left(20).toHex() << "..." << pattern.size() << "..." << pattern.right(10).toHex() << "]";
    }
#endif

    //поиск устройства среди существующих
    UnitNode *unMaster(this->possAddUnitNode(pattern.mid(7, 2))),//поиск
             *unSlave(un);//(this->possAddUnitNode(pattern.mid(9, 2)));

    if(unMaster || unSlave)
    {
        return;
    }

    this->emitInsertNewMSG(un,
                           pattern.at(4),
                           pattern,
                           true,
                           Connect_TypeGroupMSG);

    unMaster->updateFlagConnection(true);
    unSlave->updateFlagConnection(true);

    this->involvingInStructure(unMaster, unSlave);
}

//норма батареи передатчика РЛД
void BSKMsgManager::analyzer_0x9F_0x42(QByteArray pattern, UnitNode *un) /*noexcept*/
{
    if(!un)
    {
        return;
    }

#ifdef QT_DEBUG
    if(100 > pattern.size())
    {
        qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " BskMSG INPUT  << [command 9F_42] [" << pattern.toHex() << "]";
    }
    else
    {
        qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " BskMSG INPUT  << [command 9F_42] [" << pattern.left(20).toHex() << "..." << pattern.size() << "..." << pattern.right(10).toHex() << "]";
    }
#endif

    this->emitInsertNewMSG(un,
                           pattern.at(4),
                           pattern,
                           true,
                           Other_TypeGroupMSG);

    //3 установить флаги
    un->updateFlagDischarge(false);
    un->updateFlagConnection(false);
}

void BSKMsgManager::analyzer_0x9F_0x43(QByteArray pattern, UnitNode *un) /*noexcept*/
{
    if(!un)
    {
        return;
    }

#ifdef QT_DEBUG
    if(100 > pattern.size())
    {
        qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " BskMSG INPUT  << [command 9F_43] [" << pattern.toHex() << "]";
    }
    else
    {
        qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " BskMSG INPUT  << [command 9F_43] [" << pattern.left(20).toHex() << "..." << pattern.size() << "..." << pattern.right(10).toHex() << "]";
    }
#endif

    this->emitInsertNewMSG(un,
                           pattern.at(4),
                           pattern,
                           true,
                           Other_TypeGroupMSG);

    //3 установить флаги
    un->updateFlagDischarge(true);
    un->updateFlagConnection(false);
}

//команда не дошла
void BSKMsgManager::analyzer_0x9F_0x44(QByteArray pattern, UnitNode *un) /*noexcept*/
{
#ifdef QT_DEBUG
    if(100 > pattern.size())
    {
        qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " BskMSG INPUT  << [command 9F_44] [" << pattern.toHex() << "]";
    }
    else
    {
        qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " BskMSG INPUT  << [command 9F_44] [" << pattern.left(20).toHex() << "..." << pattern.size() << "..." << pattern.right(10).toHex() << "]";
    }
#endif

}

//подключение устройства (питание подключено + ПУЦ подключен)******************************************************************
void BSKMsgManager::analyzer_0x9F_0x90(QByteArray pattern, UnitNode *un) /*noexcept*/
{
#ifdef QT_DEBUG
    if(100 > pattern.size())
    {
        qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " BskMSG INPUT  << [command 9F_90] [" << pattern.toHex() << "]";
    }
    else
    {
        qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " BskMSG INPUT  << [command 9F_90] [" << pattern.left(20).toHex() << "..." << pattern.size() << "..." << pattern.right(10).toHex() << "]";
    }
#endif

    this->emitInsertNewMSG(un,
                           pattern.at(4),
                           pattern,
                           true,
                           Discharge_TypeGroupMSG);

    if(CPP_BSKDeviceType == un->_typeUN())
    {
        this->involvingInStructure(this->m_treeRootUN,
                                   un);
    }

    un->updateFlagConnection(true);
    un->updateFlagAttention(false);
    un->updateFlagFailure(false);
    un->updateFlagDischarge(false);
    un->updateFlagOff(false);

}

//отключение устройства (питание отключено)******************************************************************
void BSKMsgManager::analyzer_0x9F_0x91(QByteArray pattern, UnitNode *un) /*noexcept*/
{
#ifdef QT_DEBUG
    if(100 > pattern.size())
    {
        qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " BskMSG INPUT  << [command 9F_91] [" << pattern.toHex() << "]";
    }
    else
    {
        qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " BskMSG INPUT  << [command 9F_91] [" << pattern.left(20).toHex() << "..." << pattern.size() << "..." << pattern.right(10).toHex() << "]";
    }
#endif

    this->emitInsertNewMSG(un,
                           pattern.at(4),
                           pattern,
                           true,
                           Discharge_TypeGroupMSG);

    this->extractionFromStructure(un);

    un->updateFlagConnection(false);

    un->updateFlagAttention(false);
    un->updateFlagFailure(false);
    un->updateFlagDischarge(false);
    un->updateFlagOff(true);

}




//нет связи ******************************************************************
void BSKMsgManager::analyzer_0x9F_0xC0(QByteArray pattern, UnitNode *un) /*noexcept*/
{
#ifdef QT_DEBUG
    if(100 > pattern.size())
    {
        qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " BskMSG INPUT  << [command 9F_C0] [" << pattern.toHex() << "]";
    }
    else
    {
        qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " BskMSG INPUT  << [command 9F_C0] [" << pattern.left(20).toHex() << "..." << pattern.size() << "..." << pattern.right(10).toHex() << "]";
    }
#endif

    un->updateFlagConnection(false);

    this->extractionFromStructure(un);

    this->emitInsertNewMSG(un,
                           pattern.at(4),
                           pattern,
                           true,
                           Connect_TypeGroupMSG);

//    qDebug() << "no connection " << un->id.toHex();
}

//большой обработчик подтверждения команд ******************************************************************
void BSKMsgManager::analyzer_0x9F_0xCC(QByteArray pattern, UnitNode *un) /*noexcept*/
{
#ifdef QT_DEBUG
    if(100 > pattern.size())
    {
        qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " BskMSG INPUT  << [command 9F_CC] [" << pattern.toHex() << "]";
    }
    else
    {
        qDebug() << QTime::currentTime().toString("hh:mm:ss zzz") << " BskMSG INPUT  << [command 9F_CC] [" << pattern.left(20).toHex() << "..." << pattern.size() << "..." << pattern.right(10).toHex() << "]";
    }
#endif

    this->emitInsertNewMSG(un,
                           pattern.at(4),
                           pattern,
                           true,
                           Command_TypeGroupMSG);

    switch((quint8)pattern.at(9))//код операции
    {
        case (quint8)0x01://отключение СО
        {
            un->updateFlagOff(true);
            //при выключнии устанавливается серый значок
            break;
        }

        case (quint8)0x02://включение СО
        {
            un->updateFlagOff(false);
            //при включении значок выбирается согласно остальным флагам
            break;
        }

        case (quint8)0x05://СО поставленно на охрану
        {
            un->updateFlagOff(false);
            //при включении значок выбирается согласно остальным флагам
            break;
        }

        case (quint8)0x06://СО снято с охраны
        {
            //значок выбирается согласно остальным флагам
            break;
        }

        case (quint8)0x40://установка чувствительности СО
        {

            if(RLD_BSKDeviceType == un->_typeUN())//РЛД
            {
                //temp_un->flag_no_connection = 0;
                un->parameters[0] = (quint8)0x01 + (quint8)pattern.at(11);
            }

            if(RVP_BSKDeviceType == un->_typeUN())//РВП
            {
                //temp_un->flag_no_connection = 0;
                un->parameters[0] = (quint8)0x01 + (quint8)pattern.at(11);
            }
            break;
        }

        case 0x30://установка длины зоны обнаружения РЛО
        {
            //напряжение...только для отображения и записи в БД
            //if((quint8)pattern.at(9) != 0){temp_un->voltage = (quint8)pattern.at(9);}

            un->parameters.resize(1);
            un->parameters[0] = (quint8)pattern.at(11);
            break;
        }

        case (quint8)0x7B://Ответ на запрос конфигурации устройства
        {
#ifdef QT_DEBUG
            qDebug() << "0xCC_0x7B command";
#endif

            //считываем номер участка и номер СО и параметры датчиков

            switch(un->id.at(0)>>4)//тип СО
            {
                case 0x2://сейсмика
                {
                    un->parameters.resize(2);
                    un->parameters[0] = (quint8)pattern.at(12);


                    if(((quint8)pattern.at(11) & 0x40) == 0x40)//выделяем бит сезона (зима/лето)
                    {
                        un->parameters[1] = 1;
                    }
                    else
                    {
                        un->parameters[1] = 0;
                    }
                    break;
                }

                case 0x3://РЛО
                {
                    un->parameters.resize(1);
                    un->parameters[0] = (quint8)pattern.at(12);
                    break;
                }

                case 0x4://РЛД
                {
                    un->parameters[0] = (quint8)0x01 + (quint8)pattern.at(12);
                    break;
                }

                case 0xD://РВП
                {
                    un->parameters[0] = (quint8)0x01 + (quint8)pattern.at(12);
                    break;
                }
            }
            break;
        }

        case (quint8)0x75://подтверждение изменения регистров сейсмики
        {
#ifdef QT_DEBUG
            qDebug() << "0xCC_0x75 command";
#endif
            break;
        }

        case (quint8)0x55://подтверждение ДК
        {
            //установить флаги
            un->updateFlagDischarge(false);    //сброс разряда (по ДК)
            un->updateFlagFailure(false);      //сброс неисправности (по ДК)

#ifdef QT_DEBUG
            qDebug() << "RT confirmation for " << un->id.toHex();
#endif
            break;
        }

        case (quint8)0x89://невозможно выполнить
        {
            //значок выбирается согласно остальным флагам
            break;
        }
    }

}

quint32 BSKMsgManager::emitInsertNewMSG(UnitNode *unSender,
                                       quint8 codeMSG,
                                       QByteArray msg,
                                       bool ioType,
                                       TypeGroupMSG typeGroup,
                                       bool flagHidden) /*noexcept*/
{
    quint32 indexDBMsg(0);
    indexDBMsg = this->dbManager->addNewMSG(unSender,
                                            codeMSG,
                                            msg,
                                            ioType,
                                            BSKSystemType,
                                            typeGroup,
                                            flagHidden);
    return indexDBMsg;
}

void BSKMsgManager::emitTicPattern()
{
    emit this->ticPattern();
}


//quint32 BSKMsgManager::emitInsertNewMSG(quint32 indexDBMsg) /*noexcept*/
//{
//    emit this->insertNewMSG(indexDBMsg);
//    return indexDBMsg;
//}




