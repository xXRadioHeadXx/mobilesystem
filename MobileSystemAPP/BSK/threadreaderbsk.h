#ifndef THREADREADERBSK_H
#define THREADREADERBSK_H

#include <QThread>
#include <QSerialPort>
#include <bskmsgmanager.h>
//#include <COSVariantList.h>
#include <cosvariantlist.h>


class ThreadReaderBSK : public QThread
{
    Q_OBJECT
public:

    QSerialPort *m_port;// порт
    BSKMsgManager *bskMsgManager;
    QByteArray *m_ByteCollector;
    QByteArray newData;
    COSVariantList *m_listPattern;
    COSVariantList *m_listRequest;

    QMutex *m_mutexPattern;
    QMutex *m_mutexRequest;

    explicit ThreadReaderBSK(QSerialPort *port,
                             QByteArray *collector,
                             QByteArray data,
                             COSVariantList *listPattern,
                             COSVariantList *listRequest,
                             QMutex *mutexPattern,
                             QMutex *mutexRequest,
                             QObject *parent = nullptr) /*noexcept*/;
    virtual ~ThreadReaderBSK() /*noexcept*/;

    void analizMainBuffer() /*noexcept*/;
    void possRequestReciept(QByteArray pattern) /*noexcept*/;

    QByteArray setCRC(QByteArray msg);
    QByteArray setPreambula(QByteArray msg);
signals:

protected slots:
     void run() /*noexcept*/;

};

#endif // THREADREADERBSK_H
