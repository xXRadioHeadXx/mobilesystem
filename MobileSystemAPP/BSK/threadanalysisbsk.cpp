#include <threadanalysisbsk.h>

//ThreadAnalysisBSK::ThreadAnalysisBSK(QObject *parent) :
//    QThread(parent)
//{
//}

ThreadAnalysisBSK::ThreadAnalysisBSK(BSKMsgManager *bsk,
                                     QByteArray patternMsg,
                                     QMutex *mutexMSGAnalysis,
                                     QObject *parent) /*noexcept*/ :
    QThread(parent),
    bskMsgManager(bsk),
    pattern(patternMsg),
    m_mutexMSGAnalysis(mutexMSGAnalysis)
{
}

ThreadAnalysisBSK::~ThreadAnalysisBSK() /*noexcept*/
{
    try
    {
        bskMsgManager = nullptr;
        pattern.clear();
    }
    catch(...)
    {
#ifdef QT_DEBUG
        qDebug() << "Assert(~ThreadAnalysisBSK)";
#endif
        return;
    }
}

void ThreadAnalysisBSK::run() /*noexcept*/
{
    try
    {
//        bool mx_stat(false);
//        while(false == mx_stat)
//        {
//            mx_stat = m_mutexMSGAnalysis->tryLock(100);
//        }

        this->bskMsgManager->newMSG(pattern);

//        if(true == mx_stat)
//        {
//            m_mutexMSGAnalysis->unlock();
//        }

        this->quit();

    }
    catch (std::runtime_error &rte)
    {
        qWarning("catch runtime_error %s [ThreadAnalysisBSK::run]", rte.what());
    }
    catch (std::bad_alloc& ba)
    {
        qWarning("catch bad_alloc %s [ThreadAnalysisBSK::run]", ba.what());
    }
    catch (std::exception &e)
    {
        qWarning("catch exception %s [ThreadAnalysisBSK::run]", e.what());
    }
    catch (...)
    {
        qWarning("catch exception <unknown> [ThreadAnalysisBSK::run]");
    }
}
