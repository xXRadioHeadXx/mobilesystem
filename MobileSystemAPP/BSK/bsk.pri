QT += core serialport
CONFIG += c++11

INCLUDEPATH += $$PWD/

HEADERS += \
    $$PWD/serialportmanagerbsk.h \
    $$PWD/bskmanager.h \
    $$PWD/bskmsgmanager.h \
    $$PWD/threadanalysisbsk.h \
    $$PWD/threadreaderbsk.h 

SOURCES += \
    $$PWD/serialportmanagerbsk.cpp \
    $$PWD/bskmanager.cpp \
    $$PWD/bskmsgmanager.cpp \
    $$PWD/threadanalysisbsk.cpp \
    $$PWD/threadreaderbsk.cpp 
