﻿#include <bskmanager.h>

#ifdef QT_DEBUG
#include <QDebug>
#endif

BSKManager::BSKManager(DataBaseManager *db,
                       QObject *parent) /*noexcept*/ :
    QObject(parent),
    m_dbManager(db)
{
    try
    {
        this->m_bskMsgManager = new BSKMsgManager(m_dbManager);
        this->m_portManager = new SerialPortManagerBSK(m_bskMsgManager,
                                                       &m_mutexPattern,
                                                       &m_mutexRequest,
                                                       &m_mutexMSGAnalysis);

        this->timetTicPattern = new QTimer;
        connect(this->timetTicPattern,
                SIGNAL(timeout()),
                this,
                SLOT(timeoutTicPattern()));

        /*экземляр BSKManager создается этим конструктором*/



        //BSK___
        connect(this->m_portManager,
                SIGNAL(portCondition(qint32)),
                this,
                SIGNAL(portCondition(qint32)));

        connect(this->m_bskMsgManager,
                SIGNAL(sendBSKcommand(QByteArray)),
                this->m_portManager,
                SLOT(sendBSKMSG(QByteArray)));

        connect(this->m_bskMsgManager,
                SIGNAL(addNewUN(UnitNode*)),
                this,
                SIGNAL(addNewUN(UnitNode*)));

        connect(this->m_bskMsgManager,
                SIGNAL(updateUN(UnitNode*)),
                this,
                SIGNAL(updateUN(UnitNode*)));

        connect(this->m_bskMsgManager,
                SIGNAL(ticPattern()),
                this,
                SLOT(ticPattern()));

    //    connect(this->m_bskMsgManager,
    //            SIGNAL(setAlarmCondition(UnitNode*)),
    //            this,
    //            SLOT(setAlarmCondition(UnitNode*)));

    //    connect(this->m_bskMsgManager,
    //            SIGNAL(insertNewMSG(quint32)),
    //            this,
    //            SIGNAL(insertNewMSG(quint32)));
        //BSK___
    }
    catch (std::runtime_error &rte)
    {
        qWarning("catch runtime_error %s [BSKManager::BSKManager]", rte.what());
    }
    catch (std::bad_alloc &ba)
    {
        qWarning("catch bad_alloc %s [BSKManager::BSKManager]", ba.what());
    }
    catch (std::exception &e)
    {
        qWarning("catch exception %s [BSKManager::BSKManager]", e.what());
    }
    catch (...)
    {
        qWarning("catch exception <unknown> [BSKManager::BSKManager]");
    }
}

BSKManager::~BSKManager() /*noexcept*/
{
//#ifdef QT_DEBUG
//    qDebug() << "~BSKManager() -->";
//#endif

//    while(!listTimerOffAlarmCondition.isEmpty())
//    {
//        listTimerOffAlarmCondition.first()->stop();
//        delete listTimerOffAlarmCondition.first();
//        listTimerOffAlarmCondition.removeFirst();
//    }

    try
    {
        timetTicPattern->stop();
        delete timetTicPattern;
        delete m_portManager;
        m_portManager = nullptr;
        delete m_bskMsgManager;
        m_bskMsgManager = nullptr;
        m_dbManager = nullptr;
    }
    catch(...)
    {
#ifdef QT_DEBUG
        qDebug() << "Assert(~BSKManager)";
#endif
        return;
    }
//#ifdef QT_DEBUG
//    qDebug() << "~BSKManager() <--";
//#endif
}


//BSK___открытие порта, скопировано из камуфляж-менеджера
bool BSKManager::openPort(QString portName,
                          qint32 rate,
                          QSerialPort::DataBits dataBits,
                          QSerialPort::StopBits stopBits,
                          QSerialPort::FlowControl flow,
                          QSerialPort::Parity parity) /*noexcept*/
{
    //this->m_portManager->inst(this->m_btManager);
    if(this->m_portManager->openPort(portName,
                                     rate,
                                     dataBits,
                                     stopBits,
                                     flow,
                                     parity))
    {
        emit this->updatePortSetting(this->getPortSetting());
#ifdef QT_DEBUG
        qDebug() << "PORT OPENED IS " << portName;
#endif
        this->emitInsertNewMSG(0x00,
                               0xFF,
                               QString("Port " + portName + " opening succeeded").toUtf8(),
                               true,
                               System_TypeGroupMSG);
        this->ticPattern();
        return true;
    }

    this->emitInsertNewMSG(0x00,
                           0xFF,
                           QString("Port " + portName + " opening failed").toUtf8(),
                           true,
                           System_TypeGroupMSG);

    emit this->updatePortSetting(QList<QString>());
    return false;
}
//BSK___



//BSK___закрытие порта
void BSKManager::closePort() /*noexcept*/
{
    this->m_portManager->closePort();
#ifdef QT_DEBUG
    qDebug() << "Closing port";
#endif

    emit this->updatePortSetting(this->getPortSetting());

    this->emitInsertNewMSG(0x00,
                           0xFF,
                           QString("Port closed succeeded").toUtf8(),
                           true,
                           Connect_TypeGroupMSG);
}
//BSK___

void BSKManager::request_0x41(UnitNode *un,
                              quint8 command,
                              quint8 d1,
                              quint8 d2,
                              quint8 d3) /*noexcept*/
{
    this->m_bskMsgManager->request_0x41(un,
                                        command,
                                        d1,
                                        d2,
                                        d3);
}

void BSKManager::request_0x0B(quint8 numKit) /*noexcept*/
{
    this->m_bskMsgManager->request_0x0B(numKit);
}


void BSKManager::newMSGPKP(QByteArray pattern) /*noexcept*/ {
    this->m_bskMsgManager->newMSGPKP(pattern);
}


QList<QString> BSKManager::getPortSetting() /*noexcept*/
{
    QList<QString> listSetting;
    if(this->m_portManager->m_port)
    {
        if(this->m_portManager->isOpen())
        {
            listSetting.append(this->m_portManager->portName());
            QString strBaud("%1");
            strBaud = strBaud.arg(this->m_portManager->baudRate());
            listSetting.append(strBaud);
            QString strData("%1");
            strData = strData.arg(this->m_portManager->dataBits());
            listSetting.append(strData);
            listSetting.append(((0 != this->m_portManager->parity()) ? "Y" : "N"));
            QString strStop("%1");
            strStop = strStop.arg(this->m_portManager->stopBits());
            listSetting.append(strStop);
            return listSetting;
        }
    }
    return listSetting;
}

QList<UnitNode*> BSKManager::getListUnitNode() /*noexcept*/
{
    return this->m_bskMsgManager->listBSKUnitNode;
}

quint32 BSKManager::emitInsertNewMSG(UnitNode *unSender,
                                     quint8 codeMSG,
                                     QByteArray msg,
                                     bool ioType,
                                     TypeGroupMSG typeGroup,
                                     bool flagHidden) /*noexcept*/
{
    quint32 indexDBMsg(0);
    indexDBMsg = this->m_dbManager->addNewMSG(unSender,
                                              codeMSG,
                                              msg,
                                              ioType,
                                              BSKSystemType,
                                              typeGroup,
                                              flagHidden);
    return indexDBMsg;
}

void BSKManager::ticPattern()
{
    if(this->timetTicPattern->isActive())
    {
        this->timetTicPattern->stop();
    }
    else
    {
        this->emitInsertNewMSG(0x00,
                               0xFF,
                               QString("Recovered connect").toUtf8(),
                               true,
                               Connect_TypeGroupMSG);
    }
    this->timetTicPattern->start(10000);
}

void BSKManager::timeoutTicPattern()
{
    this->timetTicPattern->stop();

    QList<UnitNode*> list(this->getListUnitNode());
    for(int i(0), n(list.size()); i < n; i++)
    {
        list.at(i)->updateFlagConnection(false);
    }


    this->emitInsertNewMSG(0x00,
                           0xFF,
                           QString("Lost connect").toUtf8(),
                           true,
                           Connect_TypeGroupMSG);
}

//void BSKManager::setAlarmCondition(UnitNode *un) /*noexcept*/
//{
//    QTimer *timerOffAlarmCondition(new QTimer(this));

//    connect(timerOffAlarmCondition,
//            SIGNAL(timeout()),
//            un,
//            SLOT(offAlarmCondition()));

//    connect(timerOffAlarmCondition,
//            SIGNAL(timeout()),
//            timerOffAlarmCondition,
//            SLOT(stop()));

//    connect(timerOffAlarmCondition,
//            SIGNAL(timeout()),
//            this,
//            SLOT(offAlarmCondition()));

//    this->listTimerOffAlarmCondition.append(timerOffAlarmCondition);

//    un->setAlarmCondition();

//    timerOffAlarmCondition->start(3000);

//}

//void BSKManager::offAlarmCondition() /*noexcept*/
//{
//    for(int i(0); i < this->listTimerOffAlarmCondition.size();)
//    {
//        if(this->listTimerOffAlarmCondition.at(i)->isActive())
//        {
//            i++;
//        }
//        else
//        {
//            delete this->listTimerOffAlarmCondition.at(i);
//            this->listTimerOffAlarmCondition.removeAt(i);
//        }
//    }
//}
