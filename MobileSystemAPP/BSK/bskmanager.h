﻿#ifndef BSKMANAGER_H
#define BSKMANAGER_H

#include <QObject>

#include <serialportmanagerbsk.h>
#include <bskmsgmanager.h>

class BSKManager : public QObject
{
    Q_OBJECT
public:
    DataBaseManager *m_dbManager;
    SerialPortManagerBSK    *m_portManager;     //чтение и запись в порт
    BSKMsgManager           *m_bskMsgManager;   //обработка сообщений
    QList<QTimer *> listTimerOffAlarmCondition;
    QTimer *timetTicPattern;

    QMutex m_mutexPattern;
    QMutex m_mutexRequest;
    QMutex m_mutexMSGAnalysis;

//    explicit BSKManager(QObject *parent = nullptr);
    explicit BSKManager(DataBaseManager *db,
                        QObject *parent = nullptr) /*noexcept*/;
    virtual ~BSKManager() /*noexcept*/;

    quint32 emitInsertNewMSG(UnitNode *unSender,
                          quint8 codeMSG,
                          QByteArray msg,
                          bool ioType,
                          TypeGroupMSG typeGroup,
                          bool flagHidden = false) /*noexcept*/;


    QList<QString> getPortSetting() /*noexcept*/;
    QList<UnitNode*> getListUnitNode() /*noexcept*/;
    void getConfig_0x41x7C(UnitNode *un) /*noexcept*/;
    void switchON_0x41x02(UnitNode *un) /*noexcept*/;
    void switchOFF_0x41x01(UnitNode *un) /*noexcept*/;
    void sendDK_0x41x55(UnitNode *un) /*noexcept*/;
    void changeSensetivity_RLD_BSK(UnitNode *un,
                                   quint8 value) /*noexcept*/;
    void changeArea_RLO_BSK(UnitNode *un,
                            quint8 value) /*noexcept*/;

    void changeReg_S_BSK(UnitNode *un,
                         quint8 addr,
                         bool value) /*noexcept*/;

    void analyzer_0x9F_0xBB(UnitNode *un,
                            QByteArray pattern) /*noexcept*/;
    void analyzer_0x9F_0x01(UnitNode *un,
                            QByteArray pattern) /*noexcept*/;
    void analyzer_0x9F_0x02(UnitNode *un,
                            QByteArray pattern) /*noexcept*/;
    void analyzer_0x9F_0x11(UnitNode *un,
                            QByteArray pattern) /*noexcept*/;

    void request_0x41(UnitNode *un,
                      quint8 command = 0x00,
                      quint8 d1 = 0x00,
                      quint8 d2 = 0x00,
                      quint8 d3 = 0x00) /*noexcept*/;
    void request_0x0B(quint8 numKit) /*noexcept*/;

signals:

    void addNewUN(UnitNode *un);
    void updateUN(UnitNode *un);
    void insertNewMSG(quint32 indexDB);
    void updatePortSetting(QList<QString> setting);
    void portCondition(qint32 condition);

public slots:
    void ticPattern();
    void timeoutTicPattern();
    bool openPort(QString portName = "COM1",
                  qint32 rate = QSerialPort::Baud38400,
                  QSerialPort::DataBits dataBit = QSerialPort::Data8,
                  QSerialPort::StopBits stopBit = QSerialPort::TwoStop,
                  QSerialPort::FlowControl flow = QSerialPort::NoFlowControl,
                  QSerialPort::Parity parity = QSerialPort::NoParity) /*noexcept*/;

    void closePort() /*noexcept*/;
    void newMSGPKP(QByteArray pattern) /*noexcept*/;
//    void setAlarmCondition(UnitNode *un) /*noexcept*/;
//    void offAlarmCondition() /*noexcept*/;
    //BSK___
};

#endif // BSKMANAGER_H
