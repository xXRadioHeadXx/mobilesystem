﻿#include <dialogtablearchive.h>
#include <ui_dialogtablearchive.h>

DialogTableArchive::DialogTableArchive(DataBaseManager *db,
                                       KamuflageManager *km,
                                       KiparisManager *ks,
                                       BSKManager *bsk,
                                       QWidget *parent) /*noexcept*/ :
QMainWindow(parent),
dbManager(db),
kmManager(km),
ksManager(ks),
bskManager(bsk),
currentSystemType(UnknownSystemType),
isFirstShow(true),
ui(new Ui::DialogTableArchive)
{
    ui->setupUi(this);

    try
    {
        modelArchiveIMG_KM = (new TableModelArchiveIMG(db,
                                                    KMSystemType,
                                                    this));
        modelArchiveMSG_KM = (new TableModelArchiveMSG(db,
                                                    KMSystemType,
                                                    this));
        modelArchiveIMG_KS = (new TableModelArchiveIMG(db,
                                                    KSSystemType,
                                                    this));
        modelArchiveMSG_KS = (new TableModelArchiveMSG(db,
                                                    KSSystemType,
                                                    this));
        modelArchiveMSG_BSK = (new TableModelArchiveMSG(db,
                                                     BSKSystemType,
                                                     this));

    #ifdef QT_DEBUG
    #else
        ui->groupBox_TypeMSG->setVisible(false);
    #endif

        ui->pushButton_ResetFilter->click();

        this->modelArchiveMSG_KM->setNeedScroll(ui->actionSetAutoScroll->isChecked());
        this->modelArchiveIMG_KM->setNeedScroll(ui->actionSetAutoScroll->isChecked());
        this->modelArchiveIMG_KS->setNeedScroll(ui->actionSetAutoScroll->isChecked());
        this->modelArchiveMSG_KS->setNeedScroll(ui->actionSetAutoScroll->isChecked());
        this->modelArchiveMSG_BSK->setNeedScroll(ui->actionSetAutoScroll->isChecked());


        connect(this->modelArchiveMSG_KM,
                   SIGNAL(needScrollToBottom()),
                   ui->tableView_Archive,
                   SLOT(scrollToBottom()));
        connect(this->kmManager,
                SIGNAL(insertNewMSG(quint32)),
                this->modelArchiveMSG_KM,
                SLOT(updateListRecords(quint32)));

        connect(this->modelArchiveIMG_KM,
                   SIGNAL(needScrollToBottom()),
                   ui->tableView_Archive,
                   SLOT(scrollToBottom()));
        connect(this->kmManager,
                SIGNAL(insertNewIMG(quint32)),
                this->modelArchiveIMG_KM,
                SLOT(updateListRecords(quint32)));
        connect(this->modelArchiveIMG_KM,
                SIGNAL(needViewIMG(quint32,int)),
                this,
                SIGNAL(needViewIMG(quint32,int)));

        connect(this->modelArchiveMSG_KS,
                   SIGNAL(needScrollToBottom()),
                   ui->tableView_Archive,
                   SLOT(scrollToBottom()));
        connect(this->ksManager,
                SIGNAL(insertNewMSG(quint32)),
                this->modelArchiveMSG_KS,
                SLOT(updateListRecords(quint32)));

        connect(this->modelArchiveIMG_KS,
                   SIGNAL(needScrollToBottom()),
                   ui->tableView_Archive,
                   SLOT(scrollToBottom()));
        connect(this->ksManager,
                SIGNAL(insertNewIMG(quint32)),
                this->modelArchiveIMG_KS,
                SLOT(updateListRecords(quint32)));
        connect(this->modelArchiveIMG_KS,
                SIGNAL(needViewIMG(quint32,int)),
                this,
                SIGNAL(needViewIMG(quint32,int)));

        connect(this->modelArchiveMSG_BSK,
                   SIGNAL(needScrollToBottom()),
                   ui->tableView_Archive,
                   SLOT(scrollToBottom()));
        connect(this->bskManager,
                SIGNAL(insertNewMSG(quint32)),
                this->modelArchiveMSG_BSK,
                SLOT(updateListRecords(quint32)));

    //    connect(ui->centralwidget,
    //            SIGNAL(),
    //            this,
    //            SLOT(setDefaultTableSize()));

        ui->pushButton_ResetFilter->click();
        ui->actionSetViewBSKMSGArchive->trigger();
    }
    catch (std::runtime_error &rte)
    {
        qWarning("catch runtime_error %s [DialogTableArchive::DialogTableArchive]", rte.what());
    }
    catch (std::bad_alloc& ba)
    {
        qWarning("catch bad_alloc %s [DialogTableArchive::DialogTableArchive]", ba.what());
    }
    catch (std::exception &e)
    {
        qWarning("catch exception %s [DialogTableArchive::DialogTableArchive]", e.what());
    }
    catch (...)
    {
        qWarning("catch exception <unknown> [DialogTableArchive::DialogTableArchive]");
    }
}

DialogTableArchive::~DialogTableArchive() /*noexcept*/
{
    try
    {
        delete ui;

        dbManager = nullptr;
        kmManager = nullptr;
        ksManager = nullptr;
        bskManager = nullptr;

        delete modelArchiveIMG_KM;
        delete modelArchiveMSG_KM;
        delete modelArchiveIMG_KS;
        delete modelArchiveMSG_KS;
        delete modelArchiveMSG_BSK;
    }
    catch(...)
    {
#ifdef QT_DEBUG
        qDebug() << "Assert(~DialogTableArchive)";
#endif
        return;
    }
}

//void DialogTableArchive::resizeEvent(QResizeEvent *event)
//{
//}

void DialogTableArchive::on_actionSetAutoScroll_triggered(bool checked) /*noexcept*/
{
    this->modelArchiveMSG_KM->setNeedScroll(checked);
    this->modelArchiveIMG_KM->setNeedScroll(checked);
    this->modelArchiveMSG_KS->setNeedScroll(checked);
    this->modelArchiveIMG_KS->setNeedScroll(checked);
    this->modelArchiveMSG_BSK->setNeedScroll(checked);
}

void DialogTableArchive::closeEvent(QCloseEvent *event) /*noexcept*/
{
    emit this->imClosed(false);
}

void DialogTableArchive::retranslate() /*noexcept*/
{
    ui->retranslateUi(this);
}

void DialogTableArchive::on_actionSetViewKMMSGArchive_triggered() /*noexcept*/
{
    ui->actionSetViewKMMSGArchive->setChecked(true);
    ui->actionSetViewKSMSGArchive->setChecked(false);
    ui->actionSetViewBSKMSGArchive->setChecked(false);
    ui->actionSetViewIMGArchive_KM->setChecked(false);
    ui->actionSetViewIMGArchive_KS->setChecked(false);
    ui->tableView_Archive->setModel(this->modelArchiveMSG_KM);
    currentSystemType = KMSystemType;
    currentFilter = this->modelArchiveMSG_KM->currentFilter;
    viewCurrentFilter();
    setDefaultTableSize();
}

void DialogTableArchive::on_actionSetViewKSMSGArchive_triggered() /*noexcept*/
{
    ui->actionSetViewKMMSGArchive->setChecked(false);
    ui->actionSetViewKSMSGArchive->setChecked(true);
    ui->actionSetViewBSKMSGArchive->setChecked(false);
    ui->actionSetViewIMGArchive_KM->setChecked(false);
    ui->actionSetViewIMGArchive_KS->setChecked(false);
    ui->tableView_Archive->setModel(this->modelArchiveMSG_KS);
    currentSystemType = KSSystemType;
    currentFilter = this->modelArchiveMSG_KS->currentFilter;
    viewCurrentFilter();
    setDefaultTableSize();
}

void DialogTableArchive::on_actionSetViewBSKMSGArchive_triggered() /*noexcept*/
{
    ui->actionSetViewKMMSGArchive->setChecked(false);
    ui->actionSetViewKSMSGArchive->setChecked(false);
    ui->actionSetViewBSKMSGArchive->setChecked(true);
    ui->actionSetViewIMGArchive_KM->setChecked(false);
    ui->actionSetViewIMGArchive_KS->setChecked(false);
    ui->tableView_Archive->setModel(this->modelArchiveMSG_BSK);
    currentSystemType = BSKSystemType;
    currentFilter = this->modelArchiveMSG_BSK->currentFilter;
    viewCurrentFilter();
    setDefaultTableSize();
}

void DialogTableArchive::on_actionSetViewIMGArchive_KM_triggered() /*noexcept*/
{
    ui->actionSetViewKMMSGArchive->setChecked(false);
    ui->actionSetViewKSMSGArchive->setChecked(false);
    ui->actionSetViewBSKMSGArchive->setChecked(false);
    ui->actionSetViewIMGArchive_KM->setChecked(true);
    ui->actionSetViewIMGArchive_KS->setChecked(false);
    ui->tableView_Archive->setModel(this->modelArchiveIMG_KM);
    currentSystemType = KMSystemType;
    currentFilter = this->modelArchiveIMG_KM->currentFilter;
    viewCurrentFilter();
    setDefaultTableSize();
}

void DialogTableArchive::on_actionSetViewIMGArchive_KS_triggered() /*noexcept*/
{
    ui->actionSetViewKMMSGArchive->setChecked(false);
    ui->actionSetViewKSMSGArchive->setChecked(false);
    ui->actionSetViewBSKMSGArchive->setChecked(false);
    ui->actionSetViewIMGArchive_KM->setChecked(false);
    ui->actionSetViewIMGArchive_KS->setChecked(true);
    ui->tableView_Archive->setModel(this->modelArchiveIMG_KS);
    currentSystemType = KSSystemType;
    currentFilter = this->modelArchiveIMG_KS->currentFilter;
    viewCurrentFilter();
    setDefaultTableSize();
}

void DialogTableArchive::on_pushButton_ResetFilter_clicked() /*noexcept*/
{
    this->resetFilter();
    ui->tableView_Archive->scrollToBottom();
}

void DialogTableArchive::resetFilter() /*noexcept*/
{
    ui->groupBox_Filter_Date->setChecked(false);
    ui->groupBox_Filter_Time->setChecked(false);
    ui->groupBox_Filter_SectorNumber->setChecked(false);
    ui->groupBox_Filter_Type->setChecked(false);
    ui->groupBox_Filter_ID->setChecked(false);

    ui->checkBox_msgAlarm->setChecked(true);
    ui->checkBox_msgConnect->setChecked(true);
    ui->checkBox_msgDischarge->setChecked(true);
    ui->checkBox_msgError->setChecked(true);
    ui->checkBox_msgFF->setChecked(true);
    ui->checkBox_msgSystem->setChecked(true);
    ui->checkBox_msgCommand->setChecked(true);

    ui->dateEdit_From->setDate(dbManager->getMinDateMSG());
    ui->dateEdit_To->setDate(QDate::currentDate());

    ui->timeEdit_From->setTime(QTime(00, 00, 00));
    ui->timeEdit_To->setTime(QTime(23, 59, 59));

    ui->comboBox_Sector->clear();
    ui->comboBox_Sector->addItem(tr("All"), QVariant(-1));
    QList<int> listSector(dbManager->getSectorGroupUN(currentSystemType));
//    auto listSector(dbManager->getSectorGroupUN(currentSystemType));
    for(int i(0), n(listSector.size()); i < n; i++)
    {   QString str("%1");
        str = str.arg(listSector.at(i));
        ui->comboBox_Sector->addItem(str, QVariant(listSector.at(i)));
    }
    ui->comboBox_Sector->setCurrentIndex(0);

    ui->comboBox_Number->clear();
    ui->comboBox_Number->addItem(tr("All"), QVariant(-1));
    QList<int> listNumber(dbManager->getNumberGroupUN(currentSystemType));
//    auto listNumber(dbManager->getNumberGroupUN(currentSystemType));
    for(int i(0), n(listNumber.size()); i < n; i++)
    {   QString str("%1");
        str = str.arg(listNumber.at(i));
        ui->comboBox_Number->addItem(str, QVariant(listNumber.at(i)));
    }
    ui->comboBox_Number->setCurrentIndex(0);

    ui->comboBox_Type->clear();
    ui->comboBox_Type->addItem(tr("All"), QVariant(UnknownDeviceType));
//    auto listType(dbManager->getTypeGroupUN(currentSystemType));
    QList<TypeUnitNodeDevice> listType(dbManager->getTypeGroupUN(currentSystemType));
    for(int i(0), n(listType.size()); i < n; i++)
    {
        QByteArray id;
        id.append((quint8)listType.at(i) * 0x10);
        id.append((char)0x00);
        QString str(ProxyMetod().getNameDevice(id, currentSystemType));

        bool addTypeItemKey(true);
        for(int j(0), m(ui->comboBox_Type->count()); j < m; j++)
        {
            if(ui->comboBox_Type->itemText(j).contains(str))
            {
                addTypeItemKey = false;
                break;
            }
        }

        if(true == addTypeItemKey)
            ui->comboBox_Type->addItem(str, QVariant(listType.at(i)));
    }
    ui->comboBox_Type->setCurrentIndex(0);

    ui->comboBox_ID->clear();
    ui->comboBox_ID->addItem(tr("All"), QVariant(QByteArray("")));
    QList<UnitNode*> listUN;
    if(KMSystemType == currentSystemType)
    {
        listUN = kmManager->getListUnitNode();
    }
    if(KSSystemType == currentSystemType)
    {
        listUN = ksManager->getListUnitNode();
    }
    if(BSKSystemType == currentSystemType)
    {
        listUN = bskManager->getListUnitNode();
    }
    for(int i(0), n(listUN.size()); i < n; i++)
    {
        ui->comboBox_ID->addItem(listUN.at(i)->_id().toHex().toUpper(), QVariant(listUN.at(i)->_id()));
    }

    ui->pushButton_ApplyFilter->click();
}

void DialogTableArchive::on_pushButton_ApplyFilter_clicked() /*noexcept*/
{
    this->applyFilter();
}

void DialogTableArchive::applyFilter() /*noexcept*/
{
    currentFilter.filterDate = ui->groupBox_Filter_Date->isChecked();
    currentFilter.filterMinDate = ui->dateEdit_From->date();
    currentFilter.filterMaxDate = ui->dateEdit_To->date();

    currentFilter.filterTime = ui->groupBox_Filter_Time->isChecked();
    currentFilter.filterMinTime = ui->timeEdit_From->time();
    currentFilter.filterMaxTime = ui->timeEdit_To->time();

    currentFilter.filterSN = ui->groupBox_Filter_SectorNumber->isChecked();
    currentFilter.filterSector = ui->comboBox_Sector->currentData().toInt();
    currentFilter.filterNumber = ui->comboBox_Number->currentData().toInt();

    currentFilter.filterType = ui->groupBox_Filter_Type->isChecked();
    currentFilter.filterTypeValue = (TypeUnitNodeDevice)ui->comboBox_Type->currentData().toInt();

    currentFilter.filterID = ui->groupBox_Filter_ID->isChecked();
    currentFilter.filterIDValue = ui->comboBox_ID->currentData().toByteArray();

    currentFilter.filterMSGGroup = ui->groupBox_TypeMSG->isChecked();
    currentFilter.filterMSGAlarm = ui->checkBox_msgAlarm->isChecked();
    currentFilter.filterMSGConnect = ui->checkBox_msgConnect->isChecked();
    currentFilter.filterMSGDischarge = ui->checkBox_msgDischarge->isChecked();
    currentFilter.filterMSGError = ui->checkBox_msgError->isChecked();
    currentFilter.filterMSGFF = ui->checkBox_msgFF->isChecked();
    currentFilter.filterMSGSystem = ui->checkBox_msgSystem->isChecked();
    currentFilter.filterMSGCommand = ui->checkBox_msgCommand->isChecked();

    currentFilter.filterMode = currentFilter.filterDate && currentFilter.filterTime &&
                               currentFilter.filterSN && currentFilter.filterType &&
                               currentFilter.filterID && currentFilter.filterMSGGroup;

    if(KMSystemType == currentSystemType)
    {
        this->modelArchiveMSG_KM->updateFilter(currentFilter);
        this->modelArchiveIMG_KM->updateFilter(currentFilter);
    }
    if(KSSystemType == currentSystemType)
    {
        this->modelArchiveMSG_KS->updateFilter(currentFilter);
        this->modelArchiveIMG_KS->updateFilter(currentFilter);
    }
    if(BSKSystemType == currentSystemType)
    {
        this->modelArchiveMSG_BSK->updateFilter(currentFilter);
    }

}

void DialogTableArchive::needReset() /*noexcept*/
{
    if(isFirstShow)
    {
        isFirstShow = false;
        ui->pushButton_ResetFilter->click();
    }
}

void DialogTableArchive::viewCurrentFilter()
{
    ui->groupBox_Filter_Date->setChecked(currentFilter.filterDate);
    ui->groupBox_Filter_Time->setChecked(currentFilter.filterTime);
    ui->groupBox_Filter_SectorNumber->setChecked(currentFilter.filterSN);
    ui->groupBox_Filter_Type->setChecked(currentFilter.filterType);
    ui->groupBox_Filter_ID->setChecked(currentFilter.filterID);
    ui->groupBox_TypeMSG->setChecked(currentFilter.filterMSGGroup);

    ui->checkBox_msgAlarm->setChecked(currentFilter.filterMSGAlarm);
    ui->checkBox_msgConnect->setChecked(currentFilter.filterMSGConnect);
    ui->checkBox_msgDischarge->setChecked(currentFilter.filterMSGDischarge);
    ui->checkBox_msgError->setChecked(currentFilter.filterMSGError);
    ui->checkBox_msgFF->setChecked(currentFilter.filterMSGFF);
    ui->checkBox_msgSystem->setChecked(currentFilter.filterMSGSystem);
    ui->checkBox_msgCommand->setChecked(currentFilter.filterMSGCommand);

    ui->dateEdit_From->setDate(currentFilter.filterMinDate);
    ui->dateEdit_To->setDate(currentFilter.filterMaxDate);

    ui->timeEdit_From->setTime(currentFilter.filterMinTime);
    ui->timeEdit_To->setTime(currentFilter.filterMaxTime);

    ui->comboBox_Sector->clear();
    ui->comboBox_Sector->addItem(tr("All"), QVariant(-1));
    QList<int> listSector(dbManager->getSectorGroupUN(currentSystemType));
//    auto listSector(dbManager->getSectorGroupUN(currentSystemType));
    for(int i(0), n(listSector.size()); i < n; i++)
    {   QString str("%1");
        str = str.arg(listSector.at(i));
        ui->comboBox_Sector->addItem(str, QVariant(listSector.at(i)));
    }
    for(int i(0), n(ui->comboBox_Sector->count()); i < n; i++)
    {
        if(ui->comboBox_Sector->itemData(i).toInt() == currentFilter.filterSector)
        {
            ui->comboBox_Sector->setCurrentIndex(i);
            break;
        }
    }

    ui->comboBox_Number->clear();
    ui->comboBox_Number->addItem(tr("All"), QVariant(-1));
    QList<int> listNumber(dbManager->getNumberGroupUN(currentSystemType));
//    auto listNumber(dbManager->getNumberGroupUN(currentSystemType));
    for(int i(0), n(listNumber.size()); i < n; i++)
    {   QString str("%1");
        str = str.arg(listNumber.at(i));
        ui->comboBox_Number->addItem(str, QVariant(listNumber.at(i)));
    }
    for(int i(0), n(ui->comboBox_Number->count()); i < n; i++)
    {
        if(ui->comboBox_Number->itemData(i).toInt() == currentFilter.filterNumber)
        {
            ui->comboBox_Number->setCurrentIndex(i);
            break;
        }
    }

    ui->comboBox_Type->clear();
    ui->comboBox_Type->addItem(tr("All"), QVariant(UnknownDeviceType));
//    auto listType(dbManager->getTypeGroupUN(currentSystemType));
    QList<TypeUnitNodeDevice> listType(dbManager->getTypeGroupUN(currentSystemType));
    for(int i(0), n(listType.size()); i < n; i++)
    {
        QByteArray id;
        id.append((quint8)listType.at(i) * 0x10);
        id.append((char)0x00);
        QString str(ProxyMetod().getNameDevice(id, currentSystemType));

        bool addTypeItemKey(true);
        for(int j(0), m(ui->comboBox_Type->count()); j < m; j++)
        {
            if(ui->comboBox_Type->itemText(j).contains(str))
            {
                addTypeItemKey = false;
                break;
            }
        }

        if(true == addTypeItemKey)
            ui->comboBox_Type->addItem(str, QVariant(listType.at(i)));
    }
    for(int i(0), n(ui->comboBox_Type->count()); i < n; i++)
    {
        if(ui->comboBox_Type->itemData(i).toInt() == currentFilter.filterTypeValue)
        {
            ui->comboBox_Type->setCurrentIndex(i);
            break;
        }
    }

    ui->comboBox_ID->clear();
    ui->comboBox_ID->addItem(tr("All"), QVariant(QByteArray("")));
    QList<UnitNode*> listUN;
    if(KMSystemType == currentSystemType)
    {
        listUN = kmManager->getListUnitNode();
    }
    if(KSSystemType == currentSystemType)
    {
        listUN = ksManager->getListUnitNode();
    }
    if(BSKSystemType == currentSystemType)
    {
        listUN = bskManager->getListUnitNode();
    }
    for(int i(0), n(listUN.size()); i < n; i++)
    {
        ui->comboBox_ID->addItem(listUN.at(i)->_id().toHex().toUpper(), QVariant(listUN.at(i)->_id()));
    }
    for(int i(0), n(ui->comboBox_ID->count()); i < n; i++)
    {
        if(ui->comboBox_ID->itemData(i).toByteArray() == currentFilter.filterIDValue)
        {
            ui->comboBox_ID->setCurrentIndex(i);
            break;
        }
    }
}

void DialogTableArchive::setDefaultTableSize() /*noexcept*/
{
    if(this->modelArchiveMSG_KM == ui->tableView_Archive->model())
    {
        ui->tableView_Archive->setColumnWidth(0, 55);
        ui->tableView_Archive->setColumnWidth(1, 90);
        ui->tableView_Archive->setColumnWidth(2, 95);
        ui->tableView_Archive->setColumnWidth(3, 65);
        ui->tableView_Archive->setColumnWidth(4, 85);//550
        ui->tableView_Archive->setColumnWidth(5, 65);
        ui->tableView_Archive->setColumnWidth(6, 65);

        int commentColumnWidth(ui->tableView_Archive->columnWidth(7));
        ui->tableView_Archive->setColumnWidth(4, commentColumnWidth);//550
        ui->tableView_Archive->setColumnWidth(7, 85);
        for(int i(0), n(ui->tableView_Archive->model()->rowCount()); i < n; i++)
        {
            ui->tableView_Archive->setRowHeight(i, 20);
        }
        return;
    }
    if(this->modelArchiveIMG_KM == ui->tableView_Archive->model())
    {
        ui->tableView_Archive->setColumnWidth(0, 55);
        ui->tableView_Archive->setColumnWidth(1, 105);
        ui->tableView_Archive->setColumnWidth(2, 65);
        ui->tableView_Archive->setColumnWidth(3, 65);
        ui->tableView_Archive->setColumnWidth(4, 80);
        ui->tableView_Archive->setColumnWidth(5, 80);
        ui->tableView_Archive->setColumnWidth(6, 80);
        ui->tableView_Archive->setColumnWidth(7, 80);
        ui->tableView_Archive->setColumnWidth(8, 80);

        for(int i(0), n(ui->tableView_Archive->model()->rowCount()); i < n; i++)
        {
            ui->tableView_Archive->setRowHeight(i, 60);
        }

        return;
    }
    if(this->modelArchiveMSG_KS == ui->tableView_Archive->model())
    {
        ui->tableView_Archive->setColumnWidth(0, 55);
        ui->tableView_Archive->setColumnWidth(1, 50);
        ui->tableView_Archive->setColumnWidth(2, 95);
        ui->tableView_Archive->setColumnWidth(3, 65);
        ui->tableView_Archive->setColumnWidth(4, 85);//492
        ui->tableView_Archive->setColumnWidth(5, 65);
        ui->tableView_Archive->setColumnWidth(6, 65);

        int commentColumnWidth(ui->tableView_Archive->columnWidth(7));
        ui->tableView_Archive->setColumnWidth(4, commentColumnWidth);//550
        ui->tableView_Archive->setColumnWidth(7, 85);
        for(int i(0), n(ui->tableView_Archive->model()->rowCount()); i < n; i++)
        {
            ui->tableView_Archive->setRowHeight(i, 20);
        }
        return;
    }
    if(this->modelArchiveIMG_KS == ui->tableView_Archive->model())
    {
        ui->tableView_Archive->setColumnWidth(0, 55);
        ui->tableView_Archive->setColumnWidth(1, 105);
        ui->tableView_Archive->setColumnWidth(2, 65);
        ui->tableView_Archive->setColumnWidth(3, 65);
        ui->tableView_Archive->setColumnWidth(4, 80);
        ui->tableView_Archive->setColumnWidth(5, 80);
        ui->tableView_Archive->setColumnWidth(6, 80);
        ui->tableView_Archive->setColumnWidth(7, 80);
        ui->tableView_Archive->setColumnWidth(8, 80);

        for(int i(0), n(ui->tableView_Archive->model()->rowCount()); i < n; i++)
        {
            ui->tableView_Archive->setRowHeight(i, 60);
        }

        return;
    }
    if(this->modelArchiveMSG_BSK == ui->tableView_Archive->model())
    {
        ui->tableView_Archive->setColumnWidth(0, 55);
        ui->tableView_Archive->setColumnWidth(1, 50);
        ui->tableView_Archive->setColumnWidth(2, 95);
        ui->tableView_Archive->setColumnWidth(3, 65);
        ui->tableView_Archive->setColumnWidth(4, 85);//174
        ui->tableView_Archive->setColumnWidth(5, 65);
        ui->tableView_Archive->setColumnWidth(6, 65);

        int commentColumnWidth(ui->tableView_Archive->columnWidth(7));
        ui->tableView_Archive->setColumnWidth(4, commentColumnWidth);//550
        ui->tableView_Archive->setColumnWidth(7, 85);

        for(int i(0), n(ui->tableView_Archive->model()->rowCount()); i < n; i++)
        {
            ui->tableView_Archive->setRowHeight(i, 20);
        }
        return;
    }
}


void DialogTableArchive::on_tableView_Archive_doubleClicked(const QModelIndex &index)
{
    if(this->modelArchiveIMG_KM == ui->tableView_Archive->model())
    {
        this->modelArchiveIMG_KM->emitNeedViewIMG(index);
        this->close();
    }
    if(this->modelArchiveIMG_KS == ui->tableView_Archive->model())
    {
        this->modelArchiveIMG_KS->emitNeedViewIMG(index);
        this->close();
    }
}
