#ifndef DIALOGIMAGEVIWER_H
#define DIALOGIMAGEVIWER_H

#include <QDialog>
#include <QScrollBar>
#include <QSqlRecord>
#include <QFile>
#include <QDateTime>
#include <QFileDialog>

#ifdef QT_DEBUG
#include <QDebug>
#endif

namespace Ui {
class DialogImageViwer;
}

class DialogImageViwer : public QDialog
{
    Q_OBJECT

public:
    explicit DialogImageViwer(QWidget *parent = nullptr) /*noexcept*/;
    virtual ~DialogImageViwer() /*noexcept*/;

    void setMainPXM(QPixmap pxm,
                    QString label,
                    QSqlRecord record, int numShot) /*noexcept*/;
    void scaleImage(double factor) /*noexcept*/;
    void adjustScrollBar(QScrollBar *scrollBar, double factor) /*noexcept*/;


//    QScrollArea *scrollArea;
    QPixmap mainPXM;
    QString mainLable;
    double scaleFactor;
    QSqlRecord imgRecord;
    int numCurrentShot;

protected:
//    virtual void closeEvent(QCloseEvent *event);

protected slots:
    void resizeEvent(QResizeEvent *e) /*noexcept*/;

private slots:
    void on_pushButton_zoomPlus_IMG_clicked() /*noexcept*/;

    void on_pushButton_zoomMinus_IMG_clicked() /*noexcept*/;

    void on_pushButton_Close_ImageView_clicked() /*noexcept*/;

    void on_pushButton_Save_clicked();

private:
    Ui::DialogImageViwer *ui;
};

#endif // DIALOGIMAGEVIWER_H
